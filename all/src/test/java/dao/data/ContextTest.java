package dao.data;

import org.junit.Test;

public class ContextTest {
	private dao.data.Context dc;// = null;
	
	public ContextTest() {
		this.dc = new dao.data.Context("dao.sqlite.Adapter");
	}
	
	@Test
	public void test_0001() {
	  //final java.lang.String methodName = "test_0001";
		final dro.meta.Data md = new dro.meta.Data("keys-values");
		md.add(new dro.meta.data.Field("id"   ).set("$type", java.lang.Integer.class));
		md.add(new dro.meta.data.Field("key"  ).set("$type", java.lang. String.class));
		md.add(new dro.meta.data.Field("value").set("$type", java.lang. String.class));
	  //this.dc.invoke(md, (dro.Data)null, "insert"); //<<<====
		final dro.Data d = new dro.Data(md);
		d.set("key", "value");
		d.set("key", "value");
	  //this.dc.invoke(md, d, "insert"); //<<<====
	}
	
	@Test
	public void test_0002() { // FIXME: how do we know this is of type 'keys'
		final java.lang.String methodName = "test_0001";
		this.dc.invoke(dao.data.Context.class, this, methodName, new java.lang.Object[]{this.dc, (dro.meta.Data)null, "read", new java.lang.Object[]{}});
	}
  /*@Test
	public void test_0002() {
		this.dc.invoke((java.lang.Class<?>)null, (java.lang.Object)null, "$init", (java.lang.Object[])null);
		final Map<java.lang.Object, dro.meta.Data> mds = dro.meta.Data.map;
		for (final java.lang.Object mdid: mds.keySet()) {
			final dro.meta.Data md = mds.get(mdid);
			@SuppressWarnings("unchecked")
			final Map<java.lang.Object, dro.Data> ds = (Map<java.lang.Object, dro.Data>)md.get("$dro$Data%id");
			for (final java.lang.Object did: ds.keySet()) {
				final dro.Data d = ds.get(did);
				@SuppressWarnings("unchecked")
				final Map<java.lang.Object, dro.data.Field> dfs = (Map<java.lang.Object, dro.data.Field>)d.get("$dro$data$Field%id");
				for (final java.lang.Object dfid: dfs.keySet()) {
					final dro.data.Field df = dfs.get(dfid);
					final dro.meta.data.Field mdf = (dro.meta.data.Field)df.get("$dro$meta$data$Field");
					System.out.println("mdf.$id="+mdf.get("$id")+",d.get($df)="+d.get(df));
				}
			}
		}
	}*/
	
	@Test
	public void test_0003() {
		final dro.meta.Data md = new dro.meta.Data("my_meta_data"); // TODO/FIXME
		{
			this.dc.invoke(dro.meta.Data.class, md, "drop", (java.lang.Object[])null);
		}
		{
			final dro.meta.data.Field mdf = new dro.meta.data.Field("ID"); // TODO/FIXME
			mdf.set("$type", "INTEGER PRIMARY KEY AUTOINCREMENT"); // TODO/FIXME
			md.add(mdf);
		}
		{
			final dro.meta.data.Field mdf = new dro.meta.data.Field("KEY"); // TODO/FIXME
			mdf.set("$type", "TEXT NOT NULL"); // TODO/FIXME
			md.add(mdf);
		}
		{
			final dro.meta.data.Field mdf = new dro.meta.data.Field("VALUE"); // TODO/FIXME
			mdf.set("$type", "TEXT"); // TODO/FIXME
			md.add(mdf);
		}
		{
			this.dc.invoke(dro.meta.Data.class, md, "create", (java.lang.Object[])null);
		}
		final dro.Data d = new dro.Data(md);
		{
			// TODO/FIXME:
			@SuppressWarnings("unchecked")
		  /*final */java.util.Map<java.lang.Object, dro.data.Field> map = (java.util.Map<java.lang.Object, dro.data.Field>)d.get("$dro$data$Field%id");
			for (final java.lang.Object id: map.keySet()) {
				final dro.data.Field df = (dro.data.Field)map.get(id);
				final dro.meta.data.Field mdf = (dro.meta.data.Field)df.get("$dro$meta$data$Field");
				final java.lang.String name = mdf.get("$id").toString();
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == name.equals("ID")) {
				  //d.set(df, (int)1); // TODO/FIXME - knowing we're auto-generated *in the database* (not in JVM heap)
				} else if (true == name.equals("KEY")) {
					d.set(df, "key");
				} else if (true == name.equals("VALUE")) {
					d.set(df, "value");
				}
			}
			this.dc.invoke(dro.Data.class, d, "insert", (java.lang.Object[])null);
		}
		{
			this.dc.invoke(dro.meta.Data.class, md, "select", (java.lang.Object[])null);
		}
		{
			// TODO/FIXME:
			@SuppressWarnings("unchecked")
		  /*final */java.util.Map<java.lang.Object, dro.data.Field> map = (java.util.Map<java.lang.Object, dro.data.Field>)d.get("$dro$data$Field%id");
			for (final java.lang.Object id: map.keySet()) {
				final dro.data.Field df = (dro.data.Field)map.get(id);
				final dro.meta.data.Field mdf = (dro.meta.data.Field)df.get("$dro$meta$data$Field");
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == mdf.get("$id").toString().equals("ID")) {
					d.set(df, (int)1);
				} else if (true == mdf.get("$id").toString().equals("KEY")) {
				  //d.set(df, "key2");
				} else if (true == mdf.get("$id").toString().equals("VALUE")) {
					d.set(df, "value2");
				} else {
					throw new IllegalArgumentException();
				}
			}
		  //this.dc.invoke(dro.Data.class, d, "update", (java.lang.Object[])null);
		}
		{
			// TODO/FIXME:
			@SuppressWarnings("unchecked")
		  /*final */java.util.Map<java.lang.Object, dro.data.Field> map = (java.util.Map<java.lang.Object, dro.data.Field>)d.get("$dro$data$Field%id");
			for (final java.lang.Object id: map.keySet()) {
				final dro.data.Field df = (dro.data.Field)map.get(id);
				final dro.meta.data.Field mdf = (dro.meta.data.Field)df.get("$dro$meta$data$Field");
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == mdf.get("$id").toString().equals("ID")) {
					d.set(df, (int)1);
				} else if (true == mdf.get("$id").toString().equals("KEY")) {
				} else if (true == mdf.get("$id").toString().equals("VALUE")) {
				} else {
					throw new IllegalArgumentException();
				}
			}
			this.dc.invoke(dro.Data.class, d, "delete", (java.lang.Object[])null);
		}
	}
}