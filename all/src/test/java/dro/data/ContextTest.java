package dro.data;

import org.junit.Test;

public class ContextTest {
	@Test
	public void test_0000() {
	  //final dro.data.Context dc = dro.data.Context.newContext("dro.data.Context");
	  //Assert.assertNotNull(dc);
	  //dc.invoke(dc.getClass(), dc, "invoke", new java.lang.Object[]{});
	}
	
	@Test
	public void test_0001() {
	  //final dro.data.Context drc = dro.data.Context.newContext("dro.data.Context");
	  //final dro.data.Context dac = dao.data.Context.newContext("dao.data.Context"); // TODO: internally, don't necessarily auto-subscribe as we want to subscribe to a specific context's changes
	  //drc.add(dac); // TODO: get dac to subscribe to drc data events (for storing/restoring changes)
		final dro.Data d = new dro.Data();
		// But, first things first - restore all data (find out what's in the database)
	  //drc.add(d);
		d.set("key", "value");
	}
	
	@Test
	public void test_0002() {
	  //final dro.data.Context drc = dro.data.Context.newContext("dro.data.Context");
	  //final dro.data.Context dtc = dto.data.Context.newContext("dto.data.Context"); // TODO: internally, don't necessarily auto-subscribe as we want to subscribe to a specific context's changes
	  //drc.add(dtc); // TODO: get dtc to subscribe to drc data events (for transceiving changes)
		final dro.Data d = new dro.Data();
		// But, first things first - receive all data (find out what's at the other end if we're a client)
	  //drc.add(d);
		d.set("key", "value");
	}
}