## README-api-services-auto-generated.md

Pre-requisites:
```
Apache Ant (especially lib/ant-contrib-1.0b3.jar lib/ant-props-1.0Alpha.jar) (tested with 1.9.7)
Apache Maven (tested with 3.3.9)
Apache Tomcat (tested with 8.5.65)
```

Using git bash.exe (for running shell scripts)
```
\# From .../worktrees/zer0ne/release/0.0.1/bom
\# mvn clean compile -Dmaven.test.skip=true package install
\# mkdir /c/apache-tomcat-8.5.65/zer0ne
\# mkdir /c/apache-tomcat-8.5.65/zer0ne/lib
\# sh copy-for-tomcat.sh # which copies
\# from ~/itfromblighty.ca/worktrees/zer0ne/release/0.0.1
\# to /c/apache-tomcat-8.5.65 (ref. "$tomcat") $tomcat/zer0ne/ (for .war file) $tomcat/zer0ne/lib/ (for dependency jars), and not now but maybe in the future $tomcat/conf/Catalina/localhost/
\# Note: I made the following change to Tomcat server.xml:
\# /Server/Service/Engine/Host: <Context path="/api" docBase="C:\apache-tomcat-8.5.65\zer0ne\api-tomcat-0.0.1-SNAPSHOT.war"/> 
```

Using Command Prompt (my personal preference for .bat scripts)
```
@REM SET "CATALINA_HOME=C:\apache-tomcat-8.5.65"
@REM %CATALINA_HOME%\bin\catalina.bat jpda start
@REM %CATALINA_HOME%\bin\catalina.bat stop
```

There is an auto-generation algorithm in api-tomcat that generates semi-random service history for charges (not consumption - that's just what I happened to do, we can do consumption too but the original purpose for this data was charting and therefore it didn't matter if i used charged or consumption)

## IGNORE ME - Past/previous filter style (when the only units for filtering where milliseconds)

`eval $(date --date="$(date +%Y-%m-01)" +"YYYY=%Y MM=%m DD=%d" -u) && date_from=$(date -d "${YYYY}-${MM}-${DD}T00:00:00+0000" "+%s000")`
`eval $(date --date="$(date +%Y-%m-01) +1 month" +"YYYY=%Y MM=%m DD=%d" -u) && date_to=$(date -d "${YYYY}-${MM}-${DD}T00:00:00+0000" "+%s000")`
`date +%s000 \# For eye-catcher validation purposes`
`curl -X GET http://localhost:9080/api/service/health-check/line/whois/history?date-from=${date_from}\&date-to=${date_to}\&date-interval-in-ms=86400000`
```
{"whois":{"%charged":{"ms":[1619827200000,1619913600000,1620000000000,1620086400000,1620172800000,1620259200000,1620345600000,1620432000000,1620518400000,1620604800000,1620691200000,1620777600000,1620864000000,1620950400000,1621036800000,1621123200000,1621209600000,1621296000000,1621382400000,1621468800000,1621555200000,1621641600000,1621728000000,1621814400000,1621900800000,1621987200000,1622073600000,1622160000000,1622246400000,1622332800000],"units":[2.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0]},"line-id":"whois"}}
```

`eval $(date --date="today" +"YYYY=%Y MM=%m DD=%d" -u) && date_from=$(date -d "${YYYY}-${MM}-${DD}T00:00:00+0000" "+%s000")`
`eval $(date --date="tomorrow" +"YYYY=%Y MM=%m DD=%d" -u) && date_to=$(date -d "${YYYY}-${MM}-${DD}T00:00:00+0000" "+%s000")`
`curl -X GET http://localhost:9080/api/service/health-check/line/whois/history?date-from=${date_from}\&date-to=${date_to}\&date-interval-in-ms=86400000`
```
{"whois":{"%charged":{"ms":[1621036800000],"units":[3.0]},"line-id":"whois"}}
```

## PAY ATTENTION - Present/current filter style (when the only units for filtering where milliseconds)

1. Check services have been defined (automatically) properly -
`curl -X GET http://localhost:9080/api/services/health-check`

2. Check service history has been defined (automatically) for health-check whois service -
`curl -X GET http://localhost:9080/api/service/health-check/line/whois/history`
Note: if you don't specify any additional filter parameters (as we are coming on to those next/below) you will get history for one day

3. Check service history for a specified time period (first of month until seventh of month) -
Note: don't forget to back-slash/escape the ampersand when using shell as otherwise the curl command is put in the background and the date-to causes an error as a seprate command
```
curl -X GET http://localhost:9080/api/service/health-check/line/whois/history?date-from=2021-06-01\&date-to=2021-06-07
{
  "whois":{
    "%charged":{
      "1622678400594":1.0,
      "1622793600785":1.0,
      "1622822400507":1.0,
      "1623024000958":1.0,
      "1622563200432":1.0,
      "1622764800238":1.0,
      "1622534400936":1.0,
      "1622707200252":1.0,
      "1622908800630":1.0,
      "1622966400132":1.0,
      "1623110400189":1.0,
      "1623052800628":1.0,
      "1622880000581":1.0,
      "1622937600468":1.0,
      "1622592000809":1.0,
      "1622649600095":1.0,
      "1623081600861":1.0,
      "1622995200942":1.0,
      "1622851200986":1.0,
      "1622620800888":1.0,
      "1622736000670":1.0
    },
    "line-id":"whois"
  }
}
```

4a. You can use any combination of date-from or date-to date/time formats "YYYY-MM-DD" or "YYYY-MM-DD/HH:mm:ss" (without quotes).
4b. You can use any combination of date-interval-in-ms changing the "ms" for "sec", "min", "hr", "days", or "weeks" (all without quotes).
4c. You can use any combination of date-from-ms-ago and date-to-ms-ago changing the "ms" for any time unit mentioned above.

For example (one combined example) -
```
curl -X GET http://localhost:9080/api/service/health-check/line/ping/history?date-from-hr-ago=24\&date-to-min-ago=60\&date-interval-in-sec=3600
{
  "ping":{
    "%charged":{
      "ms":[1623535541492,1623539141492,1623542741492,1623546341492,1623549941492,1623553541492,1623557141492,1623560741492,1623564341492,1623567941492,1623571541492,1623575141492,1623578741492,1623582341492,1623585941492,1623589541492,1623593141492,1623596741492,1623600341492,1623603941492,1623607541492,1623611141492,1623614741492],
      "units":[60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0]
    },
    "line-id":"ping"
  }
}
```
Note: the use of date-from-*hr*-ago, date-to-*min*-ago and date-interval-in-*sec* deliberately uses factors for each value (60min = 1hr and 3600sec = 1hr) but that is just to make the concept easier to imagine.

After getting this latest output using non-millisecond date/time unit filters, I decided it would be a good/better idea to output the ms in a more human-readable format:
```
curl -X GET http://localhost:9080/api/service/health-check/line/ping/history?date-from-hr-ago=24\&date-to-min-ago=60\&date-interval-in-sec=3600
{
  "ping":{
    "%charged":{
      "ms":["2021-06-12 16:18:09","2021-06-12 17:18:09","2021-06-12 18:18:09","2021-06-12 19:18:09","2021-06-12 20:18:09","2021-06-12 21:18:09","2021-06-12 22:18:09","2021-06-12 23:18:09","2021-06-13 00:18:09","2021-06-13 01:18:09","2021-06-13 02:18:09","2021-06-13 03:18:09","2021-06-13 04:18:09","2021-06-13 05:18:09","2021-06-13 06:18:09","2021-06-13 07:18:09","2021-06-13 08:18:09","2021-06-13 09:18:09","2021-06-13 10:18:09","2021-06-13 11:18:09","2021-06-13 12:18:09","2021-06-13 13:18:09","2021-06-13 14:18:09"],
      "units":[60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0,60.0]
    },
    "line-id":"ping"
  }
}
```
Note: all output will now be in this format - I will have to go back and regenerate all the document examples accordingly..

## ---T-H-E---E-N-D--- ##