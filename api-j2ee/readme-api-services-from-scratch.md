## README-api-services-from-scratch.md

Pre-requisites:
```
Apache Ant (especially lib/ant-contrib-1.0b3.jar lib/ant-props-1.0Alpha.jar) (tested with 1.9.7)
Apache Maven (tested with 3.3.9)
Apache Tomcat (tested with 8.5.65)
```

Using git bash.exe (for running shell scripts)
```
\# From .../worktrees/zer0ne/release/0.0.1/bom
\# mvn clean compile -Dmaven.test.skip=true package install
\# mkdir /c/apache-tomcat-8.5.65/zer0ne
\# mkdir /c/apache-tomcat-8.5.65/zer0ne/lib
\# sh copy-for-tomcat.sh # which copies
\# from ~/itfromblighty.ca/worktrees/zer0ne/release/0.0.1
\# to /c/apache-tomcat-8.5.65 (ref. "$tomcat") $tomcat/zer0ne/ (for .war file) $tomcat/zer0ne/lib/ (for dependency jars), and not now but maybe in the future $tomcat/conf/Catalina/localhost/
\# Note: I made the following change to Tomcat server.xml:
\# /Server/Service/Engine/Host: <Context path="/api" docBase="C:\apache-tomcat-8.5.65\zer0ne\api-tomcat-0.0.1-SNAPSHOT.war"/> 
```

Using Command Prompt (my personal preference for .bat scripts)
```
@REM SET "CATALINA_HOME=C:\apache-tomcat-8.5.65"
@REM %CATALINA_HOME%\bin\catalina.bat jpda start
@REM %CATALINA_HOME%\bin\catalina.bat stop
```

First, create a service ("health-check") and it's service lines ("whois", "dns", "ping", "wget", and "openssl:s_client:443") -

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "health-check":{
    "service-id":"health-check","description":"health check for web/e-mail services","$state":"unavailable","%lines":{
      "whois":{
        "line-id":"whois","description":"whois for domain registration check","$state":"stopped"
      },
      "dns":{
        "line-id":"dns","description":"dns-lookup for ip resolution check","$state":"stopped"
      },
      "ping":{
        "line-id":"ping","description":"ping for ip address reachability check","$state":"stopped"
      },
      "wget":{
        "line-id":"wget","description":"HTTP GET / for web service availability check","$state":"stopped"
      },
      "openssl:s_client:443":{
        "line-id":"openssl:s_client:443","description":"openssl s_client https certificate validity check","$state":"stopped"
      }
    }
  }
}' \
http://localhost:9080/api/services
```
Note: this is done at a group level (i.e. using /api/services) so any number of services (and service lines) can be created like this.

You can check this service was created, with -
`curl -X GET http://localhost:9080/api/services`
--OR--
`curl -X GET http://localhost:9080/api/service/health-check`
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/lines`

Refer to the RESTful-API-equivalent.txt file in src/main/resources/api/j2ee for full details.

Second, create some service history (for "health-check" service, "whois" service line), and note for now, that 'date-from' and 'date-to' should use Long for now since String Date parsing is not working (yet) -

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
[
  {
    "service-id":"health-check","for-user-id":"admin","date-from":1620438433881,"date-to":1620438433883,"%lines":{
      "whois":{
        "%consumed":{
          "1620438433882":1.0
        },
        "line-id":"whois"
      }
    }
  }
]' \
http://localhost:9080/api/services/history
```
Note: this is also done at a group level (i.e. using /api/services/history) so any number of services history (and service lines history) can be created like this.

You can check this service history was created, with -
`curl -X GET http://localhost:9080/api/services/history` (#1)
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/history` (#2)
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/lines/history` (#3)

Note: You should check the output of #1 or #2 (not #3) prior to PUT-ing / GET-ing / DELETE-ing any service history or service history *lines*, because you will need to know the service-history-id

Refer to the RESTful-API-notes.txt file in src/main/resources/api/j2ee for full details.

Third, add some (more) service history (for "health-check" service, "whois" service line), and make sure you use the correct service history id at the end of the URL that follows -

```
curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "%charged":{
    "1620438433883":2.0
  }
}' \
http://localhost:9080/api/service/health-check/line/whois/history/0
```
Note: in this example, we are using service-history-id '0' (../history/0)

Check your consumed and charged:
`curl -X GET http://localhost:9080/api/service/health-check/lines/history`

Last (Fourth) for now, add some more service history, and note that while using the same service-history-id, you will change or add to the service line history for unique key+value consumption or charging.

```
curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "%consumed":{
    "1620438433884":2.0
  },
  "%charged":{
    "1620438433883":4.0
  }
}' \
http://localhost:9080/api/service/health-check/line/whois/history/0
```
Note: in this fourth/last example, we are adding more consumed units (as this is a unique date+time ms) while we are changing (just to illustate the possibility) the charged units.

Check your cumulative consumed and updated charged:
`curl -X GET http://localhost:9080/api/services/history`
--OR--
`curl -X GET http://localhost:9080/api/services/history/0`
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/history`
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/history/0`
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/lines/history`
--OR--
`curl -X GET http://localhost:9080/api/service/health-check/lines/history/0`

Get history consumed and charged for e-charting -
`curl -X GET http://localhost:9080/api/service/health-check/line/whois/history`
Should give consumed and charged number of units like this -
```
{
  "%consumed":{
    "1620438433884":2.0,
    "1620438433882":1.0
  },
  "%charged":{
    "1620438433883":4.0
  },
  "line-id":"whois"
}
```

`curl -X GET http://localhost:9080/api/service/health-check/line/whois/history?date-from=1620438433881&date-to=1620438433883`

To clear-down (without having to restart Tomcat) you can try this -
`curl -X DELETE http://localhost:9080/api/services/history`
followed by
`curl -X DELETE http://localhost:9080/api/services`

## ---T-H-E---E-N-D--- ##