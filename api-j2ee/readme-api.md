## README-extended.md

This /api/<entity-type>[/<entity-id> service can literally handle any JSON format.

The following, are simplest of simple examples:

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "one":{
    "key1": "value1"
  }
}' \
http://localhost:9080/api/my-api
```
Note: subsequent POSTs at the same URI (/api/my-api) should fail with 409=Conflict

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "key2": "value2"
}' \
http://localhost:9080/api/my-api/two
```

```
curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "key2": "value2a"
}' \
http://localhost:9080/api/my-api/two
```

`curl -X GET http://localhost:9080/api/my-api`
`curl -X GET http://localhost:9080/api/my-api/one`
`curl -X GET http://localhost:9080/api/my-api/two`

More complicated examples (but there is no server-side logic to make this useful, yet) because /api/<entity-type> is abstract and we have no abstract code -

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "whois":{
    "line-id":"whois","description":"whois for domain registration check","$state":"stopped"
  },
  "dns":{
    "line-id":"dns","description":"dns-lookup for ip resolution check","$state":"stopped"
  },
  "ping":{
    "line-id":"ping","description":"ping for ip address reachability check","$state":"stopped"
  },
  "wget":{
    "line-id":"wget","description":"HTTP GET / for web service availability check","$state":"stopped"
  },
  "openssl:s_client:443":{
    "line-id":"openssl:s_client:443","description":"openssl s_client https certificate validity check","$state":"stopped"
  }
}' \
http://localhost:9080/api/health-check
```

TODO: /api/<entity-type>[/<entity-id>]?history

## ---T-H-E---E-N-D--- ##