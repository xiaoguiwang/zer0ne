package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class GenericLogic extends Logic {
	private static java.util.Map<String, java.util.Map<String, java.util.Map<String, Object>>> entities = new java.util.HashMap<String, java.util.Map<String, java.util.Map<String, Object>>>();
	static {
		if (null == entities) {
			entities = new java.util.HashMap<String, java.util.Map<String, java.util.Map<String, Object>>>();
		}
	}
	
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/<entity>              # create (one) -- returns id
// GET   : /api/<entity>/{entity-id} # read (one)
// PUT   : /api/<entity>/{entity-id} # update (one)
// DELETE: /api/<entity>/{entity-id} # delete (one)
// DELETE: /api/<entity>/history # delete (all history of all services)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // Get rid of leading blank
		final String entity_type = uri.peek(1+1); // "/api/<entity>"
	  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
	  /*final */java.util.Map<String, Object> results = null;
		if (true == trueElseSetStatus(null != entity_type && 0 < entity_type.trim().length(), HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
			final java.util.Map<String, java.util.Map<String, Object>> entities = GenericLogic.entities.get(entity_type);
			if (true == trueElseSetStatus(null != uri && 1 <= uri.depth(), HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
				final String entity_id = 3 == uri.depth() ? null : uri.peek(1+2); // depth is absolute, peek starts at index 0 (zero)
				if (true == java.lang.Boolean.FALSE.booleanValue()) {
				} else if (true == "POST".equals(method)) {
					if (null == entity_id) {
						@SuppressWarnings("unchecked")
						final java.util.Map<String, Object> _entities = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
						if (true == trueElseSetStatus(null != _entities, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
						  /*final */java.util.Map<String, java.util.Map<String, Object>> __entities = null;
							for (final String _entity_id: _entities.keySet()) {
								if (null != entities && false == entities.containsKey(_entity_id)) {
									results = result(_entity_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
								} else {
									if (null == __entities) {
									  /*final java.util.Map<String, java.util.Map<String, Object>> */__entities = new java.util.HashMap<String, java.util.Map<String, Object>>();
									}
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Object> ___entities = (java.util.Map<String, Object>)_entities.get(_entity_id);
									__entities.put(_entity_id, ___entities);
									results = result(_entity_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
								}
								if (null != __entities) {
									GenericLogic.entities.put(entity_type, __entities);
								}
							}
							// TODO: do tiered entity-type (for now just do entity-id OR entity_type)
						}
					} else {
						@SuppressWarnings("unchecked")
						final java.util.Map<String, Object> _entity = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
						if (null == _entity) {
							results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
						} else {
							entities.put(entity_id, _entity);
							results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
						}
					}
				} else if (true == "GET".equals(method)) {
					if (true == trueElseSetStatus(null != entities, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
						if (null == entity_id) {
							for (final String key: entities.keySet()) {
								if (null == results) {
								  /*final java.util.Map<String, Object> */results = new java.util.HashMap<String, Object>();
								}
								results.put(key, entities.get(key));
							}
						  /*if (null == results || 0 == results.size()) {
								results = result(entity_type, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
							} else {
								results = result(entity_type, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
							}*/
						} else {
						  /*final java.util.Map<String, Object> */results = entities.get(entity_id);
						  /*if (null == results) {
								results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
							} else {
								results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
							}*/
						}
					}
				} else if (true == "PUT".equals(method)) {
					if (null == entity_id) {
						@SuppressWarnings("unchecked")
						final java.util.Map<String, Object> _entities = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
						if (true == trueElseSetStatus(null != _entities, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
						  /*final */java.util.Map<String, java.util.Map<String, Object>> __entities = null;
							for (final String _entity_id: _entities.keySet()) {
								if (null == entities || true == entities.containsKey(_entity_id)) {
									results = result(_entity_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
								} else {
									if (null == __entities) {
									  /*final java.util.Map<String, java.util.Map<String, Object>> */__entities = new java.util.HashMap<String, java.util.Map<String, Object>>();
									}
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Object> ___entities = (java.util.Map<String, Object>)_entities.get(_entity_id);
									__entities.put(_entity_id, ___entities);
									results = result(_entity_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
								}
								if (null != __entities) {
									GenericLogic.entities.put(entity_type, __entities);
								}
							}
							// TODO: do tiered entity-type (for now just do entity-id OR entity_type)
						}
					} else {
						@SuppressWarnings("unchecked")
						final java.util.Map<String, Object> _entity = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
						if (null == _entity) {
							results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
						} else if (false == entities.containsKey(entity_id)) {
							results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
						} else {
							entities.put(entity_id, _entity);
							results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
						}
					}
				} else if (true == "DELETE".equals(method)) {
					if (true == trueElseSetStatus(null != entities, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
						if (null == entity_id) {
							if (null != GenericLogic.entities.remove(entity_type)) {
								results = result(entity_type, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
							} else {
								results = result(entity_type, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
							}
						}
					} else {
						if (false == entities.containsKey(entity_id)) {
							results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
						} else {
							if (null != entities.remove(entity_id)) {
								results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
							} else {
								results = result(entity_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
							}
						}
					}
				} else {
				  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
					throw new IllegalArgumentException();
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
		writeResultsAndSetStatus(resp, results, httpStatuses);
	}
}