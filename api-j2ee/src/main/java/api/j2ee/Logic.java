package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Properties;
import dto.util.Stream;

abstract public class Logic {
	protected static boolean trueElseSetStatus(final boolean truth, final int httpStatus, final java.util.Set<Integer> httpStatuses) {
		if (true == truth) {
		} else {
			if (null != httpStatuses) {
				httpStatuses.add(new Integer(httpStatus));
			} else {
				throw new IllegalStateException();
			}
		}
		return truth;
	}
	protected static Integer httpStatus(final int httpStatus, final java.util.Set<Integer> httpStatuses) {
		if (null != httpStatuses) {
			httpStatuses.add(new Integer(httpStatus));
		} else {
			throw new IllegalStateException();
		}
		return httpStatus;
	}
	protected static java.util.Map<String, Object> result(final String key, final Object value, /*final */java.util.Map<String, Object> results) {
		if (null == results) {
		  /*final java.util.Map<String, Object> */results = new dto.util.HashMap<>();
		}
		results.put(key, value);
		return results;
	}
	protected static <T> java.util.Map<T, Object> result(final T id, final String key, final Object value, /*final */java.util.Map<T, Object> results) {
		if (null == results) {
		  /*final java.util.Map<T, Object> */results = new dto.util.HashMap<>();
		}
		if (null == results.get(id)) {
			results.put(id, new dto.util.HashMap<T, Object>(){
				private static final long serialVersionUID = 0L;
			});
		}
		@SuppressWarnings("unchecked")
		final java.util.Map<Object, Object> result = (java.util.Map<Object, Object>)results.get(id);
		if (null != result) {
			result.put(key, value);
		}
		return results;
	}
	private static void setStatus(final HttpServletResponse resp, final java.util.Set<Integer> httpStatuses) {
		if (null == httpStatuses || 0 == httpStatuses.size()) {
			resp.setStatus(HttpStatus.SC_OK);
		} else if (1 == httpStatuses.size()) {
			resp.setStatus(httpStatuses.toArray(new Integer[1])[0]);
		} else {
			resp.setStatus(HttpStatus.SC_MULTI_STATUS);
		}
	}
	protected static void writeResultsAndSetStatus(final HttpServletResponse resp, final java.util.Map<?, ?> results, final Stream.interface_Callback sic, final java.util.Set<Integer> httpStatuses) throws IOException {
		if (null != resp) {
			if (null != results) {
				resp.getWriter().append(dto.Adapter.getStreamFromObject(results, sic));//.flush();
			}
		}
		setStatus(resp, httpStatuses);
	}
	protected static void writeResultsAndSetStatus(final HttpServletResponse resp, final java.util.Map<?, ?> results, final java.util.Set<Integer> httpStatuses) throws IOException {
		writeResultsAndSetStatus(resp, results, (Stream.interface_Callback)null, httpStatuses);
	}
	protected static void writeResultsAndSetStatus(final HttpServletResponse resp, final java.util.List<?> results, final Stream.interface_Callback sic, final java.util.Set<Integer> httpStatuses) throws IOException {
		if (null != resp) {
			resp.getWriter().append(dto.Adapter.getStreamFromObject(results, sic));//.flush();
		}
		setStatus(resp, httpStatuses);
	}
	protected static void writeResultsAndSetStatus(final HttpServletResponse resp, final java.util.List<?> results, final java.util.Set<Integer> httpStatuses) throws IOException {
		writeResultsAndSetStatus(resp, results, (Stream.interface_Callback)null, httpStatuses);
	}
	
	Properties properties;// = null;
	
	{
	  //properties = null;
	}
	
	public Logic() {
	}
	public Logic(final Properties properties) {
		this.properties = properties;
	}
	
	public Logic properties(final Properties properties) {
		this.properties = properties;
		return this;
	}
	public Logic property(final String key, final Object value) {
		if (null == this.properties) {
			this.properties = new Properties();
		}
		this.properties.put(key, value);
		return this;
	}
	public Object property(final String key) {
	  /*final */Object value = null;
		if (null != this.properties) {
			value = this.properties.get(key);
		}
		return value;
	}
	
	public abstract void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException;
}