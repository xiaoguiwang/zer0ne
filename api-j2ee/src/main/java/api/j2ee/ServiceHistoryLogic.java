package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class ServiceHistoryLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/service/history                                   # create -- returns id
// GET   : /api/service/history                                   # read (all history for all services)
// GET   : /api/service/{service-id}/history                      # read (all history for one service)
// GET   : /api/service/{service-id}/history/{service-history-id} # read (one)
// PUT   : /api/service/{service-id}/history/{service-history-id} # update (one)
// DELETE: /api/service/{service-id}/history/{service-history-id} # delete (one)
// DELETE: /api/service/history # delete (all history of all services)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // [, api, service, {service-id}, history]
		if (true == uri.contains("service")) {
		  /*final */java.lang.String service_id = null;
			if (5 == uri.depth()) {
			  /*final String */service_id = uri.peek(1+2);
			}
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
						// service/{service-id}
						throw new IllegalStateException(); // Should never get here
					} else {
					  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
						if (true == java.lang.Boolean.FALSE.booleanValue()) {
						} else if (true == "POST".equals(method)) {
							// service/{service-id}/history
						  /*final */java.util.Map<Integer, Object> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) { // TODO: DRO aliasing, as-in in future (we won't guarantee not-null)
								@SuppressWarnings("unchecked")
								final java.util.List<java.util.Map<String, Object>> /*service*/_history_list = (java.util.List<java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
								if (true == trueElseSetStatus(null != /*service*/_history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < /*service*/_history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									  /*final */int index = 0;
										for (final java.util.Map<String, Object> /*service*/_history: /*services*/_history_list) {
											final String __service_id = null != service_id ? service_id : (String)/*service*/_history.get("service-id"); // TODO: what if /*service*/_history is null?
											final String for_user_id = (String)/*service*/_history.get("for-user-id");
											final java.util.Map<String, Object> /*service_*/history = Services.findHistoryByServiceIdAndUserId(__service_id, for_user_id);
											if (null == /*service_*/history) {
												final java.lang.Integer /*service*/_history_id = Services.createHistory(/*service*/_history);
												if (null != /*service*/_history_id) {
													results = result(index, "service-history-id", /*service*/_history_id, results);
													results = result(index, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_BAD_REQUEST, httpStatuses), results);
												}
											} else {
												results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
											}
											index++;
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "GET".equals(method)) {
							// service/{service-id}/history/{service-history-id}
						  /*final */java.util.List<java.util.Map<String, Object>> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (true == trueElseSetStatus(0 < Services.history.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									final java.lang.String /*service_*/history_id = uri.depth() <= 5 ? null : uri.peek(1+2+2);
								  /*final */Integer /*service*/_history_id = null;
									if (null != /*service_*/history_id) {
										try {
										  /*final Integer *//*service*/_history_id = new Integer(/*service_*/history_id);
										} catch (final NumberFormatException e) {
											// Silently ignore..
										}
									}
									// TODO: all history for a service (across multiple service-history-ids)
									if (null == /*service*/_history_id) {
									  /*final java.util.List<java.util.Map<String, Object>> */results = Services.readHistory(service_id);
										if (true == trueElseSetStatus(null != results, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
											trueElseSetStatus(0 < results.size(), HttpStatus.SC_NO_CONTENT, httpStatuses);
										}
									} else {
										final java.util.Map<String, Object> /*service*/_history = Services.readHistory(/*service*/_history_id);
										if (true == trueElseSetStatus(null != /*service*/_history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
											if (null == results) {
											  /*final java.util.List<java.util.Map<String, Object>> */results = new java.util.ArrayList<>();
											}
											results.add(/*service*/_history);
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "PUT".equals(method)) {
						  /*final */java.util.Map<Integer, Object> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.List<java.util.Map<String, Object>> /*service*/_history_list = (java.util.List<java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
								if (true == trueElseSetStatus(null != /*services_*/_history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < /*services_*/_history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									  /*final */int index = 0;
										for (final java.util.Map<String, Object> /*service_*/history: /*services_*/_history_list) {
											final java.lang.Integer /*service_*/history_id = (Integer)/*service_*/history.get("service-history-id");
											if (null != /*service_*/history_id) {
												results = result(index, "service-history-id", /*service_*/history_id, results);
												if (null == Services.readHistory(/*service_*/history_id)) {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
												} else if (true == trueElseSetStatus(
													(true == service_id.equals(/*service_*/history.get("service-id")))
												  // &&
												  //(null == /*service_*/history.get("for-user-id") || true == /*service_*/history.get("for-user-id").equals(/*service*/_history.get("for-user-id")))
													,
													HttpStatus.SC_BAD_REQUEST, httpStatuses
												)) {
												  //final String service_id = (String)/*service_*/history.get("service-id");
													results = result(/*service_*/history_id, "service-id", service_id, results);
													final String for_user_id = (String)/*service_*/history.get("for-user-id");
													results = result(/*service_*/history_id, "for-user-id", for_user_id, results);
													final java.util.Map<String, Object> service_history = Services.findHistoryByServiceIdAndUserId(service_id, for_user_id);
												  /*final */java.lang.Integer /*service*/_history_id = null;
													if (null != service_history) {
													  /*final java.lang.Integer *//*service*/_history_id = (Integer)service_history.get("service-history-id");
													}
													if (null != /*service*/_history_id && true == /*service*/_history_id.equals(/*service_*/history_id)) {
														if (true == Services.updateHistory(/*service_*/history_id, /*service_*/history)) {
															results = result(index, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
														} else {
															results = result(index, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
														}
													} else {
														results = result(/*service_*/history_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
													}
												}
											} else {
												results = result(/*service_*/history_id, "http-status", httpStatus(HttpStatus.SC_BAD_REQUEST, httpStatuses), results);
											}
											index++;
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "DELETE".equals(method)) { // DELETE /api/service/{service-id}/history
						  /*final */java.util.Map<Integer, Object> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final java.lang.String service_history_id = uri.pop();
							  /*final */Integer /*service*/_history_id = null;
								try {
								  /*final Integer *//*service*/_history_id = new Integer(service_history_id);
								} catch (final NumberFormatException e) {
									// Silently ignore..
								}
								if (null == /*service*/_history_id) {
									@SuppressWarnings("unchecked")
									final java.util.List<Integer> /*service*/_history_ids = (java.util.List<Integer>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
									if (true == trueElseSetStatus(null != /*service*/_history_ids, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*service*/_history_ids.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										  /*final */int index = 0;
											for (int i = 0; i < /*service*/_history_ids.size(); i++) {
											  /*final Integer *//*service*/_history_id = /*service*/_history_ids.get(i);
												results = result(index, "service-history-id", /*service*/_history_id, results);
												final java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)Services.readHistory(/*service*/_history_id);
												if (null == /*service_*/history) {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
												} else if (true == trueElseSetStatus(
													(true == service_id.equals(/*service_*/history.get("service-id")))
												  // &&
												  //(null == /*service_*/history.get("for-user-id") || true == /*service_*/history.get("for-user-id").equals(/*service*/_history.get("for-user-id")))
													,
													HttpStatus.SC_BAD_REQUEST, httpStatuses
												)) {
													results = result(index, "service-id", (String)/*service_*/history.get("service-id"), results);
													results = result(index, "for-user-id", (String)/*service_*/history.get("for-user-id"), results);
													if (true == Services.deleteHistory(/*service*/_history_id)) {
														results = result(index, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
													} else {
														results = result(index, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
													}
												}
												index++;
											}
										}
									}
								} else {
									final java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)Services.readHistory(/*service*/_history_id);
									if (null == /*service_*/history) {
										results = result(/*service*/_history_id, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
									} else if (true == trueElseSetStatus(
										(true == service_id.equals(/*service_*/history.get("service-id")))
									  // &&
									  //(null == /*service_*/history.get("for-user-id") || true == /*service_*/history.get("for-user-id").equals(/*service*/_history.get("for-user-id")))
										,
										HttpStatus.SC_BAD_REQUEST, httpStatuses
									)) {
										results = result(/*service*/_history_id, "service-id", (String)/*service_*/history.get("service-id"), results);
										results = result(/*service*/_history_id, "for-user-id", (String)/*service_*/history.get("for-user-id"), results);
										if (true == Services.deleteHistory(/*service*/_history_id)) {
											results = result(/*service*/_history_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
										} else {
											results = result(/*service*/_history_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else {
						  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
							throw new IllegalArgumentException();
						}
					}
				} else {
					if (false == uri.contains("history")) {
						// service/{service-id}/line/{line-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/line/{line-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				}
			} else {
				if (false == uri.contains("history")) {
					// service/{service-id}/lines
					throw new IllegalStateException(); // Should never get here
				} else {
					// service/{service-id}/lines/history
					throw new IllegalStateException(); // Should never get here
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}