package api.j2ee;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import api.j2ee.transform.ServiceLineHistoryFilter;
import api.j2ee.transform.ServiceLineHistoryFilterAggregate;
import dro.util.Stack;
import dto.util.Stream;

// Date filters to include/support:

// BEGINNING   ENDING
// date-from   date-to

// date-interval-in-ms|milliseconds // Long form not (yet) supported
// date-interval-in-sec|seconds // Long form not (yet) supported
// date-interval-in-min|minutes // Long form not (yet) supported
// date-interval-in-hr|hours // Long form not (yet) supported
// date-interval-in-days
// date-interval-in-weeks
// date-interval-in-months // Not (yet) supported
// date-interval-in-years // Not (yet) supported

// date-from-ms-ago date-to-ms-ago
// date-from-sec-ago date-to-sec-ago
// date-from-min-ago date-to-min-ago
// date-from-hr-ago date-to-hr-ago
// date-from-days-ago date-to-days-ago
// date-from-weeks-ago date-to-weeks-ago
// date-from-months-ago date-to-months-ago
// date-from-years-ago date-to-years-ago

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class ServiceLineHistoryLogic extends Logic {
	private java.util.Map<Integer, Object> postLogic(final String service_id, final String /*service_*/line_id, final int index, final java.util.Map<String, Object> /*service*/_history, /*final */java.util.Map<Integer, Object> results, /*final */java.util.Set<Integer> httpStatuses) {
		if (true == trueElseSetStatus(null != /*service*/_history, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
	/* +- */Integer service_history_id = (Integer)/*service*/_history.get("service-history-id"); // Ok to be NULL or not-NULL
	/* |  */final String service_id_ = (String)/*service*/_history.get("service-id");
	/* |  */if (true == service_id.equals(service_id_)) {
	/* |  */	final String for_user_id_ = (String)/*service*/_history.get("for-user-id");
	/* +----- */final java.util.Map<String, Object> /*service_*/history = Services.findHistoryByServiceIdAndUserId(service_id_, for_user_id_); // Ok to be NULL or not-NULL
	/* |  */	final java.util.List<String> _service_line_ids = Services.getHistoryLineIds(/*service_*/_history);
	/* |  */	if (null != _service_line_ids) {
	/* |  */	  /*final */java.util.Map<Integer, Object> _results = null;
	/* |  */		// Note: Eclipse is too dumb to work out that the subsequent else block is NOT dead code! )-:
	/* |  */		if (null == results || null == results.get(index)) {
	/* |  */		  /*final java.util.Map<Integer, Object> */_results = new dto.util.HashMap<Integer, Object>(){
	/* |  */				private static final long serialVersionUID = 0L;
	/* |  */			};
	/* |  */			results = result(index, "%lines", _results, results);
	/* |  */		} else {
	/* |  */			@SuppressWarnings("unchecked")
	/* |  */			final java.util.Map<Integer, Object> __results = (java.util.Map<Integer, Object>)results.get(index);
	/* |  */		  /*final java.util.Map<Integer, Object> */_results = __results;
	/* |  */		}
	/* |  */	  /*final */int jndex = 0;
	/* |  */		for (final String _service_line_id: _service_line_ids) {
	/* |  */			if (null == /*service_*/line_id || true == /*service_*/line_id.equals(_service_line_id)) {
	/* |  */				if (null != _service_line_id) { 
	/* |  */					_results = result(jndex, "line-id", _service_line_id, _results);
	/* |  */				  /*final */java.util.Map<String, Object> /*service_*/history_line = null;
	/* |  */					if (null != /*service_*/history) {
	/* |  */					  /*final java.util.Map<String, Object> *//*service_*/history_line = Services.readLineHistory(/*service_*/history, _service_line_id);
	/* |  */					}
	/* |  */					final java.util.Map<String, Object> /*service*/_history_line = Services.readLineHistory(/*service*/_history, _service_line_id);
	/* +--------------------> */if (null == /*service_*/history_line || (null == service_history_id && null != /*service*/_history_line)) {
									if (null != (service_history_id = Services.createLineHistory(service_id_, for_user_id_, /*service_*/line_id, /*service*/_history_line))) {
										results = result(index, "service-history-id", service_history_id, results);
										results = result(index, "service-id", service_id_, results);
										results = result(index, "for-user-id", for_user_id_, results);
										_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
									} else {
										_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
									}
								} else {
									_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), _results);
								}
							} else {
								_results = result(jndex, "line-id", "{null}", _results);
								_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
							}
						}
						jndex++;
					}
				} else {
					results = result(index, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
				}
			} else {
				results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
			}
		}
		return results;
	}
  //private java.util.Map<Integer, Object> getLogic(final String service_id, final String /*service_*/line_id, final int index, final java.util.Map<String, Object> /*service_*/history, /*final */java.util.Map<Integer, Object> results, /*final */java.util.Set<Integer> httpStatuses) {
	private java.util.Map<String, Object> getLogic(final String service_id, final String /*service_*/line_id, final int index, final java.util.Map<String, Object> /*service_*/history, final java.util.Date date_from, final java.util.Date date_to, /*final */java.util.Map<String, Object> results, /*final */java.util.Set<Integer> httpStatuses) {
		if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
	/* +- */final Integer service_history_id = (Integer)/*service_*/history.get("service-history-id");
	/* |  */final String service_id_ = (String)/*service_*/history.get("service-id"); // TODO: check these match the uri
	/* |  */if (true == service_id.equals(service_id_)) {
	/* |  */  //final String for_user_id_ = (String)/*service_*/history.get("for-user-id"); // TODO: be able to filter on for-user-id (instead of service-id and line-id)
	/* |  */	final java.util.List<String> service_line_ids = Services.getHistoryLineIds(/*service_*/history);
	/* |  */	// TODO: this is STILL not quite what I want, since I might have asked for a service line that doesn't exist
	/* |  */	if (null != service_line_ids) {
	/* |  */	///*final */java.util.Map<Integer, Object> _results = null;
	/* |  */		// Note: Eclipse is too dumb to work out that the subsequent else block is NOT dead code! )-:
	/* |  */	//if (null == results || null == results.get(index)) {
	/* |  */		///*final java.util.Map<Integer, Object> */_results = new dto.util.HashMap<Integer, Object>(){
	/* |  */		//		private static final long serialVersionUID = 0L;
	/* |  */		//	};
	/* |  */		  //results = result(index, "service-history-id", service_history_id, results);
	/* |  */		  //results = result(index, "service-id", service_id_, results);
	/* |  */		  //results = result(index, "for-user-id", for_user_id_, results);
	/* |  */		  //results = result(index, "%lines", _results, results);
	/* |  */	  //} else {
	/* |  */		  //@SuppressWarnings("unchecked")
	/* |  */		  //final java.util.Map<Integer, Object> __results = (java.util.Map<Integer, Object>)results.get(index);
	/* |  */		///*final java.util.Map<Integer, Object> */_results = __results;
	/* |  */	  //}
	/* |  */	///*final */int jndex = 0;
	/* |  */		for (final String service_line_id: service_line_ids) {
	/* |  */			if (null == /*service_*/line_id || true == /*service_*/line_id.equals(service_line_id)) {
	/* |  */				if (null != service_line_id) { 
	/* |  */				  //_results = result(jndex, "line-id", service_line_id, _results);
	/* |  */				  /*final */java.util.Map<String, Object> /*service_*/history_line = Services.readLineHistory(/*service_*/history, service_line_id);
	/* +--------------------> */if (null != /*service_*/history_line && null != service_history_id) {
								  /*final */boolean inDateRange = false; // TODO: make this the service_history that has *something* in date range..
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Float> consumed = (java.util.Map<String, Float>)/*service_*/history_line.get("%consumed");
									if (null != consumed) {
										for (final Object __date_consumed: consumed.keySet()) {
										  /*final */Long ms = null;
											if (__date_consumed instanceof String) {
												ms = new Long((String)__date_consumed);
											} else if (__date_consumed instanceof Long) {
												ms = (Long)__date_consumed;
											}
											if (true == dto.util.Date.inDateRange(date_from, ms, date_to, ms)) {
												inDateRange = true;
												break;
											}
										}
									}
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Float> charged = (java.util.Map<String, Float>)/*service_*/history_line.get("%charged");
									if (null != charged) {
										for (final Object __date_charged: charged.keySet()) {
										  /*final */Long ms = null;
											if (__date_charged instanceof String) {
												ms = new Long((String)__date_charged);
											} else if (__date_charged instanceof Long) {
												ms = (Long)__date_charged;
											}
											if (true == dto.util.Date.inDateRange(date_from, ms, date_to, ms)) {
												inDateRange = true;
												break;
											}
										}
									}
									if (true == inDateRange) {
									  //_results = result(jndex, service_line_id, /*service_*/history_line, _results);
										results = result(service_line_id, /*service_*/history_line, results);
									}
								} else if (null != /*service_*/line_id) {
								}
							} else {
							  //_results = result(jndex, "line-id", "{null}", _results);
							}
						}
					  //jndex++;
					}
				} else {
				  //results = result(index, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
results = result("http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
				}
			} else {
			  //results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
results = result("http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
			}
		}
		return results;
	}
	private java.util.Map<Integer, Object> putLogic(final String service_id, final String /*service_*/line_id, final int index, final java.util.Map<String, Object> /*service*/_history, /*final */java.util.Map<Integer, Object> results, /*final */java.util.Set<Integer> httpStatuses) {
		if (true == trueElseSetStatus(null != /*service*/_history, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
	/* +- */Integer service_history_id = (Integer)/*service*/_history.get("service-history-id"); // Ok to be NULL or not-NULL
	/* |  */final String service_id_ = (String)/*service*/_history.get("service-id");
	/* |  */if (true == service_id.equals(service_id_)) {
	/* |  */	final String for_user_id_ = (String)/*service*/_history.get("for-user-id");
	/* +----- */final java.util.Map<String, Object> /*service_*/history = Services.findHistoryByServiceIdAndUserId(service_id_, for_user_id_); // Ok to be NULL or not-NULL
	/* |  */	final java.util.List<String> _service_line_ids = Services.getHistoryLineIds(/*service_*/_history);
	/* |  */	if (null != _service_line_ids) {
	/* |  */	  /*final */java.util.Map<Integer, Object> _results = null;
	/* |  */		// Note: Eclipse is too dumb to work out that the subsequent else block is NOT dead code! )-:
	/* |  */		if (null == results || null == results.get(index)) {
	/* |  */		  /*final java.util.Map<Integer, Object> */_results = new dto.util.HashMap<Integer, Object>(){
	/* |  */				private static final long serialVersionUID = 0L;
	/* |  */			};
	/* |  */			results = result(index, "service-id", service_id_, results);
	/* |  */			results = result(index, "for-user-id", for_user_id_, results);
	/* |  */			results = result(index, "%lines", _results, results);
	/* |  */		} else {
	/* |  */			@SuppressWarnings("unchecked")
	/* |  */			final java.util.Map<Integer, Object> __results = (java.util.Map<Integer, Object>)results.get(index);
	/* |  */		  /*final java.util.Map<Integer, Object> */_results = __results;
	/* |  */		}
	/* |  */	  /*final */int jndex = 0;
	/* |  */		for (final String _service_line_id: _service_line_ids) {
	/* |  */			if (null == /*service_*/line_id || true == /*service_*/line_id.equals(_service_line_id)) {
	/* |  */				if (null != _service_line_id) { 
	/* |  */					_results = result(jndex, "line-id", _service_line_id, _results);
	/* |  */				  /*final */java.util.Map<String, Object> /*service_*/history_line = null;
	/* |  */					if (null != /*service_*/history) {
	/* |  */					  /*final java.util.Map<String, Object> *//*service_*/history_line = Services.readLineHistory(/*service_*/history, _service_line_id);
	/* |  */					}
	/* |  */					final java.util.Map<String, Object> /*service*/_history_line = Services.readLineHistory(/*service*/_history, _service_line_id);
	/* +--------------------> */if (null != /*service*/_history_line && null != service_history_id) {
									if (null == /*service_*/history_line) {
										if (null != (service_history_id = Services.createLineHistory(service_id_, for_user_id_, /*service_*/line_id, /*service*/_history_line))) {
											results = result(index, "service-history-id", service_history_id, results);
											_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
										} else {
											_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
										}
									} else {
										if (true == Services.updateLineHistory(service_history_id, /*service_*/line_id, /*service*/_history_line)) {
											results = result(index, "service-history-id", service_history_id, results);
											_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
										} else {
											_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
										}
									}
								} else {
									_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), _results);
								}
							} else {
								_results = result(jndex, "line-id", "{null}", _results);
								_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), _results);
							}
						}
						jndex++;
					}
				} else {
					results = result(index, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
				}
			} else {
				results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
			}
		}
		return results;
	}
	private java.util.Map<Integer, Object> deleteLogic(final String service_id, final String /*service_*/line_id, final int index, final java.util.Map<String, Object> /*service_*/history, /*final */java.util.Map<Integer, Object> results, /*final */java.util.Set<Integer> httpStatuses) {
		if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
	/* +- */final Integer service_history_id = (Integer)/*service_*/history.get("service-history-id");
	/* |  */final String service_id_ = (String)/*service_*/history.get("service-id"); // TODO: check these match the uri
	/* |  */if (true == service_id.equals(service_id_)) {
	/* |  */  //final String for_user_id_ = (String)/*service_*/history.get("for-user-id"); // TODO: be able to filter on for-user-id (instead of service-id and line-id)
	/* |  */	final java.util.List<String> service_line_ids = Services.getHistoryLineIds(/*service_*/history);
	/* |  */	if (null != service_line_ids) {
	/* |  */	  /*final */java.util.Map<Integer, Object> _results = null;
	/* |  */		// Note: Eclipse is too dumb to work out that the subsequent else block is NOT dead code! )-:
	/* |  */		if (null == results || null == results.get(index)) {
	/* |  */		  /*final java.util.Map<Integer, Object> */_results = new dto.util.HashMap<Integer, Object>(){
	/* |  */				private static final long serialVersionUID = 0L;
	/* |  */			};
	/* |  */			results = result(index, "service-history-id", service_history_id, results);
	/* |  */			results = result(index, "%lines", _results, results);
	/* |  */		} else {
	/* |  */			@SuppressWarnings("unchecked")
	/* |  */			final java.util.Map<Integer, Object> __results = (java.util.Map<Integer, Object>)results.get(index);
	/* |  */		  /*final java.util.Map<Integer, Object> */_results = __results;
	/* |  */		}
	/* |  */	  /*final */int jndex = 0;
	/* |  */		for (final String _service_line_id: service_line_ids) {
	/* |  */			if (null == /*service_*/line_id || true == /*service_*/line_id.equals(_service_line_id)) {
	/* |  */				if (null != _service_line_id) { 
	/* |  */					_results = result(jndex, "line-id", _service_line_id, _results);
	/* +--------------------> */if (true == Services.deleteLineHistory(service_history_id, _service_line_id)) {
									_results = result(jndex, "service-id", service_history_id, _results);
									_results = result(jndex, "for-user-id", service_history_id, _results);
									_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
								} else {
									_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
								}
							} else {
								_results = result(jndex, "line-id", "{null}", _results);
								_results = result(jndex, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
							}
						}
						jndex++;
					}
				} else {
					results = result(index, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
				}
			} else {
				results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
			}
		}
		return results;
	}
	
	private static final SimpleDateFormat sdf10 = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat sdf19 = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");
	
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/service/{service-id}/line/{line-id}/history # create -- returns id
// GET   : /api/service/lines/history                       # read (all lines history for all services)
// GET   : /api/service/{service-id}/lines/history          # read (all lines history for one service)
// GET   : /api/service/{service-id}/line/{line-id}/history # read (one service line all history)
// GET   : /api/service/{service-id}/line/{line-id}/history{service-history-id} # read (one)
// PUT   : /api/service/{service-id}/line/{line-id}/history # update (one)
// DELETE: /api/service/{service-id}/line/{line-id}/history # delete (one)
// DELETE: /api/service/{service-id}/lines/history          # delete (all lines history of one service)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // [, api, service, {service-id}, line(s)/{line-id}, history]
		
		final Map<String, String> qs = ServletTool.getQueryStringAsKeyValueMap(req);
	  /*final */java.util.Date date_from = null;
		if (true == qs.containsKey("date-from")) {
			final String __date_from = qs.get("date-from");
			if (0 < __date_from.length()) {
				try {
					if (__date_from.length() <= 10) {
						date_from = sdf10.parse(__date_from);
					} else {
						date_from = sdf19.parse(__date_from);
					}
				} catch (final ParseException e) {
					throw new RuntimeException(e);
				}
				if (null == date_from) {
					final java.util.Calendar c = Calendar.getInstance();
					c.setTimeInMillis(new Long(__date_from).longValue());
					date_from = c.getTime();
				}
			}
		} else {
			final java.util.Calendar c = Calendar.getInstance();
			c.setTimeZone(TimeZone.getTimeZone("UTC"));
		  //final int y  = c.get(Calendar.YEAR);
		  //final int m = c.get(Calendar.MONTH);
		  //final int dom = c.get(Calendar.DAY_OF_MONTH);
		  //c.clear();
		  //c.set(y, m, dom);
		  /*final */Long ms = null;
			if (true == qs.containsKey("date-from-ms-ago")) {
			  /*final Long */ms = new Long(qs.get("date-from-ms-ago")).longValue();
			} else if (true == qs.containsKey("date-from-sec-ago")) {
			  /*final Long */ms = new Long(qs.get("date-from-sec-ago")).longValue()*1000L;
			} else if (true == qs.containsKey("date-from-min-ago")) {
			  /*final Long */ms = new Long(qs.get("date-from-min-ago")).longValue()*1000L*60L;
			} else if (true == qs.containsKey("date-from-hr-ago")) {
			  /*final Long */ms = new Long(qs.get("date-from-hr-ago")).longValue()*1000L*60L*60L;
			} else if (true == qs.containsKey("date-from-days-ago")) {
			  /*final Long */ms = new Long(qs.get("date-from-days-ago")).longValue()*1000L*60L*60L*24L;
			} else if (true == qs.containsKey("date-from-weeks-ago")) {
			  /*final Long */ms = new Long(qs.get("date-from-weeks-ago")).longValue()*1000L*60L*60L*24L*7L;
			} else if (true == qs.containsKey("date-from-months-ago")) {
				throw new UnsupportedOperationException(); // TODO: go by nearest calendar month start/end etc.
			} else if (true == qs.containsKey("date-from-years-ago")) {
				throw new UnsupportedOperationException(); // TODO: go by nearest calendar month start/end etc.
			} else {
				ms = new Long(86400L*1000L);
			}
			c.add(Calendar.MILLISECOND, -(ms.intValue()));
			date_from = c.getTime();
		}
	  /*final */java.util.Date date_to = null;
		if (true == qs.containsKey("date-to")) {
			final String __date_to = qs.get("date-to");
			if (0 < __date_to.length()) {
				try {
					if (__date_to.length() <= 10) {
						date_to = sdf10.parse(__date_to);
					} else {
						date_to = sdf19.parse(__date_to);
					}
				} catch (final ParseException e) {
					throw new RuntimeException(e);
				}
				if (null == date_to) {
					final java.util.Calendar c = Calendar.getInstance();
					c.setTimeInMillis(new Long(__date_to).longValue());
					date_to = c.getTime();
				}
			}
		} else {
			final java.util.Calendar c = Calendar.getInstance();
			c.setTimeZone(TimeZone.getTimeZone("UTC"));
		  //final int y  = c.get(Calendar.YEAR);
		  //final int m = c.get(Calendar.MONTH);
		  //final int dom = c.get(Calendar.DAY_OF_MONTH);
		  //c.clear();
		  //c.set(y, m, dom);
		  /*final */Long ms = null;
			if (true == qs.containsKey("date-to-ms-ago")) {
			  /*final Long */ms = new Long(qs.get("date-to-ms-ago")).longValue();
			} else if (true == qs.containsKey("date-to-sec-ago")) {
			  /*final Long */ms = new Long(qs.get("date-to-sec-ago")).longValue()*1000L;
			} else if (true == qs.containsKey("date-to-min-ago")) {
			  /*final Long */ms = new Long(qs.get("date-to-min-ago")).longValue()*1000L*60L;
			} else if (true == qs.containsKey("date-to-hr-ago")) {
			  /*final Long */ms = new Long(qs.get("date-to-hr-ago")).longValue()*1000L*60L*60L;
			} else if (true == qs.containsKey("date-to-days-ago")) {
			  /*final Long */ms = new Long(qs.get("date-to-days-ago")).longValue()*1000L*60L*60L*24L;
			} else if (true == qs.containsKey("date-to-weeks-ago")) {
			  /*final Long */ms = new Long(qs.get("date-to-weeks-ago")).longValue()*1000L*60L*60L*24L*7L;
			} else if (true == qs.containsKey("date-to-months-ago")) {
				throw new UnsupportedOperationException(); // TODO: go by nearest calendar month start/end etc.
			} else if (true == qs.containsKey("date-to-years-ago")) {
				throw new UnsupportedOperationException(); // TODO: go by nearest calendar month start/end etc.
			} else {
				ms = new Long(1L);
			}
			c.add(Calendar.MILLISECOND, -(ms.intValue()));
			date_to = c.getTime();
		}
		
		if (true == uri.contains("service")) {
		  /*final */String service_id = uri.peek(1+2);
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
						// service/{service-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				} else {
				  /*final */String /*service_*/line_id = uri.peek(1+2+2);
					if (false == uri.contains("history")) {
						// service/{service-id}/line/{line-id}
						throw new IllegalStateException(); // Should never get here
					} else {
					  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
					  /*final */Integer /*service_*/history_id = null;
					  /*final */String /*service*/_history_id = null;
						if (8 == uri.depth()) {
						  /*final String *//*service*/_history_id = uri.peek(1+2+2+2);
						}
						try {
						  /*final Integer *//*service_*/history_id = new Integer(/*service*/_history_id);
						} catch (final NumberFormatException e) {
							// Silently ignore..
						}
						if (true == java.lang.Boolean.FALSE.booleanValue()) {
						} else if (true == "POST".equals(method)) { // POST /api/service/{service-id}/line/{line-id}/history // TODO: Additive (in parts)
							// service/{service-id}/line/{line-id}/history
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (null == /*service_*/history_id) {
								  /*final */java.util.Map<Integer, Object> results = null;
									@SuppressWarnings("unchecked")
									final java.util.List<java.util.Map<String, Object>> /*service*/_history_list = (java.util.List<java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
									if (true == trueElseSetStatus(null != /*service*/_history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*service*/_history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										  /*final */int index = 0;
											for (final java.util.Map<String, Object> /*service*/_history: /*service*/_history_list) {
												results = postLogic(service_id, /*service_*/line_id, index, /*service*/_history, results, httpStatuses);
												index++;
											}
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								} else {
								  /*final */java.util.Map<String, Object> results = null;
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Object> /*service*/_history_line = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
									if (true == trueElseSetStatus(true == "line-id".equals(/*service*/_history_line.get("line-id")), HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
										if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
											final java.util.Map<String, Object> /*service_*/history_line = Services.readLineHistory(/*service_*/history, /*service_*/line_id);
											if (true == trueElseSetStatus(null == /*service_*/history_line, HttpStatus.SC_CONFLICT, httpStatuses)) {
											  //final String service_id = (String)/*service_*/history.get("service-id");
												final String for_user_id = (String)/*service_*/history.get("for-user-id");
											  /*final Integer *//*service_*/history_id = Services.createLineHistory(service_id, for_user_id, /*service_*/line_id, /*service*/_history_line);
												results = result("service-history-id", /*service_*/history_id, results);
												results = result("service-id", service_id, results);
												results = result("for-user-id", for_user_id, results);
												results = result("line-id", /*service_*/line_id, results);
												if (null != /*service_*/history_id) {
													results = result("http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result("http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								}
							}
						} else if (true == "GET".equals(method)) {
							// service/{service-id}/line/{line-id}/history
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final java.util.Date __date_from = date_from;
								final java.util.Date __date_to = date_to;
							  /*final */Long date_interval_in_ms = null;
								if (true == qs.containsKey("date-interval-in-ms")) {
								  /*final Long */date_interval_in_ms = new Long(qs.get("date-interval-in-ms"));
								} else if (true == qs.containsKey("date-interval-in-sec")) {
								  /*final Long */date_interval_in_ms = new Long(new Long(qs.get("date-interval-in-sec")).longValue()*1000L);
								} else if (true == qs.containsKey("date-interval-in-min")) {
								  /*final Long */date_interval_in_ms = new Long(new Long(qs.get("date-interval-in-min")).longValue()*1000L*60L);
								} else if (true == qs.containsKey("date-interval-in-hr")) {
								  /*final Long */date_interval_in_ms = new Long(new Long(qs.get("date-interval-in-hr")).longValue()*1000L*60L*60L);
								} else if (true == qs.containsKey("date-interval-in-days")) {
								  /*final Long */date_interval_in_ms = new Long(new Long(qs.get("date-interval-in-days")).longValue()*1000L*60L*60L*24L);
								} else if (true == qs.containsKey("date-interval-in-weeks")) {
								  /*final Long */date_interval_in_ms = new Long(new Long(qs.get("date-interval-in-weeks")).longValue()*1000L*60L*60L*24L*7L);
								} else if (true == qs.containsKey("date-interval-in-months")) {
									throw new UnsupportedOperationException(); // TODO: go by nearest calendar month start/end etc.
								} else if (true == qs.containsKey("date-interval-in-years")) {
									throw new UnsupportedOperationException(); // TODO: go by nearest calendar year start/end etc.
								}
								final Long __date_interval_in_ms = date_interval_in_ms;
							  /*final */Stream.interface_Callback sic = null;
								if (null == __date_interval_in_ms) {
								  /*final Stream.interface_Callback */sic = new ServiceLineHistoryFilter()
										.configuration(new java.util.HashMap<String, Object>(){
											private static final long serialVersionUID = 1L;
											{
												put("date-from", __date_from);
												put("date-to", __date_to);
											}
										})
									;
								} else {
								  /*final Stream.interface_Callback */sic = new ServiceLineHistoryFilterAggregate()
										.configuration(new java.util.HashMap<String, Object>(){
											private static final long serialVersionUID = 1L;
											{
												put("date-from", __date_from);
												put("date-to", __date_to);
												put("date-interval-in-ms", __date_interval_in_ms);
											}
										})
									;
								}
								if (null == /*service_*/history_id) {
									final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = Services.readHistory();
									if (true == trueElseSetStatus(null != /*service_*/history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									  /*final */java.util.Map<String, Object> results = null;
										if (true == trueElseSetStatus(0 < /*service_*/history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										  /*final */int index = 0;
											for (final java.util.Map<String, Object> /*service_*/history: /*services_*/history_list) {
												results = getLogic(service_id, /*service_*/line_id, index, /*service_*/history, date_from, date_to, results, httpStatuses);
												index++;
											}
										}
										writeResultsAndSetStatus(resp, results, sic, httpStatuses);
									}
								} else {
									final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
									if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
										final java.util.Map<String, Object> results = Services.readLineHistory(/*service_*/history_id, /*service_*/line_id);
										trueElseSetStatus(null != results, HttpStatus.SC_NOT_FOUND, httpStatuses);
										writeResultsAndSetStatus(resp, results, sic, httpStatuses);
									}
								}
							}
						} else if (true == "PUT".equals(method)) {
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (null == /*service_*/history_id) {
								  /*final */java.util.Map<Integer, Object> results = null;
									@SuppressWarnings("unchecked")
									final java.util.List<java.util.Map<String, Object>> /*services*/_history_list = (java.util.List<java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
									if (true == trueElseSetStatus(null != /*service*/_history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*service*/_history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										  /*final */int index = 0;
											for (final java.util.Map<String, Object> /*service*/_history: /*services*/_history_list) {
												results = putLogic(service_id, /*service_*/line_id, index, /*service*/_history, results, httpStatuses);
												index++;
											}
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								} else {
								  /*final */java.util.Map<String, Object> results = null;
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Object> /*service*/_history_line = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								  //if (true == trueElseSetStatus(true == "line-id".equals(/*service*/_history_line.get("line-id")), HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
										if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
											final java.util.Map<String, Object> /*service_*/history_line = Services.readLineHistory(/*service_*/history, /*service_*/line_id);
											if (true == trueElseSetStatus(null != /*service_*/history_line, HttpStatus.SC_CONFLICT, httpStatuses)) {
											  //final String service_id = (String)/*service_*/history.get("service-id");
												final String for_user_id = (String)/*service_*/history.get("for-user-id");
												final boolean success = Services.updateLineHistory(/*service_*/history_id, /*service_*/line_id, /*service*/_history_line);
												results = result("service-history-id", /*service_*/history_id, results);
												results = result("service-id", service_id, results);
												results = result("for-user-id", for_user_id, results);
												results = result("line-id", /*service_*/line_id, results);
												if (true == success) {
													results = result("http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result("http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
										}
								  //}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								}
							}
						} else if (true == "DELETE".equals(method)) {
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (null == /*service_*/history_id) {
								  /*final */java.util.Map<Integer, Object> results = null;
									final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = Services.readHistory(); // TODO: accept a list via ServletTool.getObjectFromStream(..)
									if (true == trueElseSetStatus(null != /*services_*/history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*services_*/history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										  /*final */int index = 0;
											for (final java.util.Map<String, Object> /*service_*/history: /*services_*/history_list) {
												results = deleteLogic(service_id, /*service_*/line_id, index, /*service*/history, results, httpStatuses);
												index++;
											}
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								} else {
								  /*final */java.util.Map<String, Object> results = null;
									final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
									if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
										final java.util.Map<String, Object> /*service_*/line_history = Services.readLineHistory(/*service_*/history, /*service_*/line_id);
										if (true == trueElseSetStatus(null != /*service_*/line_history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
										  //final String service_id = (String)/*service_*/history.get("service-id");
											final String for_user_id = (String)/*service_*/history.get("for-user-id");
											final boolean success = Services.deleteLineHistory(/*service_*/history_id, /*service_*/line_id);
											results = result("service-history-id", /*service_*/history_id, results);
											results = result("service-id", service_id, results);
											results = result("for-user-id", for_user_id, results);
											results = result("line-id", /*service_*/line_id, results);
											if (true == success) {
												results = result("http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
											} else {
												results = result("http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
											}
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								}
							}
						} else {
						  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
							throw new IllegalArgumentException();
						}
					}
				}
			} else {
				if (false == uri.contains("history")) {
					// service/{service-id}/lines
					throw new IllegalStateException(); // Should never get here
				} else {
					// service/{service-id}/lines/history
					throw new IllegalStateException(); // Should never get here
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}