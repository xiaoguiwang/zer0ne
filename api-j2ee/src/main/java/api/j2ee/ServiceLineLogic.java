package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class ServiceLineLogic extends Logic {
	@SuppressWarnings("unused")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/service/{service-id}/line/{line-id} # create (one line of one service) -- returns id
// GET   : /api/service/{service-id}/line/{line-id} # read (one line of one service)
// PUT   : /api/service/{service-id}/line/{line-id} # update (one line of one service)
// DELETE: /api/service/{service-id}/line/{line-id} # delete (one line of one service)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // [, api, service, {service-id}, line, {line-id}[, history]]]
		if (true == uri.contains("service")) {
			final java.lang.String service_id = uri.peek(1+2);
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == "history".equals(uri.peek())) {
						// service/{service-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				} else {
					final java.lang.String /*service_*/line_id = uri.peek(1+2+2);
					if (false == uri.contains("history")) {
						// service/{service-id}/line/{line-id}
					  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
						if (true == java.lang.Boolean.FALSE.booleanValue()) {
						} else if (true == "POST".equals(method)) { // POST /api/service/{service-id}/line // TODO: Additive (in parts)
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final java.util.Map<String, Object> service = Services.read(service_id);
								if (true == trueElseSetStatus(null != service, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Object> /*_service*/_line = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
									if (true == trueElseSetStatus(null != /*_service*/_line, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										final String /*_service*/_line_id = (String)/*_service*/_line.get("line-id");
										if (true == trueElseSetStatus(null != /*_service*/_line_id, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										  /*final */java.util.Map<String, Object> _results = null;
											// Note: Eclipse is too dumb to work out that the subsequent else block is not dead code! )-:
											if (null == results || null == results.get(service_id)) {
											  /*final java.util.Map<String, Object> */_results = new dto.util.HashMap<String, Object>(){
													private static final long serialVersionUID = 0L;
												};
												results = result(service_id, /*service*/_line_id, _results, results);
											} else {
												@SuppressWarnings("unchecked")
												final java.util.Map<String, Object> __results = (java.util.Map<String, Object>)results.get(service_id);
											  /*final java.util.Map<String, Object> */_results = __results;
											}
											if (null != Services.readLine(service_id, /*service*/_line_id)) {
												_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), _results);
											} else {
												if (true == Services.createLine(service_id, /*service_*/line_id, /*service*/_line)) {
													_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
												} else {
													_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
												}
											}
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "GET".equals(method)) {
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							  /*final java.util.Map<String, Object> */results = Services.readLine(service_id, /*service_*/line_id);
								trueElseSetStatus(null != results, HttpStatus.SC_NO_CONTENT, httpStatuses);
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "PUT".equals(method)) {
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final java.util.Map<String, Object> service = Services.read(service_id);
								if (true == trueElseSetStatus(null != service, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
									@SuppressWarnings("unchecked")
									final java.util.Map<String, Object> /*_service*/_line = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
									if (true == trueElseSetStatus(null != /*_service*/_line, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										final String /*_service*/_line_id = (String)/*_service*/_line.get("line-id");
										if (true == trueElseSetStatus(null != /*_service*/_line_id, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
											if (null == Services.readLine(service_id, /*service*/_line_id)) {
											/**/results = result(service_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
											} else {
												if (true == Services.updateLine(service_id, /*service_*/line_id, /*service*/_line)) {
												/**/results = result(service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
												/**/results = result(service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "DELETE".equals(method)) {
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final String /*_service*/_line_id = uri.depth() <= 5 ? null : uri.peek(1+2+2);
								if (true == trueElseSetStatus(null != /*_service*/_line_id, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (null == Services.readLine(service_id, /*service*/_line_id)) {
										results = result(service_id, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
									} else {
										if (true == Services.deleteLine(service_id, /*service*/_line_id)) {
											results = result(service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
										} else {
											results = result(service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else {
						  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
							throw new IllegalArgumentException();
						}
					} else {
						if (false == uri.contains("history")) {
							// service/{service-id}/line/{line-id}
							throw new IllegalStateException(); // Should never get here
						} else {
							// service/{service-id}/line/{line-id}/history
							throw new IllegalStateException(); // Should never get here
						}
					}
				}
			} else {
				if (false == uri.contains("history")) {
					// service/{service-id}/lines
					throw new IllegalStateException(); // Should never get here
				} else {
					// service/{service-id}/lines/history
					throw new IllegalStateException(); // Should never get here
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}