package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class ServiceLinesHistoryLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST: /api/service/{service-id}/lines/history # create (one or more lines of one service history) -- returns ids
// GET: /api/service/{service-id}/lines/history # read (all lines of one service history)
// PUT: /api/service/{service-id}/lines/history # update (one or more lines of one service history)
// DELETE: /api/service/{service-id}/lines/history # delete (all lines of one service history)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // [, api, service, {service-id}, lines, history]
		if (true == uri.contains("service")) {
		  /*final */String service_id = null;
			if (5 == uri.depth()) {
			} else if (6 == uri.depth() && true == "lines".equals(uri.peek(1+2+1))) {
			  /*final String */service_id = uri.peek(1+2);
			} else if (7 == uri.depth() && true == "lines".equals(uri.peek(1+2+1))) {
			  /*final String */service_id = uri.peek(1+2);
			}
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
						// service/{service-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				} else {
					if (false == uri.contains("history")) {
						// service/{service-id}/line/{line-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/line/{line-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				}
			} else {
				if (false == uri.contains("history")) {
					// service/{service-id}/lines
				} else {
					// service/{service-id}/lines/history[/{service-history-id}]
				  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
				  /*final */Integer /*service_*/history_id = null;
				  /*final */String /*service*/_history_id = null;
					if (5 == uri.depth()) {
					} else if (6 == uri.depth()) {
						if (true == "history".equals(uri.peek(4))) {
						  /*final String *//*service*/_history_id = uri.peek(1+2+2);
						}
					} else if (7 == uri.depth()) {
						if (true == "history".equals(uri.peek(5))) {
						  /*final String *//*service*/_history_id = uri.peek(1+2+3);
						} else if (true == "history".equals(uri.peek(6))) {
						  /*final String *//*service*/_history_id = uri.peek(1+2+4);
						}
					}
					try {
					  /*final Integer *//*service_*/history_id = new Integer(/*service*/_history_id);
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (true == java.lang.Boolean.FALSE.booleanValue()) {
					} else if (true == "POST".equals(method)) {
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.history && null != /*service_*/history_id, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
							if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != /*service*/_lines_history && null != /*service*/_history_id, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									for (final String /*service*/_line_id: /*service*/_lines_history.keySet()) {
										@SuppressWarnings("unchecked")
										final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service*/_line_id);
										if (null == Services.readLineHistory(/*service_*/history, /*service_history*/_line_id)) {
											if (true == Services.createLineHistory(/*service_*/history, /*service*/_line_id, /*service*/_line_history)) {
												results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
											} else {
												results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
											}
										} else {
											results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
										}
									}
								}
							}
						}
						writeResultsAndSetStatus(resp, results, httpStatuses);
					} else if (true == "GET".equals(method)) {
						// service/{service-id}/lines/history[/{service-history-id}]
						if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							if (true == trueElseSetStatus(0 < Services.history.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
								if (null == /*service*/_history_id) {
									final java.util.List<java.util.Map<String, Object>> results;// = null;
									if (null == service_id) {
									  /*final java.util.List<java.util.Map<String, Object>> */results = Services.readLinesHistory();
									} else {
									  /*final java.util.List<java.util.Map<String, Object>> */results = Services.readLinesHistory(service_id);
									}
									if (true == trueElseSetStatus(null != results, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
										trueElseSetStatus(0 < results.size(), HttpStatus.SC_NO_CONTENT, httpStatuses);
										writeResultsAndSetStatus(resp, results, httpStatuses);
									}
								} else {
									final java.util.Map<String, Object> results = Services.readLinesHistory(/*service_*/history_id);
									trueElseSetStatus(null != results, HttpStatus.SC_NOT_FOUND, httpStatuses);
									writeResultsAndSetStatus(resp, results, httpStatuses);
								}
							}
						}
					} else if (true == "PUT".equals(method)) {
						// service/{service-id}/lines/history[/{service-history-id}]
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.history && null != /*service_*/history_id, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
							if (true == trueElseSetStatus(null != /*service_*/history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != /*service*/_lines_history && null != /*service*/_history_id, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									for (final String /*service*/_line_id: /*service*/_lines_history.keySet()) {
										@SuppressWarnings("unchecked")
										final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service*/_line_id);
										if (null != Services.readLineHistory(/*service_*/history, /*service_history*/_line_id)) {
											if (true == Services.updateLineHistory(/*service_*/history_id, /*service*/_line_id, /*service*/_line_history)) {
												results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
											} else {
												results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
											}
										} else {
											results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
										}
									}
								}
							}
						}
						writeResultsAndSetStatus(resp, results, httpStatuses);
					} else if (true == "DELETE".equals(method)) {
						// service/{service-id}/lines/history[/{service-history-id}]
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							if (true == trueElseSetStatus(0 < Services.history.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
								if (null == /*service*/_history_id) {
									final java.util.List<java.util.Map<String, Object>> /*service_*/lines_history = Services.readLinesHistory(service_id);
								/**/if (true == trueElseSetStatus(null != /*service_*/lines_history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*service_*/lines_history.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
											Services.deleteHistory(/*service_*/history_id);
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								} else {
									final java.util.Map<String, Object> /*service_*/lines_history = Services.readLinesHistory(/*service_*/history_id);
								/**/if (true == trueElseSetStatus(null != /*service_*/lines_history, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*service_*/lines_history.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
											for (final String /*service_*/line_id: /*service_*/lines_history.keySet()) { 
												if (true == Services.deleteLineHistory(/*service_*/history_id, /*service_*/line_id)) {
													results = result(/*service_*/line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result(/*service_*/line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
										}
									}
									writeResultsAndSetStatus(resp, results, httpStatuses);
								}
							}
						}
					} else {
					  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
						throw new IllegalArgumentException();
					}
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}