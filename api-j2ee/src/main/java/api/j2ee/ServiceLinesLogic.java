package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class ServiceLinesLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/service/{service-id}/lines # create (one or more lines of one service) -- returns ids
// GET   : /api/service/{service-id}/lines # read (all lines of one service)
// GET   : /api/service/lines              # read (all lines of all services)
// PUT   : /api/service/{service-id}/lines # update (one or more lines of one service)
// DELETE: /api/service/{service-id}/lines # delete (all lines of one service)
// DELETE: /api/service/lines              # delete (all lines of all services -- needs special permission)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // [, api, service, {service-id}, lines[, history]]]
		if (true == uri.contains("service")) {
			final java.lang.String service_id = uri.peek(1+2);
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
						// service/{service-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				} else {
					if (false == uri.contains("history")) {
						// service/{service-id}/line/{line-id}
						throw new IllegalStateException(); // Should never get here
					} else {
						// service/{service-id}/line/{line-id}/history
						throw new IllegalStateException(); // Should never get here
					}
				}
			} else {
				if (false == uri.contains("history")) {
					// service/{service-id}/lines
				  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
					if (true == java.lang.Boolean.FALSE.booleanValue()) {
					} else if (true == "POST".equals(method)) {
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							final java.util.Map<String, Object> service = Services.read(service_id);
							if (true == trueElseSetStatus(null != service, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.Map<String, Object> /*service*/_lines = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != /*service*/_lines, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < /*service*/_lines.size(), HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									  /*final */java.util.Map<String, Object> _results = null;
										final java.util.Map<String, Object> /*service_*/lines = Services.readLines(service_id);
										for (final String /*service*/_line_id: /*service*/_lines.keySet()) {
											// Note: Eclipse is too dumb to work out that the subsequent else block is not dead code! )-:
											if (null == results || null == results.get(service_id)) {
											  /*final java.util.Map<String, Object> */_results = new dto.util.HashMap<String, Object>(){
													private static final long serialVersionUID = 0L;
												};
												results = result(service_id, "%lines", _results, results);
											} else {
												@SuppressWarnings("unchecked")
												final java.util.Map<String, Object> __results = (java.util.Map<String, Object>)results.get(service_id);
											  /*final java.util.Map<String, Object> */_results = __results;
											}
											if (null != Services.readLine(/*service_*/lines, /*service*/_line_id)) {
												_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), _results);
											} else {
												final java.util.Map<String, Object> /*service*/_line = Services.readLine(/*service*/_lines, /*service*/_line_id);
												if (null != /*service*/_line) {
													if (true == Services.createLine(service_id, /*service*/_line_id, /*service*/_line)) {
														_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
													} else {
														_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
													}
												} else {
													_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_BAD_REQUEST, httpStatuses), _results);
												}
											}
										}
									}
								}
							}
						}
						writeResultsAndSetStatus(resp, results, httpStatuses);
					} else if (true == "GET".equals(method)) {
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
						  /*final java.util.Map<String, Object> */results = Services.readLines(service_id);
							if (true == trueElseSetStatus(null != results, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
								trueElseSetStatus(0 < results.size(), HttpStatus.SC_NO_CONTENT, httpStatuses);
							}
						}
						writeResultsAndSetStatus(resp, results, httpStatuses);
					} else if (true == "PUT".equals(method)) {
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							final java.util.Map<String, Object> service = Services.read(service_id);
							if (true == trueElseSetStatus(null != service, HttpStatus.SC_NOT_FOUND, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.Map<String, Object> /*service*/_lines = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != /*service*/_lines, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < /*service*/_lines.size(), HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									  /*final */java.util.Map<String, Object> _results = null;
										final java.util.Map<String, Object> /*service_*/lines = Services.readLines(service_id);
										for (final String /*service*/_line_id: /*service*/_lines.keySet()) {
											// Note: Eclipse is too dumb to work out that the subsequent else block is not dead code! )-:
											if (null == results || null == results.get(service_id)) {
											  /*final java.util.Map<String, Object> */_results = new dto.util.HashMap<String, Object>(){
													private static final long serialVersionUID = 0L;
												};
												results = result(service_id, "%lines", _results, results);
											} else {
												@SuppressWarnings("unchecked")
												final java.util.Map<String, Object> __results = (java.util.Map<String, Object>)results.get(service_id);
											  /*final java.util.Map<String, Object> */_results = __results;
											}
											if (null == Services.readLine(/*service_*/lines, /*service*/_line_id)) {
												_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), _results);
											} else {
												final java.util.Map<String, Object> /*service*/_line = Services.readLine(/*service*/_lines, /*service*/_line_id);
												if (null != /*service*/_line) {
													if (true == Services.updateLine(service_id, /*service*/_line_id, /*service*/_line)) {
														_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), _results);
													} else {
														_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), _results);
													}
												} else {
													_results = result(/*service*/_line_id, "http-status", httpStatus(HttpStatus.SC_BAD_REQUEST, httpStatuses), _results);
												}
											}
										}
									}
								}
							}
						}
						writeResultsAndSetStatus(resp, results, httpStatuses);
					} else if (true == "DELETE".equals(method)) {
					  /*final */java.util.Map<String, Object> results = null;
						if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							@SuppressWarnings("unchecked")
							final java.util.List<String> /*_service*/_line_ids = (java.util.List<String>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
							if (null != /*_service*/_line_ids) {
								if (true == trueElseSetStatus(0 < /*_service*/_line_ids.size(), HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									for (final String /*_service*/_line_id: /*_service*/_line_ids) {
										if (null == Services.readLine(service_id, /*service*/_line_id)) {
											results = result(service_id, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
										} else {
											if (true == Services.deleteLine(service_id, /*service*/_line_id)) {
												results = result(service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
											} else {
												results = result(service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
											}
										}
									}
								}
							} else {
								final java.util.List<String> /*service_*/line_ids = Services.getLineIds(service_id);
								for (final String /*service_*/line_id: /*service_*/line_ids) {
									if (true == Services.deleteLine(service_id, /*service_*/line_id)) {
										results = result(/*service_*/line_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
									} else {
										results = result(/*service_*/line_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
									}
								}
							}
						}
						writeResultsAndSetStatus(resp, results, httpStatuses);
					} else {
					  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
						throw new IllegalArgumentException();
					}
				} else {
					// service/{service-id}/lines/history
					throw new IllegalStateException(); // Should never get here
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}