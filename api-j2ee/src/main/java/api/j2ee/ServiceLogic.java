package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
public class ServiceLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/service              # create (one) -- returns id
// GET   : /api/service/{service-id} # read (one)
// PUT   : /api/service/{service-id} # update (one)
// DELETE: /api/service/{service-id} # delete (one)
// DELETE: /api/service/history # delete (all history of all services)
		final Stack<String> uri = new Stack<>(req.getRequestURI().toString().split("\\/")); // [, api, service, {service-id}[, line, {line-id}]]
		if (true == uri.contains("service")) {
			final java.lang.String service_id = uri.peek(1+2);
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
						// service/{service-id}
					  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
						if (true == java.lang.Boolean.FALSE.booleanValue()) {
						} else if (true == "POST".equals(method)) { // POST /api/service // TODO: Additive (in parts)
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.Map<String, Object> _service = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != _service, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									final String _service_id = (String)_service.get("service-id");
									if (null != Services.read(_service_id)) {
										results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
									} else {
										if (true == Services.create(_service_id, _service)) {
											results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
										} else {
											results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "GET".equals(method)) {
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							  /*final java.util.Map<String, Object> */results = Services.read(service_id);
								trueElseSetStatus(null != results, HttpStatus.SC_NO_CONTENT, httpStatuses);
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "PUT".equals(method)) {
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								@SuppressWarnings("unchecked")
								final java.util.Map<String, Object> _service = (java.util.Map<String, Object>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != _service, HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									final String _service_id = (String)_service.get("service-id");
									if (null == Services.read(service_id)) {
										resp.setStatus(HttpStatus.SC_CONFLICT);
									} else {
										if (true == Services.update(_service_id, _service)) {
											results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
										} else {
											results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "DELETE".equals(method)) {
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (true == trueElseSetStatus(null != service_id && 0 < service_id.length(), HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									final java.util.Map<String, Object> service = Services.read(service_id);
									if (null != service) {
										if (true == Services.delete(service_id)) {
											results = result(service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
										} else {
											results = result(service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
										}
									} else {
										results = result(service_id, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else {
						  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
							throw new IllegalArgumentException();
						}
					} else {
						// service/{service-id}/history
						new api.j2ee.ServiceHistoryLogic()
							.logic(req, resp);
					}
				} else {
					if (false == uri.contains("history")) {
						// service/{service-id}/line/{line-id}
						new api.j2ee.ServiceLineLogic()
							.logic(req, resp);
					} else {
						// service/{service-id}/line/{line-id}/history
						new api.j2ee.ServiceLineHistoryLogic()
							.logic(req, resp);
					}
				}
			} else {
				if (false == uri.contains("history")) {
					// service/{service-id}/lines
					new api.j2ee.ServiceLinesLogic()
						.logic(req, resp);
				} else {
					// service/{service-id}/lines/history
					new api.j2ee.ServiceLinesHistoryLogic()
						.logic(req, resp);
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}