package api.j2ee;

import java.util.Calendar;
import java.util.TimeZone;

@SuppressWarnings("unchecked")
public class Services {
  //private static final Object me = new Object(); 
	public static /*final */java.util.Map<String, java.util.Map<String, Object>> services;// = null;
	public static /*final */java.util.List<java.util.Map<String, Object>> history;// = null;
public static /*final */dro.lang.Integer sequence;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Services.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
		if (null == services) {
			services = new dto.util.HashMap<String, java.util.Map<String, Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("health-check", new dto.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-id", "health-check");
							put("description", "health check for web/e-mail services");
							put("$state", "available");
							put("%lines", new dto.util.HashMap<String, java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									put("whois", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "whois");
											put("description", "whois for domain registration check");
											put("$state", "stopped");
										}
									});
									put("dns", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "dns");
											put("description", "dns-lookup for ip resolution check");
											put("$state", "stopped");
										}
									});
									put("ping", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "ping");
											put("description", "ping for ip address reachability check");
											put("$state", "stopped");
										}
									});
									put("wget", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "wget");
											put("description", "wget for web service availability check");
											put("$state", "stopped");
										}
									});
									put("openssl:s_client:443", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "openssl:s_client:443");
											put("description", "openssl s_client ssl certificate validity check");
											put("$state", "stopped");
										}
									});
								}
							});
						}
					});
				}
			};
		}
		if (null == sequence) {
			sequence = new dro.lang.Integer(0);
		}
		if (null == history) {
			history = new dto.util.ArrayList<java.util.Map<String, Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dto.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-history-id", sequence.value(sequence.value()+1).value());
							put("service-id", "health-check");
							put("for-user-id", "admin");
							put("%lines", new dto.util.HashMap<String, Object>(){
								private static final long serialVersionUID = 0L;
								{
									put("whois", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "whois");
											put("%charged", new dto.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
											  /*{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
												}*/
											});
										}
									});
									put("dns", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "dns");
											put("%charged", new dto.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
											  /*{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
												}*/
											});
										}
									});
									put("ping", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "ping");
											put("%charged", new dto.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
											  /*{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
												}*/
											});
										}
									});
									put("wget", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "wget");
											put("%charged", new dto.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
											  /*{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
												}*/
											});
										}
									});
									put("openssl:s_client:443", new dto.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "openssl:s_client:443");
											put("%charged", new dto.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
											  /*{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
												}*/
											});
										}
									});
								}
							});
						}
					});
				}
			};
final Calendar c = Calendar.getInstance();
c.setTimeZone(TimeZone.getTimeZone("UTC"));
final int y  = c.get(Calendar.YEAR);
final int m = c.get(Calendar.MONTH);
c.clear();
c.set(y, m, 1);
final java.util.Map<String, Object>/*service_*/history_lines = (java.util.Map<String, Object>)history.get(0).get("%lines");
Services.fakeCharges(c, /*service_*/history_lines, "whois", 60*8, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 60 minutes x 8 = 8 hours
Services.fakeCharges(c, /*service_*/history_lines, "dns", 60, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 60 minutes
Services.fakeCharges(c, /*service_*/history_lines, "ping", 1, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 1 minute
Services.fakeCharges(c, /*service_*/history_lines, "wget", 15, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 15 minutes
Services.fakeCharges(c, /*service_*/history_lines, "openssl:s_client:443", 60*24, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 60 minutes x 24 hours = 1 day
		}
	}
public static void fakeCharges(final java.util.Calendar c, final java.util.Map<String, Object> /*service_*/history_lines, final String service_id, final int dm, final Double units_factor, final Float units_absolute, final Double ms_offset_factor) {
	final java.util.Map<Long, Float> charged = (java.util.Map<Long, Float>)((java.util.Map<String, Object>)/*service_*/history_lines.get(service_id)).get("%charged");
	for (int d = 1; d <= c.getActualMaximum(Calendar.DAY_OF_MONTH); d++) {
		c.set(Calendar.DATE, d);
		long offset = c.getTimeInMillis();
		for (long m = 0; m < 60*24; m+=dm) {
			final Float units = null == units_factor ? (null == units_absolute ? 0.0f : units_absolute) : (float)((double)units_factor*Math.random());
			final long random = null == ms_offset_factor ? 0L : (long)(ms_offset_factor*Math.random());
			final Long ms = new Long(offset+1000L*60L*m+random);
			charged.put(ms, units);
		}
	}
}
	
	public static /*final */java.util.Map<String, java.util.List<String>> state_transitions;// = null;
	
	static {
	  /*final java.util.Map<String, java.util.List<String>> */state_transitions = new java.util.HashMap<String, java.util.List<String>>(){
			private static final long serialVersionUID = 0L;
			{
				put("running", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("stopped");
					}
				});
				put("stopped", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("running");
					}
				});
				put("available", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("unavailable");
					}
				});
				put("unavailable", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("available");
					}
				});
			}
		};
	}
	
	public static java.util.Map<String, Object> get(final Integer /*service_*/id) {
	  /*final */java.util.Map<String, Object> service = null;
		if (null != /*service_*/id) {
			final int intValue = /*service_*/id.intValue();
			for (final String service_id: services.keySet()) {
if (true == service_id.contains("$id")) continue;
				final java.util.Map<String, Object> __service = services.get(service_id);
				if (intValue == ((Integer)__service.get("$id")).intValue()) {
					service = __service;
					break;
				}
			}
		}
		return service;
	}
	
	public static java.util.List<String> getServiceIds(final java.util.Map<String, java.util.Map<String, Object>> services) {
		return new java.util.ArrayList<String>(services.keySet());
	}
	public static java.util.List<String> getServiceIds() {
		return Services.getServiceIds(Services.services);
	}
	
	public static boolean create(final java.lang.String service_id, final java.util.Map<String, Object> service) {
	  /*final */boolean success = false;
		if (null != services && null != service_id && null != service) {
			// TODO: data validation
			Services.services.put(service_id, service);
		  /*final boolean */success = true;
		}
		return success;
	}
	public static java.util.Map<String, java.util.Map<String, Object>> read() {
		return Services.services;
	}
	public static java.util.Map<String, Object> read(final java.util.Map<String, java.util.Map<String, Object>> services, final java.lang.String service_id) {
		return null == services ? null : null == service_id ? null : services.get(service_id);
	}
	public static java.util.Map<String, Object> read(final java.lang.String service_id) {
		return Services.read(Services.services, service_id);
	}
	public static boolean update(final java.lang.String service_id, final java.util.Map<String, Object> service) {
	  /*final */boolean success = false;
		if (null != services && null != service_id && null != Services.read(service_id)) {
			// TODO: data validation, and potentially, map from argument above to parameter below
			Services.services.put(service_id, service); // TODO: do NOT fully overwrite!
		  /*final boolean */success = true;
		}
		return success;
	}
	public static boolean delete(final java.lang.String service_id) {
	  /*final */boolean success = false;
		if (null != services && null != service_id && null != Services.read(service_id)) {
			Services.services.remove(service_id);
		  /*final boolean */success = true;
		}
		return success;
	}
	
	public static java.lang.Integer createHistory(final java.util.Map<String, Object> /*service_*/history) {
	  /*final */java.lang.Integer /*service_*/history_id = null;
		if (null != Services.history && /*null != *//*service_*//*history_id && */null != /*service_*/history) {
		  /*final java.lang.Integer *//*service_*/history_id = Services.sequence.value();
			/*service_*/history.put("service-history-id", /*service_*/history_id);
			Services.sequence.value(/*service_*/history_id.intValue()+1);
			Services.history.add(history);
		}
		return /*service_*/history_id;
	}
	public static java.util.List<java.util.Map<String, Object>> readHistory() {
		return Services.history;
	}
	public static java.util.Map<String, Object> readHistory(final java.util.List<java.util.Map<String, Object>> /*services_*/history, final java.lang.Integer /*service_*/history_id) {
	  /*final */java.util.Map<String, Object> /*service*/_history = null;
		if (null != /*service_*/history && null != /*service_*/history_id) {
			for (final java.util.Map<String, Object> service_history: /*service_*/history) {
				if (null != service_history && true == ((Integer)service_history.get("service-history-id")).equals(/*service_*/history_id)) {
			  //if (null != service_history && true == ((Long)service_history.get("service-history-id")).equals(new Long(/*service_*/history_id.toString()))) { // BEWARE/TODO: look what JSON Parser made me do!
					/*service*/_history = service_history;
					break;
				}
			}
		}
		return /*service*/_history;
	}
	public static java.util.Map<String, Object> readHistory(final java.lang.Integer /*service_*/history_id) {
		return Services.readHistory(Services.history, /*service_*/history_id);
	}
	public static java.util.List<java.util.Map<String, Object>> readHistory(final String service_id) {
	  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/history_list = null;
		for (final java.util.Map<String, Object> /*service*/_history: Services.readHistory()) {
			if (true == service_id.equals(/*service*/_history.get("service-id"))) {
				if (null == /*service_*/history_list) {
				  /*final java.util.List<java.util.Map<String, Object>> *//*service_*/history_list = new dto.util.ArrayList<>();
				}
				/*service_*/history_list.add(/*service*/_history);
			}
		}
		return /*service_*/history_list;
	}
public static java.util.Map<String, Object> findHistoryByServiceIdAndUserId(final java.lang.String service_id, final java.lang.String for_user_id) {
  /*final */java.util.Map<String, Object> /*service_*/history = null;
	for (final java.util.Map<String, Object> /*service*/_history: Services.readHistory()) {
		if (true == service_id.equals(/*service*/_history.get("service-id"))
			&&
			true == for_user_id.equals(/*service*/_history.get("for-user-id"))
		) {
			/*service_*/history = /*service*/_history;
			break;
		}
	}
	return /*service_*/history;
}
	public static boolean updateHistory(final java.lang.Integer /*service_*/history_id, final java.util.Map<String, Object> history) {
	  /*final */boolean success = false;
		if (null != Services.history && null != /*service_*/history_id) {
			java.util.Map<String, Object> /*service*/_history = Services.readHistory(/*service_*/history_id);
			if (null != /*service*/_history) {
				// TODO: data validation
			  //Services.history.set(/*service_*/history_id.intValue(), history);
				for (final String key: /*service_*/history.keySet()) {
					if (true == "service-id".equals(key) || true == "service-history-id".equals(key)) continue;
					final Object value = /*service_*/history.get(key);
					if (null != value) {
						/*service*/_history.put(key, value);
					}
				}
			  /*final boolean */success = true;
			}
		}
		return success;
	}
public static java.util.List<Integer> getHistoryIds(final java.util.List<java.util.Map<String, Object>> /*services_*/history) {
  /*final */java.util.List<Integer> /*service_*/history_ids = null;
	if (null != /*services_*/history) {
		for (final java.util.Map<String, Object> service_history: /*services_*/history) {
			if (null == /*service_*/history_ids) {
			  /*final java.util.List<Integer> *//*service_*/history_ids = new java.util.ArrayList<>();
			}
			/*service_*/history_ids.add((Integer)service_history.get("service-history-id"));
		  ///*service_*/history_ids.add(new Integer(((Long)service_history.get("service-history-id")).toString()));
		}
	}
	return /*service_*/history_ids;
}
public static java.util.List<Integer> getHistoryIds() {
	return Services.getHistoryIds(Services.history);
}
	public static boolean deleteHistory(final java.lang.Integer /*service_*/history_id) {
	  /*final */boolean success = false;
		final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
		if (null != Services.history && null != /*service_*/history_id && null != /*service_*/history) {
			Services.history.remove(/*service_*/history);
		  /*final boolean */success = true;
		}
		return success;
	}
	
	public static boolean createLine(final java.lang.String service_id, final java.lang.String /*service_*/line_id, final java.util.Map<String, Object> /*service_*/line) {
	  /*final */boolean success = false;
	  /*final */java.util.Map<String, Object> service = Services.read(service_id);
		if (null != Services.services && null != service && null != /*service_*/line_id && null != /*service_*/line) {
		  /*final */boolean create_service_lines = false, create_service_line = false;
		  /*final */java.util.Map<String, Object> /*service_*/lines = Services.getLines(service, false);
			if (null == /*service_*/lines) {
			  /*final boolean */create_service_lines = true;
			  /*final boolean */create_service_line = true;
			} else if (null == /*service_*/lines.get(/*service_*/line_id)) {
			  /*final boolean */create_service_line = true;
			}
			if (true == create_service_lines) {
			  /*final java.util.Map<String, Object> service_*/lines = new dto.util.HashMap<String, Object>();
				service.put("%lines", /*service_*/lines);
			}
			if (true == create_service_line) {
				// TODO: data validation
			  /*service_*/lines.put(/*service_*/line_id, /*service_*/line);
			  /*final boolean */success = true;
			}
		} else {
			throw new IllegalStateException();
		}
		return success;
	}
private static java.util.Map<String, Object> getLines(final java.util.Map<String, Object> service, final boolean newIfNotExists) {
  /*final */java.util.Map<String, Object> /*service_*/lines = null;
	if (null != service) {
	  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
		if (null == /*service_*/lines && true == newIfNotExists) {
		  /*final java.util.Map<String, Object> service_*/lines = new dto.util.HashMap<String, Object>();
			service.put("%lines", /*service_*/lines);
		}
	}
	return /*service_*/lines;
}
public static java.util.List<String> getLineIds(final String service_id) {
  /*final */java.util.List<String> /*service_*/line_ids = null;
	final java.util.Map<String, Object> service = Services.read(service_id);
	if (null != service) {
		final java.util.Map<String, Object> /*service_*/lines = Services.getLines(service, false);
		if (null != /*service_*/lines) {
			/*service_*/line_ids = new java.util.ArrayList<>(lines.keySet());
		}
	}
	return /*service_*/line_ids;
}
	public static java.util.Map<String, Object> readLines(final java.lang.String service_id) {
		final java.util.Map<String, Object> service = Services.read(service_id);
		return Services.getLines(service, false);
	}
	public static java.util.Map<String, Object> readLine(final java.lang.String service_id, final java.lang.String /*service_*/line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line = null;
		if (null != Services.services && null != service_id) {
		  /*final */java.util.Map<String, Object> service = Services.read(Services.services, service_id);
			if (null != service) {
				final java.util.Map<String, Object> /*service_*/lines = Services.getLines(service, false);
				if (null != /*service_*/lines) {
				  /*final java.util.Map<String, Object> service_*/line = (java.util.Map<String, Object>)lines.get(/*service_*/line_id);
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return /*service_*/line;
	}
	public static java.util.Map<String, Object> readLine(final java.util.Map<String, Object> /*service_*/lines, final java.lang.String /*service_*/line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line = null;
		if (null != /*service_*/lines) {
		  /*final java.util.Map<String, Object> service_*/line = (java.util.Map<String, Object>)lines.get(/*service_*/line_id);
		}
		return /*service_*/line;
	}
	public static boolean updateLine(final java.lang.String service_id, final java.lang.String /*service_*/line_id, final java.util.Map<String, Object> /*service_*/line) {
	  /*final */boolean success = false;
		if (null != Services.services && null != service_id && null != /*service_*/line_id && null != /*service_*/line) {
		  /*final */java.util.Map<String, Object> service = Services.read(service_id);
			if (null != service) {
				final java.util.Map<String, Object> /*service_*/lines = Services.getLines(service, false);
				if (null != /*service_*/lines) {
					if (null != /*service_*/lines.get(/*service_*/line_id)) {
						// TODO: data validation
					  /*service_*/lines.put(/*service_*/line_id, /*service_*/line); // TODO: do NOT fully overwrite!
					  /*final boolean */success = true;
					}
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return success;
	}
	public static boolean deleteLine(final java.lang.String service_id, final java.lang.String /*service_*/line_id) {
	  /*final */boolean success = false;
		if (null != Services.services && null != service_id && null != /*service_*/line_id) {
		  /*final */java.util.Map<String, Object> service = Services.read(service_id);
			if (null != service) {
				final java.util.Map<String, Object> /*service_*/lines = Services.getLines(service, false);
				if (null != /*service_*/lines) {
					if (null != /*service_*/lines.get(/*service_*/line_id)) { // Should not need, in future, but for now, there is a defect in dao.util.Map (although there would not be, in dto.util.Map)
					  /*service_*/lines.remove(/*service_*/line_id);
						if (0 == /*service_*/lines.size()) {
							service.remove("%lines");
						}
					  /*final boolean */success = true;
					}
				}
			}
		}
		return success;
	}
	
	public static Integer createLineHistory(final String service_id, final String for_user_id, final String /*service_*/line_id, final java.util.Map<String, Object> /*service*/_line_history) {
	  /*final */java.lang.Integer /*service_*/history_id = null;
		if (null != Services.history && null != service_id && null != for_user_id && null != /*service_*/line_id && null != /*service*/_line_history) {
			final java.util.Map<String, Object> service = Services.read(service_id);
			if (null != service) {
				if (null != Services.readLine(service_id, /*service_*/line_id)) {
				  /*final */boolean create_service_history = false, create_service_history_line = false;
				  /*final */java.util.Map<String, Object> /*service_*/history = Services.findHistoryByServiceIdAndUserId(service_id, for_user_id);
				  /*final */java.util.Map<String, Object> /*service_*/history_line = null;
				  /*final */java.util.Map<String, Object> /*service_*/history_lines = null;
					if (null == /*service_*/history) {
					  /*final boolean */create_service_history = true;
					  /*final boolean */create_service_history_line = true;
					} else {
					  /*final java.util.Map<String, Object> *//*service_*/history_lines = Services.getHistoryLines(/*service_*/history, true);
						if (null == /*service_*/history_lines.get(/*service_*/line_id)) {
						  /*final boolean */create_service_history_line = true;
						}
					}
					if (true == create_service_history_line) {
					  /*final java.util.Map<String, Object> *//*service_*/history_line = new dto.util.HashMap<String, Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("line-id", /*service_*/line_id);
								for (final String key: /*service*/_line_history.keySet()) {
									if (true == "line-id".equals(key)) continue;
									final Object value = /*service*/_line_history.get(key);
									if (null != value) {
										put(key, value);
									}
								}
							}
						};
					}
				  /*final java.util.Map<String, Object> *//*service_*/history_line = (java.util.Map<String, Object>)/*service_*/history_lines.get(/*service_*/line_id);
				  /*final */java.util.Map<Long, Object> consumed = (java.util.Map<Long, Object>)/*service_*/history_line.get("%consumed");
				  /*final */java.util.Map</*Long*/String, Object> _consumed = (java.util.Map</*Long*/String, Object>)/*service*/_line_history.get("%consumed");
					if (null == consumed && null != _consumed) {
					  /*final java.util.Map<Long, Object> */consumed = new dto.util.HashMap<Long, Object>();
						/*service_*/history_line.put("%consumed", consumed);
					}
					if (null != _consumed) {
						for (final java.util.Map.Entry</*Long*/String, Object> entry: _consumed.entrySet()) { // TODO: if exists, then fail (this is create)
							consumed.put(new Long(entry.getKey()), entry.getValue());
						}
					}
				  /*final */java.util.Map<Long, Object> charged = (java.util.Map<Long, Object>)/*service_*/history_line.get("%charged");
				  /*final */java.util.Map</*Long*/String, Object> _charged = (java.util.Map</*Long*/String, Object>)/*service*/_line_history.get("%charged");
					if (null == charged && null != _charged) {
					  /*final java.util.Map<Long, Object> */charged = new dto.util.HashMap<Long, Object>();
						/*service_*/history_line.put("%charged", charged);
					}
					if (null != _charged) {
						for (final java.util.Map.Entry</*Long*/String, Object> entry: _charged.entrySet()) { // TODO: if exists, then fail (this is create)
							charged.put(new Long(entry.getKey()), entry.getValue());
						}
					}
					if (true == create_service_history) {
						final java.util.Map<String, Object> __service_history_line = /*service_*/history_line;
					  /*final java.util.Map<String, Object> *//*service_*/history = new dto.util.HashMap<String, Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("service-id", service_id);
								put("for-user-id", for_user_id);
								put("%lines", new dto.util.HashMap<String, Object>(){
									private static final long serialVersionUID = 0L;
									{
										put(/*service_*/line_id, __service_history_line);
									}
								});
							}
						};
						/*service_*/history_id = Services.createHistory(/*service_*/history); // service-history-id assigned inside createHistory(..)
						/*service_*/history_lines.put(/*service_*/line_id, /*service_*/history_line);
					} else {
						/*service_*/history_id = (Integer)/*service_*/history.get("service-history-id");
					}
				} else {
					throw new IllegalArgumentException();
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return /*service_*/history_id;
	}
	public static boolean createLineHistory(final java.util.Map<String, Object> /*service_*/history, final String /*service_*/line_id, final java.util.Map<String, Object> /*service_*/line_history) {
	  /*final */boolean success = false;
		if (null != /*service_*/history && null != /*service_*/line_id && null != /*service_*/line_history) {
			final java.util.Map<String, Object> /*service_*/lines_history = Services.getHistoryLines(/*service_*/history, true);
			if (null == Services.readLineHistory(/*service_*/history, /*service_*/line_id)) {
				/*service_*/lines_history.put(/*service_*/line_id, /*service_*/line_history);
			  /*final boolean */success = true;
			}
		}
		return success;
	}
private static java.util.Map<String, Object> getHistoryLines(final java.util.Map<String, Object> /*service_*/history, final boolean newIfNotExists) {
  /*final */java.util.Map<String, Object> /*service_*/history_lines = null;
	if (null != /*service_*/history) {
	  /*final java.util.Map<String, Object> *//*service_*/history_lines = (java.util.Map<String, Object>)/*service_*/history.get("%lines");
		if (null == /*service_*/history_lines && true == newIfNotExists) {
		  /*final java.util.Map<String, Object> service_*/history_lines = new dto.util.HashMap<String, Object>();
			/*service_*/history.put("%lines", /*service_*/history_lines);
		}
	}
	return /*service_*/history_lines;
}
private static java.util.List<java.util.Map<String, Object>> getHistoryLines(final java.util.List<java.util.Map<String, Object>> /*service_*/history_list, final boolean newIfNotExists) {
  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/history_lines = null;
	for (final java.util.Map<String, Object> /*service*/_history: /*service_*/history_list) {
	  /*final */java.util.Map<String, Object> /*service*/_history_lines = null;
		if (null != /*service*/_history) {
		  /*final java.util.Map<String, Object> *//*service*/_history_lines = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
			if (null == /*service*/_history_lines && true == newIfNotExists) {
			  /*final java.util.Map<String, Object> service*/_history_lines = new dto.util.HashMap<String, Object>();
			}
			if (null != /*service*/_history_lines) {
				if (null == /*service_*/history_lines) {
				  /*final java.util.List<java.util.Map<String, Object>> *//*service_*/history_lines = new dto.util.ArrayList<>();
				}
				/*service_*/history_lines.add(/*service*/_history_lines);
			}
		}
	}
	return /*service_*/history_lines;
}
	public static java.util.Map<String, Object> readLinesHistory(final java.lang.Integer /*service_*/history_id) {
		final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
		return Services.getHistoryLines(/*service_*/history, false);
	}
	public static java.util.List<java.util.Map<String, Object>> readLinesHistory(final java.lang.String service_id) {
		final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = Services.readHistory(service_id);
		return Services.getHistoryLines(/*service_*/history_list, false);
	}
	public static java.util.List<java.util.Map<String, Object>> readLinesHistory() {
		final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = Services.readHistory();
		return Services.getHistoryLines(/*service_*/history_list, false);
	}
	public static java.util.Map<String, Object> readLineHistory(final java.util.Map<String, Object> /*service_*/history, final java.lang.String /*service_*/line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		if (null != /*service_*/history && null != /*service_*/line_id) {
			final java.util.Map<String, Object> /*service_*/history_lines = Services.getHistoryLines(/*service_*/history, false);
			if (null != /*service_*/history_lines) {
			  /*final java.util.Map<String, Object> /*service_*/line_history = (java.util.Map<String, Object>)/*service_*/history_lines.get(/*service_*/line_id);
			}
		} else {
			throw new IllegalStateException();
		}
		return /*service_*/line_history;
	}
	public static java.util.Map<String, Object> readLineHistory(final java.lang.Integer /*service_*/history_id, final java.lang.String /*service_*/line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		if (null != Services.history && null != /*service_*/history_id && null != /*service_*/line_id) {
		  /*final */java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
			if (null != /*service_*/history) {
				final java.util.Map<String, Object> /*service_*/history_lines = Services.getHistoryLines(/*service_*/history, false);
				if (null != /*service_*/history_lines) {
				  /*final java.util.Map<String, Object> /*service_*/line_history = (java.util.Map<String, Object>)/*service_*/history_lines.get(/*service_*/line_id);
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return /*service_*/line_history;
	}
public static java.util.List<String> getHistoryLineIds(final java.util.Map<String, Object> /*service_*/history) {
  /*final */java.util.List<String> /*service_*/history_line_ids = null;
	if (null != /*service_*/history) {
		final java.util.Map<String, Object> /*service_*/history_lines = Services.getHistoryLines(/*service_*/history, false);
		if (null != /*service_*/history_lines) {
			for (final java.util.Map.Entry<String, Object> entry: /*service_*/history_lines.entrySet()) {
				if (null == /*service_*/history_line_ids) {
				  /*final java.util.List<String> *//*service_*/history_line_ids = new dto.util.ArrayList<>();
				}
				final String /*service_*/line_id = entry.getKey();
				/*service_*/history_line_ids.add(/*service_*/line_id);
			}
		}
	}
	return /*service_*/history_line_ids;
}
public static java.util.Map<String, Object> findLineHistoryByServiceIdAndUserIdAndLineId(final java.lang.String service_id, final java.lang.String for_user_id, final java.lang.String /*service_*/line_id) {
  /*final */java.util.Map<String, Object> /*service*/line_history = null;
	final java.util.Map<String, Object> /*service_*/history = Services.findHistoryByServiceIdAndUserId(service_id, for_user_id);
	if (null != /*service_*/history) {
		final java.util.Map<String, Object> /*service_*/lines_history = Services.getHistoryLines(/*service_*/history, false);
		if (null != /*service_*/lines_history) {
		  /*final java.util.Map<String, Object> service*/line_history = (java.util.Map<String, Object>)/*service_*/lines_history.get(/*service_*/line_id);
		}
	}
	return /*service*/line_history;
}
// TODO: make this a generic dto.util.Map convergence algorithm..
private static void combineMaps(final java.util.Map<String, Object> map1, final java.util.Map<String, Object> map2) {
	for (final String key1: map1.keySet()) {
		final Object val1 = map1.get(key1);
		if (null != val1) {
			final Object val2 = map2.get(key1);
			if (val1 instanceof java.util.Map && val2 instanceof java.util.Map) {
				Services.combineMaps((java.util.Map<String, Object>)val2, (java.util.Map<String, Object>)val1);
			} else {
				map2.put(key1, val1);
			}
		}
	}
}
	public static boolean updateLineHistory(final java.lang.Integer /*service_*/history_id, final String /*service*/_line_id, final java.util.Map<String, Object> /*service*/_line_history) {
	  /*final */boolean success = false;
		if (null != Services.history && null != /*service_*/history_id && null != /*service*/_line_id && null != /*service*/_line_history) {
			final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
			if (null != /*service_*/history) {
				final java.util.Map<String, Object> /*service_*/line_history = Services.readLineHistory(/*service_*/history_id, /*service*/_line_id);
				if (null != /*service_*/line_history) {
					for (final String key: /*service*/_line_history.keySet()) {
						if (true == "service-id".equals(key) || true == "service-history-id".equals(key)) continue;
						final Object _value = /*service*/_line_history.get(key);
						if (null != _value) {
							final Object value = /*service_*/line_history.get(key);
							if (_value instanceof java.util.Map && value instanceof java.util.Map) {
								Services.combineMaps((java.util.Map<String, Object>)_value, (java.util.Map<String, Object>)value);
							} else {
								/*service_*/line_history.put(key, _value);
							}
						}
						// TODO: remove entries in service_line_history that aren't in /*service_*/line_history??
					}
				  /*final boolean */success = true;
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return success;
	}
	public static boolean deleteLineHistory(final java.lang.Integer /*service_*/history_id, final java.lang.String /*service_*/line_id) {
	  /*final */boolean success = false;
		if (null != Services.history && null != /*service_*/history_id && null != /*service_*/line_id) {
			final java.util.Map<String, Object> /*service_*/history = Services.readHistory(/*service_*/history_id);
			if (null != /*service_*/history) {
				final java.util.Map<String, Object> /*service_*/history_lines = Services.getHistoryLines(/*service_*/history, false);
				if (null != /*service_*/history_lines && null != /*service_*/history_lines.get(/*service_*/line_id)) {
					/*service_*/history_lines.remove(/*service_*/line_id);
					if (0 == /*service_*/history_lines.size()) {
						/*service_*/history.remove("%lines");
					}
				  /*final boolean */success = true;
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return success;
	}
	
	public static java.util.Map<String, Object> getLine(final Integer id, final Integer line_id) {
	  /*final */java.util.Map<String, Object> line = null;
		if (null != id && null != line_id) {
			final java.util.Map<String, Object> service = Services.get(id);
			if (null != service) {
				final int intValue = line_id.intValue();
				final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)service.get("%lines");
				if (null != lines) {
					for (final String key: lines.keySet()) {
if (true == key.contains("$id")) continue;
						final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)lines.get(key);
						if (intValue == ((Integer)__line.get("$id")).intValue()) {
						  /*service_*/line = /*service*/__line;
							break;
						}
					}
				}
			}
		}
		return /*service_*/line;
	}
	public static java.util.Map<String, Object> getLineById(final Integer /*service_*/line_id) {
	  /*final */java.util.Map<String, Object> line = null;
		if (null != line_id) {
			for (final String service_id: services.keySet()) {
if (true == service_id.contains("$id")) continue;
				final java.util.Map<String, Object> __service = services.get(service_id);
				final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__service.get("%lines");
				if (null != lines && 0 < lines.size()) {
					final int intValue = /*service_*/line_id.intValue();
						for (final String /*service*/_line_id: lines.keySet()) {
if (true == /*service*/_line_id.contains("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line = (java.util.Map<String, Object>)/*service_*/lines.get(/*service*/_line_id);
						if (intValue == ((Integer)/*service*/_line.get("$id")).intValue()) {
						  /*service_*/line = /*service*/_line;
							break;
						}
					}
				}
			}
		}
		return /*service_*/line;
	}
	
	public static java.util.Map<String, Object> findServiceByLineId(final String service_line_id) {
	  /*final */java.util.Map<String, Object> service = null;
		for (final String key: services.keySet()) {
			final java.util.Map<String, Object> __service = services.get(key);
			final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__service.get("%lines");
			if (null != lines && 0 < lines.size() && true == lines.containsKey(service_line_id)) {
				service = __service;
				break;
			}
		}
		return service;
	}
	public static java.util.Map<String, Object> findHistoryByServiceId(final String service_id) {
	  /*final */java.util.Map<String, Object> history = null;
		if (null != service_id) {
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String __service_id = (String)__history.get("service-id");
				if (true == service_id.equals(__service_id)) {
					history = __history;
					break;
				}
			}
		}
		return history;
	}
	public static java.util.Map<String, Object> findServiceLineById(final String service_line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line = null;
		for (final String key: services.keySet()) {
			final java.util.Map<String, Object> __service = services.get(key);
			final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__service.get("%lines");
			if (null != lines && 0 < lines.size() && true == lines.containsKey(service_line_id)) {
				line = (java.util.Map<String, Object>)lines.get(service_line_id);
				break;
			}
		}
		return /*service_*/line;
	}
	public static java.util.Map<String, Object> findHistoryLineById(final String service_history_line_id) {
	  /*final */java.util.Map<String, Object> /*service_history_*/line = null;
		for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
			final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			if (null != lines && 0 < lines.size() && true == lines.containsKey(service_history_line_id)) {
				line = (java.util.Map<String, Object>)lines.get(service_history_line_id);
				break;
			}
		}
		return /*service_history_*/line;
	}
	public static java.util.Map<String, Object> findHistoryById(final Integer /*service_history_*/id) {
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
		if (null != /*service_history_*/id) {
			final int intValue = id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				final Integer __id = (Integer)/*service*/_history.get("$id");
				if (null != __id && intValue == __id.intValue()) {
				  /*service_*/history = /*service*/_history;
					break;
				}
			}
		}
		return /*service_*/history;
	}
	public static java.util.Map<String, Object> findLineHistoryById(final Integer /*service_line_history_*/id) {
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		if (null != /*service_line_history_*/id) {
			final int intValue = id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				final java.util.Map<String, Object> /*service_history_*/lines = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
				for (final String line_id: /*service_history_*/lines.keySet()) {
if (true == line_id.equals("$id")) continue;
					final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service_history_*/lines.get(line_id);
					final Integer __id = (Integer)/*service*/_line_history.get("$id");
					if (null != __id && intValue == __id.intValue()) {
					  /*service_*/line_history = /*service*/_line_history;
						break;
					}
				}
			}
		}
		return /*service_*/line_history;
	}
	public static java.util.Map<String, Object> findHistoryByLineId(final Integer /*service_history_line_*/id) {
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
		if (null != /*service_history_line_*/id) {
			final int intValue = /*service_history_line_*/id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
				for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
if (true == /*service_history*/_line_id.equals("$id")) continue;
					final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
					final Integer __id = (Integer)/*service*/_line_history.get("$id");
					if (null != __id && intValue == __id.intValue()) {
					  /*service_*/history = /*service*/_history;
						break;
					}
					break;
				}
			}
		}
		return /*service_*/history;
	}
	
	public static boolean checkService(final String service_line_id, final String user_id) {
		return null != Services.findServiceByLineId(service_line_id);
	}
	public static Integer reserveService(final String service_line_id, final String user_id, final Float number_of_units) {
	  /*final */Integer id = null;
		final java.util.Map<String, Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
			// Find (an open) or start a (new) bill: (find a way to configure service provider (user) against service against bill)
			final Integer bill_history_id = null;// TODO: = Bills.reserveBill("0"/*FIXME*/, user_id, service_line_id, number_of_units);
/**/	  /*final */java.util.Map<String, Object> history = null;
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String for_user_id = (String)__history.get("for-user-id");
				if (null != for_user_id && true == for_user_id.equals(user_id)) {
					final String __service_line_id = (String)__history.get("line-id");
					if (null != service_line_id && true == __service_line_id.equals(service.get("line-id"))) {
						history = __history;
						break;
					}
				}
			}
			if (null == history) {
/**/		  /*final java.util.Map<String, Object> */history = new /*dto.util.HashMap<>*/java.util.HashMap<>(); // Hack!
				history.put("service-id", service.get("service-id"));
				history.put("for-user-id", user_id);
			  //history.put("number-of-units:reserved", number_of_units);
				history.put("bill-history-id", bill_history_id);
				history.put("%lines", new dto.util.HashMap<String, Object>(){
					private static final long serialVersionUID = 0L;
					{
						put(service_line_id, new dto.util.HashMap<String, Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("line-id", service_line_id);
								put("number-of-units:reserved", number_of_units);
							}
						});
					}
				});
				Services.createHistory(history);
			} else {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
			  /*final */Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
				if (null != number_of_units_reserved && null != number_of_units) {
					line.put("number-of-units:reserved", new Float(number_of_units_reserved.floatValue()+number_of_units.floatValue()));
				} else {
					line.put("number-of-units:reserved", number_of_units);
				}
			}
		  /*final Integer */id = (Integer)history.get("service-history-id");
		} else {
			throw new IllegalArgumentException();
		}
		return id;
	}
	public static void consumeService(final String service_line_id, final String user_id, final Float number_of_units) {
		final java.util.Map<String, Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
		  /*final */java.util.Map<String, Object> history = null;
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String for_user_id = (String)__history.get("for-user-id");
				if (null != for_user_id && true == for_user_id.equals(user_id)) {
					final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__history.get("%lines");
					if (true == lines.containsKey(service_line_id)) {
						history = __history;
						break;
					}
				}
			}
			// Eek! You have to reserve service (history) to get a bill-history-id..
			if (null != history) {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
			  /*final */Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
				if (null != number_of_units_reserved && null != number_of_units) {
				  /*final */java.util.Map<Long, Object> consumed = (java.util.Map<Long, Object>)line.get("%consumed");
					if (null == consumed) {
					  /*final java.util.Map<Long, Object> */consumed = new dto.util.HashMap<>();
						line.put("%consumed", consumed);
					}
					final Long ms = new Long(System.currentTimeMillis());
					consumed.put(ms, number_of_units);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	public static void consumeService(final String service_id, final String user_id) {
		Services.consumeService(service_id, user_id, (Float)null);
	}
	public static void chargeForConsumedServices(final Integer service_history_id, final String service_line_id, final String user_id) {
	  /*final */java.util.Map<String, Object> history = null;
		for (final java.util.Map<String, Object> __history: Services.history) {
			if (((Integer)__history.get("service-history-id")).intValue() == service_history_id.intValue()) {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__history.get("%lines");
				if (true == lines.containsKey(service_line_id)) {
					history = __history;
					break;
				}
			}
		}
		if (null != history) {
		///*final */String service_id = (String)history.get("service-id");
		  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
		  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
		///*final */String user_id = (String)history.get("for-user-id");
		  /*final */java.util.Map<Long, Float> consumed = (java.util.Map<Long, Float>)line.get("%consumed");
		  /*final */java.util.Map<Long, Float> charged = (java.util.Map<Long, Float>)line.get("%charged");
		  /*final */float number_of_units = 0.0f;
			if (null != consumed && 0 < consumed.size()-1) {
			  /*final */java.util.List<Long> keys = new java.util.ArrayList<>();
				for (final Object o: consumed.keySet()) {
if (true == o.toString().equals("$id")) continue;
					final Long ms = (Long)o;
					if (null == charged) {
					  /*final java.util.Map<Long, Float> */charged = new dto.util.HashMap<Long, Float>();
						line.put("%charged", charged);
					}
					number_of_units += ((Float)consumed.get(ms)).floatValue();
					charged.put(ms, consumed.get(ms));
					if (null == keys) {
						keys = new java.util.ArrayList<Long>();
					}
					keys.add(ms);
				}
				for (final Long ms: keys) {
					consumed.remove(ms);
				}
			  //consumed.clear(); consumed = null;
			  //line.remove("%consumed");
				final Integer bill_history_id = (Integer)history.get("bill-history-id");
			  /*final Float number_of_units_remaining = Bills.chargeBill(bill_history_id, user_id, service_line_id, number_of_units);*/
//if (number_of_units_reserved.floatValue()-number_of_units.floatValue() != number_of_units_remaining.floatValue()) {
//		throw new IllegalStateException(); // This should never happen but identifies a discrepancy between Services and Bills i.e. (likely) something broke part-way through
//}
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	// README: pre-book (get an upfront reservation on the bill) and bulk blocking (e.g. 100 e-mails)
	//         don't intermingle very well with unreserveService, as when we unreserve a reservation
	//         we set the service reservation to zero, but that would wipe any upfront bookings..
	public static void unreserveService(final String service_line_id, final String user_id, final Float number_of_units) {
		final java.util.Map<String, Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
		  /*final */java.util.Map<String, Object> history = null;
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String for_user_id = (String)__history.get("for-user-id");
				if (null != for_user_id && true == for_user_id.equals(user_id)) {
					final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__history.get("%lines");
					if (true == lines.containsKey(service_line_id)) {
						history = __history;
						break;
					}
				}
			}
			if (null != history) {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
				final Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
			  /*final */Float number_of_units_to_unreserve = number_of_units;
				if (null == number_of_units_to_unreserve) {
				  /*final Float */number_of_units_to_unreserve = (Float)line.get("number-of-units:reserved");
				}
			  //Bills./*un*/reserveBill("0"/*FIXME*/, user_id, service_line_id, -(number_of_units_to_unreserve.floatValue()));
				final Float number_of_units_remaining = new Float(number_of_units_reserved.floatValue()-number_of_units_to_unreserve.floatValue());
				if (number_of_units_remaining.floatValue() > 0.0f) {
					line.put("number-of-units:reserved", number_of_units_remaining);
				} else if (number_of_units_remaining.floatValue() == 0.0f) {
				  //line.put("number-of-units:reserved", new Float(0.0f));
					line.remove("number-of-units:reserved");
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	public static void unreserveService(final String service_id, final String user_id) {
		Services.unreserveService(service_id, user_id, (Float)null);
	}
	
	public static java.util.List<String> getNextStates(final String state) {
		return Services.state_transitions.get(state);
	}
	
	public static void remove(final Integer /*service_*/id) {
		// TODO: check dependencies (can't delete a service/line if assigned to role/user/group.. etc.
		final java.util.Map<String, Object> service = Services.get(id);
		if (null != service) {
			final java.util.Map<String, Object> /*service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
			if (null != /*service_*/lines) {
			  /*final */java.util.List<String> line_ids = null;
				for (final String /*service_*/line_id: /*service_*/lines.keySet()) {
if (true == line_id.equals("$id")) continue;
				///*final java.util.Map<String, Object> service_*/line = (java.util.Map<String, Object>)/*service_*/lines.get(line_id);
					if (null == /*service_*/line_ids) {
					  /*final java.util.List<String> service_*/line_ids = new java.util.ArrayList<>();
					}
				  /*service_*/line_ids.add(/*service_*/line_id);
				}
				// TODO: is this really necessary, won't dto.util.HashMap do this anyway for free?
				if (null != /*service_*/line_ids) {
					for (final String /*service_*/line_id: /*service_*/line_ids) {
					  /*service_*/lines.remove(/*service_*/line_id);
					}
				}
				service.remove("%lines");
			}
			Services.services.remove((String)service.get("service-id"));
		}
	}
	public static void removeLine(final Integer /*service_*/id, Integer /*service_*/line_id) {
		// TODO: check dependencies (can't delete a service/line if assigned to role/user/group.. etc.
		final java.util.Map<String, Object> service = Services.get(/*service_*/id);
		if (null != service && null != /*service_*/line_id) {
			final int intValue = /*service_*/line_id.intValue();
			final java.util.Map<String, Object> /*service*/_lines = (java.util.Map<String, Object>)service.get("%lines");
			if (null != /*service*/_lines) {
			  /*final */String /*service_*/line_id_to_remove = null;
				for (final String /*service*/_line_id: /*service*/_lines.keySet()) {
if (true == /*service*/_line_id.equals("$id")) continue;
					final java.util.Map<String, Object> /*service*/_line = (java.util.Map<String, Object>)/*service*/_lines.get(/*service*/_line_id);
					if (null != /*service*/_line && null != /*service*/_line.get("$id")) {
						if (intValue == ((Integer)/*service*/_line.get("$id")).intValue()) {
						  /*service_*/line_id_to_remove = (String)/*service*/_line.get("line-id");
							break;
						}
					}
				}
				if (null != /*service_*/line_id_to_remove) {
				  /*service*/_lines.remove(/*service_*/line_id_to_remove);
				}
			}
		}
	}
	public static java.util.List<java.util.Map<String, Object>> getHistoryForServiceId(final Integer /*service_*/id) {
	  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/history_list = null;
		final java.util.Map<String, Object> service = Services.get(/*service_*/id);
		if (null != service) {
			final String service_id = (String)service.get("service-id");
			for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
				if (null != history.get("service-id") && /*service_*/history.get("service-id").equals(service_id)) {
					if (null == /*service_*/history_list) {
					  /*final java.util.List<java.util.Map<String, Object>> service_*/history_list = new dto.util.ArrayList<java.util.Map<String, Object>>();
					}
				  /*service_*/history_list.add(/*service_*/history);
				}
			}
		}
		return /*service_*/history_list;
	}
	public static java.util.List<java.util.Map<String, Object>> getHistoryForServiceId(final String /*service_*/id) {
	  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/history_list = null;
		final java.util.Map<String, Object> service = Services.read(/*service_*/id);
		if (null != service) {
			final String service_id = (String)service.get("service-id");
			for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
				if (null != history.get("service-id") && /*service_*/history.get("service-id").equals(service_id)) {
					if (null == /*service_*/history_list) {
					  /*final java.util.List<java.util.Map<String, Object>> service_*/history_list = new dto.util.ArrayList<java.util.Map<String, Object>>();
					}
				  /*service_*/history_list.add(/*service_*/history);
				}
			}
		}
		return /*service_*/history_list;
	}
	public static java.util.List<java.util.Map<String, Object>> getHistoryLinesForServiceId(final String /*service_*/id, final String /*service_*/line_id) {
	  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/line_history_list = null;
		final java.util.Map<String, Object> service = Services.read(/*service_*/id);
		if (null != service) {
			final String service_id = (String)service.get("service-id");
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (null != /*service*/_history.get("service-id") && /*service*/_history.get("service-id").equals(service_id)) {
					final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
					for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
//if (true == /*service_history*/_line_id.equals("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
						if (true == /*service*/_line_history.get("line-id").equals(/*service_*/line_id)) {
							if (null == /*service_*/line_history_list) {
							  /*final java.util.List<java.util.Map<String, Object>> service_*/line_history_list = new dto.util.ArrayList<java.util.Map<String, Object>>();
							}
						  /*service_*/line_history_list.add(/*service*/_line_history);
						}
					}
				}
			}
		}
		return /*service_*/line_history_list;
	}
	public static java.util.Map<String, Object> getHistoryById(final Integer /*service_*/history_id) {
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
		if (null != /*service_*/history_id) {
			final int intValue = /*service_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (intValue == ((Integer)/*service*/_history.get("$id")).intValue()) {
				  /*service_*/history = /*service*/_history;
					break;
				}
			}
		}
		return /*service_*/history;
	}
	public static void removeHistory(final Integer /*service_history_*/id) {
		// TODO: check dependencies (can't delete a service/history if assigned to role/user/group.. etc.
		if (null != /*service_history_*/id) {
			final int intValue = id.intValue();
		  /*final */Integer /*service_history*/_id_to_remove = null;
			for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
				final Integer /*service_history*/_id = (Integer)/*service_*/history.get("service-history-id");
				if (null != /*service_history*/_id && /*service_history*/_id.intValue() == intValue) {
				  /*service_history*/_id_to_remove = /*service_history*/_id;
					break;
				}
			}
			if (null != /*service_history*/_id_to_remove) {
				Services.history.remove(/*service_history*/_id_to_remove.intValue()); // FIXME/TODO: now we're hooped!
			}
		}
	}
	public static java.util.Map<String, Object> getLineHistoryById(final Integer /*service_*/history_id, final Integer /*service_history_*/line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		if (null != /*service_*/history_id) {
		  /*final */int intValue = /*service_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (intValue == ((Integer)/*service*/_history.get("$id")).intValue()) {
				  /*final int */intValue = /*service_history_*/line_id.intValue();
					final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
					for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
if (true == /*service_history*/_line_id.equals("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
						if (intValue == ((Integer)/*service*/_line_history.get("$id")).intValue()) {
						  /*service_*/line_history = /*service*/_line_history;
							break;
						}
					}
				}
			}
		}
		return /*service_*/line_history;
	}
	public static void removeLineHistoryById(final Integer /*service_*/history_id, final Integer /*service_history_*/line_id) {
		if (null != /*service_*/history_id) {
		  /*final */int intValue = /*service_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (intValue == ((Integer)/*service*/_history.get("$id")).intValue()) {
				  /*final int */intValue = /*service_history_*/line_id.intValue();
					final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
				  /*final */String /*service_history_*/line_id_to_remove = null;
					for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
if (true == /*service_history*/_line_id.equals("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
						if (intValue == ((Integer)/*service*/_line_history.get("$id")).intValue()) {
						  /*service_history_*/line_id_to_remove = /*service_history*/_line_id;
							break;
						}
					}
					if (null != /*service_history_*/line_id_to_remove) {
					  /*service*/_lines_history.remove(/*service_history_*/line_id_to_remove);
					}
				}
			}
		}
	}
}