package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

public class ServicesHistoryLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/services/history # create (one or many) -- return ids
// GET   : /api/services/history # read (all)
// PUT   : /api/services/history # update (one or many)
// DELETE: /api/services/history # delete (one or many) -- needs special permission
		final Stack<String> uri = new Stack<String>(req.getRequestURI().toString().split("\\/")); // [, api, services, history]
		if (true == uri.contains("services")) {
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
						// services
						throw new IllegalStateException(); // Should never get here
					} else {
					  /*final java.lang.String __history = */uri.pop();
						// "services/history" (every service)
					  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
						if (true == java.lang.Boolean.FALSE.booleanValue()) {
						} else if (true == "POST".equals(method)) { // POST /api/services/history // TODO: Additive (in parts)
						  /*final */java.util.Map<Integer, Object> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) { // TODO: DRO aliasing, as-in in future (we won't guarantee not-null)
								final java.util.List<java.util.Map<String, Object>> /*services*/_history_list = (java.util.List<java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
								if (true == trueElseSetStatus(null != /*services*/_history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < /*services*/_history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									  /*final */int index = 0;
										for (final java.util.Map<String, Object> /*service*/_history: /*services*/_history_list) {
											final String service_id = (String)/*service*/_history.get("service-id");
											final String for_user_id = (String)/*service*/_history.get("for-user-id");
											final java.util.Map<String, Object> service_history = Services.findHistoryByServiceIdAndUserId(service_id, for_user_id);
											if (null == service_history) {
												final java.lang.Integer /*service*/_history_id = Services.createHistory(/*service_*/_history);
												if (null != /*service*/_history_id) {
													results = result(index, "service-history-id", /*service*/_history_id, results);
													results = result(index, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											} else {
												results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
											}
											index++;
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "GET".equals(method)) {
						  /*final */java.util.List<java.util.Map<String, Object>> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (true == trueElseSetStatus(0 < Services.history.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
								  /*final java.util.List<java.util.Map<String, Object> */results = Services.readHistory();
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "PUT".equals(method)) { // PUT /api/services/history // TODO: Additive (in parts)
						  /*final */java.util.Map<Integer, Object> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final java.util.List<java.util.Map<String, Object>> /*services*/_history_list = (java.util.List<java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
								if (true == trueElseSetStatus(null != /*services*/_history_list, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < /*services*/_history_list.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									  /*final */int index = 0;
										for (final java.util.Map<String, Object> /*service*/_history: /*services*/_history_list) {
											final java.lang.Integer /*service*/_history_id = new Integer(((Long)/*service*/_history.get("service-history-id")).toString()); // BEWARE/TODO: look what JSON Parser made me do!
											results = result(index, "service-history-id", /*service*/_history_id, results);
											if (null == Services.readHistory(/*service*/_history_id)) {
												results = result(index, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
											} else {
												final String service_id = (String)/*service*/_history.get("service-id");
												results = result(/*service*/_history_id, "service-id", service_id, results);
												final String for_user_id = (String)/*service*/_history.get("for-user-id");
												results = result(/*service*/_history_id, "for-user-id", for_user_id, results);
												final java.util.Map<String, Object> service_history = Services.findHistoryByServiceIdAndUserId(service_id, for_user_id);
											  /*final */java.lang.Integer service_history_id = null;
												if (null != service_history) {
												  /*final java.lang.Integer */service_history_id = (Integer)service_history.get("service-history-id");
												///*final java.lang.Integer */service_history_id = new Integer(((Long)service_history.get("service-history-id")).toString());
												}
												if (null != service_history_id && true == service_history_id.equals(/*service*/_history_id)) {
													if (true == Services.updateHistory(service_history_id, /*service*/_history)) {
														results = result(index, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
													} else {
														results = result(index, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
													}
												} else {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
												}
											}
											index++;
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "DELETE".equals(method)) { // DELETE /api/services
						  /*final */java.util.Map<Integer, Object> results = null;
							if (true == trueElseSetStatus(null != Services.history, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							  //final java.util.List<Integer> /*service*/_history_ids = (java.util.List<Integer>)ServletTool.getObjectFromStream(req.getReader(), java.util.List.class);
								final java.util.List<Long> /*service*/_history_ids = (java.util.List<Long>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
								if (null != /*service*/_history_ids) {
									if (true == trueElseSetStatus(0 < /*service*/_history_ids.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
									  /*final */int index = 0;
									  //for (final Integer /*service*/_history_id: /*service*/_history_ids) {
										for (final Long /*service*/_history_id: /*service*/_history_ids) {
											results = result(index, "service-history-id", /*service*/_history_id, results);
										  //final java.util.Map<String, Object> /*service*/_history = (java.util.Map<String, Object>)Services.readHistory(/*service*/_history_id);
											final java.util.Map<String, Object> /*service*/_history = (java.util.Map<String, Object>)Services.readHistory(new Integer(/*service*/_history_id.toString()));
											if (null == /*service*/_history) {
												results = result(index, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
											} else {
												results = result(index, "service-id", (String)/*service*/_history.get("service-id"), results);
												results = result(index, "for-user-id", (String)/*service*/_history.get("for-user-id"), results);
											  //if (true == Services.deleteHistory(/*service*/_history_id)) {
												if (true == Services.deleteHistory(new Integer(/*service*/_history_id.toString()))) {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result(index, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
											index++;
										}
									}
								} else {
									final java.util.List<Integer> /*service_*/history_ids = Services.getHistoryIds();
									if (true == trueElseSetStatus(null != /*service_*/history_ids, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
										if (true == trueElseSetStatus(0 < /*service_*/history_ids.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
											for (final Integer /*service_*/history_id: /*service_*/history_ids) {
												if (null != /*service_*/history_id) {
													if (true == Services.deleteHistory(/*service_*/history_id)) {
														results = result(/*service_*/history_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
													} else {
														results = result(/*service_*/history_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
													}
												} else {
													results = result(/*service_*/history_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else {
						  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
							throw new IllegalArgumentException();
						}
					}
				} else {
				  /*final java.lang.String __line = */uri.pop();
					if (false == uri.contains("history")) {
						// "services/line"
						throw new IllegalArgumentException();
					} else {
					  /*final java.lang.String __history = */uri.pop();
						// "services/line/history"
						throw new IllegalArgumentException();
					}
				}
			} else {
			  /*final java.lang.String __lines = */uri.pop();
				if (false == uri.contains("history")) {
					// "services/lines"
					throw new IllegalStateException(); // Should never get here
				} else {
				  /*final java.lang.String __history = */uri.pop();
					// "services/lines/history"
					throw new IllegalStateException(); // Should never get here
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}