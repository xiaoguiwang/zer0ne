package api.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Stack;

public class ServicesLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  //final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String method = req.getMethod(); // ["HEAD", ]"POST", "GET", "PUT", "DELETE", ["OPTIONS", ]etc.
// POST  : /api/services # create (one or many) -- return ids
// GET   : /api/services # read (all)
// PUT   : /api/services # update (one or many)
// DELETE: /api/services # delete (one or many) -- needs special permission
		final Stack<String> uri = new Stack<String>(req.getRequestURI().toString().split("\\/")); // [, api, services[, history]]
		if (true == uri.contains("services")) {
			if (false == uri.contains("lines")) {
				if (false == uri.contains("line")) {
					if (false == uri.contains("history")) {
					  /*final */java.util.Set<Integer> httpStatuses = new java.util.HashSet<>();
						if (true == java.lang.Boolean.FALSE.booleanValue()) {
						} else if (true == "POST".equals(method)) { // POST /api/services
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) { // TODO: DRO aliasing, as-in in future (we won't guarantee not-null)
								final java.util.Map<String, java.util.Map<String, Object>> _services = (java.util.Map<String, java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != _services, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < _services.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										for (final String _service_id: _services.keySet()) {
											if (null != Services.read(_service_id)) {
												results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
											} else {
												final java.util.Map<String, Object> _service = (java.util.Map<String, Object>)Services.read(_services, _service_id);
												if (null != _service) {
													if (true == Services.create(_service_id, _service)) {
														results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
													} else {
														results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
													}
												} else {
													results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
												}
											}
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "GET".equals(method)) { // GET /api/services // Note: with SC_NO_CONTENT Chrome shows previous cached page!
						  /*final */java.util.Map<String, java.util.Map<String, Object>> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								if (true == trueElseSetStatus(0 < Services.services.size(), HttpStatus.SC_OK/*NO_CONTENT*/, httpStatuses)) {
								  /*final java.util.Map<String, java.util.Map<String, Object>> */results = Services.read();
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "PUT".equals(method)) { // PUT /api/services
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
								final java.util.Map<String, java.util.Map<String, Object>> _services = (java.util.Map<String, java.util.Map<String, Object>>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.Map.class);
								if (true == trueElseSetStatus(null != _services, HttpStatus.SC_BAD_REQUEST, httpStatuses)) {
									if (true == trueElseSetStatus(0 < _services.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										for (final String _service_id: _services.keySet()) {
											if (null == Services.read(_service_id)) {
												results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_CONFLICT, httpStatuses), results);
											} else {
												final java.util.Map<String, Object> _service = (java.util.Map<String, Object>)Services.read(_services, _service_id);
												if (null != _service) {
													if (true == Services.update(_service_id, _service)) { // TODO: partial update (not full overwrite)
														results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
													} else {
														results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
													}
												} else {
													results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_NO_CONTENT, httpStatuses), results);
												}
											}
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else if (true == "DELETE".equals(method)) { // DELETE /api/services
						  /*final */java.util.Map<String, Object> results = null;
							if (true == trueElseSetStatus(null != Services.services, HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses)) {
							  /*final */java.util.List<String> _service_ids = (java.util.List<String>)dto.Adapter.getObjectFromStream(req.getReader(), java.util.List.class);
								if (null != _service_ids) {
									if (true == trueElseSetStatus(0 < _service_ids.size(), HttpStatus.SC_NO_CONTENT, httpStatuses)) {
										for (final String _service_id: _service_ids) {
											if (null == Services.read(_service_id)) {
												results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_NOT_FOUND, httpStatuses), results);
											} else {
												if (true == Services.delete(_service_id)) {
													results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
												} else {
													results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
												}
											}
										}
									}
								} else {
								  /*final java.util.List<String> */_service_ids = Services.getServiceIds();
									for (final String _service_id: _service_ids) {
										if (true == Services.delete(_service_id)) {
											results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_OK, httpStatuses), results);
										} else {
											results = result(_service_id, "http-status", httpStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR, httpStatuses), results);
										}
									}
								}
							}
							writeResultsAndSetStatus(resp, results, httpStatuses);
						} else {
						  //resp.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);
							throw new IllegalArgumentException();
						}
					} else {
						// "services/history" (every service)
						new api.j2ee.ServicesHistoryLogic()
							.logic(req, resp);
					}
				} else {
					// "services/line"
					resp.setStatus(HttpStatus.SC_BAD_REQUEST);
				  //throw new IllegalArgumentException();
				}
			} else {
			  /*final java.lang.String __lines = */uri.pop();
				if (false == uri.contains("history")) {
					// "services/lines"
					resp.setStatus(HttpStatus.SC_BAD_REQUEST);
				  //throw new IllegalArgumentException();
				} else {
				  /*final java.lang.String __history = */uri.pop();
					// "services/lines/history"
					resp.setStatus(HttpStatus.SC_BAD_REQUEST);
				  //throw new IllegalArgumentException();
				}
			}
		} else {
			throw new IllegalStateException(); // Should never get here
		}
	}
}