package api.j2ee;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.Properties;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
  //private static dto.Context context;// = null;
	
	static {
		try {
			if (null == Properties.properties()) {
				Properties.properties(new Properties(Servlet.class));
			}
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	  /*context = */new dto.Context();
	}
	
	public final Map<String, String> map = new HashMap<>();
	
	@Override
	public void init() {
		Sessions.startup();
	}
	@Override
	public void destroy() {
		Sessions.shutdown();
	}
	
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	
	// https://stackoverflow.com/questions/8933054/how-to-read-and-copy-the-http-servlet-response-output-stream-content-for-logging
	protected void doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  /*final */java.util.Map<String, Object> session = null;
	  /*final */String session_id = ServletTool.getSessionId(req, resp);
		if (null != session_id) {
			if (false == Sessions.sessions.containsKey(session_id)) {
			  //session = new dao.util.HashMap<String, Object>();
				session = new java.util.HashMap<String, Object>();
				session.put("$date+time", System.currentTimeMillis());
				Sessions.sessions.put(session_id, session);
			} else {
				session = Sessions.sessions.get(session_id);
			}
			final Map<String, String[]> pm = req.getParameterMap();
			final String[] sa = (String[])pm.get("js");
			if (null != sa && 1 <= sa.length) {
				ServletTool.put(session, "js", new Boolean(pm.get("js")[0]));
				return;
			}
			req.setAttribute("session-id", session_id); // Required by almost/every page
		} else {
			req.removeAttribute("session-id"); // Hint: this should never happen..
		}
		
		if (null != session) {
			final String sign_in_method = (String)ServletTool.get(session, "sign-in-method");
			if (null != sign_in_method) {
				req.setAttribute("sign-in-method", sign_in_method);
			}
			if (null != ServletTool.get(session, "user")) {
				req.setAttribute("signed-in-method", (String)ServletTool.get(session, "signed-in-method"));
			}
		}
		
		final String url = null == req.getAttribute("url") ? req.getRequestURL().toString() : (String)req.getAttribute("url");
		final Map<String, String> qs = ServletTool.getQueryStringAsKeyValueMap(req);
	  /*final */Boolean redirect = null;
		final String __redirect = qs.get("redirect");
		if (true == qs.containsKey("redirect") && 0 < __redirect.length()) {
			if (true == __redirect.equals("true") || (true == __redirect.equals("false"))) {
				redirect = new Boolean(__redirect);
			}
		}
		redirect = null == req.getAttribute("redirect") ? redirect : (Boolean)req.getAttribute("redirect"); // Always allow override by req
	  //resp.setHeader("Pragma", "no-cache");
	  //final boolean basic = null == authorization ? false : authorization.toLowerCase().startsWith("basic");
	  //req.setAttribute("a-href-append", ServletTool.href((String)req.getAttribute("a-href-append"), "session-id="+session_id)); // TODO: url encoding
	/*//if (null == dts.get(session_id)) {
			final String previous_dts = dts.get(session_id);
			req.setAttribute("previous-dts", previous_dts);
	  //}
		dts.put(session_id, new Long(System.currentTimeMillis()).toString());*/
		
		final String uri = req.getRequestURI().toString();
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == uri.startsWith("/api") && true == uri.contains("/services")) {
			new ServicesLogic()
				.logic(req, resp);
		} else if (true == uri.contains("/api") && true == uri.contains("/service")) {
			new ServiceLogic()
				.logic(req, resp);
		} else if (true == uri.contains("/api")) {
			new GenericLogic()
				.logic(req, resp);
		} else if (true == uri.contains("/api") && false == url.endsWith(".jsp")) {
			if (null != qs && true == qs.containsKey("sign-in-method") && true == qs.get("sign-in-method").equals("basic-auth")) {
			  /*new SignInLogic()
					.logic(req, resp);*/
			} else {
				if (null == ServletTool.get(session, "user")) {
					ServletTool.put(session, "signed-in-method", "html-form");
					final String user_id = "admin";
					ServletTool.put(session, "sign-id", user_id);
					final java.util.Map<String, Object> user = null; // TODO: = Users.users.get(user_id); // This version of Servlet is in api.j2ee (so lacks Users for now)
					if (null != user) {
						if (null != user.get("sign-in-this-date+time")) {
							user.put("sign-in-last-successful", user.get("sign-in-this-date+time"));
						}
						user.put("sign-in-this-date+time", sdf.format(new java.util.Date()));
						ServletTool.put(session, "sign-in-last-successful", user.get("sign-in-last-successful"));
						if (null != user.get("sign-in-attempts-failed")) {
							ServletTool.put(session, "sign-in-attempts-failed", user.remove("sign-in-attempts-failed"));
						}
						ServletTool.put(session, "user", user);
					}
				}
			  /*new DefaultLogic()
					.logic(req, resp);*/
			}
		} else {
			throw new IllegalStateException();
		}
	  //response.setStatus(HttpStatus.SC_OK);
	}
}