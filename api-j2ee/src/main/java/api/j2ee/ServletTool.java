package api.j2ee;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletTool {
	// https://stackoverflow.com/questions/4011075/how-do-you-format-the-day-of-the-month-to-say-11th-21st-or-23rd-ordinal
	private static String[] suffixes = {
		"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th",
		"11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th",
		"21st", "22nd", "23rd", "24th", "25th", "26th", "27th", "28th", "29th", "30th",
		"31st"
	};
	private static final Calendar now = Calendar.getInstance();
	private static final int dayOfMonth = now.get(Calendar.DAY_OF_MONTH);
  //private static final SimpleDateFormat sdf27 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
  //private static final SimpleDateFormat sdf24 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static final SimpleDateFormat sdf20 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MMM/dd'"+suffixes[-1+dayOfMonth]+"'-HH:mm:ss");
	private final static String version = "0.0.1"+"@"+sdf.format(now.getTime()); // For example: 2020/Feb/20th-09:06:00
	
	static {
	  //sdf27.setTimeZone(TimeZone.getTimeZone("UTC"));
	  //sdf24.setTimeZone(TimeZone.getTimeZone("UTC"));
		sdf20.setTimeZone(TimeZone.getTimeZone("UTC"));
	  //sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	}
	
	public static final String getVersion() {
		return version;
	}
	
	public final static java.util.Set<String> SIGN_IN_METHODS = new java.util.HashSet<String>(){
		private static final long serialVersionUID = 0L;
		{
			add("default");
			add("html-form");
			add("basic-auth");
		  //add("client-ssl");
		}
	};

	public static final String generateString(final int size) {
		return dro.lang.String.generateString(size);
	}

	public static final String encodeString(final String encode) {
	  /*final */String encoded = null;
		try {
			encoded = URLEncoder.encode(encode, StandardCharsets.UTF_8.toString());
		} catch (final UnsupportedEncodingException e) {
			// TODO: something in future..
		}
		return encoded;
	}

	public static final String decodeString(final String encoded) {
	  /*final */String decoded = null;
		try {
			decoded = URLDecoder.decode(encoded, StandardCharsets.UTF_8.toString());
		} catch (final UnsupportedEncodingException e) {
			// TODO: something in future..
		}
		return decoded;
	}

	public static String getParameterOrAttribute(final HttpServletRequest req, final String key, final String __default) {
	  /*final */String value = req.getParameter(key);
		if (null == value) {
			value = __default;
		}
		return value;
	}
	public static Boolean getParameterOrAttribute(final HttpServletRequest req, final String key, final Boolean __default) {
	  /*final */Boolean value = null;
		final String __value = req.getParameter(key);
		if (null == __value) {
			final Boolean ____value = (Boolean)req.getAttribute(key);
			if (null == ____value) {
				value = __default;
			} else {
				value = ____value;
			}
		} else {
			value = new Boolean(true == "on".equals(__value));
		}
		return value;
	}
	public static void setOrRemoveAttribute(final HttpServletRequest req, final String key, final Object value) {
		if (null != value) {
			req.setAttribute(key, value);
		} else {
		  //req.removeAttribute(key); // Shouldn't be necessary, if we're conservative. We shouldn't encourage setting then removing attributes
		}
	}
	
	public static String[] doAuthorization(final HttpServletRequest req) {
		final String authorization = req.getHeader("Authorization");
		final String[] values;// = null;
		if (null != authorization) {
			String base64Credentials = authorization.substring("basic".length()).trim();
			byte[] decoded = Base64.getDecoder().decode(base64Credentials);
			String credentials = new String(decoded, StandardCharsets.UTF_8);
			/* final String[] */values = credentials.split(":", 2);
			/* final */String user = req.getRemoteUser();
			if (null != user) {
				values[0] = user;
			}
		} else {
			values = null;
		}
		return values;
	}

	public static String getSessionId(final HttpServletRequest req, final HttpServletResponse resp) {
	  /*final */String session_id = req.getParameter("session-id");
		{
		  /*final */HttpSession session = null;
			if (null == session_id) {
				session = req.getSession(/*true*/);
			  /*final String */session_id = (String)session.getAttribute("session-id");
				if (null == session_id) {
				  /*final String */session_id = ServletTool.generateString(64);
					session.setAttribute("session-id", session_id);
				  //session.removeAttribute("session-id");
				}
			} else {
				session = req.getSession(false);
				if (null != session) {
					session.invalidate();
				}
			}
		}
		return session_id;
	}
	public static java.util.Map<String, Object>getSessionById(final String session_id) {
		return (java.util.Map<String, Object>)Sessions.getSessionById(session_id);
	}
	
  /*public static String href(*//* final *//*String href, final String append) {
		if (null == href || 0 == href.trim().length()) {
			href = append;
		} else {
			href = href + "&" + append;
		}
		return href;
	}*/
	
	public static void put(final HttpSession session, final String name, final Object value) {
		session.setAttribute(name, value);
	}
	public static Object get(final HttpSession session, final String name) {
		return session.getAttribute(name);
	}
	public static void remove(final HttpSession session, final String name) {
		session.removeAttribute(name);
	}

	public static void put(final java.util.Map<String, Object> session, final String key, final Object value) {
		session.put(key, value);
	}
	public static Object get(final java.util.Map<String, Object> session, final String key) {
		return session.get(key);
	}
	public static Object remove(final java.util.Map<String, Object> session, final String key) {
		return session.remove(key);
	}
	
	public static final java.util.Map<java.lang.String, java.lang.String> getQueryStringAsKeyValueMap(final HttpServletRequest req) {
	  /*final */java.lang.String[] pairs = null;
		final String queryString = req.getQueryString();
		if (null != queryString) {
			pairs = queryString.split("&");
		}
		final java.util.Map<java.lang.String, java.lang.String> map = new LinkedHashMap<>();
		if (null != pairs) {
			/* final */java.util.List<String> __redirect = null;
			for (final java.lang.String pair : pairs) {
				final java.lang.String[] split = pair.split("=", 2);
				if (1 == split.length) {
					map.put(split[0], "");
				} else {
					map.put(split[0], split[1]);
				}
				if (null == __redirect && true == split[0].equals("redirect")) {
					__redirect = new java.util.ArrayList<String>();
					__redirect.add(split[1]);
				} else if (null != __redirect) {
					if (1 == __redirect.size()) {
						__redirect.add("?");
					} else {
						__redirect.add("&");
					}
					__redirect.add(pair);
				}
				if (null != __redirect) {
					map.put("redirect", "");
					for (final String s : __redirect) {
						map.put("redirect", map.get("redirect") + s);
					}
				}
			}
		}
		return map;
	}
}