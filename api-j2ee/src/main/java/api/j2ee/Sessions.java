package api.j2ee;

import java.text.SimpleDateFormat;

public class Sessions {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
  /*private static final dao.Context context;*/// = null;
  //private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> sessions;// = null;

	static {
	  /*try {
			Properties.properties(new Properties(Sessions.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	///*context = */new dao.Context();
	  /*final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> */sessions = new java.util.HashMap<String, java.util.Map<String, Object>>(){
			private static final long serialVersionUID = 0L;
		};
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getSessionById(final java.lang.String session_id) {
		return sessions.get(session_id);
	}
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> session = null;
		for (final String session_id: sessions.keySet()) {
			final java.util.Map<java.lang.String, java.lang.Object> __token = sessions.get(session_id);
			if (true == id.equals(__token.get("$id").toString())) {
				session = __token;
				break;
			}
		}
		return session;
	}
	
	private static /*final */java.lang.Thread thread;// = null;
	private static final java.util.Map<String, Object> service = new java.util.HashMap<String, Object>();
	
	public static void startup() {
		final java.util.Map<String, Object> __service = service;
		Sessions.service.put("starting", sdf.format(new java.util.Date()));
		try {
			Sessions.thread = new java.lang.Thread(){
				{
					__service.put("state", 1);
					__service.put("state", 2);
				}
				@Override
				public void run() {
					__service.put("started", sdf.format(new java.util.Date()));
					__service.remove("starting");
					while (null == __service.get("stopping")) {
						__service.put("state", 3);
						try {
							for (final String key: Sessions.sessions.keySet()) {
								final java.util.Map<String, Object> map = Sessions.sessions.get(key);
								final Boolean expired = (Boolean)map.get("$expired");
								if (null == expired || false == expired) {
									final Long ms = (Long)map.get("$date+time");
									if (ms > System.currentTimeMillis()-60L*1000L/*seconds*/) {
									  //final String type = (String)map.get("$type");
										map.put("$expired", Boolean.TRUE);
									}
								} else {
									final Long ms = (Long)map.get("$date+time");
									if (ms > System.currentTimeMillis()-60L*1000L/*seconds*/*10L/*minutes*/) {
									  //final String type = (String)map.get("$type");
										map.remove(key);
									}
								}
							}
							__service.put("last-check", sdf.format(new java.util.Date()));
							__service.put("state", 4);
						} catch (final Exception exc) {
							__service.put("state", -3);
							__service.put("error", exc.getMessage());
						} finally {
						}
						try {
							Thread.sleep(60*1000L);
						} catch (final InterruptedException e) {
							// Silently ignore..
						}
					}
					__service.put("stopped", sdf.format(new java.util.Date()));
					__service.remove("stopping");
				}
			};
			Sessions.thread.start();
		} catch (final Exception e) {
			__service.put("state", -1);
			__service.put("error", e.getMessage());
		}
	}
	
	public static void shutdown() {
		if (null != Sessions.thread) {
			Sessions.service.put("stopping", sdf.format(new java.util.Date()));
		}
	}
}