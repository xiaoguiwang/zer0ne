package api.j2ee;

@SuppressWarnings("unchecked")
public class Users {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> users;// = null;
	
	static {
		if (null == users) {
			users = new dto.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("admin", new dto.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("user-id", "admin");
							put("password", "1epinards");
							put("properties", new dto.util.HashMap<java.lang.String, java.lang.String>(){
								private static final long serialVersionUID = 0L;
								{
									put("http-stub-for-urls", "http://uat.itfromblighty");
									put("https-stub-for-urls", "http://uat.itfromblighty");
									put("signature-from-email-address", "do-not-email-us@itfromblighty");
								}
							});
							put("sq:customer#id", "");
							put("%cards", new dto.util.ArrayList<java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									add(new dto.util.HashMap<java.lang.String, java.lang.Object>(){
										private static final long serialVersionUID = 0L;
										{
										  //put("sq:nonce", "");
											put("sq:card#id", "");
											put("obfuscated", ""); // XXXX-XXXX-XXXX-1234
											put("nickname", "");
										}
									});
								}
							});
						}
					});
					put("arussell", new dto.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("user-id", "arussell");
							put("preferred-name", "Alex");
							put("email", "alex@itfromblighty");
							put("confirmed-email", true);
							put("sms", "14039706104");
							put("confirmed-sms", true);
							put("password", "1epinards");
						}
					});
					put("rwang", new dto.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("user-id", "rwang");
							put("preferred-name", "Lixing");
							put("email", "wanglixing@gmail.com");
							put("confirmed-email", true);
							put("sms", "15878882808");
							put("confirmed-sms", false);
							put("password", "Pas5w0rd");
						}
					});
					
				}
			};
		}
	}

	// SquareUp specific implementation (move out to an adapter custom at some point)
	public static String getCustomerId() {
		throw new IllegalStateException("support via hci");
	}
	// SquareUp specific implementation (move out to an adapter custom at some point)
	// "cnon:card-nonce-ok"
	public static String getCustomerCardId(final String /*customer_*/id, final String /*customer_*/card_nonce) {
		throw new IllegalStateException("support via hci");
	}
	
	public static java.util.Map<String, Object> get(final Integer id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> user = null;
		for (final java.lang.String user_id: users.keySet()) {
			final java.util.Map<java.lang.String, java.lang.Object> __user = users.get(user_id);
			if (true == id.equals(__user.get("$id"))) {
				user = __user;
				break;
			}
		}
		return user;
	}
	public static java.util.Map<java.lang.String, java.lang.Object> get(final java.lang.String id) {
		return Users.getId(id);
	}
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> user = null;
		for (final java.lang.String user_id: users.keySet()) {
			final java.util.Map<java.lang.String, java.lang.Object> __user = users.get(user_id);
			if (true == id.equals(__user.get("$id").toString())) {
				user = __user;
				break;
			}
		}
		return user;
	}
	public static java.util.Map<java.lang.String, java.lang.Object> findUser(final java.lang.String bySign) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> user = null;
		if (null != bySign && 0 < bySign.trim().length()) {
			for (final java.lang.String user_id: users.keySet()) {
if (true == user_id.equals("$id")) continue;
			  /*final */java.util.Map<java.lang.String, java.lang.Object> __user = users.get(user_id);
				if (true == __user.get("user-id").equals(bySign)) {
					user = __user;
					break;
				} else if (null != __user.get("email") && true == __user.get("email").equals(bySign)) {
					user = __user;
					break;
				} else if (null != __user.get("sms") && true == __user.get("sms").equals(bySign)) {
					user = __user;
					break;
				}
			}
		}
		return user;
	}
}