package api.j2ee.transform;

import dro.util.Stack;
import dto.util.Stream;
import dto.util.Stream.Callback;

public class ServiceLineHistoryFilter extends Stream.Callback {
	protected Stack<Integer> state = new Stack<Integer>().push(0);
	protected java.util.Date date_from;// = null;
	protected java.util.Date date_to;
	{
		date_from = null;
		date_to = null;
	}
	@Override
	public Callback configuration(final java.util.Map<String, Object> configuration) {
		super.configuration(configuration);
		if (true == configuration.containsKey("date-from")) {
			this.date_from = (java.util.Date)configuration.get("date-from");
		}
		if (true == configuration.containsKey("date-to")) {
			this.date_to = (java.util.Date)configuration.get("date-to");
		}
		return this;
	}
	@Override
	public Callback configure(final java.lang.String key, java.lang.Object value) {
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == "phase".equals(key)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "start".equals(value)) {
				this.state.push(this.state.peek()+1);
			} else if (true == "finish".equals(value)) {
				this.state.pop();
			} else {
			}
		} else {
		}
		return this;
	}
	@Override
	public java.lang.Object process(final java.lang.Object o) {
	  /*final */boolean result = true;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (0 < this.state.peek()) {
			if (o instanceof String) {
				if (true == "%consumed".equals((String)o) || true == "%charged".equals((String)o)) {
					this.state.push(-this.state.pop());
				}
			}
		} else if (0 > this.state.peek()) {
		  /*final */Long ms = null;
			if (o instanceof String) {
				try {
					ms = new Long((String)o);
				} catch (final NumberFormatException e) {
				  //throw new RuntimeException();
				}
			} else if (o instanceof Long) {
				ms = (Long)o;
			}
			if (null != ms && null != this.date_from && null != this.date_to) {
				if (false == dto.util.Date.inDateRange(this.date_from, ms, this.date_to, ms)) {
					result = false;
				}
			}
		} else {
		}
		return result;
	}
}