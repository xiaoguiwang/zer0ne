package api.j2ee.transform;

import java.text.SimpleDateFormat;
import java.util.TreeSet;

import dto.util.Stream.Callback;

public class ServiceLineHistoryFilterAggregate extends ServiceLineHistoryFilter {
  //private static final SimpleDateFormat sdf23 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static final SimpleDateFormat sdf   = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private Long date_interval_in_ms;// = null;
	private Integer interval;// = null;
	private java.lang.Double[] totals;// = null;
	{
		date_interval_in_ms = null;
		interval = null;
		totals = null;
	}
	@Override
	public Callback configuration(final java.util.Map<String, Object> configuration) {
		super.configuration(configuration);
		if (true == configuration.containsKey("date-interval-in-ms")) {
			this.date_interval_in_ms = (Long)configuration.get("date-interval-in-ms");
		}
		return this;
	}
	@Override
	public java.lang.Object translate(final java.lang.Object o) {
	  /*final */java.lang.Object __o = null;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (0 < this.state.peek()) {
			if (o instanceof String) {
				if (true == "%consumed".equals((String)o) || true == "%charged".equals((String)o)) {
					super.state.push(-this.state.pop());
				}
			}
			__o = o;
		} else if (0 > super.state.peek()) {
			if (o instanceof java.util.Map) {
				@SuppressWarnings("unchecked")
				final java.util.Map<Object, Object> map = (java.util.Map<Object, Object>)o;
				final java.util.Map<Object, Object> msVsUnits = new java.util.HashMap<Object, Object>();
				for (final java.util.Map.Entry<Object, Object> e: map.entrySet()) {
					final java.lang.Object key = e.getKey(), value = e.getValue();
					if (null == this || true == (Boolean)this.process(key)) {
						final java.lang.Object __key = null == this ? key : this.translate(key);
						final java.lang.Object __value = null == this ? value : this.translate(value);
					  /*jo.put(
							dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(key))).transform(__key, sic, Source.Native),
							dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(value))).transform(__value, sic, Source.Native)
						);*/
						msVsUnits.put(__key, __value);
					}
				}
				final java.util.Map<String, java.util.List<Object>> __map = new dto.util.HashMap<String, java.util.List<Object>>();
				final java.util.List<Object> msList = new java.util.ArrayList<>();
				final java.util.List<Object> unitsList = new java.util.ArrayList<>();
				for (final Object key: new TreeSet<>(msVsUnits.keySet())) {
					final Object value = msVsUnits.get(key);
					final String __key = sdf.format(new java.util.Date(((Long)key).longValue()));
					msList.add(__key);
					unitsList.add(value);
				}
				__map.put("ms", msList);
				__map.put("units", unitsList);
				__o = __map;
			} else {
			  /*final */Long ms = null;
				if (o instanceof String) {
					try {
						ms = new Long((String)o);
					} catch (final NumberFormatException e) {
					  //throw new RuntimeException();
					}
				} else if (o instanceof Long) {
					ms = (Long)o;
				}
				if (o instanceof String || o instanceof Long) {
					if (null != ms && null != super.date_from && null != this.date_to) {
						long __date_from = super.date_from.getTime();
						long __date_to = super.date_to.getTime();
						if (null == this.totals) {
							int intervals = 0;
							long __date = __date_from;
							while (__date+this.date_interval_in_ms <= __date_to) {
								intervals++;
								__date += this.date_interval_in_ms;
							}
							this.totals = new java.lang.Double[intervals];
						}
						{
							int interval = 0;
							long __date = __date_from;
							while (__date+this.date_interval_in_ms <= ms && __date+this.date_interval_in_ms <= __date_to) {
								interval++;
								__date += this.date_interval_in_ms;
							}
							if (0 <= interval && interval < this.totals.length) {
								this.interval = interval;
							} else {
							  //throw new IllegalStateException(); // Now that things are aggregating properly we can switch to this.interval=null (below) as this can also happen when we are outside of time boundaries
								this.interval = null;
							}
							if (o instanceof String) {
								__o = new Long(__date).toString();
							} else if (o instanceof Long) {
								__o = new Long(__date);
							}
						}
					} else {
						__o = o;
					}
				} else if (o instanceof Float || o instanceof Double) {
					if (null != this.totals && null != this.interval) {
						if (null == this.totals[this.interval]) {
							this.totals[this.interval] = 0.0/*d*/; // i.e. double
						}
						double total = this.totals[this.interval];
						if (o instanceof Float) {
							final float units = ((Float)o).floatValue();
							total += (double)units;
						} else if (o instanceof Double) {
							final double units = ((Double)o).doubleValue();
							total += units;
						}
						this.totals[this.interval] = total;
						if (o instanceof Float) {
							__o = new Double(total).floatValue();
						} else if (o instanceof Double) {
							__o = o;
						}
					} else {
						__o = o;
					}
				} else {
					__o = o;
				}
			}
		} else {
			__o = o;
		}
		return __o;
	}
}