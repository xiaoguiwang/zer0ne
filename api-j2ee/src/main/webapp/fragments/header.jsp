<!-- fragments/header.jsp -->
<%@ page import="api.j2ee.ServletTool" %>
<%@ page import="api.j2ee.Users" %>
<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = ServletTool.getSessionById(session_id);
	final java.util.Map<String, Object> user = null == __session ? null : (java.util.Map<String, Object>)__session.get("user");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta property="og:title" content="IT From Blighty"/><!--  Note: only og:title and og:image supported by Apple -->
<meta property="og:image" content="https://www.itfromblighty:8443/hci/images/image.jpg"/><!-- TODO: substitute. Note: must be .jpg for iPhone and iOS 10+ -->
<%
  /*final */String meta_http_equiv = null == request.getAttribute("meta-http-equiv") ? "" : (String)request.getAttribute("meta-http-equiv");
  /*final */boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	final String url = request.getRequestURL().toString();
	if (0 == meta_http_equiv.trim().length() && (true == url.endsWith("/hci/") || true == url.endsWith("/hci/index.jsp"))) {
		meta_http_equiv = "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index'\" />";
		redirecting = true;
	} // else { .. } // Are there other scenarios to benefit?
%>
<%= meta_http_equiv %>
<% 	final String title = null == request.getAttribute("$html$head$title") ? "{untitled}" : (String)request.getAttribute("$html$head$title"); %>
<title><%= title %></title>
<link rel="stylesheet" type="text/css" href="/hci/css/index.css?<%= ServletTool.getVersion() %>">
<style>
<% {
	final String font_family = null == request.getParameter("font-family") ? "Lucida Console, monospace" : request.getParameter("font-family");
	final String font_size   = null == request.getParameter("font-size"  ) ? "20px" : request.getParameter("font-size");
	final String font_weight = null == request.getParameter("font-weight") ? "bold" : request.getParameter("font-weight");
%>
td { position: relative; <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
body { font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
input { font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
select { text-align-last: center; padding-left: 29px; } font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
textarea { resize: none; font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
<% } %>
</style>
<script src="/hci/js/index.js?<%= ServletTool.getVersion() %>"></script>
</head>
<body onload='bodyOnLoad("<%= session_id %>")'>
<!-- table .. style="table-layout:fixed" .. -->
<table border=0 id="id#table-1" cellpadding=10 cellspacing=0 height="100%" width="100%" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
<tr valign="middle">
<td align="center">
<table border=0 id="id#table-2" cellpadding=0 cellspacing=4 height="100%" width="100%">
<tr valign="middle">
<td align="center">
<table border=0 id="id#table-3" cellpadding=4 cellspacing=0 height="100%" width="100%">
<tr valign="middle">
<td align="center">

<table border=0 cellpadding=0 cellspacing=0 height="100%" width="100%">
<tr height="1%">
<td>
<table border=0>
<tr valign="top">
<td nowrap width="1%">
<% 	if (false == redirecting) { %>
<a href="/hci/index?session-id=<%= session_id %>">home</a>
<% 		if (null != user) {
		final java.util.Map<String, Object> self = null == __session ? null : (java.util.Map<String, Object>)__session.get("user"); %>
<br/>
<a href="/hci/user?session-id=<%= session_id %>">user</a>/<% if (true == true/*Users.hasPermission(self, "any-user-read")*/) { %><a href="/hci/users?session-id=<%= session_id %>"><% } %>s<% if (true == true/*Users.hasPermission(self, "any-user-read")) { %></a><% } %> (<% if (true == Users.hasPermission(self, "any-user-create")) { %><a href="/hci/user?session-id=<%= session_id %>&user-mode=create"><% } %>new<% if (true == true/*Users.hasPermission(self, "any-user-create")*/) { %></a><% } %>)
<br/>
<a href="/hci/bills?session-id=<%= session_id %>">bills</a>/<a href="/hci/bills?history&session-id=<%= session_id %>">history</a>  (<a href="/hci/bill?session-id=<%= session_id %>&bill-mode=create">new</a>)
<br/>
<a href="/hci/addresses?session-id=<%= session_id %>">addresses</a> (<a href="/hci/address?session-id=<%= session_id %>&address-mode=create">new</a>)
<br/>
<a href="/hci/groups?session-id=<%= session_id %>">groups</a> (<a href="/hci/group?session-id=<%= session_id %>&group-mode=create">new</a>)
<br/>
<a href="/hci/roles?session-id=<%= session_id %>">roles</a> (<a href="/hci/role?session-id=<%= session_id %>&role-mode=create">new</a>)
<br/>
<a href="/hci/permissions?session-id=<%= session_id %>">permissions</a> (<a href="/hci/permission?session-id=<%= session_id %>&permission-mode=create">new</a>)
<br/>
<a href="/hci/texts?session-id=<%= session_id %>">texts</a>/<a href="/hci/texts?history&session-id=<%= session_id %>">history</a> (<a href="/hci/text?session-id=<%= session_id %>&text-mode=create">new</a>)
<br/>
<a href="/hci/emails?session-id=<%= session_id %>">e-mails</a>/<a href="/hci/emails?history&session-id=<%= session_id %>">history</a> (<a href="/hci/email?session-id=<%= session_id %>&email-mode=create">new</a>)
<br/>
<a href="/hci/services?session-id=<%= session_id %>">services</a>/<a href="/hci/services?history&session-id=<%= session_id %>">history</a> (<a href="/hci/service?session-id=<%= session_id %>&service-mode=create">new</a>)
<br/>
<a href="/hci/products?session-id=<%= session_id %>">products</a> (<a href="/hci/product?session-id=<%= session_id %>&product-mode=create">new</a>)
<% 		} else { %>
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<% 		} %>
<% 	} else { %>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<br/>
&nbsp;
<% } %>
</td>
<td nowrap width="98%">
</td>
<td nowrap width="1%">
<% 	if (false == redirecting) {
		if (null == user && false == url.endsWith("/hci/sign-up")) { %>
<a href="/hci/sign-up?step=0&session-id=<%= session_id %>">sign-up</a>
<% 		} else { %>
sign-up
<% 		} %>
|
<% 		if (null == user) {
			final String sign_in_method = null == __session ? "default" : null == (String)__session.get("sign-in-method") ? "default" : (String)__session.get("sign-in-method");
			final String alternative_sign_in_method = null == __session ? "html-form" : null == __session.get("alternative-sign-in-method") ? "html-form" : (String)__session.get("alternative-sign-in-method");
			if (false == sign_in_method.equals("basic-auth")) { %>
<a href="/hci/sign-in?step=0&session-id=<%= session_id %>&sign-in-method=<%= sign_in_method %>">sign-in</a> (method=&quot;<% if (false == url.endsWith("/hci/sign-in")) { %><a href="<%= url %>?session-id=<%= session_id %>&alternative-sign-in-method=<%= alternative_sign_in_method %>"><% } %><%= sign_in_method %><% if (false == url.endsWith("/hci/sign-in")) { %></a><% } %>&quot;)
<% 			} else { %>
<a href="/hci/index?session-id=<%= session_id %>&sign-in-method=<%= sign_in_method %>">sign-in</a> (method=&quot;<a href="<%= url %>?session-id=<%= session_id %>&alternative-sign-in-method=<%= alternative_sign_in_method %>"><%= sign_in_method %></a>&quot;)
<% 			}
		} else {
			final String signed_in_method = (String)__session.get("signed-in-method"); %>
sign-in (method=&quot;<%= signed_in_method %>&quot;)
<% 		} %>
|
<% 		if (null != user) { %>
<a href="/hci/sign-out?step=0&session-id=<%= session_id %>">sign-out</a> (<a href="/hci/user?session-id=<%= session_id %>"><%= (String)user.get("user-id") %></a>)
<% 		} else { %>
sign-out
<% 		}
	} %>
</td>
</tr>
</table>
</td>
</tr>

<tr height="98%">
<td id="id#td" align="center">
<% } %>