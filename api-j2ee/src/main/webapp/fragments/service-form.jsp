<!-- fragments/service-form.jsp -->
<!-- services list e.g. e-mail, sms, and state i.e. started/stopped -->
<% if (null == request.getAttribute("service#history")) {
	final String session_id = (String)request.getAttribute("session-id");
	final Object o = request.getAttribute("service");
  /*final */java.util.Map<String, Object> service = null;
	if (true == o instanceof Boolean) {
	} else {
	  /*final java.util.Map<String, Object> */service = (java.util.Map<String, Object>)o;
	}
	final Integer service_id = null == service ? null : (Integer)service.get("$id");
	final Boolean /*service_*/history = (Boolean)request.getAttribute("service@history");
	final String mode = (String)request.getAttribute("mode");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create service:
<% 	} else if (false == "delete".equals(mode)) { %>
Update service:
<% 	} else { %>
Delete service:
<% 	} %>
<br/>
<br/>
<% if (null != request.getAttribute("cancel")) { %><input name='cancel' type='hidden' value='<%= request.getAttribute("cancel") %>'/><% } %>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<% 	{
		final String key = "service-id";
		final String value = null == service ? null : (String)service.get(key); %>
<td width='44%' align='right' nowrap><%= key %>: </td>
<td width='12%' align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>'/></td>
<td width='44%' align='left'><input name='id' type='hidden' value='<%= null == service_id ? "" : service_id %>'/></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "service:description";
		final String label = "description";
		final String value = null == service ? null : (String)service.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value.replaceAll("'", "&apos;") %>'/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "service:$state";
		final String label = "$state";
		final String value = null == service ? null : (String)service.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<!--td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td-->
<td align='left' nowrap>
<%= null == value ? "{null}" : 0 == value.trim().length() ? "{blank}" : value %>
<% 		final java.util.List<String> states = api.j2ee.Services.getNextStates(value);
		if (null != states && 0 < states.size()) {
			for (final String state: states) { %>
=&gt; <input id='id#state:<%= state %>' name='__submit' type='submit' value='state:<%= state %>'/>
<br/>
<% 			}
		} %>
</td>
<% 	} %>
<td></td>
</tr>
</table>
<br/>
<% 	if (false == "create".equals(mode)) { %>
<a href="/hci/service?line&session-id=<%= session_id %>&id=<%= service_id %>&mode=create&cancel=service">create line</a>
|
<% 		if (null == /*service_*/history) { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= service_id %>&mode=create&cancel=service">create history</a>
<% 		} else { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= service_id %>&cancel=service">history</a>
<% 		}
	} %>
|
<% 	if (true == "create".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
<% 	} else { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	} %>
<% 	if (null != request.getAttribute("cancel")) { %>
|
<% 		if (true == "services".equals((String)request.getAttribute("cancel"))) { %>
<a href="/hci/services?session-id=<%= session_id %>">cancel</a>
<% 		} else { %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 		}
	} %>
<% } else { %>
<%@include file='/fragments/service-form-history.jsp' %>
<% } %>