<!-- fragments/service-line-form.jsp -->
<!-- service line e.g. 1 e-mail and unit price, 5 e-mails and bulk price -->
<% if (null == request.getAttribute("service#line#history")) {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
  /*final */Integer id = null;
  /*final */Integer service_id = null;
  /*final */java.util.Map<String, Object> /*service_*/line = null;
	final String mode = (String)request.getAttribute("mode");
	final Object o = request.getAttribute("service#line");
	if (null != o) {
		if (true == o instanceof Boolean) {
		} else {
		  /*final java.util.Map<String, Object> service_*/line = (java.util.Map<String, Object>)o;
		}
		if (true == "create".equals(mode)) {
		  /*final Integer */id = null == service ? null : (Integer)service.get("$id");
		} else {
		  /*final Integer */service_id = /*null == service ? null : */(Integer)service.get("$id");
		  /*final Integer */id = null == /*service_*/line ? null : (Integer)/*service_*/line.get("$id");
		}
	}
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create service line:
<% 	} else if (false == "delete".equals(mode)) { %>
Update service line:
<% 	} else { %>
Delete service line:
<% 	} %>
<br/>
<br/>
<input name='line' type='hidden'/>
<% 	if (null != service_id) { %><input name='service-id' type='hidden' value='<%= service_id %>'/><% } %>
<input name='id' type='hidden' value='<%= id %>'/>
<% 	if (null != request.getAttribute("cancel")) { %><input name='cancel' type='hidden' value='<%= request.getAttribute("cancel") %>'/><% } %>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<% 	{
		final String key = "service-id";
		final String value = null == service ? null : (String)service.get(key); %>
<td width='44%' align='right' nowrap><%= key %>: </td>
<td width='12%' align='center' nowrap><input id='id#<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td width='44%' align='left'></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = "service:line-id";
		final String label = "line-id";
		final String value = null == /*service_*/line ? "" : (String)/*service_*/line.get(label); %>
<td width='44%' align='right' nowrap><%= label %>: </td>
<td width='12%' align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>'/></td>
<td width='44%' align='left'></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "service:line:description";
		final String label = "description";
		final String value = null == /*service_*/line ? "" : (String)/*service_*/line.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value.replaceAll("'", "&apos;") %>'/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "service:line:$state";
		final String label = "$state";
		final String value = null == /*service_*/line ? "" : (String)/*service_*/line.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<!--td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td-->
<td align='left' nowrap>
<%= null == value ? "{null}" : 0 == value.trim().length() ? "{blank}" : value %>
<% 		final java.util.List<String> states = api.j2ee.Services.getNextStates(value);
		if (null != states && 0 < states.size()) {
			for (final String state: states) { %>
=&gt; <input id='id#state:<%= state %>' name='__submit' type='submit' value='state:<%= state %>'/>
<br/>
<% 			}
		} %>
</td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "service:line:unit-of-measure";
		final String label = "unit-of-measure";
		final String value = null == /*service_*/line ? "" : (String)/*service_*/line.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>'/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "service:line:price-per-unit";
		final String label = "price-per-unit";
		final Float value = null == /*service_*/line ? null : (Float)/*service_*/line.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : new java.text.DecimalFormat("#,##0.00").format(value) %>'/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
</table>
<br/>
<% 	if (true == "create".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
<% 	} else { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	} %>
|
<% 	if (true == "service".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/service?session-id=<%= session_id %>&id=<%= null != service_id ? service_id : id %>">cancel</a>
<% 	} else if (true == "service-lines".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/service?lines&session-id=<%= session_id %>&id=<%= null != service_id ? service_id : id %>">cancel</a>
<% 	} else { %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 	} %>
<% } else { %>
<%@include file='/fragments/service-line-form-history.jsp' %>
<% } %>