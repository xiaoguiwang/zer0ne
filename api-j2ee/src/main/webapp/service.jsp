<!-- service.jsp -->
<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<% {
  /*final */String jsp = "IT From Blighty [service.jsp]";
	final String[] split = request.getRequestURL().toString().split("/");
  /*final */int i = split.length;
	if (null == split[i-1] || 0 == split[i-1].trim().length()) {
		i = i-1;
	}
	if (true == split[i-1].startsWith("service")) {
	  /*final String */jsp = "IT From Blighty ["+split[i-1]+"]";
	} else {
	  /*final String */jsp = "IT From Blighty..";
	}
	request.setAttribute("$html$head$title", jsp);
} %>
<%@ include file='/fragments/header.jsp' %>
<% {
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	if (true == redirecting) { %>
<h1>IT From Blighty</h1>
<% 	} else {
		{
			final String session_id = (String)request.getAttribute("session-id");
		  /*final */String __lines = null;
		  /*final */String __line = null;
		  /*final */String __history = null;
			if (null == request.getParameter("for-line-id")) {
			  /*final String */__lines = request.getParameter("lines");
			  /*final String */__line = request.getParameter("line");
			  /*final String */__history = request.getParameter("history");
			}
		  /*final */Integer __service_id = null;
		  /*final */Integer __history_id = null;
		  /*final */Integer __line_id = null;
		  /*final */Integer __id = null;
		  //if (null != __service) {
				if (null != __lines) {
					if (null != __history) {
						final Object o = request.getAttribute("service#lines#history");
						if (null != o) {
							if (true == o instanceof Boolean) {
							} else {
								final java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)request.getAttribute("service#history");
								__id = (Integer)/*service_*/history.get("$id");
							}
						}
					} else {
						final Object o = request.getAttribute("service#lines");
						if (null != o) {
							if (true == o instanceof Boolean) {
							} else {
								final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
								__id = (Integer)service.get("$id");
							}
						}
					}
					__lines = "lines";
				} else {
					if (null != __line) {
						if (null != __history) {
							final Object o = request.getAttribute("service#line#history");
							if (null != o) {
								if (true == o instanceof Boolean) {
								} else {
									final java.util.Map<String, Object> /*service_*/line_history = (java.util.Map<String, Object>)o;
									__id = (Integer)/*service_*/line_history.get("$id");
								  //final java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)request.getAttribute("service#history");
								  //__history_id = (Integer)/*service_*/history.get("$id");
								}
							}
						} else {
							final Object o = request.getAttribute("service#line");
							if (null != o) {
								if (true == o instanceof Boolean) {
								} else {
									final java.util.Map<String, Object> /*service_*/line = (java.util.Map<String, Object>)o;
									__id = (Integer)/*service_*/line.get("$id");
								  //final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
								  //__service_id = (Integer)/service.get("$id");
								}
							}
						}
					} else {
						if (null != __history) {
							final Object o = request.getAttribute("service#history");
							if (null != o) {
								if (true == o instanceof Boolean) {
								} else {
									final java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)o;
									__id = (Integer)/*service_*/history.get("$id");
								  //final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
								  //__service_id = (Integer)/service.get("$id");
								}
							}
						} else {
							final Object o = request.getAttribute("service");
							if (null != o) {
								if (true == o instanceof Boolean) {
								} else {
									final java.util.Map<String, Object> service = (java.util.Map<String, Object>)o;
									__id = (Integer)service.get("$id");
								}
							}
						}
					}
				}
		  //}
			final String uri = /*null == request.getAttribute("uri") ? */request.getRequestURI().toString()/* : (String)request.getAttribute("uri")*/;
			final String qs_lines = null == __lines ? "" : "lines&";
			final String qs_line = null == __line ? "" : "line&";
			final String qs_history = null == __history ? "" : "history&";
			final String __mode = (String)request.getParameter("mode");
			final String qs_mode = null == __mode ? "" : "&mode="+__mode;
			final String qs_service_id = null == __service_id ? "" : "&service-id="+__service_id;
			final String qs_line_id = null == __line_id ? "" : "&line-id="+__line_id;
			final String qs_history_id = null == __history_id ? "" : "&history-id="+__history_id;
			final String qs_id = null == __id ? "" : "&id="+__id;
			final String __cancel = (String)request.getParameter("cancel");
			final String qs_cancel = null == __cancel ? "" : "&cancel="+__cancel; %>
<form id='id#form-service' name='form-service' method='POST' action='/hci/service?<%= qs_lines %><%= qs_line %><%= qs_history %>session-id=<%= session_id %><%= qs_service_id %><%= qs_line_id %><%= qs_history_id %><%= qs_id %><%= qs_mode %><%= qs_cancel %>'>
<% 		}
		if (null != request.getAttribute("service#line#history")) { %>
<%@include file='/fragments/service-line-form-history.jsp' %>
<% 		} else 
		if (null != request.getAttribute("service#line")) { %>
<%@include file='/fragments/service-line-form.jsp' %>
<% 		} else
		if (null != request.getAttribute("service#lines#history")) { %>
<%@include file='/fragments/service-lines-form-history.jsp' %>
<% 		} else
		if (null != request.getAttribute("service#lines")) { %>
<%@include file='/fragments/service-lines-form.jsp' %>
<% 		} else
		if (null != request.getAttribute("service#history")) { %>
<%@include file='/fragments/service-form-history.jsp' %>
<% 		} else
		if (null != request.getAttribute("service")) { %>
<%@include file='/fragments/service-form.jsp' %>
<% 		} %>
</form>
<% 	}
} %>
<%@include file='/fragments/footer.jsp' %>