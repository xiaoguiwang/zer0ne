<!-- services.jsp -->
<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<% {
  /*final */String jsp = "IT From Blighty [services.jsp]";
	final String[] split = request.getRequestURL().toString().split("/");
  /*final */int i = split.length;
	if (null == split[i-1] || 0 == split[i-1].trim().length()) {
		i = i-1;
	}
	if (true == split[i-1].startsWith("services")) {
	  /*final String */jsp = "IT From Blighty ["+split[i-1]+"]";
	} else {
	  /*final String */jsp = "IT From Blighty..";
	}
	request.setAttribute("$html$head$title", jsp);
} %>
<%@ include file='/fragments/header.jsp' %>
<% {
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	if (true == redirecting) { %>
<h1>IT From Blighty</h1>
<% 	} else {
		{
			final String session_id = (String)request.getAttribute("session-id");
			final String __service_id = (String)request.getAttribute("service-id");
			final String qs_service_id = null == __service_id ? "" : "&service-id="+__service_id;
			final String __lines = (String)request.getAttribute("lines");
			final String qs_lines = null == __lines ? "" : "lines&";
			final String __history = (String)request.getAttribute("history");
			final String qs_history = null == __history ? "" : "history&"; %>
<form id="id#form-services" name="form-services" method="POST" action="/hci/services?session-id=<%= session_id %><%= qs_lines %><%= qs_history %><%= qs_service_id %>">
<% 		} %>
<%@include file='/fragments/services-form.jsp' %>
</form>
<% 	}
} %>
<%@include file='/fragments/footer.jsp' %>