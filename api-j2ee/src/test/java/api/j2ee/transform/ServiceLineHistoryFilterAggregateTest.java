package api.j2ee.transform;

import java.util.Calendar;
import java.util.TimeZone;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import api.j2ee.Services;
import dto.util.Stream;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServiceLineHistoryFilterAggregateTest {
	@Test
	public void test_0000() {
		final java.util.Date __date_from;// = null;
		{
			final Calendar c = Calendar.getInstance();
			c.setTimeZone(TimeZone.getTimeZone("UTC"));
		  //c.add(Calendar.MONTH, 0);
			final int y = c.get(Calendar.YEAR);
			final int m = c.get(Calendar.MONTH);
			c.clear();
			c.set(y, m, 1);
		  /*final java.util.Date */__date_from = c.getTime();
		}
		final java.util.Date __date_to;// = null;
		{
			final Calendar c = Calendar.getInstance();
			c.setTimeZone(TimeZone.getTimeZone("UTC"));
			final int y = c.get(Calendar.YEAR);
			c.add(Calendar.MONTH, 1);
			final int m = c.get(Calendar.MONTH);
			c.clear();
			c.set(y, m, 1);
		  /*final java.util.Date */__date_to = c.getTime();
		}
		
		final Stream.interface_Callback sic = new ServiceLineHistoryFilterAggregate()
			.configuration(new java.util.HashMap<String, Object>(){
				private static final long serialVersionUID = 1L;
				{
					put("date-from", __date_from);
					put("date-to", __date_to);
					put("date-interval-in-ms", new Long(1000L*60*60*24));
				}
			})
		;
		
	  /*final dto.util.HashMap<String, Object> /*service_*//*line_history = new dto.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("line-id", "whois");
				put("%charged", new dto.util.HashMap<Long, Float>(){
					private static final long serialVersionUID = 0L;
				*//*{
						final Calendar date = Calendar.getInstance();
						date.add(Calendar.MONTH, 0);
						
						date.set(Calendar.DATE, 1);
						put(date.getTimeInMillis()-1, new Float(1.0f));
						put(date.getTimeInMillis()+0, new Float(2.0f));
						put(date.getTimeInMillis()+1, new Float(3.0f));
						
						date.set(Calendar.DATE, 2);
						put(date.getTimeInMillis()-1, new Float(1.0f));
						put(date.getTimeInMillis()+0, new Float(2.0f));
						put(date.getTimeInMillis()+1, new Float(3.0f));
					}*//*
				});
			}
		};*/
		final java.util.List<java.util.Map<String, Object>> /*service_*/history = new dto.util.ArrayList<java.util.Map<String, Object>>(){
			private static final long serialVersionUID = 0L;
			{
				add(new dto.util.HashMap<String, Object>(){
					private static final long serialVersionUID = 0L;
					{
						put("service-history-id", Services.sequence.value(Services.sequence.value()+1).value());
						put("service-id", "health-check");
						put("for-user-id", "admin");
						put("%lines", new dto.util.HashMap<String, Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("whois", new dto.util.HashMap<String, Object>(){
									private static final long serialVersionUID = 0L;
									{
										put("line-id", "whois");
										put("%charged", new dto.util.HashMap<Long, Float>(){
											private static final long serialVersionUID = 0L;
										  /*{
												put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
											}*/
										});
									}
								});
								put("dns", new dto.util.HashMap<String, Object>(){
									private static final long serialVersionUID = 0L;
									{
										put("line-id", "dns");
										put("%charged", new dto.util.HashMap<Long, Float>(){
											private static final long serialVersionUID = 0L;
										  /*{
												put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
											}*/
										});
									}
								});
								put("ping", new dto.util.HashMap<String, Object>(){
									private static final long serialVersionUID = 0L;
									{
										put("line-id", "ping");
										put("%charged", new dto.util.HashMap<Long, Float>(){
											private static final long serialVersionUID = 0L;
										  /*{
												put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
											}*/
										});
									}
								});
								put("wget", new dto.util.HashMap<String, Object>(){
									private static final long serialVersionUID = 0L;
									{
										put("line-id", "wget");
										put("%charged", new dto.util.HashMap<Long, Float>(){
											private static final long serialVersionUID = 0L;
										  /*{
												put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
											}*/
										});
									}
								});
								put("openssl:s_client:443", new dto.util.HashMap<String, Object>(){
									private static final long serialVersionUID = 0L;
									{
										put("line-id", "openssl:s_client:443");
										put("%charged", new dto.util.HashMap<Long, Float>(){
											private static final long serialVersionUID = 0L;
										  /*{
												put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
											}*/
										});
									}
								});
							}
						});
					}
				});
			}
		};
		
		final Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("UTC"));
		final int y  = c.get(Calendar.YEAR);
		final int m = c.get(Calendar.MONTH);
		c.clear();
		c.set(y, m, 1);
		
		@SuppressWarnings("unchecked")
		final java.util.Map<String, Object>/*service_*/history_lines = (java.util.Map<String, Object>)/*service_*/history.get(0).get("%lines");
		Services.fakeCharges(c, /*service_*/history_lines, "whois", 60*8, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 60 minutes x 8 = 8 hours2
		Services.fakeCharges(c, /*service_*/history_lines, "dns", 60, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 60 minutes
		Services.fakeCharges(c, /*service_*/history_lines, "ping", 1, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 1 minute
		Services.fakeCharges(c, /*service_*/history_lines, "wget", 15, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 15 minutes
		Services.fakeCharges(c, /*service_*/history_lines, "openssl:s_client:443", 60*24, (Double)null, new Float(1.0f), new Double(-1.0+1000.0-1.0)); // Every 60 minutes x 24 hours = 1 day
		
		final String stream =  dto.Adapter.getStreamFromObject(/*service_*/history_lines, sic);
		System.out.println(stream);
	}
}