# readme.md

## https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

### List of HTTP status codes - 4xx client errors

- https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#4xx_client_errors

Implemented (for Wildfly) in web.xml

### List of HTTP status codes - 5xx server errors

- https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_server_errors

Implemented (for Wildfly) in web.xml

###### --T-H-E--E-N-D-- ######