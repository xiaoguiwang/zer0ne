# readme.md

# Tomcat:

# Note: this is in C:\apache-tomcat-8.5.65\bin\setenv.bat
SET "JPDA_OPTS=-agentlib:jdwp=transport=dt_socket,address=8787,server=y,suspend=n"
@REM SET "CATALINA_HOME=C:\apache-tomcat-8.5.65"
@REM %CATALINA_HOME%\bin\catalina.bat jpda start
@REM netstat -an|findstr 8787

%CATALINA_HOME%\bin\startup.bat
--OR--
%CATALINA_HOME%\bin\catalina.bat start

Change/Check {tomcat}/conf/catalina.properties
re. catalina.base/zer0ne/lib

Dependent JAR libraries:
(size guide)
    90,078 dao-zer0ne-0.0.1-SNAPSHOT.jar
 4,906,906 dao-sqlite-zer0ne-0.0.1-SNAPSHOT(-all).jar
 3,166,043 dao-mysql-zer0ne-0.0.1-SNAPSHOT(-all).jar
   445,056 dro-zer0ne-0.0.1-SNAPSHOT(-jar-with-dependencies).jar
    52,810 dto-zer0ne-0.0.1-SNAPSHOT.jar
 1,016,207 dto-mail-zer0ne-0.0.1-SNAPSHOT(-all).jar
 1,792,373 dto-http-zer0ne-0.0.1-SNAPSHOT(-all).jar
13,652,061 dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT(-all).jar

# https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-deploy-a-JAR-file-to-Tomcat-the-right-way

export TOMCAT=/c/apache-tomcat-8.5.65

cd ~/itfromblighty.ca/worktrees/zer0ne/release/0.0.1
cp dao/target/dao-zer0ne-0.0.1-SNAPSHOT.jar                               $TOMCAT/zer0ne/lib/dao-zer0ne-0.0.1-SNAPSHOT.jar
cp dao-sqlite/target/dao-sqlite-zer0ne-0.0.1-SNAPSHOT-all.jar             $TOMCAT/zer0ne/lib/dao-sqlite-zer0ne-0.0.1-SNAPSHOT.jar
cp dao-mysql/target/dao-mysql-zer0ne-0.0.1-SNAPSHOT-all.jar               $TOMCAT/zer0ne/lib/dao-mysql-zer0ne-0.0.1-SNAPSHOT.jar
cp dro/target/dro-zer0ne-0.0.1-SNAPSHOT-jar-with-dependencies.jar         $TOMCAT/zer0ne/lib/dro-zer0ne-0.0.1-SNAPSHOT.jar
cp dto/target/dto-zer0ne-0.0.1-SNAPSHOT.jar                               $TOMCAT/zer0ne/lib/dto-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-mail/target/dto-mail-zer0ne-0.0.1-SNAPSHOT-all.jar                 $TOMCAT/zer0ne/lib/dto-mail-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-http/target/dto-http-zer0ne-0.0.1-SNAPSHOT-all.jar                 $TOMCAT/zer0ne/lib/dto-http-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-cache-ignite/target/dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT-all.jar $TOMCAT/zer0ne/lib/dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT.jar
#
cp ../api-tomcat/target/api-tomcat-0.0.1-SNAPSHOT.war                        $TOMCAT/webapps/
#
cd -


## https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

### List of HTTP status codes - 4xx client errors

- https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#4xx_client_errors

Implemented (for Wildfly) in web.xml

### List of HTTP status codes - 5xx server errors

- https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_server_errors

Implemented (for Wildfly) in web.xml

###### --T-H-E--E-N-D-- ######