export TOMCAT=/c/apache-tomcat-8.5.65

cd ~/itfromblighty.ca/worktrees/zer0ne/release/0.0.1
#cp dao/target/dao-zer0ne-0.0.1-SNAPSHOT.jar                               $TOMCAT/zer0ne/lib/dao-zer0ne-0.0.1-SNAPSHOT.jar
#cp dao-sqlite/target/dao-sqlite-zer0ne-0.0.1-SNAPSHOT-all.jar             $TOMCAT/zer0ne/lib/dao-sqlite-zer0ne-0.0.1-SNAPSHOT.jar
#cp dao-mysql/target/dao-mysql-zer0ne-0.0.1-SNAPSHOT-all.jar               $TOMCAT/zer0ne/lib/dao-mysql-zer0ne-0.0.1-SNAPSHOT.jar
cp dro/target/dro-zer0ne-0.0.1-SNAPSHOT-jar-with-dependencies.jar         $TOMCAT/zer0ne/lib/dro-zer0ne-0.0.1-SNAPSHOT.jar
cp dto/target/dto-zer0ne-0.0.1-SNAPSHOT.jar                               $TOMCAT/zer0ne/lib/dto-zer0ne-0.0.1-SNAPSHOT.jar
#cp dto-mail/target/dto-mail-zer0ne-0.0.1-SNAPSHOT-all.jar                 $TOMCAT/zer0ne/lib/dto-mail-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-http/target/dto-http-zer0ne-0.0.1-SNAPSHOT-all.jar                 $TOMCAT/zer0ne/lib/dto-http-zer0ne-0.0.1-SNAPSHOT.jar
#cp dto-cache-ignite/target/dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT-all.jar $TOMCAT/zer0ne/lib/dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT.jar
#
## This (the commented-out cp of the .xml file, immediately below) can only be achieved (path vs. docBase) in server.xml
#cp api-tomcat/api-tomcat-0.0.1-SNAPSHOT.xml                               $TOMCAT/conf/Catalina/localhost/
cp api-tomcat/target/api-tomcat-0.0.1-SNAPSHOT.war                        $TOMCAT/zer0ne/
#
cd -
