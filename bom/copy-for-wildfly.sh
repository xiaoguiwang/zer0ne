export WILDFLY=/c/wildfly-21.0.2.Final

cd ~/itfromblighty.ca/worktrees/zer0ne/release/0.0.1
cp dao/target/dao-zer0ne-0.0.1-SNAPSHOT.jar                               $WILDFLY/modules/zer0ne/main/dao-zer0ne-0.0.1-SNAPSHOT.jar
cp dao-sqlite/target/dao-sqlite-zer0ne-0.0.1-SNAPSHOT-all.jar             $WILDFLY/modules/zer0ne/main/dao-sqlite-zer0ne-0.0.1-SNAPSHOT.jar
cp dao-mysql/target/dao-mysql-zer0ne-0.0.1-SNAPSHOT-all.jar               $WILDFLY/modules/zer0ne/main/dao-mysql-zer0ne-0.0.1-SNAPSHOT.jar
cp dro/target/dro-zer0ne-0.0.1-SNAPSHOT-jar-with-dependencies.jar         $WILDFLY/modules/zer0ne/main/dro-zer0ne-0.0.1-SNAPSHOT.jar
cp dto/target/dto-zer0ne-0.0.1-SNAPSHOT.jar                               $WILDFLY/modules/zer0ne/main/dto-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-mail/target/dto-mail-zer0ne-0.0.1-SNAPSHOT-all.jar                 $WILDFLY/modules/zer0ne/main/dto-mail-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-http/target/dto-http-zer0ne-0.0.1-SNAPSHOT-all.jar                 $WILDFLY/modules/zer0ne/main/dto-http-zer0ne-0.0.1-SNAPSHOT.jar
cp dto-cache-ignite/target/dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT-all.jar $WILDFLY/modules/zer0ne/main/dto-cache-ignite-zer0ne-0.0.1-SNAPSHOT.jar
#
#cp api-jboss/target/api-jboss-0.0.1-SNAPSHOT.war                          $WILDFLY/standalone/deployments/
#rm $WILDFLY/standalone/deployments/api-jboss-0.0.1-SNAPSHOT.war
cp hci-jboss/target/hci-jboss-0.0.1-SNAPSHOT.war                          $WILDFLY/standalone/deployments/
#
cd -
