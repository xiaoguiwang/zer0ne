package dao.db2;

import dao.Connection;
import dao.PreparedStatement;
import dao.ResultSet;
import dao.SQLException;
import dao.Statement;

/*
CREATE TABLE ESBDBID.POC(
  "ID" INTEGER GENERATED ALWAYS AS IDENTITY(
    START WITH 1
   ,INCREMENT BY 1
   ,CYCLE
   ,MINVALUE 1
  )
 ,"KEY" VARCHAR(255) NOT NULL
 ,"VALUE" VARCHAR(255)
);
INSERT INTO ESBDBID.POC("KEY","VALUE") VALUES('key','value')
SELECT "ID","KEY","VALUE" FROM ESBDBID.POC WHERE "KEY"='key'
UPDATE ESBDBID.POC SET "VALUE"='value2' WHERE "KEY"='key'
DELETE FROM ESBDBID.POC WHERE "VALUE"='value2'
DROP TABLE ESBDBID.POC
*/
// This is dao.db2.DatabaseAdapter
public class Adapter extends dao.Adapter {
	protected java.lang.String tableSchema = "ESBDBID";
	
	public Adapter() {
	  //super(); // Implicit super constructor..
		super.q = "\"";
	}
	
	public boolean existsTable(final java.lang.String name) {
	  /*final */java.lang.Boolean exists = false;
		try {
			final StringBuffer sb = new StringBuffer("SELECT COUNT(*) FROM SYSIBM.SYSTABLES WHERE TYPE='T' AND CREATOR='"+tableSchema+"' AND NAME='").append(name).append("'");
			final java.lang.String sql = sb.toString();
System.out.println(sql);
			final ResultSet rs = new Connection() // TODO: convert to selectFromTable but need to support aliases
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.executeQuery(sql, Statement.Return.ResultSet)
				.next(ResultSet.Return.ResultSet)
			;
			exists = 1L == rs.getLong(1);
System.out.println("exists="+exists);
			rs.close(ResultSet.Return.Statement)
			  .close(PreparedStatement.Return.Connection)
			  .close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return exists;
	}
	public Adapter createTable(final java.lang.String name, final java.lang.Integer primaryIdIndex, final java.lang.String[] columnNames, java.lang.String[] columnDescs) {
		try {
			if (null != primaryIdIndex) {
				super.pk.put(q+tableSchema+q+"."+name, primaryIdIndex);
				super.cn.put(q+tableSchema+q+"."+name, columnNames);
			}
			final StringBuffer sb = new StringBuffer("CREATE TABLE").append(" ").append(q+tableSchema+q+"."+name).append("(");
		  /*final */boolean comma = false;
			for (int i = 0; i < columnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				sb.append(q).append(columnNames[i]).append(q).append(" ").append(columnDescs[i]);
				if (null != primaryIdIndex && primaryIdIndex == i) {
				  /*if (false == columnDescs[i].endsWith("NOT NULL")) {
						sb.append(" NOT NULL");
					}*/
					sb.append(" ")
					  .append("GENERATED ALWAYS AS IDENTITY(")
					  .append(  "START WITH 1")
					  .append( ",INCREMENT BY 1")
					  .append( ",CYCLE")
					  .append( ",MINVALUE 1")
					  .append(")")
					;
				}
				comma = true;
			}
			final java.lang.String sql = sb.append(")").toString();
System.out.println(sql);
			new Connection()
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Statement)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
	public java.lang.Integer insertIntoTable(final java.lang.String name, final java.lang.String[] insertColumnNames, final java.lang.Object[] insertColumnValues) {
		final java.lang.Integer id = super.insertIntoTable(q+tableSchema+q+"."+name, insertColumnNames, insertColumnValues);
		if (null != super.pk.get(q+tableSchema+q+"."+name)) {
System.out.println("dao.PreparedStatement.getGeneratedKeys()="+id);
		}
		return id;
	}
	public java.lang.Object[][] selectFromTable(final java.lang.String name, final java.lang.String[] selectColumnNames, final java.lang.Object[] selectColumnTypes, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		return super.selectFromTable(q+tableSchema+q+"."+name, selectColumnNames, selectColumnTypes, whereColumnNames, whereColumnOperators, whereColumnValues);
	}
	public Adapter updateTableSet(final java.lang.String name, final java.lang.String[] updateColumnNames, final java.lang.Object[] updateColumnValues, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.updateTableSet(q+tableSchema+q+"."+name, updateColumnNames, updateColumnValues, whereColumnNames, whereColumnOperators, whereColumnValues);
		return this;
	}
	public Adapter deleteFromTable(final java.lang.String name, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.deleteFromTable(q+tableSchema+q+"."+name, whereColumnNames, whereColumnOperators, whereColumnValues);
		return this;
	}
	public Adapter dropTable(final java.lang.String name) {
		try {
			final java.lang.String sql = new StringBuffer("DROP TABLE").append(" ").append(q+tableSchema+q+"."+name).toString();
System.out.println(sql);
			new Connection()
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Connection)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
}