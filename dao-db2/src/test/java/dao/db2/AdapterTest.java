package dao.db2;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dao.Connection;
import dro.util.Properties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Before
	public void before() {
		try {
			Properties.properties(new Properties(AdapterTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected final Adapter adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public AdapterTest() {
		this.adapter = new Adapter();
	}
	
	@Test
	public void test_0001() {
		if (true == this.adapter.existsTable("POC")) {
			this.adapter.dropTable("POC");
		}
		final java.lang.String[] stringColumnTypes = new java.lang.String[]{"INTEGER","VARCHAR(255) NOT NULL","VARCHAR(255)"}; // FIXME!
		final java.lang.Object[] objectColumnTypes = new java.lang.Object[]{new Integer((int)0), new java.lang.String(""), new java.lang.String("")};
		this.adapter.createTable("POC", 0, new java.lang.String[]{"ID","KEY","VALUE"}, stringColumnTypes);
		this.adapter.existsTable("POC");
		this.adapter.insertIntoTable("POC", new java.lang.String[]{"KEY","VALUE"}, new java.lang.Object[]{"key","value"});
		this.adapter.insertIntoTable("POC", new java.lang.String[]{"KEY","VALUE"}, new java.lang.Object[]{"key1","value1"});
		this.adapter.insertIntoTable("POC", new java.lang.String[]{"KEY","VALUE"}, new java.lang.Object[]{"key2","value2"});
		this.adapter.selectFromTable("POC", new java.lang.String[]{"ID","KEY","VALUE"}, objectColumnTypes, new java.lang.String[]{"KEY"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"key"});
		this.adapter.updateTableSet("POC", new java.lang.String[]{"VALUE"}, new java.lang.Object[]{"value2"}, new java.lang.String[]{"KEY"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"key"});
		this.adapter.deleteFromTable("POC", new java.lang.String[]{"VALUE"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"value2"});
		this.adapter.dropTable("POC");
		this.adapter.existsTable("POC");
	}
	
	@After
	public void after() {
		Connection.shutdown();
	}
}