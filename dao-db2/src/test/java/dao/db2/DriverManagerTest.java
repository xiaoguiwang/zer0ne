package dao.db2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DriverManagerTest {
	@Test
	public void test_0001() throws SQLException, FileNotFoundException, IOException {
		final dro.util.Properties p = new dro.util.Properties(DriverManagerTest.class);
		final String url = p.getProperty(java.sql.Connection.class.getName()+"#url");
		final String user = p.getProperty(java.sql.Connection.class.getName()+"#user");
		final String password = p.getProperty(java.sql.Connection.class.getName()+"#password");
		final Connection c = DriverManager.getConnection(url, user, password);
		final DatabaseMetaData md = c.getMetaData();
		final String driverName = md.getDriverName();
		final String driverVersion = md.getDriverVersion();
		final String jdbcVersion = md.getJDBCMajorVersion()+"."+ md.getJDBCMinorVersion();
		System.out.println("driver-manager:meta-data:");
		System.out.println(" driver-name="+driverName);
		System.out.println(">driver-version="+driverVersion+"<<====");
		System.out.println(" jdbc:major/minor-version="+jdbcVersion);
	}
}