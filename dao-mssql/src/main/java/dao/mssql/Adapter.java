package dao.mssql;

import dao.Connection;
import dao.PreparedStatement;
import dao.ResultSet;
import dao.SQLException;
import dao.Statement;

/*
CREATE TABLE temp.POC(
  "ID" INT PRIMARY KEY IDENTITY(1, 1)
 ,"KEY" VARCHAR(255) NOT NULL
 ,"VALUE" VARCHAR(255)
);
INSERT INTO temp.POC("KEY","VALUE") VALUES('key','value')
SELECT "ID","KEY","VALUE" FROM temp.POC WHERE "KEY"='key'
UPDATE temp.POC SET "VALUE"='value2' WHERE "KEY"='key'
DELETE FROM temp.POC WHERE "VALUE"='value2'
DROP TABLE temp.POC
*/
// This is dao.mssql.DatabaseAdapter
public class Adapter extends dao.Adapter {
	protected java.lang.String tableSchema = "temp"; // FIXME!
	
	public Adapter() {
	  //super(); // Implicit super constructor..
		super.q = "\"";
	}
	
	public boolean existsTable(final java.lang.String name) {
	  /*final */java.lang.Boolean exists = false;
		try {
			final StringBuffer sb = new StringBuffer("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='"+tableSchema+"' AND TABLE_NAME='").append(name).append("'");
			final java.lang.String sql = sb.toString();
System.out.println(sql);
			final ResultSet rs = new Connection() // TODO: convert to selectFromTable but need to support aliases
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.executeQuery(sql, Statement.Return.ResultSet)
				.next(ResultSet.Return.ResultSet)
			;
			exists = 1L == rs.getLong(1);
System.out.println("exists="+exists);
			rs.close(ResultSet.Return.Statement)
			  .close(PreparedStatement.Return.Connection)
			  .close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return exists;
	}
	public Adapter createTable(final java.lang.String name, final java.lang.Integer primaryIdIndex, final java.lang.String[] columnNames, java.lang.String[] columnDescs) {
		try {
			if (null != primaryIdIndex) {
				super.pk.put(q+tableSchema+q+"."+name, primaryIdIndex);
				super.cn.put(q+tableSchema+q+"."+name, columnNames);
			}
			final StringBuffer sb = new StringBuffer("CREATE TABLE").append(" ").append(q+tableSchema+q+"."+name).append("(");
		  /*final */boolean comma = false;
			for (int i = 0; i < columnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				sb.append(this.q).append(columnNames[i]).append(this.q).append(" ").append(columnDescs[i]);
				if (null != primaryIdIndex && primaryIdIndex == i) {
					sb.append(" PRIMARY KEY IDENTITY(1, 1)");
				}
				comma = true;
			}
			final java.lang.String sql = sb.append(")").toString();
System.out.println(sql);
			new Connection()
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Statement)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
	public java.lang.Integer insertIntoTable(final java.lang.String name, final java.lang.String[] insertColumnNames, final java.lang.Object[] insertColumnValues) {
		final java.lang.Integer id = super.insertIntoTable(q+tableSchema+q+"."+name, insertColumnNames, insertColumnValues);
		if (null != super.pk.get(q+tableSchema+q+"."+name)) {
System.out.println("dao.PreparedStatement.getGeneratedKeys()="+id);
		}
		return id;
	}
	public java.lang.Object[][] selectFromTable(final java.lang.String name, final java.lang.String[] selectColumnNames, final java.lang.Object[] selectColumnTypes, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		return super.selectFromTable(q+tableSchema+q+"."+name, selectColumnNames, selectColumnTypes, whereColumnNames, whereColumnOperators, whereColumnValues);
	}
	public Adapter updateTableSet(final java.lang.String name, final java.lang.String[] updateColumnNames, final java.lang.Object[] updateColumnValues, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.updateTableSet(q+tableSchema+q+"."+name, updateColumnNames, updateColumnValues, whereColumnNames, whereColumnOperators, whereColumnValues);
		return this;
	}
	public Adapter deleteFromTable(final java.lang.String name, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.deleteFromTable(q+tableSchema+q+"."+name, whereColumnNames, whereColumnOperators, whereColumnValues);
		return this;
	}
	public Adapter dropTable(final java.lang.String name) {
		try {
			final java.lang.String sql = new StringBuffer("DROP TABLE").append(" ").append(q+tableSchema+q+"."+name).toString();
System.out.println(sql);
			new Connection()
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Connection)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
}