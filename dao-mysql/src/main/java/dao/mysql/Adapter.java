package dao.mysql;

import java.util.HashMap;
import java.util.HashSet;

import dao.Connection;
import dao.SQLException;

/*
CREATE TABLE ZER0NE.POC(
  `ID` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY
 ,`KEY` TEXT NOT NULL
 ,`VALUE` TEXT
);
INSERT INTO ZER0NE.POC(`KEY`,`VALUE`) VALUES('key','value')
SELECT `ID`,`KEY`,`VALUE` FROM ZER0NE.POC WHERE `KEY`='key'
UPDATE ZER0NE.POC SET `VALUE`='value2' WHERE `KEY`='key'
DELETE FROM ZER0NE.POC WHERE `VALUE`='value2'
DROP TABLE ZER0NE.POC
*/
// This is dao.mysql.DatabaseAdapter
public class Adapter extends dao.Adapter {
	protected static java.util.Map<java.lang.String, java.lang.String> grammar = new HashMap<>();
	protected static java.util.Set<java.lang.String> reserve = new HashSet<>();
	static {
		// https://dev.mysql.com/doc/refman/8.0/en/keywords.html
		dao.mysql.Adapter.reserve(new java.lang.String[]{
			"ACCESSIBLE","ADD","ALL","ALTER","ANALYZE","AND","AS","ASC","ASENSITIVE",
			"BEFORE","BETWEEN","BIGINT","BINARY","BLOB","BOTH","BY",
			"CALL","CASCADE","CASE","CHANGE","CHAR","CHARACTER","CHECK","COLLATE","COLUMN","CONDITION","CONSTRAINT","CONTINUE","CONVERT","CREATE","CROSS","CUBE","CUME_DIST","CURRENT_DATE","CURRENT_TIME","CURRENT_TIMESTAMP","CURRENT_USER","CURSOR",/**/"CUME_DIST",
			"DATABASE","DATABASES","DAY_HOUR","DAY_MICROSECOND","DAY_MINUTE","DAY_SECOND","DEC","DECIMAL","DECLARE","DEFAULT","DELAYED","DELETE","DENSE_RANK","DESC","DESCRIBE","DETERMINISTIC","DISTINCT","DISTINCTROW","DIV","DOUBLE","DROP","DUAL",/**/"DENSE_RANK",
			"EACH","ELSE","ELSEIF","EMPTY","ENCLOSED","ESCAPED","EXCEPT","EXISTS","EXIT","EXPLAIN",/**/"EMPTY","EXCEPT",
			"FALSE","FETCH","FLOAT","FLOAT4","FLOAT8","FOR","FORCE","FOREIGN","FROM","FULLTEXT","FUNCTION",/**/"FIRST_VALUE",
			"GENERATED","GET","GRANT","GROUP",/**/"GROUPING","GROUPS",
			"HAVING","HIGH_PRIORITY","HOUR_MICROSECOND","HOUR_MINUTE","HOUR_SECOND",
			"IF","IGNORE","IN","INDEX","INFILE","INNER","INOUT","INSENSITIVE","INSERT","INT","INT1","INT2","INT3","INT4","INT8","INTEGER","INTERVAL","INTO","IO_AFTER_GTIDS","IO_BEFORE_GTIDS","IS","ITERATE",
			"JOIN",/**/"JSON_TABLE",
			"KEY","KEYS","KILL",
			"LEADING","LEAVE","LEFT","LIKE","LIMIT","LINEAR","LINES","LOAD","LOCALTIME","LOCALTIMESTAMP","LOCK","LONG","LONGBLOB","LONGTEXT","LOOP","LOW_PRIORITY",/**/"LAG","LAST_VALUE","LATERAL","LEAD",
			"MASTER_BIND","MATCH","MAXVALUE","MEDIUMBLOB","MEDIUMLIMIT","MEDIUMTEXT","MIDDLEINT","MINUTE_MICROSECOND","MINUTE_SECOND","MOD","MODIFIES",
			"NATURAL","NOT","NO_WRITE_TO_BIN_LOG","NULL","NUMERIC",/**/"NTH_VALUE","NTILE",
			"ON","OPTIMIZE","OPTIMIZER_COSTS","OPTION","OPTIONALLY","OR","ORDER","OUT","OUTER","OUTFILE",/**/"OF","OVER",
			"PARTITION","PRECISION","PRIMARY","PROCEDURE","PURGE",/**/"PERCENT_RANK",
			"RANGE","READ","READS","READ_WRITE","REAL","REFERENCES","REGEXP","RELEASE","RENAME","REPEAT","REPLACE","REQUIRE","RESIGNAL","RESTRICT","RETURN","REVOKE","RIGHT","RLIKE","ROW","ROWS",/**/"RANK","RECURSIVE","ROW_NUMBER",
			"SCHEMA","SCHEMAS","SECOND_MICROSECOND","SELECT","SENSITIVE","SEPARATOR","SET","SHOW","SIGNAL","SMALLINT","SPATIAL","SPECIFIC","SQL","SQLEXCEPTION","SQLSTATE","SQLWARNING","SQL_BIG_RESULT","SQL_CALC_FOUND_ROWS","SQL_SMALL_RESULT","SSL","STARTING","STORED","STRAIGHT_JOIN",/**/"SYSTEM",
			"TABLE","TERMINATED","THEN","TINYBLOB","TINYINT","TINYTEXT","TO","TRAILING","TRIGGER","TRUE",
			"UNDO","UNION","UNIQUE","UNLOCK","UNSIGNED","UPDATE","USAGE","USE","USING","UTC_DATE","UTC_TIME","UTC_TIMESTAMP",
			"VALUES","VARBINARY","VARCHAR","VARCHARACTER","VARYING","VIRTUAL",
			"WHEN","WHERE","WHILE","WITH","WRITE",/**/"WINDOW",
			"XOR",
			"YEAR_MONTH",
			"ZEROFILL"
		});
	}
	
	public static void reserve(final java.lang.String[] reserves) {
		for (final java.lang.String reserve: reserves) {
			if (false == dao.mysql.Adapter.reserve.contains(reserve.toUpperCase())) {
				dao.mysql.Adapter.reserve.add(reserve);
			}
		}
	}
	protected static java.util.Map<java.lang.Class<?>, java.lang.String> mapTypeToDatabase;// = new HashMap<>();
	protected static java.util.Map<java.lang.String, java.lang.Class<?>> mapTypeFromDatabase;// = new HashMap<>();
	
	static {
		mapTypeToDatabase = new HashMap<java.lang.Class<?>, java.lang.String>(){
			private static final long serialVersionUID = 1L;
			{
				put((java.lang.Class<?>)null, "NULL");
				put(java.lang.Boolean.class, "BIT");//
				put(java.lang.Short.class, "INT");//
				put(java.lang.Integer.class, "INT");////
				put(java.lang.Long.class, "BIGINT");//
				put(java.lang.Float.class, "FLOAT");
				put(java.lang.Double.class, "DOUBLE");
				put(java.math.BigInteger.class, "BIGINT");
				put(java.math.BigDecimal.class, "DECIMAL");
				put(java.lang.Character.class, "TINYINT");//
				put(java.lang.Byte.class, "CHAR");
				put(java.lang.String.class, "TEXT");
				put(java.util.Date.class, "DATE"); // Dates can be INTEGER (seconds since epoch), REAL (Julian day numbers including fractions), and ISO8601 string ("YYYY-MM-DD HH:MM:SS.SSS")
			}
		};
		mapTypeFromDatabase = new HashMap<java.lang.String, java.lang.Class<?>>(){
			// https://dev.mysql.com/doc/refman/8.0/en/data-types.html
			// https://dev.mysql.com/doc/refman/8.0/en/numeric-types.html
			// Note: 
			private static final long serialVersionUID = 1L;
			{
				put("NULL", (java.lang.Class<?>)null);
				put("INTEGER", java.lang.Integer.class);
				put("INT", java.lang.Integer.class);
				put("SMALLINT", java.lang.Integer.class);
				put("TINYINT", java.lang.Integer.class);
				put("MEDIUMINT", java.lang.Integer.class);
				put("INTEGER", java.math.BigInteger.class); // Consider excess precision - need dro.meta.Data to help
				put("DECIMAL", java.math.BigDecimal.class); // Consider excess precision - need dro.meta.Data to help
				put("FLOAT", java.lang.Float.class); 
				put("DOUBLE", java.lang.Double.class); 
				put("BIT", java.lang.Boolean.class); 
				put("CHAR", java.lang.Character.class); 
				put("VARCHAR", java.lang.String.class); 
				put("BINARY", java.lang.Byte[].class);
				put("VARBINARY", java.lang.Byte[].class);
				put("BLOB", java.lang.Byte[].class);
				put("TEXT", java.lang.String.class);
				put("DATE", java.util.Date.class);
				put("DATETIME", java.lang.String.class);
				put("TIMESTAMP", java.lang.String.class);
			  //put("ENUM", ..);
			  //put("SET", ..);
			}
		};
	}
	
	protected java.lang.String tableSchema = "zer0ne";
	
	public Adapter() {
	  //super(); // Implicit super constructor..
		super.q = "`";
		super.mapTypeToDatabase(dao.mysql.Adapter.mapTypeToDatabase);
	  //super.mapTypeFromDatabase(dao.mysql.Adapter.mapTypeFromDatabase);
		super.reserve(dao.mysql.Adapter.reserve);
		super.grammar("unique-index", "AUTO_INCREMENT PRIMARY KEY"); // TODO: dro.Data.set generated-key instead?
	  //super.grammar("table-append", "ENGINE=NDBCLUSTER"); // TODO: dro.Data.set cluster-backend instead?
	}
	
	public boolean existsTable(final java.lang.String name) {
	  /*final */boolean exists = false;
		if (false == super.tables.containsKey(name)) {
			final java.lang.Object[][] oa = this.selectFromTable("information_schema.tables", new java.lang.String[]{"COUNT(*)"}, new java.lang.Object[]{new java.lang.Long(0L)}, new java.lang.String[]{"table_schema","table_name"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{tableSchema,name});
			try {
				new Connection().close();
			} catch (final SQLException e) {
				// Silently ignore..
			}
		  /*final boolean */exists = 1L == (java.lang.Long)oa[0][0];
			super.tables.put(name, exists);
System.out.println("exists="+exists);
		} else {
			exists = super.tables.get(name);
		}
		return exists;
	}
	public Adapter createTable(final java.lang.String name, final java.lang.Integer primaryIdIndex, final java.lang.String[] columnNames, java.lang.String[] columnTypes) {
		super.createTable(name, primaryIdIndex, columnNames, columnTypes);
	  /*try {
			if (null != primaryIdIndex) {
				super.pk.put(q+tableSchema+q+"."+name, primaryIdIndex);
				super.cn.put(q+tableSchema+q+"."+name, columnNames);
			}
			final StringBuffer sb = new StringBuffer("CREATE TABLE").append(" ").append(tableSchema+"."+name).append("(");
		*//*final *//*boolean comma = false;
			for (int i = 0; i < columnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				sb.append(q).append(columnNames[i]).append(q).append(" ").append(columnDescs[i]);
				if (null != primaryIdIndex && primaryIdIndex == i) {
					if (false == columnDescs[i].endsWith("NOT NULL")) {
						sb.append(" NOT NULL");
					}
					sb.append(" AUTO_INCREMENT PRIMARY KEY");
				}
				comma = true;
			}
		  //sb.append(",PRIMARY KEY(`"+columnNames[primaryIdIndex]+"`)");
			final java.lang.String sql = sb.append(")").toString();
System.out.println(sql);
			new Connection()
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Statement)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}*/
		return this;
	}
	public java.lang.Integer insertIntoTable(final java.lang.String name, final java.lang.String[] insertColumnNames, final java.lang.Object[] insertColumnValues) {
		final java.lang.Integer id = super.insertIntoTable(name, insertColumnNames, insertColumnValues);
		if (null != super.pk.get(name)) {
System.out.println("dao.PreparedStatement.getGeneratedKeys()="+id);
		}
		return id;
	}
	public java.lang.Object[][] selectFromTable(final java.lang.String name, final java.lang.String[] selectColumnNames, final java.lang.Object[] selectColumnTypes, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		return super.selectFromTable(name, selectColumnNames, selectColumnTypes, whereColumnNames, whereColumnOperators, whereColumnValues);
	}
	public Adapter updateTableSet(final java.lang.String name, final java.lang.String[] updateColumnNames, final java.lang.Object[] updateColumnValues, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.updateTableSet(name, updateColumnNames, updateColumnValues, whereColumnNames, whereColumnOperators, whereColumnValues);
		return this;
	}
	public Adapter deleteFromTable(final java.lang.String name, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.deleteFromTable(name, whereColumnNames, whereColumnOperators, whereColumnValues);
		return this;
	}
	public Adapter dropTable(final java.lang.String name) {
		super.dropTable(name);
	  /*try {
			final java.lang.String sql = new StringBuffer("DROP TABLE").append(" ").append(tableSchema+"."+name).toString();
System.out.println(sql);
			new Connection()
				.properties(this.p)
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Connection)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}*/
		return this;
	}
	/*
	public Adapter shutdown() {
		throw new IllegalStateException();
	  //return this;
	}*/
}
