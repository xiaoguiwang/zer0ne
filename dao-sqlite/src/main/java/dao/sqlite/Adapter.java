package dao.sqlite;

import java.util.HashMap;
import java.util.HashSet;

import dao.Connection;
import dao.SQLException;

//final String Name = clazz.getName();
//final String NAME = Name.replaceAll("\\.", "_").toUpperCase();

/*
CREATE TABLE POC(
  ID INTEGER PRIMARY KEY AUTOINCREMENT
 ,"KEY" TEXT NOT NULL
 ,VALUE TEXT
)
INSERT INTO POC("KEY",VALUE) VALUES('key','value')
SELECT ID,"KEY",VALUE FROM POC WHERE "KEY"='key'
UPDATE POC SET VALUE='value2' WHERE "KEY"='key'
DELETE FROM POC WHERE VALUE='value2'
DROP TABLE POC
*/
// This is dao.sqlite.DatabaseAdapter
public class Adapter extends dao.Adapter {
	protected static java.util.Map<java.lang.String, java.lang.String> grammar = new HashMap<>();
	protected static java.util.Set<java.lang.String> reserve = new HashSet<>();
	static {
		// https://www.sqlite.org/lang_keywords.html
		dao.sqlite.Adapter.reserve(new java.lang.String[]{
			"ABORT","ACTION","ADD","AFTER","ALL","ALTER","ALWAYS","ANALYZE","AND","AS","ASC","ATTACH","AUTOINCREMENT",
			"BEFORE","BEGIN","BETWEEN","BY",
			"CASCADE","CASE","CAST","CHECK","COLLATE","COLUMN","COMMIT","CONFLICT","CONSTRAINT","CREATE","CROSS","CURRENT","CURRENT_DATE","CURRENT_TIME","CURRENT_TIMESTAMP",
			"DATABASE","DEFAULT","DEFERRABLE","DEFERRED","DELETE","DESC","DETACH","DISTINCT","DO","DROP",
			"EACH","ELSE","END","ESCAPE","EXCEPT","EXCLUDE","EXCLUSIVE","EXISTS","EXPLAIN",
			"FAIL","FILTER","FIRST","FOLLOWING","FOR","FOREIGN","FROM","FULL",
			"GENERATED","GLOB","GROUP","GROUPS",
			"HAVING",
			"IF","IGNORE","IMMEDIATE","IN","INDEX","INDEXED","INITIALLY","INNER","INSERT","INSTEAD","INTERSECT","INTO","IS","ISNULL",
			"JOIN",
			"KEY",
			"LAST","LEFT","LIKE","LIMIT",
			"MATCH",
			"NATURAL","NO","NOT","NOTHING","NOTNULL","NULL","NULLS",
			"OF","OFFSET","ON","OR","ORDER","OTHERS","OUTER","OVER",
			"PARTITION","PLAN","PRAGMA","PRECEDING","PRIMARY",
			"QUERY",
			"RAISE","RANGE","RECURSIVE","REFERENCES","REGEXP","REINDEX","RELEASE","RENAME","REPLACE","RESTRICT","RIGHT","ROLLBACK","ROW","ROWS",
			"SAVEPOINT","SELECT","SET",
			"TABLE","TEMP","TEMPORARY","THEN","TIES","TO","TRANSACTION","TRIGGER",
			"UNBOUNDED","UNION","UNIQUE","UPDATE","USING",
			"VACUUM","VALUES","VIEW","VIRTUAL",
			"WHEN","WHERE","WINDOW","WITH","WITHOUT"
		});
	}
	protected static java.util.Map<java.lang.Class<?>, java.lang.String> mapTypeToDatabase;// = new HashMap<>();
	protected static java.util.Map<java.lang.String, java.lang.Class<?>> mapTypeFromDatabase;// = new HashMap<>();
	
	static {
	  //grammar = new HashMap<java.lang.String, java.lang.String>();
	  //reserve = new HashSet<java.lang.String>();
		// INTEGER: INT, INTEGER, TINYINT, SMALLINT, MEDIUMINT, BIGINT, UNSIGNED BIG INT, INT2, INT8
		// TEXT: CHARACTER(N), VARCHAR(N), VARYING CHARACTER(N), NCHAR(N), NATIVE CHARACTER(N), NVARCHAR(N), TEXT, CLOB
		// BLOB: BLOB
		// REAL: REAL, DOUBLE, DOUBLE PRECISION, FLOAT
		// NUMERIC: NUMERIC, DECIMAL(N,M), BOOLEAN, DATE, DATETIME
		// Note#1: "FLOATING PO>INT<" is INTEGER (not REAL) and STRING is NUMERIC (not TEXT)
		// Note#2: A column with TEXT affinity stores all data using storage classes NULL, TEXT, or BLOB
		// Note#3: A column with NUMERIC affinity may contain values using all five storage classes 
		mapTypeToDatabase = new HashMap<java.lang.Class<?>, java.lang.String>(){
			private static final long serialVersionUID = 1L;
			{
				put((java.lang.Class<?>)null, "NULL");
				put(java.lang.Boolean.class, "INTEGER");
				put(java.lang.Short.class, "INTEGER");
				put(java.lang.Integer.class, "INTEGER");
				put(java.lang.Long.class, "INTEGER");
				put(java.lang.Float.class, "REAL");
				put(java.lang.Double.class, "REAL");
				put(java.math.BigInteger.class, "INTEGER");
				put(java.math.BigDecimal.class, "REAL");
				put(java.lang.Character.class, "TEXT");
				put(java.lang.Byte.class, "INTEGER");
				put(java.lang.String.class, "TEXT");
				put(java.util.Date.class, "TEXT"); // Dates can be INTEGER (seconds since epoch), REAL (Julian day numbers including fractions), and ISO8601 string ("YYYY-MM-DD HH:MM:SS.SSS")
			}
		};
		mapTypeFromDatabase = new HashMap<java.lang.String, java.lang.Class<?>>(){
			// https://www.sqlite.org/limits.html
			// Note: while there is no limit on width (e.g. VARCHAR(255)) there is maximum length of a string or blob (SQLITE_MAX_LENGTH=)1,000,000,000 bytes
			private static final long serialVersionUID = 1L;
			{
				put("NULL", (java.lang.Class<?>)null);
				put("INTEGER", java.math.BigInteger.class); // Consider excess precision - need dro.meta.Data to help
				put("REAL", java.math.BigDecimal.class); // Consider excess precision - need dro.meta.Data to help
				put("TEXT", java.lang.String.class); 
				put("BLOB", java.lang.Byte[].class);
			}
		};
	}
	
	public static void reserve(final java.lang.String[] reserves) {
		for (final java.lang.String reserve: reserves) {
			if (false == dao.sqlite.Adapter.reserve.contains(reserve.toUpperCase())) {
				dao.sqlite.Adapter.reserve.add(reserve);
			}
		}
	}
	
	public Adapter() {
	  //super(); // Implicit super constructor..
		super.q = "\"";
		super.mapTypeToDatabase(dao.sqlite.Adapter.mapTypeToDatabase);
	  //super.mapTypeFromDatabase(dao.sqlite.Adapter.mapTypeFromDatabase);
		super.reserve(dao.sqlite.Adapter.reserve);
		super.grammar("unique-index", "PRIMARY KEY AUTOINCREMENT"); // TODO: dro.Data.set instead?
	}
	
	@Override
	public Adapter startup() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
		  //file.delete();
		}
		return this;
	}
	
	public boolean existsTable(final java.lang.String name) {
	  /*final */boolean exists = false;
		if (false == super.tables.containsKey(name)) {
			final java.lang.Object[][] oa = this.selectFromTable("sqlite_master", new java.lang.String[]{"COUNT(*)"}, new java.lang.Object[]{new java.lang.Long(0L)}, new java.lang.String[]{"type","name"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"table",name});
			try {
				new Connection().close();
			} catch (final SQLException e) {
				// Silently ignore..
			}
		  /*final boolean */exists = 1L == (java.lang.Long)oa[0][0];
			super.tables.put(name, exists);
System.out.println("exists="+exists);
		} else {
			exists = super.tables.get(name);
		}
		return exists;
	}
	// Historical#1: final java.lang.String[] stringColumnTypes = new java.lang.String[]{"INTEGER","TEXT NOT NULL","TEXT"};
	// Historical#2: final java.lang.String[] columnTypes = new java.lang.Object[]{new Integer((int)0), new java.lang.String(), (java.lang.String)null}
	public Adapter createTable(final java.lang.String name, final java.lang.Integer primaryIdIndex, final java.lang.String[] columnNames, final java.lang.Object[] columnTypes) {
		super.createTable(name, primaryIdIndex, columnNames, columnTypes);
	  /*try {
			new Connection().close(); // Note: not needed for CREATE TABLE(..) DDL
		} catch (final SQLException e) {
			// Silently ignore..
		}*/
		return this;
	}
	public java.lang.Integer insertIntoTable(final java.lang.String name, final java.lang.String[] insertColumnNames, final java.lang.Object[] insertColumnValues) {
		// Don't close connection in super.insertTable(..) otherwise last_insert_rowid()=0
		super.insertIntoTable(name, insertColumnNames, insertColumnValues);
		// If connection is closed in super.insertIntoTable(..) then last_insert_rowid() from super.selectFromTable(..) will always return 0
	  /*final */java.lang.Integer id = null;
		if (null != super.pk.get(name)) {
			final java.lang.Object[][] oa = this.selectFromTable(
				(java.lang.String)null,
				new java.lang.String[]{"last_insert_rowid()"},
				new java.lang.Object[]{new java.lang.Integer((int)0)},
				(java.lang.String[])null,
				(java.lang.String[])null,
				(java.lang.Object[])null
			);
			try {
				new Connection().close();
			} catch (final SQLException e) {
				// Silently ignore..
			}
			id = (java.lang.Integer)oa[0][0]; // If connection was closed in super.insertIntoTable(..) then last_insert_rowid() will always return 0
System.out.println("last_insert_rowid()="+id);
		}
		return id;
	}
  /*public java.lang.Object[][] selectFromTable(final java.lang.String name, final java.lang.String[] selectColumnNames, final java.lang.Object[] selectColumnTypes, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		return super.selectFromTable(name, selectColumnNames, selectColumnTypes, whereColumnNames, whereColumnOperators, whereColumnValues);
	}*/
	public Adapter updateTableSet(final java.lang.String name, final java.lang.String[] updateColumnNames, final java.lang.Object[] updateColumnValues, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.updateTableSet(name, updateColumnNames, updateColumnValues, whereColumnNames, whereColumnOperators, whereColumnValues);
		try {
			new Connection().close();
		} catch (final SQLException e) {
			// Silently ignore..
		}
		return this;
	}
	public Adapter deleteFromTable(final java.lang.String name, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		super.deleteFromTable(name, whereColumnNames, whereColumnOperators, whereColumnValues);
		try {
			new Connection().close();
		} catch (final SQLException e) {
			// Silently ignore..
		}
		return this;
	}
	public Adapter dropTable(final java.lang.String name) {
		super.dropTable(name);
	  /*try {
			new Connection().close(); // Note: not needed for DROP TABLE(..) DDL
		} catch (final SQLException e) {
			// Silently ignore..
		}*/
		return this;
	}
	
	@Override
	public Adapter shutdown() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
		  //file.delete();
		}
		return this;
	}
}