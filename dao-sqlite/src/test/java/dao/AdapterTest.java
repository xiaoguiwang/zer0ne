package dao;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.util.Properties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	protected final Adapter adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public AdapterTest() {
		try {
			Properties.properties(new Properties(AdapterTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		this.adapter = (dao.Adapter)dao.Adapter.newAdapter("dao.sqlite.Adapter");
	}
	
	@Before
	public void before() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
	}
	
	@Test
	public void test_0001() {
		if (true == this.adapter.existsTable("POC")) {
			this.adapter.dropTable("POC");
		}
		final java.lang.String[] stringColumnTypes = new java.lang.String[]{"INTEGER","TEXT NOT NULL","TEXT"};
		final java.lang.Object[] objectColumnTypes = new java.lang.Object[]{new Integer((int)0), new java.lang.String(""), new java.lang.String("")};
		this.adapter.createTable("POC", new java.lang.Integer(0), new java.lang.String[]{"ID","KEY","VALUE"}, stringColumnTypes);
		this.adapter.existsTable("POC");
		this.adapter.insertIntoTable("POC", new java.lang.String[]{"KEY","VALUE"}, new java.lang.Object[]{"key","value"});
		this.adapter.insertIntoTable("POC", new java.lang.String[]{"KEY","VALUE"}, new java.lang.Object[]{"key1","value1"});
		this.adapter.insertIntoTable("POC", new java.lang.String[]{"KEY","VALUE"}, new java.lang.Object[]{"key2","value2"});
		this.adapter.selectFromTable("POC", new java.lang.String[]{"ID","KEY","VALUE"}, objectColumnTypes, new java.lang.String[]{"KEY"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"key"});
		this.adapter.updateTableSet("POC", new java.lang.String[]{"VALUE"}, new java.lang.Object[]{"value2"}, new java.lang.String[]{"KEY"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"key"});
		this.adapter.deleteFromTable("POC", new java.lang.String[]{"VALUE"}, new java.lang.String[]{"=","="}, new java.lang.Object[]{"value2"});
		this.adapter.dropTable("POC");
		this.adapter.existsTable("POC");
	}
	
	@After
	public void after() {
		Connection.shutdown();
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
	}
}