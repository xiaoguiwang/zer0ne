package dao;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.util.Properties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContextTest {
	private final static dao.data.Byte __Byte = (dao.data.Byte)Adapter.access(java.lang.Byte.class);
	private final static dao.data.Character __Character = (dao.data.Character)Adapter.access(java.lang.Character.class);
	private final static dao.data.Boolean __Boolean = (dao.data.Boolean)Adapter.access(java.lang.Boolean.class);
	private final static dao.data.Short __Short = (dao.data.Short)Adapter.access(java.lang.Short.class);
	private final static dao.data.Integer __Integer = (dao.data.Integer)Adapter.access(java.lang.Integer.class);
	private final static dao.data.Long __Long = (dao.data.Long)Adapter.access(java.lang.Long.class);
	private final static dao.data.Float __Float = (dao.data.Float)Adapter.access(java.lang.Float.class);
	private final static dao.data.Double __Double = (dao.data.Double)Adapter.access(java.lang.Double.class);
	private final static dao.data.BigInteger __BigInteger = (dao.data.BigInteger)Adapter.access(java.math.BigInteger.class);
	private final static dao.data.BigDecimal __BigDecimal = (dao.data.BigDecimal)Adapter.access(java.math.BigDecimal.class);
	private final static dao.data.String __String = (dao.data.String)Adapter.access(java.lang.String.class);
	private final static dao.data.Date __Date = (dao.data.Date)Adapter.access(java.util.Date.class);
	
 //private final static dao.lang.Array __Array = (dao.lang.Array)Adapter.access(java.lang.Object[].class);
	private final static dao.lang.List __List = (dao.lang.List)Adapter.access(java.util.List.class);
	private final static dao.lang.Set __Set = (dao.lang.Set)Adapter.access(java.util.Set.class);
	private final static dao.lang.Map __Map = (dao.lang.Map)Adapter.access(java.util.Map.class);
	
	protected final dao.Context context;// = null;
	
	{
	  //context = null;
		try {
			Properties.properties(new Properties(ContextTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public ContextTest() {
		this.context = new dao.Context();
	}
	
	@Before
	public void before() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
	}
	
  //@Test
	public void test_0001() {
		{
			dao.lang.Primitive<java.lang.Byte> p = __Byte;
			final java.lang.Byte Byte_75 = new java.lang.Byte((byte)75);
		  /*final java.lang.Integer row_id = */p.insert(Byte_75);
			dao.Context.getAdapterFor(this).alias(Byte_75, "byte"); 
			final java.lang.Byte b = (java.lang.Byte)dao.Context.getAdapterFor(this).alias("byte", java.lang.Byte.class);
			Assert.assertTrue(Byte_75.byteValue() == b.byteValue());
		}
		{
			dao.lang.Primitive<java.lang.Character> p = __Character;
			final java.lang.Character Char_75 = new java.lang.Character((char)75);
		  /*final java.lang.Integer row_id = */p.insert(Char_75);
			dao.Context.getAdapterFor(this).alias(Char_75, "char"); 
			final java.lang.Character c = (java.lang.Character)dao.Context.getAdapterFor(this).alias("char", java.lang.Character.class);
			Assert.assertTrue(Char_75.charValue() == c.charValue());
		}
		{
			dao.lang.Primitive<java.lang.Boolean> p = __Boolean;
			final java.lang.Boolean Boolean_true = new java.lang.Boolean(true);
		  /*final java.lang.Integer row_id = */p.insert(Boolean_true);
			dao.Context.getAdapterFor(this).alias(Boolean_true, "boolean"); 
			final java.lang.Boolean b = (java.lang.Boolean)dao.Context.getAdapterFor(this).alias("boolean", java.lang.Boolean.class);
			Assert.assertTrue(Boolean_true.booleanValue() == b.booleanValue());
		}
		{
			dao.lang.Primitive<java.lang.Short> p = __Short;
		  //final java.lang.Short Short_1975 = new java.lang.Short((short)1975);
		  /*final java.lang.Integer row_id = */p.insert((short)1975);
			dao.Context.getAdapterFor(this).alias((short)1975, "short"); 
			final java.lang.Short n = (java.lang.Short)dao.Context.getAdapterFor(this).alias("short", java.lang.Short.class);
			Assert.assertTrue((short)1975 == n.shortValue());
		}
		{
			dao.lang.Primitive<java.lang.Integer> p = __Integer;
		  /*final java.lang.Integer row_id = */p.insert(1975);
			dao.Context.getAdapterFor(this).alias(1975, "int"); 
			final java.lang.Integer n = (java.lang.Integer)dao.Context.getAdapterFor(this).alias("int", java.lang.Integer.class);
			Assert.assertTrue(1975 == n.intValue());
		}
		{
			dao.lang.Primitive<java.lang.Long> p = __Long;
		  /*final java.lang.Integer row_id = */p.insert(1975L);
			dao.Context.getAdapterFor(this).alias(1975L, "long"); 
			final java.lang.Long n = (java.lang.Long)dao.Context.getAdapterFor(this).alias("long", java.lang.Long.class);
			Assert.assertTrue(1975L == n.longValue());
		}
		{
			dao.lang.Primitive<java.lang.Float> p = __Float;
		  /*final java.lang.Integer row_id = */p.insert(1975.0f);
			dao.Context.getAdapterFor(this).alias(1975.0f, "float"); 
			final java.lang.Float n = (java.lang.Float)dao.Context.getAdapterFor(this).alias("float", java.lang.Float.class);
			Assert.assertTrue(1975.0f == n.floatValue());
		}
		{
			dao.lang.Primitive<java.lang.Double> p = __Double;
		  /*final java.lang.Integer row_id = */p.insert(1975.0);
			dao.Context.getAdapterFor(this).alias(1975.0, "double"); 
			final java.lang.Double n = (java.lang.Double)dao.Context.getAdapterFor(this).alias("double", java.lang.Double.class);
			Assert.assertTrue(1975.0 == n.intValue());
		}
		{
			dao.lang.Primitive<java.math.BigInteger> p = __BigInteger;
			final java.math.BigInteger BigInteger_1975 = new java.math.BigInteger("1975");
		  /*final java.lang.Integer row_id = */p.insert(BigInteger_1975);
			dao.Context.getAdapterFor(this).alias(BigInteger_1975, "bigint"); 
			final java.math.BigInteger n = (java.math.BigInteger)dao.Context.getAdapterFor(this).alias("bigint", java.math.BigInteger.class);
			Assert.assertTrue(true == BigInteger_1975.equals(n));
		}
		{
			dao.lang.Primitive<java.math.BigDecimal> p = __BigDecimal;
			final java.math.BigDecimal BigDecimal_1975 = new java.math.BigDecimal("1975.0");
		  /*final java.lang.Integer row_id = */p.insert(BigDecimal_1975);
			dao.Context.getAdapterFor(this).alias(BigDecimal_1975, "BigDecimal"); 
			final java.math.BigDecimal n = (java.math.BigDecimal)dao.Context.getAdapterFor(this).alias("BigDecimal", java.math.BigDecimal.class);
			Assert.assertTrue(true == BigDecimal_1975.equals(n));
		}
		{
			dao.lang.Primitive<java.lang.String> p = __String;
			final java.lang.String String_1975 = new java.lang.String("1975");
		  /*final java.lang.Integer row_id = */p.insert(String_1975);
			dao.Context.getAdapterFor(this).alias(String_1975, "String"); 
			final java.lang.String v = (java.lang.String)dao.Context.getAdapterFor(this).alias("String", java.lang.String.class);
			Assert.assertTrue(true == String_1975.equals(v));
		}
		{
			dao.lang.Primitive<java.util.Date> p = __Date;
			final java.util.Date Date_1975 = new java.util.Date(System.currentTimeMillis());
		  /*final java.lang.Integer row_id = */p.insert(Date_1975);
			dao.Context.getAdapterFor(this).alias(Date_1975, "Date"); 
			final java.util.Date d = (java.util.Date)dao.Context.getAdapterFor(this).alias("Date", java.util.Date.class);
			Assert.assertTrue(true == Date_1975.equals(d));
		}
	}
	
	@Test
	public void test_B0001() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
		final java.util.Set<java.lang.String> users =
			new dao.util.HashSet<java.lang.String>(){
				private static final long serialVersionUID = 0L;
				{
					add("arussell");
					add("rwang");
				}
			}
		;
//		dao.lang.Set p = __Set;
	  /*final java.lang.Integer row_id = *//*p.insert(users);*/
	  //dao.Context.getAdapterFor(this).alias(users, "users");
		users.remove("arussell");
		users.remove("rwang");
	}
	@Test
	public void test_B0002() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
		final java.util.Map<java.lang.String, java.util.Set<java.lang.String>> users =
			new dao.util.HashMap<java.lang.String, java.util.Set<java.lang.String>>(){
				private static final long serialVersionUID = 0L;
				{
					put("alex", new dao.util.HashSet<java.lang.String>(){
						private static final long serialVersionUID = 0L;
						{
							add("russell");
						}
					});
					put("richard", new dao.util.HashSet<java.lang.String>(){
						private static final long serialVersionUID = 0L;
						{
							add("rwang");
						}
					});
				}
			}
		;
//		dao.lang.Map p = __Map;
	  /*final java.lang.Integer row_id = *//*p.insert(users);*/
		dao.Context.getAdapterFor(this).alias(users, "users");
	}
	
	@Test
	public void test_A0001() {
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
		final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> users =
			new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("arussell", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("username", "arussell"); // TODO: update map (ind) .count accordingly
							put("password", "xxxxxxxx");
						}
					});
					put("rwang", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("username", "rwang");
							put("password", "xxxxx");
						}
					});
				}
			}
		;
//		dao.lang.Map p = __Map;
	  /*final java.lang.Integer row_id = *//*p.insert(users);*/
		dao.Context.getAdapterFor(this).alias(users, "users");
	}
	@SuppressWarnings("unchecked")
	@Test
	public void test_A0002() {
		final dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>> users = (
			dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>>
		)dao.Context.getAdapterFor(this).alias("users", java.util.Map.class);
		final dao.util.interface_Map<java.lang.String, java.lang.Object> arussell = users.get("arussell");
		System.out.println("users.get('arussell').get('password')='"+arussell.get("password")+"'");
		final dao.util.interface_Map<java.lang.String, java.lang.Object> rwang = users.get("rwang");
		System.out.println("users.get('rwang').get('password')='"+rwang.get("password")+"'");
	  /*final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}*/
	}
	@SuppressWarnings("unchecked")
	@Test
	public void test_A0003() {
		final dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>> users = (
			dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>>
		)dao.Context.getAdapterFor(this).alias("users", java.util.Map.class);
		final dao.util.interface_Map<java.lang.String, java.lang.Object> arussell = users.get("arussell");
		System.out.println("1#users.get('arussell').get('password')='"+arussell.get("password")+"'");
		arussell.put("password", "--------");
		System.out.println("2#users.get('arussell').get('password')='"+arussell.get("password")+"'");
	  /*final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}*/
	}
	@SuppressWarnings("unchecked")
	@Test
	public void test_A0004() {
		final dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>> users = (
			dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>>
		)dao.Context.getAdapterFor(this).alias("users", java.util.Map.class);
		final dao.util.interface_Map<java.lang.String, java.lang.Object> arussell = users.get("arussell");
		arussell.put("email", "alex@itfromblighty");
	  /*final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}*/
	}
	@SuppressWarnings("unchecked")
	@Test
	public void test_A0005() {
		final dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>> users = (
			dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>>
		)dao.Context.getAdapterFor(this).alias("users", java.util.Map.class);
		final dao.util.interface_Map<java.lang.String, java.lang.Object> arussell = users.get("arussell");
		System.out.println("users.get('arussell').get('email')='"+arussell.get("email")+"'");
	  /*final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}*/
	}
	@SuppressWarnings("unchecked")
	@Test
	public void test_A0006() {
		final dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>> users = (
			dao.util.interface_Map<java.lang.String, dao.util.interface_Map<java.lang.String, java.lang.Object>>
		)dao.Context.getAdapterFor(this).alias("users", java.util.Map.class);
		final dao.util.interface_Map<java.lang.String, java.lang.Object> arussell = users.get("arussell");
		arussell.remove("email");
	  /*final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}*/
	}
	
	@Test
	public void test_0002() {
	  /*{
			dao.lang.Array p = __Array;
			final java.lang.Integer[] array = new java.lang.Integer[]{3,5,7};
		*//*final java.lang.Integer row_id = *//*p.insert(array);
			dao.Context.getAdapterFor(this).alias(array, "Array");
			final java.lang.Object[] oa = (java.lang.Object[])dao.Context.getAdapterFor(this).alias("Array", java.lang.Integer[].class);
			Assert.assertTrue(array.length == oa.length);
			for (int i = 0; i < oa.length; i++) {
				Assert.assertTrue(array[i] == (java.lang.Integer)oa[i]);
			}
		}*/
		{
			dao.lang.List p = __List;
			final java.util.List<java.lang.Integer> list = new java.util.ArrayList<java.lang.Integer>(){
				private static final long serialVersionUID = 0L;
				{
					add(3);
					add(5);
					add(7);
				}
			};
		  /*final java.lang.Integer row_id = */p.insert(list);
			dao.Context.getAdapterFor(this).alias(list, "List");
			final java.util.List<?> __list = (java.util.List<?>)dao.Context.getAdapterFor(this).alias("List", java.util.List.class);
			Assert.assertTrue(true == list.containsAll(__list) && true == __list.containsAll(list));
		}
		{
			dao.lang.Set p = __Set;
			final java.util.Set<java.lang.Integer> set = new java.util.HashSet<java.lang.Integer>(){
				private static final long serialVersionUID = 0L;
				{
					add(3);
					add(5);
					add(7);
				}
			};
		  /*final java.lang.Integer row_id = */p.insert(set);
			dao.Context.getAdapterFor(this).alias(set, "Set");
			final java.util.Set<?> __set = (java.util.Set<?>)dao.Context.getAdapterFor(this).alias("Set", java.util.Set.class);
			Assert.assertTrue(true == set.containsAll(__set) && true == __set.containsAll(set));
		}
		{
			dao.lang.Map p = __Map;
			final java.util.Map<java.lang.String, java.lang.Integer> map = new java.util.HashMap<java.lang.String, java.lang.Integer>(){
				private static final long serialVersionUID = 0L;
				{
					put("three", 3);
					put("five", 5);
					put("seven", 7);
				}
			};
		  /*final java.lang.Integer row_id = */p.insert(map);
			dao.Context.getAdapterFor(this).alias(map, "Map"); 
			final java.util.Map<?, ?> __map = (java.util.Map<?, ?>)dao.Context.getAdapterFor(this).alias("Map", java.util.Map.class);
			for (final java.lang.String key: map.keySet()) {
				Assert.assertTrue(true == __map.containsKey(key) && map.get(key) == __map.get(key));
			}
			for (final java.lang.Object __key: __map.keySet()) {
				Assert.assertTrue(true == map.containsKey(__key) && __map.get(__key) == map.get(__key));
			}
		}
	}
	
	@Test
	public void test_0003() {
		{
			final dao.util.interface_List<java.util.Map<java.lang.String, java.lang.Integer>> list = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Integer>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<java.lang.String, java.lang.Integer>(){
						private static final long serialVersionUID = 0L;
						{
							put("three", 3);
							put("five", 5);
							put("seven", 7);
						}
					});
					add(new dao.util.HashMap<java.lang.String, java.lang.Integer>(){
						private static final long serialVersionUID = 0L;
						{
							put("two", 2);
							put("four", 4);
							put("six", 6);
						}
					});
				}
			};
			dao.lang.List p = __List;
		  /*final java.lang.Integer row_id = */p.insert(list);
			dao.Context.getAdapterFor(this).alias(list, "list-of-maps");
			final java.util.List<?> __list = (java.util.List<?>)dao.Context.getAdapterFor(this).alias("list-of-maps", java.util.List.class);
			Assert.assertTrue(true == list.containsAll(__list) && true == __list.containsAll(list));
		}
	}
	
	@Test
	public void test_0004() {
		{
			final dao.util.interface_Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> map =
				new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
					private static final long serialVersionUID = 0L;
					{
						put("arussell", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("me", "arussell");
							}
						});
						put("rwang", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("me", "rwang");
							}
						});
					}
				}
			;
		  //dao.lang.Map p = __Map;
		///*final java.lang.Integer row_id = */p.insert(map);
			dao.Context.getAdapterFor(this).alias(map, "map-of-maps");
			{
				final java.util.Map<?, ?> __map = (java.util.Map<?, ?>)dao.Context.getAdapterFor(this).alias("map-of-maps", java.util.Map.class);
				Assert.assertNotNull(__map);
			}
			{
				final java.util.Map<java.lang.String, java.lang.Object> arussell = (java.util.Map<java.lang.String, java.lang.Object>)map.get("arussell");
				final java.util.Map<java.lang.String, java.lang.Object> rwang = (java.util.Map<java.lang.String, java.lang.Object>)map.get("rwang");
				rwang.put("arussell", arussell);
				arussell.put("rwang", rwang);
			}
			dao.Context.resetLookup();
			{
				final java.util.Map<?, ?> __map = (java.util.Map<?, ?>)dao.Context.getAdapterFor(this).alias("map-of-maps", java.util.Map.class);
				@SuppressWarnings("unchecked")
				final java.util.Map<java.lang.String, java.lang.Object> arussell = (java.util.Map<java.lang.String, java.lang.Object>)__map.get("arussell");
				Assert.assertNotNull(arussell);
				@SuppressWarnings("unchecked")
				final java.util.Map<java.lang.String, java.lang.Object> rwang = (java.util.Map<java.lang.String, java.lang.Object>)__map.get("rwang");
				Assert.assertNotNull(rwang);
			}
		}
	}
	
	@Test
	public void test_0005() {
		{
			final dao.util.interface_Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> map =
				new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
					private static final long serialVersionUID = 0L;
					{
						put("arussell", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("username", "arussell");
								put("password", "xxxxxxxx");
							}
						});
						put("rwang", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("username", "rwang");
								put("password", "xxxxx");
							}
						});
					}
				}
			;
		  //dao.lang.Map p = __Map;
		///*final java.lang.Integer row_id = */p.insert(map);
			dao.Context.getAdapterFor(this).alias(map, "map-of-maps");
			{
				final java.util.Map<?, ?> __map = (java.util.Map<?, ?>)dao.Context.getAdapterFor(this).alias("map-of-maps", java.util.Map.class);
				Assert.assertNotNull(__map);
				@SuppressWarnings("unchecked")
				final java.util.Map<java.lang.String, java.lang.Object> arussell = (java.util.Map<java.lang.String, java.lang.Object>)__map.get("arussell");
				Assert.assertNotNull(arussell);
				arussell.put("password", "--------");
				map.remove("rwang");
			}
			dao.Context.resetLookup();
			{
				final java.util.Map<?, ?> __map = (java.util.Map<?, ?>)dao.Context.getAdapterFor(this).alias("map-of-maps", java.util.Map.class);
				Assert.assertNotNull(__map);
				@SuppressWarnings("unchecked")
				java.util.Map<java.lang.String, java.lang.Object> rwang =  (java.util.Map<java.lang.String, java.lang.Object>)__map.get("rwang");
				Assert.assertNull(rwang);
			}
		}
	}
	
  /*@After
	public void after() {
		Connection.shutdown();
		final java.io.File file = new java.io.File("dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
	}*/
}