package dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import dao.util.Access;

abstract public class Adapter extends dro.Adapter {
	public static class Return extends dro.Adapter.Return {
		public static final dao.Adapter.Return Adapter = (dao.Adapter.Return)null;
	}
	
	public static final java.lang.String ID = "ID";
	public static final java.lang.String ALIAS = "ALIAS";
	public static final java.lang.String TYPE = "TYPE";
	public static final java.lang.String REF_ID = "REF_ID";
	
	static final java.util.Map<java.lang.Class<?>, Access> access = new java.util.HashMap<>();
	
	public static Access access(final java.lang.Class<?> c, final Access a) {
		return access.put(c, a);
	}
	public static Access access(final java.lang.Class<?> c) {
		return access.get(c);
	}
	
	static {
		access.put(byte.class, new dao.data.Byte());
		access.put(java.lang.Byte.class, new dao.data.Byte());
		access.put(char.class, new dao.data.Character());
		access.put(java.lang.Character.class, new dao.data.Character());
		access.put(boolean.class, new dao.data.Boolean());
		access.put(java.lang.Boolean.class, new dao.data.Boolean());
		access.put(short.class, new dao.data.Short());
		access.put(java.lang.Short.class, new dao.data.Short());
		access.put(int.class, new dao.data.Integer());
		access.put(java.lang.Integer.class, new dao.data.Integer());
		access.put(long.class, new dao.data.Long());
		access.put(java.lang.Long.class, new dao.data.Long());
		access.put(float.class, new dao.data.Float());
		access.put(java.lang.Float.class, new dao.data.Float());
		access.put(double.class, new dao.data.Double());
		access.put(java.lang.Double.class, new dao.data.Double());
		access.put(java.math.BigInteger.class, new dao.data.BigInteger());
		access.put(java.math.BigDecimal.class, new dao.data.BigDecimal());
		access.put(java.lang.String.class, new dao.data.String());
		access.put(java.util.Date.class, new dao.data.Date());
		access.put(java.lang.Object[].class, new dao.lang.Array());
		access.put(java.util.List.class, new dao.lang.List());
		access.put(java.util.Set.class, new dao.lang.Set());
		access.put(java.util.Map.class, new dao.lang.Map());
	}
	
	protected java.lang.String q = "\"";
	
	protected java.util.Map<java.lang.String, java.lang.String> grammar = new HashMap<>();
	protected java.util.Set<java.lang.String> reserve = new HashSet<>();
	
	protected final java.util.Map<String, Boolean> tables = new HashMap<>();
	
	protected java.util.Map<java.lang.String, java.lang.Integer> pk;// = null;
	protected java.util.Map<java.lang.String, java.lang.String[]> cn;// = null;
	protected java.util.Map<java.lang.String, java.lang.String> sn;// = null;
	
	private java.util.Map<java.lang.Class<?>, java.lang.String> mapTypeToDatabase;// = new HashMap<>(); // Don't push this down (by visibility) to sub-classes
  //private java.util.Map<java.lang.String, java.lang.Class<?>> mapTypeFromDatabase;// = new HashMap<>(); // Don't push this down (by visibility) to sub-classes
	
	{
	  //grammar = new HashMap<java.lang.String, java.lang.String>();
	  //reserve = new HashSet<java.lang.String>();
		
		pk = new HashMap<java.lang.String, java.lang.Integer>();
		cn = new HashMap<java.lang.String, java.lang.String[]>();
		sn = new HashMap<java.lang.String, java.lang.String>();
		
	  //mapTypeToDatabase = new HashMap<java.lang.Class<?>, java.lang.String>();
	  //mapTypeFromDatabase = new HashMap<java.lang.String, java.lang.Class<?>>();
	}
	
	public Adapter() {
	  //super(); // Implicit super constructor..
	}
	
	public Adapter startup() {
		throw new IllegalStateException();
	  //return this;
	}
	
	public Adapter setTableSchema(final java.lang.String tableName, final java.lang.String schemaName) {
		this.sn.put(tableName, schemaName);
		return this;
	}
	
	protected java.lang.StringBuffer appendTableName(final java.lang.StringBuffer sb, final java.lang.String tableName) {
		if (true == this.sn.containsKey(tableName)) {
			this.__appendName(sb, this.sn.get(tableName)).append(".");
		}
		this.__appendName(sb, tableName);
		return sb;
	}
	protected java.lang.StringBuffer appendColumnName(final java.lang.StringBuffer sb, final java.lang.String columnName) {
		return this.__appendName(sb, columnName);
	}
	
	protected void reserve(final java.util.Set<java.lang.String> reserves) {
		for (final java.lang.String reserve: reserves) {
			if (false == this.reserve.contains(reserve.toUpperCase())) {
				this.reserve.add(reserve);
			}
		}
	}
	public void reserve(final java.lang.String reserve) {
		this.reserve.add(reserve);
	}
	
	protected Adapter grammar(final java.lang.String concept, final java.lang.String phrase) {
		this.grammar.put(concept, phrase);
		return this;
	}
	
	private java.lang.StringBuffer __appendName(final java.lang.StringBuffer sb, final java.lang.String name) {
		final boolean contains = this.reserve.contains(name.toUpperCase());
		if (true == contains) {
			sb.append(this.q);
		}
		sb.append(name);
		if (true == contains) {
			sb.append(this.q);
		}
		return sb;
	}
	
	protected void mapTypeToDatabase(final java.util.Map<java.lang.Class<?>, java.lang.String> mapToDatabase) {
		this.mapTypeToDatabase = mapToDatabase;
	}
  /*protected void mapTypeFromDatabase(final java.util.Map<java.lang.String, java.lang.Class<?>> mapFromDatabase) {
		this.mapTypeFromDatabase = mapFromDatabase;
	}*/
	
	// Insert
	private void alias(final java.lang.String alias, final java.lang.Class<?> c, final java.lang.Integer rowId) {
		if (null == alias) {
			throw new IllegalArgumentException();
		}
		this.createTableIfNotExists( // Note: we do this because on cold-start we don't know the primaryIndex - this is the only way we have (so far) to re-state the primaryIndex for last-row-id/generated-key for insert
			ALIAS, // String name
			0, // Integer primaryIdIndex
			new java.lang.String[]{ID,ALIAS,TYPE,REF_ID}, // String[] columnNames
			new Object[]{new java.math.BigInteger("0"),"","",new java.math.BigInteger("0")} // Object[] columnTypes
		);
	  /*final java.lang.Integer id = */this.insertIntoTable(
			ALIAS,
			new java.lang.String[]{ALIAS,TYPE,REF_ID},
			new java.lang.Object[]{alias,dao.lang.Class.getTypeReference(c),rowId}
		);
	}
	// Insert
	public void alias(final java.lang.Object o, final java.lang.String alias) {
	  //final java.lang.Class<?> c = (false == o.getClass().isArray()) ? o.getClass() : java.lang.Object[].class;
	  //final java.lang.Integer id = lookup.useObjectToGetRowId(c, o);
	  /*final */java.lang.Class<?> c = null;
		if (true == o.getClass().isArray()) {
			c = java.lang.Object[].class;
		} else if (true == o instanceof java.util.List) {
			c = java.util.List.class;
		} else if (true == o instanceof java.util.Set) {
			c = java.util.Set.class;
		} else if (true == o instanceof java.util.Map) {
			c = java.util.Map.class;
		} else {
			c = o.getClass();
		}
		final java.lang.Integer id = dao.Context.getLookupFor(this).useObjectToGetRowId(c, o);
		this.alias(alias, c, id);
	}
	// Select
	public java.lang.Object alias(final java.lang.String alias, final java.lang.Class<?> c) {
	  /*final */java.lang.Object o = null;
		if (null == alias) {
			throw new IllegalArgumentException();
		}
		if (true == this.existsTable(ALIAS)) {
			final java.lang.Object[][] oa = this.selectFromTable(
				ALIAS,
				new java.lang.String[]{TYPE,REF_ID}, // String[] selectColumnNames
				new java.lang.Object[]{"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
				new java.lang.String[]{TYPE,ALIAS}, // String[] whereColumnNames
				new java.lang.String[]{"=","="}, // String[] whereColumnOperators
				new java.lang.Object[]{/*FIXME*/dao.lang.Class.getTypeReference(c),alias} // Object[] whereColumnValues
			);
			if (null != oa && 1 == oa.length && 2 == oa[0].length) {
				o = Adapter.access(dao.lang.Class.getAccessClass(c)).select(new java.lang.Integer(oa[0][1].toString()));
			}
		}
		return o;
	}
	// Delete
	public void unalias(final java.lang.String alias) {
		if (null == alias) {
			throw new IllegalArgumentException();
		}
		if (true == this.existsTable(ALIAS)) {
			this.deleteFromTable(
				ALIAS,
				new java.lang.String[]{ALIAS}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{alias} // Object[] whereColumnValues
			);
		}
	}
	
	public boolean existsTable(final java.lang.String name) {
		return false;
	}
	
	public Adapter createTableIfNotExists(final java.lang.String name, final java.lang.Integer primaryIdIndex, final java.lang.String[] columnNames, final java.lang.Object[] columnTypes) {
		if (true == this.existsTable(name)) {
			if (null != primaryIdIndex) {
				this.pk.put(name, primaryIdIndex);
				this.cn.put(name, columnNames);
			}
		} else {
			this.createTable(name, primaryIdIndex, columnNames, columnTypes);
		}
		return this;
	}
	public Adapter createTable(final java.lang.String name, final java.lang.Integer primaryIdIndex, final java.lang.String[] columnNames, final java.lang.Object[] columnTypes) {
		try {
			this.tables.put(name, true);
			if (null != primaryIdIndex) {
				this.pk.put(name, primaryIdIndex);
				this.cn.put(name, columnNames);
			}
			final StringBuffer sb = new StringBuffer("CREATE TABLE").append(" ");
			this.appendTableName(sb, name).append("(");
			final java.lang.Object[] columnDescs = new java.lang.Object[columnTypes.length];
		  /*final */boolean comma = false;
			for (int i = 0; i < columnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (columnTypes[i] instanceof java.lang.Boolean) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Boolean.class);
				} else if (columnTypes[i] instanceof java.lang.Short) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Short.class);
				} else if (columnTypes[i] instanceof java.lang.Integer) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Integer.class);
				} else if (columnTypes[i] instanceof java.lang.Long) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Long.class);
				} else if (columnTypes[i] instanceof java.lang.Float) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Float.class);
				} else if (columnTypes[i] instanceof java.lang.Double) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Double.class);
				} else if (columnTypes[i] instanceof java.math.BigInteger) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.math.BigInteger.class);
				} else if (columnTypes[i] instanceof java.math.BigDecimal) {
					columnDescs[i] = this.mapTypeToDatabase.get(java.math.BigDecimal.class);
				} else if (columnTypes[i] instanceof java.lang.Byte) {
					if (null == (java.lang.Byte)columnTypes[i]) {
						columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Byte.class);
					} else {
						columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Byte.class)+" NOT NULL";
					}
				} else if (columnTypes[i] instanceof java.lang.Character) {
					if (null == (java.lang.Character)columnTypes[i]) {
						columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Character.class);
					} else {
						columnDescs[i] = this.mapTypeToDatabase.get(java.lang.Character.class)+" NOT NULL";
					}
				} else if (columnTypes[i] instanceof java.lang.String) {
					if (0 == ((java.lang.String)columnTypes[i]).length()) {
						columnDescs[i] = this.mapTypeToDatabase.get(java.lang.String.class);
					} else {
						columnDescs[i] = this.mapTypeToDatabase.get(java.lang.String.class)+" NOT NULL";
					}
				} else if (columnTypes[i] instanceof java.util.Date) {
					if (null == (java.util.Date)columnTypes[i]) {
						columnDescs[i] = this.mapTypeToDatabase.get(java.util.Date.class);
					} else {
						columnDescs[i] = this.mapTypeToDatabase.get(java.util.Date.class)+" NOT NULL";
					}
				} else {
					throw new UnsupportedOperationException();
				}
				this.appendColumnName(sb, columnNames[i]).append(" ").append(columnDescs[i]);
				if (null != primaryIdIndex && primaryIdIndex == i) {
					sb.append(" ").append(this.grammar.get("unique-index"));
				}
				comma = true;
			}
			sb.append(")");
			if (null != this.grammar.get("table-append")) {
				sb.append(" ").append(this.grammar.get("table-append"));
			}
			final java.lang.String sql = sb.toString();
System.out.println(sql);
			new Connection()
				.properties(super.properties())
				.createStatement(Connection.Return.Statement)
				.execute(sql, Statement.Return.Statement)
				.close(Statement.Return.Statement)
				.close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
	
	public java.lang.Integer insertIntoTable(final java.lang.String name, final java.lang.String[] insertColumnNames, final java.lang.Object[] insertColumnValues) {
		final Object[][] oa = new Object[1][];
		try {
			final StringBuffer sb = new StringBuffer("INSERT INTO").append(" ");
			this.appendTableName(sb, name).append("(");
		  /*final */boolean comma = false;
			for (int i = 0; i < insertColumnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				this.appendColumnName(sb, insertColumnNames[i]);
				comma = true;
			}
			sb.append(")").append(" ").append("VALUES(");
		  /*final boolean */comma = false;
			for (int i = 0; i < insertColumnValues.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				sb.append("?");
				comma = true;
			}
			final java.lang.String sql = sb.append(")").toString();
System.out.println(sql);
			final PreparedStatement ps;// = null;
			if (false == this.pk.containsKey(name)) {
				ps = new Connection()
					.properties(super.properties())
					.prepareStatement(sql, Connection.Return.PreparedStatement)
				;
			} else {
				final java.lang.Integer pk = this.pk.get(name);
				final java.lang.String[] cn = this.cn.get(name);
				final java.lang.String[] generatedKeys = new java.lang.String[]{cn[pk]};
				ps = new Connection()
					.properties(super.properties())
					.prepareStatement(sql, generatedKeys, Connection.Return.PreparedStatement)
				;
			}
			for (int i = 0; i < insertColumnValues.length; i++) {
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == insertColumnValues[i] instanceof java.lang.Boolean) {
					ps.setBoolean(1+i, ((java.lang.Boolean)insertColumnValues[i]).booleanValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Short) {
					ps.setShort(1+i, ((java.lang.Short)insertColumnValues[i]).shortValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Integer) {
					ps.setInt(1+i, ((java.lang.Integer)insertColumnValues[i]).intValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Long) {
					ps.setLong(1+i, ((java.lang.Long)insertColumnValues[i]).longValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Float) {
					ps.setFloat(1+i, ((java.lang.Float)insertColumnValues[i]).floatValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Double) {
					ps.setDouble(1+i, ((java.lang.Double)insertColumnValues[i]).doubleValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.math.BigInteger) {
				  //throw new IllegalArgumentException();
					ps.setLong(1+i, ((java.math.BigInteger)insertColumnValues[i]).longValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.math.BigDecimal) {
					ps.setBigDecimal(1+i, (java.math.BigDecimal)insertColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Byte) {
				  //throw new IllegalArgumentException();
					ps.setString(1+i, java.lang.Byte.toString((java.lang.Byte)insertColumnValues[i]), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.Character) {
					ps.setString(1+i, java.lang.Character.toString((java.lang.Character)insertColumnValues[i]), PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.lang.String) {
					ps.setString(1+i, (java.lang.String)insertColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == insertColumnValues[i] instanceof java.util.Date) {
					// https://javarevisited.blogspot.com/2015/10/how-to-convert-javautildate-to-javasqldate-jdbc-example.html
					ps.setLong(1+i, ((java.util.Date)insertColumnValues[i]).getTime(), PreparedStatement.Return.PreparedStatement);
				} else { /*FIXME*/ // We have to be able to access types here (from earlier table definition, like with primaryIndex) so that we don't have to pass-in columnTypes but we can use them instead of insertColumnValues..
					throw new IllegalArgumentException();
				}
System.out.println((1+i)+"="+insertColumnValues[i].toString());
			}
			if (null != pk) {
				ps.executeUpdate(PreparedStatement.Return.PreparedStatement)
				  .getGeneratedKeys(oa, PreparedStatement.Return.PreparedStatement)
				//.close(PreparedStatement.Return.Connection)
				  .close()
				;
			} else {
				ps.executeUpdate(PreparedStatement.Return.PreparedStatement)
				//.close(PreparedStatement.Return.Connection)
				  .close()
				;
			}
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return new java.lang.Integer(oa[0][0].toString());
	}
	
	public java.lang.Object[][] selectFromTable(final java.lang.String name, final java.lang.String[] selectColumnNames, final java.lang.Object[] selectColumnTypes, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, java.lang.Object[] whereColumnValues) {
	  /*final */java.util.List<java.lang.Object[]> list = null;
		try {
			final StringBuffer sb = new StringBuffer("SELECT").append(" ");
		  /*final */boolean comma = false;
			for (int i = 0; i < selectColumnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				this.appendColumnName(sb, selectColumnNames[i]);
				comma = true;
			}
			if (null != name) {
				sb.append(" ").append("FROM").append(" ");
				this.appendTableName(sb, name);
			}
			if (null != whereColumnNames) {
				sb.append(" ").append("WHERE").append(" ");
			  /*final */boolean and = false;
				for (int i = 0; i < whereColumnNames.length; i++) {
					if (true == and) {
						sb.append(" AND ");
					}
					this.appendColumnName(sb, whereColumnNames[i]);
					if (null == whereColumnOperators) {
						sb.append("=");
					} else {
						sb.append(whereColumnOperators[i]);
					}
					if (null == whereColumnValues[i] || true == whereColumnValues.equals("?")) {
						sb.append("?");
					} else if (true == whereColumnValues[i] instanceof java.lang.String) {
						sb.append("'")
						  .append(whereColumnValues[i])
						  .append("'");
					} else {
						sb.append(whereColumnValues[i].toString());
					}
					and = true;
				}
			}
			final java.lang.String sql = sb.toString();
System.out.println(sql);
			final PreparedStatement ps = new Connection()
				.properties(super.properties())
				.prepareStatement(sql, Connection.Return.PreparedStatement)
			;
			if (null != whereColumnNames) {
				for (int i = 0, j = 0; i < whereColumnValues.length; i++) {
					if (false == whereColumnValues[i].equals("?")) continue;
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
					} else if (true == whereColumnValues[j] instanceof java.lang.Short) {
						ps.setShort(1+j, ((java.lang.Short)whereColumnValues[j]).shortValue(), PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.lang.Integer) {
						ps.setInt(1+j, ((java.lang.Integer)whereColumnValues[j]).intValue(), PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.lang.Long) {
						ps.setLong(1+j, ((java.lang.Long)whereColumnValues[j]).longValue(), PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.lang.Float) {
						ps.setFloat(1+j, ((java.lang.Float)whereColumnValues[j]).floatValue(), PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.lang.Double) {
						ps.setDouble(1+j, ((java.lang.Double)whereColumnValues[j]).doubleValue(), PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.math.BigInteger) {
						throw new IllegalArgumentException();
					} else if (true == whereColumnValues[j] instanceof java.math.BigDecimal) {
						ps.setBigDecimal(1+j, (java.math.BigDecimal)whereColumnValues[j], PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.lang.String) {
						ps.setString(1+j, (java.lang.String)whereColumnValues[j], PreparedStatement.Return.PreparedStatement);
					} else if (true == whereColumnValues[j] instanceof java.util.Date) {
						// https://javarevisited.blogspot.com/2015/10/how-to-convert-javautildate-to-javasqldate-jdbc-example.html
						ps.setDate(1+j, new java.sql.Date(((java.util.Date)whereColumnValues[j]).getTime()), PreparedStatement.Return.PreparedStatement);
					} else {
						throw new IllegalArgumentException();
					}
System.out.println("?="+whereColumnValues[i].toString());
					j++;
				}
			}
			final ResultSet rs = ps
				.executeQuery(PreparedStatement.Return.ResultSet)
			;
			while (true == rs.next()) {
				if (null == list) {
					list = new ArrayList<java.lang.Object[]>();
				}
				final java.lang.Object[] selectedColumnValues = new java.lang.Object[selectColumnNames.length];
				for (int i = 0; i < selectColumnTypes.length; i++) {
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
					} else if (true == selectColumnTypes[i] instanceof java.lang.Boolean) {
						selectedColumnValues[i] = rs.getBoolean(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.lang.Short) {
						selectedColumnValues[i] = rs.getShort(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.lang.Integer) {
						selectedColumnValues[i] = rs.getInt(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.lang.Long) {
						selectedColumnValues[i] = rs.getLong(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.lang.Float) {
						selectedColumnValues[i] = rs.getFloat(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.lang.Double) {
						selectedColumnValues[i] = rs.getDouble(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.math.BigInteger) {
					  //throw new IllegalArgumentException();
						selectedColumnValues[i] = new java.math.BigInteger(((java.lang.Long)rs.getLong(1+i)).toString());
					} else if (true == selectColumnTypes[i] instanceof java.math.BigDecimal) {
						selectedColumnValues[i] = rs.getBigDecimal(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.lang.Byte) {
						selectedColumnValues[i] = new java.lang.Byte(rs.getString(1+i));
					} else if (true == selectColumnTypes[i] instanceof java.lang.Character) {
						selectedColumnValues[i] = new java.lang.Character(rs.getString(1+i).charAt(0));
					} else if (true == selectColumnTypes[i] instanceof java.lang.String) {
						selectedColumnValues[i] = rs.getString(1+i);
					} else if (true == selectColumnTypes[i] instanceof java.util.Date) {
						// https://javarevisited.blogspot.com/2015/10/how-to-convert-javautildate-to-javasqldate-jdbc-example.html
						selectedColumnValues[i] = new java.util.Date(rs.getLong(1+i));
					} else {
						throw new IllegalArgumentException();
					}
System.out.println((1+i)+"="+selectedColumnValues[i].toString());
				}
				list.add(selectedColumnValues);
			}
			rs.close(ResultSet.Return.PreparedStatement)
			//.close(PreparedStatement.Return.Connection)
			  .close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return null != list ? list.toArray(new java.lang.Object[0][0]) : new java.lang.Object[0][0];
	}
	
	public Adapter updateTableSet(final java.lang.String name, final java.lang.String[] updateColumnNames, final java.lang.Object[] updateColumnValues, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		try {
			final StringBuffer sb = new StringBuffer("UPDATE").append(" ");
			this.appendTableName(sb, name).append(" ").append("SET").append(" ");
		  /*final */boolean comma = false;
			for (int i = 0; i < updateColumnNames.length; i++) {
				if (true == comma) {
					sb.append(",");
				}
				this.appendColumnName(sb, updateColumnNames[i]).append("=").append("?");
				comma = true;
			}
			sb.append(" ").append("WHERE").append(" ");
		  /*final */boolean and = false;
			for (int i = 0; i < whereColumnNames.length; i++) {
				if (true == and) {
					sb.append(" AND ");
				}
				this.appendColumnName(sb, whereColumnNames[i]).append("=").append("?");
				and = true;
			}
			final java.lang.String sql = sb.toString();
System.out.println(sql);
			final PreparedStatement ps = new Connection()
				.properties(super.properties())
				.prepareStatement(sql, Connection.Return.PreparedStatement)
			;
		  /*final */int c = 1;
			for (int i = 0; i < updateColumnValues.length; i++, c++) {
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == updateColumnValues[i] instanceof java.lang.Short) {
					ps.setShort(c, ((java.lang.Short)updateColumnValues[i]).shortValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.lang.Integer) {
					ps.setInt(c, ((java.lang.Integer)updateColumnValues[i]).intValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.lang.Long) {
					ps.setLong(c, ((java.lang.Long)updateColumnValues[i]).longValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.lang.Float) {
					ps.setFloat(c, ((java.lang.Float)updateColumnValues[i]).floatValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.lang.Double) {
					ps.setDouble(c, ((java.lang.Double)updateColumnValues[i]).doubleValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.math.BigInteger) {
					throw new IllegalArgumentException();
				} else if (true == updateColumnValues[i] instanceof java.math.BigDecimal) {
					ps.setBigDecimal(c, (java.math.BigDecimal)updateColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.lang.String) {
					ps.setString(c, (java.lang.String)updateColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == updateColumnValues[i] instanceof java.util.Date) {
					// https://javarevisited.blogspot.com/2015/10/how-to-convert-javautildate-to-javasqldate-jdbc-example.html
					ps.setDate(c, new java.sql.Date( ((java.util.Date)updateColumnValues[i]) .getTime()) , PreparedStatement.Return.PreparedStatement);
				} else {
					throw new IllegalArgumentException();
				}
System.out.println((i+1)+"="+updateColumnValues[i].toString());
			}
		  /*final int c = 1*/;
			for (int i = 0; i < whereColumnValues.length; i++, c++) {
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == whereColumnValues[i] instanceof java.lang.Short) {
					ps.setShort(c, (java.lang.Short)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Integer) {
					ps.setInt(c, (java.lang.Integer)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Long) {
					ps.setLong(c, (java.lang.Long)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Float) {
					ps.setFloat(c, (java.lang.Float)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Double) {
					ps.setDouble(c, (java.lang.Double)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.math.BigInteger) {
				  //throw new IllegalArgumentException();
					ps.setLong(c, ((java.math.BigInteger)whereColumnValues[i]).longValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.math.BigDecimal) {
					ps.setBigDecimal(c, (java.math.BigDecimal)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.String) {
					ps.setString(c, (java.lang.String)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.util.Date) {
					// https://javarevisited.blogspot.com/2015/10/how-to-convert-javautildate-to-javasqldate-jdbc-example.html
					ps.setLong(c, ((java.util.Date)whereColumnValues[i]).getTime(), PreparedStatement.Return.PreparedStatement);
				} else {
					throw new IllegalArgumentException();
				}
System.out.println("?="+whereColumnValues[i].toString());
			}
			ps.executeUpdate(PreparedStatement.Return.PreparedStatement)
			//.close(PreparedStatement.Return.Connection)
			  .close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
	
	public Adapter deleteFromTable(final java.lang.String name, final java.lang.String[] whereColumnNames, final java.lang.String[] whereColumnOperators, final java.lang.Object[] whereColumnValues) {
		try {
			final StringBuffer sb = new StringBuffer("DELETE").append(" ").append("FROM").append(" ");
			this.appendTableName(sb, name).append(" ").append("WHERE").append(" ");
		  /*final */boolean and = false;
			for (int i = 0; i < whereColumnValues.length; i++) {
				if (true == and) {
					sb.append(" AND ");
				}
				this.appendColumnName(sb, whereColumnNames[i]).append("=").append("?");
				and = true;
			}
			final java.lang.String sql = sb.toString();
System.out.println(sql);
			final PreparedStatement ps = new Connection()
				.properties(super.properties())
				.prepareStatement(sql, Connection.Return.PreparedStatement)
			;
			for (int i = 0; i < whereColumnValues.length; i++) {
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == whereColumnValues[i] instanceof java.lang.Short) {
					ps.setShort(1+i, ((java.lang.Short)whereColumnValues[i]).shortValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Integer) {
					ps.setInt(1+i, ((java.lang.Integer)whereColumnValues[i]).intValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Long) {
					ps.setLong(1+i, ((java.lang.Long)whereColumnValues[i]).longValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Float) {
					ps.setFloat(1+i, ((java.lang.Float)whereColumnValues[i]).floatValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.Double) {
					ps.setDouble(1+i, ((java.lang.Double)whereColumnValues[i]).doubleValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.math.BigInteger) {
				  //throw new IllegalArgumentException();
					ps.setLong(1+i, ((java.math.BigInteger)whereColumnValues[i]).longValue(), PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.math.BigDecimal) {
					ps.setBigDecimal(1+i, (java.math.BigDecimal)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.lang.String) {
					ps.setString(1+i, (java.lang.String)whereColumnValues[i], PreparedStatement.Return.PreparedStatement);
				} else if (true == whereColumnValues[i] instanceof java.util.Date) {
					// https://javarevisited.blogspot.com/2015/10/how-to-convert-javautildate-to-javasqldate-jdbc-example.html
					ps.setDate(1+i, new java.sql.Date( ((java.util.Date)whereColumnValues[i]) .getTime()) , PreparedStatement.Return.PreparedStatement);
				} else {
					throw new IllegalArgumentException();
				}
System.out.println("?="+whereColumnValues[i].toString());
			}
			ps.executeUpdate(PreparedStatement.Return.PreparedStatement)
			//.close(PreparedStatement.Return.Connection)
			  .close()
			;
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
		}
		return this;
	}
	
	public Adapter dropTable(final java.lang.String name) {
		if (true == this.existsTable(name)) {
			try {
				final StringBuffer sb = new StringBuffer("DROP TABLE").append(" ");
				this.appendTableName(sb, name);
				final java.lang.String sql = sb.toString();
System.out.println(sql);
				new Connection()
					.properties(super.properties())
					.createStatement(Connection.Return.Statement)
					.execute(sql, Statement.Return.Statement)
					.close(Statement.Return.Statement)
					.close()
				;
				this.tables.remove(name);
			} catch (final SQLException e) {
				throw new RuntimeException(e);
			} finally {
			}
		}
		return this;
	}
	
	public Adapter shutdown() {
		throw new IllegalStateException();
	  //return this;
	}
}