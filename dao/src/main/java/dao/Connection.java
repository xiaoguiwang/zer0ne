package dao;

import java.io.IOException;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import dro.util.Logger;
import dro.util.Properties;

public final class Connection {
	private static final String className = Connection.class.getName();
	
	public static final class Return extends dro.lang.Return {
		public static final Connection       .Return Connection        = (Connection       .Return)null;
		public static final Statement        .Return Statement         = (Statement        .Return)null;
		public static final PreparedStatement.Return PreparedStatement = (PreparedStatement.Return)null;
		public static final Properties       .Return Properties        = (Properties       .Return)null;
	}
	public static class Property {
		public final static String url = dro.lang.Class.getName(java.sql.Connection.class)+"#url";
	}
	
	public static class Retry extends dro.util.Retry {
		public static class Handler extends dro.util.Retry.Handler {
			public Handler() {
				super();
			}
			public Handler(final dro.util.Properties p) {
				super(p);
			}
			protected void handler() throws SQLException {
				throw new IllegalStateException();
			}
			protected void handle() throws SQLException {
				while (true) {
					try {
						this.handler();
						break;
					} catch (final SQLException e) {
						if (true == super.shouldRetry()) {
							Logger.getLogger(Connection.className).log(
								Level.WARNING,
								dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- "+e.getMessage(),
								e.e
							);
							super.sleepBeforeRetry();
						} else {
							throw e;
						}
					}
				}
			}
		}
	}
	
	protected static final Map<Thread, java.sql.Connection> connections;// = new HashMap<Thread, java.sql.Connection>();
	protected static final Map<Thread, Map<String, java.sql.Statement>> statements;// = new HashMap<>();
	
	static {
		connections = new HashMap<Thread, java.sql.Connection>();
		statements = new HashMap<>();
	}
	
	protected Properties p;// = null;
	private String driver;// = null;
	private String url;// = null;
	protected Object[] call;
	private SQLException e;// = null
	
	{
		p = null;
		driver = null;
		url = null;
	  //e = null;
	}
	
	public Connection() {
	  //super(); // Implicit super constructor
	}
	public Connection(final java.sql.Connection c) {
	  //this(); // Implicit same-as this/super constructor
		this.setConnection(c);
	}
	public Connection(final String url) {
		this.url = url;
	  /*try {
			this.c = java.sql.DriverManager.getConnection(url);
		} catch (final java.sql.SQLException e) {
			this.exception(new SQLException(e));
		}*/
	}
	public Connection(final String url, final java.util.Properties info) {
		this.url = url;
		try {
			this.p = new dro.util.Properties(info);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	  /*try {
			c.put(Thread.currentThread(), java.sql.DriverManager.getConnection(url, this.p));
		} catch (final java.sql.SQLException e) {
			this.exception(new SQLException(e));
		}*/
	}
	public Connection(final String url, final String user, final String password) {
		this.url = url;
		this.p = new dro.util.Properties()
			.property("user", user)
			.property("password", password)
		;
	  /*try {
			this.c = java.sql.DriverManager.getConnection(url, user, password);
		} catch (final java.sql.SQLException e) {
			this.exception(new SQLException(e));
		}*/
	}
	
	protected void exception(final SQLException e) {
		this.e = e;
	}
	public SQLException exception() {
		return this.e;
	}
	
	public Connection properties(final dro.util.Properties p) { // FIXME
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	public dro.util.Properties returns(final Properties.Return r) {
		return this.p != null ? this.p : Properties.properties();
	}
	
	public Connection returns(final Connection.Return r) {
		return this;// != null ? this : null;
	}
	
	public java.sql.Connection getConnection() {
		synchronized(this) {
			if (true == dro.lang.String.isNullOrTrimmedBlank(this.driver)) {
				this.driver = this.returns(Return.Properties)
				                  .getProperty(dro.lang.Class.getName(java.sql.DriverManager.class)+"#class");
			}
			if (false == dro.lang.String.isNullOrTrimmedBlank(this.driver)) {
				try {
					java.lang.Class.forName(this.driver);
				} catch (final ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
		}
		final Thread t = Thread.currentThread();
		java.sql.Connection c;
		synchronized(connections) {
			c = connections.get(t);
		}
		if (null == c) {
			try {
				final Properties p = this.returns(Return.Properties);
				this.url = p.getProperty(dro.lang.Class.getName(java.sql.Connection.class)+"#url");
				final String user = p.getProperty(dro.lang.Class.getName(java.sql.Connection.class)+"#user");
				final String username = p.getProperty(dro.lang.Class.getName(java.sql.Connection.class)+"#username");
				final String password = p.getProperty(dro.lang.Class.getName(java.sql.Connection.class)+"#password");
				final java.util.Properties __props;
				if (false == dro.lang.String.isNullOrTrimmedBlank(user)
					||
					false == dro.lang.String.isNullOrTrimmedBlank(password)
				   ) {
					__props = new java.util.Properties(){
						private static final long serialVersionUID = 0L;
						{
							if (false == dro.lang.String.isNullOrTrimmedBlank(user)) {
								put("user", user);
							} else if (false == dro.lang.String.isNullOrTrimmedBlank(username)) {
								put("username", username);
							}
							put("password", password);
						}
					};
				} else {
					__props = null;
				}
				final Connection __this = this;
				new Retry.Handler(p){
					@Override
					public void handler() throws SQLException {
						try {
							final java.sql.Connection c;// = null;
							if (null == __props) {
								c = DriverManager.getConnection(__this.url);
							} else {
								c = DriverManager.getConnection(__this.url, __props);
							}
							synchronized(connections) {
								connections.put(t, c);
							}
						} catch (final java.sql.SQLException e) {
							throw new SQLException(e, __this);
						} finally {
						}
					}
				}
				.handle();
			} catch (final SQLException e) {
				Logger.getLogger(Connection.className).log(
					Level.SEVERE,
					dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- "+e.getMessage(),
					e.e
				);
				throw new RuntimeException(e);
			}
			c = connections.get(t);
		}
		if (null == c && null != this.e) throw new RuntimeException(this.e);
		return c;
	}
	public void setConnection(final java.sql.Connection c) {
		final Thread t = Thread.currentThread();
		connections.put(t, c);
	}
	
	public Statement createStatement(final Statement.Return r) throws SQLException {
		this.call = new Object[]{"createStatement"};
		try {
			return new Statement(this.getConnection().createStatement())
				.connection(this)
				.returns(Return.Statement)
			;
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Statement createStatement(final int resultSetType, final int resultSetConcurrency, final Statement.Return r) throws SQLException {
		this.call = new Object[]{"createStatement",resultSetType,resultSetConcurrency};
		try {
			return new Statement(this.getConnection().createStatement(resultSetType, resultSetConcurrency))
				.connection(this)
				.returns(Return.Statement)
			;
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Statement createStatement(final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability, final Statement.Return r) throws SQLException {
		this.call = new Object[]{"createStatement",resultSetType,resultSetConcurrency,resultSetHoldability};
		try {
			return new Statement(this.getConnection().createStatement(resultSetType, resultSetConcurrency, resultSetHoldability))
				.connection(this)
				.returns(Return.Statement)
			;
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	
	public Statement prepareCall(final String sql, final Statement.Return r) throws SQLException {
		this.call = new Object[]{"prepareCall",sql};
		try {
			return new Statement(this.getConnection().prepareCall(sql))
				.connection(this)
				.returns(Return.Statement)
			;
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Statement prepareCall(final String sql, final int resultSetType, final int resultSetConcurrency, final Statement.Return r) throws SQLException {
		this.call = new Object[]{"prepareCall",sql,resultSetType,resultSetConcurrency};
		try {
			return new Statement(this.getConnection().prepareCall(sql, resultSetType, resultSetConcurrency))
				.connection(this)
				.returns(Return.Statement)
			;
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Statement prepareCall(final String sql, final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability, final Statement.Return r) throws SQLException {
		this.call = new Object[]{"prepareCall",sql,resultSetType,resultSetConcurrency,resultSetHoldability};
		try {
			return new Statement(this.getConnection().prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability))
				.connection(this)
				.returns(Return.Statement)
			;
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	
	private Map<String, java.sql.Statement> newOrExistingMap(final Thread t) {
		Map<String, java.sql.Statement> map = statements.get(t);
		if (null == map) {
			map = new HashMap<String, java.sql.Statement>();
			statements.put(t, map);
		}
		return map;
	}
	public PreparedStatement prepareStatement(final String sql, final PreparedStatement.Return r) throws SQLException {
		this.call = new Object[]{"prepareStatement",sql};
		final java.sql.Statement __s;
	  /*synchronized(statements) {
			Map<String, java.sql.Statement> map = this.newOrExistingMap(Thread.currentThread());
			if (false == map.containsKey(sql)) {
			  */try {
					__s = this.getConnection().prepareStatement(sql);
				  /*map.put(sql, __s);*/
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this);
				}/*
			} else {
				__s = map.get(sql);
			}
		}*/
		return new PreparedStatement((java.sql.PreparedStatement)__s)
			.connection(this)
			.returns(Return.PreparedStatement)
		;
	}
	public PreparedStatement prepareStatement(final String sql, final int autoGeneratedKeys, final PreparedStatement.Return r) throws SQLException {
		this.call = new Object[]{"prepareStatement",sql,autoGeneratedKeys};
		final java.sql.Statement __s;
		synchronized(statements) {
			Map<String, java.sql.Statement> map = this.newOrExistingMap(Thread.currentThread());
			if (false == map.containsKey(sql)) {
				try {
					__s = this.getConnection().prepareStatement(sql, autoGeneratedKeys);
					map.put(sql, __s);
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this);
				}
			} else {
				__s = map.get(sql);
			}
		}
		return new PreparedStatement((java.sql.PreparedStatement)__s)
			.connection(this)
			.returns(Return.PreparedStatement)
		;
	}
	public PreparedStatement prepareStatement(final String sql, final int[] columnIndexes, final PreparedStatement.Return r) throws SQLException {
		this.call = new Object[]{"prepareStatement",sql,columnIndexes};
		final java.sql.Statement __s;
		synchronized(statements) {
			Map<String, java.sql.Statement> map = this.newOrExistingMap(Thread.currentThread());
			if (false == map.containsKey(sql)) {
				try {
					__s = this.getConnection().prepareStatement(sql, columnIndexes);
					map.put(sql, __s);
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this);
				}
			} else {
				__s = map.get(sql);
			}
		}
		return new PreparedStatement((java.sql.PreparedStatement)__s)
			.connection(this)
			.returns(Return.PreparedStatement)
		;
	}
	public PreparedStatement prepareStatement(final String sql, final String[] columnNames, final PreparedStatement.Return r) throws SQLException {
		this.call = new Object[]{"prepareStatement",sql,columnNames};
		final java.sql.Statement __s;
	  /*synchronized(statements) {
			Map<String, java.sql.Statement> map = this.newOrExistingMap(Thread.currentThread());
			if (false == map.containsKey(sql)) {
			  */try {
					__s = this.getConnection().prepareStatement(sql, columnNames);
				  /*map.put(sql, __s);*/
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this);
				}/*
			} else {
				__s = map.get(sql);
			}
		}*/
		return new PreparedStatement((java.sql.PreparedStatement)__s)
			.connection(this)
			.returns(Return.PreparedStatement)
		;
	}
	public PreparedStatement prepareStatement(final String sql, final int resultSetType, final int resultSetConcurrency, final PreparedStatement.Return r) throws SQLException {
		this.call = new Object[]{"prepareStatement",sql,resultSetType,resultSetConcurrency};
		final java.sql.Statement __s;
		synchronized(statements) {
			Map<String, java.sql.Statement> map = this.newOrExistingMap(Thread.currentThread());
			if (false == map.containsKey(sql)) {
				try {
					__s = this.getConnection().prepareStatement(sql, resultSetType, resultSetConcurrency);
					map.put(sql, __s);
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this);
				}
			} else {
				__s = map.get(sql);
			}
		}
		return new PreparedStatement((java.sql.PreparedStatement)__s)
			.connection(this)
			.returns(Return.PreparedStatement)
		;
	}
	public PreparedStatement prepareStatement(final String sql, final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability, final PreparedStatement.Return r) throws SQLException {
		this.call = new Object[]{"prepareStatement",sql,resultSetType,resultSetConcurrency,resultSetHoldability};
		final java.sql.Statement __s;
		synchronized(statements) {
			Map<String, java.sql.Statement> map = this.newOrExistingMap(Thread.currentThread());
			if (false == map.containsKey(sql)) {
				try {
					__s = this.getConnection().prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
					map.put(sql, __s);
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this);
				}
			} else {
				__s = map.get(sql);
			}
		}
		return new PreparedStatement((java.sql.PreparedStatement)__s)
			.connection(this)
			.returns(Return.PreparedStatement)
		;
	}
	
	public Connection close() throws SQLException {
		final Thread t = Thread.currentThread();
		final Map<String, java.sql.Statement> map = statements.get(t);
		if (null != map) {
			for (final java.sql.Statement s: map.values()) {
				try {
					if (false == s.isClosed()) {
						s.close();
					}
				} catch (final java.sql.SQLException e) {
					throw new SQLException(e, this); // Silently ignore.. (TODO: the non-java.sql.SQLException version i.e. the wrapper version)
				}
			}
			statements.remove(t);
		}
		final java.sql.Connection c = connections.get(t);
		if (null != c) {
			try {
				if (false == c.isClosed()) {
					c.close();
				}
			} catch (final java.sql.SQLException e) {
			  //throw new SQLException(e, this); // Silently ignore, especially in the case of 'connection broken' (whatever that might be)
			}
			connections.remove(t);
		}
		return this;
	}
	
	public static void shutdown() {
		synchronized(statements) {
			for (final Thread t: statements.keySet()) {
				final Map<String, java.sql.Statement> map = statements.get(t);
				for (final java.sql.Statement s: map.values()) {
					try {
						if (false == s.isClosed()) {
							s.close();
						}
					} catch (final java.sql.SQLException e) {
					  //throw new SQLException(e, this); // Silently ignore.. (TODO: the non-java.sql.SQLException version i.e. the wrapper version)
					} finally {
					  //s = null;
					}
				}
				final Iterator<Map.Entry<String, java.sql.Statement>> s = map.entrySet().iterator();
				while (true == s.hasNext()) {
					s.next();
					s.remove();
				}
			}
		}
		for (final Thread t: connections.keySet()) {
			final java.sql.Connection c = connections.get(t);
			try {
				if (false == c.isClosed()) {
					c.close();
				}
			} catch (final java.sql.SQLException e) {
			  //throw new SQLException(e, this); // Silently ignore, especially in the case of 'connection broken' (whatever that might be)
			} finally {
			  //c = null;
			}
		}
		final Iterator<Map.Entry<Thread, java.sql.Connection>> c = connections.entrySet().iterator();
		while (true == c.hasNext()) {
			c.next();
			c.remove();
		}
	}
}