package dao;

import java.io.FileNotFoundException;
import java.io.IOException;

import dro.util.Properties;

public class Context {
	public static class Lookup {
		public/*FIXME*/ final java.util.Map<java.lang.Class<?>, java.util.Map<java.lang.Object, java.lang.Integer>> objectToRowId = new java.util.HashMap<>();
		public void useObjectToSetRowId(final java.lang.Class<?> c, /*final */java.lang.Object o, final java.lang.Integer rowId) {
		  /*final */java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = this.objectToRowId.get(c);
			if (null == objectToRowId) {
			  /*final java.util.Map<java.lang.Object, java.lang.Integer> */objectToRowId = new java.util.HashMap<java.lang.Object, java.lang.Integer>();
				this.objectToRowId.put(c, objectToRowId);
			}
		////if (true == o instanceof dro.lang.Id) {
		////o = ((dro.lang.Id)o).id();
		////}
			o = dao.lang.Class.associate(c, o);
			objectToRowId.put(o, rowId);
		}
		public java.lang.Integer useObjectToGetRowId(final java.lang.Class<?> c, /*final */java.lang.Object o) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = this.objectToRowId.get(c);
		////if (true == o instanceof dro.lang.Id) {
		////	o = ((dro.lang.Id)o).id();
		////}
			o = dao.lang.Class.associated(c, o);
			if (null != objectToRowId) {
				rowId = objectToRowId.get(o);
			}
			return rowId;
		}
		public java.lang.Integer useObjectToRemoveRowId(final java.lang.Class<?> c, /*final */java.lang.Object o) {
		  /*final */java.lang.Integer rowId = null;
		  /*final */java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = this.objectToRowId.get(c);
			if (null != objectToRowId) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.disassociate(c, o);
				rowId = objectToRowId.remove(o);
				if (0 == objectToRowId.size()) {
					this.objectToRowId.remove(c);
				}
			}
			return rowId;
		}
		
	  /*private */final java.util.Map<java.lang.Class<?>, java.util.Map<java.lang.Integer, java.lang.Object>> rowIdToObject = new java.util.HashMap<>();
		public void useRowIdToSetObject(final java.lang.Class<?> c, final java.lang.Integer rowId, /*final */java.lang.Object o) {
		  /*final */java.util.Map<java.lang.Integer, java.lang.Object> rowIdToObject = this.rowIdToObject.get(c);
			if (null == rowIdToObject) {
			  /*final java.util.Map<java.lang.Integer, java.lang.Integer> */rowIdToObject = new java.util.HashMap<java.lang.Integer, java.lang.Object>();
				this.rowIdToObject.put(c, rowIdToObject);
			}
		  /*if (true == o instanceof dro.lang.Id) {
				o = ((dro.lang.Id)o).id();
			}*/
			rowIdToObject.put(rowId, o);
		}
		public java.lang.Object useRowIdToGetObject(final java.lang.Class<?> c, final java.lang.Integer rowId) {
		  /*final */java.lang.Object o = null;
			final java.util.Map<java.lang.Integer, java.lang.Object> rowIdToObject = this.rowIdToObject.get(c);
			if (null != rowIdToObject) {
				o = rowIdToObject.get(rowId);
			}
		////if (true == o instanceof dro.util.Id) {
		////	o = ((dro.util.Id)o).object();
		////}
		////o = dao.lang.Class.associated(c, o);
			return o;
		}
		public java.lang.Object useRowIdToRemoveObject(final java.lang.Class<?> c, final java.lang.Integer rowId) {
		  /*final */java.lang.Object o = null;
		  /*final */java.util.Map<java.lang.Integer, java.lang.Object> rowIdToObject = this.rowIdToObject.get(c);
			if (null != rowIdToObject) {
				o = rowIdToObject.remove(rowId);
				if (0 == rowIdToObject.size()) {
					this.rowIdToObject.remove(c);
				}
			}
		////if (true == o instanceof dro.util.Id) {
		////	o = ((dro.util.Id)o).object();
		////}
		////o = dao.lang.Class.disassociate(c, o);
			return o;
		}
		
	  /*private */final java.util.Map<java.lang.Class<?>, java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>>> objectAndIndexToRowId = new java.util.HashMap<>();
	  /*private */final java.util.Map<java.lang.Class<?>, java.util.Map<java.lang.Object, java.util.List<java.lang.Integer>>> objectToIndexes = new java.util.HashMap<>();
		public void useObjectAndIndexToSetRowId(final java.lang.Class<?> c, /*final */java.lang.Object o, final java.lang.Integer index, final java.lang.Object p, final java.lang.Integer rowId) {
		  /*final */java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>> objectAndIndexToRowId = this.objectAndIndexToRowId.get(c);
			if (null == objectAndIndexToRowId) {
			  /*java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>> */objectAndIndexToRowId = new java.util.HashMap<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>>();
				this.objectAndIndexToRowId.put(c, objectAndIndexToRowId);
			}
		  /*final */java.util.Map<java.lang.Object, java.util.List<java.lang.Integer>> objectToIndexes = this.objectToIndexes.get(c);
			if (null == objectToIndexes) {
			  /*java.util.Map<java.lang.Object, java.util.List<java.lang.Integer>> */objectToIndexes = new java.util.HashMap<java.lang.Object, java.util.List<java.lang.Integer>>();
				this.objectToIndexes.put(c, objectToIndexes);
			}
		////if (true == o instanceof dro.lang.Id) {
		////	o = ((dro.lang.Id)o).id();
		////}
			o = dao.lang.Class.associate(c, o);
		  /*final */java.util.Map<java.lang.Integer, java.lang.Integer> indexToRowId = objectAndIndexToRowId.get(o);
			if (null == indexToRowId) {
				indexToRowId = new java.util.HashMap<java.lang.Integer, java.lang.Integer>();
				objectAndIndexToRowId.put(o, indexToRowId);
			}
			indexToRowId.put(index, rowId);
		  /*final */java.util.List<java.lang.Integer> indexes = objectToIndexes.get(p);
			if (null == indexes) {
				indexes = new java.util.ArrayList<java.lang.Integer>();
				objectToIndexes.put(p, indexes);
			}
			indexes.add(index);
		}
		public java.lang.Integer useObjectAndIndexToGetRowId(final java.lang.Class<?> c, /*final */java.lang.Object o, final java.lang.Integer index) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>> objectAndIndexToRowId = this.objectAndIndexToRowId.get(c);
			if (null != objectAndIndexToRowId) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.associated(c, o);
			  /*final */java.util.Map<java.lang.Integer, java.lang.Integer> indexToRowId = objectAndIndexToRowId.get(o);
				if (null != indexToRowId) {
					rowId = indexToRowId.get(index);
				}
			}
			return rowId;
		}
		public java.lang.Integer useObjectAndFirstToGetRowId(final java.lang.Class<?> c, /*final*/ java.lang.Object o, /*final */java.lang.Object p) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>> objectAndIndexToRowId = this.objectAndIndexToRowId.get(c);
			final java.util.Map<java.lang.Object, java.util.List<java.lang.Integer>> objectToIndexes = this.objectToIndexes.get(c);
			if (null != objectAndIndexToRowId && null != objectToIndexes) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.associated(c, o);
				final java.util.Map<java.lang.Integer, java.lang.Integer> indexToRowId = objectAndIndexToRowId.get(o);
				if (null != indexToRowId) {
				  //p = dao.lang.Class.associated(dao.lang.Class.getAccessClass(p), p);
					final java.util.List<java.lang.Integer> indexes = objectToIndexes.get(p);
					if (null != indexes) {
						final java.lang.Integer index = indexes.get(0);
						if (null != index) {
							rowId = indexToRowId.get(index);
						}
					}
				}
			}
			return rowId;
		}
		public java.lang.Integer useObjectAndIndexToRemoveRowId(final java.lang.Class<?> c, /*final */java.lang.Object o, final java.lang.Integer index) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>> objectAndIndexToRowId = this.objectAndIndexToRowId.get(c);
			if (null != objectAndIndexToRowId) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.disassociate(c, o);
			  /*final */java.util.Map<java.lang.Integer, java.lang.Integer> indexToRowId = objectAndIndexToRowId.get(o);
				if (null != indexToRowId) {
					rowId = indexToRowId.remove(index);
					if (0 == indexToRowId.size()) {
						objectAndIndexToRowId.remove(o);
						if (0 == objectAndIndexToRowId.size()) {
							this.objectAndIndexToRowId.remove(c);
						}
					}
				}
			}
			return rowId;
		}
		public java.lang.Integer useObjectAndFirstToRemoveRowId(final java.lang.Class<?> c, /*final*/ java.lang.Object o, /*final */java.lang.Object p) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.util.Map<java.lang.Integer, java.lang.Integer>> objectAndIndexToRowId = this.objectAndIndexToRowId.get(c);
			final java.util.Map<java.lang.Object, java.util.List<java.lang.Integer>> objectToIndexes = this.objectToIndexes.get(c);
			if (null != objectAndIndexToRowId && null != objectToIndexes) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.disassociate(c, o);
			  /*final */java.util.Map<java.lang.Integer, java.lang.Integer> indexToRowId = objectAndIndexToRowId.get(o);
				if (null != indexToRowId) {
				  //p = dao.lang.Class.disassociate(dao.lang.Class.getAccessClass(p), p);
					final java.util.List<java.lang.Integer> indexes = objectToIndexes.get(p);
					if (null != indexes) {
						final java.lang.Integer index = indexes.remove(0);
						if (null != index) {
							if (0 == indexes.size()) {
								objectToIndexes.remove(p);
								if (0 == objectToIndexes.size()) {
									this.objectToIndexes.remove(c);
								}
							}
							rowId = indexToRowId.remove(index);
							if (0 == indexToRowId.size()) {
								objectAndIndexToRowId.remove(o);
								if (0 == objectAndIndexToRowId.size()) {
									this.objectAndIndexToRowId.remove(c);
								}
							}
						}
					}
				}
			}
			return rowId;
		}
		
	  /*private */final java.util.Map<java.lang.Class<?>, java.util.Map<java.lang.Object, java.util.Map<java.lang.Object, java.lang.Integer>>> objectAndUniqueToRowId = new java.util.HashMap<>();
		public void useObjectAndUniqueToSetRowId(final java.lang.Class<?> c, /*final*/ java.lang.Object o, /*final */java.lang.Object u, java.lang.Integer rowId) {
		  /*final */java.util.Map<java.lang.Object, java.util.Map<java.lang.Object, java.lang.Integer>> objectAndUniqueToRowId = this.objectAndUniqueToRowId.get(c);
			if (null == objectAndUniqueToRowId) {
			  /*java.util.Map<java.lang.Object, java.util.Map<java.lang.Object, java.lang.Integer>> */objectAndUniqueToRowId = new java.util.HashMap<java.lang.Object, java.util.Map<java.lang.Object, java.lang.Integer>>();
				this.objectAndUniqueToRowId.put(c, objectAndUniqueToRowId);
			}
		////if (true == o instanceof dro.lang.Id) {
		////	o = ((dro.lang.Id)o).id();
		////}
			o = dao.lang.Class.associate(c, o);
		  /*final */java.util.Map<java.lang.Object, java.lang.Integer> uniqueToRowId = objectAndUniqueToRowId.get(o);
			if (null == uniqueToRowId) {
				uniqueToRowId = new java.util.HashMap<java.lang.Object, java.lang.Integer>();
				objectAndUniqueToRowId.put(o, uniqueToRowId);
			}
		  //u = dao.lang.Class.associate(dao.lang.Class.getAccessClass(u), u);
			uniqueToRowId.put(u, rowId);
		}
		public java.lang.Integer useObjectAndUniqueToGetRowId(final java.lang.Class<?> c, /*final*/ java.lang.Object o, /*final */java.lang.Object u) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.util.Map<java.lang.Object, java.lang.Integer>> objectAndUniqueToRowId = this.objectAndUniqueToRowId.get(c);
			if (null != objectAndUniqueToRowId) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.associated(c, o);
			  /*final */java.util.Map<java.lang.Object, java.lang.Integer> uniqueToRowId = objectAndUniqueToRowId.get(o);
				if (null != uniqueToRowId) {
				  //u = dao.lang.Class.associated(dao.lang.Class.getAccessClass(u), u);
					rowId = uniqueToRowId.get(u);
				}
			}
			return rowId;
		}
		public java.lang.Integer useObjectAndUniqueToRemoveRowId(final java.lang.Class<?> c, /*final*/ java.lang.Object o, /*final */java.lang.Object u) {
		  /*final */java.lang.Integer rowId = null;
			final java.util.Map<java.lang.Object, java.util.Map<java.lang.Object, java.lang.Integer>> objectAndUniqueToRowId = this.objectAndUniqueToRowId.get(c);
			if (null != objectAndUniqueToRowId) {
			////if (true == o instanceof dro.lang.Id) {
			////	o = ((dro.lang.Id)o).id();
			////}
				o = dao.lang.Class.disassociate(c, o);
			  /*final */java.util.Map<java.lang.Object, java.lang.Integer> uniqueToRowId = objectAndUniqueToRowId.get(o);
				if (null != uniqueToRowId) {
				  //u = dao.lang.Class.disassociate(dao.lang.Class.getAccessClass(u), u);
					rowId = uniqueToRowId.remove(u);
					if (0 == uniqueToRowId.size()) {
						objectAndUniqueToRowId.remove(o);
						if (0 == objectAndUniqueToRowId.size()) {
							this.objectAndUniqueToRowId.remove(c);
						}
					}
				}
			}
			return rowId;
		}
	}
	
  //private static final Context context;// = null;
	private static /*final */Adapter __adapter = null;
	private static /*final */Lookup lookup = new Lookup(); // TODO: link lookup with Context and adapter gets lookup from context
	
	static {
	  /*context = */new dao.Context();
	}
	
	public static Adapter getAdapterFor(final java.lang.Object o) {
		return __adapter;
	}
	
	public static Lookup getLookupFor(final java.lang.Object o) {
		return lookup;
	}
	public static void resetLookup() {
		lookup = new Lookup();
	}
	
	{
		try {
			if (null == Properties.properties()) {
				Properties.properties(new Properties(dao.Context.class));
			}
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		synchronized(this.getClass()) {
			if (null == __adapter) {
			  //__adapter = (dao.Adapter)dao.Adapter.newAdapter("dao.sqlite.Adapter");
				__adapter = (dao.Adapter)dao.Adapter.newAdapter("dao.mysql.Adapter");
			}
		}
	}
}