package dao;

import java.sql.SQLException;
import java.util.Properties;

public class DriverManager {
  //static void java.sql.DriverManager.deregisterDriver(Driver driver);

  //static java.sql.Connection getConnection(String url);
	public static Connection getConnection(final String url) throws SQLException {
		return new Connection(java.sql.DriverManager.getConnection(url));
	}

  //static java.sql.Connection getConnection(String url, Properties info);
	static Connection getConnection(final String url, final Properties info) throws SQLException {
		return new Connection(java.sql.DriverManager.getConnection(url, info));
	}

  //static java.sql.Connection getConnection(String url, String user, String password);
	static Connection getConnection(final String url, final String user, final String password) throws SQLException {
		return new Connection(java.sql.DriverManager.getConnection(url, user, password));
	}

  //static Driver java.sql.DriverManager.getDriver(String url);
  //static Enumeration java.sql.DriverManager.getDrivers();
  //static int java.sql.DriverManager.getLoginTimeout();
  //static PrintStream java.sql.DriverManager.getLogStream();
  //static PrintWriter java.sql.DriverManager.getLogWriter();
  //static void java.sql.DriverManager.println(String message);
  //static void java.sql.DriverManager.registerDriver(Driver driver);
  //static void java.sql.DriverManager.registerDriver(Driver driver, DriverAction da);
  //static void java.sql.DriverManager.setLoginTimeout(int seconds);
  //static void java.sql.DriverManager.setLogStream(PrintStream out);
  //static void java.sql.DriverManager.setLogWriter(PrintWriter out);

	static {
	}

	{
	}

  //private String url;
  //private String username;
  //private String password;
	
	{
	  //url = null;
	  //username = null;
	  //password = null;
	}
	
  /*private DriverManager() {
	}*/
}