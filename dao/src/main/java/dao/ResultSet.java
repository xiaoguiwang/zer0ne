package dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class ResultSet {
	public static class Return extends dro.lang.Return {
		public static final ResultSet        .Return ResultSet         = (ResultSet        .Return)null;
		public static final Statement        .Return Statement         = (Statement        .Return)null;
		public static final PreparedStatement.Return PreparedStatement = (PreparedStatement.Return)null;
	}
	
  //protected Connection c = null;
	protected Statement s = null;
	protected java.sql.ResultSet rs = null;
	private SQLException e;// = null;
	
	{
	  //c = null;
	  //s = null;
	  //rs = null;
	  //e = null;
	}
	
	ResultSet(final java.sql.ResultSet rs) {
		this.rs = rs;
	}
	
	protected ResultSet statement(final Statement s) {
		this.s = s;
		return this;
	}
	public Statement statement() {
		return this.s;
	}
	
	protected void exception(final SQLException e) {
		this.e = e;
	}
	public SQLException exception() {
		return this.e;
	}
	
	public Statement returns(final Statement.Return r) {
		return this.s;// != null ? this : null;
	}
	public PreparedStatement returns(final PreparedStatement.Return r) {
		return (PreparedStatement)this.s;// != null ? this : null;
	}
	public ResultSet returns(final ResultSet.Return r) {
		return this;// != null ? this : null;
	}
	
	public java.sql.ResultSet getResultSet() {
		if (null == this.rs && null != this.e) throw new RuntimeException(this.e);
		return this.rs;
	}
	
	public java.sql.ResultSetMetaData getMetaData() throws SQLException {
		if (null == this.rs && null != this.e) throw new RuntimeException(this.e);
		try {
			return this.rs.getMetaData();
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	
	public BigDecimal getBigDecimal(final int arg0) throws SQLException {
		try {
			return this.rs.getBigDecimal(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public BigDecimal getBigDecimal(final String arg0) throws SQLException {
		try {
			return this.rs.getBigDecimal(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public boolean getBoolean(final int arg0) throws SQLException {
		try {
			return this.rs.getBoolean(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public boolean getBoolean(final String arg0) throws SQLException {
		try {
			return this.rs.getBoolean(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Date getDate(final int arg0) throws SQLException {
		try {
			return this.rs.getDate(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Date getDate(final String arg0) throws SQLException {
		try {
			return this.rs.getDate(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public double getDouble(final int arg0) throws SQLException {
		try {
			return this.rs.getDouble(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public double getDouble(final String arg0) throws SQLException {
		try {
			return this.rs.getDouble(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Object getObject(final int arg0) throws SQLException {
		try {
			return this.rs.getObject(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Object getObject(final String arg0) throws SQLException {
		try {
			return this.rs.getObject(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public float getFloat(final int arg0) throws SQLException {
		try {
			return this.rs.getFloat(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public float getFloat(final String arg0) throws SQLException {
		try {
			return this.rs.getFloat(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public int getInt(final int arg0) throws SQLException {
		try {
			return this.rs.getInt(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public int getInt(final String arg0) throws SQLException {
		try {
			return this.rs.getInt(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public long getLong(final int arg0) throws SQLException {
		try {
			return this.rs.getLong(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public long getLong(final String arg0) throws SQLException {
		try {
			return this.rs.getLong(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public short getShort(final int arg0) throws SQLException {
		try {
			return this.rs.getShort(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public short getShort(final String arg0) throws SQLException {
		try {
			return this.rs.getShort(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public String getString(final int arg0) throws SQLException {
		try {
			return this.rs.getString(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public String getString(final String arg0) throws SQLException {
		try {
			return this.rs.getString(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Time getTime(final int arg0) throws SQLException {
		try {
			return this.rs.getTime(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Time getTime(final String arg0) throws SQLException {
		try {
			return this.rs.getTime(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Timestamp getTimestamp(final int arg0) throws SQLException {
		try {
			return this.rs.getTimestamp(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	public Timestamp getTimestamp(final String arg0) throws SQLException {
		try {
			return this.rs.getTimestamp(arg0);
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
	}
	
	public boolean next() throws SQLException {
		boolean next = false;
		if (null != this.rs) {
			try {
				next = this.rs.next();
			} catch (final java.sql.SQLException e) {
				throw new SQLException(e, this);
			}
		}
		return next;
	}
	protected volatile boolean next;// = false;
	public ResultSet next(final ResultSet.Return r) throws SQLException {
		this.next = this.next();
		return this;
	}
	
	public ResultSet close() throws SQLException {
		if (null != this.rs) {
			try {
				if (false == this.rs.isClosed()) {
					this.rs.close();
				}
			} catch (final java.sql.SQLException e) {
				throw new SQLException(e, this);
			} finally {
				this.rs = null;
			}
		}
		return this;
	}
	public ResultSet close(final ResultSet.Return r) throws SQLException {
		return this.close();
	}
	public PreparedStatement close(final PreparedStatement.Return r) throws SQLException {
		return this.close().returns(Return.PreparedStatement);
	}
	public Statement close(final Statement.Return r) throws SQLException {
		return this.close().returns(Return.Statement);
	}
}