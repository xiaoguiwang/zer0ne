package dao;

import org.junit.Assert;

import dao.Connection;

// TODO: fully-wrap (assuming it's possible) all SQLException methods so we can choose this class without having to distinguish java.sql and without risking incompatible behaviour
// TODO: but it's not quite that simple as every method that could throw a com.ibm.mq.MQException would need to be wrapped to re-throw a non-java.sql SQLException!..
public class SQLException extends Exception {
	private static final long serialVersionUID = 0L;
	
	public static class Return extends dro.lang.Return {
		public static final Connection.Return Connection = (Connection.Return)null;
		public static final Statement .Return Statement  = (Statement .Return)null;
	}
	
	public static java.sql.SQLException nextException(final java.sql.SQLException e) {
		return e.getNextException();
	}
	
	public static void assertEquals(final SQLException expected, final java.sql.SQLException actual) {
		Assert.assertEquals( "SQLState not what was", expected.getSQLState (), actual.getSQLState ());
		Assert.assertEquals("ErrorCode not what was", expected.getErrorCode(), actual.getErrorCode());
	}
	
	public String SQLState;
	public int vendorCode;
	protected java.sql.SQLException e;
	
	public SQLException() {
		super();
	}
	public SQLException(String SQLState, int vendorCode) {
		this();
		this.SQLState = SQLState;
		this.vendorCode = vendorCode;
	}
	SQLException(final java.sql.SQLException e) {
		this(e.getSQLState(), e.getErrorCode());
		this.e = e;
	}
	SQLException(final java.sql.SQLException e, final Connection c) {
		this(e.getSQLState(), e.getErrorCode());
		this.e = e;
		try {
			c.close();
		} catch (final SQLException ex) {
			// Silently ignore..
		}
	}
	SQLException(final java.sql.SQLException e, final Statement s) {
		this(e.getSQLState(), e.getErrorCode());
		this.e = e;
		final Connection c = s.returns(Return.Connection);
		try {
			s.close();
		} catch (final SQLException ex) {
			// Silently ignore..
		}
		try {
			c.close();
		} catch (final SQLException ex) {
			// Silently ignore..
		}
	}
	SQLException(final java.sql.SQLException e, final ResultSet rs) {
		this(e.getSQLState(), e.getErrorCode());
		this.e = e;
		final Statement s = rs.returns(Return.Statement);
		try {
			rs.close();
		} catch (SQLException ex) {
			// Silently ignore..
		}
		final Connection c = s.returns(Return.Connection);
		try {
			s.close();
		} catch (SQLException ex) {
			// Silently ignore..
		}
		try {
			c.close();
		} catch (SQLException ex) {
			// Silently ignore..
		}
	}
	
	public String getSQLState() {
		return this.SQLState;
	}
	public int getErrorCode() {
		return this.vendorCode;
	}
	
	public SQLException exception(final java.sql.SQLException e) {
		return this;
	}
	public java.sql.SQLException exception() {
		return this.e;
	}
	
	public java.lang.String getMessage() {
		return this.e.getMessage();
	}
	
	@Override
	public boolean equals(final Object o) {
		boolean equals = false;
		if (null != o) {
			if (o instanceof java.sql.SQLException) {
				final java.sql.SQLException e = (java.sql.SQLException)o;
				if (e.getErrorCode() == this.vendorCode && e.getSQLState() == this.SQLState) {
					equals = true;
				}
			}
		}
		return equals;
	}
}