package dao;

import java.util.ArrayList;
import java.util.List;

public class Statement {
	public static class Return extends dro.lang.Return {
		public static final Statement .Return Statement  = (Statement .Return)null;
		public static final ResultSet .Return ResultSet  = (ResultSet .Return)null;
		public static final Connection.Return Connection = (Connection.Return)null;
	}
	
	protected Connection c = null;
	protected java.sql.Statement s = null;
	protected ResultSet rs = null;
	protected Exception e;// = null;
	protected Exception ignoreException;// = null;
	{
	  //c = null;
	  //s = null;
	  //rs = null;
	  //e = null;
	  //ignoreException = null;
	}
	protected Statement(final java.sql.Statement s) {
		this.statement(s);
	}
	protected Statement connection(final Connection c) {
		this.c = c;
		return this;
	}
	protected Statement statement(final java.sql.Statement s) {
		this.s = s;
		return this;
	}
	protected java.sql.Statement statement() {
		return this.s;
	}
	protected void exception(final java.lang.Exception e) { // FIXME
		this.e = new Exception(e);
	}
	public java.lang.Exception exception() { // FIXME
		return this.e;
	}
	public Connection returns(final Connection.Return r) {
		return this.c;
	}
	public Statement returns(final Statement.Return r) {
		return this;
	}
	
	public java.sql.Statement getStatement() throws SQLException {
		if (null == this.s) {
			if (null != this.c.call) {
				final Object[] call = this.c.call;
				if (true == call[0].equals("createStatement")) {
					if (1 == call.length) {
						try {
							this.s = this.c.getConnection().createStatement();
						} catch (final java.sql.SQLException e) {
							throw new SQLException(e, this);
						}
					} else if (3 == call.length || 4 == call.length) {
						final int resultSetType = (Integer)call[1];
						final int resultSetConcurrency = (Integer)call[2];
						try {
							this.s = this.c.getConnection().createStatement(resultSetType, resultSetConcurrency);
						} catch (final java.sql.SQLException e) {
							throw new SQLException(e, this);
						}
						if (3 == call.length) {
						} else {//if (4 == call.length) {
							final int resultSetHoldability = (Integer)call[3];
							try {
								this.s = this.c.getConnection().createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
							} catch (final java.sql.SQLException e) {
								throw new SQLException(e, this);
							}
						}
					} else {
						throw new IllegalStateException();
					}
				} else if (true == call[0].equals("prepareCall")) {
					String sql = (String)call[1];
					if (2 == call.length) {
						try {
							this.s = this.c.getConnection().prepareCall(sql);
						} catch (final java.sql.SQLException e) {
							throw new SQLException(e, this);
						}
					} else if (4 == call.length || 5 == call.length) {
						final int resultSetType = (Integer)call[2];
						final int resultSetConcurrency = (Integer)call[3];
						if (4 == call.length) {
							try {
								this.s = this.c.getConnection().prepareCall(sql, resultSetType, resultSetConcurrency);
							} catch (final java.sql.SQLException e) {
								throw new SQLException(e, this);
							}
						} else {//if (5 == call.length) {
							final int resultSetHoldability = (Integer)call[4];
							try {
								this.s = this.c.getConnection().prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
							} catch (final java.sql.SQLException e) {
								throw new SQLException(e, this);
							}
						}/* else {
							throw new IllegalStateException();
						}*/
					} else {
						throw new IllegalStateException();
					}
				} else if (true == call[0].equals("prepareStatement")) {
					throw new IllegalArgumentException();
				} else {
					throw new IllegalStateException();
				}
			}
		}
		if (null == this.s && null != this.e) throw new RuntimeException(this.e);
		return this.s;
	}
	
	public Statement ignoreException(final Exception ignoreException) {
		this.ignoreException = ignoreException;
		return this;
	}
	
	protected volatile boolean execute;// = false;
	public Statement execute(final java.lang.String arg0, final Statement.Return r) throws SQLException {
		try {
			this.execute = this.getStatement().execute(arg0);
		} catch (final java.sql.SQLException e) {
			if (null == this.ignoreException || false == this.ignoreException.equals(e)) {
				throw new SQLException(e, this);
			}
		}
		return this;
	}
	protected volatile int[] executeBatch;// = new int[]{0,..};
	public Statement executeBatch(final java.lang.String[] arg0, final Statement.Return r) throws SQLException {
		try {
			this.getStatement().clearBatch();
			for (final java.lang.String s: arg0) {
				java.lang.String[] sa = s.split("\\r?\\n");
				for (final java.lang.String __s: sa) {
					this.getStatement().addBatch(__s);
				}
			}
		} catch (final java.sql.SQLException e) {
			throw new SQLException(e, this);
		}
		try {
			this.executeBatch = this.getStatement().executeBatch();
		} catch (final java.sql.SQLException e) {
			if (null == this.ignoreException || false == this.ignoreException.equals(e)) {
				throw new SQLException(e, this);
			}
		}
		return this;
	}
	public /*?*/void executeBatch(final java.lang.String[][] arg0) throws SQLException {
		List<java.lang.String> list = null;
		try {
			this.getStatement().clearBatch();
			for (final java.lang.String[] sa: arg0) {
				StringBuffer sb = null;
				for (final java.lang.String s: sa) {
					if (null != s && 0 < s.trim().length()) {
						if (null == sb) {
							sb = new StringBuffer();
						}
						sb.append(s);
					}
				}
				if (null != sb && 0 < sb.length()) {
					final java.lang.String s = sb.toString();
					if (null == list) {
						list = new ArrayList<>();
					}
					list.add(s);
					this.getStatement().addBatch(s);
				}
			}
			try {
				this.executeBatch = this.getStatement().executeBatch();
			} catch (final java.sql.SQLException e) {
				if (null == this.ignoreException || false == this.ignoreException.equals(e)) {
					throw e;
				}
			}
		} catch (java.sql.SQLException e) {
			final java.lang.String[] sa = list.toArray(new java.lang.String[0]);
			for (final java.lang.String s: sa) {
				System.err.println(s); // TODO: Logger.debug
			}
			this.exception(e);
			while (null != e) {
				e.printStackTrace(System.err);
				e = e.getNextException();
			}
		}
	}
	public Statement executeBatch(final java.lang.String[][] arg0, final Statement.Return r) throws SQLException {
		this.executeBatch(arg0);
		return this;
	}
	
	public ResultSet executeQuery(final String arg0, final ResultSet.Return r) throws SQLException {
		try {
			this.rs = new ResultSet(this.getStatement().executeQuery(arg0))
				.statement(this)
				.returns(Return.ResultSet)
			;
		} catch (final java.sql.SQLException e) {
			if (null == this.ignoreException || false == this.ignoreException.equals(e)) {
				throw new SQLException(e, this);
			}
		}
		return this.rs;
	}
	
	protected volatile int executeUpdate;// = 0;
	public void executeUpdate(final String arg0) throws SQLException {
		try {
			this.executeUpdate = this.getStatement().executeUpdate(arg0);
		} catch (final java.sql.SQLException e) {
			if (null == this.ignoreException || false == this.ignoreException.equals(e)) {
				throw new SQLException(e, this);
			}
		}
	}
	public Statement executeUpdate(final String arg0, final Statement.Return r) throws SQLException {
		this.executeUpdate(arg0);
		return this;
	}
	public /*?*/void executeUpdate(final java.lang.String[][] arg0) throws SQLException { // TODO: resurrect catch multiple exceptions during the array iteration.. (as done in executeBatch)
		for (final java.lang.String[] sa: arg0) {
			StringBuffer sb = null;
			for (final java.lang.String s: sa) {
				if (null != s && 0 < s.trim().length()) {
					if (null == sb) {
						sb = new StringBuffer();
					}
					sb.append(s);
				}
			}
			if (null != sb && 0 < sb.length()) {
				try {
					this.executeUpdate(sb.toString());
				} catch (final SQLException e) {
					if (null == this.ignoreException || false == this.ignoreException.equals(e)) {
						throw e;
					}
				}
			}
		}
	}
	public Statement executeUpdate(final java.lang.String[][] arg0, final Statement.Return r) throws SQLException {
		this.executeUpdate(arg0);
		return this;
	}
	
	public Statement/*void?*/ close() throws SQLException {
		if (null != this.s) {
			try {
			  //if (false == this.s.isClosed()) {
					this.s.close();
			  //}
			} catch (final java.sql.SQLException e) {
				throw new SQLException(e, this);
			} finally {
				this.s = null;
			}
		}
		return this;
	}
	public Statement close(final Statement.Return r) throws SQLException {
		this.close();
		return this;
	}
	public Connection close(final Connection.Return r) throws SQLException {
		this.close(Return.Statement);
		return this.c;
	}
}