package dao.data;

import dao.lang.Primitive;

public class Boolean extends Primitive<java.lang.Boolean> {
	public Boolean() {
		super(true);
	}/*
	@Override
	public java.lang.Boolean newOne(final java.lang.Object o) {
		return null == o ? null: java.lang.Boolean.parseBoolean(o.toString());
	}*/
}