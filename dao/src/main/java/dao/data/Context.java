package dao.data;

import dro.lang.Event.Listener;

// This is dao.data.Context
public class Context extends dro.data.Context {
	protected static final java.lang.String metaData = "META_DATA";
	protected static final java.lang.String[] metaDataColumnNames = new java.lang.String[]{"ID","NAME","VERSION"};
  //protected static final java.lang.String[] metaDataColumnNameNoId = new java.lang.String[]{"NAME","VERSION"};
	protected static final java.lang.String metaDataField = "META_DATA_FIELD";
	protected static final java.lang.String[] metaDataFieldColumnNames = new java.lang.String[]{"ID","MD#ID","NAME","TYPE","SIZE"};
  //protected static final java.lang.String[] metaDataFieldColumnNameNoId = new java.lang.String[]{"MD#ID","NAME","TYPE","SIZE"};
	protected static final java.lang.String data = "DATA";
	protected static final java.lang.String[] dataColumnNames = new java.lang.String[]{"ID","MD#ID"};
  //protected static final java.lang.String[] dataColumnNameNoId = new java.lang.String[]{"MD#ID"};
	protected static final java.lang.String dataField = "DATA_FIELD";
	protected static final java.lang.String[] dataFieldColumnNames = new java.lang.String[]{"ID","D#ID","MD#ID","MDF#ID","VALUE"};
  //protected static final java.lang.String[] dataFieldColumnNameNoId = new java.lang.String[]{"D#ID","MD#ID","MDF#ID","VALUE"};
	
	protected dao.Adapter adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public Context() {
	  //super(); // Implicit super constructor..
		this.adapter = (dao.Adapter)dao.Adapter.newAdapter(super.p.getProperty(dro.lang.Class.getName(super.getClass())+"#adapter"));
	}
	private boolean init = false;
	public Context(final java.lang.String adapterClassName) {
	  //super(); // Implicit super constructor..
		this.adapter = (dao.Adapter)dao.Adapter.newAdapter(adapterClassName);
		final dao.data.Context __context = this;
		this.el = new dro.lang.Event.Listener(){
			@Override
			public Listener dispatch(final dro.lang.Event e) {
				if (false == __context.init) { 
				  //__context.invoke(null, null, "$init");
					__context.adapter.startup();
				  //__context.adapter.shutdown();
					__context.init = true;
				}
				__context.invoke(e);
				return this;
			}
		};
		dro.lang.Event.register(this.el, new java.lang.Class<?>[]{dro.meta.Data.class, dro.Data.class}, (dro.Data[])null, (java.lang.String[])null);
	}
	
  //@Override
	public Context invoke(final dro.lang.Event e) {
		
		return this;
	}
  //@Override
	public java.lang.Object invoke(final dro.meta.Data md, final dro.Data d, final java.lang.String action) {
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (null == md && null == d) {
			
			if (true == action.equals("exists")) {
			
				if (true == this.adapter.existsTable(metaData)) {
					if (true == this.adapter.existsTable(metaDataField)) {
						if (true == this.adapter.existsTable(data)) {
							if (true == this.adapter.existsTable(dataField)) {
							}
						}
					}
				}
			
			}
		
		} else if (null != md && null == d) {
			
			if (true == action.equals("exists")) {
				
				if (true == this.adapter.existsTable(metaData)) {
					if (true == this.adapter.existsTable(metaDataField)) {
						final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
						if (true == this.adapter.existsTable(name)) {
						}
					}
				}
			
			}
		
		} else if (null != md && null != d) {
		} else if (null == md && null != d) {
		} else {
		}
		
		{
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
				
			} else if (true == action.equals("create")) {
				
				if (false == this.adapter.existsTable(metaData)) {
					this.adapter.createTable(
						metaData,
						new java.lang.Integer(0),
						metaDataColumnNames, // "ID","NAME","VERSION"
						new java.lang.String[]{"INTEGER","TEXT NOT NULL","TEXT"} // FIXME: these data types are specific to SQLite
					);
				}
				
				if (false == this.adapter.existsTable(metaDataField)) {
					this.adapter.createTable(
						metaDataField,
						new java.lang.Integer(0),
						metaDataFieldColumnNames, // "ID","MD#ID","NAME","TYPE","SIZE"
						new java.lang.String[]{"INTEGER","INTEGER NOT NULL","TEXT NOT NULL","TEXT","INTEGER"} // FIXME: these data types are specific to SQLite
					);
				}
				
				if (false == this.adapter.existsTable(data)) {
					this.adapter.createTable(
						data,
						new java.lang.Integer(0),
						dataColumnNames, // "ID","MD#ID"
						new java.lang.String[]{"INTEGER","INTEGER NOT NULL"} // FIXME: these data types are specific to SQLite
					);
				}
				
				if (false == this.adapter.existsTable(dataField)) {
					this.adapter.createTable(
						dataField,
						new java.lang.Integer(0),
						dataFieldColumnNames, // "ID","D#ID","MD#ID","MDF#ID","VALUE"
						new java.lang.String[]{"INTEGER","INTEGER NOT NULL","INTEGER NOT NULL","INTEGER NOT NULL","TEXT"} // FIXME: these data types are specific to SQLite
					);
				}
			
			} else if (true == action.equals("insert")) {
			
				if (true == this.adapter.existsTable(metaData)) {
					if (true == this.adapter.existsTable(metaDataField)) {
						if (true == this.adapter.existsTable(data)) {
							if (true == this.adapter.existsTable(dataField)) {
							} else {
							  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
							}
						} else {
						  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
						}
					} else {
					  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
					}
				} else {
				  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
				}
			
			} else if (true == action.equals("select")) {
				
				// Implementation: looping, one-by-one
				if (true == this.adapter.existsTable(metaData)) {
					final java.lang.Object[][] metaDataColumnValues = this.adapter.selectFromTable(
						metaData,
						metaDataColumnNames, // "ID","NAME","VERSION"
						new java.lang.Object[]{
							new java.lang.Integer((int)0),
							new java.lang.String(""),
							new java.lang.String("")
						},
						(java.lang.String[])null,
						(java.lang.String[])null,
						(java.lang.Object[])null
					);
					for (final java.lang.Object[] mdcv: metaDataColumnValues) {
						final java.lang.Integer __md_id__  = (java.lang.Integer)mdcv[0];
						final java.lang.String __md_name__ = (java.lang.String)mdcv[1];
					  //final java.lang.String __md_version__ = (java.lang.String)mdcv[2];
						final dro.meta.Data __md__ = new dro.meta.Data()
							.set("$id", __md_id__)
							.set("$name", __md_name__)
						  //.set("$version", __md_version__)
						;
						this.add(__md__);
						
						if (true == this.adapter.existsTable(metaDataField)) {
							final java.lang.Object[][] metaDataFieldColumnValues = this.adapter.selectFromTable(
								metaDataField,
								metaDataFieldColumnNames, // "ID","MD#ID","NAME","TYPE","SIZE"
								new java.lang.Object[]{
									new java.lang.Integer((int)0),
									new java.lang.Integer((int)0),
									new java.lang.String(""),
									new java.lang.String(""),
									new java.lang.Integer(0)
								},
								new java.lang.String[]{"MD#ID"},
								new java.lang.String[]{"="},
								new java.lang.Object[]{new java.lang.Integer(__md_id__)}
							);
							for (final java.lang.Object[] mdfcv: metaDataFieldColumnValues) {
								final java.lang.Integer __mdf_id__ = (java.lang.Integer)mdfcv[0];
							  //final java.lang.Integer __mdf_md_id__ = (java.lang.Integer)mdfcv[1];
								final java.lang.String __mdf_name__ = (java.lang.String)mdfcv[2];
								final java.lang.Object __mdf_type__ = (java.lang.Object)mdfcv[3];
								final dro.meta.data.Field __mdf__ = new dro.meta.data.Field()
									.set("$id",  __mdf_id__)
									.set("$dro$meta$Data", __md__)
									.set("$name", __mdf_name__)
									.set("$type", __mdf_type__)
								;
								__md__.add(__mdf__);
							}
							
							if (true == this.adapter.existsTable(data)) {
								final java.lang.Object[][] dataColumnValues = this.adapter.selectFromTable(
									data,
									dataColumnNames, // "ID","MD#ID"
									new java.lang.Object[]{
										new java.lang.Integer((int)0),
										new java.lang.Integer((int)0)
									},
									new java.lang.String[]{"MD#ID"},
									new java.lang.String[]{"="},
									new java.lang.Object[]{new java.lang.Integer(__md_id__)}
								);
								for (final java.lang.Object[] dcv: dataColumnValues) {
									final java.lang.Integer __d_id__  = (java.lang.Integer)dcv[0];
								  //final java.lang.Integer __d_md_id__ = (java.lang.String)dcv[1];
									final dro.Data __d__ = new dro.Data(__md__)
										.set("$id", __d_id__)
									  //.set("$dro$meta$Data", __md__)
									;
									__md__.add(__d__);
									
									if (true == this.adapter.existsTable(dataField)) {
										final java.lang.Object[][] dataFieldColumnValues = this.adapter.selectFromTable(
											dataField,
											dataFieldColumnNames, // "ID","D#ID","MD#ID","MDF#ID","VALUE"
											new java.lang.Object[]{
												new java.lang.Integer((int)0),
												new java.lang.Integer((int)0),
												new java.lang.Integer((int)0),
												new java.lang.Integer((int)0),
												new java.lang.String(""),
											},
											new java.lang.String[]{"D#ID","MD#ID"},
											new java.lang.String[]{"=","="},
											new java.lang.Object[]{new java.lang.Integer(__d_id__),new java.lang.Integer(__md_id__)}
										);
										for (final java.lang.Object[] dfcv: dataFieldColumnValues) {
											final java.lang.Integer __df_id__ = (java.lang.Integer)dfcv[0];
										  //final java.lang.Integer __df_d_id__ = (java.lang.Integer)dfcv[1];
										  //final java.lang.Integer __df_md_id__ = (java.lang.Integer)dfcv[2];
										  //final java.lang.Integer __df_mdf_id__ = (java.lang.Integer)dfcv[3];
											final java.lang.String __df_value__ = (java.lang.String)dfcv[4];
											final dro.data.Field __df__ = d.returns(__df_id__, dro.data.Field.Return.Field);
											__d__.set(__df__, __df_value__);
										}
									} else {
									  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
									}
									
								}
							} else {
							  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
							}
							
						} else {
						  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
						}
						
					}
				} else {
				  //throw new IllegalStateException(); // Not first-time around, no, this is a legal state
				}
			
			} else if (true == action.equals("update")) {
				
				if (true == this.adapter.existsTable(metaData)) {
					if (true == this.adapter.existsTable(metaDataField)) {
						if (true == this.adapter.existsTable(data)) {
							if (true == this.adapter.existsTable(dataField)) {
							}
						}
					}
				}
			
			} else if (true == action.equals("delete")) {
				
				if (true == this.adapter.existsTable(metaData)) {
					if (true == this.adapter.existsTable(metaDataField)) {
						if (true == this.adapter.existsTable(data)) {
							if (true == this.adapter.existsTable(dataField)) {
							}
						}
					}
				}
			
			} else if (true == action.equals("drop")) {
				
				if (true == this.adapter.existsTable(metaData)) {
					this.adapter.dropTable(metaData);
				}
				if (true == this.adapter.existsTable(metaDataField)) {
					this.adapter.dropTable(metaDataField);
				}
				if (true == this.adapter.existsTable(data)) {
					this.adapter.dropTable(data);
				}
				if (true == this.adapter.existsTable(dataField)) {
					this.adapter.dropTable(dataField);
				}
			
			}
		}
		return null;
	}
  //@Override
	public java.lang.Object invoke(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
	  /*final */java.lang.Object returns = null;
		final java.lang.String meta = "zer0ne_meta";
		final java.lang.String[] metaColumnNames = new java.lang.String[]{"MD#NAME"};
		final java.lang.String meta_data = "zer0ne_meta_data";
		final java.lang.String[] metaDataColumnNames = new java.lang.String[]{"MD#ID","MDF#NAME","MDF#TYPE"};
		if (false == methodName.equals("$init")) {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (false == this.adapter.existsTable(meta)) {
				if (false == this.adapter.existsTable(meta_data)) {
					// False && False
					this.adapter.createTable(meta, new java.lang.Integer(0), new java.lang.String[]{"ID","MD#NAME"}, new java.lang.String[]{"INTEGER","TEXT NOT NULL"});
					this.adapter.createTable(meta_data, new java.lang.Integer(0), new java.lang.String[]{"ID","MD#ID","MDF#NAME","MDF#TYPE"}, new java.lang.String[]{"INTEGER","INTEGER NOT NULL","TEXT NOT NULL","TEXT"});
				} else {
					// False && True
					throw new IllegalStateException();
				}
			} else {
				if (false == this.adapter.existsTable(meta_data)) {
					// True && False
					throw new IllegalStateException();
				} else {
					// True && True
				}
			}
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (true == methodName.equals("exists")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.meta.Data.class == c && true == o instanceof dro.meta.Data) {
					final dro.meta.Data md = (dro.meta.Data)o;
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
				  /*final boolean exists = */this.adapter.existsTable(name);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (true == methodName.equals("create")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.meta.Data.class == c && true == o instanceof dro.meta.Data) {
					final dro.meta.Data md = (dro.meta.Data)o;
					final java.lang.Object __id__ = md.get("$id");
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
					if (false == this.adapter.existsTable(name)) {
						final java.lang.Integer __db_id__ = this.adapter.insertIntoTable(meta, metaColumnNames, new java.lang.Object[]{md.get("$name")});
						super.mdl.set(__id__, __db_id__);
					  //final java.util.List<java.lang.Object> list = new ArrayList<>(md./**/keys/**/(dro.lang.Set.Return.Set)); // TODO
						final java.util.Map<java.lang.Object, dro.meta.data.Field> map = md.returns(dro.meta.data.Field.Return.Map);
						final java.lang.String[] columnNames = new java.lang.String[map.size()];
						final java.lang.String[] columnDescs = new java.lang.String[columnNames.length];
					  /*final */int i = 0;
						for (final java.lang.Object id: map.keySet()) {
							columnNames[i] = id.toString();
							final dro.meta.data.Field mdf = (dro.meta.data.Field)map.get(id);
							columnDescs[i] = mdf.get("$type").toString();
							this.adapter.insertIntoTable(meta_data, metaDataColumnNames, new java.lang.Object[]{__db_id__, columnNames[i], columnDescs[i]});
							// TODO: above, the "0" needs to be the primary id of the row in the database
							i++;
						}
						this.adapter.createTable(name, new java.lang.Integer(0), columnNames, columnDescs);
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else if (true == methodName.equals("insert")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == c && true == o instanceof dro.Data) {
					final dro.Data d = (dro.Data)o;
					final dro.meta.Data md = d.returns(dro.meta.Data.Return.Data);
					final java.lang.Object __id__ = md.get("$id");
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
					if (true == this.adapter.existsTable(name)) {
					  //final java.util.List<java.lang.Object> list = new ArrayList<>(md./**/keys/**/(dro.lang.Set.Return.Set)); // TODO: better
					  /*final */java.util.Map<java.lang.Object, dro.data.Field> map = d.returns(dro.data.Field.Return.Map);
						final java.lang.String[] insertColumnNames = new java.lang.String[map.size()-1];
						final java.lang.Object[] insertColumnValues = new java.lang.Object[insertColumnNames.length];
					  /*final */int i = 0;
						for (final java.lang.Object id: map.keySet()) {
							final dro.data.Field df = (dro.data.Field)map.get(id);
							final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
							final java.lang.String __id = mdf.get("$id").toString();
						  //final java.lang.String type = mdf.get("$type").toString();
						  //if (false == type.contains("AUTOINCREMENT")) {
							if (false == __id.equals("ID")) {
								insertColumnNames[i] = mdf.get("$id").toString();
								insertColumnValues[i] = d.get(df);
								i++;
							}
						}
						final java.lang.Integer __db_id__ = this.adapter.insertIntoTable(name, insertColumnNames, insertColumnValues);
						super.dl.set(__id__, __db_id__);
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else if (true == methodName.equals("select")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.meta.Data.class == c && true == o instanceof dro.meta.Data) {
					final dro.meta.Data md = (dro.meta.Data)o;
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
					if (true == this.adapter.existsTable(name)) {
					  //final java.util.List<java.lang.Object> list = new ArrayList<>(md./**/keys/**/(dro.lang.Set.Return.Set)); // TODO
						final java.util.Map<java.lang.Object, dro.meta.data.Field> map = md.returns(dro.meta.data.Field.Return.Map);
						final java.lang.String[] selectColumnNames = new java.lang.String[map.size()];
						final java.lang.Object[] selectColumnTypes = new java.lang.String[selectColumnNames.length];
					  /*final */int i = 0;
						for (final java.lang.Object id: map.keySet()) {
							final dro.meta.data.Field mdf = (dro.meta.data.Field)map.get(id);
							selectColumnNames[i] = mdf.get("$id").toString();
							selectColumnTypes[i] = mdf.get("$type");
							i++;
						}
						returns = this.adapter.selectFromTable(name, selectColumnNames, selectColumnTypes, (java.lang.String[])null, (java.lang.String[])null, (java.lang.Object[])null);
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else if (true == methodName.equals("update")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == c && true == o instanceof dro.Data) {
					final dro.Data d = (dro.Data)o;
					final dro.meta.Data md = d.returns(dro.meta.Data.Return.Data);
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
					if (true == this.adapter.existsTable(name)) {
					  //final java.util.List<java.lang.Object> list = new ArrayList<>(md./**/keys/**/(dro.lang.Set.Return.Set)); // TODO
						final java.util.Map<java.lang.Object, dro.data.Field> map = d.returns(dro.data.Field.Return.Map);
						final java.lang.String[] updateColumnNames = new java.lang.String[map.size()-1];
						final java.lang.Object[] updateColumnValues = new java.lang.Object[updateColumnNames.length];
						final java.lang.String[] whereColumnNames = new java.lang.String[1];
						final java.lang.String[] whereColumnOperators = new java.lang.String[1];
						final java.lang.Object[] whereColumnValues = new java.lang.Object[whereColumnNames.length];
					  /*final */int i = 0, j = 0;
						for (final java.lang.Object id: map.keySet()) {
							final dro.data.Field df = (dro.data.Field)map.get(id);
							final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
							final java.lang.String __id = mdf.get("$id").toString();
						  //final java.lang.String type = mdf.get("$type").toString();
						  //if (true == type.contains("AUTOINCREMENT")) {
							if (true == __id.equals("ID")) {
								whereColumnNames[j] = __id;
								whereColumnOperators[j] = "=";
								whereColumnValues[j] = super.dl.get(d.get(df));
								j++;
							} else {
								updateColumnNames[i] = __id;
								updateColumnValues[i] = d.get(df);
								i++;
							}
						}
						this.adapter.updateTableSet(name, updateColumnNames, updateColumnValues, whereColumnNames, whereColumnOperators, whereColumnValues);
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else if (true == methodName.equals("delete")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == c && true == o instanceof dro.Data) {
					final dro.Data d = (dro.Data)o;
					final dro.meta.Data md = d.returns(dro.meta.Data.Return.Data);
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
					if (true == this.adapter.existsTable(name)) {
					  //final java.util.List<java.lang.Object> list = new ArrayList<>(md./**/keys/**/(dro.lang.Set.Return.Set)); // TODO
						final java.util.Map<java.lang.Object, dro.data.Field> map = d.returns(dro.data.Field.Return.Map);
						final java.lang.String[] whereColumnNames = new java.lang.String[1];
						final java.lang.String[] whereColumnOperators = new java.lang.String[1];
						final java.lang.Object[] whereColumnValues = new java.lang.Object[whereColumnNames.length];
					  /*final */int i = 0;
						for (final java.lang.Object id: map.keySet()) {
							final dro.data.Field df = (dro.data.Field)map.get(id);
							final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
							final java.lang.String __id = mdf.get("$id").toString();
						  //final java.lang.String type = mdf.get("$type").toString();
						  //if (true == type.contains("AUTOINCREMENT")) {
							if (true == __id.equals("ID")) {
								whereColumnNames[i] = __id;
								whereColumnOperators[i] = "=";
								whereColumnValues[i] = super.dl.get(d.get(df));
								i++;
							}
						}
						this.adapter.deleteFromTable(name, whereColumnNames, whereColumnOperators, whereColumnValues);
						// TODO: select count(*) from name --AND-- if 0 (zero) drop the meta/name table (if auto-purge is configured)
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else if (true == methodName.equals("drop")) { // FIXME
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (dro.meta.Data.class == c && true == o instanceof dro.meta.Data) {
					final dro.meta.Data md = (dro.meta.Data)o;
					final java.lang.String name = (java.lang.String)md.get("$name"); // TODO: mapping rules symmetric (or no?)
					if (true == this.adapter.existsTable(name)) {
					  /*final boolean dropped = */this.adapter.dropTable(name);
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		}
		return returns;
	}
}