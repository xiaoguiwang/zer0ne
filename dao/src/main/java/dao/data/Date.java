package dao.data;

import dao.lang.Primitive;

public class Date extends Primitive<java.util.Date> {
	public Date() {
		super(new java.util.Date(System.currentTimeMillis()));
	}/*
	@Override
	public java.util.Date newOne(final java.lang.Object o) {
		return null == o ? null : new java.util.Date(java.lang.Long.parseLong(o.toString()));
	}*/
}