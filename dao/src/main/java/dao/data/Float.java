package dao.data;

import dao.lang.Primitive;

public class Float extends Primitive<java.lang.Float> {
	public Float() {
		super(0.0f);
	}/*
	@Override
	public java.lang.Float newOne(final java.lang.Object o) {
		return null == o ? null: java.lang.Float.parseFloat(o.toString());
	}*/
}