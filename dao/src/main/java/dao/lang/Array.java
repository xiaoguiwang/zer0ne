package dao.lang;

public class Array extends Indexed {
	public static final java.lang.String ARRAY = "ARRAY";
	public static final java.lang.String ARRAY_ID = "ARRAY_ID";
	public static final java.lang.String ARRAY_X_REF = "ARRAY_X_REF";
	
	public Array() {
		super(new java.lang.Object[]{});
	}
	
	protected Array length(final int length) {
		return (Array)super.count(length);
	}
	
	@Override
	public java.lang.Integer insert(final java.lang.Object o) {
	  /*final */java.lang.Integer arrayId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.lang.Object[].class, o);
		if (null == arrayId) {
			final java.lang.Object[] oa = (java.lang.Object[])o;
		  /*final java.lang.Integer */arrayId = super.__insert(ARRAY, java.lang.Object[].class, java.lang.Boolean.TRUE, oa.length); // <== []
			dao.Context.getLookupFor(this).useRowIdToSetObject(java.lang.Object[].class, arrayId, oa);
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.lang.Object[].class, oa, arrayId);
			for (int i = 0; i < (null == oa ? 0 : oa.length); i++) {
				final Reference r = new Reference(null, null, i, oa[i].getClass(), oa[i]);
				final java.lang.Integer id = super.__insertCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, r); // <== [#]
				dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.lang.Object[].class, oa, i, null, id);
			}
		} else {
		  //throw new IllegalStateException();
		}
		return arrayId;
	}
	
	@Override
	public java.lang.Object select(final java.lang.Integer arrayId) {
	  /*final */java.lang.Object[] oa = (java.lang.Object[])dao.Context.getLookupFor(this).useRowIdToGetObject(java.lang.Object[].class, arrayId);
		if (null == oa) {
			final Indexed ind = this.__select(ARRAY, arrayId); // <== []
		  /*final java.lang.Object[] */oa = new java.lang.Object[ind.count];
			for (int i = 0; i < ind.count; i++) {
				final Reference r = this.__selectCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, new Reference(java.lang.Object[].class, oa, i, null, null)); // <== [#]
				dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.lang.Object[].class, oa, i, null, r.i);
				oa[i] = r.o;
			}
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.lang.Object[].class, oa, arrayId);
		}
		return oa;
	}
	@Override
	public java.lang.Object select(final java.lang.Object o, final java.lang.Object i) {
		final java.lang.Integer arrayId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.lang.Object[].class, o);
	  /*final */java.lang.Object[] oa = null;
		if (null == arrayId) {
			final Indexed ind = this.__select(ARRAY, arrayId); // <== []
		  /*final java.lang.Object[] */oa = new java.lang.Object[ind.count];
		}
	  /*final */java.lang.Object __o = null;
		if (null != oa) {
			final java.lang.Integer index = (java.lang.Integer)i;
			if (null == oa[index]) {
				final Reference r = this.__selectCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, new Reference(java.lang.Object[].class, oa, index, null, null)); // <== [#]
				oa[index] = r.o;
				dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.lang.Object[].class, oa, index, null, r.i);
			}
			__o = oa[index];
		}
		return __o;
	}
	
	@Override
	public void update(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		final java.lang.Integer arrayId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.lang.Object[].class, o);
		if (null == arrayId) {
			throw new IllegalStateException();
		}
		final java.lang.Object[] oa = (java.lang.Object[])o;
		final java.lang.Integer index = (java.lang.Integer)i;
		final Reference r = this.__selectCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, new Reference(java.lang.Object[].class, oa, index, null, null)); // <== [#]
		if (p.getClass() == r.cp) {
			this.__updateCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, new Reference(java.lang.Object[].class, oa, index, p.getClass(), p));
		} else {
			dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.lang.Object[].class, oa, index);
			this.__deleteCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, r);
			this.__insertCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, new Reference(null, null, index, p.getClass(), p));
			dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.lang.Object[].class, oa, index, null, r.i);
		}
	}
	
	@Override
	public void delete(final java.lang.Object o, final java.lang.Object i) {
		throw new UnsupportedOperationException();
	}
	@Override
	public void delete(final java.lang.Object o) {
		final java.lang.Integer arrayId = dao.Context.getLookupFor(this).useObjectToRemoveRowId(java.lang.Object[].class, o);
		if (null == arrayId) {
			throw new IllegalStateException();
		}
		final java.lang.Object[] oa = (java.lang.Object[])o;
		final Reference[] ra = this.__selectCrossReferences(ARRAY_X_REF, ARRAY_ID, arrayId);
		for (int i = 0; i < ra.length; i++) {
			dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.lang.Object[].class, oa, i);
			this.__deleteCrossReference(ARRAY_X_REF, ARRAY_ID, arrayId, ra[i].delete()); // <== [#]
		}
		this.__delete(ARRAY, arrayId); // <== []
		dao.Context.getLookupFor(this).useRowIdToRemoveObject(java.lang.Object[].class, arrayId);
	}
	@Override
	public void delete() {
		final java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = (java.util.Map<java.lang.Object, java.lang.Integer>)dao.Context.getLookupFor(this).objectToRowId.get(this.c);
		if (null != objectToRowId) {
			final java.lang.Object[] oa = objectToRowId.keySet().toArray(new java.lang.Object[0]);
			for (final java.lang.Object o: oa) {
				this.delete(o);
			}
		}
	}
}