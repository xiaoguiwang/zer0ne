package dao.lang;

import java.util.ArrayList;
import java.util.HashMap;

public class Class {
	public static java.lang.Class<?> getAccessClass(final java.lang.Object o) {
	  /*final */java.lang.Class<?> __c = o.getClass();
		if (true == __c.isArray()) {
			__c = java.lang.Object[].class;
		} else if (true == o instanceof java.util.List) {
			__c = java.util.List.class;
		} else if (true == o instanceof java.util.Set) {
			__c = java.util.Set.class;
		} else if (true == o instanceof java.util.Map) {
			__c = java.util.Map.class;
		}
		return __c;
	}
	public static java.lang.Class<?> getAccessClass(final java.lang.Class<?> c) {
	  /*final */java.lang.Class<?> __c = c;
		if (true == c.isArray()) {
			__c = java.lang.Object[].class;
		} else if (c == dao.util.ArrayList.class) { /*FIXME: make sure to register each known class of java.util.List*/
			__c = java.util.List.class;
		} else if (c == dao.util.HashSet.class) { /*FIXME: make sure to register each known class of java.util.Set*/
			__c = java.util.Set.class;
		} else if (c == dao.util.HashMap.class) { /*FIXME: make sure to register each known class of java.util.Map*/
			__c = java.util.Map.class;
		}
		return __c;
	}
	
	public static java.lang.String getTypeReference(final java.lang.Object o) {
		final java.lang.String type_ref;// = null;
		if (true == o instanceof java.lang.Object[]) {
			type_ref = java.lang.Object[].class.getName();
		} else if (true == o instanceof java.util.Set) {
			type_ref = dao.util.HashSet.class.getName();
		} else if (true == o instanceof java.util.List) {
			type_ref = dao.util.ArrayList.class.getName();
		} else if (true == o instanceof java.util.Map) {
			type_ref = dao.util.HashMap.class.getName();
		} else if (true == o instanceof java.lang.Class) {
			type_ref = ((java.lang.Class<?>)o).getName();
		} else {
			type_ref = o.getClass().getName();
		}
		return type_ref;
	}
	
	private static final java.util.Map<java.lang.Class<?>, java.util.List<java.lang.Object>> mapOfObjectsWithDynamicHashCodes = new HashMap<>();
	private static final java.util.Map<java.lang.Class<?>, java.util.List<java.lang.Object>> mapOfObjectsWithFixedHashCodes = new HashMap<>();
	public static java.lang.Object associate(final java.lang.Class<?> c, final java.lang.Object objectWithDynamicHashCode) {
	  /*final */java.lang.Object objectWithFixedHashCode = null;
		if (true == objectWithDynamicHashCode instanceof dro.lang.Id) {
			objectWithFixedHashCode = ((dro.lang.Id)objectWithDynamicHashCode).id();
		} else {
		  /*final */java.util.List<java.lang.Object> mapOfObjectsWithDynamicHashCodes = Class.mapOfObjectsWithDynamicHashCodes.get(c);
		  /*final */java.util.List<java.lang.Object> mapOfObjectsWithFixedHashCodes = Class.mapOfObjectsWithFixedHashCodes.get(c);
			if (null == mapOfObjectsWithDynamicHashCodes) {
				mapOfObjectsWithDynamicHashCodes = new ArrayList<java.lang.Object>();
				Class.mapOfObjectsWithDynamicHashCodes.put(c, mapOfObjectsWithDynamicHashCodes);
				mapOfObjectsWithFixedHashCodes = new ArrayList<java.lang.Object>();
				Class.mapOfObjectsWithFixedHashCodes.put(c, mapOfObjectsWithFixedHashCodes);
			}
			final int size = mapOfObjectsWithDynamicHashCodes.size();
			for (int i = 0; i < size; i++) {
				if (objectWithDynamicHashCode == mapOfObjectsWithDynamicHashCodes.get(i)) {
					objectWithFixedHashCode = mapOfObjectsWithFixedHashCodes.get(i);
					break;
				}
			}
			if (null == objectWithFixedHashCode) {
				mapOfObjectsWithDynamicHashCodes.add(objectWithDynamicHashCode);
				objectWithFixedHashCode = new java.lang.Object();
				mapOfObjectsWithFixedHashCodes.add(objectWithFixedHashCode);
			}
		}
		return objectWithFixedHashCode;
	}
	public static java.lang.Object associated(final java.lang.Class<?> c, final java.lang.Object objectWithDynamicHashCode) {
	  /*final */java.lang.Object objectWithFixedHashCode = null;
		if (true == objectWithDynamicHashCode instanceof dro.lang.Id) {
			objectWithFixedHashCode = ((dro.lang.Id)objectWithDynamicHashCode).id();
		} else {
		  /*final */java.util.List<java.lang.Object> mapOfObjectsWithFixedHashCodes = Class.mapOfObjectsWithFixedHashCodes.get(c);
			if (null != mapOfObjectsWithFixedHashCodes) {
				final int size = mapOfObjectsWithFixedHashCodes.size();
				for (int i = 0; i < size; i++) {
					if (objectWithDynamicHashCode == mapOfObjectsWithFixedHashCodes.get(i)) {
					  /*final */java.util.List<java.lang.Object> mapOfObjectsWithDynamicHashCodes = Class.mapOfObjectsWithDynamicHashCodes.get(c);
						objectWithFixedHashCode = mapOfObjectsWithDynamicHashCodes.get(i);
						break;
					}
				}
			}
		}
		return objectWithFixedHashCode;
	}
	public static java.lang.Object disassociate(final java.lang.Class<?> c, final java.lang.Object objectWithDynamicHashCode) {
	  /*final */java.lang.Object objectWithFixedHashCode = null;
		if (true == objectWithDynamicHashCode instanceof dro.lang.Id) {
			objectWithFixedHashCode = ((dro.lang.Id)objectWithDynamicHashCode).id();
		} else {
		  /*final */java.util.List<java.lang.Object> mapOfObjectsWithDynamicHashCodes = Class.mapOfObjectsWithDynamicHashCodes.get(c);
			if (null != mapOfObjectsWithDynamicHashCodes) {
				final int size = mapOfObjectsWithDynamicHashCodes.size();
				for (int i = 0; i < size; i++) {
					if (objectWithDynamicHashCode == mapOfObjectsWithDynamicHashCodes.get(i)) {
						mapOfObjectsWithDynamicHashCodes.remove(i);
					  /*final */java.util.List<java.lang.Object> mapOfObjectsWithFixedHashCodes = Class.mapOfObjectsWithFixedHashCodes.get(c);
						objectWithFixedHashCode = mapOfObjectsWithFixedHashCodes.remove(i);
						break;
					}
				}
			}
		}
		return objectWithFixedHashCode;
	}
}