package dao.lang;

import dao.util.Access;

public class Indexed extends Access {
	public static final java.lang.String ID = "ID";
	public static final java.lang.String TYPE = "TYPE";
	public static final java.lang.String INDEX = "INDEX";
	public static final java.lang.String TYPE_REF = "TYPE_REF";
	public static final java.lang.String REF_ID = "REF_ID";
	public static final java.lang.String INDEXED = "INDEXED";
	public static final java.lang.String COUNT = "COUNT";
	
	static class Reference {
		public final java.lang.Class<?> c;
		public final java.lang.Object o;
		public /*final */java.lang.Integer id;
		public final java.lang.Integer index;
		public /*final */java.lang.Class<?> cp;
		public /*final */java.lang.Object p;
		public final java.lang.Integer i;
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> cp, final java.lang.Object p) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.cp = cp;
			this.p = p;
			this.i = null != p ? dao.Adapter.access(dao.lang.Class.getAccessClass(p)).insert(p) : null;
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> cp, final java.lang.Object p, final java.lang.Integer rowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.cp = cp;
			this.p = p;
			this.i = rowId;
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> cp, final java.lang.Integer rowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.cp = cp;
			this.p = null != cp && null != rowId ? dao.Adapter.access(dao.lang.Class.getAccessClass(cp)).select(rowId) : null;
			this.i = rowId;
		}
		public Reference delete() {
			dao.Adapter.access(dao.lang.Class.getAccessClass(this.cp)).delete(this.i);
			return this;
		}
	}
	
	boolean indexed;// = false;
	int count;// = -1;
	
	{
	  //indexed = false;
	  //count = 0;
	}
	
	protected Indexed(final java.lang.Object o) {
		super(o);
	}
	
	protected Indexed indexed(final boolean indexed) {
		this.indexed = indexed;
		return this;
	}
	protected Indexed count(final int count) {
		this.count = count;
		return this;
	}
	
	protected java.lang.Integer __insert(final java.lang.String name, final java.lang.Class<?> c, final boolean indexed, final int count) {
		dao.Context.getAdapterFor(this).createTableIfNotExists( // Note: we do this because on cold-start we don't know the primaryIndex - this is the only way we have (so far) to re-state the primaryIndex for last-row-id/generated-key for insert
			name, // String name
			0, // Integer primaryIdIndex
			new java.lang.String[]{ID,TYPE,INDEXED,COUNT}, // String[] columnNames
			new java.lang.Object[]{new java.math.BigInteger("0"),"",false,0} // Object[] columnTypes
		);
		final java.lang.Integer rowId = dao.Context.getAdapterFor(this).insertIntoTable(
			name,
			new java.lang.String[]{TYPE,INDEXED,COUNT},
			new java.lang.Object[]{dao.lang.Class.getTypeReference(c),indexed,count}
		);
		return rowId;
	}
	protected java.lang.Integer __insertCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, final Reference r) {
		dao.Context.getAdapterFor(this).createTableIfNotExists( // Note: we do this because on cold-start we don't know the primaryIndex - this is the only way we have (so far) to re-state the primaryIndex for last-row-id/generated-key for insert
			name, // String name
			0, // Integer primaryIdIndex
			new java.lang.String[]{ID,nameId,INDEX,TYPE_REF,REF_ID}, // String[] columnNames
			new java.lang.Object[]{new java.math.BigInteger("0"),new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0")} // Object[] columnTypes
		);
		final java.lang.Integer rowId = dao.Context.getAdapterFor(this).insertIntoTable(
			name,
			new java.lang.String[]{nameId,INDEX,TYPE_REF,REF_ID},
			new java.lang.Object[]{id,r.index,dao.lang.Class.getTypeReference(r.p),r.i}
		);
		return rowId;
	}
	@Override
	public java.lang.Integer insert(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	}
	public java.lang.Integer insert(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		throw new UnsupportedOperationException();
	}
	
	protected Indexed __select(final java.lang.String name, final java.lang.Integer id) {
	  /*final */Indexed ind = null;
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			final Object[][] pa = dao.Context.getAdapterFor(this).selectFromTable(
				name,
				new java.lang.String[]{TYPE,INDEXED,COUNT}, // String[] selectColumnNames
				new java.lang.Object[]{"",java.lang.Boolean.TRUE,new java.lang.Integer(0)}, // Object[] selectColumnTypes
				new java.lang.String[]{ID}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
			ind = null != pa && 1 == pa.length ? new Indexed(name).indexed((java.lang.Boolean)pa[0][1]).count((java.lang.Integer)pa[0][2]) : null;
		}
		return ind;
	}
	protected Reference[] __selectCrossReferences(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id) {
	  /*final */Reference[] ra = null;
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			final java.lang.Object[][] qa = dao.Context.getAdapterFor(this).selectFromTable(
				name,
				new java.lang.String[]{ID,INDEX,TYPE_REF,REF_ID}, // String[] selectColumnNames
				new java.lang.Object[]{new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
				new java.lang.String[]{nameId}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
			ra = null != qa && 0 < qa.length ? new Reference[qa.length] : null;
			for (int i = 0; i < (null == qa ? 0 : qa.length); i++) {
				final java.lang.Class<?> cp;// = null;
				try {
					cp = java.lang.Class.forName(qa[i][2].toString());
				} catch (final ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
				final java.lang.Integer rowId = new java.lang.Integer(qa[i][0].toString());
				final java.lang.Integer pRowId = new java.lang.Integer(qa[i][3].toString());
				ra[i] = new Reference(null, null, i, cp, pRowId);
				ra[i].id = rowId; /*FIXME*/
			}
		}
		return ra;
	}
	protected Reference __selectCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, /*final */Reference r) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
		  /*final */java.lang.Object[][] qa = null;
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (null != r.index) {
			  /*final java.lang.Object[][] */qa = dao.Context.getAdapterFor(this).selectFromTable(
					name,
					new java.lang.String[]{ID,INDEX,TYPE_REF,REF_ID}, // String[] selectColumnNames
					new java.lang.Object[]{new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
					new java.lang.String[]{nameId,INDEX}, // String[] whereColumnNames
					new java.lang.String[]{"=","="}, // String[] whereColumnOperators
					new java.lang.Object[]{id,r.index} // Object[] whereColumnValues
				);
			} else if (null != r.cp && null != r.p) {
			  /*final java.lang.Object[][] */qa = dao.Context.getAdapterFor(this).selectFromTable(
					name,
					new java.lang.String[]{ID,INDEX,TYPE_REF,REF_ID}, // String[] selectColumnNames
					new java.lang.Object[]{new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
					new java.lang.String[]{nameId,TYPE_REF,REF_ID}, // String[] whereColumnNames
					new java.lang.String[]{"=","=","="}, // String[] whereColumnOperators
					new java.lang.Object[]{id,dao.lang.Class.getTypeReference(r.p),r.i} // Object[] whereColumnValues
				);
			} else {
				throw new IllegalArgumentException();
			}
			if (null != qa && 1 == qa.length) {
				final java.lang.Integer rowId = new java.lang.Integer(qa[0][0].toString());
				final java.lang.Integer pRowId = new java.lang.Integer(qa[0][3].toString());
				final java.lang.Integer index = new java.lang.Integer(qa[0][1].toString());
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (null != r.c && null != r.o) {// && null != r.index) {
					final java.lang.Class<?> cp;// = null;
					try {
						cp = java.lang.Class.forName(qa[0][2].toString());
					} catch (final ClassNotFoundException e) {
						throw new RuntimeException(e);
					}
					r = null != qa && 1 == qa.length ? new Reference(r.c, r.o, /*r.*/index, cp, pRowId) : null;
					r.id = rowId; /*FIXME */
				} else if (null != r.c && null != r.o && null != r.cp && null != r.p) {
					r = null != qa && 1 == qa.length ? new Reference(r.c, r.o, /**/index, r.cp, pRowId) : null;
					r.id = rowId; /*FIXME*/
				} else {
					throw new IllegalArgumentException();
				}
			}
		}
		return r;
	}
	@Override
	public java.lang.Object select(final java.lang.Integer id) {
		new UnsupportedOperationException();
		return null;
	}
	public java.lang.Object select(final java.lang.Object o, final java.lang.Object i) {
		throw new UnsupportedOperationException();
	}
	
	protected void __update(final java.lang.String name, final java.lang.Integer id, final int count) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).updateTableSet(
				name,
				new java.lang.String[]{COUNT},
				new java.lang.Object[]{count},
				new java.lang.String[]{ID},
				new java.lang.String[]{"="},
				new java.lang.Object[]{id}
			);
		}
	}
	protected void __updateCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, final Reference r) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).updateTableSet(
				name,
				new java.lang.String[]{TYPE_REF,REF_ID},
				new java.lang.Object[]{dao.lang.Class.getTypeReference(r.p),r.i},
				new java.lang.String[]{ID},
				new java.lang.String[]{"="},
				new java.lang.Object[]{id}
			);
		}
	}
	public void update(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	}
	public void update(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		throw new UnsupportedOperationException();
	}
	
	protected void __deleteCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, final Reference r) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				name,
				new java.lang.String[]{/*nameId,*/ID}, // String[] whereColumnNames
				new java.lang.String[]{/*"=",*/"="}, // String[] whereColumnOperators
				new java.lang.Object[]{/*id,*/r.id} // Object[] whereColumnValues
			);
		}
	}
	protected void __deleteCrossReferences(final java.lang.String name, java.lang.String nameId, final java.lang.Integer id) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				name,
				new java.lang.String[]{nameId}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
		}
	}
	protected void __delete(final java.lang.String name, final java.lang.Integer id) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				name,
				new java.lang.String[]{ID}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
		}
	}
	public void delete(final java.lang.Object o, final java.lang.Object i) {
		throw new UnsupportedOperationException();
	}
	public void delete(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	}
	public void delete() {
		throw new UnsupportedOperationException();
	}
}