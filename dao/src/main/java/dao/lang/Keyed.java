package dao.lang;

import dao.util.Access;

public class Keyed extends Access {
	public static final java.lang.String ID = "ID";
	public static final java.lang.String TYPE = "TYPE";
	public static final java.lang.String INDEX = "INDEX";
	public static final java.lang.String TYPE_KEY = "TYPE_KEY";
	public static final java.lang.String KEY_ID = "KEY_ID";
	public static final java.lang.String TYPE_VAL = "TYPE_VAL";
	public static final java.lang.String VAL_ID = "VAL_ID";
	public static final java.lang.String INDEXED = "INDEXED";
	public static final java.lang.String COUNT = "COUNT";
	
	static class Reference {
		public final java.lang.Class<?> c;
		public final java.lang.Object o;
		public /*final */java.lang.Integer id;
		public final java.lang.Integer index;
		public final java.lang.Class<?> ck;
		public final java.lang.Object ok;
		public final java.lang.Integer ik;
		public /*final */java.lang.Class<?> cv;
		public /*final */java.lang.Object ov;
		public final java.lang.Integer iv;
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Object ok, final java.lang.Class<?> cv, final java.lang.Object ov) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = ok;
			this.ik = null != ck && null != ok ? dao.Adapter.access(ck).insert(ok) : null;
			this.cv = cv;
			this.ov = ov;
			this.iv = null != ov ? dao.Adapter.access(dao.lang.Class.getAccessClass(ov)).insert(ov) : null;
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Object ok, final java.lang.Integer kRowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = ok;
			this.ik = kRowId;
			this.cv = null;
			this.ov = null;
			this.iv = null;
		}
	  /*private Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Integer kRowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = null != ck && null != kRowId ? dao.Adapter.access(ck).select(kRowId) : null;
			this.ik = kRowId;
			this.cv = null;
			this.ov = null;
			this.iv = null;
		}*/
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Integer kRowId, final java.lang.Class<?> cv, final java.lang.Integer vRowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = null != ck && null != kRowId ? dao.Adapter.access(ck).select(kRowId) : null;
			this.ik = kRowId;
			this.cv = cv;
			this.ov = null != cv && null != vRowId ? dao.Adapter.access(dao.lang.Class.getAccessClass(cv)).select(vRowId) : null;
			this.iv = vRowId;
		}
		public Reference delete() {
			dao.Adapter.access(this.ck).delete(this.ik);
			dao.Adapter.access(this.cv).delete(this.iv); //<==4
			return this;
		}
	}
	
	boolean indexed;// = false;
	int count;// = -1;
	
	{
	  //indexed = false;
	  //count = 0;
	}
	
	protected Keyed(final java.lang.Object o) {
		super(o);
	}
	
	protected Keyed indexed(final boolean indexed) {
		this.indexed = indexed;
		return this;
	}
	protected Keyed count(final int count) {
		this.count = count;
		return this;
	}
	
	protected java.lang.Integer __insert(final java.lang.String name, final java.lang.Class<?> c, final boolean indexed, final int count) {
		dao.Context.getAdapterFor(this).createTableIfNotExists( // Note: we do this because on cold-start we don't know the primaryIndex - this is the only way we have (so far) to re-state the primaryIndex for last-row-id/generated-key for insert
			name, // String name
			0, // Integer primaryIdIndex
			new java.lang.String[]{ID,TYPE,INDEXED,COUNT}, // String[] columnNames
			new java.lang.Object[]{new java.math.BigInteger("0"),"",false,0} // Object[] columnTypes
		);
		final java.lang.Integer rowId = dao.Context.getAdapterFor(this).insertIntoTable(
			name,
			new java.lang.String[]{TYPE,INDEXED,COUNT},
			new java.lang.Object[]{dao.lang.Class.getTypeReference(c),indexed,count} // ABC changed from c.getName()
		);
		return rowId;
	}
	protected java.lang.Integer __insertCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, final Reference r) {
		dao.Context.getAdapterFor(this).createTableIfNotExists( // Note: we do this because on cold-start we don't know the primaryIndex - this is the only way we have (so far) to re-state the primaryIndex for last-row-id/generated-key for insert
			name, // String name
			0, // Integer primaryIdIndex
			new java.lang.String[]{ID,nameId,INDEX,TYPE_KEY,KEY_ID,TYPE_VAL,VAL_ID}, // String[] columnNames
			new java.lang.Object[]{new java.math.BigInteger("0"),new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0"),"",new java.math.BigInteger("0")} // Object[] columnTypes
		);
		final java.lang.Integer rowId = dao.Context.getAdapterFor(this).insertIntoTable(
			name,
			new java.lang.String[]{nameId,INDEX,TYPE_KEY,KEY_ID,TYPE_VAL,VAL_ID},
			new java.lang.Object[]{id,r.index,dao.lang.Class.getTypeReference(r.ok),r.ik,dao.lang.Class.getTypeReference(r.ov),r.iv}
		);
		return rowId;
	}
	@Override
	public java.lang.Integer insert(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	}
	public java.lang.Integer insert(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		throw new UnsupportedOperationException();
	}
	
	protected Keyed __select(final java.lang.String name, final java.lang.Integer id) {
	  /*final */Keyed ind = null;
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			final Object[][] pa = dao.Context.getAdapterFor(this).selectFromTable(
				name,
				new java.lang.String[]{TYPE,INDEXED,COUNT}, // String[] selectColumnNames
				new java.lang.Object[]{"",java.lang.Boolean.TRUE,new java.lang.Integer(0)}, // Object[] selectColumnTypes
				new java.lang.String[]{ID}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
			ind = null != pa && 1 == pa.length ? new Keyed(name).indexed((java.lang.Boolean)pa[0][1]).count((java.lang.Integer)pa[0][2]) : null;
		}
		return ind;
	}
	protected Reference[] __selectCrossReferences(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id) {
	  /*final */Reference[] ra = null;
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			final java.lang.Object[][] qa = dao.Context.getAdapterFor(this).selectFromTable(
				name,
				new java.lang.String[]{ID,INDEX,TYPE_KEY,KEY_ID,TYPE_VAL,VAL_ID}, // String[] selectColumnNames
				new java.lang.Object[]{new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0"),"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
				new java.lang.String[]{nameId}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
			ra = null != qa && 0 < qa.length ? new Reference[qa.length] : null;
			for (int i = 0; i < (null == qa ? 0 : qa.length); i++) {
				final java.lang.Class<?> ck;// = null;
				try {
					ck = java.lang.Class.forName(qa[i][2].toString());
				} catch (final ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
				final java.lang.Class<?> cv;// = null;
				try {
					cv = java.lang.Class.forName(qa[i][4].toString());
				} catch (final ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
				final java.lang.Integer rowId = new java.lang.Integer(qa[i][0].toString());
				final java.lang.Integer kRowId = new java.lang.Integer(qa[i][3].toString());
				final java.lang.Integer vRowId = new java.lang.Integer(qa[i][5].toString());
				ra[i] = new Reference(null, null, i, ck, kRowId, cv, vRowId);
				ra[i].id = rowId; /*FIXME*/
			}
		}
		return ra;
	}
	protected Reference __selectCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, /*final */Reference r) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
		  /*final */java.lang.Object[][] qa = null;
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (null != r.index) {
			  /*final java.lang.Object[][] */qa = dao.Context.getAdapterFor(this).selectFromTable(
					name,
					new java.lang.String[]{ID,INDEX,TYPE_KEY,KEY_ID,TYPE_VAL,VAL_ID}, // String[] selectColumnNames
					new java.lang.Object[]{new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0"),"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
					new java.lang.String[]{nameId,INDEX}, // String[] whereColumnNames
					new java.lang.String[]{"=","="}, // String[] whereColumnOperators
					new java.lang.Object[]{id,r.index} // Object[] whereColumnValues
				);
			} else if (null != r.ck && null != r.ok) {
			  /*final java.lang.Object[][] */qa = dao.Context.getAdapterFor(this).selectFromTable(
					name,
					new java.lang.String[]{ID,INDEX,TYPE_KEY,KEY_ID,TYPE_VAL,VAL_ID}, // String[] selectColumnNames
					new java.lang.Object[]{new java.math.BigInteger("0"),0,"",new java.math.BigInteger("0"),"",new java.math.BigInteger("0")}, // Object[] selectColumnTypes
					new java.lang.String[]{nameId,TYPE_KEY,KEY_ID}, // String[] whereColumnNames
					new java.lang.String[]{"=","=","="}, // String[] whereColumnOperators
					new java.lang.Object[]{id,dao.lang.Class.getTypeReference(r.ok),r.ik} // Object[] whereColumnValues
				);
			} else {
				throw new IllegalArgumentException();
			}
			if (null != qa && 1 == qa.length) {
				final java.lang.Integer rowId = new java.lang.Integer(qa[0][0].toString());
				final java.lang.Integer kRowId = new java.lang.Integer(qa[0][3].toString());
				final java.lang.Integer vRowId = new java.lang.Integer(qa[0][5].toString());
				final java.lang.Integer index = new java.lang.Integer(qa[0][1].toString());
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (null != r.c && null != r.o) {// && null != r.index) {
					final java.lang.Class<?> ck;// = null;
					try {
						ck = java.lang.Class.forName(qa[0][2].toString());
					} catch (final ClassNotFoundException e) {
						throw new RuntimeException(e);
					}
					final java.lang.Class<?> cv;// = null;
					try {
						cv = java.lang.Class.forName(qa[0][4].toString());
					} catch (final ClassNotFoundException e) {
						throw new RuntimeException(e);
					}
					r = null != qa && 1 == qa.length ? new Reference(r.c, r.o, /*r.*/index, ck, kRowId, cv, vRowId) : null;
					r.id = rowId; /*FIXME*/
				} else if (null != r.c && null != r.o && null != r.ck && null != r.ok) {
				  //final java.lang.Integer index = new java.lang.Integer(qa[0][1].toString());
				  /*final java.lang.Class<?> cv;// = null;
					try {
						cv = java.lang.Class.forName(qa[0][4].toString());
					} catch (final ClassNotFoundException e) {
						throw new RuntimeException(e);
					}*//*FIXME#1*/
					r = null != qa && 1 == qa.length ? new Reference(r.c, r.o, /**/index, r.ck, kRowId, /**/r./**/cv, vRowId) : null;
					r.id = rowId; /*FIXME*/
				} else {
					throw new IllegalArgumentException();
				}
			}
		}
		return r;
	}
	@Override
	public java.lang.Object select(final java.lang.Integer id) {
		new UnsupportedOperationException();
		return null;
	}
	public java.lang.Object select(final java.lang.Object o, final java.lang.Object k) {
		new UnsupportedOperationException();
		return null;
	}
	
	protected void __update(final java.lang.String name, final java.lang.Integer id, final java.lang.Integer count) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).updateTableSet(
				name,
				new java.lang.String[]{COUNT},
				new java.lang.Object[]{count},
				new java.lang.String[]{ID},
				new java.lang.String[]{"="},
				new java.lang.Object[]{id}
			);
		}
	}
	protected void __updateCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, final Reference r) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).updateTableSet(
				name,
				new java.lang.String[]{TYPE_KEY,KEY_ID,TYPE_VAL,VAL_ID},
				new java.lang.Object[]{r.ck.getName(),r.ik,r.cv.getName(),r.iv}, // FIXME
				new java.lang.String[]{ID},
				new java.lang.String[]{"="},
				new java.lang.Object[]{id}
			);
		}
	}
	public void update(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	}
	public void update(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		throw new UnsupportedOperationException();
	}
	
	protected void __deleteCrossReference(final java.lang.String name, final java.lang.String nameId, final java.lang.Integer id, final Reference r) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				name,
				new java.lang.String[]{/*nameId,*/ID}, // String[] whereColumnNames
				new java.lang.String[]{/*"=",*/"="}, // String[] whereColumnOperators
				new java.lang.Object[]{/*id,*/r.id} // Object[] whereColumnValues
			);
		}
	}
	protected void __deleteCrossReferences(final java.lang.String name, java.lang.String nameId, final java.lang.Integer id) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				name,
				new java.lang.String[]{nameId}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
		}
	}
	protected void __delete(final java.lang.String name, final java.lang.Integer id) {
		if (true == dao.Context.getAdapterFor(this).existsTable(name)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				name,
				new java.lang.String[]{ID}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new java.lang.Object[]{id} // Object[] whereColumnValues
			);
		}
	}
	public void delete(final java.lang.Object o, final java.lang.Object i) {
		throw new UnsupportedOperationException();
	}
	public void delete(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	}
	public void delete() {
		throw new UnsupportedOperationException();
	}
}