package dao.lang;

import dro.lang.Break;

public class List extends Indexed {
	public static final java.lang.String LIST = "LIST";
	public static final java.lang.String LIST_ID = "LIST_ID";
	public static final java.lang.String LIST_X_REF = "LIST_X_REF";
	
	public List() {
		super(new dao.util.ArrayList<java.lang.Object>(dro.lang.Break.Recursion));
	}
	
	protected List length(final int size) {
		return (List)super.count(size);
	}
	
	@Override
	public java.lang.Integer insert(final java.lang.Object o) {
	  /*final */java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.List.class, o);
		if (null == listId) {
			final java.util.List<?> list = (java.util.List<?>)o;
		  /*final java.lang.Integer */listId = super.__insert(LIST, java.util.List.class, java.lang.Boolean.TRUE, list.size()); // <== []
			dao.Context.getLookupFor(this).useRowIdToSetObject(java.util.List.class, listId, list);
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.List.class, list, listId);
			int i = 0;
			for (final java.lang.Object p: list) {
				final Reference r = new Reference(java.util.List.class, list, i, p.getClass(), p);
			  /*final java.lang.Integer id = */super.__insertCrossReference(LIST_X_REF, LIST_ID, listId, r); // <== [#]
				dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.List.class, list, i/*==r.index*/, r.p, r.i);
				i++;
			}
			super.__update(LIST, listId, list.size()); // update count
		} else {
		  //throw new IllegalStateException();
		}
		return listId;
	}
	public java.lang.Integer insert(final java.lang.Object o, final java.lang.Object p) {
	  /*final */java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.List.class, o);
		if (null == listId) {
			throw new IllegalStateException();
		}
		@SuppressWarnings("unchecked")
		final java.util.List<java.lang.Object> list = (java.util.List<java.lang.Object>)o;
		final Reference r = new Reference(java.util.List.class, list, list.size(), p.getClass(), p);
		final java.lang.Integer id = super.__insertCrossReference(LIST_X_REF, LIST_ID, listId, r); // <== [#]
/*ACFR*/dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.List.class, list, r.index, r.p, r.i);
		if (true == list instanceof dao.util.interface_List) {
			((dao.util.interface_List<java.lang.Object>)list).add(p, Break.Recursion);
		} else {
			list.add(p);
		}
		super.__update(LIST, listId, list.size()); // update count
		return id;
	}
	
	@Override
	public java.lang.Object select(final java.lang.Integer listId) {
		@SuppressWarnings("unchecked")
	  /*final */java.util.List<java.lang.Object> list = (java.util.List<java.lang.Object>)dao.Context.getLookupFor(this).useRowIdToGetObject(java.util.List.class, listId);
		if (null == list) {
			final Indexed ind = this.__select(LIST, listId); // <== []
		  /*final java.util.List<java.lang.Object> */list = new dao.util.ArrayList<>(/*ind.count, */dro.lang.Break.Recursion);
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.List.class, list, listId); // We *have* to do this, to avoid recursion, simply, put
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
				for (int i = 0; i < ind.count; i++) {
					final Reference r = this.__selectCrossReference(LIST_X_REF, LIST_ID, listId, new Reference(java.util.List.class, list, i, null, null)); // <== [#]
			/*ACFR*/dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.List.class, list, i, r.p, r.i);
					if (true == list instanceof dao.util.interface_List) {
						((dao.util.interface_List<java.lang.Object>)list).add(r.p, Break.Recursion);
					} else {
						list.add(r.p);
					}
				}
			} else {
				final Reference[] ra = this.__selectCrossReferences(LIST_X_REF, LIST_ID, listId); // <== []
				for (int i = 0; i < ra.length; i++) {
					if (true == ind.indexed) { 
						dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.List.class, list, ra[i].index, ra[i].p, ra[i].i);
					}
					dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.List.class, list, ra[i].p, ra[i].i);
					if (true == list instanceof dao.util.interface_List) {
						((dao.util.interface_List<java.lang.Object>)list).add(ra[i].p, Break.Recursion);
					} else {
				/*ACFR*/list.add(ra[i].p);
					}
				}
			}
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.List.class, list, listId);
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.Object select(final java.lang.Object o, final java.lang.Object i) {
		final java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.List.class, o);
	  /*final */java.util.List<java.lang.Object> list = null;
		if (null == listId) {
		  //final Indexed ind = this.__select(LIST, listId); // <== []
		  /*final java.util.List<?> */list = new java.util.ArrayList<>(/*ind.count*/);
		} else {
		  /*final java.util.List<java.lang.Object> */list = (java.util.List<java.lang.Object>)o;
		}
	  /*final */java.lang.Object p = null;
		if (null != list) {
		  /*final */Reference r = null;
			final java.lang.Integer index = (java.lang.Integer)i;
			// FIXME: (was null == list.get(index), unconditionally, but that obviously went into recursive loop..
			if (true == list instanceof dao.util.interface_List) {
				p = ((dao.util.interface_List<java.lang.Object>)list).get(index, Break.Recursion);
			} else {
				p = list.get(index);
			}
			if (null == p) {
			  /*final Reference */r = this.__selectCrossReference(LIST_X_REF, LIST_ID, listId, new Reference(java.util.List.class, list, index, null, null)); // <== [#]
				dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.List.class, list, r.index, r.p, r.i);
				p = r.p;
				if (true == list instanceof dao.util.interface_List) {
				  /*p = */((dao.util.interface_List<java.lang.Object>)list).set(index, p, Break.Recursion);
				} else {
				  /*p = */list.set(index, p);
				}
			}
		  /*if (null != r) {
				if (true == list instanceof dao.util.interface_List) {
					p = ((dao.util.interface_List<java.lang.Object>)list).get(index, Break.Recursion);
				} else {
					p = list.get(index);
				}
			}*/
		}
		return p;
	}
	
	@Override
	public void update(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	  /*final java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.List.class, o);
		if (null == listId) {
			throw new IllegalStateException();
		}
		final java.util.List<?> list = (java.util.List<?>)o;
		if (null != list) {
			super.__update(LIST, listId, list.size());
		}*/
	}
	@Override
	public void update(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		final java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.List.class, o);
		if (null == listId) {
			throw new IllegalStateException();
		}
		final java.util.List<?> list = (java.util.List<?>)o;
		final java.lang.Integer index = (java.lang.Integer)i;
		final Reference r = this.__selectCrossReference(LIST_X_REF, LIST_ID, listId, new Reference(java.util.List.class, list, index, (java.lang.Class<?>)null, (java.lang.Integer)null)); // <== [#]
		if (p.getClass() == r.cp) {
			final java.lang.Integer j = dao.Context.getLookupFor(this).useObjectAndIndexToGetRowId(java.util.List.class, list, index);
			this.__updateCrossReference(LIST_X_REF, LIST_ID, listId, new Reference(java.util.List.class, list, index, p.getClass(), p, j));
		} else {
			dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(this.c, list, r.index);
			this.__deleteCrossReference(LIST_X_REF, LIST_ID, listId, r);
			this.__insertCrossReference(LIST_X_REF, LIST_ID, listId, new Reference(java.util.List.class, list, index, p.getClass(), p));
			dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.List.class, list, index, r.p, r.i);
		}
	}
	
	@Override
	public void delete(final java.lang.Integer listId) {
		@SuppressWarnings("unchecked")
	  /*final */java.util.List<java.lang.Object> list = (java.util.List<java.lang.Object>)dao.Context.getLookupFor(this).useRowIdToGetObject(java.util.List.class, listId);
		if (null != list) {
			final Reference[] ra = this.__selectCrossReferences(LIST, LIST_ID, listId);
			for (int i = 0; i < ra.length; i++) {
				dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.List.class, list, i);
				this.__deleteCrossReference(LIST_X_REF, LIST_ID, listId, ra[i].delete()); // <== [#]
				if (true == list instanceof dao.util.interface_List) {
			/*ACFR*/((dao.util.interface_List<?>)list).remove(ra[i].p, Break.Recursion);
				} else {
			/*ACFR*/list.remove(ra[i].p);
				}
			}
			this.__delete(LIST, listId); // <== []
			dao.Context.getLookupFor(this).useRowIdToRemoveObject(java.util.List.class, listId);
			dao.Context.getLookupFor(this).useObjectToRemoveRowId(java.util.List.class, list);
		} else {
			throw new IllegalStateException();
		}
	}
	@Override
	public void delete(final java.lang.Object o) {
		final java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToRemoveRowId(java.util.List.class, o);
		if (null != listId) {
			this.delete(listId);
		} else {
			throw new IllegalStateException();
		}
	}
	public void delete(final java.lang.Object o, final java.lang.Object i) {
		final java.lang.Integer listId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.List.class, o);
		if (null == listId) {
			throw new IllegalStateException();
		}
		final java.util.List<?> list = (java.util.List<?>)o;
		final java.lang.Object p;// = null;
		final java.lang.Integer j;// = null;
		if (true == i instanceof java.lang.Integer) {
			final java.lang.Integer index = (java.lang.Integer)i;
			if (true == list instanceof dao.util.interface_List) {
				p = ((dao.util.interface_List<?>)list).get(index, Break.Recursion);
			} else {
				p = list.get(index);
			}
		  /*final java.lang.Integer *//*id*/j = dao.Context.getLookupFor(this).useObjectAndIndexToGetRowId(java.util.List.class, list, index);
		} else {
			p = i;
		  /*final java.lang.Integer *//*id*/j = dao.Context.getLookupFor(this).useObjectAndFirstToGetRowId(java.util.List.class, list, p);
		}
/*ACFR*/final Reference r = this.__selectCrossReference(LIST_X_REF, LIST_ID, listId, new Reference(java.util.List.class, list, /*index*/(java.lang.Integer)null, p.getClass(), p, (java.lang.Integer)j)); // <== [#]
/*TODO*/r.cp = dao.lang.Class.getAccessClass(p);
/*TODO*/r.p = p;
		if (true == i instanceof java.lang.Integer) {
		  /*final java.lang.Integer id = */dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.List.class, list, r.index);
		} else {
		  /*final java.lang.Integer id = */dao.Context.getLookupFor(this).useObjectAndFirstToRemoveRowId(java.util.List.class, list, r.p);
		}
		this.__deleteCrossReference(LIST_X_REF, LIST_ID, listId, r.delete()); // <== [#]
		if (true == i instanceof java.lang.Integer) {
			final java.lang.Integer index = (java.lang.Integer)i;
			if (true == list instanceof dao.util.interface_List) {
			  /*p = */((dao.util.interface_List<?>)list).remove(index, Break.Recursion);
			} else {
			  /*p = */list.remove(index);
			}
		} else {
			if (true == list instanceof dao.util.interface_List) {
			  /*p = */((dao.util.interface_List<?>)list).remove(p, Break.Recursion);
			} else {
			  /*p = */list.remove(p);
			}
		}
	}
	@Override
	public void delete() {
		final java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = (java.util.Map<java.lang.Object, java.lang.Integer>)dao.Context.getLookupFor(this).objectToRowId.get(java.util.List.class);
		if (null != objectToRowId) {
			final java.util.Set<?> set = objectToRowId.keySet();
			for (final java.lang.Object o: set) {
				this.delete(o);
			}
		}
	}
}