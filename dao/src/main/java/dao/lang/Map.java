package dao.lang;

import dro.lang.Break;

public class Map extends Keyed {
	public static final java.lang.String MAP = "MAP";
	public static final java.lang.String MAP_ID = "MAP_ID";
	public static final java.lang.String MAP_X_REF = "MAP_X_REF";
	
	public Map() {
		super(new dao.util.HashMap<java.lang.Object, java.lang.Object>(dro.lang.Break.Recursion));
	}
	
	protected Map length(final int size) {
		return (Map)super.count(size);
	}
	
	@Override
	public java.lang.Integer insert(final java.lang.Object o) {
	  /*final */java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
		if (null == mapId) {
			@SuppressWarnings("unchecked")
		  //final java.util.Map<?, ?> map = (java.util.Map<?, ?>)o;
			final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)o;
		  /*final java.lang.Integer */mapId = super.__insert(MAP, java.util.Map.class, java.lang.Boolean.FALSE, map.size()); // <== []
map.put("$id", mapId);
			dao.Context.getLookupFor(this).useRowIdToSetObject(java.util.Map.class, mapId, map);
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.Map.class, map, mapId);
			int i = 0;
			for (final java.lang.Object ok: map.keySet()) {
				final java.lang.Object ov;// = null;
				if (true == map instanceof dao.util.interface_Map) {
					ov = ((dao.util.interface_Map<?,?>)map).get(ok, Break.Recursion);
				} else {
					ov = map.get(ok);
				}
				final Reference r = new Reference(java.util.Map.class, map, i, ok.getClass(), ok, ov.getClass(), ov);
			  /*final java.lang.Integer id = */super.__insertCrossReference(MAP_X_REF, MAP_ID, mapId, r); // <== [#]
				// TODO: if indexed then..
			  //dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Map.class, map, r.index, r.ik); // Was "id" but needed to change to r.*ik* so that we can use the current Reference mechanism to get the id
				dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Map.class, map, r.ok, r.ik); // Was "id" but needed to change to r.*ik* so that we can use the current Reference mechanism to get the id
				i++;
			}
			super.__update(MAP, mapId, map.size()); // update count
		} else {
		  //throw new IllegalStateException();
		}
		return mapId;
	}
	@Override
	public java.lang.Integer insert(final java.lang.Object o, final java.lang.Object k, final java.lang.Object v) {
		final java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
		if (null == mapId) {
			throw new IllegalStateException();
		}
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)o;
		final Reference r = new Reference(java.util.Map.class, map, map.size(), k.getClass(), k, v.getClass(), v);
		final java.lang.Integer id = super.__insertCrossReference(MAP_X_REF, MAP_ID, mapId, r); // <== [#]
		// TODO: if indexed then..
	  //dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Map.class, map, r.index, r.ik); // Was "id" but needed to change to r.*ik* so that we can use the current Reference mechanism to get the id
		dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Map.class, map, r.ok, r.ik); // Was "id" but needed to change to r.*ik* so that we can use the current Reference mechanism to get the id
		if (true == map instanceof dao.util.interface_Map) {
			((dao.util.interface_Map<java.lang.Object, java.lang.Object>)map).put(k, v, Break.Recursion);
		} else {
			map.put(k, v);
		}
		super.__update(MAP, mapId, map.size()); // update count
		return id;
	}
	
	@Override
	public java.lang.Object select(final java.lang.Integer mapId) {
		@SuppressWarnings("unchecked")
	  /*final */java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)dao.Context.getLookupFor(this).useRowIdToGetObject(java.util.Map.class, mapId);
		if (null == map) {
			final Keyed ind = this.__select(MAP, mapId); // <== []
		  /*final java.util.Map<java.lang.Object, java.lang.Object> */map = new dao.util.HashMap<>(/*ind.count, */dro.lang.Break.Recursion);
			dao.Context.getLookupFor(this).useRowIdToSetObject(java.util.Map.class, mapId, map); // We *have* to do this here, to avoid recursion, simply
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
				for (int i = 0; i < ind.count; i++) {
					final Reference r = this.__selectCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, i, null, null, null, null)); // <== [#]
					if (true == ind.indexed) {
						dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Map.class, map, r.index, null, r.ik);
					}
					dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Map.class, map, r.ok, r.ik);
					if (true == map instanceof dao.util.interface_Map) {
						((dao.util.interface_Map<java.lang.Object, java.lang.Object>)map).put(r.ok, r.ov, Break.Recursion);
					} else {
						map.put(r.ok, r.ov);
					}
				}
			} else {
				final Reference[] ra = this.__selectCrossReferences(MAP_X_REF, MAP_ID, mapId); // <== []
				for (int i = 0; i < ra.length; i++) {
					if (true == ind.indexed) {
						dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Map.class, map, ra[i].index, null, ra[i].ik);
					}
					dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Map.class, map, ra[i].ok, ra[i].ik);
					if (true == map instanceof dao.util.interface_Map) {
						((dao.util.interface_Map<java.lang.Object, java.lang.Object>)map).put(ra[i].ok, ra[i].ov, Break.Recursion);
					} else {
						map.put(ra[i].ok, ra[i].ov);
					}
				}
			}
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.Map.class, map, mapId);
		}
		return map;
	}
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.Object select(final java.lang.Object o, final java.lang.Object k) {
		final java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
	  /*final */java.util.Map<java.lang.Object, java.lang.Object> map = null;
		if (null == mapId) {
		  //final Keyed ind = this.__select(MAP, mapId); // <== []
		  /*final java.util.Map<?, ?> */map = new dao.util.HashMap<>(/*ind.count*/);
		} else {
		  /*final java.util.Map<java.lang.Object, java.lang.Object> */map = (java.util.Map<java.lang.Object, java.lang.Object>)o;
		}
	  /*final */java.lang.Object v = null;
		if (null != map) {
		  /*final */Reference r = null;
			if (false == map.containsKey(k)) {
				if (true == k instanceof java.lang.Integer) {
				  /*final Reference */r = this.__selectCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, (java.lang.Integer)k, null, null, null, null)); // <== [#]
				} else {
				  /*final Reference */r = this.__selectCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, null, k.getClass(), k, null, null)); // <== [#]
				}
				// TODO if indexed then..
			  //dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Map.class, map, r.index, r.ik);
				dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Map.class, map, r.ok, r.ik);
			}
		  //if (null != r) {
				if (true == map instanceof dao.util.interface_Map) {
					v = ((dao.util.interface_Map<java.lang.Object, java.lang.Object>)map).get(k, Break.Recursion);
				} else {
					v = map.get(k);
				}
		  //}
		}
		return v;
	}
	
	@Override
	public void update(final java.lang.Object o) {
		throw new UnsupportedOperationException();
	  /*final java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
		if (null == mapId) {
			throw new IllegalStateException();
		}
		final java.util.Map<?,?> map = (java.util.Map<?,?>)o;
		if (null != map) {
			super.__update(MAP, mapId, map.size());
		}*/
	}
	@Override
	public void update(final java.lang.Object o, final java.lang.Object k, final java.lang.Object v) {
		final java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
		if (null == mapId) {
			throw new IllegalStateException();
		}
		final java.util.Map<?,?> map = (java.util.Map<?,?>)o;
	  /*final */Reference r = null;
		if (true == k instanceof java.lang.Integer) {
		  /*final Reference */r = this.__selectCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, (java.lang.Integer)k, (java.lang.Class<?>)null, (java.lang.Integer)null, (java.lang.Class<?>)null, (java.lang.Integer)null)); // <== [#]
		} else {
			final java.lang.Integer ik = dao.Context.getLookupFor(this).useObjectAndUniqueToGetRowId(java.util.Map.class, map, k);
		  /*final Reference */r = this.__selectCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, (java.lang.Integer)null, k.getClass(), k, ik)); // <== [#]
		}
		if (k.getClass() == r.ck && true == k.equals(r.ok) && v.getClass() == r.cv) { // Note: we will never have a value for cv as we've overloaded Reference constructor and we're not passing in v
			this.__updateCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, (java.lang.Integer)null, k.getClass(), k, v.getClass(), v));
		} else { // username=arussell#1 password=xxxxxxxx#2 arussell={..}#3 username=rwang#4 password=xxxxx#5 rwang={..}#6
			// TODO if indexed then..
		  //dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.Map.class, map, r.index);
			dao.Context.getLookupFor(this).useObjectAndUniqueToRemoveRowId(java.util.Map.class, map, r.ok);
			this.__deleteCrossReference(MAP_X_REF, MAP_ID, mapId, r);
			this.__insertCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, /*FIXME*/new java.lang.Integer(0), k.getClass(), k, v.getClass(), v));
			// TODO if indexed then..
		  //dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Map.class, map, (java.lang.Integer)null, r.ik);
			dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Map.class, map, r.ok, r.ik);
		}
	}
	
	@Override
	public void delete(final java.lang.Integer mapId) {
		@SuppressWarnings("unchecked")
	  /*final */java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)dao.Context.getLookupFor(this).useRowIdToGetObject(java.util.Map.class, mapId);
		if (null != map) {
			final Reference[] ra = this.__selectCrossReferences(MAP_X_REF, MAP_ID, mapId);
			for (int i = 0; i < (null == ra ? 0 : ra.length); i++) {
				// TODO if indexed then..
			  //dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.Map.class, map, i);
				dao.Context.getLookupFor(this).useObjectAndUniqueToRemoveRowId(java.util.Map.class, map, ra[i].ok);
				this.__deleteCrossReference(MAP_X_REF, MAP_ID, mapId, ra[i].delete()); // <== [#]
				if (true == map instanceof dao.util.interface_Map) {
					((dao.util.interface_Map<?, ?>)map).remove(ra[i].ok, Break.Recursion);
				} else {
					map.remove(ra[i].ok);
				}
			}
			this.__delete(MAP, mapId); // <== []
			dao.Context.getLookupFor(this).useRowIdToRemoveObject(java.util.Map.class, mapId);
			dao.Context.getLookupFor(this).useObjectToRemoveRowId(java.util.Set.class, map);
		}
	}
	@Override
	public void delete(final java.lang.Object o) {
		final java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
		if (null != mapId) {
			this.delete(mapId);
		} else {
			throw new IllegalStateException();
		}
	}
	@Override
	public void delete(final java.lang.Object o, final java.lang.Object k) {
		final java.lang.Integer mapId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Map.class, o);
		if (null == mapId) {
			throw new IllegalStateException();
		}
		final java.util.Map<?,?> map = (java.util.Map<?,?>)o;
		final java.lang.Object v;// = null;
		if (true == map instanceof dao.util.interface_Map) {
			v = ((dao.util.interface_Map<?, ?>)map).get(k, Break.Recursion);
		} else {
			v = map.get(k);
		}
		final java.lang.Integer /*id*/ik = dao.Context.getLookupFor(this).useObjectAndUniqueToGetRowId(java.util.Map.class, map, k);
		final Reference r = this.__selectCrossReference(MAP_X_REF, MAP_ID, mapId, new Reference(java.util.Map.class, map, (java.lang.Integer)null, k.getClass(), k, ik)); // <== [#]
/*TODO*/r.cv = dao.lang.Class.getAccessClass(v);
/*TODO*/r.ov = v;
		// TODO if indexed then..
	///*final java.lang.Integer id = */dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.Map.class, map, r.index);
	  /*final java.lang.Integer id = */dao.Context.getLookupFor(this).useObjectAndUniqueToRemoveRowId(java.util.Map.class, map, r.ok);
		this.__deleteCrossReference(MAP_X_REF, MAP_ID, mapId, r.delete()); // <== [#] //<==3
		if (true == map instanceof dao.util.interface_Map) {
		  /*v = */((dao.util.interface_Map<?, ?>)map).remove(k, Break.Recursion);
		} else {
		  /*v = */map.remove(k);
		}
	}
	@Override
	public void delete() {
		final java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = (java.util.Map<java.lang.Object, java.lang.Integer>)dao.Context.getLookupFor(this).objectToRowId.get(java.util.Map.class);
		if (null != objectToRowId) {
			final java.util.Set<?> set = objectToRowId.keySet();
			for (final java.lang.Object o: set) {
				this.delete(o);
			}
		}
	}
}