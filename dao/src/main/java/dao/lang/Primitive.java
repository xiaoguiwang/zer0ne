package dao.lang;

import dao.util.Access;

public class Primitive<T> extends Access {
	public static final java.lang.String ID = "ID";
	public static final java.lang.String VALUE = "VALUE";
	
	static final class Reference {
		public final java.lang.Class<?> c;
		public final java.lang.Object o;
		public final java.lang.Integer rowId;
		public Reference(final java.lang.Class<?> c, final java.lang.Object o) {
			this.c = c;
			this.o = o;
			this.rowId = dao.Adapter.access(c).insert(o);
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Integer rowId) {
			this.c = c;
			this.o = dao.Adapter.access(c).select(rowId);
			this.rowId = rowId;
		}
	}
	
	public Primitive(final T o) {
		super(o);
	}
	/*
	public java.lang.Object newOne(final java.lang.Object o) {
		throw new IllegalStateException();
	}
	*/
	protected java.lang.Integer __insert(final java.lang.Object o) {
		dao.Context.getAdapterFor(this).createTableIfNotExists( // Note: we do this because on cold-start we don't know the primaryIndex - this is the only way we have (so far) to re-state the primaryIndex for last-row-id/generated-key for insert
			this.m, // String name
			0, // Integer primaryIdIndex
			new java.lang.String[]{ID,VALUE}, // String[] columnNames
			new java.lang.Object[]{new java.math.BigInteger("0"),this.o} // Object[] columnTypes
		);
		final java.lang.Integer id = dao.Context.getAdapterFor(this).insertIntoTable(
			this.m,
			new java.lang.String[]{VALUE},
			new java.lang.Object[]{o}
		);
		return id;
	}
	public java.lang.Integer insert(final java.lang.Object o) {
	  /*final */java.lang.Integer id = (java.lang.Integer)dao.Context.getLookupFor(this).useObjectToGetRowId(this.c, o);
		if (null == id) {
		  /*final java.lang.Integer */id = this.__insert(o);
			dao.Context.getLookupFor(this).useObjectToSetRowId(this.c, o, id);
			dao.Context.getLookupFor(this).useRowIdToSetObject(this.c, id, o);
		}
		return id;
	}
	
	protected java.lang.Object[][] __select() {
	  /*final */java.lang.Object[][] pa = null;
		if (true == dao.Context.getAdapterFor(this).existsTable(this.m)) {
		  /*final java.lang.Object[][] */pa = dao.Context.getAdapterFor(this).selectFromTable(
				this.m, // String name
				new java.lang.String[]{ID,VALUE}, // String[] selectColumnNames
				new java.lang.Object[]{new java.math.BigInteger("0"),this.o}, // Object[] selectColumnTypes
				(java.lang.String[])null, // String[] whereColumnNames
				(java.lang.String[])null, // String[] whereColumnOperators
				(java.lang.Object[])null // Object[] whereColumnValues
			);
		}
		return pa;
	}
	public java.lang.Object[] select() {
	  /*final */java.lang.Object[] oa = null;
		final java.lang.Object[][] pa = this.__select();
	  /*final java.lang.Object[] */oa = new java.lang.Object[pa.length];
		for (int i = 0; i < (null == pa ? 0 : pa.length); i++) {
			oa[i] = /*this.newOne(*/pa[i][1]/*)*/;
			final java.lang.Integer id = (java.lang.Integer)pa[i][0];
			dao.Context.getLookupFor(this).useObjectToSetRowId(this.c, oa[i], id);
			dao.Context.getLookupFor(this).useRowIdToSetObject(this.c, id, oa[i]);
		}
		return oa;
	}
	protected java.lang.Object[] __select(final java.lang.Integer id) {
	  /*final */java.lang.Object[][] pa = null;
		if (true == dao.Context.getAdapterFor(this).existsTable(this.m)) {
		  /*final java.lang.Object[][] */pa = dao.Context.getAdapterFor(this).selectFromTable(
				this.m, // String name
				new java.lang.String[]{VALUE}, // String[] selectColumnNames
				new Object[]{this.o}, // Object[] selectColumnTypes
				new java.lang.String[]{ID}, // String[] whereColumnNames
				new java.lang.String[]{"="}, // String[] whereColumnOperators
				new Object[]{id} // Object[] whereColumnValues
			);
		}
		return null == pa ? null : pa[0];
	}
	@Override
	public java.lang.Object select(final java.lang.Integer id) {
	  /*final */java.lang.Object o = dao.Context.getLookupFor(this).useRowIdToGetObject(this.c, id);
		if (null == o) {
			final java.lang.Object[] oa = this.__select(id);
			if (null != oa && 1 == oa.length) {
				o = /*this.newOne(*/oa[0]/*)*/;
				dao.Context.getLookupFor(this).useObjectToSetRowId(this.c, o, id);
				dao.Context.getLookupFor(this).useRowIdToSetObject(this.c, id, o);
			}
		}
		return o;
	}
	
	protected void __update(final java.lang.Integer id, final java.lang.Object o) {
		if (true == dao.Context.getAdapterFor(this).existsTable(this.m)) {
			dao.Context.getAdapterFor(this).updateTableSet(
				this.m,
				new java.lang.String[]{VALUE},
				new java.lang.Object[]{o},
				new java.lang.String[]{ID},
				new java.lang.String[]{"="},
				new java.lang.Object[]{id}
			);
		}
	}
	@Override
	public void update(final java.lang.Integer id, final java.lang.Object o) {
		dao.Context.getLookupFor(this).useRowIdToSetObject(this.c, id, o);
		this.__update(id, o);
	}
	
	protected void __delete(final java.lang.Integer id) {
		if (true == dao.Context.getAdapterFor(this).existsTable(this.m)) {
			dao.Context.getAdapterFor(this).deleteFromTable(
				this.m,
				new java.lang.String[]{ID},
				new java.lang.String[]{"="},
				new Object[]{id}
			);
		}
	}
	@Override
	public void delete(final java.lang.Integer id) {
		this.__delete(id);
		final java.lang.Object o = dao.Context.getLookupFor(this).useRowIdToRemoveObject(this.c, id);
		dao.Context.getLookupFor(this).useObjectToRemoveRowId(this.c, o);
	}
	public void delete() {
		final java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = (java.util.Map<java.lang.Object, java.lang.Integer>)dao.Context.getLookupFor(this).objectToRowId.get(this.c);
		if (null != objectToRowId) {
			final java.lang.Object[] oa = objectToRowId.keySet().toArray(new java.lang.Object[0]);
			for (final java.lang.Object o: oa) {
				this.delete(objectToRowId.get(o));
			}
		}
	}
}