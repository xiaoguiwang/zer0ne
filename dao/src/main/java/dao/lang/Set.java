package dao.lang;

import dro.lang.Break;

// Optionally Ordered (meaning fifo/lilo)
// Never Indexed
// Always Unique (by Ref#Id)
public class Set extends Indexed {
	public static final java.lang.String SET = "SET";
	public static final java.lang.String SET_ID = "SET_ID";
	public static final java.lang.String SET_X_REF = "SET_X_REF";
	
	public Set() {
		super(new dao.util.HashSet<java.lang.Object>(dro.lang.Break.Recursion));
	}
	
	protected Set length(final int size) {
		return (Set)super.count(size);
	}
	
	@Override
	public java.lang.Integer insert(final java.lang.Object o) {
	  /*final */java.lang.Integer setId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Set.class, o);
		if (null == setId) {
			final java.util.Set<?> set = (java.util.Set<?>)o;
		  /*final java.lang.Integer */setId = super.__insert(SET, java.util.Set.class, java.lang.Boolean.FALSE, set.size()); // <== []
			dao.Context.getLookupFor(this).useRowIdToSetObject(java.util.Set.class, setId, set);
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.Set.class, set, setId);
			int i = 0;
			for (final java.lang.Object p: set) {
				final Reference r = new Reference(java.util.Set.class, set, i, p.getClass(), p);
			  /*final java.lang.Integer id = */super.__insertCrossReference(SET_X_REF, SET_ID, setId, r); // <== [#]
				// TODO: if indexed then..
			  //dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Set.class, set, r.index, r.i); // Was "id" but needed to change to r.*i* so that we can use the current Reference mechanism to get the id
				dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Set.class, set, r.p, r.i); // Was "id" but needed to change to r.*i* so that we can use the current Reference mechanism to get the id
				i++;
			}
			super.__update(SET, setId, set.size()); // update count
		} else {
		  //throw new IllegalStateException();
		}
		return setId;
	}
	public java.lang.Integer insert(final java.lang.Object o, final java.lang.Object p) {
	  /*final */java.lang.Integer setId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Set.class, o);
		if (null == setId) {
			throw new IllegalStateException();
		}
		@SuppressWarnings("unchecked")
		final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)o;
		final Reference r = new Reference(java.util.Set.class, set, set.size(), p.getClass(), p);
		final java.lang.Integer id = super.__insertCrossReference(SET_X_REF, SET_ID, setId, r); // <== [#]
		// TODO: if indexed then..
	  //dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Set.class, set, r.index, r.i); // Was "id" but needed to change to r.*i* so that we can use the current Reference mechanism to get the id
		dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Set.class, set, r.p, r.i); // Was "id" but needed to change to r.*i* so that we can use the current Reference mechanism to get the id
		if (true == set instanceof dao.util.interface_Set) {
			((dao.util.interface_Set<java.lang.Object>)set).add(p, Break.Recursion);
		} else {
			set.add(p);
		}
		super.__update(SET, setId, set.size()); // update count
		return id;
	}
	
	@Override
	public java.lang.Object select(final java.lang.Integer setId) {
		@SuppressWarnings("unchecked")
	  /*final */java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)dao.Context.getLookupFor(this).useRowIdToGetObject(java.util.Set.class, setId);
		if (null == set) {
			final Indexed ind = this.__select(SET, setId); // <== []
		  /*final java.util.Set<java.lang.Object> */set = new dao.util.HashSet<>(/*ind.count, */dro.lang.Break.Recursion);
			dao.Context.getLookupFor(this).useRowIdToSetObject(java.util.Set.class, setId, set); // We *have* to do this here, to avoid recursion, simply, put
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
				for (int i = 0; i < ind.count; i++) {
					final Reference r = this.__selectCrossReference(SET_X_REF, SET_ID, setId, new Reference(java.util.Set.class, set, i, null, null)); // <== [#]
					if (true == ind.indexed) { 
						dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Set.class, set, r.index, null, r.i);
					}
					dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Set.class, set, r.p, r.i);
					if (true == set instanceof dao.util.interface_Set) {
						((dao.util.interface_Set<java.lang.Object>)set).add(r.p, Break.Recursion);
					} else {
						set.add(r.p);
					}
				}
			} else {
				final Reference[] ra = this.__selectCrossReferences(SET_X_REF, SET_ID, setId); // <== []
				for (int i = 0; i < ra.length; i++) {
					if (true == ind.indexed) { 
						dao.Context.getLookupFor(this).useObjectAndIndexToSetRowId(java.util.Set.class, set, ra[i].index, null, ra[i].i);
					}
					dao.Context.getLookupFor(this).useObjectAndUniqueToSetRowId(java.util.Set.class, set, ra[i].p, ra[i].i);
					if (true == set instanceof dao.util.interface_Set) {
						((dao.util.interface_Set<java.lang.Object>)set).add(ra[i].p, Break.Recursion);
					} else {
						set.add(ra[i].p);
					}
				}
			}
			dao.Context.getLookupFor(this).useObjectToSetRowId(java.util.Set.class, set, setId);
		}
		return set;
	}
	@Override
	public java.lang.Object select(final java.lang.Object o, final java.lang.Object i) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void update(final java.lang.Object o, final java.lang.Object i, final java.lang.Object p) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void delete(final java.lang.Integer setId) {
		@SuppressWarnings("unchecked")
	  /*final */java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)dao.Context.getLookupFor(this).useRowIdToGetObject(java.util.Set.class, setId);
		if (null != set) {
			final Reference[] ra = this.__selectCrossReferences(SET_X_REF, SET_ID, setId);
			for (int i = 0; i < (null == ra ? 0 : ra.length); i++) {
				// TODO if indexed then..
			  //dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.Set.class, set, i);
				dao.Context.getLookupFor(this).useObjectAndUniqueToRemoveRowId(java.util.Set.class, set, ra[i].p);
				this.__deleteCrossReference(SET_X_REF, SET_ID, setId, ra[i].delete()); // <== [#]
				if (true == set instanceof dao.util.interface_Set) {
					((dao.util.interface_Set<?>)set).remove(ra[i].p, Break.Recursion);
				} else {
					set.remove(ra[i].p);
				}
			}
			this.__delete(SET, setId); // <== []
			dao.Context.getLookupFor(this).useRowIdToRemoveObject(java.util.Set.class, setId);
			dao.Context.getLookupFor(this).useObjectToRemoveRowId(java.util.Set.class, set);
		} else {
			throw new IllegalStateException();
		}
	}
	@Override
	public void delete(final java.lang.Object o) {
		final java.lang.Integer setId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Set.class, o);
		if (null != setId) {
			this.delete(setId);
		} else {
			throw new IllegalStateException();
		}
	}
	@Override
	public void delete(final java.lang.Object o, final java.lang.Object p) {
		final java.lang.Integer setId = dao.Context.getLookupFor(this).useObjectToGetRowId(java.util.Set.class, o);
		if (null == setId) {
			throw new IllegalStateException();
		}
		final java.util.Set<?> set = (java.util.Set<?>)o;
		final java.lang.Integer /*id*/i = dao.Context.getLookupFor(this).useObjectAndUniqueToGetRowId(java.util.Set.class, set, p);
		final Reference r = this.__selectCrossReference(SET_X_REF, SET_ID, setId, new Reference(java.util.Set.class, set, (java.lang.Integer)null, p.getClass(), p, i)); // <== [#]
		// TODO if indexed then..1
	///*final java.lang.Integer id = */dao.Context.getLookupFor(this).useObjectAndIndexToRemoveRowId(java.util.Set.class, set, r.index);
	  /*final java.lang.Integer id = */dao.Context.getLookupFor(this).useObjectAndUniqueToRemoveRowId(java.util.Set.class, set, r.p);
	  //dao.Context.getLookupFor(this).useObjectAndUniqueToRemoveRowId(java.util.Set.class, set, p);
		this.__deleteCrossReference(SET_X_REF, SET_ID, setId, r.delete()); // <== [#]
		if (true == set instanceof dao.util.interface_Set) {
			((dao.util.interface_Set<?>)set).remove(p, Break.Recursion);
		} else {
			set.remove(p);
		}
	}
	@Override
	public void delete() {
		final java.util.Map<java.lang.Object, java.lang.Integer> objectToRowId = (java.util.Map<java.lang.Object, java.lang.Integer>)dao.Context.getLookupFor(this).objectToRowId.get(java.util.Set.class);
		if (null != objectToRowId) {
			final java.util.Set<?> set = objectToRowId.keySet();
			for (final java.lang.Object o: set) {
				this.delete(objectToRowId.get(o));
			}
		}
	}
}