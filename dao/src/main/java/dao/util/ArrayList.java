package dao.util;

public class ArrayList<E> extends java.util.ArrayList<E> implements dao.util.interface_List<E>, dro.lang.Id {
	private static final long serialVersionUID = 0L;
	
	public final dro.util.Id id;
	{
		id = new dro.util.Id(this);
	}
	@Override
	public java.lang.Object id() {
		return id;
	}
	
	public ArrayList(final dro.lang.Break.Recursion br) {
		super();
	}
	public ArrayList() {
		super();
		((dao.lang.List)dao.Adapter.access(java.util.List.class)).insert(this);
	}
	
	@Override
	public boolean add(final E o) {
	  /*final */boolean added = false;
		((dao.lang.List)dao.Adapter.access(java.util.List.class)).insert(this, o);
	  //added = super.add(o);
/**/added = true; // FIXME: We could interpret rowId as whether this was inserted..
		return added;
	}
	@Override
	public boolean add(final E o, final dro.lang.Break.Recursion br) {
	  /*final */boolean added = false;
		added = super.add(o);
		return added;
	}
	
	@Override
	public E set(final int index, final E o) {
	  /*final */E e = null;
	  /*e = (E)*/((dao.lang.List)dao.Adapter.access(java.util.List.class)).update(this, index, o);
	  //e = super.set(index, o);
		return e;
	}
	@Override
	public E set(final int index, final E o, final dro.lang.Break.Recursion br) {
	  /*final */E e = null;
		e = super.set(index, o);
		return e;
	}
	
	@Override @SuppressWarnings("unchecked")
	public E get(final int index) {
	  /*final */E e = null;
		e = (E)((dao.lang.List)dao.Adapter.access(java.util.List.class)).select(this, index);
	  //e = super.get(index);
		return e;
	}
	@Override
	public E get(final int index, final dro.lang.Break.Recursion br) {
	  /*final */E e = null;
		e = super.get(index);
		return e;
	}
	
	@Override
	public boolean remove(final java.lang.Object o) {
	  /*final */boolean removed = false;
		((dao.lang.List)dao.Adapter.access(java.util.List.class)).delete(this, o);
	  //removed = super.remove(o);
/**/removed = true; // FIXME: We can't actually know, because delete didn't return a (useful) value.. 
		return removed;
	}
	@Override
	public boolean remove(final java.lang.Object o, final dro.lang.Break.Recursion br) {
	  /*final */boolean removed = false;
		removed = super.remove(o);
		return removed;
	}
	@Override
	public E remove(final int index) {
	  /*final */E o = null;
o = super.get(index);
		((dao.lang.List)dao.Adapter.access(java.util.List.class)).delete(this, index);
	  /*o = */super.remove(index);  // FIXME: We can't actually know, because delete didn't return a (useful) value..
		return o;
	}
	@Override
	public E remove(final int index, final dro.lang.Break.Recursion br) {
	  /*final */E o = null;
		o = super.remove(index);
		return o;
	}
}