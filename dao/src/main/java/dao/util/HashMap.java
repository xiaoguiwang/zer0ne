package dao.util;

public class HashMap<K, V> extends java.util.HashMap<K, V> implements dao.util.interface_Map<K, V> {
	private static final long serialVersionUID = 0L;
	
	public final dro.util.Id id;
	{
		id = new dro.util.Id(this);
	}
	@Override
	public java.lang.Object id() {
		return id;
	}
	
	public HashMap(final dro.lang.Break.Recursion br) {
		super();
	}
	public HashMap() {
		super();
		((dao.lang.Map)dao.Adapter.access(java.util.Map.class)).insert(this);
	}
  /*public HashMap(final int initialCapacity) {
		super(initialCapacity);
		((dao.Adapter.Map)dao.Adapter.access(java.util.Map.class)).insert(this);
	}*/
	
	@Override
	public V put(final K k, final V v) {
	  /*final */V w = null;
		if (false == k.toString().equals("$id")) {
			final dao.lang.Map a = (dao.lang.Map)dao.Adapter.access(java.util.Map.class);
			final java.lang.Object o = a.select(this, k);
			if (null == o) {
				a.insert(this, k, v);
			} else {
				a.update(this, k, v);
			}
		}
		w = super.put(k, v);
		return w;
	}
	@Override
	public V put(final K k, final V v, final dro.lang.Break.Recursion br) {
	  /*final */V w = null;
		w = super.put(k, v);
		return w;
	}
	
	@Override
	public V get(final java.lang.Object k) {
	  /*final */V w = null;
		if (false == k.toString().equals("$id")) {
		  /*w = (V)*/((dao.lang.Map)dao.Adapter.access(java.util.Map.class)).select(this, k);
		}
		w = super.get(k);
		return w;
	}
	@Override
	public V get(final java.lang.Object k, final dro.lang.Break.Recursion br) {
	  /*final */V w = null;
		w = super.get(k);
		return w;
	}
	
	@Override
	public boolean remove(final java.lang.Object k, final java.lang.Object v) {
	  /*final */boolean removed = false;
		if (false == k.toString().equals("$id")) {
			((dao.lang.Map)dao.Adapter.access(java.util.Map.class)).delete(this, k);
		}
		removed = super.remove(k, v);
		return removed;
	}
	@Override
	public boolean remove(final java.lang.Object k, final java.lang.Object v, final dro.lang.Break.Recursion br) {
	  /*final */boolean removed = false;
		removed = super.remove(k, v);
		return removed;
	}
	@Override
	public V remove(final java.lang.Object k) {
	  /*final */V w = null;
w = (V)((dao.lang.Map)dao.Adapter.access(java.util.Map.class)).select(this, k);
		((dao.lang.Map)dao.Adapter.access(java.util.Map.class)).delete(this, k);
//w = super.remove(k);
		return w;
	}
	@Override
	public V remove(final java.lang.Object k, final dro.lang.Break.Recursion br) {
	  /*final */V w = null;
		w = super.remove(k);
		return w;
	}
}