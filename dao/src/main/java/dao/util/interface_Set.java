package dao.util;

public interface interface_Set<E> extends java.util.Set<E>, dro.lang.Id {
	public abstract boolean add(final E e, final dro.lang.Break.Recursion br);
	public abstract boolean remove(final java.lang.Object o, final dro.lang.Break.Recursion br);
}