package dao.lang;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClassTest {
	@Test
	@SuppressWarnings("unchecked")
	public void test_0001() {
		final java.util.Map<java.lang.Object, java.lang.Object> map = new java.util.HashMap<>();
		final java.util.Map<java.lang.String, java.lang.Object> maps = new java.util.HashMap<>();
	  /*final */java.util.Map<java.lang.String, java.lang.Object> map1 = new java.util.HashMap<>();
		map1.put("Hello", "There?");
		final java.lang.Object o = dao.lang.Class.associate(java.util.HashMap.class, map1);
		map.put(map1, map1);
		map1.put("Hello", "World!");
		final java.util.Map<java.lang.String, java.lang.Object> map2 = new java.util.HashMap<>();
		map2.put("Hello", "World!");
		maps.put("map1", map1);
		maps.put("map2", map2);
	  /*final java.util.Map<java.lang.String, java.lang.Object> */map1 = (java.util.Map<java.lang.String, java.lang.Object>)map.get(map1);
		Assert.assertNull(map1);
	  /*final java.util.Map<java.lang.String, java.lang.Object> */map1 = (java.util.Map<java.lang.String, java.lang.Object>)dao.lang.Class.associated(java.util.HashMap.class, o);
		Assert.assertNotNull(map1);
	}
}