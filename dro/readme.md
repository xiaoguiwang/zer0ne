# readme.md #

package dro.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class Link {
	protected final Map<java.lang.Object, java.lang.Object> links;// = new LinkedHashMap<>();
	protected final Map<java.lang.Object, java.lang.Object> linkeds;// = new LinkedHashMap<>();
	
	{
	  //links = new LinkedHashMap<java.lang.Object, java.lang.Object>();
	  //linkeds = new LinkedHashMap<java.lang.Object, java.lang.Object>();
	}
	
	public Link() {
		this.links = new LinkedHashMap<java.lang.Object, java.lang.Object>();
		this.linkeds = new LinkedHashMap<java.lang.Object, java.lang.Object>();
	}
	
	public Link link(final java.lang.Object link, final java.lang.Object linked) {
		this.links.put(link, linked);
		this.linkeds.put(linked, link);
		return this;
	}
	public java.lang.Object useLinkToGetLinked(final java.lang.Object link) {
		return this.links.get(link);
	}
	public java.lang.Object useLinkedToGetLink(final java.lang.Object linked) {
		return this.linkeds.get(linked);
	}
	
	public Link useLinkToUnlink(final java.lang.Object link) {
		final boolean linked = this.links.containsKey(link);
		if (true == linked) {
			this.useLinkedToUnlink(this.links.remove(link));
		}
		return this;
	}
	public Link useLinkedToUnlink(final java.lang.Object linked) {
		final boolean link = this.linkeds.containsKey(linked);
		if (true == link) {
			this.linkeds.remove(this.linkeds.remove(linked));
		}
		return this;
	}
}