package dro;

import java.io.FileNotFoundException;
import java.io.IOException;

import dro.util.Properties;
import dro.util.State;

public abstract class AbstractAdapter {
	public static final String getName(final Class<?> c) {
		return c.getPackage().getName()+"."+c.getSimpleName();
	}
	
	protected Properties p;// = null;
	protected Exception e;// = null;
	protected State state = State.NONE;
	private boolean background = false;
	private boolean once = false;
	
	{
	  //p = null;
	  //e = null;
	}
	protected AbstractAdapter() {
		try {
			this.p = new Properties(this.getClass());
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	protected AbstractAdapter(final Properties p) {
		this.p = p;
	}
	protected AbstractAdapter(final String filename) {
		try {
			this.p = new dro.util.Properties(filename);
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected void exception(final Exception e) {
	/**/e.printStackTrace(System.err);
		this.e = e;
	}
	public Exception exception() {
		return this.e;
	}
	
	protected void setProperty(final String propertyName) {
		this.setProperty(propertyName, System.getProperty(propertyName));
	}
	protected void setProperty(final String propertyName, final String propertyValue) {
		if (null != propertyValue) {
			if (null == this.p) {
				this.p = new Properties();
			}
			this.p.setProperty(propertyName, propertyValue);
		}
	}
	protected String getProperty(final String propertyName) {
		String propertyValue = null;
		if (null != this.p) {
			propertyValue = (String)this.p.getProperty(propertyName);
		}
		return propertyValue;
	}
	
	public AbstractAdapter background(final boolean background) {
		this.background = background;
		return this;
	}
	public boolean background() {
		return this.background;
	}
	
	public AbstractAdapter once(final boolean once) {
		this.once = once;
		return this;
	}
	public boolean once() {
		return this.once;
	}
	
	public State state() {
		return this.state;
	}
	
	public void shutdown() throws java.lang.Exception { // FIXME
	}
}