package dro;

public class Adapter {
	public static class Map {
		public static final Map Return = (Map)null;
	}
	public static class Return {
		public static final dro.Adapter.Return Adapter = (dro.Adapter.Return)null;
		public static final dro.data.Context.Return Context = (dro.data.Context.Return)null;
		public static final dro.data.Context.Map Map = (dro.data.Context.Map)null;
	}
	
	public static Adapter newAdapter(final java.lang.String className) {
		final dro.Adapter adapter;// = null;
		final java.lang.Class<?> clazz;// = null;
		try {
			clazz = java.lang.Class.forName(className);
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		try {
			adapter = (dro.Adapter)clazz.newInstance();
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		return adapter;
	}
	
	protected dro.util.Properties p;// = null;
	protected dro.lang.Event.Listener el;// = null;
	
	{
		p = null;
		el = null;
	}
	
	public Adapter() {
	  //super(); // Implicit super constructor..
	  /*try {
			this.properties(new Properties(this));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		if (null == Properties.properties()) {
			Properties.properties(this.properties());
		}*/
	}
	
	public Adapter properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
	public Adapter invoke(final dro.lang.Event e) {
		return this;
	}
	public java.lang.Object invoke(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodNames, final java.lang.Object[] oa) {
		// Default implementation (local anonymous context)
		return (java.lang.Object)null;
	}
}