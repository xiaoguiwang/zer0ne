package dro;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import dro.util.Properties;
import dro.util.State;

public class CommandAdapter extends AbstractAdapter {
	private static final String className = dro.lang.Class.getName(CommandAdapter.class);
	
	private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	public static class Return extends dro.lang.Return {
		public static final CommandAdapter.Return CommandAdapter = (CommandAdapter.Return)null;
		public static final Properties.Return Properties = (Properties.Return)null;
		public static final Process.Return Process = (Process.Return)null;
	}
	public static class Property {
		public final static String Process = dro.lang.Class.getName(java.lang.Process.class);
	}
	
	public static class Process {
		public static String name = Property.name;
		public static class Return extends dro.lang.Return {
			public static final Process.Return File = (Process.Return)null;
			public static final OutputStream.Return OutputStream = (OutputStream.Return)null;
			public static final InputStream.Return InputStream = (InputStream.Return)null;
			public static final ErrorStream.Return ErrorStream = (ErrorStream.Return)null;
			public static final CommandAdapter.Return CommandAdapter = (CommandAdapter.Return)null;
		}
		public static class Property {
			public static final String Process = CommandAdapter.getName(java.lang.Process.class);
			public static final String name = Process+"#name";
		}
		
		public static class OutputStream {
			public static String name = Property.name;
			public static class Return extends dro.lang.Return {
				public static final Process.Return Process = (Process.Return)null;
				public static final OutputStream.Return OutputStream = (OutputStream.Return)null;
				public static final CommandAdapter.Return CommandAdapter = (CommandAdapter.Return)null;
			}
			public static class Property {
				public static final String OutputStream = CommandAdapter.getName(java.io.OutputStream.class);
				public static final String name = OutputStream+"#name";
			}
			
		  //protected Properties p;// = null;
			protected Process p = null;
			protected java.io.OutputStream os = null;
			private java.io.IOException e;// = null; // FIXME
			{
			  //p = null;
			  //p = null;
			  //os = null;
			  //e = null;
			}
			public OutputStream(final Process p) {
				this.p = p;
				try {
					this.os = this.p.getProcess().getOutputStream();
				} catch (final java.io.IOException e) { // FIXME
					this.exception(e);
				}
			}
			protected void exception(final java.io.IOException e) { // FIXME
				this.e = e;
			}
			public java.io.IOException exception() { // FIXME
				return this.e;
			}
			
			protected OutputStream properties(final dro.util.Properties p) { // FIXME
			  //this.p = p;
				return this;
			}
			public dro.util.Properties properties() {
				return (dro.util.Properties)null;//this.p;
			}
			
			public Process returns(final Process.Return r) {
				return this.p;
			}
			public OutputStream returns(final OutputStream.Return r) {
				return this;// != null ? this : null;
			}
			
			public java.io.OutputStream getOutputStream() {
				if (null == this.os && null != this.e) throw new RuntimeException(this.e);
				return this.os;
			}
			
			public OutputStream write(final String string) throws java.io.IOException { // FIXME
				this.os.write(string.getBytes());
				return this;
			}
		}
		
		public static class InputStream {
			public static String name = Property.name;
			public static class Return extends dro.lang.Return {
				public static final Process.Return Process = (Process.Return)null;
				public static final InputStream.Return InputStream = (InputStream.Return)null;
				public static final CommandAdapter.Return CommandAdapter = (CommandAdapter.Return)null;
			}
			public static class Property {
				public static final String InputStream = CommandAdapter.getName(java.io.InputStream.class);
				public static final String name = InputStream+"#name";
			}
			
		  //protected Properties p;// = null;
			protected Process p = null;
			protected java.io.InputStream is = null;
			protected InputStream.Reader r;// = null;
			private java.io.IOException e;// = null; // FIXME
			{
			  //p = null;
			  //p = null;
			  //is = null;
			  //r = null;
			  //e = null;
			}
			private InputStream() {
			}
			public InputStream(final Process p) {
				this();
				this.p = p;
				try {
					this.is = this.p.getProcess().getInputStream();
				} catch (final java.io.IOException e) { // FIXME
					this.exception(e);
				}
			}
			protected void exception(final java.io.IOException e) { // FIXME
				this.e = e;
			}
			public java.io.IOException exception() { // FIXME
				return this.e;
			}
			
			protected InputStream properties(final dro.util.Properties p) { // FIXME
			  //this.p = p;
				return this;
			}
			public dro.util.Properties properties() {
				return (dro.util.Properties)null;//this.p;
			}
			
			public Process returns(final Process.Return r) {
				return this.p;
			}
			public InputStream returns(final InputStream.Return r) {
				return this;// != null ? this : null;
			}
			
			public java.io.InputStream getInputStream() {
				if (null == this.is && null != this.e) throw new RuntimeException(this.e);
				return this.is;
			}
			
			public java.lang.String read() throws java.io.IOException { // FIXME
				java.lang.String string = null;
				try {
					byte[] data = null;
					ByteArrayOutputStream baos = null;
					int read = 0;
					while (0 <= read) {
						if (null == data) {
							data = new byte[1024*1024]; // 1MByte
						}
						read = this.is.read(data, 0, data.length);
						if (0 < read) {
							if (null == baos) {
								baos = new ByteArrayOutputStream();
							}
							baos.write(data, 0, read);
							break; // FIXME: not good if we want to stream/read data in background then return in a big(ger) chunk
						}
					}
					if (null != baos) {
						baos.flush();
						string = baos.toString();
					}
				} catch (final IOException e) {
					throw new RuntimeException(e);
				}
				return string;
			}
			public InputStream read(final Reader.Callback handler) throws java.io.IOException { // FIXME
				if (null == this.r) {
					this.r = new InputStream.Reader(this, handler);
				} else if (null != this.r) {
					this.r.callback(handler);
				}
				return this;
			}
			
			public static class Reader implements Runnable {
				public static class Return extends CommandAdapter.Return {
					public static final InputStream.Return InputStream = (InputStream.Return)null;
				  //public static final Reader.Return Reader = (Reader.Return)null;
				}
				public static class Callback {
					public Callback() {
					}
					public void handle(final java.lang.String string) {}
				}
				
				protected InputStream is;// = null;
				protected Reader.Callback callback;// = null;
				
				{
				  //is = null;
				  //callback = null;
				}
				
				public Reader(final InputStream is) {
					this.is = is;
				}
				public Reader(final InputStream is, final Reader.Callback callback) {
					this.is = is;
					if (null != callback) {
						this.callback(callback);
					}
				}
				
				public Reader callback(final Reader.Callback callback) {
					this.callback = callback;
					if (null != this.callback) {
						Thread thread = new Thread(this);
						Runtime.getRuntime().addShutdownHook(thread);
						new java.lang.Thread(this).start();
					}
					return this;
				}
				
				private class Thread extends java.lang.Thread {
					private ErrorStream.Reader r;
					private Thread(final ErrorStream.Reader r) {
						this.r = r;
					}
					public void run() {
						this.r.shutdown();
						this.r = null;
					}
				}
				
				private boolean shutdown = false;
				protected boolean isShutdown() {
					return this.shutdown;
				}
				protected void shutdown() {
					this.shutdown = true;
				}
				
				@Override
				public void run() throws RuntimeException {
					while (false == this.shutdown) {
						try {
							java.lang.String string = this.is.read();
							if (null == string) {
								final boolean isAlive = false/*this.is
									.returns(Return.Process)
									.getProcess()
									.isAlive()*/
								;
								if (Boolean.FALSE.booleanValue() == isAlive) {
									this.shutdown = true;
									this.is.returns(Reader.Return.InputStream)
									       .returns(InputStream.Return.Process)
									       .returns(Process.Return.CommandAdapter)
									       .shutdown()
									;
									break;
								}
							} else if (null != this.callback) {
								this.callback.handle(string);
							}
							try {
								Thread.sleep(200L);
							} catch (final InterruptedException iExc) {
							  //iExc.printStackTrace(); // Silently ignore..
							}
						} catch (final java.io.IOException e) { // FIXME
							throw new RuntimeException(e);
						}
					}
				}
			}
		}
		
		public static class ErrorStream extends InputStream {
			public static String name = Property.name;
			public static class Return extends dro.lang.Return {
				public static final Process.Return Process = (Process.Return)null;
				public static final ErrorStream.Return ErrorStream = (ErrorStream.Return)null;
				public static final CommandAdapter.Return CommandAdapter = (CommandAdapter.Return)null;
			}
			public static class Property {
				public static final String ErrorStream = CommandAdapter.getName(java.io.InputStream.class); // FIXME
				public static final String name = ErrorStream+"#name";
			}
			
			public ErrorStream(final Process p) {
				super();
				super.p = p;
				try {
					super.is = super.p.getProcess().getErrorStream();
				} catch (final java.io.IOException e) { // FIXME
					this.exception(e);
				}
			}
			
			protected ErrorStream properties(final dro.util.Properties p) { // FIXME
				return (ErrorStream)super.properties(p);
			}
			
			public ErrorStream returns(final InputStream.Return r) {
				return (ErrorStream)super.returns(r);
			}
			
			public java.io.InputStream getErrorStream() {
				return super.is;
			}
			
			public java.lang.String read() throws java.io.IOException { // FIXME
				return super.read();
			}
		}
		
		protected java.lang.Process p = null;
		protected OutputStream out = null; // Connected to process input stream
		protected InputStream in = null; // Connected to process output stream
		protected ErrorStream err = null; // Connected to process error stream
		private Exception e;// = null;
		private CommandAdapter adapter;
		{
		  //p = null;
		  //e = null;
			adapter = null;
		}
		public Process(final String command) throws java.io.IOException {
			this.p = java.lang.Runtime.getRuntime().exec(command);
		}
		private Process(final java.lang.Process p) {
			this.p = p;
		}
		protected void exception(final Exception e) {
			this.e = e;
		}
		public Exception exception() {
			return this.e;
		}
		
		Process adapter(final CommandAdapter adapter) {
			this.adapter = adapter;
			return this;
		}
		public CommandAdapter returns(final CommandAdapter.Return r) {
			return this.adapter;
		}
		public Process returns(final Process.Return r) {
			return this;// != null ? this : null;
		}
		public OutputStream returns(final OutputStream.Return r) {
			if (null == this.out) {
				this.out = new OutputStream(this);
			}
			return this.out;// != null ? this : null;
		}
		public InputStream returns(final InputStream.Return r) {
			if (null == this.in) {
				this.in = new InputStream(this);
			}
			return this.in;// != null ? this : null;
		}
		public ErrorStream returns(final ErrorStream.Return r) {
			if (null == this.err) {
				this.err = new ErrorStream(this);
			}
			return this.err;// != null ? this : null;
		}
		
		public java.lang.Process getProcess() throws java.io.IOException {
		  /*if (null == this.p) {
				this.p = java.lang.Runtime.getRuntime().exec(".."); // TODO
			}*/
			return this.p;
		}
		
		public OutputStream getOutputStream() throws java.io.IOException {
			if (null == this.out) {
				this.out = new OutputStream(this);
			}
			return this.out;
		}
		public InputStream getInputStream() throws java.io.IOException {
			if (null == this.in) {
				this.in = new InputStream(this);
			}
			return this.in;
		}
		public InputStream getErrorStream() throws java.io.IOException {
			if (null == this.err) {
				this.err = new ErrorStream(this);
			}
			return this.err;
		}
		
		public int exitValue() {
		  /*final */int exitValue = -1;
			if (null != this.p) {
				try {
					exitValue = this.p.exitValue();
				} catch (final IllegalThreadStateException e1) {
					try {
						exitValue = this.p.waitFor();
					} catch (final InterruptedException e2) {
						// Silently ignore..
					}
				}
			}
			return exitValue;
		}
		
		public Process close() {
			if (null != this.p) {
			  //if (true == this.p.isAlive()) {
					this.p.destroy();
			  //}
				this.p = null;
			}
			return this;
		}
	}
	
	protected Process p;// = null;
	
	{
	  //p = null;
	}
	
	public CommandAdapter(final Properties properties) {
		super(properties);
		logger.entering(className, "(Properties)", properties);
		
		logger.exiting(className, "(Properties)");
	}
	public CommandAdapter() throws RuntimeException {
	  /*try {
			super.p = new Properties(CommandAdapter.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	}
	public CommandAdapter(final String command) throws RuntimeException {
	  /*try {
			super.p = new Properties(filename);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
		this();
	  //super.setProperty(Process.Property.name, command);
		try {
			this.p = new Process(command).adapter(this);
		} catch (final java.io.IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public CommandAdapter returns(final CommandAdapter.Return r) {
		return this;// != null ? this : null;
	}
	public Process returns(final Process.Return r) {
		return this.p;// != null ? this : null;
	}
	
	public Process exec(final java.lang.String command) throws java.io.IOException { // FIXME
		this.p = new Process(Runtime.getRuntime().exec(command)).adapter(this);
		return this.p;
	}
	public Process exec(final java.lang.String[] cmdarray) throws java.io.IOException { // FIXME
	  //Logger.getLogger(this.getClass()).log(Level.FINEST, java.lang.String.join(" ", cmdarray));
//System.err.println(java.lang.String.join(" ", cmdarray));
		this.p = new Process(Runtime.getRuntime().exec(cmdarray)).adapter(this);
		return this.p;
	}
	public Process exec(final java.lang.String command, final java.lang.String[] envp) throws java.io.IOException { // FIXME
		this.p = new Process(Runtime.getRuntime().exec(command, envp)).adapter(this);
		return this.p;
	}
	public Process exec(final java.lang.String command, final java.lang.String[] envp, final java.io.File wd) throws java.io.IOException { // FIXME
		this.p = new Process(Runtime.getRuntime().exec(command, envp, wd)).adapter(this);
		return this.p;
	}
	public Process exec(final java.lang.String[] cmdarray, final java.lang.String[] envp) throws java.io.IOException { // FIXME
		this.p = new Process(Runtime.getRuntime().exec(cmdarray, envp)).adapter(this);
		return this.p;
	}
	public Process exec(final java.lang.String[] cmdarray, final java.lang.String[] envp, final java.io.File wd) throws java.io.IOException { // FIXME
		this.p = new Process(Runtime.getRuntime().exec(cmdarray, envp, wd)).adapter(this);
		return this.p;
	}
	
	public void shutdown() throws java.io.IOException { // FIXME
		super.state = State.SHUTDOWN;
		if (null != this.p) {
		  //this.p.destroy();
			this.p = null;
		}
		try {
			super.shutdown();
		} catch (final java.lang.Exception e) { // FIXME
			throw new RuntimeException(e);
		}
	}
}