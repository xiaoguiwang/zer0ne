package dro;

import java.util.ArrayList;
import java.util.LinkedHashSet;

/*
 * [dro.]Data has a value (scalar, set, map, but *not* Data itself) as well as 0:1 meta-data, 0:n [data] rules, and 0:n [data] fields
 * [data] fields have [data field] rules..
 * A data rule does not have data as a parent (its referenced uni-directional)
 * A data field has a meta-data field as its parent
 */
//$id, $, @, %, $dro$meta$Data, $dro$data@Rule, $dro$data%Field
public /*final */class Data implements dro.data.Identity, java.lang.Cloneable {
	public static class Array {
		public static final Array Return = (Array)null;
	}
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static class Return {
			public static final Set Set = (Set)null;
		}
	}
	public static class Map {
		public static class Return {
			public static final Map Map = (Map)null;
		}
	}
	
	public static class Return {
		public static final dro.Data.Return Data = (dro.Data.Return)null;
		public static final dro.Data.Array Array = (dro.Data.Array)null;
		public static final dro.Data.List List = (dro.Data.List)null;
		public static final dro.Data.Set Set = (dro.Data.Set)null;
		public static final dro.Data.Map Map = (dro.Data.Map)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Data.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	protected /*final */dro.data.Zer0ne zero;// = null;
	
	{
	  //dro.lang.Event.entering(dro.Data.class, dro.data.Zer0ne.class, "", this);
		zero = new dro.data.Zer0ne(this);
	  //dro.lang.Event.exited(dro.Data.class, dro.data.Zer0ne.class, "", zero);
	}
	
	public Data() {
		super();
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "");
			this.__Data();
		} finally {
		  //dro.lang.Event.exiting(dro.Data.class, this, "", this);
		}
	}
	public Data(final dro.meta.Data md) {
		this();
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "", md);
			this.__Data(md);
		} finally {
		  //dro.lang.Event.exiting(dro.Data.class, this, "", this);
		}
	}
  /*protected */void __Data() {
		try {
		  //dro.lang.Event.entering(dro.Data.class, zero, "set", new java.lang.Object[]{"$id",this.hashCode()});
			this.set("$id", this.hashCode());
		  //dro.lang.Event.exited(Data.class, zero, "set");
		} finally {
		}
	}
  /*protected */void __Data(final dro.meta.Data md) {
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "__Data", md);
			if (null != md) {
				this.set(dro.meta.Data.$, md);
				final java.util.Set<dro.meta.data.Field> set = md.returns(dro.meta.data.Field.Return.Set);
				if (null != set) {
					for (final dro.meta.data.Field mdf: set) {
					  /*final dro.data.Field df = */this.add(new dro.data.Field(mdf), dro.data.Field.Return.Field);
					}
				}
				md.add(this); // Special-case: don't do this in data-field=>meta-data-field or data-rule=>meta-data-rule or data-field-rule=>meta-data-field-rule
			} else {
				throw new IllegalArgumentException();
			}
		} finally {
		  //dro.lang.Event.exiting(dro.Data.class, this, "__Data");
		}
	}
	
	@Override
	public java.lang.Object id() {
		return null == zero ? null : zero.get("$id");
	}
	
	public dro.data.Context returns(final dro.data.Context.Return r) {
		return null == zero ? null : (dro.data.Context)zero.get(dro.data.Context.$);
	}
	public dro.meta.Data returns(final dro.meta.Data.Return r) {
		return null == zero ? null : (dro.meta.Data)zero.get(dro.meta.Data.$);
	}
	public java.util.Map<java.lang.Object, dro.data.Field> returns(final dro.data.Field.Map r) {
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, dro.data.Field> map = (java.util.Map<java.lang.Object, dro.data.Field>)zero.get("$dro$data%Field");
		return map;
	}
	public java.util.Set<dro.data.Field> returns(final dro.data.Field.Set r) {
		final java.util.Set<dro.data.Field> set;// = null;
		final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
		if (null != map) {
		  //set = new LinkedHashSet<dro.data.Field>(map.values());
			set = new LinkedHashSet<dro.data.Field>();
			for (final java.lang.Object o: map.keySet()) {
				set.add(map.get(o));
			}
		} else {
			set = new LinkedHashSet<dro.data.Field>();
		}
		return set;
	}
	public java.util.List<dro.data.Field> returns(final dro.data.Field.List r) {
		final java.util.List<dro.data.Field> list;// = null;
		final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
		if (null != map) {
		  //list = new ArrayList<dro.data.Field>(map.values());
			list = new ArrayList<dro.data.Field>();
			for (final java.lang.Object o: map.keySet()) {
				list.add(map.get(o));
			}
		} else {
			list = new ArrayList<dro.data.Field>();
		}
		return list;
	}
	public dro.data.Field returns(final java.lang.Object id, final dro.data.Field.Return r) {
		final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
		return null == map ? null : (dro.data.Field)map.get(id);
	}
	
  /*public java.util.Set<dro.data.Rule> returns(final dro.data.Rule.Set r) {
		@SuppressWarnings("unchecked")
		final java.util.Set<dro.data.Rule> set = (java.util.Set<dro.data.Rule>)zero.get("$dro$data@Rule");
		return set;
	}*/
	
	public dro.Data add(final dro.data.Field df, final dro.Data.Return r) {
		this.add(df);
		return this;
	}
	public dro.data.Field add(final dro.data.Field df, final dro.data.Field.Return r) {
		this.add(df);
		return df;
	}
	public Data add(final java.lang.Object o) {
	  /*final */boolean handled = false;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "add", o);
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
			  /*} else if (true == o instanceof dro.data.Rule) {
				*//*final *//*java.util.Set<dro.data.Rule> set = this.returns(dro.data.Rule.Return.Set);
					if (null == set) {
						set = new java.util.LinkedHashSet<dro.data.Rule>();
						zero.set("$dro$data@Rule", set);
					}
					final dro.data.Rule dr = (dro.data.Rule)o;
					set.add(dr);
					handled = true;*/
				} else if (true == o instanceof dro.data.Field) {
				  /*final */java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
					if (null == map) {
						map = new java.util.LinkedHashMap<java.lang.Object, dro.data.Field>();
						zero.set("$dro$data%Field", map);
					}
					final dro.data.Field df = (dro.data.Field)o;
					if (false == map.containsKey(df.id())) {
						map.put(df.id(), df); // Always first! Because df.set will check data $dro$data%Field map
						df.set(dro.Data.$, this);
					}
					handled = true;
				} else if (true == o instanceof java.lang.Object) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == zero.has("$")) {
						final java.util.List<java.lang.Object> list = new ArrayList<>();
						list.add(zero.get("$"));
						zero.remove("$");
						zero.set("@", list);
					} else if (true == zero.has("@")) {
						@SuppressWarnings("unchecked")
						final java.util.List<java.lang.Object> list = (java.util.List<java.lang.Object>)zero.get("@");
						list.add(o);
					} else if (true == zero.has("%")) {
						if (true == o instanceof dro.data.Identity) {
							@SuppressWarnings("unchecked")
							final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
							final java.lang.Object k = ((dro.data.Identity)o).id();
							final java.lang.Object v = o;
							map.put(k, v);
						} else {
							throw new IllegalArgumentException();
						}
					} else {
						zero.set("$", o);
					}
					handled = true;
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "add");
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "add", this);
		}
		return this;
	}
	
	public Data set(final java.lang.Object o) {
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "set", o);
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.entering((dro.data.Context)null, this, "set");
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof java.lang.Object[]) {
					final java.util.List<java.lang.Object> list = new ArrayList<>();
					final java.lang.Object[] oa = (java.lang.Object[])o;
					for (final java.lang.Object __o: oa) {
						list.add(__o);
					}
					zero.set("@", list);
				} else if (true == o instanceof java.util.Set<?>) {
					final java.util.List<java.lang.Object> list = new ArrayList<>();
					final java.util.Set<?> set = (java.util.Set<?>)o;
					for (final java.lang.Object __o: set) {
						list.add(__o);
					}
					zero.set("@", list);
				} else if (true == o instanceof java.util.List<?>) {
					zero.set("@", o);
				} else if (true == o instanceof java.util.Map<?, ?>) {
				  //final java.util.Map<java.lang.Object, java.lang.Object> map = new LinkedHashMap<>();
					final java.util.Map<?, ?> map = (java.util.Map<?, ?>)o;
				  /*for (final java.lang.Object k: map.keySet()) {
						final java.lang.Object v = map.get(k);
						map.put(k, v);
					}*/
					zero.set("%", map);
				} else if (true == o instanceof java.lang.Object) {
				  /*dro.data.Event e = dro.data.Event
						.context((dro.data.Context)null)
						.call(this.getClass(), this, "value", o)
						.returns(this)
					;*/
					zero.set("$", o);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "set");
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "set", this);
		}
		return this;
	}
	public Data set(final java.lang.Object k, final java.lang.Object v) {
	  /*final */boolean handled = false;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "set", new java.lang.Object[]{k, v});
			
			if (true == k instanceof java.lang.String && true == ((java.lang.String)k).startsWith("$")) {
				if (true == v instanceof dro.data.Identity) {
					this.set(((java.lang.String)k)+"#id", ((dro.data.Identity)v).id());
				}
				zero.set(k, v);
			} else if (false == zero.has("$dro$Data#id!proxy")) {
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == this.getClass()) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == k instanceof dro.data.Field) {
						final dro.data.Field df = (dro.data.Field)k;
						if (null == this.returns(df.id(), dro.data.Field.Return.Field)) {
							this.add(df, dro.data.Field.Return.Field);
						}
						df.set(v);
						handled = true;
					} else if (true == k instanceof java.lang.String) {
						final java.util.Set<dro.data.Field> set = this.returns(dro.data.Field.Set.Return.Set);
						for (final dro.data.Field df: set) {
							if (true == ((String)k).equals(df.get("$name"))) {
								final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
								final java.lang.String type = (String)mdf.get("$type");
								if (false == Boolean.TRUE.booleanValue()) {
								} else if (true == type.equals("boolean")) {
									df.set("$value", new java.lang.Boolean(v.toString()));
									break;
								} else if (true == type.equals("number")) {
									df.set("$value", new java.lang.Integer(v.toString()));
									break;
								} else if (true == type.equals("text")) {
									df.set("$value", v/*.toString()*/);
									break;
								} else {
									throw new IllegalArgumentException();
								}
							}
						}
					} else if (true == k instanceof java.lang.Object) {
						final dro.data.Field df = this.returns(k, dro.data.Field.Return.Field);
						if (null != df) {
							df.set(v);
						} else {
							@SuppressWarnings("unchecked")
						  /*final */java.util.Map<java.lang.Object, java.lang.Object> __map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
							if (null == __map) {
								__map = new java.util.LinkedHashMap<>();
								zero.set("%", __map);
							}
							__map.put(k, v);
						}
						handled = true;
					} else {
						throw new IllegalArgumentException();
					}
				} else {
					throw new UnsupportedOperationException();
				}
			} else { // else if (true == zero.has("$dro$Data#id!proxy")) {
				final dro.data.Context dc = (dro.data.Context)zero.get(dro.data.Context.$);
				final java.lang.Object _id = zero.get("$dro$Data#id!proxy");
				if (false == zero.has("$dro$data$Context!proxy")) {
					final dro.Data _d = dc.returns(_id, dro.Data.Return.Data);
					_d.set(k, v);
				} else {
					final dro.data.Context _dc = (dro.data.Context)zero.get("$dro$data$Context!proxy");
					final dro.data.Event e = new dro.data.Event()
						.set(dro.data.Context.$, dc)
						.set("$dro$data$Context!proxy", _dc)
						.set(dro.Data.$, this)
						.set("$dro$Data#id!proxy", _id)
						.set("$java$lang$String#action", "set")
						.set("$java$lang$Object!k", k)
						.set("$java$lang$Object!v", v)
					;
					new dro.lang.Event(dro.Data.class, this, "set")
						.event(e)
						.dispatch(); // Synchronously!..
				  //o = e.get("$java$lang$Object#returns");
				}
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "set");
			} else {
			}
		  //dro.lang.Event.exiting(dro.Data.class, this, "set", this);
		}
		return this;
	}
	
	public java.lang.Object[] keys(final dro.Data.Array r) {
	  /*final */java.lang.Object[] oa = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "keys", r);
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
				final java.util.List<?> list = this.keys(dro.Data.Return.List);
				oa = list.toArray();
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
		  //dro.lang.Event.exiting(dro.Data.class, this, "keys", oa);
		}
		return oa;
	}
	public java.util.Set<?> keys(final dro.Data.Set r) {
	  /*final */java.util.Set<?> set = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "keys", r);
			if (false == Boolean.TRUE.booleanValue()) {
			} else { //if (dro.Data.class == this.getClass()) {
				final java.util.Map<?, ?> map = (java.util.Map<?, ?>)zero.get("%");
				if (null != map) {
					set = map.keySet();
				} else {
					set = new LinkedHashSet<>();
				}
		  //} else {
			  //throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
		  //dro.lang.Event.exiting(dro.Data.class, this, "keys", set);
		}
		return set;
	}
	public java.util.List<?> keys(final dro.Data.List r) {
	  /*final */java.util.List<?> list = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "keys", r);
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
				list = new ArrayList<java.lang.Object>(this.keys(dro.Data.Return.Set));
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
		  //dro.lang.Event.exiting(dro.Data.class, this, "keys", list);
		}
		return list;
	}
	
	public boolean has(final java.lang.Object o) {
	  /*final */boolean contains = false;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "has", o);
			
			if (true == o instanceof java.lang.String && true == ((java.lang.String)o).equals("$")) { // For method has, this is a special-case
				contains = zero.has(o);
			} else if (false == zero.has("$dro$Data#id!proxy")) {
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == this.getClass()) {
				  //dro.data.Event.entered((dro.data.Context)null, this, "contains", new java.lang.Object[]{o});
					if (false == Boolean.TRUE.booleanValue()) {
				  /*} else if (true == o instanceof dro.data.Rule) {
						final java.util.Set<dro.data.Rule> set = this.returns(dro.data.Rule.Return.Set);
						if (null != set) {
							final dro.data.Rule dr = (dro.data.Rule)o;
							contains = set.contains(dr);
						}*/
					} else if (true == o instanceof dro.data.Field) {
						final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
						if (null != map) {
							final dro.data.Field df = (dro.data.Field)o;
							contains = map.containsKey(df.id()) || map.containsValue(df);
						}
					} else if (true == o instanceof java.lang.Object) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (true == zero.has("$")) {
							contains = o == zero.get("$");
						} else if (true == o instanceof java.lang.String && true == ((java.lang.String)o).startsWith("$")) {
							contains = zero.has(o);
						} else if (true == zero.has("@")) {
							@SuppressWarnings("unchecked")
							final java.util.List<java.lang.Object> list = (java.util.List<java.lang.Object>)zero.get("@");
							if (null != list) {
								contains = list.contains(o);
							}
						} else if (true == zero.has("%")) {
							@SuppressWarnings("unchecked")
							final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
							if (null != map) {
								if (true == o instanceof dro.data.Identity) {
									final java.lang.Object k = ((dro.data.Identity)o).id();
									final java.lang.Object v = o;
									contains = map.containsKey(k) || map.containsValue(v);
								} else {
									contains = map.containsKey(o);// || map.containsValue(o);
								}
							} else {
							  //throw new IllegalArgumentException();
							}
						} else {
						  //throw IllegalStateException();
						}
					} else {
						throw new IllegalArgumentException();
					}
				} else {
					throw new UnsupportedOperationException();
				}
			} else { // else if (true == zero.has("$dro$Data#id!proxy")) {
				final dro.data.Context dc = (dro.data.Context)zero.get(dro.data.Context.$);
				final java.lang.Object _id = zero.get("$dro$Data#id!proxy");
				if (false == zero.has("$dro$data$Context!proxy")) {
					final dro.Data _d = dc.returns(_id, dro.Data.Return.Data);
					contains = _d.has(o);
				} else {
					final dro.data.Context _dc = (dro.data.Context)zero.get("$dro$data$Context!proxy");
					final dro.data.Event e = new dro.data.Event()
						.set(dro.data.Context.$, dc)
						.set("$dro$data$Context!proxy", _dc)
						.set(dro.Data.$, this)
						.set("$dro$Data#id!proxy", _id)
						.set("$java$lang$String#action", "has")
						.set("$java$lang$Object!o", o)
					;
					new dro.lang.Event(dro.Data.class, this, "has")
						.event(e)
						.dispatch(); // Synchronously!..
					final java.lang.Boolean _contains = (Boolean)e.get("$boolean#returns");
					contains = null == _contains ? Boolean.FALSE : (Boolean)_contains; // FIXME! (Boolean)null==>"false"!?
				}
			}
		} finally {
			if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "contains");
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "has", contains);
		}
		return contains;
	}
	
	public dro.data.Field remove(final dro.data.Field df, final dro.data.Field.Return r) {
	  /*final dro.data.Field df = (dro.data.Field)*/this.remove(df);
		return df;
	}
	public dro.data.Field remove(final java.lang.Object id, final dro.data.Field.Return r) {
		final dro.data.Field df = (dro.data.Field)this.remove(id);
		return df;
	}
  //@Override
	public java.lang.Object remove(final java.lang.Object o) {
	  /*final */java.lang.Object remove = null;
	  /*final */boolean handled = false;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "remove", o);
			if (false == handled && dro.Data.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "remove", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.meta.Data) {
					zero.remove(dro.meta.Data.$id);
					zero.remove(dro.meta.Data.$);
					final java.util.Set<dro.data.Field> set = this.returns(dro.data.Field.Set.Return.Set);
					for (final dro.data.Field df: set) {
						this.remove(df);
					}
					final dro.meta.Data md = (dro.meta.Data)o;
					md.remove(this);
					handled = true;
				} else if (true == o instanceof dro.data.Context) {
					zero.remove(dro.data.Context.$id);
					zero.remove(dro.data.Context.$);
					final dro.data.Context dc = (dro.data.Context)o;
					dc.remove(this);
					handled = true;
			  /*} else if (true == o instanceof dro.data.Rule) {
					final java.util.Set<dro.data.Rule> set = this.returns(dro.data.Rule.Return.Set);
					if (null != set) {
						final dro.data.Rule dr = (dro.data.Rule)o;
						set.remove(dr);
						remove = o;
						if (0 == set.size()) {
							zero.remove("$dro$data@Rule");
						}
					}
					handled = true;*/
				} else if (true == o instanceof dro.data.Field) {
					final dro.data.Field df = (dro.data.Field)o;
					final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
					if (null != map) {
						if (true == map.containsKey(df.id())) {
							remove = map.remove(df.id());
							if (0 == map.size()) {
								zero.remove("$dro$data%Field");
							}
							df.remove(this);
						}
					}
					handled = true;
				} else if (java.lang.Integer.class == o.getClass()) {
					final java.lang.Integer id = (java.lang.Integer)o;
					{
						final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Map.Return.Map);
						if (null != map) {
							if (true == map.containsKey(id)) {
								final dro.data.Field df = (dro.data.Field)map.remove(id);
								df.remove(this);
							}
						}
					}
					handled = true;
				}
			}
			if (false == handled && true == this instanceof dro.Data) {
				if (true == o instanceof java.lang.String && true == ((String)o).startsWith("$")) {
					zero.remove((String)o);
					handled = true;
				}
			}
			if (false == handled) {
				throw new IllegalArgumentException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "remove");
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "remove", remove);
		}
		return remove;
	}
	
	public java.lang.Object[] values(final dro.Data.Array r) {
	  /*final */java.lang.Object[] oa = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "values", r);
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
				final java.util.List<?> list = this.values(dro.Data.Return.List);
				oa = list.toArray();
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "values", oa);
		}
		return oa;
	}
	
	// $id, $, @, %, $dro$meta$Data, $dro$data@Rule, $dro$data%Field
	public java.util.Set<?> values(final dro.Data.Set r) {
	  /*final */java.util.Set<?> set = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "values", r);
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == zero.has("$")) {
					throw new IllegalStateException();
				} else if (true == zero.has("@")) {
					@SuppressWarnings("unchecked")
					final java.util.List<java.lang.Object> list = (java.util.List<java.lang.Object>)zero.get("@");
					set = new LinkedHashSet<java.lang.Object>(list);
				} else if (true == zero.has("%")) {
					@SuppressWarnings("unchecked")
					final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
					set = new LinkedHashSet<java.lang.Object>(map.values());
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "values", set);
		}
		return set;
	}
	// $id, $, @, %, $dro$meta$Data, $dro$data@Rule, $dro$data%Field
	public java.util.List<?> values(final dro.Data.List r) {
	  /*final */java.util.List<?> list = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "values", r);
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
				list = new ArrayList<java.lang.Object>(this.values(dro.Data.Return.Set));
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "values", list);
		}
		return list;
	}
	
	// dro.[meta.]data.field.Rule.class
	// dro.[meta.]data.Field.class
	// dro.[meta.]data.Rule.class
	// dro.[meta.]data.Context.class
	// dro.[meta.]Data.class
	// $id, $, @, %, $dro$meta$Data, $dro$data@Rule, $dro$data%Field
	public java.lang.Object get(final java.lang.Object k) {
	  /*final */java.lang.Object v = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "get", k);
			
			if (true == k instanceof java.lang.String && true == ((java.lang.String)k).startsWith("$")) {
				v = zero.get(k);
			} else if (false == zero.has("$dro$Data#id!proxy")) {
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == this.getClass()) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == k instanceof java.lang.String && true == ((java.lang.String)k).startsWith("$")) {
						v = zero.get(k);
					} else if (true == k instanceof dro.data.Field) {
					  /*final *//*java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
						if (null != map) {
						  */final dro.data.Field df = (dro.data.Field)k;/*
						  //map.get(df.id());
						  */v = df.get("$");/*
						}*/
					} else if (true == k instanceof java.lang.Object) {
						final java.util.Map<java.lang.Object, dro.data.Field> map = this.returns(dro.data.Field.Return.Map);
					  /*final */dro.data.Field df = null;
						if (null != map) {
						  //if (true == map.containsKey(k)) {
								df = map.get(k);
						  //}
						}
						if (null != df) {
						  //if (true == df.has(k)) {
								v = df.get(k);
						  //}
						} else {
							@SuppressWarnings("unchecked")
						  /*final */java.util.Map<java.lang.Object, java.lang.Object> __map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
							if (null != __map) {
								if (true == __map.containsKey(k)) {
									v = __map.get(k);
								}
							} else {
								v = zero.get(k);
							}
						}
					} else {
						throw new IllegalArgumentException();
					}
				} else if (true == this instanceof dro.Data) {
					if (true == k instanceof java.lang.String && true == ((java.lang.String)k).startsWith("$")) {
						v = zero.get(k);
					} else {
						@SuppressWarnings("unchecked")
						final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
						if (null != map) {
							v = map.get(k);
						}
					}
				} else {
					throw new UnsupportedOperationException();
				}
			} else { // else if (true == zero.has("$dro$Data#id!proxy")) {
				final dro.data.Context dc = (dro.data.Context)zero.get(dro.data.Context.$);
				final java.lang.Object _id = zero.get("$dro$Data#id!proxy");
				if (false == zero.has("$dro$data$Context!proxy")) {
					final dro.Data _d = dc.returns(_id, dro.Data.Return.Data);
					v = _d.get(k);
				} else {
					final dro.data.Context _dc = (dro.data.Context)zero.get("$dro$data$Context!proxy");
					final dro.data.Event e = new dro.data.Event()
						.set(dro.data.Context.$, dc)
						.set("$dro$data$Context!proxy", _dc)
						.set(dro.Data.$, this)
						.set("$dro$Data#id!proxy", _id)
						.set("$java$lang$String#action", "get")
						.set("$java$lang$Object!k", k)
					;
					new dro.lang.Event(dro.Data.class, this, "get")
						.event(e)
						.dispatch(); // Synchronously!..
					v = e.get("$java$lang$Object!v");
				}
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "get");
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "get", v);
		}
		return v;
	}
	// $id, $, @, %, $dro$meta$Data, $dro$data@Rule, $dro$data%Field
	public java.lang.Object get() {
	  /*final */java.lang.Object o = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "get");
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			  /*dro.data.Event e = dro.data.Event
					.context((dro.data.Context)null)
					.call(this.getClass(), this, "value")
					.returns(this.o)
				;*/
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == zero.has("$")) {
					o = zero.get("$");
				} else if (true == zero.has("@")) {
					o = zero.get("@");
				} else if (true == zero.has("%")) {
					o = zero.get("%");
				} else {
				  //throw new IllegalStateException(); // Silently return {null}
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.Data.class == this.getClass()) {
			} else {
			}
			
		  //dro.lang.Event.exiting(dro.Data.class, this, "get");
		}
		return o;
	}
	
	@Override
	public boolean equals(final java.lang.Object o) {
		boolean equals = false;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "equals", o);
			
			if (this == o) { // Note: there's no point comparing if any hashCode (e.g. zero.get("$id")) is going to make equals false..
				equals = true;
			} else if (true == o instanceof dro.Data) {
				equals = true;
				final dro.Data d = (dro.Data)o;
				// Check for $, @, %, $dro$meta$Data, $dro$data@Rule, $dro$data%Field
				if (true == equals) {
					final dro.meta.Data md1 = this.returns(dro.meta.Data.Return.Data);
					final dro.meta.Data md2 = d.returns(dro.meta.Data.Return.Data);
					equals = dro.data.Tool.equals((dro.Data)md1, (dro.Data)md2);
				}
			  /*if (true == equals) {
					final java.util.Set<?> set1 = this.returns(dro.data.Rule.Return.Set);
					final java.util.Set<?> set2 = d.returns(dro.data.Rule.Return.Set);
					equals = dro.data.Tool.equals(set1, set2);
				}*/
				if (true == equals) {
					final java.util.Map<?, ?> map1 = this.returns(dro.data.Field.Return.Map);
					final java.util.Map<?, ?> map2 = d.returns(dro.data.Field.Return.Map);
					equals = dro.data.Tool.equals(map1, map2);
				}
				if (true == equals) {
					final java.lang.Object o1 = zero.get("$");
					final java.lang.Object o2 = d.zero.get("$"); // FIXME - this should be d.get("$")
					equals = dro.data.Tool.equals(o1, o2);
				}
				if (true == equals) {
					// Hmm.. how to break recursion?! especially if we have child to parent to child relationships..
					final java.util.List<?> list1 = (java.util.List<?>)zero.get("@");
					final java.util.List<?> list2 = (java.util.List<?>)d.zero.get("@"); // FIXME - this should be d.get("@")
					equals = dro.data.Tool.equals(list1, list2);
				}
				if (true == equals) {
					// Hmm.. how to break recursion?! especially if we have child to parent to child relationships..
					final java.util.Map<?, ?> map1 = (java.util.Map<?, ?>)zero.get("%");
					final java.util.Map<?, ?> map2 = (java.util.Map<?, ?>)d.zero.get("%"); // FIXME - this should be d.get("%")
					equals = dro.data.Tool.equals(map1, map2);
				}
			}
		} finally {
		  //dro.lang.Event.exiting(dro.Data.class, this, "equals", equals);
		}
		return equals;
	}
	
	@Override
	public dro.Data clone() {
	  /*final */dro.Data d = null;
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "clone");
			
			final dro.meta.Data md = this.returns(dro.meta.Data.Return.Data);
			if (null == md) {
				d = new dro.Data();
			  //final java.util.Set<dro.data.Rule> set = this.returns(dro.data.Rule.Return.Set);
			  /*for (final dro.data.Rule dr: set) {
					d.add(dr.clone());
				}*/
			} else {
				d = new dro.Data(md);
			}
			{
				final java.util.Set<dro.data.Field> set = this.returns(dro.data.Field.Return.Set);
				if (null != set) {
					for (final dro.data.Field df: set) {
						df.add(df.clone());
					}
				}
			}
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == zero.has("$")) {
				d.add(zero.get("$"));
			} else if (true == zero.has("@")) {
				@SuppressWarnings("unchecked")
				final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)zero.get("@");
				if (null != set) {
					for (final java.lang.Object o: set) {
						d.add(o);
					}
				}
			} else if (true == zero.has("%")) {
				@SuppressWarnings("unchecked")
				final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)zero.get("%");
				if (null != map) {
					for (final java.lang.Object k: map.keySet()) {
						final java.lang.Object v = map.get(k);
						d.set(k, v);
					}
				}
			} else {
			}
		} finally {
		  //dro.lang.Event.exiting(dro.Data.class, this, "clone", d);
		}
		return d;
	}
}