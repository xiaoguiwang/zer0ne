package dro;

import java.util.ArrayList;
import java.util.HashMap;

import dro.lang.Sequence;

/**
 * Thinkings, not specifically of Serializer but of the walker model using interfaces/callback handlers..
 * Pass in objects (or primitives which will be wrapped to objects and vice-versa to primitives as these are discrete and fixed)
 * This could be for serialising to string (for DTO) or serialising to jdbc calls (for DAO)
 * If we hit an object we could use reflection for member types and/or if we find it's not a primitive we expect implementation of an interface (e.g. dro.lang.Object)
 * which we can call to pass-back key classes and value classes, on a per member basis
 * As we go along we track what we have already serialised so that we can easily break recursion
 * We also need to be able to build a reference matrix (of n:m, not 1:n or m:1)
 * @author alex.russell
 *
 */

public class Serializer {
	protected Sequence seq = null;
	
	protected java.util.Map<java.lang.Integer, java.lang.Object> mapIdToRef = new HashMap<>();
  //public static java.util.Map<java.lang.Object, java.lang.Integer> mapRefToId = new HashMap<>();
	
	{
	  //this.seq = new Sequence();
	}
	
	public Serializer() {
		this.seq = new Sequence();
	}
	
	protected boolean exists(final java.lang.Object o) {
	  /*final */boolean exists = false;
		for (final java.lang.Integer j: this.mapIdToRef.keySet()) {
			final java.lang.Object k = this.mapIdToRef.get(j);
			if (k == o) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, /*final */dro.lang.Object by, final java.lang.Class<?> c, final java.lang.Object o) {
		if (null == list) list = new ArrayList<dro.lang.Object>();
		final dro.lang.Object to = new dro.lang.Object(c, o);
		if (null != by) {
			by.reference(to);
		}
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (o instanceof java.util.Map) {
			to.type(java.util.Map.class);
			final boolean exists = this.exists(o);
			if (false == exists) {
				this.mapIdToRef.put(this.seq.next(), o);
				list.add(to);
				final java.util.Map<?,?> __map = (java.util.Map<?,?>)o;
				for (final java.lang.Object __key: __map.keySet()) {
					final dro.lang.Object z = new dro.lang.Object();
					to.reference(z);
				  //list.add(z);
					list = this.serialise(list, z, __key.getClass(), __key);
					final java.lang.Object __val = __map.get(__key);
					list = this.serialise(list, z, __val.getClass(), __val);
				}
			}
		} else if (o instanceof java.util.Set) {
			to.type(java.util.Set.class);
			final boolean exists = this.exists(o);
			if (false == exists) {
				this.mapIdToRef.put(this.seq.next(), o);
				list.add(to);
				final java.util.Set<?> __set = (java.util.Set<?>)o;
				for (final java.lang.Object x: __set) {
					list = this.serialise(list, to, x.getClass(), x);
				}
			}
		} else if (o instanceof java.util.List) {
			to.type(java.util.List.class);
			final boolean exists = this.exists(o);
			if (false == exists) {
				this.mapIdToRef.put(this.seq.next(), o);
				list.add(to);
				final java.util.List<?> __list = (java.util.List<?>)o;
				for (final java.lang.Object x: __list) {
					list = this.serialise(list, to, x.getClass(), x);
				}
			}
		} else if (o instanceof java.lang.Object[]) {
			to.type(java.lang.Object[].class);
			final boolean exists = this.exists(o);
			if (false == exists) {
				this.mapIdToRef.put(this.seq.next(), o);
				list.add(to);
				final java.lang.Object[] arr = (java.lang.Object[])o;
				for (final java.lang.Object x: arr) {
					list = this.serialise(list, to, x.getClass(), x);
				}
			}
		} else if (o instanceof dro.lang.Object) {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (to instanceof java.lang.Object) {
			} else {
				throw new IllegalArgumentException();
			}
			to.type(o.getClass());
			final boolean exists = this.exists(o);
			if (false == exists) {
				this.mapIdToRef.put(this.seq.next(), o);
			  //list.add(to);
			}
		} else if (o instanceof java.lang.Object) {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (o instanceof java.lang.Boolean) {
				to.type(java.lang.Boolean.class);
			} else if (o instanceof java.lang.Byte) {
				to.type(java.lang.Byte.class);
			} else if (o instanceof java.lang.Short) {
				to.type(java.lang.Short.class);
			} else if (o instanceof java.lang.Integer) {
				to.type(java.lang.Integer.class);
			} else if (o instanceof java.lang.Long) {
				to.type(java.lang.Long.class);
			} else if (o instanceof java.lang.Float) {
				to.type(java.lang.Float.class);
			} else if (o instanceof java.lang.Double) {
				to.type(java.lang.Double.class);
			} else if (o instanceof java.math.BigInteger) {
				to.type(java.math.BigInteger.class);
			} else if (o instanceof java.math.BigDecimal) {
				to.type(java.math.BigDecimal.class);
			} else if (o instanceof java.lang.String) {
				to.type(java.lang.String.class);
			} else {
				throw new IllegalArgumentException();
			}
			final boolean exists = this.exists(o);
			if (false == exists) {
			  //this.mapIdToRef.put(this.seq.next(), o);
			}
			if (false == exists) {
			  //list.add(to);
			}
		} else if (o == null) {
			to.type(java.lang.Object.class);
			final boolean exists = this.exists(o);
			if (false == exists) {
			  //this.mapIdToRef.put(this.seq.next(), o);
			}
			if (false == exists) {
			  //list.add(to);
			}
	  /*} else {
			throw new IllegalArgumentException();
	  */}
		return list;
	}
	
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final java.lang.Class<?> c, final java.lang.Object o) {
		return this.serialise(list, (dro.lang.Object)null, c, o);
	}
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final java.lang.Object o) {
		return this.serialise(list, (dro.lang.Object)null, o.getClass(), o);
	}
	
	public java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final boolean to) {
		return this.serialise(list, by, boolean.class, new java.lang.Boolean(to));
	}
	public java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final boolean to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final boolean o) {
		return this.serialise(null, null, o);
	}
	
	public java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final byte to) {
		return this.serialise(list, by, byte.class, new java.lang.Byte(to));
	}
	public java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final byte to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final byte o) {
		return this.serialise(null, null, o);
	}
	
	public java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final short to) {
		return this.serialise(list, by, short.class, new java.lang.Short(to));
	}
	public java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final short to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final short o) {
		return this.serialise(null, null, o);
	}
	
	public java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final int to) {
		return this.serialise(list, by, int.class, new java.lang.Integer(to));
	}
	public java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final int to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final int o) {
		return this.serialise(null, null, o);
	}
	
	public java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final long to) {
		return this.serialise(list, by, long.class, new java.lang.Long(to));
	}
	public java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final long to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final long o) {
		return this.serialise(null, null, o);
	}
	
	public java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final float to) {
		return this.serialise(list, by, float.class, new java.lang.Float(to));
	}
	public java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final float to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final float o) {
		return this.serialise(null, null, o);
	}
	
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final double to) {
		return this.serialise(list, by, double.class, new java.lang.Double(to));
	}
	protected java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final double to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final double o) {
		return this.serialise(null, null, o);
	}
	
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final java.math.BigInteger to) {
		return this.serialise(list, by, java.math.BigInteger.class, to);
	}
	protected java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final java.math.BigInteger to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final java.math.BigInteger o) {
		return this.serialise((java.util.List<dro.lang.Object>)null, (dro.lang.Object)null, o.getClass(), (java.lang.Object)o);
	}
	
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final java.math.BigDecimal to) {
		return this.serialise(list, by, java.math.BigDecimal.class, to);
	}
	protected java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final java.math.BigDecimal to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final java.math.BigDecimal o) {
		return this.serialise((java.util.List<dro.lang.Object>)null, (dro.lang.Object)null, o.getClass(), (java.lang.Object)o);
	}
	
	protected java.util.List<dro.lang.Object> serialise(/*final */java.util.List<dro.lang.Object> list, final dro.lang.Object by, final java.lang.String to) {
		return this.serialise(list, by, java.lang.String.class, to);
	}
	protected java.util.List<dro.lang.Object> serialise(final dro.lang.Object by, final java.lang.String to) {
		return this.serialise(null, by, to);
	}
	public java.util.List<dro.lang.Object> serialise(final java.lang.String o) {
		return this.serialise((java.util.List<dro.lang.Object>)null, (dro.lang.Object)null, o);
	}
	
	public java.util.List<dro.lang.Object> serialise(final java.lang.Object o) {
		return this.serialise((java.util.List<dro.lang.Object>)null, o);
	}
}