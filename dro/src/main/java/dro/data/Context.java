package dro.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dro.util.Properties;

public class Context extends dro.Data {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static class Return {
			public static final Set Set = (Set)null;
		}
	}
	public static class Map {
		public static class Return {
			public static final Map Map = (Map)null;
		}
	}
	
	public static class Return {
		public static final dro.data.Context.Set.Return Set = (dro.data.Context.Set.Return)null;
		public static final dro.data.Context.Map.Return Map = (dro.data.Context.Map.Return)null;
		public static final dro.Data.Return Data = (dro.Data.Return)null;
		public static class Data {
			public static final dro.data.Context.Return Context = (dro.data.Context.Return)null;
		}
		public static class Meta {
			public static final dro.meta.Data.Return Data = (dro.meta.Data.Return)null;
		}
		public static final dro.data.Context.Return Context = (dro.data.Context.Return)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Context.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	protected dro.util.Properties p;// = null;
	protected dro.lang.Event.Listener el;// = null;
	protected dro.Data mdl;// = null;
	protected dro.Data dl;// = null;
	
	{
		el = null;
	/**/mdl = new dro.Data();
	/**/dl = new dro.Data();
	}
	
	public Context(final dro.meta.data.Context mdc) {
		super(); // implicit super constructor..
	  /*if (null == mdc) {
			throw new IllegalStateException();
		}*/
		this.__Context(mdc);
	}
	public Context() {
		this((dro.meta.data.Context)null);
	}
	protected void __Context(final dro.meta.data.Context mdc) {
		if (null != mdc) {
			this.set(dro.meta.data.Context.$, mdc);
		}
		try {
			this.properties(new Properties(this));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		if (null == Properties.properties()) {
			Properties.properties(this.properties());
		}
	  /*final dro.data.Context __this = this;
		this.el = new dro.lang.Event.Listener(){
			public dro.lang.Event.Listener dispatch(final dro.lang.Event e) {
				__this.invoke(e.c(), e.o(), e.methodName(), (java.lang.Object[])null); // FIXME
				return this;
			}
		};*/
	/*	dro.lang.Event.register(this.el, new java.lang.Class<?>[]{dro.Data.class}, (dro.Data[])null, new java.lang.String[]{"set"});*/
	/*//dro.lang.Event.deregister(this.el, new java.lang.Class<?>[]{dro.Data.class}, (dro.Data[])null, new java.lang.String[]{"set"});*/
	  /*dro.lang.Event.register(new dro.lang.Event.Listener(){
			@Override
			public Listener dispatch(final dro.lang.Event e) {
				final dro.data.Event _e = e.event();
				final dro.data.Context dc = (dro.data.Context)_e.get("$dro$data$Context");
				final dro.data.Context _dc = (dro.data.Context)_e.get("$dro$data$Context!proxy");
				final dro.Data d = (dro.Data)_e.get("$dro$Data");
				final java.lang.Object _id = _e.get("$dro$Data#id!proxy");
				final java.lang.String action = (java.lang.String)_e.get("$action");
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == action.equals("get")) {
					final java.lang.Object k = _e.get("$java$lang$Object!k");
					final dro.Data _d = _dc.returns(_id, dro.Data.Return.Data);
					_e.set("$java$lang$Object!v", _d.get(k));
				} else if (true == action.equals("set")) {
					final java.lang.Object k = _e.get("$java$lang$Object!k");
					final java.lang.Object v = _e.get("$java$lang$Object!v");
					final dro.Data _d = _dc.returns(_id, dro.Data.Return.Data);
					_e.set("$java$lang$Object#returns", _d.set(k, v));
				} else if (true == action.equals("has")) {
					final java.lang.Object o = _e.get("$java$lang$Object!o");
					final dro.Data _d = _dc.returns(_id, dro.Data.Return.Data);
					_e.set("$boolean#returns", _d.has(o)); // TODO: switch from boolean to Boolean to support null
				} else {
					throw new UnsupportedOperationException();
				}
				return this;
			}
		}, new java.lang.Class<?>[]{dro.Data.class}, (java.lang.Object[])null, new java.lang.String[]{"get","set","has"});*/
	}
	
	public Context properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
  /*public Context invoke(final dro.lang.Event e) {
		return this;
	}*/
  /*public java.lang.Object invoke(final dro.meta.Data md, final dro.Data d, final java.lang.String action) {
		return (java.lang.Object)null;
	}*/
  /*public java.lang.Object invoke(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
		// Default implementation (local anonymous context)
		return (java.lang.Object)null;
	}*/
	
	public dro.meta.data.Context returns(final dro.meta.data.Context.Return r) {
		return null == zero ? null : (dro.meta.data.Context)zero.get(dro.meta.data.Context.$);
	}
	public java.util.Map<java.lang.Object, dro.Data> returns(final dro.Data.Map r) {
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, dro.Data> map = (java.util.Map<java.lang.Object, dro.Data>)zero.get("$dro%Data");
		return map;
	}
	public java.util.Set<dro.Data> returns(final dro.Data.Set r) {
		final java.util.Set<dro.Data> set;// = null;
		final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Return.Map);
		if (null != map) {
			set = new LinkedHashSet<dro.Data>(map.values());
		} else {
			set = new LinkedHashSet<dro.Data>();
		}
		return set;
	}
	public dro.Data returns(final java.lang.Object id, final dro.Data.Return r) {
		final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Return.Map);
		return null == map ? null : (dro.Data)map.get(id);
	}
	
	public dro.Data add(final dro.Data d, final dro.data.Context.Return r) {
		this.add(d);
		return this;
	}
	public dro.Data add(final dro.Data d, final dro.Data.Return r) {
		this.add(d);
		return d;
	}
	
	// final java.lang.String regex = "(.*?)(\\$\\{)(.*?)(:)(.*?)(\\})(.*)";
	public java.util.Set<dro.Data> returns(final java.lang.Object id, final java.lang.String op, final java.lang.String regex, final dro.Data.Set r) {
	  /*final */java.util.Set<dro.Data> set = null;
		final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Return.Map);
		if (null != map) {
		  /*final */java.lang.String _regex = null;
			for (final java.lang.Object _id: map.keySet()) {
				final dro.Data d = (dro.Data)map.get(_id);
				if (true == d.has(id)) {
					if (true == op.equals("EQ")) {
						if (null == _regex) {
							_regex = regex.replaceAll("\\*", ".*");
						}
						final Pattern pattern = Pattern.compile(_regex, Pattern.DOTALL);
						final java.lang.Object o = d.get(id);
						if (null != o) {
							final Matcher m = pattern.matcher(d.get(id).toString());
							while (true == m.find()) {
								if (null == set) {
									set = new LinkedHashSet<dro.Data>();
								}
								set.add(d);
							}
						}
					} else {
						throw new UnsupportedOperationException();
					}
				}
			}
		}
		return set;
	}
	
	@Override
	public Context add(final java.lang.Object o) {
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Context.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (dro.Data.class == o.getClass()) {
					final dro.Data d = (dro.Data)o;
				  /*final *//*dro.Data __d = null;
					if (true == d.has(dro.data.Context.$)) {
						final dro.data.Context dc = d.returns(dro.data.Context.Return.Context);
						if (null != dc && dc != this) {
							_d = new dro.Data();
						  //_d.set(dro.meta.Data.$, d.returns(dro.meta.Data.Return.Data)); // TODO
							_d.set("$dro$Data#id!proxy", d.id());
							_d.set("$dro$data$Context!proxy", dc);
						} else {
							_d = d;
						}
					} else {
						_d = d;
					}*/
				  /*final */java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Map.Return.Map);
					if (null == map) {
						map = new LinkedHashMap<java.lang.Object, dro.Data>();
						zero.set("$dro%Data", map);
					}
					if (false == map.containsKey(d.id())) {
						map.put(d.id(), d);
						d.set(dro.data.Context.$, this);
					}
				} else {
					throw new IllegalArgumentException();
				}
		  //} else if (dro.Data.class == this.getClass()) {
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Context.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "add");
			} else {
			}
		}
		return this;
	}
	
	@Override
	public Context set(final java.lang.Object k, final java.lang.Object v) {
		super.set(k, v);
		return this;
	}
	
	public dro.Data remove(final dro.Data d, final dro.Data.Return r) {
	  /*final dro.Data d = (dro.Data)*/this.remove(d);
		return d;
	}
	public dro.Data remove(final java.lang.Object id, final dro.Data.Return r) {
		final dro.Data d = (dro.Data)this.remove(id);
		return d;
	}
	@Override
	public java.lang.Object remove(final java.lang.Object o) {
	  /*final */java.lang.Object remove = null;
	  /*final */boolean handled = false;
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Context.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.meta.data.Context) {
					final java.util.Set<dro.Data> set = this.returns(dro.Data.Set.Return.Set);
					for (final dro.Data d: set) {
						this.remove(d);
					}
					zero.remove(dro.meta.data.Context.$id);
					zero.remove(dro.meta.data.Context.$);
					final dro.meta.data.Context mdc = (dro.meta.data.Context)o;
					mdc.remove(this);
					handled = true;
				} else if (true == o instanceof dro.Data) {
					final dro.Data d = (dro.Data)o;
					final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Map.Return.Map);
					if (null != map) {
						if (true == map.containsKey(d.id())) {
							remove = map.remove(d.id());
							d.remove(this);
						}
					}
					handled = true;
				} else if (java.lang.Integer.class == o.getClass()) {
					final java.lang.Integer id = (java.lang.Integer)o;
					final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Map.Return.Map);
					if (null != map) {
						if (true == map.containsKey(id)) {
							final dro.Data d = (dro.Data)map.remove(id);
							d.remove(this);
						}
					}
					handled = true;
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Context.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "remove");
			} else {
			}
		}
		return remove;
	}
}