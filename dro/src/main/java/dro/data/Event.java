package dro.data;

public class Event extends dro.data.Zer0ne {
	public static class Return {
		public static final dro.data.Event.Return Event = (dro.data.Event.Return)null;
	}
	
	public static void entering(final dro.data.Context dc, final dro.Data d, final java.lang.String action, final java.lang.Object[] oa) {
	  //new dro.lang.Event(Event.class, (Event)null, "entering")
		new dro.lang.Event(d.getClass(), d, "entering")
			.event(Event.build(dc, d, action, oa))
			.dispatch();
	}
	public static void entering(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (Event)null, "entering")
		new dro.lang.Event(d.getClass(), d, "entering")
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	public static void enter(final dro.data.Context dc, final dro.Data d, final java.lang.String action, java.lang.Object[] oa) {
	  //new dro.lang.Event(Event.class, (java.lang.Object)null, "enter")
		new dro.lang.Event(d.getClass(), d, "enter")
			.event(Event.build(dc, d, action, oa))
			.dispatch();
	}
	public static void enter(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (java.lang.Object)null, "enter")
		new dro.lang.Event(d.getClass(), d, "enter")
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	public static void entered(final dro.data.Context dc, final dro.Data d, final java.lang.String action, final java.lang.Object[] oa) {
	  //new dro.lang.Event(Event.class, (Event)null, "entered")
		new dro.lang.Event(d.getClass(), d, "entered")
			.event(Event.build(dc, d, action, oa))
			.dispatch();
	}
	public static void entered(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (Event)null, "entered")
		new dro.lang.Event(d.getClass(), d, "entered")
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	
	public static void event(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (Event)null, "event")
		new dro.lang.Event(d.getClass(), d, "event")
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	
	// Was "protected" but we're calling this directly from inside dro.Data.set(.., ..) now - for time being..
  //protected static dro.data.Event build(final dro.data.Context dc, final dro.Data d, final java.lang.String action, final java.lang.Object[] oa) {
	public static dro.data.Event build(final dro.data.Context dc, final dro.Data d, final java.lang.String action, final java.lang.Object[] oa) {
		final dro.data.Event e = new dro.data.Event()
			.set(dro.data.Context.$, dc)
		;
		final java.lang.Class<?> c = d.getClass();
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (c == dro.Data.class) {
			final dro.Data __d = (dro.Data)d;
			e.set(dro.Data.$, __d);
			if (null != __d) {
				final dro.meta.Data __md = (dro.meta.Data)__d.returns(dro.meta.Data.Return.Data);
				e.set(dro.meta.Data.$, __md);
			}
	  /*} else if (c == dro.data.Rule.class) {
			final dro.data.Rule __dr = (dro.data.Rule)d;
			e.set(dro.data.Rule.$, __dr);
			if (null != __dr) {
				final dro.meta.data.Rule __mdr = (dro.meta.data.Rule)__dr.returns(dro.meta.data.Rule.Return.Rule);
				e.set(dro.meta.data.Rule.$, __mdr);
				final dro.Data __d = (dro.Data)__dr.returns(dro.Data.Return.Data);
				e.set(dro.Data.$, __d);
			}*/
		} else if (c == dro.data.Field.class) {
			final dro.data.Field __df = (dro.data.Field)d;
			if (null != __df) {
				final dro.meta.data.Field __mdf = (dro.meta.data.Field)__df.returns(dro.meta.data.Field.Return.Field);
				e.set(dro.meta.data.Field.$, __mdf);
				final dro.Data __d = (dro.Data)__df.returns(dro.Data.Return.Data);
				e.set(dro.Data.$, __d);
			}
		} else if (c == dro.data.field.Rule.class) {
			final dro.data.field.Rule __dfr = (dro.data.field.Rule)d;
			e.set(dro.data.field.Rule.$, __dfr);
			if (null != __dfr) {
				final dro.meta.data.field.Rule __mdfr = (dro.meta.data.field.Rule)__dfr.returns(dro.meta.data.field.Rule.Return.Rule);
				e.set(dro.meta.data.field.Rule.$, __mdfr);
			}
		} else if (c == dro.meta.Data.class) {
			final dro.meta.Data __md = (dro.meta.Data)d;
			e.set(dro.meta.Data.$, __md);
		} else if (c == dro.meta.data.Rule.class) {
			final dro.meta.data.Rule __mdr = (dro.meta.data.Rule)d;
			e.set(dro.meta.data.Rule.$, __mdr);
		} else if (c == dro.meta.data.Field.class) {
			final dro.meta.data.Field __mdf = (dro.meta.data.Field)d;
			e.set(dro.meta.data.Field.$, __mdf);
		} else if (c == dro.meta.data.field.Rule.class) {
			final dro.meta.data.field.Rule __mdfr = (dro.meta.data.field.Rule)d;
			e.set(dro.meta.data.field.Rule.$, __mdfr);
		} else if (true == d instanceof dro.data.Context) {
			final dro.data.Context __dc = (dro.data.Context)d;
			e.set(dro.data.Context.$, __dc);
		} else {
			throw new IllegalStateException();
		}
		e.set("$action", action);
		if (null != oa) {
			e.set("$java$lang@Object", oa);
		}
		return e;
	}
	
	public static void exiting(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (Event)null, "exiting", new java.lang.Object[]{dc, d, action})
		new dro.lang.Event(d.getClass(), d, "exiting", new java.lang.Object[]{dc, d, action})
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	public static void exit(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (Event)null, "exit")
		new dro.lang.Event(d.getClass(), d, "exit", new java.lang.Object[]{dc, d, action})
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	public static void exited(final dro.data.Context dc, final dro.Data d, final java.lang.String action) {
	  //new dro.lang.Event(Event.class, (Event)null, "exited")
		new dro.lang.Event(d.getClass(), d, "exited", new java.lang.Object[]{dc, d, action})
			.event(Event.build(dc, d, action, (java.lang.Object[])null))
			.dispatch();
	}
	
  /*public Event(final dro.meta.data.Event mde) {
	  //super(); // implicit super constructor..
		if (null == mde) {
			throw new IllegalStateException();
		}
		this.__Event(/mde);
	}*/
	public Event() {
		super(); // implicit super constructor..
	}
	protected void __Event(/*final dro.meta.data.Event mde*/) {
	  //this.set("$id", this.hashCode()); // Already done in 
	  //this.set("$dro$meta$data$Event", mde);
	}
	
	@Override
	public Event set(final java.lang.Object k, final java.lang.Object o) {
	  /*final dro.Data d = */super.set(k, o);
		return this;
	}
}