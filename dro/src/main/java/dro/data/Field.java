package dro.data;

import java.util.LinkedHashSet;

//$id, $name, $dro$data$field@Rule
public class Field extends dro.Data implements java.lang.Cloneable {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static class Return {
			public static final Set Set = (Set)null;
		}
	}
	public static class Map {
		public static class Return {
			public static final Map Map = (Map)null;
		}
	}
	
	public static class Return {
		public static final dro.data.Field.Return Field = (dro.data.Field.Return)null;
		public static final dro.data.Field.List List = (dro.data.Field.List)null;
		public static final dro.data.Field.Set Set = (dro.data.Field.Set)null;
		public static final dro.data.Field.Map Map = (dro.data.Field.Map)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Field.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	public Field() {
		super();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()");
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
	public Field(final dro.meta.data.Field mdf) {
		this();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()", new java.lang.Object[]{md});
			this.__Field(mdf);
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
  /*protected */void __Field(final dro.meta.data.Field mdf) {
		try {
		  //dro.lang.Event.entered(dro.Data.class, this, "__Data", md);
			if (null != mdf) {
				zero.set("$name", mdf.get("$name"));
				this.set(dro.meta.data.Field.$, mdf);
			  /*final java.util.List<dro.meta.data.field.Rule> list = mdf.returns(dro.meta.data.field.Rule.Return.Rule);
				if (null != list && 0 < list.size()) {
					final java.util.Map<java.lang.Object, dro.data.field.Rule> map = new LinkedHashMap<java.lang.Object, dro.data.field.Rule>();
					zero.set("$dro$data$field@Rule", map);
				}*/
			} else {
				throw new IllegalArgumentException();
			}
		} finally {
		  //dro.lang.Event.exiting(dro.Data.class, this, "__Field");
		}
	}
	
	public dro.data.Field returns(final dro.data.Field.Return r) {
		return this;
	}
	public dro.Data returns(final dro.Data.Return r) {
		return (dro.Data)zero.get(dro.Data.$);
	}
	public dro.meta.data.Field returns(final dro.meta.data.Field.Return r) {
		return (dro.meta.data.Field)zero.get(dro.meta.data.Field.$);
	}
	
	public java.util.Set<dro.data.field.Rule> returns(final dro.data.field.Rule.Set r) {
		@SuppressWarnings("unchecked")
		final java.util.Set<dro.data.field.Rule> set = (java.util.Set<dro.data.field.Rule>)zero.get("$dro$data$field@Rule");
		return set;
	}
	
  //$id, $dro$data$field@Rule
	@Override
	public dro.data.Field set(final java.lang.Object o) {
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Field.class == this.getClass()) {
				final dro.Data d = this.returns(dro.Data.Return.Data);
				final dro.meta.data.Field mdf = this.returns(dro.meta.data.Field.Return.Field);
			  //final Result result = new Result(Result.UNKNOWN);
			/**/if (null == mdf.get("$type") || o.getClass() == mdf.get("$type")) { // TODO: make this part of the rule again, sometime soon..
				  //result.result(Result.TRUE);
					{
						final java.util.Set<dro.meta.data.field.Rule> set = mdf.returns(dro.meta.data.field.Rule.Return.Set);
						if (null != set && 0 < set.size()) {
						  //result.result(Result.FALSE);
						  /*for (final dro.meta.data.field.Rule mdfr: set) {
								final Result applied = mdfr.apply(new java.lang.Object[]{d, this, o});
								if (Result.TRUE == applied.result()) {
									result.result(applied);
								} else {
									result.result(applied);
									break;
								}
							}*/
						}
					}
					{
						final java.util.Set<dro.data.field.Rule> set = this.returns(dro.data.field.Rule.Return.Set);
						if (null != set && 0 < set.size()) {
						  //result.result(Result.FALSE);
							for (final dro.data.field.Rule dfr: set) {
							  //final dro.meta.data.field.Rule mdfr = (dro.meta.data.field.Rule)dfr.get("$dro$meta$data$field$Rule");
								final java.lang.Object[] oa = (java.lang.Object[])dfr.get("$");
								if (null != oa) {
									final java.lang.Object[] __oa = new java.lang.Object[oa.length];
									for (int i = 0; i < oa.length; i++) {
										__oa[i] = oa[i];
									}
									if (true == ((java.lang.String)oa[0]).equals("$")) {
										__oa[0] = o;
									} else if (true == ((java.lang.String)oa[0]).startsWith("$")) {
										__oa[0] = this.get(oa[0]);
									}
									if (null != this.get(oa[0])) { // Use a better check
									  /*final Result applied = mdfr.apply(new java.lang.Object[]{d, df, oa});
										final Result applied = mdfr.apply(__oa);
										if (Result.TRUE == applied.result()) {
											result.result(applied);
										} else {
											result.result(applied);
											break;
										}*/
									} else {
									  //result.result(Result.TRUE); // FIXME - this is bad, trying to apply rules when we can't apply them, if we're prior to setting a value for the first time..
									}
								} else {
								  //result.result(Result.TRUE);
								}
							}
						}
					}
				  //if (Result.TRUE == result.result()) {
						if (false == o instanceof java.lang.String) {
							d.set("$value", o); // FIXME: really should be $
						} else {
							// We could have set $ to o here but we have an opportunity to layer a new class (i.e. Text) here instead of supporting all the logic (e.g. $length) right here, we can support it in Text class
							// We could have also buried away this String=>Text logic in dro.data.Field but what when the String/Text is *not* a field? - we still have to modify the logic below to support that but at least we haven't backed ourselves into a corner with dro.data.Field..
						  //d.set("$", new Text(this).set((java.lang.String)o));
						}
				  //}
				}
			} else {
				throw new IllegalStateException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "set");
			} else {
			}
		}
		return this;
	}
	
  //$id, $dro$data$field@Rule
	@Override
	public Field add(final java.lang.Object o) {
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Field.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.data.field.Rule) {
				  /*final */java.util.Set<dro.data.field.Rule> set = this.returns(dro.data.field.Rule.Return.Set);
					if (null == set) {
						set = new LinkedHashSet<dro.data.field.Rule>();
						zero.set("$dro$data$field@Rule", set);
					}
					final dro.data.field.Rule dfr = (dro.data.field.Rule)o;
					set.add(dfr);
				} else if (true == o instanceof java.lang.Object) {
					throw new UnsupportedOperationException();
				} else {
					throw new IllegalArgumentException();
				}
		  //} else if (dro.data.Rule.class == this.getClass()) {
		  //} else if (dro.Data.class == this.getClass()) {
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "add");
			} else {
			}
		}
		return this;
	}
	
	@Override
	public java.lang.Object remove(final java.lang.Object o) {
	  /*final */java.lang.Object remove = null;
	  /*final */boolean handled = false;
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Field.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "remove", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.Data) {
					zero.remove(dro.Data.$id);
					zero.remove(dro.Data.$);
					final dro.Data d = (dro.Data)o;
					d.remove(this);
					handled = true;
			  /*} else if (true == o instanceof dro.data.field.Rule) {
					final java.util.Set<dro.data.field.Rule> set = this.returns(dro.data.field.Rule.Return.Set);
					if (null != set) {
						final dro.data.field.Rule dfr = (dro.data.field.Rule)o;
						set.remove(dfr);
						remove = o;
						if (0 == set.size()) {
							zero.remove("$dro$data$field@Rule");
						}
					}
					handled = true;*/
				} else {
					throw new IllegalArgumentException();
				}
		  //} else if (dro.data.Rule.class == this.getClass()) {
		  //} else if (dro.Data.class == this.getClass()) {
			} else {
				throw new IllegalStateException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "remove");
			} else {
			}
		}
		return remove;
	}
	
	@Override
	public dro.data.Field set(final java.lang.Object k, final java.lang.Object o) {
		super.set(k, o);
		return this;//.returns(dro.data.Field.Return.Rule);
	}
	
	@Override
	public dro.data.Field clone() {
		final dro.meta.data.Field mdf = this.returns(dro.meta.data.Field.Return.Field);
		final dro.data.Field df = new dro.data.Field(mdf);
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == zero.has("$")) {
			df.set(zero.get("$"));
		} else if (true == zero.has("@")) {
			throw new IllegalStateException();
		} else if (true == zero.has("%")) {
			throw new IllegalStateException();
		} else {
			throw new IllegalStateException();
		}
		return df;
	}
}