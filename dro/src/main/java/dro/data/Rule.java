package dro.data;

import dro.lang.Result;

public class Rule extends dro.Data {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static final Set Return = (Set)null;
	}
	
	public static class Return {
		public static final dro.data.Rule.Return Rule = (dro.data.Rule.Return)null;
		public static final dro.data.Rule.List List = (dro.data.Rule.List)null;
		public static final dro.data.Rule.Set Set = (dro.data.Rule.Set)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Rule.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	public Rule() {
		super();
	}
	public Rule(final dro.meta.data.Rule mdr) {
		this();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()", new java.lang.Object[]{md});
			if (null != mdr) {
				this.__Rule(mdr);
			} else {
				throw new IllegalStateException();
			}
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
  /*protected */void __Rule(final dro.meta.data.Rule mdr) {
		zero.set("$dro$meta$data$Rule", mdr);
	}
	
	public dro.data.Rule returns(final dro.data.Rule.Return r) {
		return this;
	}
	
	public dro.meta.data.Rule returns(final dro.meta.data.Rule.Return r) {
		return (dro.meta.data.Rule)zero.get("$dro$meta$data$Rule");
	}
	
	public Result apply(final java.lang.Object[] oa) {
		return Result.UNKNOWN;
	}
	public dro.lang.Number apply(final java.lang.Object[] oa, final dro.lang.Number.Return r) {
		return dro.lang.Number.UNDEFINED;
	}
	
	
	@Override
	public dro.data.Rule set(final java.lang.Object k, final java.lang.Object o) {
		super.set(k, o);
		return this;//.returns(dro.data.Rule.Return.Rule);
	}
  /*@Override
	public java.lang.Object get(final java.lang.Object key) {
		return super.get(key);
	}*/
}