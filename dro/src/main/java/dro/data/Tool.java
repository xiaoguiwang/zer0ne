package dro.data;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Tool {
	public static boolean __equals(/*final java.util.Set<?> in_1_and_in_2, */final java.util.Set<?> in_1_not_in_2, final java.util.Set<?> in_2_not_in_1) {
	  /*final */boolean equals = false;
		if (null == in_1_not_in_2) {
			if (null == in_2_not_in_1) {
				equals = true;
		  //} else if (null != in_2_not_in_1) {
			}
		} else { //if (null != in_1_not_in_2) {
			if (null == in_2_not_in_1) {
			} else {//if (null != in_2_not_in_1) {
				equals = 0 == in_1_not_in_2.size() && 0 == in_2_not_in_1.size();
			}
		}
		return equals;
	}
	public static boolean equals(final java.lang.Object o1, final java.lang.Object o2) {
	  /*final */boolean equals = false;
		if (null == o1 && null == o2) {
			equals = true;
		} else if (null != o1) {
			equals = o1.equals(o2);
		}
		return equals;
	}
	public static boolean equals(final java.util.Set<?> set1, final java.util.Set<?> set2) {
	  /*final */boolean equals = false;
		if (null == set1 && null == set2) {
			equals = true;
		} else if (null != set1 && null != set2) {
			equals = dro.data.Tool.equals(new ArrayList<>(set1), new ArrayList<>(set2));
		}
		return equals;
	}
	public static boolean equals(final java.util.List<?> list1, final java.util.List<?> list2) {
	  /*final */boolean equals = false;
		if (null == list1 && null == list2) {
			equals = true;
		} else if (null != list1 && null != list2) {
			final dro.Data diff = dro.data.Tool.asDiff((dro.Data)null, list1, list2);
			equals = dro.data.Tool.__equals(
			  //(java.util.Set<?>)diff.get("in-1-and-in-2"),
				(java.util.Set<?>)diff.get("in-1-not-in-2"),
				(java.util.Set<?>)diff.get("in-2-not-in-1")
			);
		}
		return equals;
	}
  /*private static boolean equals(final java.util.Map<?, ?> map1, final java.util.Map<?, ?> map2) {
	*//*final *//*boolean equals = false;
		if (null == map1 && null == map2) {
			equals = true;
		} else if (null != map1 && null != map2) {
			final dro.Data diff = dro.data.Tool.asDiff(map1, map2);
			equals = dro.data.Tool.__equals(
			  //(java.util.Set<?>)diff.get("in-1-and-in-2"),
				(java.util.Set<?>)diff.get("in-1-not-in-2"),
				(java.util.Set<?>)diff.get("in-2-not-in-1")
			);
		}
		return equals;
	}*/
	
	public static dro.Data asDiff(final java.lang.Object[] oa1, final java.lang.Object[] oa2) {
		return Tool.asDiff((dro.Data)null, oa1, oa2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object[] oa1, final java.lang.Object[] oa2) {
		final java.util.List<java.lang.Object> list1 = new ArrayList<>();
		for (final java.lang.Object o1: oa1) {
			list1.add(o1);
		}
		final java.util.List<java.lang.Object> list2 = new ArrayList<>();
		for (final java.lang.Object o2: oa2) {
			list2.add(o2);
		}
		return Tool.asDiff(diff, list1, list2);
	}
	
	public static dro.Data asDiff(final java.lang.Object[] oa1, final java.lang.Object o2) {
		return Tool.asDiff((dro.Data)null, oa1, o2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object[] oa1, final java.lang.Object o2) {
		return Tool.asDiff(diff, oa1, new java.lang.Object[]{o2});
	}
	
	public static dro.Data asDiff(final java.lang.Object o1, final java.lang.Object[] oa2) {
		return Tool.asDiff((dro.Data)null, o1, oa2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object o1, final java.lang.Object[] oa2) {
		return Tool.asDiff(diff, new java.lang.Object[]{o1}, oa2);
	}
	
	public static dro.Data asDiff(final java.util.Set<java.lang.Object> set1, final java.util.Set<java.lang.Object> set2) {
		return Tool.asDiff((dro.Data)null, set1, set2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.Set<java.lang.Object> set1, final java.util.Set<java.lang.Object> set2) {
		final java.util.List<?> list1 = new ArrayList<>(set1);
		final java.util.List<?> list2 = new ArrayList<>(set2);
		return Tool.asDiff(diff, list1, list2);
	}
	
	public static dro.Data asDiff(final java.util.Set<java.lang.Object> set1, final java.lang.Object[] oa2) {
		return Tool.asDiff((dro.Data)null, set1, oa2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.Set<java.lang.Object> set1, final java.lang.Object[] oa2) {
		final java.util.Set<?> set2 = new LinkedHashSet<>(set1);
		return Tool.asDiff(diff, set1, set2);
	}
	
	public static dro.Data asDiff(final java.util.Set<java.lang.Object> set1, final java.lang.Object o2) {
		return Tool.asDiff((dro.Data)null, set1, o2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.Set<java.lang.Object> set1, final java.lang.Object o2) {
		return Tool.asDiff(diff, set1, new java.lang.Object[]{o2});
	}
	
	public static dro.Data asDiff(final java.lang.Object[] oa1, final java.util.Set<java.lang.Object> set2) {
		return Tool.asDiff((dro.Data)null, oa1, set2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object[] oa1, final java.util.Set<java.lang.Object> set2) {
		final java.util.Set<java.lang.Object> set1 = new LinkedHashSet<>();
		for (final java.lang.Object o1: set1) {
			set2.add(o1);
		}
		return Tool.asDiff(diff, set1, set2);
	}
	
	public static dro.Data asDiff(final java.lang.Object o1, final java.util.Set<java.lang.Object> set2) {
		return Tool.asDiff((dro.Data)null, o1, set2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object o1, final java.util.Set<java.lang.Object> set2) {
		return Tool.asDiff(diff, new java.lang.Object[]{o1}, set2);
	}
	
	public static dro.Data asDiff(final java.util.List<java.lang.Object> list1, final java.util.List<java.lang.Object> list2) {
		return Tool.asDiff((dro.Data)null, list1, list2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.List<java.lang.Object> list1, final java.util.List<java.lang.Object> list2) {
		if (null == diff) {
			diff = new dro.Data();
		}
		for (final java.lang.Object o1: list1) {
		  /*final */boolean found = false;
			for (final java.lang.Object o2: list2) {
				if (true == o1.equals(o2)) {
					if (null == diff.get("in-1-and-in-2")) {
						diff.set("in-1-and-in-2", new LinkedHashSet<java.lang.Object>());
					}
					@SuppressWarnings("unchecked")
					final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)diff.get("in-1-and-in-2");
					set.add(o1);
				  //set.add(o2);
					found = true;
					break;
				}
			}
			if (false == found) {
				if (null == diff.get("in-1-not-in-2")) {
					diff.set("in-1-not-in-2", new LinkedHashSet<java.lang.Object>());
				}
				@SuppressWarnings("unchecked")
				final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)diff.get("in-1-not-in-2");
				set.add(o1);
			  //break;
			}
		}
		for (final java.lang.Object o2: list2) {
		  /*final */boolean found = false;
			for (final java.lang.Object o1: list1) {
				if (true == o2.equals(o1)) {
					found = true;
					break;
				}
			}
			if (false == found) {
				if (null == diff.get("in-2-not-in-1")) {
					diff.set("in-2-not-in-1", new LinkedHashSet<java.lang.Object>());
				}
				@SuppressWarnings("unchecked")
				final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)diff.get("in-2-not-in-1");
				set.add(o2);
			  //break;
			}
		}
		return diff;
	}
	
	public static dro.Data asDiff(final java.util.List<java.lang.Object> list1, final java.lang.Object[] oa2) {
		return Tool.asDiff((dro.Data)null, list1, oa2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.List<java.lang.Object> list1, final java.lang.Object[] oa2) {
		final java.util.List<java.lang.Object> list2 = new ArrayList<>();
		for (final java.lang.Object o2: oa2) {
			list2.add(o2);
		}
		return Tool.asDiff(diff, list1, list2);
	}
	
	public static dro.Data asDiff(final java.util.List<java.lang.Object> list1, final java.lang.Object o2) {
		return Tool.asDiff((dro.Data)null, list1, o2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.List<java.lang.Object> list1, final java.lang.Object o2) {
		return Tool.asDiff(diff, list1, new java.lang.Object[]{o2});
	}
	
	public static dro.Data asDiff(final java.lang.Object[] oa1, final java.util.List<java.lang.Object> list2) {
		return Tool.asDiff((dro.Data)null, oa1, list2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object[] oa1, final java.util.List<java.lang.Object> list2) {
		final java.util.List<java.lang.Object> list1 = new ArrayList<>();
		for (final java.lang.Object o1: oa1) {
			list1.add(o1);
		}
		return Tool.asDiff(diff, list1, list2);
	}
	
	public static dro.Data asDiff(final java.lang.Object o1, final java.util.List<java.lang.Object> list2) {
		return Tool.asDiff((dro.Data)null, o1, list2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object o1, final java.util.List<java.lang.Object> list2) {
		return Tool.asDiff(diff, new java.lang.Object[]{o1}, list2);
	}
	
	public static dro.Data asDiff(final java.util.Map<java.lang.Object, java.lang.Object> map1, final java.util.Map<java.lang.Object, java.lang.Object> map2) {
		return Tool.asDiff((dro.Data)null, map1, map2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.util.Map<java.lang.Object, java.lang.Object> map1, final java.util.Map<java.lang.Object, java.lang.Object> map2) {
		if (null == diff) {
			diff = new dro.Data();
		}
		for (final java.lang.Object k: map1.keySet()) {
		  /*final */boolean found = false;
			if (true == map2.containsKey(k)) {
				final java.lang.Object v1 = map1.get(k);
				final java.lang.Object v2 = map2.get(k);
				if (true == v1.equals(v2)) {
					if (null == diff.get("in-1-and-in-2")) {
						diff.set("in-1-and-in-2", new LinkedHashSet<java.lang.Object>());
					}
					@SuppressWarnings("unchecked")
					final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)diff.get("in-1-not-in-2");
					set.add(k);
					found = true;
				  //break;
				}
			}
			if (false == found) {
				if (null == diff.get("in-1-not-in-2")) {
					diff.set("in-1-not-in-2", new LinkedHashSet<java.lang.Object>());
				}
				@SuppressWarnings("unchecked")
				final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)diff.get("in-1-not-in-2");
				set.add(k);
			  //break;
			}
		}
		for (final java.lang.Object k: map2.keySet()) {
		  /*final */boolean found = false;
			if (true == map2.containsKey(k)) {
				found = true;
			  //break;
			}
			if (false == found) {
				if (null == diff.get("in-2-not-in-1")) {
					diff.set("in-2-not-in-1", new LinkedHashSet<java.lang.Object>());
				}
				@SuppressWarnings("unchecked")
				final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)diff.get("in-2-not-in-1");
				set.add(k);
			  //break;
			}
		}
		return diff;
	}
	
	public static dro.Data asDiff(final java.lang.Object o1, final java.lang.Object o2) {
		return Tool.asDiff((dro.Data)null, o1, o2);
	}
	public static dro.Data asDiff(/*final */dro.Data diff, final java.lang.Object o1, final java.lang.Object o2) {
		if (null == diff) {
			diff = new dro.Data();
		}
		if (null == o1 && null == o2) {
			diff.set("1-is-2", true);
			diff.set("1-is-not-2", false);
		} else if (null != o1) {
			final boolean equals = o1.equals(o2);
			diff.set("1-is-2", equals);
			diff.set("1-is-not-2", !equals);
		} else {
			diff.set("1-is-2", false);
			diff.set("1-is-not-2", true);
		}
		return diff;
	}
	
	public static java.util.List<dro.Data> asList(/*final */java.util.List<dro.Data> list, final dro.Data d) {
		if (null == list) {
			list = new ArrayList<dro.Data>();
		}
		list.add(d);
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == d.get("$") instanceof dro.Data) {
			Tool.asList(list, (dro.Data)d.get("$"));
		} else if (true == d.has("@")) {
			@SuppressWarnings("unchecked")
			final java.util.List<java.lang.Object> __list = (java.util.List<java.lang.Object>)d.get("@");
			for (final java.lang.Object o: __list) {
				if (true == o instanceof dro.Data) {
					Tool.asList(list, (dro.Data)o);
				}
			}
		} else if (true == d.has("%")) {
			@SuppressWarnings("unchecked")
			final java.util.Map<java.lang.Object, java.lang.Object> __map = (java.util.Map<java.lang.Object, java.lang.Object>)d.get("%");
			for (final java.lang.Object k: __map.keySet()) {
				final java.lang.Object v = __map.get(k);
				if (true == v instanceof dro.Data) {
					Tool.asList(list, (dro.Data)v);
				}
			}
		} else {
		}
		final java.util.Set<?> set = d.keys(dro.Data.Return.Set);
		if (null != set) {
			for (final java.lang.Object k: set) {
				final java.lang.Object v = d.get(k);
				if (true == v instanceof dro.Data) {
					Tool.asList(list, (dro.Data)v);
				}
			}
		}
		return list;
	}
	public static java.util.List<dro.Data> asList(final dro.Data d) {
		return Tool.asList((java.util.List<dro.Data>)null, d);
	}
}