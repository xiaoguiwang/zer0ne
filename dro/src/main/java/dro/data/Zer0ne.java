package dro.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class Zer0ne {
	protected static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	protected static Date getDateTimeStamp() {
		final Date d = new Date();
	  //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	  //final String dts = sdf.format(d);
		return d;
	}
	
	protected java.lang.Object o;// = null;
	{
	  //o = null;
	}
	protected Zer0ne() {
	  //super(); // Implicit super-constructor..
		this.__set("$created-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
	}
	public Zer0ne(final dro.Data d) {
	  //super(); // Implicit super-constructor..
		this.__set("$created-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
	}
	
	protected Zer0ne __set(final java.lang.Object k, final java.lang.Object v, final boolean flag) {
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (null == this.o) {
			final java.util.Map<java.lang.Object, java.lang.Object> map = new LinkedHashMap<java.lang.Object, java.lang.Object>();
			map.put(k, v);
			this.o = map;
		} else if (true == this.o instanceof java.util.Map<?, ?>) {
			@SuppressWarnings("unchecked")
			final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)this.o;
			map.put(k, v);
		} else {
			throw new IllegalArgumentException();
		}
		if (true == flag) {
			this.__set("$modified-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
		}
		return this;
	}
	public Zer0ne set(final java.lang.Object k, final java.lang.Object v) {
		return this.__set(k, v, true);
	}
	
	protected Zer0ne __add(final java.lang.Object o, final boolean flag) {
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (null == this.o) {
			final java.util.Set<java.lang.Object> set = new LinkedHashSet<java.lang.Object>();
			set.add(o);
			this.o = set;
		} else if (true == this.o instanceof java.util.Set<?>) {
			@SuppressWarnings("unchecked")
			final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)this.o;
			set.add(o);
		} else if (true == this.o instanceof java.lang.Object) {
			java.util.Set<java.lang.Object> set = new java.util.LinkedHashSet<>();
			set.add(this.o);
			set.add(o);
			this.o = set;
		} else {
			throw new IllegalArgumentException();
		}
		if (true == flag) {
			this.__set("$modified-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
		}
		return this;
	}
	public Zer0ne add(final java.lang.Object o) {
		return this.__add(o, true);
	}
	
	protected boolean __has(final java.lang.Object o, final boolean flag) {
	  /*final */boolean contains = false;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == this.o instanceof java.util.Set<?>) {
			final java.util.Set<?> set = (java.util.Set<?>)this.o;
			if (null != set) {
				contains = set.contains(o);
			}
		} else if (true == this.o instanceof java.util.Map<?, ?>) { // Treat o as map key
			final java.util.Map<?, ?> map = (java.util.Map<?, ?>)this.o;
			if (null != map) {
				contains = map.containsKey(o);
			}
		} else {
			throw new IllegalArgumentException();
		}
		if (true == flag) {
			this.__set("$accessed-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
		}
		return contains;
	}
	public boolean has(final java.lang.Object o) {
		return this.__has(o, true);
	}
	
	protected java.lang.Object __remove(final java.lang.Object o, final boolean flag) {
	  /*final */java.lang.Object remove = null;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == this.o instanceof java.util.Set<?>) {
			@SuppressWarnings("unchecked")
			final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)this.o;
			if (null != set) {
				set.remove(o);
				remove = o;
			}
		} else if (true == this.o instanceof java.util.Map<?, ?>) { // Treat o as map key
			@SuppressWarnings("unchecked")
			final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)this.o;
			if (null != map) {
				if (true == map.containsKey(o)) {
					remove = map.remove(o);
				}
			}
		} else {
			throw new IllegalArgumentException();
		}
		if (true == flag) {
			this.__set("$modified-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
		}
		return remove;
	}
	public java.lang.Object remove(final java.lang.Object o) {
		return this.__remove(o, true);
	}
	
	protected java.lang.Object __get(final java.lang.Object k, final boolean flag) {
	  /*final */java.lang.Object o = null;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == this.o instanceof java.util.Map<?, ?>) {
			@SuppressWarnings("unchecked")
			final java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)this.o;
			if (null != map) {
				o = map.get(k);
			}
		} else {
		  //throw new IllegalArgumentException();
			o = this.o;
		}
		if (true == flag) {
			this.__set("$accessed-date-time-stamp", Zer0ne.getDateTimeStamp(), false);
		}
		return o;
	}
	public java.lang.Object get(final java.lang.Object k) {
		return this.__get(k, true);
	}
}