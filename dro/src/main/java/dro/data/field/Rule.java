package dro.data.field;

public class Rule extends dro.data.Rule {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static final Set Return = (Set)null;
	}
	
	public static class Return {
		public static final dro.data.field.Rule.Return Rule = (dro.data.field.Rule.Return)null;
		public static final dro.data.field.Rule.List List = (dro.data.field.Rule.List)null;
		public static final dro.data.field.Rule.Set Set = (dro.data.field.Rule.Set)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Rule.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	public Rule(final dro.meta.data.field.Rule mdfr) {
	  //super(); // implicit super constructor..
		if (null == mdfr) {
			throw new IllegalStateException();
		}
		this.__Rule(mdfr);
	}
	public Rule() {
	}
	protected void __Rule(final dro.meta.data.field.Rule mdfr) {
		zero.set(dro.meta.data.field.Rule.$, mdfr);
	}
	
	public dro.data.field.Rule returns(final dro.data.field.Rule.Return r) {
		return this;
	}
	
	public dro.meta.data.field.Rule returns(final dro.meta.data.field.Rule.Return r) {
		return (dro.meta.data.field.Rule)zero.get("$dro$meta$data$field$Rule");
	}
	
	@Override
	public dro.data.field.Rule set(final java.lang.Object k, final java.lang.Object o) {
		super.set(k, o);
		return this;//.returns(dro.data.field.Rule.Return.Rule);
	}
	
  /*@Override
	public dro.data.field.Rule add(final java.lang.Object o) {
		if (dro.data.field.Rule.class == this.getClass()) {
			if (true == o instanceof java.lang.Object[]) {
				this.set("$", o);
			} else {
				throw new IllegalStateException();
			}
		} else {
			throw new IllegalStateException();
		}
		return this;
	}
	
  /*@Override
	public java.lang.Object get(final java.lang.Object key) {
		return super.get(key);
	}*/
}