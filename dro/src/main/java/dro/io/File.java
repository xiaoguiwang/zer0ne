package dro.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class File {
  //private static final String className = File.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	public static final java.lang.String separator = java.io.File.separator;
	
	public static class Return extends dro.lang.Return {
		public static final File.Return File = (File.Return)null;
		public static final File.Reader.Return Reader = (File.Reader.Return)null;
		public static final File.Writer.Return Writer = (File.Writer.Return)null;
	}
	public static class Property {
		public static final String name = dro.lang.Class.getName(java.io.File.class)+"#name";
	}
	
	public static List<java.lang.String> list(final java.lang.String directoryName, final java.lang.String filenameOrFilter) {
		final List<String> list = new ArrayList<>();
		if (true == filenameOrFilter.contains("*") || true == filenameOrFilter.contains("?")) {
			final String paths[] = new java.io.File(directoryName).list(new FilenameFilter(){
				final String __filenameFilter = filenameOrFilter
					.replaceAll("\\.", "\\\\.")
					.replaceAll("\\(", "\\\\(")
					.replaceAll("\\)", "\\\\)")
					.replaceAll("\\*", ".\\*")
					.replaceAll("\\?", ".")
				;
				@Override
				public boolean accept(final java.io.File arg0, final java.lang.String arg1) {
					if (true == arg1.matches(__filenameFilter) && true == new java.io.File(arg0.getAbsolutePath()+java.io.File.separator+arg1).isFile()) {
						return true;
					} else {
						return false;
					}
				}
			});
			if (null != paths) {
				for (final java.lang.String path: paths) {
					list.add(path);
				}
			}
		}
		return list;
	}
	
	public static void delete(final java.lang.String directoryName, final java.lang.String filenameOrFilter) {
		if (false == filenameOrFilter.contains("*") && false == filenameOrFilter.contains("?")) {
			new java.io.File(filenameOrFilter).delete();
		} else {
			final List<java.lang.String> fileNames = File.list(directoryName, filenameOrFilter);
			for (final java.lang.String fileName: fileNames) {
				new java.io.File(directoryName+java.io.File.separator+fileName).delete();
			}
		}
	}
	
	protected java.io.File f = null;
  //protected dro.util.Properties p = null;
	protected File.Writer writer;
	protected File.Reader reader;
	
	{
	  //f = null;
	}
	
	public File(final java.lang.String fileName) {
	  //this.p.property(File.Property.name, fileName);
		this.f = new java.io.File(fileName);
	}
	
	public java.io.File getFile() {
		return this.f;
	}
	
	public long length() {
		return this.getFile().length();
	}
	
	public boolean exists() {
		return this.getFile().exists();
	}
	
	public boolean delete() {
		return this.getFile().delete();
	}
	
	public File.Reader returns(final File.Reader.Return r) {
		return new File.Reader(this);
	}
	public File.Writer returns(final File.Writer.Return r) {
		return new File.Writer(this);
	}
	
	public File close() throws Exception {
		if (null != this.f) {
		  //if (true == this.f.isOpen()) {
			  //this.f.close();
				this.f = null;
		  //}
		}
		return this;
	}
	
	public static class Reader {
		public static class Return extends File.Return {
			public static final Reader.Return Reader = (Reader.Return)null;
		}
		
		protected File f;// = null;
		
		{
		  //f = null;
		}
		
		public Reader(final File f) {
			this.f = f;
		}
		
		public byte[] read(final dro.lang.Byte.Array.Return r) throws java.io.IOException {
			return this.read();
		}
		public java.lang.String read(final dro.lang.String.Return r) throws java.io.IOException {
			return new java.lang.String(this.read());
		}
		public byte[] read() throws java.io.IOException {
			InputStream is = null;
			OutputStream os = null;
			try {
				final java.io.File file = this.f.getFile();
				try {
					is = new FileInputStream(this.f.getFile());
				} catch (final FileNotFoundException e) {
					// Silently ignore..
					is = this.getClass().getClassLoader().getResourceAsStream(file.getName());
					if (null == is) {
						throw e;
					}
				}
				if (null != is) {
				  //final java.io.Reader in = new InputStreamReader(is);//, "UTF-8");
				  //final char[] ca = new char[1024];
					final byte[] ba = new byte[1024];
					for (int r = 0; r >= 0; ) {
						r = is.read(ba, 0, ba.length);
						if (r >= 0) {
							if (null == os) {
								os = new ByteArrayOutputStream();
							}
							os.write(ba, 0, r);
						}
					}
					if (null != os) {
					  //os.close();
					}
				}
			} finally {
				if (null != is) {
					is.close();
				  //is = null;
				}
			}
			return null == os ? null : ((ByteArrayOutputStream)os).toByteArray();
		}
		
		public void close() throws java.io.IOException {
			if (null != this.f) {
			  //this.f.close();
				this.f = null;
			}
		}
	}
	
	public static class Writer {
		public static class Return extends File.Return {
			public static final Writer.Return Writer = (Writer.Return)null;
		}
		
		protected File f;// = null;
		
		{
		  //f = null;
		}
		
		public Writer(final File f) {
			this.f = f;
		}
		
		protected Writer write(final java.lang.String string) throws java.io.IOException {
			InputStream is = null;
			OutputStream os = null;
			try {
				is = new ByteArrayInputStream(string.getBytes());
				os = new FileOutputStream(this.f.getFile());
				final byte[] ba = new byte[1024];
				for (int r = 0; r >= 0; ) {
					r = is.read(ba, 0, ba.length);
					if (r >= 0) {
						os.write(ba, 0, r);
					}
				}
				if (null != os) {
					os.close();
				}
			} finally {
				if (null != is) {
					is.close();
				  //is = null;
				}
			}
			return this;
		}
		
		public void close() throws java.io.IOException {
			if (null != this.f) {
			  //this.f.close();
				this.f = null;
			}
		}
	}
}