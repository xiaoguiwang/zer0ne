package dro.lang;

public class Boolean {
	public static final class Return {
		public static final dro.lang.Boolean.Return Boolean = (dro.lang.Boolean.Return)null;
	}
	
	protected boolean value;// = false;
	
	{
	  //value = true;
	}
	
	public Boolean(final boolean value) {
		this.value(value);
	}
	
	public Boolean value(final boolean value) {
		this.value = value;
		return this;
	}
	public boolean value() {
		return this.value;
	}
}