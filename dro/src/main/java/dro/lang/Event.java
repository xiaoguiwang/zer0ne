package dro.lang;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Event extends java.lang.Object {
	public static class Return {
		public static final dro.lang.Event.Return Event = (dro.lang.Event.Return)null;
	}
	
	protected static final Map<Long, java.util.Map<?, ?>> ctx = new HashMap<>();
	
	protected static final Event build(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName) {
		final Long threadId = new Long(Thread.currentThread().getId());
		synchronized(ctx) {
		  /*final */java.util.Map<?, ?> __ctx = ctx.get(threadId);
			if (null == __ctx) {
				__ctx = new HashMap<Long, java.util.Map<?, ?>>();
				ctx.put(threadId, __ctx);
			}
		}
		return new Event(c, o, methodName);
	}
	
	public static class Listener {
		public Listener dispatch(final dro.lang.Event e) {
			throw new IllegalStateException();
		}
	}
	
	protected static final Map<Event.Listener, java.util.Map<java.lang.Class<?>, java.util.Map<java.lang.Object, java.util.Set<java.lang.String>>>> els = new LinkedHashMap<>();
	
	protected static void register(
		final Event.Listener el,
		java.util.Map<
			java.lang.Class<?>,
			java.util.Map<
				java.lang.Object,
				java.util.Set<java.lang.String>
			>
		> map0,
		final java.lang.Class<?> c,
		final java.util.Map<
			java.lang.Object,
			java.util.Set<java.lang.String>
		> map1,
		final java.lang.Object o,
		final java.util.Set<java.lang.String> map2,
		final java.lang.String methodName
	) {
		if (false == map2.contains(methodName)) {
			map2.add(methodName);
		}
	}
	protected static void register(
		final Event.Listener el,
		java.util.Map<
			java.lang.Class<?>,
			java.util.Map<
				java.lang.Object,
				java.util.Set<java.lang.String>
			>
		> map0,
		final java.lang.Class<?> c,
		final java.util.Map<
			java.lang.Object,
			java.util.Set<java.lang.String>
		> map1,
		final java.lang.Object o,
		final java.lang.String[] methodNames
	) {
		if (false == map1.containsKey(o)) {
			map1.put(o, new java.util.LinkedHashSet<java.lang.String>());
		}
		final java.util.Set<java.lang.String> map2 = (
			java.util.Set<java.lang.String>
		)map1.get(o);
		if (null == methodNames) {
			Event.register(el, map0, c, map1, (java.lang.Object)null, map2, (java.lang.String)null);
		} else {
			for (final java.lang.String methodName: methodNames) {
				Event.register(el, map0, c, map1, o, map2, methodName);
			}
		}
	}
	protected static void register(
		final Event.Listener el,
		java.util.Map<
			java.lang.Class<?>,
			java.util.Map<
				java.lang.Object,
				java.util.Set<java.lang.String>
			>
		> map0,
		final java.lang.Class<?> c,
		final java.lang.Object[] oa,
		final java.lang.String[] methodNames
	) {
		if (false == map0.containsKey(c)) {
			map0.put(c, new java.util.LinkedHashMap<java.lang.Object, java.util.Set<java.lang.String>>());
		}
		final java.util.Map<
			java.lang.Object,
			java.util.Set<java.lang.String>
		> map1 = (
			java.util.Map<
				java.lang.Object,
				java.util.Set<java.lang.String>
			>
		)map0.get(c);
		if (null == oa) {
			Event.register(el, map0, c, map1, (java.lang.Object)null, methodNames);
		} else {
			for (final java.lang.Object o: oa) {
				Event.register(el, map0, c, map1, o, methodNames);
			}
		}
	}
	public static void register(
		final Event.Listener el,
		final java.lang.Class<?>[] ca,
		final java.lang.Object[] oa,
		final java.lang.String[] methodNames
	) {
		if (false == els.containsKey(el)) {
			els.put(el, new java.util.LinkedHashMap<java.lang.Class<?>, java.util.Map<java.lang.Object, java.util.Set<java.lang.String>>>());
		}
		final java.util.Map<
			java.lang.Class<?>,
			java.util.Map<
				java.lang.Object,
				java.util.Set<java.lang.String>
			>
		> map0 = (
			java.util.Map<
				java.lang.Class<?>,
				java.util.Map<
					java.lang.Object,
					java.util.Set<java.lang.String>
				>
			>
		)els.get(el);
		if (null == ca) {
			Event.register(el, map0, (java.lang.Class<?>)null, oa, methodNames);
		} else {
			for (final java.lang.Class<?> c: ca) {
				Event.register(el, map0, c, oa, methodNames);
			}
		}
	}
	
	public static Event entering(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
		return Event.enter(Event.class, (Event)null, "entering", new java.lang.Object[]{c, o, methodName, oa});
	}
	public static Event entering(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object arg) {
		return Event.enter(Event.class, (Event)null, "entering", new java.lang.Object[]{c, o, methodName, arg});
	}
	public static Event enter(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
		final java.lang.String __methodName = (null == methodName ? "enter" : methodName);
		return Event.build(c, o, __methodName).oa(oa).dispatch();
	}
	public static Event entered(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
		return Event.enter(Event.class, (Event)null, "entered", new java.lang.Object[]{c, o, methodName, oa});
	}
	public static Event entered(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object arg) {
		return Event.enter(Event.class, (Event)null, "entered", new java.lang.Object[]{c, o, methodName, arg});
	}
	public static Event entered(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName) {
		return Event.enter(Event.class, (Event)null, "entered", new java.lang.Object[]{c, o, methodName});
	}
	
	public static Event exiting(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName) {
		return Event.exit(Event.class, (Event)null, "exiting", new java.lang.Object[]{c, o, methodName});
	}
	public static Event exiting(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object returned) {
		return Event.exit(Event.class, (Event)null, "exiting", new java.lang.Object[]{c, o, methodName, returned});
	}
	public static Event exit(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
		final java.lang.String __methodName = (null == methodName ? "exit" : methodName);
		return Event.build(c, o, __methodName).oa(oa).dispatch();
	}
	public static Event exited(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName) {
		return Event.exit(Event.class, (Event)null, "exited", new java.lang.Object[]{c, o, methodName});
	}
	public static Event exited(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object returned) {
		return Event.exit(Event.class, (Event)null, "exited", new java.lang.Object[]{c, o, methodName, returned});
	}
	
	public static void deregister(final dro.lang.Event.Listener el, final java.lang.Class<?>[] ca, final java.lang.Object[] oa, final java.lang.String[] methodNames) {
		if (true == els.containsKey(el)) {
			final java.util.Map<
				java.lang.Class<?>,
				java.util.Map<
					java.lang.Object,
					java.util.Set<java.lang.String>
				>
			> map0 = (
				java.util.Map<
					java.lang.Class<?>,
					java.util.Map<
						java.lang.Object,
						java.util.Set<java.lang.String>
					>
				>
			)els.get(el);
			for (final java.lang.Class<?> c: ca) {
				if (true == map0.containsKey(c)) {
					final java.util.Map<
						java.lang.Object,
						java.util.Set<java.lang.String>
					> map1 = (
						java.util.Map<
							java.lang.Object,
							java.util.Set<java.lang.String>
						>
					)map0.get(c);
					for (final java.lang.Object o: oa) {
						if (true == map1.containsKey(o)) {
							final java.util.Set<java.lang.String> map2 = (
								java.util.Set<java.lang.String>
							)map1.get(o);
							for (final java.lang.String methodName: methodNames) {
								if (true == map2.contains(methodName)) {
									map2.remove(methodName);
								}
							}
							if (0 == map2.size()) {
								map1.remove(o);
							}
						}
					}
					if (0 == map1.size()) {
						map0.remove(c);
					}
				}
			}
			if (0 == map0.size()) {
				els.remove(el);
			}
		}
	}
	public static void deregister(final dro.lang.Event.Listener el) {
		// TODO: deregiste the listener from any/all events
	}
	
	protected /*final */java.lang.Class<?> c;// = null;
	protected /*final */java.lang.Object o;// = null;
	protected /*final */java.lang.String methodName;// = null;
	protected /*final */java.lang.Object[] oa;// = null;
	
  //protected final java.util.Map<java.lang.Object, java.lang.Object> map;// = null;
	
	protected /*final */dro.data.Event de;// = null;
	
	{
	  //map = new HashMap<java.lang.Object, java.lang.Object>();
	  //de = null;
	}
	
	public Event(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName) {
	  //super(); // implicit super constructor..
		this.c(c).o(o).methodName(methodName);
	}
	public Event(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.String methodName, final java.lang.Object[] oa) {
		this(c, o, methodName);
		this.oa(oa);
	}
	
	protected Event c(final java.lang.Class<?> c) {
		this.c = c;
		return this;
	}
  //protected java.lang.Class<?> c() {
	public java.lang.Class<?> c() {
		return this.c;
	}
	
	protected Event o(final java.lang.Object o) {
		this.o = o;
		return this;
	}
  //protected java.lang.Object o() {
	public java.lang.Object o() {
		return this.o;
	}
	
  //protected Event methodName(final java.lang.String methodName) {
	public Event methodName(final java.lang.String methodName) {
		this.methodName = methodName;
		return this;
	}
	public java.lang.String methodName() {
		return this.methodName;
	}
	
	protected Event oa(final java.lang.Object[] oa) {
		this.oa = oa;
		return this;
	}
  //protected java.lang.Object[] oa() {
	public java.lang.Object[] oa() {
		return this.oa;
	}
	
  /*protected Event set(final java.lang.Object k, final java.lang.Object v) {
		this.map.put(k, v);
		return this;
	}*/
  /*protected java.lang.Object get(final java.lang.Object k) {
		return this.map.get(k);
	}*/
	
	public Event event(final dro.data.Event de) {
		this.de = de;
		return this;
	}
	public dro.data.Event event() {
		return this.de;
	}
	
	public Event dispatch() {
		if (null != els) {
			for (final Event.Listener el: els.keySet()) {
				final java.util.Map<
					java.lang.Class<?>,
					java.util.Map<
						java.lang.Object,
						java.util.Set<java.lang.String>
					>
				> map0 = (
					java.util.Map<
						java.lang.Class<?>,
						java.util.Map<
							java.lang.Object,
							java.util.Set<java.lang.String>
						>
					>
				)els.get(el);
				if (null == map0 || true == map0.containsKey(this.c) || true == map0.containsKey((java.lang.Class<?>)null) || null == this.c) {
				  /*final */java.util.Map<
						java.lang.Object,
						java.util.Set<java.lang.String>
					> map1;// = null;
					if (null == map0) {
						map1 = null;
					} else {
						map1 = (
							java.util.Map<
								java.lang.Object,
								java.util.Set<java.lang.String>
							>
						)map0.get(this.c);
						if (null == map1 && null != this.c) {
							map1 = (
								java.util.Map<
								java.lang.Object,
								java.util.Set<java.lang.String>
							>)map0.get((java.lang.Class<?>)null);
						}
					}
					if (null == map1 || true == map1.containsKey(this.o) || true == map1.containsKey((java.lang.Object)null) || null == this.o) {
					  /*final */java.util.Set<java.lang.String> map2;// = null;
						if (null == map1) {
							map2 = null;
						} else {
							map2 = (
								java.util.Set<java.lang.String>
							)map1.get(this.o);
							if (null == map2 && null != this.o) {
								map2 = (
									java.util.Set<java.lang.String>
								)map1.get((java.lang.Object)null);
							}
						}
						if (
							(
								null == map2
							) || (
								true == map2.contains(this.methodName)
							) || (
								null == this.methodName && true == map2.contains((java.lang.String)null)
							) || (
								1 == map2.size() && map2.contains(null)
							)
						){
							el.dispatch(this);
						}
					}
				}
			}
		}
		return this;
	}
}