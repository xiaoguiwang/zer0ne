package dro.lang;

public class Integer extends Object {
	public static final class Return {
		public static final Integer.Return Integer = (Integer.Return)null;
	}
	public static class Property {
		public static final java.lang.String Integer = dro.lang.Class.getName(java.lang.Integer.class);
		public static final java.lang.String value = Integer+"#value";
	}
	
	private java.lang.Integer __Integer;// = null;
	
	{
	  //__Integer = null;
	}
	
	public Integer() {
	}
	public Integer(final java.lang.Integer __Integer) {
		this.__Integer = __Integer;
	}
	
	public Integer value(final java.lang.Integer __Integer) {
		this.__Integer = __Integer;
		return this;
	}
	public java.lang.Integer value() {
		return this.__Integer;
	}
	
	@Override
	public java.lang.String toString() {
		return this.__Integer.toString();
	}
}