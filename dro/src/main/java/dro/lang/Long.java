package dro.lang;

public class Long extends Object {
	public static final class Return {
		public static final Long.Return Long = (Long.Return)null;
	}
	public static class Property {
		public static final java.lang.String Long = dro.lang.Class.getName(java.lang.Long.class);
		public static final java.lang.String value = Long+"#value";
	}
	
	private java.lang.Long __Long;// = null;
	
	{
	  //__Long = null;
	}
	
	public Long() {
	}
	public Long(final java.lang.Long __Long) {
		this.__Long = __Long;
	}
	public Long(final java.lang.Integer __Integer) {
		this(new java.lang.Long(__Integer));
	}
	
	public Long value(final java.lang.Long __Long) {
		this.__Long = __Long;
		return this;
	}
	public java.lang.Long value() {
		return this.__Long;
	}
	
	@Override
	public java.lang.String toString() {
		return this.__Long.toString();
	}
}