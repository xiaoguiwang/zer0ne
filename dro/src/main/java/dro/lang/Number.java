package dro.lang;

public class Number {
	public static final Number UNDEFINED = new Number();
	
	public static class Return {
		public static final dro.lang.Number.Return Number = (dro.lang.Number.Return)null;
	}
	
	protected java.lang.Integer n;// = null;
	
	private Number() {
	}
	public Number(final java.lang.Integer n) { // FIXME!
		this.value(n);
	}
	
	public Number value(final java.lang.Integer n) {
		this.n = n;
		return this;
	}
	public java.lang.Integer value(final dro.lang.Number.Return r) {
		return this.n;
	}
}