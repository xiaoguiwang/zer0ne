package dro.lang;

import java.util.ArrayList;
import java.util.HashMap;

public class Object extends java.lang.Object {
	protected static Sequence seq = null;  // ==> convert to dro.Context sequence
	
	protected static java.util.Map<java.lang.Integer, java.lang.Object> mapIdToRef = new HashMap<>();
  //protected static java.util.Map<java.lang.Object, java.lang.Integer> mapRefToId = new HashMap<>();
	
	public static boolean exists(final java.lang.Object o) {
	  /*final */boolean exists = false;
		for (final java.lang.Integer i: dro.lang.Object.mapIdToRef.keySet()) {
			final java.lang.Object p = dro.lang.Object.mapIdToRef.get(i);
			if (p == o) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	public static int hashCode(final java.lang.Object o) {
	  /*final */int hashCode;
		try {
			hashCode = o.hashCode();
		} catch (java.lang.StackOverflowError e) {
			hashCode = 0;
		}
		return hashCode;
	}
	
	static {
		dro.lang.Object.seq = new Sequence();
	}
	
	protected java.lang.Class<?> c;
	
	protected java.util.List<dro.lang.Object> by = null; // referenced-by
	protected java.util.List<dro.lang.Object> to = null; // references to
	// Temporary hack for JUnit SerializerTest
	public java.lang.Integer i;// = null;
	protected java.lang.Class<?> type;// = null;
	
	{
	  //i = new Integer(0);
	  //type = null;
	}
	
	public static class Return {
		public static final dro.lang.Object.Return Object = (dro.lang.Object.Return)null;
	}
	
	public Object() {
	}
	public Object(final java.lang.Class<?> c, final java.lang.Object o) {
		if (null != c) {
			this.c = o.getClass();
		} else {
			this.c = java.lang.Object.class;
		}
		if (false == dro.lang.Object.exists(o)) {
			this.i = dro.lang.Object.seq.next();
			dro.lang.Object.mapIdToRef.put(this.i, o);
		} else {
			for (final java.lang.Integer i: dro.lang.Object.mapIdToRef.keySet()) {
				final java.lang.Object p = dro.lang.Object.mapIdToRef.get(i);
				if (p == o) {
					this.i = i;
					break;
				}
			}
		}
	}
	public Object(final java.lang.Boolean o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Byte o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Short o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Integer o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Long o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Float o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Double o) {
		this(o.getClass(), o);
	}
	public Object(final java.math.BigInteger o) {
		this(o.getClass(), o);
	}
	public Object(final java.math.BigDecimal o) {
		this(o.getClass(), o);
	}
	public Object(final java.lang.Object o) {
		this(o.getClass(), o);
	}
	
	public dro.lang.Object type(final java.lang.Class<?> type) {
		this.type = type;
		return this;
	}
	public java.lang.Class<?> type() {
		return this.type;
	}
	
	public java.lang.Class<?> returns(final dro.lang.Class.Return r) {
		return this.c;
	}
	public java.lang.Object returns(final dro.lang.Object.Return r) {
		return dro.lang.Object.mapIdToRef.get(this.i);
	}
	
	public dro.lang.Object referenced(final dro.lang.Object by) {
		if (null == this.by) {
			this.by = new ArrayList<dro.lang.Object>();
		}
		this.by.add(by);
		return this;
	}
	public java.util.List<dro.lang.Object> referenced() {
		return this.by;
	}
	public dro.lang.Object dereferenced(final dro.lang.Object by) {
		if (null != this.by) {
			this.by.remove(by);
		}
		return this;
	}
	
	private dro.lang.Object references(final dro.lang.Object to, final boolean recurse) {
		if (null == this.to) {
			this.to = new ArrayList<dro.lang.Object>();
		}
		this.to.add(to);
		if (true == recurse) {
			to.referenced(this);
		}
		return this;
	}
	public dro.lang.Object references(final dro.lang.Object to) {
		return this.references(to, false);
	}
	public dro.lang.Object reference(final dro.lang.Object to) {
		return this.references(to, true);
	}
	public java.util.List<dro.lang.Object> references() {
		return this.to;
	}
	public dro.lang.Object dereference(final java.lang.Object to) {
		if (null != this.to) {
			this.to.remove(to);
		}
		return this;
	}
	
	@Override
	public int hashCode() {
	  //return super.hashCode();
		return this.i;
	}
	@Override
	public boolean equals(final java.lang.Object o) {
		return super.equals(o);
	}
}