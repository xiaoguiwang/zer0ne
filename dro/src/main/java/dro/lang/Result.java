package dro.lang;

public class Result {
	public static final Result UNKNOWN     = new Result();
	public static final Result UNCOUNTABLE = new Result();
	public static final Result TRUE        = new Result();
	public static final Result FALSE       = new Result();
	public static final Result UNDECIDABLE = new Result();
	
	public static class Return {
		public static final dro.lang.Result.Return Result = (dro.lang.Result.Return)null;
		public static final dro.lang.Number.Return Number = (dro.lang.Number.Return)null;
	}
	
	protected Result result;// = Result.UNKNOWN;
	
	{
	  //result = Result.UNKNOWN;
	}
	
	public Result(final Result result) {
		this.result = result;
	}
	public Result() {
		this.result = this;
	}
	
	public Result result(final Result result) {
		if (null != this.result) {
			this.result = result;
		} else {
			throw new IllegalStateException();
		}
		return this;
	}
	public Result result() {
		return this.result;
	}
}