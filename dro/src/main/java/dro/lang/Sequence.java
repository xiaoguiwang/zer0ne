package dro.lang;

public class Sequence {
	private int i;// = 1;
	{
	  //i = 1;
	}
	public Sequence() {
		this.i = 1;
	}
	public int next() {
		int i = this.i;
		this.i++;
		return i;
	}
}