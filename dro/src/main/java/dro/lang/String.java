package dro.lang;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dro.util.Properties;

public class String extends java.lang.Object {
	public static class Return extends dro.lang.Return {
		public static final String.Return String = (String.Return)null;
	}
	public static class Property {
		public static final java.lang.String String = dro.lang.Class.getName(java.lang.String.class);
	}
	
	private static final java.lang.String regex;
	private static final Pattern pattern;
	
	static {
		regex = "(.*?)(\\$\\{)(.*?)(\\})(.*)";
		pattern = Pattern.compile(regex, Pattern.DOTALL);
	}
	
	public static boolean isNull(final dro.lang.String value) {
		return null == value || null == value.value();
	}
	public static boolean isNull(final java.lang.String value) {
		return null == value;
	}
	public static boolean isNullOrBlank(final dro.lang.String value) {
		return String.isNull(value) || 0 == value.length();
	}
	public static boolean isNullOrBlank(final java.lang.String value) {
		return String.isNull(value) || 0 == value.length();
	}
	public static boolean isNullOrTrimmedBlank(final dro.lang.String value) {
		return String.isNull(value) || 0 == value.trim().length();
	}
	public static boolean isNullOrTrimmedBlank(final java.lang.String value) {
		return String.isNull(value) || 0 == value.trim().length();
	}
	
	public static java.lang.String[] getArray(java.lang.String value, java.lang.String delim) {
		java.lang.String[] array = null;
		if (null != value) {
			final StringTokenizer tokenizer = new StringTokenizer(value, delim);
			array = new java.lang.String[tokenizer.countTokens()];
			int countTokens = tokenizer.countTokens();
			for (int t = 0; t < countTokens; t++) {
				array[t] = tokenizer.nextToken();
			}
		} else {
			array = new java.lang.String[]{};
		}
		return array;
	}
	
	@Deprecated
	public static final java.lang.String __resolve(final java.lang.String s, final Properties p) {
		StringBuffer t = null;
		if (null != s) {
			t = new StringBuffer();
			java.lang.String __s = s;
			boolean found = true;
			long max = 1000L;
			for (long x = 0; x < max && true == found; x++) {
				found = false;
				Matcher m = pattern.matcher(__s);
				while (true == m.find()) {
					t.append(m.group(1)); // |..| ${ .. } ..
					found = true;
					java.lang.String k = m.group(3); // .. ${ |..| } ..
					// Be aware: the .hasProperty method in dro.util.Properties will resolve the property including out on file, in database, etc.
					if (null != p && true == p.hasProperty(k)) {
						t.append(p.getProperty(k));
					} else {
						t.append(m.group(2)) // .. |${| .. } ..
						 .append(k)
						 .append(m.group(4)); // .. ${ .. |}| ..
					}
					__s = m.group(5); // .. ${ .. } |..|
					m = pattern.matcher(__s);
				}
			}
			if (0 < __s.length()) {
				t.append(__s);
			}
		}
		return null == t ? null : t.toString();
	}
	public static final java.lang.String resolve(/*final */java.lang.String s, /*final */Properties p) {
		if (null != s) {
		  //String t = null;
			int state = 0;
			int from = 0, to = s.length();
			// TODO: could use a dro.Data with history and rollback (for pop)
		  /*final */dro.util.Stack<java.lang.Integer[]> stack = null;
			for (int i = from; i < to; i++) {
				char c = s.charAt(i);
				// TODO: could use a rule-set ($dro$data@Rule) in future-future
				if (c == '$') {
					state = 1;
				} else if (c == '{') {
					if (1 == state) {
						from = i;
						if (null == stack) {
							stack = new dro.util.Stack<java.lang.Integer[]>();
						}
						stack.push(new java.lang.Integer[]{0,from});
						state = 2;
					}
				} else if (c == '}') {
					if (2 == state || -2 == state) {
						final java.lang.Integer[] ia = stack.pop();
						state = ia[0];
						from = ia[1];
					  /*final */java.lang.String left = "";
						if (from > 0) {
							left = s.substring(0, from-1);
						}
					  /*final */java.lang.String center = s.substring(from+1, i);
						if (null != p) {
							if (null == p.getProperty(center)) {
								center = "${"+center+"}";
							} else {
								center = dro.lang.String.resolve(p.getProperty(center), p);
							}
						}
					  /*final */java.lang.String right = "";
						if (i+1 < s.length()) {
							right = s.substring(i+1, s.length());
						}
						s = left+center+right;
						i = from-1; // for-loop will ++ on next loop
						to = s.length();
					}
				}
			}
		}
		return s;
	}
	public static final java.lang.String resolve(final java.lang.String s) {
		return String.resolve(s, new Properties());
	}
	
	public static final java.lang.String[] unresolved(/*final */java.lang.String s, /*final */Properties p) {
	  /*final */java.util.List<java.lang.String> unresolved = null;
		if (null != s) {
		  //String t = null;
			int state = 0;
			int from = 0, to = s.length();
			// TODO: could use a dro.Data with history and rollback (for pop)
		  /*final */dro.util.Stack<java.lang.Integer[]> stack = null;
			for (int i = from; i < to; i++) {
				char c = s.charAt(i);
				// TODO: could use a rule-set ($dro$data@Rule) in future-future
				if (c == '$') {
					state = 1;
				} else if (c == '{') {
					if (1 == state) {
						from = i;
						state = 2;
						if (null == stack) {
							stack = new dro.util.Stack<java.lang.Integer[]>();
						}
						stack.push(new java.lang.Integer[]{state,from});
					}
				} else if (c == '}') {
					if (2 == state || -2 == state) {
						final java.lang.Integer[] ia = stack.pop();
						state = ia[0];
						from = ia[1];
					  /*final */java.lang.String left = "";
						if (from > 0) {
							left = s.substring(0, from-1);
						}
					  /*final */java.lang.String center = s.substring(from+1, i);
						if (null != p) {
							if (null == p.getProperty(center)) {
								if (null == unresolved) {
									unresolved = new java.util.ArrayList<java.lang.String>();
								}
								unresolved.add(center);
							} else {
								center = dro.lang.String.resolve(p.getProperty(center), p);
							}
						} else {
							if (null == unresolved) {
								unresolved = new java.util.ArrayList<java.lang.String>();
							}
							unresolved.add(center);
						}
					  /*final */java.lang.String right = "";
						if (i+1 < s.length()) {
							right = s.substring(i+1, s.length());
						}
						s = left+center+right;
						i = from-1; // for-loop will ++ on next loop
						to = s.length();
					}
				}
			}
		}
		return null == unresolved ? null : unresolved.toArray(new java.lang.String[0]);
	}
	
	public static final java.lang.String[] split(final java.lang.String s, final java.lang.String regex) {
		return s.split(regex);
	}
	public static final java.lang.String[] lines(final java.lang.String s, final java.lang.String regex) {
		return dro.lang.String.split(s, regex);
	}
	public static final java.lang.String[][] fields(final java.lang.String[] sa, final java.lang.String regex) {
		final java.lang.String[][] fields = new java.lang.String[sa.length][];
		for (int s = 0; s < sa.length; s++) {
			fields[s] = dro.lang.String.split(sa[s], regex);
		}
		return fields;
	}
	
	private final static java.lang.String choiceOfCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	
	public static final java.lang.String generateString(final int size) {
		final StringBuffer sb = new StringBuffer();
		final int length = choiceOfCharacters.length();
	  /*final */int p = -1, q = -1;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= 10; j++) {
				q = (int) (Math.random() * (double) length);
				if (q != p) {
					p = q;
				}
			}
			sb.append(choiceOfCharacters.charAt(p));
		}
		return null == sb ? "" : sb.toString();
	}

	private java.lang.String __String;// = null;
	
	{
	  //__String = null;
	}
	
	public String() {
	}
	public String(final java.lang.String __String) {
		this.__String = __String;
	}
	
	public String value(final java.lang.String __String) {
		this.__String = __String;
		return this;
	}
	public String value(final dro.lang.String string) {
		return this.value(null == string.toString() ? null : string.toString());
	}
	public java.lang.String value() {
		return this.__String;
	}
	
	public dro.lang.String trim() {
		return null == this.__String ? null : new dro.lang.String(this.__String.trim());
	}
	public int length() {
		return null == this.__String ? -1 : this.__String.length();
	}
	
	@Override
	public java.lang.String toString() {
		return this.__String.toString();
	}
}