package dro.main;

//import dro.util.State;

public class Context {
	protected final static dro.lang.Boolean done = new dro.lang.Boolean(false);
	
  //protected /*final */static State state = State.NONE;
	
	static {
	  //state = State.INITIALISING;
	}
	
	public static void start(final java.lang.String[] args) throws InterruptedException {
	  //state = State.RUNNING;
		while (false == done.value()) {
			Thread.sleep(1000L);
		}
	}
	public static void stop(final java.lang.String[] args) throws InterruptedException {
		done.value(true);
	  //state = State.SHUTDOWN;
		System.exit(0);
	}
	public static void main(final java.lang.String[] args) throws InterruptedException {
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (1 <= args.length && true == args[0].equals("start")) {
			Context.start(args);
		} else if (1 <= args.length && true == args[0].equals("stop")) {
			Context.stop(args);
		} else {
			Runtime.getRuntime().addShutdownHook(new Thread(){
				public void run() {
					done.value(true);
				  //state = State.SHUTDOWN;
				  //System.exit(0);
				}
			});
			while (false == done.value()) {
				Thread.sleep(1000L);
			}
			System.exit(0);
		}
	}
}
