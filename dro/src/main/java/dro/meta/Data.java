package dro.meta;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * [dro.meta.]Data has no value but does have 0:n data, 0:n [meta-data] rules, and 0:n [meta-data] fields
 * [meta-data] fields have [meta-data field] rules..
 * A meta-data field is the same type as a data field
 * A meta-data field rule is the same type as a data field rule
 */
//$id, $name, $version, $dro%Data, $dro$meta$data@Rule, $dro$meta$data%Field
public class Data extends dro.Data {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static class Return {
			public static final Set Set = (Set)null;
		}
	}
	public static class Map {
		public static class Return {
			public static final Map Map = (Map)null;
		}
	}
	
	public static class Return {
		public static final dro.meta.Data.Set.Return Set = (dro.meta.Data.Set.Return)null;
		public static final dro.meta.Data.Map.Return Map = (dro.meta.Data.Map.Return)null;
		public static final dro.meta.Data.Return Data = (dro.meta.Data.Return)null;
		public static final dro.meta.Data.List List = (dro.meta.Data.List)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Data.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	public Data() {
		super();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()");
		  /*final java.lang.Class<?> c = this.getClass();
			if (c == dro.meta.Data.class) {
				dro.data.Event.event((dro.data.Context) null, this, "new");
			}*/
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
	public Data(final java.lang.String name) {
		this();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()", new java.lang.Object[]{name});
			if (null != name) {
				zero.set("$name", name);
			} else {
				throw new IllegalArgumentException();
			}
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
	
	public dro.meta.data.Context returns(final dro.meta.data.Context.Return r) {
		return null == zero ? null : (dro.meta.data.Context) zero.get(dro.meta.data.Context.$);
	}	
	public java.util.Map<java.lang.Object, dro.meta.data.Field> returns(final dro.meta.data.Field.Map r) {
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, dro.meta.data.Field> map = (java.util.Map<java.lang.Object, dro.meta.data.Field>)zero.get("$dro$meta$data%Field");
		return map;
	}
	public java.util.Set<dro.meta.data.Field> returns(final dro.meta.data.Field.Set r) {
		final java.util.Map<java.lang.Object, dro.meta.data.Field> map = this.returns(dro.meta.data.Field.Return.Map);
		final java.util.Set<dro.meta.data.Field> set;// = null;
		if (null != map) {
			set = new LinkedHashSet<dro.meta.data.Field>(map.values());
		} else {
			set = new LinkedHashSet<dro.meta.data.Field>();
		}
		return set;
	}
	public dro.meta.data.Field returns(final java.lang.Object id, final dro.meta.data.Field.Return r) {
		final java.util.Map<java.lang.Object, dro.meta.data.Field> map = this.returns(dro.meta.data.Field.Map.Return.Map);
		return null == map ? null : (dro.meta.data.Field) map.get(id);
	}
	
	public java.util.Map<java.lang.Object, dro.Data> returns(final dro.Data.Map r) {
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, dro.Data> map = (java.util.Map<java.lang.Object, dro.Data>)zero.get("$dro%Data");
		return map;
	}
	public java.util.Set<dro.Data> returns(final dro.Data.Set r) {
		final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Return.Map);
		final java.util.Set<dro.Data> set;// = null;
		if (null != map) {
			set = new LinkedHashSet<dro.Data>(map.values());
		} else {
			set = new LinkedHashSet<dro.Data>();
		}
		return set;
	}
	public dro.Data returns(final java.lang.Object id, final dro.Data.Return r) {
		final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Map.Return.Map);
		return null == map ? null : (dro.Data) map.get(id);
	}
	
	public dro.meta.Data add(final dro.meta.data.Field mdf, final dro.meta.Data.Return r) {
		this.add(mdf);
		return this;
	}
	public dro.meta.data.Field add(final dro.meta.data.Field mdf, final dro.meta.data.Field.Return r) {
		this.add(mdf);
		return mdf;
	}
	public dro.meta.Data add(final dro.Data d, final dro.meta.Data.Return r) {
		this.add(d);
		return this;
	}
	public dro.Data add(final dro.Data d, final dro.Data.Return r) {
		this.add(d);
		return d;
	}
	@Override
	public Data add(final java.lang.Object o) {
		try {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.Data.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.meta.data.Field) {
				  /*final */java.util.Map<java.lang.Object, dro.meta.data.Field> map = this.returns(dro.meta.data.Field.Map.Return.Map);
					if (null == map) {
						map = new java.util.LinkedHashMap<java.lang.Object, dro.meta.data.Field>();
						zero.set("$dro$meta$data%Field", map);
					}
					final dro.meta.data.Field mdf = (dro.meta.data.Field)o;
					if (false == map.containsKey(mdf.id())) {
						map.put(mdf.id(), mdf);
						mdf.set(dro.meta.Data.$, this);
					}
				} else if (dro.Data.class == o.getClass()) {
				  /*final */java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Return.Map);
					if (null == map) {
						map = new java.util.LinkedHashMap<java.lang.Object, dro.Data>();
						zero.set("$dro%Data", map);
					}
					final dro.Data d = (dro.Data)o;
					if (false == map.containsKey(d.id())) {
						map.put(d.id(), d);
						d.set(dro.meta.Data.$, this);
					}
				} else if (true == o instanceof java.lang.String && ((String)o).startsWith("$")) {
					super.add(o);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "add");
			} else {
			}
		}
		return this;
	}
	
	@Override
	public Data set(final java.lang.Object k, final java.lang.Object v) {
		super.set(k, v);
		return this;
	}
	
  /*@Override
	public java.lang.Object get(final java.lang.Object k) {
		return super.get(k);
	}*/
	
	public dro.meta.data.Field remove(final dro.meta.data.Field mdf, final dro.meta.data.Field.Return r) {
	  /*final dro.meta.data.Field mdf = (dro.meta.data.Field)*/this.remove(mdf);
		return mdf;
	}
	// Note: given that removing by id does not specify type, we go with the type being the returns type
	public dro.meta.data.Field remove(final java.lang.Object id, final dro.meta.data.Field.Return r) {
		final dro.meta.data.Field mdf = (dro.meta.data.Field)this.remove(id);
		return mdf;
	}
	public dro.meta.Data remove(final dro.meta.data.Field mdf, final dro.meta.Data.Return r) {
	  /*final dro.meta.data.Field mdf = (dro.meta.data.Field)*/this.remove(mdf);
		return this;
	}
	public dro.Data remove(final dro.Data d, final dro.Data.Return r) {
	  /*final dro.Data d = (dro.Data)*/this.remove(d);
		return d;
	}
	// Note: given that removing by id does not specify type, we go with the type being the returns type
	public dro.Data remove(final java.lang.Object id, final dro.Data.Return r) {
		final dro.Data d = (dro.Data)this.remove(id);
		return d;
	}
	@Override
	public java.lang.Object remove(final java.lang.Object o) {
	  /*final */java.lang.Object remove = null;
	  /*final */boolean handled = false;
		try {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.Data.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.meta.data.Context) {
					zero.remove(dro.meta.data.Context.$id);
					zero.remove(dro.meta.data.Context.$);
					{
						final java.util.Set<dro.meta.data.Field> set = this.returns(dro.meta.data.Field.Set.Return.Set);
						for (final dro.meta.data.Field mdf: set) {
							this.remove(mdf);
						}
					}
					{
						final java.util.Set<dro.Data> set = this.returns(dro.Data.Set.Return.Set);
						for (final dro.Data d: set) {
							this.remove(d);
						}
					}
					final dro.meta.data.Context mdc = (dro.meta.data.Context)o;
					mdc.remove(this);
					handled = true;
				} else if (true == o instanceof dro.meta.data.Field) {
					final dro.meta.data.Field mdf = (dro.meta.data.Field)o;
					final java.util.Map<java.lang.Object, dro.meta.data.Field> map = this.returns(dro.meta.data.Field.Map.Return.Map);
					if (null != map) {
						if (true == map.containsKey(mdf.id())) {
							remove = map.remove(mdf.id());
							mdf.remove(this);
						}
					}
					handled = true;
				} else if (true == o instanceof dro.Data) {
					final dro.Data d = (dro.Data)o;
					final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Map.Return.Map);
					if (null != map) {
						if (true == map.containsKey(d.id())) {
							remove = map.remove(d.id());
							d.remove(this);
						}
					}
					handled = true;
				} else if (java.lang.Integer.class == o.getClass()) {
					final java.lang.Integer id = (java.lang.Integer)o;
					{
						final java.util.Map<java.lang.Object, dro.meta.data.Field> map = this.returns(dro.meta.data.Field.Map.Return.Map);
						if (null != map) {
							if (true == map.containsKey(id)) {
								final dro.meta.data.Field mdf = (dro.meta.data.Field)map.remove(id);
								mdf.remove(this);
							}
						}
					}
					{
						final java.util.Map<java.lang.Object, dro.Data> map = this.returns(dro.Data.Map.Return.Map);
						if (null != map) {
							if (true == map.containsKey(id)) {
								final dro.Data d = (dro.Data)map.remove(id);
								d.remove(this);
							}
						}
					}
					handled = true;
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.Data.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "remove");
			} else {
			}
		}
		return remove;
	}
	
	private abstract static class Expression {
	  //protected final java.lang.Object op;// = null;
		{
		  //op = null;
		}
		protected Expression(final java.lang.Object op) {
			// this.op = op;
		}
		private Expression() {
			this((java.lang.Object) null);
		}
		public abstract java.util.List<dro.Data> express(final dro.meta.Data md);
	}
	
	public static class Boolean {
		public static class Expression extends dro.meta.Data.Expression {
			public static final dro.meta.Data.Boolean.Expression EQ = new dro.meta.Data.Boolean.Expression("EQ") {
				@Override
				public java.util.List<dro.Data> express(final dro.meta.Data md) {
					throw new IllegalStateException();
				}
			};
			private /*final */java.lang.Object k;// = null;
			private dro.meta.Data.Expression e;// = null;
			private /*final */java.lang.Class<?> t;// = null;
			private /*final */java.lang.Object v;// = null;
			private Expression(final java.lang.Object op) {
				super(op);
			}
			public Expression(final java.lang.Object k, final dro.meta.Data.Expression e, final java.lang.String v) {
				super();
				this.k = k;
				this.e = e;
				this.t = java.lang.String.class;
				this.v = ((java.lang.String)v).replaceAll("\\*", ".*");
			}
			public Expression(final java.lang.Object k, final dro.meta.Data.Expression e, final java.lang.Integer v) {
				super();
				this.k = k;
				this.e = e;
				this.t = int.class;
				this.v = (Integer)v;
			}
			
			@Override
			public java.util.List<dro.Data> express(final dro.meta.Data md) {
				final java.util.List<dro.Data> dl = new ArrayList<>();
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (this.t == java.lang.String.class) {
					if (this.e == dro.meta.Data.Boolean.Expression.EQ) {
						final Pattern pattern = Pattern.compile((java.lang.String) this.v, Pattern.DOTALL);
					  /*final */java.util.Map<java.lang.Object, dro.Data> map = md.returns(dro.Data.Return.Map);
						for (final java.lang.Object id : map.keySet()) {
							final dro.Data d = (dro.Data) map.get(id);
							final java.lang.String v = (java.lang.String)d.get(this.k); // FIXME
							if (null != v) {
								final Matcher m = pattern.matcher(v);
								if (true == m.find()) {
									dl.add(d);
								}
							}
						}
					}
				} else if (this.t == int.class) {
					if (this.e == dro.meta.Data.Boolean.Expression.EQ) {
					  /*final */java.util.Map<java.lang.Object, dro.Data> map = md.returns(dro.Data.Return.Map);
						for (final java.lang.Object id : map.keySet()) {
							final dro.Data d = (dro.Data) map.get(id);
							final Integer v = (java.lang.Integer)d.get(this.k); // FIXME
							if (null != v) {
								if (true == v.equals(this.v)) {
									dl.add(d);
								}
							}
						}
					}
				} else {
					throw new IllegalArgumentException();
				}
				return dl;
			}
		}
	}

	public java.util.List<dro.Data> get(final dro.meta.Data.Boolean.Expression e) {
	  /*final */java.util.List<dro.Data> dl = null;
		try {
		  //dro.data.Event.entering((dro.data.Context)null, this, "get", new java.lang.Object[]{e});
			dl = e.express(this);
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "get");
		}
		return dl;
	}
}