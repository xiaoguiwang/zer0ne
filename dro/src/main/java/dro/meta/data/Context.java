package dro.meta.data;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class Context extends dro.data.Context {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static class Return {
			public static final Set Set = (Set)null;
		}
	}
	public static class Map {
		public static class Return {
			public static final Map Map = (Map)null;
		}
	}
	
	public static class Return {
		public static final dro.meta.data.Context.Set.Return Set = (dro.meta.data.Context.Set.Return)null;
		public static final dro.meta.data.Context.Map.Return Map = (dro.meta.data.Context.Map.Return)null;
		public static final dro.meta.Data.Return Data = (dro.meta.Data.Return)null;
		public static class Meta {
			public static final dro.meta.Data.Return Data = (dro.meta.Data.Return)null;
		}
		public static final dro.meta.data.Context.Return Context = (dro.meta.data.Context.Return)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Context.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	protected static java.util.Map<java.lang.Object, dro.meta.data.Context> map;// = null;
	
	static {
		map = new LinkedHashMap<java.lang.Object, dro.meta.data.Context>();
	}
	
	public static java.util.Map<java.lang.Object, dro.meta.data.Context> returns(final dro.meta.data.Context.Map r) {
		return map;
	}
	public static java.util.Set<dro.meta.data.Context> returns(final dro.meta.data.Context.Set r) {
		final java.util.Map<java.lang.Object, dro.meta.data.Context> map = dro.meta.data.Context.returns(dro.meta.data.Context.Map.Return.Map);
		final java.util.Set<dro.meta.data.Context> set;// = null;
		if (null != map) {
			set = new LinkedHashSet<dro.meta.data.Context>(map.values());
		} else {
			set = new LinkedHashSet<dro.meta.data.Context>();
		}
		return set;
	}
	public static dro.meta.data.Context returns(final java.lang.Object id, final dro.meta.data.Context.Return r) {
		return null == map ? null : (dro.meta.data.Context)map.get(id);
	}
	
	public static dro.meta.data.Context add(final dro.meta.data.Context mdc, final dro.meta.data.Context.Return r) {
		map.put(mdc.id(), mdc);
		return mdc;
	}
	public static dro.meta.data.Context remove(final java.lang.Object id, final dro.meta.data.Context.Return r) {
		final dro.meta.data.Context mdc = map.remove(id);
		return mdc;
	}
	public static dro.meta.data.Context remove(final dro.meta.data.Context mdc, final dro.meta.data.Context.Return r) {
	  /*final dro.meta.data.Context mdc = */map.remove(mdc.id());
		return mdc;
	}
	
	public Context() {
		super(); // implicit super constructor..
	}
	public Context(final java.lang.String name) {
		this();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()", new java.lang.Object[]{name});
			if (null != name) {
				zero.set("$name", name);
			} else {
				throw new IllegalArgumentException();
			}
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
	
	public java.util.Map<java.lang.Object, dro.data.Context> returns(final dro.data.Context.Map r) {
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, dro.data.Context> map = (java.util.Map<java.lang.Object, dro.data.Context>)zero.get("$dro$data%Context");
		return map;
	}
	public java.util.Set<dro.data.Context> returns(final dro.data.Context.Set r) {
		final java.util.Map<java.lang.Object, dro.data.Context> map = this.returns(dro.data.Context.Map.Return.Map);
		final java.util.Set<dro.data.Context> set;// = null;
		if (null != map) {
			set = new java.util.LinkedHashSet<dro.data.Context>(map.values());
		} else {
			set = new java.util.LinkedHashSet<dro.data.Context>();
		}
		return set;
	}
	public dro.data.Context returns(final java.lang.Object id, final dro.data.Context.Return r) {
		final java.util.Map<java.lang.Object, dro.data.Context> map = this.returns(dro.data.Context.Map.Return.Map);
		return null == map ? null : (dro.data.Context)map.get(id);
	}
	
	public java.util.Map<java.lang.Object, dro.meta.Data> returns(final dro.meta.Data.Map r) {
		@SuppressWarnings("unchecked")
		final java.util.Map<java.lang.Object, dro.meta.Data> map = (java.util.Map<java.lang.Object, dro.meta.Data>)zero.get("$dro$meta%Data");
		return map;
	}
	public java.util.Set<dro.meta.Data> returns(final dro.meta.Data.Set r) {
		final java.util.Map<java.lang.Object, dro.meta.Data> map = this.returns(dro.meta.Data.Map.Return.Map);
		final java.util.Set<dro.meta.Data> set;// = null;
		if (null != map) {
			set = new java.util.LinkedHashSet<dro.meta.Data>(map.values());
		} else {
			set = new java.util.LinkedHashSet<dro.meta.Data>();
		}
		return set;
	}
	public dro.meta.Data returns(final java.lang.Object id, final dro.meta.Data.Return r) {
		final java.util.Map<java.lang.Object, dro.meta.Data> map = this.returns(dro.meta.Data.Map.Return.Map);
		return null == map ? null : (dro.meta.Data)map.get(id);
	}
	
	public dro.meta.data.Context add(final dro.data.Context dc, final dro.meta.data.Context.Return r) {
		this.add(dc);
		return this;
	}
	public dro.data.Context add(final dro.data.Context dc, final dro.data.Context.Return r) {
		this.add(dc);
		return dc;
	}
	public dro.meta.data.Context add(final dro.meta.Data md, final dro.meta.data.Context.Return r) {
		this.add(md);
		return this;
	}
	public dro.meta.Data add(final dro.meta.Data md, final dro.meta.Data.Return r) {
		this.add(md);
		return md;
	}
	
	@Override
	public Context add(final java.lang.Object o) {
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Context.class == this.getClass()) {
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.data.Context) {
				  //dro.data.Event.entering(dro.meta.data.Context)null, this, "add");
				  /*final */java.util.Map<java.lang.Object, dro.data.Context> map = this.returns(dro.data.Context.Map.Return.Map);
					if (null == map) {
						map = new LinkedHashMap<java.lang.Object, dro.data.Context>();
						zero.set("$dro$data%Context", map);
					}
					final dro.data.Context dc = (dro.data.Context)o;
					if (false == map.containsKey(dc.id())) {
						map.put(dc.id(), dc);
						dc.set(dro.meta.data.Context.$, this);
					}
				} else if (true == o instanceof dro.meta.Data) {
				  //dro.data.Event.entering(dro.meta.data.Context)null, this, "add");
				  /*final */java.util.Map<java.lang.Object, dro.meta.Data> map = this.returns(dro.meta.Data.Map.Return.Map);
					if (null == map) {
						map = new LinkedHashMap<java.lang.Object, dro.meta.Data>();
						zero.set("$dro$meta%Data", map);
					}
					final dro.meta.Data md = (dro.meta.Data)o;
					if (false == map.containsKey(md.id())) {
						map.put(md.id(), md);
						md.set(dro.meta.data.Context.$, this);
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.data.Context.class == o.getClass()) {
			  //dro.data.Event.exiting((dro.meta.data.Context)null, this, "add");
			} else if (dro.meta.Data.class == o.getClass()) {
			  //dro.data.Event.exiting((dro.meta.data.Context)null, this, "add");
			} else {
			}
		}
		return this;
	}
	
	@Override
	public Context set(final java.lang.Object k, final java.lang.Object v) {
		super.set(k, v);
		return this;
	}
	
	public dro.data.Context remove(final dro.data.Context dc, final dro.data.Context.Return r) {
	  /*final dro.data.Context dc = (dro.data.context)*/this.remove(dc);
		return dc;
	}
	public dro.data.Context remove(final java.lang.Object id, final dro.data.Context.Return r) {
		final dro.data.Context dc = (dro.data.Context)this.remove(id);
		return dc;
	}
	public dro.meta.Data remove(final dro.meta.Data md, final dro.meta.Data.Return r) {
	  /*final dro.meta.Data md = (dro.meta.Data)*/this.remove(md);
		return md;
	}
	public dro.meta.Data remove(final java.lang.Object id, final dro.meta.Data.Return r) {
		final dro.meta.Data md = (dro.meta.Data)this.remove(id);
		return md;
	}
	@Override
	public java.lang.Object remove(final java.lang.Object o) {
	  /*final */java.lang.Object remove = null;
	  /*final */boolean handled = false;
		try {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Context.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.data.Context) {
					final dro.data.Context dc = (dro.data.Context)o;
					final java.util.Map<java.lang.Object, dro.data.Context> map = this.returns(dro.data.Context.Map.Return.Map);
					if (null != map) {
						if (true == map.containsKey(dc.id())) {
							remove = map.remove(dc.id());
							dc.remove(this);
						}
					}
					handled = true;
				} else if (true == o instanceof dro.meta.Data) {
					final dro.meta.Data md = (dro.meta.Data)o;
					final java.util.Map<java.lang.Object, dro.meta.Data> map = this.returns(dro.meta.Data.Map.Return.Map);
					if (null != map) {
						if (true == map.containsKey(md.id())) {
							remove = map.remove(md.id());
							md.remove(this);
						}
					}
					handled = true;
				} else if (java.lang.Integer.class == o.getClass()) {
					final java.lang.Integer id = (java.lang.Integer)o;
					{
						final java.util.Map<java.lang.Object, dro.data.Context> map = this.returns(dro.data.Context.Map.Return.Map);
						if (null != map) {
							if (true == map.containsKey(id)) {
								final dro.data.Context dc = (dro.data.Context)map.remove(id);
								dc.remove(this);
							}
						}
					}
					{
						final java.util.Map<java.lang.Object, dro.meta.Data> map = this.returns(dro.meta.Data.Map.Return.Map);
						if (null != map) {
							if (true == map.containsKey(id)) {
								final dro.meta.Data md = (dro.meta.Data)map.remove(id);
								md.remove(this);
							}
						}
					}
					handled = true;
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Context.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "remove");
			} else {
			}
		}
		return remove;
	}
}