package dro.meta.data;

//$id, $name, $dro$meta$data$field@Rule
public class Field extends dro.data.Field {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static class Return {
			public static final Set Set = (Set)null;
		}
	}
	public static class Map {
		public static class Return {
			public static final Map Map = (Map)null;
		}
	}
	
	public static class Return {
		public static final dro.meta.data.Field.Return Field = (dro.meta.data.Field.Return)null;
		public static final dro.meta.data.Field.List List = (dro.meta.data.Field.List)null;
		public static final dro.meta.data.Field.Set Set = (dro.meta.data.Field.Set)null;
		public static final dro.meta.data.Field.Map Map = (dro.meta.data.Field.Map)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Field.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	public Field() {
		super();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()");
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
	public Field(final java.lang.String name) {
		this();
		try {
			zero.set("$name", name);
		} finally {
		}
	}
	
	public dro.meta.data.Field returns(final dro.meta.data.Field.Return r) {
		return this;
	}
	public java.util.Set<dro.meta.data.field.Rule> returns(final dro.meta.data.field.Rule.Set r) {
		@SuppressWarnings("unchecked")
		final java.util.Set<dro.meta.data.field.Rule> set = (java.util.Set<dro.meta.data.field.Rule>)zero.get("$dro$meta$data$field@Rule");
		return set;
	}
	
	@Override
	public dro.meta.data.Field set(final java.lang.Object k, final java.lang.Object o) {
		super.set(k, o);
		return this;//.returns(dro.meta.data.Field.Return.Field);
	}
	
  //$id, $dro$meta$data$field@Rule
	@Override
	public Field add(final java.lang.Object o) {
		try {
			if (false == Boolean.TRUE.booleanValue()) {
		  //} else if (dro.meta.data.field.Rule.class == this.getClass()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "add", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
			  /*} else if (true == o instanceof dro.meta.data.field.Rule) {
				*//*final *//*java.util.Set<dro.meta.data.field.Rule> set = this.returns(dro.meta.data.field.Rule.Return.Set);
					if (null == set) {
						set = new LinkedHashSet<dro.meta.data.field.Rule>();
						zero.set("$dro$meta$data$field@Rule", set);
					}
					final dro.meta.data.field.Rule mdfr = (dro.meta.data.field.Rule)o;
					set.add(mdfr);
					handled = true;*/
				} else if (true == o instanceof java.lang.Object) {
					throw new UnsupportedOperationException();
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "add");
			} else {
			}
		}
		return this;
	}
	
	public java.lang.Object remove(final java.lang.Object o) {
	  /*final */java.lang.Object remove = null;
	  /*final */boolean handled = false;
		try {
			if (false == Boolean.TRUE.booleanValue()) {
		  //} else if (dro.meta.data.field.Rule.class == this.getClass()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.entered((dro.data.Context)null, this, "remove", new java.lang.Object[]{o});
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == o instanceof dro.meta.Data) {
					zero.remove(dro.meta.Data.$id);
					zero.remove(dro.meta.Data.$);
					final dro.meta.Data md = (dro.meta.Data)o;
					md.remove(this);
					handled = true;
			  /*} else if (true == o instanceof dro.meta.data.field.Rule) {
					final java.util.Set<dro.meta.data.field.Rule> set = this.returns(dro.meta.data.field.Rule.Return.Set);
					if (null != set) {
						final dro.meta.data.field.Rule mdfr = (dro.meta.data.field.Rule)o;
						set.remove(mdfr);
						remove = o;
						if (0 == set.size()) {
							zero.remove("$dro$meta$data$field@Rule");
						}
					}
					handled = true;*/
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalStateException();
			}
		} finally {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (dro.meta.data.Field.class == this.getClass()) {
			  //dro.data.Event.exiting((dro.data.Context)null, this, "remove");
			} else {
			}
		}
		return remove;
	}
}