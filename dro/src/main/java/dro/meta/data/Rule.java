package dro.meta.data;

public class Rule extends dro.data.Rule {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static final Set Return = (Set)null;
	}
	
	public static class Return {
		public static final dro.meta.data.Rule.Return Rule = (dro.meta.data.Rule.Return)null;
		public static final dro.meta.data.Rule.List List = (dro.meta.data.Rule.List)null;
		public static final dro.meta.data.Rule.Set Set = (dro.meta.data.Rule.Set)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Rule.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
	public Rule() {
	  //super(); // implicit super constructor..
		try {
			zero.set("$id", this.hashCode());
		} finally {
		}
	}
	public Rule(final dro.meta.Data md) {
		this();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()", new java.lang.Object[]{md});
			if (null != md) {
				this.__Rule(md);
			} else {
				throw new IllegalStateException();
			}
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
  /*protected */void __Rule(final dro.meta.Data md) {
		zero.set("$dro$meta$Data", md);
	}
	
	public dro.meta.data.Rule returns(final dro.meta.data.Rule.Return r) {
		return this;
	}
	
	public dro.meta.Data returns(final dro.meta.Data.Return r) {
		return (dro.meta.Data)zero.get("$dro$meta$Data");
	}
	
	@Override
	public dro.meta.data.Rule set(final java.lang.Object k, final java.lang.Object o) {
		super.set(k, o);
		return this;//.returns(dro.meta.data.Rule.Return.Rule);
	}
  /*@Override
	public java.lang.Object get(final java.lang.Object key) {
		return super.get(key);
	}*/
}