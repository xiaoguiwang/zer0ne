package dro.meta.data.field;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import dro.lang.Result;

public class Rule extends dro.data.field.Rule {
	public static class List {
		public static final List Return = (List)null;
	}
	public static class Set {
		public static final Set Return = (Set)null;
	}
	
	public static class Return {
		public static final dro.meta.data.field.Rule.Return Rule = (dro.meta.data.field.Rule.Return)null;
		public static final dro.meta.data.field.Rule.List List = (dro.meta.data.field.Rule.List)null;
		public static final dro.meta.data.field.Rule.Set Set = (dro.meta.data.field.Rule.Set)null;
	}
	
	public static final java.lang.String $ = "$"+String.join("$", Rule.class.getName().split("\\."));
	public static final java.lang.String $id = $+"#id";
	
  /*public static dro.meta.data.field.Rule returns(final java.lang.Object id, final dro.meta.data.field.Rule.Return r) {
		return (dro.meta.data.field.Rule)rules.get(id);
	}*/
	public static class Compare extends dro.meta.data.field.Rule {
		public static class Return {
			public static final dro.meta.data.field.Rule.Compare.Return Compare = (dro.meta.data.field.Rule.Compare.Return)null;
		}
		
		protected static final java.util.Map<java.lang.Object, dro.meta.data.field.Rule.Compare> compares = new HashMap<>();
		
		public static dro.meta.data.field.Rule.Compare get(final java.lang.Object id, final dro.meta.data.field.Rule.Compare.Return r) {
			return (dro.meta.data.field.Rule.Compare)dro.meta.data.field.Rule.Compare.compares.get(id);
		}
		
		static {
			compares.put("$compare-number", new Compare(){
				@Override
				public Result compare(final java.lang.Object o1, final java.lang.String op, final java.lang.Object o2) {
				  /*final */Result truth = Result.UNKNOWN;
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == o1 instanceof java.lang.Short) {
						if (true == o2 instanceof java.lang.Short) {
							return this.compare(((java.lang.Short)o1).shortValue(), op, ((java.lang.Short)o2).shortValue());
						} else {
							throw new IllegalArgumentException();
						}
					} else if (true == o1 instanceof java.lang.Integer) {
						if (true == o2 instanceof java.lang.Integer) {
							return this.compare(((java.lang.Integer)o1).intValue(), op, ((java.lang.Integer)o2).intValue());
						} else {
							throw new IllegalArgumentException();
						}
					} else if (true == o1 instanceof java.lang.Long) {
						if (true == o2 instanceof java.lang.Long) {
							return this.compare(((java.lang.Long)o1).longValue(), op, ((java.lang.Long)o2).longValue());
						} else {
							throw new IllegalArgumentException();
						}
					} else if (true == o1 instanceof java.lang.Float) {
						if (true == o2 instanceof java.lang.Float) {
							return this.compare(((java.lang.Float)o1).floatValue(), op, ((java.lang.Float)o2).floatValue());
						} else {
							throw new IllegalArgumentException();
						}
					} else if (true == o1 instanceof java.lang.Double) {
						if (true == o2 instanceof java.lang.Double) {
							return this.compare(((java.lang.Double)o1).doubleValue(), op, ((java.lang.Double)o2).doubleValue());
						} else {
							throw new IllegalArgumentException();
						}
					} else if (true == o1 instanceof java.math.BigInteger) {
						if (true == o2 instanceof java.math.BigInteger) {
							return this.compare((java.math.BigInteger)o1, op, (java.math.BigInteger)o2);
						} else {
							throw new IllegalArgumentException();
						}
					} else if (true == o1 instanceof java.math.BigDecimal) {
						if (true == o2 instanceof java.math.BigDecimal) {
							return this.compare((java.math.BigDecimal)o1, op, (java.math.BigDecimal)o2);
						} else {
							throw new IllegalArgumentException();
						}
					} else {
						throw new IllegalArgumentException();
					}
					return truth;
				}
			});
		}
		
		public Compare() {
		}
		public Compare(final java.lang.String c, final java.lang.String cop, final java.lang.String cof) {
			this.set("$compare", c);
			this.set("$compare-op", cop);
			this.set("$compare-field", cof);
		}
		
		protected Result compare(final short a, final java.lang.String op, final short b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a <  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (true == (a <= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a == b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (true == (a != b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a >= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (true == (a >  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		protected Result compare(final int a, final java.lang.String op, final int b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a <  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (true == (a <= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a == b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (true == (a != b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a >= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (true == (a >  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		protected Result compare(final long a, final java.lang.String op, final long b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a <  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (true == (a <= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a == b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (true == (a != b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a >= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (true == (a >  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		protected Result compare(final float a, final java.lang.String op, final float b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a <  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (true == (a <= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a == b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (true == (a != b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a >= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (true == (a >  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		protected Result compare(final double a, final java.lang.String op, final double b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a <  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (true == (a <= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a == b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (true == (a != b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a >= b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (true == (a >  b)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		protected Result compare(final BigInteger a, final java.lang.String op, final BigInteger b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a.compareTo(b) <  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (false == (a.compareTo(b) >  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a.compareTo(b) == 0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (false == (a.compareTo(b) == 0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a.compareTo(b) >  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (false == (a.compareTo(b) <  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		protected Result compare(final BigDecimal a, final java.lang.String op, final BigDecimal b) {
		  /*final */Result truth = Result.UNKNOWN;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == "LT".equals(op)) {
				if (true == (a.compareTo(b) <  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "LE".equals(op)) {
				if (false == (a.compareTo(b) >  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "EQ".equals(op)) {
				if (true == (a.compareTo(b) == 0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "NE".equals(op)) {
				if (false == (a.compareTo(b) == 0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GE".equals(op)) {
				if (true == (a.compareTo(b) >  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else if (true == "GT".equals(op)) {
				if (false == (a.compareTo(b) <  0)) {
					truth = Result.TRUE;
				} else {
					truth = Result.FALSE;
				}
			} else {
				throw new IllegalStateException();
			}
			return truth;
		}
		@Override
		public Result apply(final java.lang.Object[] oa) {
		  /*final */Result result = Result.UNKNOWN;
			if (null == oa) {
				result = Result.UNDECIDABLE;
			} else if (2 >= oa.length) {
				result = Result.UNDECIDABLE;
			} else if (3 == oa.length) {
				if (true == oa[0] instanceof dro.Data) {
				  //final dro.Data d = (dro.Data)oa[0];
					if (true == oa[1] instanceof dro.data.Field) {
						final dro.data.Field df = (dro.data.Field)oa[1];
						final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
						result = Rule.Compare.get(this.get("$compare"), dro.meta.data.field.Rule.Compare.Return.Compare).compare(oa[2], (java.lang.String)this.get("$compare-op"), mdf.get(this.get("$compare-field")));
					} else {
						result = Result.FALSE;
					}
				} else {
					result = Result.FALSE;
				}
			} else {
				result = Result.UNDECIDABLE;
			}
			return result; // "The Truth will out"
		}
		
		public Result compare(final java.lang.Object a, final java.lang.String op, final java.lang.Object b) {
			return Result.UNKNOWN;
		}
	}
	
	static {
	  /*rules.put("$rule-type-check", new Rule(){
			@Override
			public Result apply(final java.lang.Object[] oa) {
			*//*final *//*Result result = Result.UNKNOWN;
				if (null == oa) {
					result = Result.UNDECIDABLE;
				} else if (2 >= oa.length) {
					result = Result.UNDECIDABLE;
				} else if (3 == oa.length) {
					if (true == oa[0] instanceof dro.Data) {
					  //final dro.Data d = (dro.Data)oa[0];
						if (true == oa[1] instanceof dro.data.Field) {
							final dro.data.Field df = (dro.data.Field)oa[1];
							final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
							if (oa[2].getClass() == mdf.get("$type")) {
								result = Result.TRUE;
							} else {
								result = Result.FALSE;
							}
						} else {
							result = Result.UNDECIDABLE;
						}
					} else {
						result = Result.UNDECIDABLE;
					}
				} else {
					result = Result.UNDECIDABLE;
				}
				return result;
			}
		});*/
	  /*rules.put("$rule-number-check", new Rule(){
			@Override
			public Result apply(final java.lang.Object[] oa) {
			*//*final *//*Result result = Result.UNKNOWN;
				if (null == oa) {
					result = Result.UNDECIDABLE;
				} else if (2 >= oa.length) {
					result = Result.UNDECIDABLE;
				} else if (3 == oa.length) {
					result = Rule.Compare
						.get("$compare-number", dro.meta.data.field.Rule.Compare.Return.Compare)
						.compare(oa[0], (java.lang.String)oa[1], oa[2])
					;
				} else {
					result = Result.UNDECIDABLE;
				}
				return result;
			}
		});*/
	  /*rules.put("$rule-text-check", new Rule(){
			@Override
			public Result apply(final java.lang.Object[] oa) {
			*//*final *//*Result result = Result.UNKNOWN;
				if (null == oa) {
					result = Result.UNDECIDABLE;
				} else if (2 >= oa.length) {
					result = Result.UNDECIDABLE;
				} else if (3 == oa.length) {
					if (true == oa[0] instanceof java.lang.String) {
						if (true == oa[1] instanceof java.lang.String) {
							final java.lang.String op = (java.lang.String)oa[1]; 
							if (true == oa[2] instanceof java.lang.String) {
								final java.lang.String s1 = (java.lang.String)oa[0];
								final java.lang.String s2 = (java.lang.String)oa[2];
								if (false == Boolean.TRUE) {
								} else if (true == "equals".equals(op)) {
									if (true == s1.equals(s2)) {
										result = Result.TRUE;
									} else {
										result = Result.FALSE;
									}
								} else if (true == "starts-with".equals(op)) {
									if (true == s1.startsWith(s2)) {
										result = Result.TRUE;
									} else {
										result = Result.FALSE;
									}
								} else if (true == "ends-with".equals(op)) {
									if (true == s1.endsWith(s2)) {
										result = Result.TRUE;
									} else {
										result = Result.FALSE;
									}
								} else if (true == "contains".equals(op)) {
									if (true == s1.contains(s2)) {
										result = Result.TRUE;
									} else {
										result = Result.FALSE;
									}
								} else {
									throw new IllegalArgumentException();
								}
							} else {
								result = Result.UNDECIDABLE;
							}
						} else {
							result = Result.UNDECIDABLE;
						}
					} else {
						result = Result.UNDECIDABLE;
					}
				} else {
					result = Result.UNDECIDABLE;
				}
				return result;
			}
			@Override
			public dro.lang.Number apply(final java.lang.Object[] oa, final dro.lang.Number.Return r) {
			*//*final *//*dro.lang.Number number = dro.lang.Number.UNDEFINED;
				if (null == oa) {
					number = dro.lang.Number.UNDEFINED; // Undecidable?
				} else if (2 >= oa.length) {
					number = dro.lang.Number.UNDEFINED; // Undecidable?
				} else if (3 == oa.length) {
					if (true == oa[0] instanceof java.lang.String) {
						if (true == oa[1] instanceof java.lang.String) {
							final java.lang.String op = (java.lang.String)oa[1]; 
							if (true == oa[2] instanceof java.lang.String) {
								final java.lang.String s1 = (java.lang.String)oa[0];
								final java.lang.String s2 = (java.lang.String)oa[2];
								if (false == Boolean.TRUE) {
								} else if (true == "index-of".equals(op)) {
									number = new dro.lang.Number(s1.indexOf(s2));
								} else {
									throw new IllegalArgumentException();
								}
							} else {
								number = dro.lang.Number.UNDEFINED; // Undecidable?
							}
						} else {
							number = dro.lang.Number.UNDEFINED; // Undecidable?
						}
					} else {
						number = dro.lang.Number.UNDEFINED; // Undecidable?
					}
				} else {
					number = dro.lang.Number.UNDEFINED; // Undecidable?
				}
				return number;
			}
		});*/
	}
	
	public Rule() {
	  //super(); // implicit super constructor..
		try {
			zero.set("$id", this.hashCode());
		} finally {
		}
	}
	public Rule(final dro.meta.data.Field mdf) {
		this();
		try {
		  //dro.data.Event.entered((dro.data.Context)null, this, "()", new java.lang.Object[]{md});
			if (null != mdf) {
				this.__Rule(mdf);
			} else {
				throw new IllegalStateException();
			}
		} finally {
		  //dro.data.Event.exiting((dro.data.Context)null, this, "()");
		}
	}
  /*protected */void __Rule(final dro.meta.data.Field mdf) {
		zero.set("$dro$meta$data$Field", mdf);
	}
	
	public dro.meta.data.field.Rule returns(final dro.meta.data.field.Rule.Return r) {
		return this;
	}
	
	public dro.meta.data.Field returns(final dro.meta.data.Field.Return r) {
		return (dro.meta.data.Field)zero.get("$dro$meta$data$Field");
	}
	
	@Override
	public dro.meta.data.field.Rule set(final java.lang.Object k, final java.lang.Object o) {
		super.set(k, o);
		return this;//.returns(dro.meta.data.field.Rule.Return.Rule);
	}
  /*@Override
	public java.lang.Object get(final java.lang.Object key) {
		return super.get(key);
	}*/
}