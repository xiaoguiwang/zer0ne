package dro.util;

public class HashMap<K, V> extends java.util.HashMap<K, V> implements dro.util.Map<K, V> {
	private static final long serialVersionUID = 0L;
	
	public final dro.util.Id id;
	{
		id = new dro.util.Id(this);
	}
	@Override
	public java.lang.Object id() {
		return id;
	}
	
	@Override
	public V put(final K k, final V v) {
	  /*final */V w = null;
		w = super.put(k, v);
		return w;
	}
	
	@Override
	public V get(final java.lang.Object k) {
	  /*final */V w = null;
		w = super.get(k);
		return w;
	}
	
	@Override
	public boolean remove(final java.lang.Object k, final java.lang.Object v) {
	  /*final */boolean removed = false;
		removed = super.remove(k, v);
		return removed;
	}
	@Override
	public V remove(final java.lang.Object k) {
	  /*final */V w = null;
		w = super.remove(k);
		return w;
	}
}