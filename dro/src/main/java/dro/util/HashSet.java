package dro.util;

public class HashSet<E> extends java.util.HashSet<E> implements dro.util.Set<E> {
	private static final long serialVersionUID = 0L;
	
	public final dro.util.Id id;
	{
		id = new dro.util.Id(this);
	}
	@Override
	public java.lang.Object id() {
		return id;
	}
	
	@Override
	public boolean add(final E o) {
	  /*final */boolean added = false;
		added = super.add(o);
		return added;
	}
	
	@Override
	public boolean remove(final java.lang.Object o) {
	  /*final */boolean removed = false;
		removed = super.remove(o);
		return removed;
	}
}