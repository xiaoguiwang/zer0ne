package dro.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;

import dro.lang.Event;
import dro.lang.Event.Listener;

// https://www.logicbig.com/tutorials/core-java-tutorial/logging/loading-properties.html
public class Logger extends java.util.logging.Logger {
	private static final String javaUtilLoggingConfigFile = "java.util.logging.config.file";
	/*
	 * Apparently, according to:
	 * 
	 * http://logging.apache.org/log4j/2.x/manual/jmx.html
	 * 
	 * System.setProperty("log4j2.disableJmx", Boolean.TRUE.toString());
	 * 
	 * should address (prevent) the following System.err (but it does not) -
	 * 
	 * log4j:WARN No appenders could be found for logger (org.apache.activemq.broker.jmx.ManagementContext).
	 * log4j:WARN Please initialize the log4j system properly.
	 * log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
	 * 
	 * But then again, having a JMX console could be useful going forward..
	 */
	private static InputStream propertiesAreOutsideOfArchive(final String fileName) {
		InputStream is = null;
		try {
			final String canonicalPath = new File(fileName).getCanonicalPath();
			is = new FileInputStream(new File(canonicalPath));
		} catch (final FileNotFoundException e) {
		  //e.printStackTrace(System.err);
		} catch (final IOException e) {
		  //e.printStackTrace(System.err);
		}
	  /*if (null != is) {
			try {
				new java.util.Properties().load(is);
			} catch (final IOException e) {
				is = null;
				e.printStackTrace(System.err);
			}
		}*/
		return is;
	}
	private static InputStream propertiesAreInsideOfArchive(final String fileName) {
		InputStream is = null;
		if (false == fileName.matches("[\\\\\\/]")) {
			is = ClassLoader.getSystemResourceAsStream(fileName);
		}
	  /*if (null != is) {
			try {
				new java.util.Properties().load(is);
			} catch (final IOException e) {
				is = null;
				e.printStackTrace(System.err);
			}
		}*/
		return is;
	}
	
	private static final Event.Listener el;// = null;
	
	static {
		LogManager.getLogManager().reset();
		
	  /*final */InputStream is = null;
	  /*final */String propertyValue = null;
		
		if (null == is) {
			propertyValue = System.getProperty(javaUtilLoggingConfigFile);
			if (null != propertyValue) {
				is = Logger.propertiesAreOutsideOfArchive(propertyValue);
			}
		}
		if (null == is) {
			is = Logger.propertiesAreOutsideOfArchive("logging.properties");
		}
		if (null == is) {
			is = Logger.propertiesAreInsideOfArchive("logging.properties");
		}
		if (null != is) {
			try {
				LogManager.getLogManager().readConfiguration(is);
			} catch (final IOException e) {
				e.printStackTrace(System.err);
			}
		}
		if (null != is) {
			try {
				is.close();
			} catch (final IOException e) {
				e.printStackTrace(System.err);
			}
			is = null;
		}
		el = new Event.Listener(){
			@Override
			public Listener dispatch(final dro.lang.Event e) {
				final java.lang.Class<?> c = e.c();
				final java.lang.Object o = e.o();
				if (null == o) {
					final java.lang.Object[] oa = e.oa();
					final java.lang.Class<?> c1 = (java.lang.Class<?>)oa[0];
					final java.lang.Object o1 = (java.lang.Object)oa[1];
					if (true == o1 instanceof dro.Data) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (3 == oa.length || 4 == oa.length) {
							final java.lang.String methodName1 = (java.lang.String)oa[2];
							if (false == Boolean.TRUE.booleanValue()) {
							} else if (true == e.methodName().startsWith("enter")) {
								if (3 == oa.length) {
									if (0 == methodName1.trim().length()) {
										Logger.getLogger(c1).log(Level.INFO, "==> "+c1.getName()+"()");
									} else {
										Logger.getLogger(c1).log(Level.INFO, "==> "+c1.getName()+":"+o1.toString()+"."+methodName1+"()");
									}
								} else { // else if (4 == oa.length) {
									if (false == Boolean.TRUE.booleanValue()) {
									} else if (true == oa[3] instanceof java.lang.Object[]) {
										final java.lang.Object[] oa1 = (java.lang.Object[])oa[3];
										if (0 == methodName1.trim().length()) {
											Logger.getLogger(c1).log(Level.INFO, "==> "+c1.getName()+"("+oa1+")");
										} else {
											Logger.getLogger(c1).log(Level.INFO, "==> "+c1.getName()+":"+o1.toString()+"."+methodName1+"("+oa1+")");
										}
									} else { //if (true == oa[3] instanceof java.lang.Object) {
										if (0 == methodName1.trim().length()) {
											Logger.getLogger(c1).log(Level.INFO, "==> "+c1.getName()+"("+oa[3]+")");
										} else {
											Logger.getLogger(c1).log(Level.INFO, "==> "+c1.getName()+":"+o1.toString()+"."+methodName1+"("+oa[3]+")");
										}
									}
								}
							} else if (true == e.methodName().startsWith("exit")) {
								if (3 == oa.length) {
									if (0 == methodName1.trim().length()) {
										Logger.getLogger(c1).log(Level.INFO, "<== "+c1.getName());
									} else {
										Logger.getLogger(c1).log(Level.INFO, "<== "+c1.getName()+"."+methodName1);
									}
								} else { // else if (4 == oa.length) {
									if (0 == methodName1.trim().length()) {
										Logger.getLogger(c1).log(Level.INFO, "<== "+c1.getName()+" ["+oa[3]+"]");
									} else {
										Logger.getLogger(c1).log(Level.INFO, "<== "+c1.getName()+":"+o1.toString()+"."+methodName1+" ["+oa[3]+"]");
									}
								}
							}
						} else {
							throw new IllegalStateException();
						}
					}
				} else {
					// Do nothing
				}
				return this;
			}
		};
		dro.lang.Event.register(el, (java.lang.Class<?>[])null, (java.lang.Object[])null, (java.lang.String[])null);
	}
	
	public static java.util.logging.Logger getLogger(final String name) {
		final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(name);
		return null != logger ? logger : java.util.logging.Logger.getGlobal();
	}
	public static java.util.logging.Logger getLogger(final Class<?> clazz) {
		final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(clazz.getName());
		return null != logger ? logger : java.util.logging.Logger.getGlobal();
	}
	
	protected Logger(final String name, final String resourceBundleName) {
		super(name, resourceBundleName);
	}
	
	@Override
	public void finalize() {
		synchronized(el) {
			dro.lang.Event.deregister(el); // TODO
		}
	}
}