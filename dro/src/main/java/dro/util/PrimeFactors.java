package dro.util;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class PrimeFactors {
	static final Set<Integer> primes;// = null;
	static final Set<BigInteger> __primes;// = null;
	
	static {
	  /*final Set<Integer> */primes = new LinkedHashSet<Integer>();
	  /*final Set<BigInteger> */__primes = new LinkedHashSet<BigInteger>();
	}
	
	public static List<Integer> __primeFactors(final Integer i0) {
	  /*final */List<Integer> primeFactors = null;
		if (null != i0) {
		  /*final */List<Integer> factors = null;
			for (int i = i0; i >= 1; i--) {
				final Integer factor = PrimeFactors.factor(i0, new Integer(i));
				if (null != factor) {
					if (null == factors) {
					  /*final List<Integer> */factors = new ArrayList<>();
					}
					factors.add(factor);
				}
			}
			if (null == factors || 0 == factors.size()) {
				if (1 < i0.intValue()) {
					primes.add(i0);
				}
			} else {
			  /*final */boolean hasPrimeFactorsOnly = true;
				for (final Integer f: factors) {
					if (false == primes.contains(f)) {
					  /*final boolean */hasPrimeFactorsOnly = false;
					}
				}
				if (true == hasPrimeFactorsOnly) {
					for (final Integer f: factors) {
						if (null == primeFactors) {
						  /*final List<Integer> */primeFactors = new ArrayList<>();
						}
						primeFactors.add(f);
					}
				}
			}
		}
		return primeFactors;
	}
	public static List<Integer> primeFactors(final Integer i0) {
	  /*final */List<Integer> primeFactors = null;
		final long start = Calendar.getInstance().getTimeInMillis();
	  /*final */int __i0 = i0.intValue();
		for (int __i = 2; __i < __i0; __i++) {
			while (__i0 % __i == 0) {
				if (null == primeFactors) {
				  /*final List<Integer> */primeFactors = new ArrayList<>();
				}
				primeFactors.add(new Integer(__i));
				__i0 = __i0 / __i;
			}
		}
		final long finish = Calendar.getInstance().getTimeInMillis();
		System.err.println(i0+" took "+(finish-start)+"ms to calculate");
		if (__i0 > 2) {
			if (null == primeFactors) {
				primes.add(i0);
			  /*final List<Integer> */primeFactors = new ArrayList<>();
			} else {
				if (null == primeFactorStore) {
				  /*final Map<Integer, List<Integer>> */primeFactorStore = new HashMap<>();
				}
				primeFactorStore.put(new Integer(i0), primeFactors);
				if (null == primeFactorTimer) {
				  /*final Map<Integer, List<Integer>> */primeFactorTimer = new HashMap<>();
				}
				primeFactorTimer.put(new Integer(i0), new Long[]{start, finish});
			}
			primeFactors.add(new Integer(__i0));
		}
		return primeFactors;
	}
	private final static BigInteger BigInteger_0 = new BigInteger("0");
	private final static BigInteger BigInteger_1 = new BigInteger("1");
	private final static BigInteger BigInteger_2 = new BigInteger("2");
	private static BigInteger BigInteger_z;// = new BigInteger("0");
	private final static BigInteger BigInteger_Z = new BigInteger("1000000");
  /*final */static Map<Integer, List<Integer>> primeFactorStore;// = null;
  /*final */static Map<Integer, Long[]> primeFactorTimer;// = null;
	static {
		primeFactorStore = null;
		primeFactorTimer = null;
	}
	public static List<BigInteger> primeFactors(/*final */BigInteger i0) {
	  /*final */List<BigInteger> primeFactors = null;
		final long start = Calendar.getInstance().getTimeInMillis();
		BigInteger_z = BigInteger_0;
	  /*final */BigInteger i = i0;
		for (BigInteger __i = BigInteger_2; -1 == __i.compareTo(i); __i = __i.add(BigInteger_1)) {
			BigInteger_z = BigInteger_z.add(BigInteger_1);
			if (BigInteger_z.mod(BigInteger_Z).equals(BigInteger_0)) {
				System.err.println(i0.toString()+"#"+BigInteger_z.toString());
			}
			while (i.mod(__i).equals(BigInteger_0)) {
				if (null == primeFactors) {
				  /*final List<BigInteger> */primeFactors = new ArrayList<>();
				}
				primeFactors.add(__i);
				i = i.divide(__i);
			}
		}
		if (+1 == i0.compareTo(BigInteger_2)) {
			if (null == primeFactors) {
				__primes.add(i0);
System.err.println(i0.toString()+" is prime");
			  /*final List<Integer> */primeFactors = new ArrayList<>();
			}
			primeFactors.add(i);
		}
		final long finish = Calendar.getInstance().getTimeInMillis();
		System.err.println(i0+" took "+(finish-start)+"ms to calculate");
		return primeFactors;
	}
	
	// Assumption is that for any "i" all the numbers from 1 to "i" (inclusive, except for "i") have been run through this algorithm, non-recursively
	public static Integer factor(final Integer i0, final Integer i) {
	  /*final */Integer factor = null;
		if (null != i0 && null != i) {
			final int __i0 = i0.intValue(), __i = i.intValue();
			if (__i0 > __i && __i > 1) {
 				if (0 == __i0 % __i) {
 					factor = new Integer(i); // Should divide, and then try again, but this would involve recursion
 				}
			}
		}
		return factor;
	}
	
	// Someone else's algorithm.. Note no recursion (-:
	public static void main(String args[]) {
		final Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number ::");
		int number = sc.nextInt();
		for (int i = 2; i < number; i++) {
			while (number % i == 0) {
				System.out.println(i + " ");
				number = number / i;
			}
		}
		if (number > 2) {
			System.out.println(number);
		}
		sc.close();
	}
}