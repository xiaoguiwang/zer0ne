package dro.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Properties p = null;
for (final Object o: properties.keySet()) {
	String k = (String)o;
	String v = properties.getProperty(k);
	final String[] s = k.split("#");
	if (s.length >= 2) {
		v = v.split("#")[0].trim();
		if (true == s[1].startsWith("param[")) {
			final String[] t = v.split("=");
			k = t[0];
			v = dro.lang.String.resolve(t[1], null);
		} else {
			k = s[1];
		}
		if (true == s[1].startsWith("param[")) {
			if (null == p) {
				p = new Properties();
			}
			p.setProperty(k, v);
		} else {
			if (null == p) {
				p = new Properties();
			}
		  //try {
		  //	Integer i = Integer.parseInt(v);
		  //	p.setProperty(k, i);
		  //} catch (NumberFormatException e) {
				p.setProperty(k, v);
		  //}
		}
	}
}
*/

// TODO: define priority/hierarchy of property resolution
public class Properties extends java.util.Properties {
	private static final long serialVersionUID = 0L;
	
	private static final String className = Properties.class.getName();
	
	private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	private static final String dotProperties = ".properties";
	private static final long dotPropertiesLength = dotProperties.length();
	
	public static class Return extends dro.lang.Return {}
	
	public abstract interface Callback {
		public abstract boolean handle(final InputStream is) throws IOException;
	}
	
	private static Properties p;// = null;
	private static String[] paths;// = null [1st]
	private static String[] suffixes;// = null [1st]
	
	static { // [2nd]
		logger.entering(className, "static {}");
		
		p = null;
		paths = null;
		
	  //paths = null;
		
		logger.exiting(className, "static {}");
	}
	
	public static void paths(final String[] paths) {
		logger.entering(className, "paths(String[])", paths);
		
		if (null == Properties.paths) {
			Properties.paths = paths;
		} else {
			final List<String> __paths = new ArrayList<String>();
			for (final String __path: Properties.paths) {
				__paths.add(__path);
			}
			for (final String __path: paths) {
				__paths.add(__path);
			}
			Properties.paths = __paths.toArray(new String[]{});
		}
		
		logger.exiting(className, "paths(String[])");
	}
	public static String[] paths() {
		return paths;
	}
	public static void path(final String path) {
		logger.entering(className, "path(String)", path);
		
		Properties.paths(new String[]{path});
		
		logger.exiting(className, "path(String)");
	}
	
	private static void suffixes(final String[] suffixes) {
		logger.entering(className, "suffixes", suffixes);
		
		if (null == Properties.suffixes) {
			Properties.suffixes = suffixes;
		} else {
			final List<String> __suffixes = new ArrayList<String>();
			for (final String __suffix: Properties.suffixes) {
				__suffixes.add(__suffix);
			}
			for (final String __suffix: suffixes) {
				__suffixes.add(__suffix);
			}
			Properties.suffixes = __suffixes.toArray(new String[]{});
		}
		
		logger.exiting(className, "suffixes(String[])");
	}
	public static String[] suffixes() {
		if (null == suffixes) {
			final String profile = System.getProperty("profile");
			if (null != profile && 0 < profile.length()) {
				suffix("-profile-"+profile);
			}
			suffix("-username-"+System.getProperty("user.name"));
			try {
				suffix("-hostname-"+InetAddress.getLocalHost().getHostName());
			} catch (final UnknownHostException e) {
				e.printStackTrace(System.err);
			}
			final String env = System.getProperty("environment");
			if (null != env && 0 < env.length()) {
				suffix("-environment-"+env);
			}
			final Pattern pattern = Pattern.compile("([a-zA-Z]+)\\s*.*", Pattern.DOTALL);
			final Matcher m = pattern.matcher(System.getProperty("os.name"));
			if (true == m.find()) {
				suffix("-os-"+m.group(1).toLowerCase());
			}
			suffix("-local");
		}
		return suffixes;
	}
	public static void suffix(final String suffix) {
		logger.entering(className, "suffix(String)", suffix);
		
		Properties.suffixes(new String[]{suffix});
		
		logger.exiting(className, "suffix(String)");
	}
	
	public static Properties properties(final Properties p) {
		Properties.p = p;
		return Properties.p;
	}
	public static Properties properties() {
		return Properties.p;
	}
	
	private List<String> propertyPaths;// = null;
	private String propertyPrefix;// = null;
	
	{
		logger.entering(className, "{}");
		
		propertyPaths = null;// new ArrayList<String>();
		propertyPrefix = "";
		
		logger.exiting(className, "{}");
	}
	
	public Properties() { // [2nd]
		super();
		logger.entering(className, "()");
		
		logger.exiting(className, "()", this);
	}
	// Not sure if we'll ever need this constructor, but maybe it will be useful for testing and/or bootstrapping
	Properties(final java.util.Properties properties) {
		this();
		logger.entering(className, "(Properties)", properties);
		
		for (final Object o: properties.keySet()) {
			this.setProperty((String)o, properties.getProperty((String)o));
		}
		
		logger.exiting(className, "(Properties)", this);
	}
	public Properties(final Class<?> cl, final String filename) throws FileNotFoundException, IOException { // [3rd]
		this();
		logger.entering(className, "(Class, String)", new Object[]{cl, filename});
		
	  /*boolean loaded = */this.__load(Properties.paths, cl, filename);
		
		logger.exiting(className, "(Class, String)", this);
	}
	public Properties(final String filename) throws FileNotFoundException, IOException { // [3rd]
		this((Class<?>)null, filename);
		logger.entering(className, "(String)", filename);
		
		logger.exiting(className, "(String)", this);
	}
	// TODO: squash certain situations where FileNotFoundException might come up - but we don't care
	public Properties(final String filename, final Callback callback) throws FileNotFoundException, IOException { // [3rd]
		this();
		logger.entering(className, "(String)", filename);
		
		this.callback = callback;
	  /*boolean loaded = */this.__load(Properties.paths, (Class<?>)null, filename);
		
		logger.exiting(className, "(String)", this);
	}
	public Properties(final Class<?> cl, final Class<?> clazz) throws FileNotFoundException, IOException { // [3rd]
	  //this(cl, clazz.getSimpleName());
		this();
		logger.entering(className, "(Class, Class)", new Class<?>[]{cl, clazz});
		
	  /*boolean loaded = */this.__load(Properties.paths, cl, clazz.getSimpleName());
	  /*boolean loaded = */this.__load(Properties.paths, (Class<?>)null, clazz.getSimpleName());
	  /*boolean loaded = */this.__load(Properties.paths, (Class<?>)null, clazz.getPackage().getName()+"."+clazz.getSimpleName());
		
		logger.exiting(className, "(Class, Class)", this);
	}
	public Properties(final Class<?> cl, final Object object) throws FileNotFoundException, IOException { // [3rd]
		this(cl, object.getClass());
		logger.entering(className, "(Class, Object)", new Object[]{cl, object});
		
		logger.exiting(className, "(Class, Object)", this);
	}
	public Properties(final Class<?> clazz) throws FileNotFoundException, IOException { // 3rd
		this(clazz, clazz);
		logger.entering(className, "(Class)", clazz);
		
		logger.exiting(className, "(Class)", this);
	}
	public Properties(final Object object) throws FileNotFoundException, IOException { // 3rd
		this(object.getClass());
		logger.entering(className, "(Object)", object);
		
		logger.exiting(className, "(Object)", this);
	}
	
	private boolean loaded;// = false;
	private Callback callback;// = null;
	
	{
		loaded = false;
	  //callback = null;
	}
	
	private void loaded(final boolean loaded) {
		this.loaded = loaded;
	}
	public boolean loaded() {
		return this.loaded;
	}
	
	private boolean __load(final InputStream is) throws IOException {
		logger.entering(className, "__load(InputStream)", is);
		
		boolean loaded = false;
		if (null != is && null == this.callback) {
			final char[] buffer = new char[1024];
			final StringBuffer sb = new StringBuffer();
			final Reader in = new InputStreamReader(is, "UTF-8");
			for (int r = 0; r >= 0; ) {
				r = in.read(buffer, 0, buffer.length);
				if (r >= 0) {
					sb.append(buffer, 0, r);
				}
			}
			final String s = sb.toString().trim();
			if (0 == s.length()) {
				logger.log(Level.WARNING, "Properties file contents are empty?");
			} else{
				super.load(new ByteArrayInputStream(s.getBytes()));
				logger.finer(new StringBuffer() // TODO: tried to use logger.log(Level.CONFIG.. but nothing came out..
					.append("\n|Properties file contents below:\n")
					.append("v------------------------------\n")
					.append(sb.toString().replaceAll("pass(\\w*)=\\w+", "pass$1=#pass$1#")+"\n") // FIXME for where the whole file content is the password..
					.append("^------------------------------\n")
					.append("|Properties file contents above")
					.toString()
				);
			}
			loaded = true;
		} else if (null != this.callback) {
			loaded = this.callback.handle(is);
		}
		
		logger.exiting(className, "__load(InputStream)", loaded);
		return loaded;
	}
	// Note: we'd like this to be protected, but we inherited it from java.util.Properties and so we can't reduce visibility from
	//       public - but we could if we wrapped it, but then we might force developers to change existing code, which wouldn't
	//       be a bad thing (necessarily) since we're drawing attention to the fact that this dro.util.Properties is 'different'
	public void load(final InputStream is) throws IOException {
		logger.entering(className, "load(InputStream)", is);
		
	  /*boolean loaded = */this.__load(is);
		
		logger.exiting(className, "load(InputStream)");
	}
	// Note: yes we could have left the 'final' modified on String filename, but we're indicating here (by using the 'final'
	//       modified everywhere else we can (but don't have to) use it to indicate it's value may change inside this method
	@SuppressWarnings("resource")
	private boolean __load(final Class<?> cl, String filename) throws IOException {
		logger.entering(className, "__load(Class, String)", new Object[]{cl, filename});
		
		boolean loaded = false;
		final String __filename = dro.lang.String.resolve(filename, this);
		if (false == filename.equals(__filename)) {
			logger.finer("Resolved from '"+filename+"' into '"+__filename+"'");
		}
	  /*FileInputStream fis = null;*/
		InputStream is = null;
		if (null == cl) {
			try {
				/*f*/is = new FileInputStream(new File(__filename));
			} catch (final FileNotFoundException e) {
			  //logger.log(Level.WARNING, "Properties '"+__filename+"' not loaded: "+e.getMessage());
			  //throw e;
			}
			if (null != /*f*/is) { // TODO: properties hierarchy/priority (over)loading etc..
				logger.finest("Properties '"+__filename+"' loaded via new FileInputStream(new File(filename))");
			  /*is = fis;*/
			} else if (false == __filename.matches("[\\\\\\/]")) {
				is = ClassLoader.getSystemResourceAsStream(__filename);
				if (null != is) {
					logger.finest("Properties '"+__filename+"' loaded via ClassLoader.getSystemResourceAsStream(..)");
				}
			}
		} else if (false == __filename.matches("[\\\\\\/]")) {
			is = cl.getResourceAsStream(__filename);
			if (null != is) {
				logger.finest("Properties '"+__filename+"' loaded via class.getResourceAsStream(..)");
			}
		}
		if (null != is) {
			try {
				loaded = this.__load(is);
				logger.info("Properties '"+__filename+"' loaded");
			} catch (final IOException e) { // TODO: let this percolate up
				logger.log(Level.WARNING, "Properties '"+__filename+"' not loaded: "+e.getMessage());
				throw e;
			}
		}
		if (null != is) {
			try {
				is.close();
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "Properties '"+__filename+"' closed with error(s): "+e.getMessage());
			}
		}
		
		logger.exiting(className, "__load(Class, String)", loaded);
		return loaded;
	}
	private final boolean __load(final String[] paths, final Class<?> cl, final String filename, final String suffix) throws IOException {
		logger.entering(className, "__load(String[], Class<?>, String, String)", new Object[]{paths, cl, filename, suffix});
		
		boolean loaded = false;
		
		String __path__;
		if (null != filename && 0 < filename.trim().length()) {
			final String __filename = filename.trim();
			StringBuffer path = new StringBuffer(__filename);
			if (null == suffix || 0 == suffix.trim().length()) {
				__path__ = path.toString();
				if (false == __filename.endsWith(dotProperties)) {
					path.append(dotProperties);
				}
			} else { // if (null != suffix && 0 < suffix.trim().length()) {
				if (false == __filename.endsWith(dotProperties)) {
					if (false == __filename.endsWith(suffix)) {
						path.append(suffix);
					}
				} else {
					path.setLength(path.length()-(int)dotPropertiesLength);
					if (false == path.toString().endsWith(suffix)) {
						path.append(suffix);
					}
				}
				__path__ = path.toString();
				path.append(dotProperties);
			}
			final String __path = path.toString();
			if (null == paths) {
				if (null == this.propertyPaths) {
					this.propertyPaths = new ArrayList<String>();
				}
				if (false == this.propertyPaths.contains(__path)) {
					logger.info("Load '"+__path__+"' properties..");
					loaded = this.__load(cl, __path);
					if (true == loaded) {
						logger.info("Loaded '"+__path__+"' properties");
						this.propertyPaths.add(__path);
					}
				}
			} else {
				boolean __loaded = false;
				final String[] __paths = new String[paths.length];
				for (int s = 0; s < paths.length; s++) {
					__paths[s] = new StringBuffer(paths[s])
						.append(File.separator)
						.append(__path.toString())
						.toString()
					;
					if (null == this.propertyPaths) {
						this.propertyPaths = new ArrayList<String>();
					}
					// Note: only ever try to load from the class-loader once - all other times,
					//       consider/entertain that the contents could have changed in the cwd
					//       except this __load(er) is static in design/nature..
					if (false == this.propertyPaths.contains(__paths[s])) {
						logger.info("Load '"+__path__+"' properties..");
						__loaded = this.__load(cl, __paths[s]);
						if (true == __loaded) {
							logger.info("Loaded '"+__path__+"' properties");
							this.propertyPaths.add(__paths[s]);
							loaded = true;
						}
					}
				}
			}
		}
		
		this.loaded(loaded);
		logger.entering(className, "__load(String[], Class<?>, String, String)", loaded);
		return loaded;
	}
	private final boolean __load(final String[] paths, final Class<?> cl, final String filename) throws IOException {
		logger.entering(className, "__load(String[], Class<?>, String, String)", new Object[]{paths, cl, filename});
		
		boolean loaded = false;
		if (null == cl) {
			boolean suffixed = false;
			for (final String suffix: Properties.suffixes()) {
				if (null != suffix && 0 < suffix.trim().length()) {
					if (true == filename.endsWith(suffix) || true == filename.endsWith(suffix+".properties")) {
						suffixed = true;
					}
				}
			}
			if (false == suffixed) {
				for (final String suffix: Properties.suffixes()) {
					if (true == this.__load(Properties.paths(), (Class<?>)null, filename, suffix)) {
						loaded = true;
					}
				}
			}
		}
		if (true == this.__load(Properties.paths(), cl, filename, (String)null)) {
			loaded = true;
		}
		
		logger.entering(className, "__load(String[], Class<?>, String)", loaded);
		return loaded;
	}
	
	public boolean load(final File file) throws FileNotFoundException, IOException {
		logger.entering(className, "load(File)", file);
		
		logger.finest("Loading '"+file.getCanonicalPath()+"' properties using properties.__load(FileInputStream(file))..");
		boolean loaded = this.__load(new FileInputStream(file));
		logger.finest("Loaded '"+file.getAbsolutePath()+"' properties");
		
		logger.exiting(className, "load(File)", loaded);
		return loaded;
	}
	public boolean load(final String filename) throws IOException {
		logger.entering(className, "load(String)", filename);
		
		logger.finest("Loading '"+filename+"' properties using properties.__load(Properties.paths, null, filename)..");
		boolean loaded = this.__load(Properties.paths, (Class<?>)null, filename);
		logger.finest("Loaded '"+filename+"' properties");
		
		logger.exiting(className, "load(String)", loaded);
		return loaded;
	}
	public boolean load(final Class<?> clazz) throws IOException {
		logger.entering(className, "load(Class)", clazz);
		
		boolean loaded = this.__load(Properties.paths, clazz, clazz.getSimpleName());
		
		logger.exiting(className, "load(Clazz)", loaded);
		return loaded;
	}
	
	@Override
	public Object setProperty(final String propertyKey, final String propertyValue) {
		logger.entering(className, "setProperty(String, String)", new String[]{propertyKey,propertyValue});
		
		final Object object;// = null;
		final String __propertyKey = dro.lang.String.resolve(propertyKey, this);
		if (null != propertyValue) {
		  //final String __propertyValue = dro.lang.String.resolve(propertyValue, this);
			final String __propertyValue = propertyValue;
			object = super.setProperty(__propertyKey, __propertyValue);
		} else {
			object = null;
		}
		
		logger.exiting(className, "setProperty(String, String)", object);
		return object;
	}
	
	public boolean hasProperty(final String propertyKey) {
		logger.entering(className, "hasProperty(String)", propertyKey);
		
		boolean hasProperty;
	  /*final */String __propertyKey = dro.lang.String.resolve(propertyKey, this);
		if (true == __propertyKey.contains(":")) {
			final String[] sa = __propertyKey.split(":", 2);
			final String __propertyPath = dro.lang.String.resolve(sa[0], this);
			__propertyKey = dro.lang.String.resolve(sa[1], this);
			hasProperty = this.hasProperty(__propertyPath, __propertyKey);
		} else {
			if (null != System.getProperty(__propertyKey)) {
				hasProperty = true;
			} else if (null != this.propertyPrefix && null != System.getProperty(this.propertyPrefix+__propertyKey)) {
				hasProperty = null != System.getProperty(this.propertyPrefix+__propertyKey);
			} else if (null != Properties.properties() && this != Properties.properties() && true == Properties.properties().containsKey(__propertyKey)) {
				hasProperty = true;
			} else {
				hasProperty = super.containsKey(__propertyKey);
			}
		}
		
		logger.exiting(className, "hasProperty(String)", hasProperty);
		return hasProperty;
	}
	public boolean hasProperty(final String propertyPath, final String propertyKey) throws RuntimeException {
		logger.entering(className, "hasProperty(String, String)", new String[]{propertyPath,propertyKey});
		
		final boolean hasProperty;
		final String __propertyPath = dro.lang.String.resolve(propertyPath, this);
		if (false == propertyKey.equals("#")) {
		  /*boolean loaded = false;*/
			try {
			  /*loaded = */this.load(__propertyPath);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			final String __propertyKey = dro.lang.String.resolve(propertyKey, this);
			
			hasProperty = this.hasProperty(__propertyKey);
		} else {
			hasProperty = new File(__propertyPath).exists();
		}
		logger.exiting(className, "hasProperty(String, String)", hasProperty);
		return hasProperty;
	}
	
	@Override
	public String getProperty(final String propertyKey) {
	  //logger.entering(className, "getProperty(String)", propertyKey); // Too noisy
		
		String __propertyValue;// = null;
	  /*final */String __propertyKey = dro.lang.String.resolve(propertyKey, this);
		if (true == __propertyKey.contains(":")) {
			final String[] sa = __propertyKey.split(":", 2);
			final String __propertyPath = dro.lang.String.resolve(sa[0], this);
			__propertyKey = dro.lang.String.resolve(sa[1], this);
			__propertyValue = this.getProperty(__propertyPath, __propertyKey);
		} else {
		  /*if (null != System.getProperty(__propertyKey)) {
				__propertyValue = System.getProperty(__propertyKey);
			} else if (null != this.propertyPrefix && null != System.getProperty(this.propertyPrefix+__propertyKey)) {
				__propertyValue = System.getProperty(this.propertyPrefix+__propertyKey);
			} else if (null != Properties.properties() && this != Properties.properties() && null != Properties.properties().getProperty(__propertyKey)) {
				__propertyValue = Properties.properties().getProperty(__propertyKey);
			} else {
				__propertyValue = super.getProperty(__propertyKey);
			}*/
			if (true == super.containsKey(__propertyKey)) {
				__propertyValue = super.getProperty(__propertyKey);
			} else if (null != Properties.properties() && this != Properties.properties() && null != Properties.properties().getProperty(__propertyKey)) {
				__propertyValue = Properties.properties().getProperty(__propertyKey);
			} else if (null != this.propertyPrefix && null != System.getProperty(this.propertyPrefix+__propertyKey)) {
				__propertyValue = System.getProperty(this.propertyPrefix+__propertyKey);
			} else {
				__propertyValue = System.getProperty(__propertyKey);
			}
			__propertyValue = dro.lang.String.resolve(__propertyValue, this);
		}
		
	  //logger.exiting(className, "getProperty(String)", propertyValue); // Too noisy
		return __propertyValue;
	}
	public Integer getProperty(final String propertyKey, final Integer defaultValue, final dro.lang.Integer.Return r) {
		final String propertyValue = this.getProperty(propertyKey);
		return null != propertyValue ? Integer.parseInt(propertyValue) : defaultValue;
	}
	public Long getProperty(final String propertyKey, final Long defaultValue, final dro.lang.Long.Return r) {
		final String propertyValue = this.getProperty(propertyKey);
		return null != propertyValue ? new Long(propertyValue) : defaultValue;
	}
	public Boolean getProperty(final String propertyKey, final Boolean defaultValue, final dro.lang.Boolean.Return r) {
		final String propertyValue = this.getProperty(propertyKey);
		return null != propertyValue ? new Boolean(propertyValue) : defaultValue;
	}
	public String getProperty(final String propertyPath, final String propertyKey) throws RuntimeException {
		logger.entering(className, "getProperty(String, String)", new String[]{propertyPath, propertyKey});
		
		final String __propertyValue;
		final String __propertyPath = dro.lang.String.resolve(propertyPath, this);
		if (false == propertyKey.equals("#")) {
		  /*boolean loaded = false;*/
			try {
			  /*loaded = */this.load(__propertyPath);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			final String __propertyKey = dro.lang.String.resolve(propertyKey, this);
			
			__propertyValue = this.getProperty(__propertyKey);
		} else {
			StringBuffer sb = null;
			try {
				BufferedReader br = new BufferedReader(new FileReader(__propertyPath));
				String line = "";
				while (null != line) {
					line = br.readLine();
					if (null != line) {
						if (null == sb) {
							sb = new StringBuffer();
						}
						sb.append(line);
					}
				}
				br.close();
			  //br = null;
			} catch (final FileNotFoundException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
			__propertyValue = null == sb ? null : sb.toString();
		}
		final String __propertyValue__;
		if (true == propertyKey.equals("#")) {
			__propertyValue__ = "xxxxxx";
		} else {
			__propertyValue__ = __propertyValue;
		}
		logger.exiting(className, "getProperty(String, String)", __propertyValue__);
		return __propertyValue;
	}
	public String[] getPropertyArray(final String propertyKey, final String delim) {
		return dro.lang.String.getArray(super.getProperty(propertyKey), delim);
	}
	public String[] getPropertyArray(final String propertyKey, String defs[], String delim) {
		String[] propertyValues = this.getPropertyArray(propertyKey, delim);
		if (null == propertyValues || 0 == propertyValues.length) {
			propertyValues = dro.lang.String.getArray((String)super.get("DEFAULT."+propertyKey), delim);
		}
		if (null == propertyValues || 0 == propertyValues.length) {
			propertyValues = defs;
		}
		return propertyValues;
	}
	
	public Properties __getPropertiesPrefixedWith(Properties __proper, final String __prefix, final String __delimiter) {
		logger.entering(className, "__getPropertiesPrefixedWith(Properties, String, String)", new Object[]{__proper, __prefix, __delimiter});
		
		final Set<Object> keys = super.keySet();
		if (null != keys) {
			final Iterator<Object> i = keys.iterator();
			while (true == i.hasNext()) {
				final String propertyKey = (String)i.next();
				if (true == propertyKey.startsWith(__prefix+__delimiter)) {
					if (null == __proper) {
						__proper = new Properties();
						__proper.propertyPrefix = __prefix+__delimiter;
					}
					final int __prefixPlusDelimiterLength = (__prefix+__delimiter).length();
					final int propertyKeyLength = propertyKey.length();
					final String superGetProperty = super.getProperty(propertyKey);
					final String propertyKeySubString = propertyKey.substring(__prefixPlusDelimiterLength, propertyKeyLength);
					__proper.setProperty(propertyKeySubString, superGetProperty);
				}
			}
		}
		
		logger.exiting(className, "__getPropertiesPrefixedWith(Properties, String, String)", __proper);
		return __proper;
	}
	public Properties getPropertiesPrefixedWith(final String prefix) {
		logger.entering(className, "__getPropertiesPrefixedWith(String)", prefix);
		
		Properties proper = this.__getPropertiesPrefixedWith((Properties)null, prefix, "#");
		
		final Properties __proper = this.__getPropertiesPrefixedWith(proper, prefix, "@");
		logger.exiting(className, "__getPropertiesPrefixedWith(String)", __proper);
		return __proper;
	}
	public Properties getPropertiesPrefixedWith(final Class<?> clazz) {
		logger.entering(className, "__getPropertiesPrefixedWith(Class)", clazz);
		
		final Properties __proper = this.getPropertiesPrefixedWith(clazz.getName());
		logger.exiting(className, "__getPropertiesPrefixedWith(Class)", __proper);
		return __proper;
	}
	public Properties getPropertiesPrefixedWith(final Class<?> clazz, final String extension) {
		logger.entering(className, "__getPropertiesPrefixedWith(Class, String)", new Object[]{clazz,extension});
		
		final Properties __proper;
		if (this == Properties.properties() || null == Properties.properties()) {
			__proper = this.__getPropertiesPrefixedWith((Properties)null, clazz.getName()+"@"+extension, "#");
		} else if (null != Properties.properties()) {
			__proper = Properties.properties().__getPropertiesPrefixedWith(this, clazz.getName()+"@"+extension, "#");
		} else {
			__proper = null;
		}
		
		logger.entering(className, "__getPropertiesPrefixedWith(Class, String)", __proper);
		return __proper;
	}
	
	// Used by adapters for .api construction ???--Alex Russell 2019/Sep/4th
	public Properties property(final String key, final String value) {
		super.setProperty(key, value);
		return this;
	}
	public String property(final String key) {
		return dro.lang.String.resolve(super.getProperty(key), this);
	}
	
	@Override
	public java.lang.String toString() {
		return super.toString().replaceAll("pass(\\w*)=\\w+", "pass$1=#pass$1#");
	}
}