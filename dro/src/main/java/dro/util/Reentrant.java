package dro.util;

import java.util.HashMap;
import java.util.Map;

public abstract class Reentrant<T> {
	protected final Map<Thread, T> map = new HashMap<>();
	
	public T getInstance() {
		final T o;// = null;
		synchronized(this.map) {
			final Thread t = Thread.currentThread();
			if (false == this.map.containsKey(t)) {
				o = this.newInstance();
				this.map.put(t, o);
			} else {
				o = this.map.get(t);
			}
		}
		return o;
	}
	
	public abstract T newInstance();
}