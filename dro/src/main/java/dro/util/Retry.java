package dro.util;

import java.util.List;
import java.util.logging.Level;

public class Retry {
	private static final String className = Retry.class.getName();
	
	private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	public static class Handler {
		protected Retry retry;
		public Handler() {
			this.retry = new Retry();
		}
		public Handler(dro.util.Properties p) {
			if (null == p) {
				p = new Properties();
			}
			this.retry = new Retry(
				p.getProperty("shortRetryCount"   ,  (int)5     , dro.lang.Integer.Return.Integer),
				p.getProperty("shortRetryInterval", (long)12000L, dro.lang.Long   .Return   .Long),
				p.getProperty("longRetryCount"    ,  (int)5     , dro.lang.Integer.Return.Integer),
				p.getProperty("longRetryInterval" , (long)60000L, dro.lang.Long   .Return   .Long)
			);
		}
		protected boolean shouldRetry() {
			return this.retry.shouldRetry();
		}
		protected void sleepBeforeRetry() {
			this.retry.sleepBeforeRetry();
		}
		protected void handle() throws Exception {
			while (true) {
				try {
					this.handler();
					break;
				} catch (final Exception e) {
					if (true == this.shouldRetry()) {
						Logger.getLogger(className).log(
							Level.WARNING,
							dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- "+e.getMessage(),
							e
						);
						this.sleepBeforeRetry();
					} else {
						throw e;
					}
				}
			}
		}
		protected void handler() throws Exception {
			throw new IllegalStateException();
		}
	}
	
	public final int retryShortCount;
	public final long retryShortInterval;
	public final int retryLongCount;
	public final long retryLongInterval;
	
	protected int retryShort;
	protected int retryLong;
	
	protected List<Exception> exceptions;// = null;
	
	{
	  //exceptions = null;
	}
	
	protected Retry(final int retryShortCount, final long retryShortInterval, final int retryLongCount, final long retryLongInterval) {
		this.retryShortCount    = retryShortCount   ;
		this.retryShortInterval = retryShortInterval;
		this.retryLongCount     = retryLongCount    ;
		this.retryLongInterval  = retryLongInterval ;
	}
	protected Retry() {
		this((int)5, (long)200L, (int)5, (long)2000L);
	}
	
	protected boolean shouldRetry() {
		if (this.retryLong < this.retryLongCount) {
			if (this.retryShort < this.retryShortCount) {
				this.retryShort++;
			} else {
				this.retryLong++;
			}
		}
		logger.log(
			Level.INFO,
			"retry -- "+this.toString()
		);
		return this.retryLong < this.retryLongCount;
	}
	protected void sleepBeforeRetry() {
		if (this.retryShort < this.retryShortCount) {
			logger.log(
				Level.INFO,
				dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- sleep (short) -- "+this.toString()
			);
			try {
				Thread.sleep(this.retryShortInterval);
			} catch (final InterruptedException ex) {
				throw new RuntimeException(ex);
			}
			logger.log(
				Level.INFO,
				dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- awake from sleep (short)"
			);
		} else {
			logger.log(
				Level.INFO,
				dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- sleep (long) -- "+this.toString()
			);
			try {
				Thread.sleep(this.retryLongInterval);
			} catch (final InterruptedException ex) {
				throw new RuntimeException(ex);
			}
			logger.log(
				Level.INFO,
				dro.lang.Class.getName(java.lang.Thread.class)+"#"+java.lang.Thread.currentThread().getId()+" -- awake from sleep (long)"
			);
		}
	}
	
	@Override
	public java.lang.String toString() {
		return new StringBuffer()
			.append(super.toString())
			.append("[retryShort=")
			.append(this.retryShort)
			.append("|retryShortCount=")
			.append(this.retryShortCount)
			.append("|retryShortInterval=")
			.append(this.retryShortInterval)
			.append("|retryLong=")
			.append(this.retryLong)
			.append("|retryLongCount=")
			.append(this.retryLongCount)
			.append("|retryLongInterval=")
			.append(this.retryLongInterval)
			.append("]")
			.toString()
		;
	}
}