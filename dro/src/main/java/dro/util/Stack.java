package dro.util;

public class Stack<T> extends java.util.ArrayList<T> {
	private static final long serialVersionUID = 0L;
	
	public Stack() {
		super(); // Implicit super constructor..
	}
	public Stack(final T[] oa) {
		super(); // Implicit super constructor..
		for (final T o: oa) {
			this.push(o);
		}
	}
	
	public Stack<T> push(final T o) {
		super.add(o);
		return this;
	}
	public int depth() {
		return super.size();
	}
	public T peek(final int index) {
		return super.get(index);
	}
	public T peek() {
		return super.get(this.depth()-1);
	}
	public T pop() {
		return super.remove(this.depth()-1);
	}
}