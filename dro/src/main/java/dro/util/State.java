package dro.util;

public class State {
	public static final State NONE         = new State();
	public static final State INITIALISING = new State();
	public static final State RUNNING      = new State();
	public static final State SHUTDOWN     = new State();
	
	public static final String LT = "LT";
	public static final String LE = "LE";
	public static final String EQ = "EQ";
	public static final String NE = "NE";
	public static final String GE = "GE";
	public static final String GT = "GT";
	
	private State() {
	}
	
	public static class Boolean {
		private boolean state;
		public Boolean() {
		}
		public Boolean(final boolean state) {
			this.state(state);
		}
		public void state(final boolean state) {
			this.state = state;
		}
		public boolean state() {
			return this.state;
		}
	}
	public static class Long {
		private long state;
		public Long() {
		}
		public Long(final long state) {
			this.set(state);
		}
		public void set(final long state) {
			this.state = state;
		}
		public long get() {
			return this.state;
		}
		public final void blockOnCondition(final String condition, long value) throws InterruptedException {
			if (false == new java.lang.Boolean(java.lang.Boolean.TRUE).booleanValue()) {
			} else if (State.NE.equals(condition) == true) {
				while (this.get() != value) {
					Thread.sleep(200L);
				}
			} else if (State.EQ.equals(condition) == true) {
				while (this.get() == value) {
					Thread.sleep(200L);
				}
			} else if (State.GE.equals(condition) == true) {
				while (this.get() >= value) {
					Thread.sleep(200L);
				}
			} else if (State.LE.equals(condition) == true) {
				while (this.get() <= value) {
					Thread.sleep(200L);
				}
			} else if (State.GT.equals(condition) == true) {
				while (this.get() > value) {
					Thread.sleep(200L);
				}
			} else if (State.LT.equals(condition) == true) {
				while (this.get() < value) {
					Thread.sleep(200L);
				}
			} else {
				throw new IllegalArgumentException();
			}
		}
	}
}