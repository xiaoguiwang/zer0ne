package dro;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataTest {
	static {
		try {
			Class.forName(dro.util.Logger.class.getName());
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException();
		}
	}
	
	public DataTest() {
	}
	
	@Before
	public void before() {
	}
	
	@Test
	public void test_0000() {
	  /*final dro.Data d = */new dro.Data();
	}
	
	@Test // Equals
	public void test_0001() {
		final dro.Data d1 = new dro.Data();
		final dro.Data d2 = new dro.Data();
		Assert.assertEquals(d1, d2);
	}
	
	@Test // Clone (and therefore equals)
	public void test_0002() {
		final dro.Data d1 = new dro.Data();
		final dro.Data d2 = d1.clone();
		Assert.assertEquals(d1, d2);
	}
	
	@Test // For purposes of serialisation
	public void test_0003() {
		final dro.Data d1 = new dro.Data();
		final dro.Data d2 = new dro.Data();
		d1.add(d2);
		final dro.Data d3 = new dro.Data();
		d2.add(d3);
		java.util.List<dro.Data> list = dro.data.Tool.asList(d1);
		Assert.assertTrue(3 == list.size());
	}
	
	@Test // For purposes of differencing
	public void test_0004() {
		final java.lang.Object o1 = new java.lang.Object();
		final java.lang.Object o2 = new java.lang.Object();
		final dro.Data diff = dro.data.Tool.asDiff(o1, o2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = false;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
		{
			final boolean expected = true;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-not-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	@Test // For purposes of differencing
	public void test_0005() {
		final dro.Data d1 = new dro.Data();
		final dro.Data d2 = new dro.Data();
		final dro.Data diff = dro.data.Tool.asDiff(d1, d2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = true;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	@Test // For purposes of differencing
	public void test_0006() {
		final dro.Data d1 = new dro.Data();
		d1.set("key", "value");
		final dro.Data d2 = new dro.Data();
		d2.set("key", "value");
		final dro.Data diff = dro.data.Tool.asDiff(d1, d2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = true;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	@Test // For purposes of differencing
	public void test_0007() {
		final dro.Data d1 = new dro.Data();
		d1.set("key1", "value");
		final dro.Data d2 = new dro.Data();
		d2.set("key2", "value");
		final dro.Data diff = dro.data.Tool.asDiff(d1, d2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = false;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	@Test // For purposes of differencing
	public void test_0008() {
		final dro.Data d1 = new dro.Data();
		d1.set("key", "value1");
		final dro.Data d2 = new dro.Data();
		d2.set("key", "value2");
		final dro.Data diff = dro.data.Tool.asDiff(d1, d2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = false;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	@Test // For purposes of differencing
	public void test_0009() {
		final dro.Data d1 = new dro.Data();
		d1.set("key", new java.util.HashMap<java.lang.String, java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				put("a", 1);
			}
		});
		final dro.Data d2 = new dro.Data();
		d2.set("key", new java.util.HashMap<java.lang.String, java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				put("a", 1);
			}
		});
		final dro.Data diff = dro.data.Tool.asDiff(d1, d2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = true;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	@Test // For purposes of differencing
	public void test_0010() {
		final dro.Data d1 = new dro.Data();
		d1.set("key", new java.util.HashMap<java.lang.String, java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				put("a", 1);
			}
		});
		final dro.Data d2 = new dro.Data();
		d1.set("key", new java.util.HashMap<java.lang.String, java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				put("a", 2);
			}
		});
		final dro.Data diff = dro.data.Tool.asDiff(d1, d2);
		{
			final boolean expected = true;
			final boolean actual = null != diff;
			Assert.assertEquals(expected, actual);
		}
		{
			final boolean expected = false;
			final java.lang.Boolean actual = ((Boolean)diff.get("1-is-2"));
			Assert.assertEquals(expected, null != actual && true == actual.booleanValue());
		}
	}
	
	@Test // For purposes of proxying
	public void test_0011() {
		final dro.Data d = new dro.Data();
		d.set("key", "value");
		final dro.Data d_ = d.clone();
		Assert.assertNotNull(d_);
		final java.lang.String s = (java.lang.String)d_.get("key");
		Assert.assertNotNull(s);
		Assert.assertEquals((java.lang.String)d.get("key"), s);
	}
	
	@Test
	public void test_0012() {
		final dro.meta.Data md = new dro.meta.Data("/etc/passwd");
		final java.lang.String[] fieldNames = new java.lang.String[]{"username","password","uid","gid","comment","home","shell"};
		final java.lang.Class<?>[] fieldTypes = new java.lang.Class<?>[]{java.lang.String.class,java.lang.String.class,int.class,int.class,java.lang.String.class,java.lang.String.class,java.lang.String.class};
		for (int f = 0; f < fieldNames.length; f++) {
			md.add(new dro.meta.data.Field(fieldNames[f]).set("$type", fieldTypes[f])); // TODO: we want the field type to be int (for uid, gid, etc.) but to do so we need a transformer
		};
		final java.lang.String _etc_passwd = java.lang.String.join("\n"
		   ,"root:x:0:0:root:/root:/bin/bash"
		   ,"bin:x:1:1:bin:/bin:/sbin/nologin"
		   ,"daemon:x:2:2:daemon:/sbin:/sbin/nologin"
		   ,"adm:x:3:4:adm:/var/adm:/sbin/nologin"
		   ,"lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin"
		   ,"sync:x:5:0:sync:/sbin:/bin/sync"
		   ,"shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown"
		   ,"halt:x:7:0:halt:/sbin:/sbin/halt"
		   ,"mail:x:8:12:mail:/var/spool/mail:/sbin/nologin"
		   ,"operator:x:11:0:operator:/root:/sbin/nologin"
		   ,"games:x:12:100:games:/usr/games:/sbin/nologin"
		   ,"ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin"
		   ,"nobody:x:99:99:Nobody:/:/sbin/nologin"
		   ,"avahi-autoipd:x:170:170:Avahi IPv4LL Stack:/var/lib/avahi-autoipd:/sbin/nologin"
		   ,"dbus:x:81:81:System message bus:/:/sbin/nologin"
		   ,"abrt:x:173:173::/etc/abrt:/sbin/nologin"
		   ,"polkitd:x:999:998:User for polkitd:/:/sbin/nologin"
		   ,"tss:x:59:59:Account used by the trousers package to sandbox the tcsd daemon:/dev/null:/sbin/nologin"
		   ,"postfix:x:89:89::/var/spool/postfix:/sbin/nologin"
		   ,"sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin"
		);
		// TODO: move from dro.lang.String static method to Transformer class
		final java.lang.String[][] fieldValues = dro.lang.String.fields(
			dro.lang.String.lines(java.lang.String.join("\n", _etc_passwd), "\\n"), "\\:"
		);
		for (int y = 0; y < fieldValues.length; y++) {
			dro.Data d = new dro.Data(md);
			for (int x = 0; x < fieldValues[y].length; x++) {
				d.set(fieldNames[x], fieldValues[y][x]);
			}
		}
		{
			final java.util.List<dro.Data> dl = md.get(
				new dro.meta.Data.Boolean.Expression("username", dro.meta.Data.Boolean.Expression.EQ, "*")
			);
			Assert.assertSame(20, dl.size());
		}
		{
			final java.util.List<dro.Data> dl = md.get(
				new dro.meta.Data.Boolean.Expression("gid", dro.meta.Data.Boolean.Expression.EQ, 0)
			);
			Assert.assertSame(5, dl.size());
		}
	}
	
	@After
	public void after() {
	}
}