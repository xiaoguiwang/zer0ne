package dro;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.lang.Event.Listener;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EventTest {
	@Test
	public void test_0000() throws InterruptedException {
		final dro.lang.Boolean done = new dro.lang.Boolean(false);
		dro.lang.Event.register(
			new dro.lang.Event.Listener(){
				@Override
				public Listener dispatch(final dro.lang.Event e) {
				  //final dro.data.Event de = e.event();
					if (true == e.methodName().equals("exiting")) {
						done.value(true);
					}
					return this;
				}
			},
			new java.lang.Class[]{dro.Data.class}, new java.lang.Object[]{(dro.Data)null}, new java.lang.String[]{"entered", "exiting"}
		);
		long ms = System.currentTimeMillis();
	  /*final dro.Data d = */new dro.Data();
		while (false == done.value() && System.currentTimeMillis()-ms<1000L) {
			Thread.sleep(100L);
		}
		Assert.assertEquals(true, done.value());
	}
	
	@Test
	public void test_0001() throws InterruptedException {
		final dro.lang.Boolean done = new dro.lang.Boolean(false);
		dro.lang.Event.register(
			new dro.lang.Event.Listener(){
				@Override
				public Listener dispatch(final dro.lang.Event e) {
				  //final dro.data.Event de = e.event();
					if (true == e.methodName().equals("exiting")) {
						done.value(true);
					}
					return this;
				}
			},
			new java.lang.Class[]{dro.Data.class}, new java.lang.Object[]{(dro.Data)null}, new java.lang.String[]{"entered", "exiting"}
		);
		long ms = System.currentTimeMillis();
		final dro.meta.Data md = new dro.meta.Data();
	  /*final dro.Data d = */new dro.Data(md);
		while (false == done.value() && System.currentTimeMillis()-ms<1000L) {
			Thread.sleep(100L);
		}
		Assert.assertEquals(true, done.value());
	}
}