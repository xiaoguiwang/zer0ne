package dro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SerializerTest {
  /*public SerializerTest() {
	}*/
  /*@Before
	public void before() {
	}*/
  /*@After
	public void after() {
	}*/
	
  //@Test
	public void test_0000() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise((java.lang.Object)null);
		Assert.assertTrue(list != null);
		Assert.assertSame(1, list.size());
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.Object.class);
			Assert.assertTrue(__o.returns(dro.lang.Object.Return.Object) == (java.lang.Object)null);
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0001() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise((short)1);
		Assert.assertTrue(list != null);
		Assert.assertSame(1, list.size());
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == short.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.Short);
			final java.lang.Short v = (java.lang.Short)o;
			Assert.assertTrue((short)1 == v.shortValue());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0002() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new java.lang.Integer((int)1));
		Assert.assertTrue(list != null);
		Assert.assertSame(1, list.size());
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.Integer.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.Integer);
			final java.lang.Integer v = (java.lang.Integer)o;
			Assert.assertSame((int)1, v.intValue());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0003() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new java.lang.String[]{"Hello World!"});
		Assert.assertTrue(list != null);
		Assert.assertSame(2, list.size());
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.String[].class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.String[]);
			final java.lang.String v[] = (java.lang.String[])o;
			Assert.assertTrue(v != null);
			Assert.assertSame(1, v.length);
			Assert.assertTrue(true == v[0].equals("Hello World!"));
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			final java.util.List<dro.lang.Object> __to = __o.references();
			Assert.assertTrue(__to != null);
			Assert.assertSame(1, __to.size());
		}
		{
			final dro.lang.Object __o = list.get(1);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.String.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.String);
			final java.lang.String s = (java.lang.String)o;
			Assert.assertTrue(true == s.equals("Hello World!"));
			final java.util.List<dro.lang.Object> __by = __o.referenced();
			Assert.assertTrue(__by != null);
			Assert.assertSame(1, __by.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0004() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new ArrayList<java.lang.String>(){
			private static final long serialVersionUID = 1L;
			{
				add(new java.lang.String("Hello World!"));
			}
		});
		Assert.assertTrue(list != null);
		Assert.assertSame(2, list.size() );
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Object.Return.Object) instanceof ArrayList);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof ArrayList);
			final ArrayList<?> v = (ArrayList<?>)o;
			Assert.assertTrue(v != null);
			Assert.assertSame(1, v.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			final java.util.List<dro.lang.Object> __to = __o.references();
			Assert.assertTrue(__to != null);
			Assert.assertSame(1, __to.size());
		}
		{
			final dro.lang.Object __o = list.get(1);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.String.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.String);
			final java.lang.String s = (java.lang.String)o;
			Assert.assertTrue(true == s.equals("Hello World!"));
			final java.util.List<dro.lang.Object> __by = __o.referenced();
			Assert.assertTrue(__by != null);
			Assert.assertSame(1, __by.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0005() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new HashSet<java.lang.String>(){
			private static final long serialVersionUID = 1L;
			{
				add(new java.lang.String("Hello World!"));
			}
		});
		Assert.assertTrue(list != null);
		Assert.assertSame(2, list.size());
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Object.Return.Object) instanceof HashSet);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof HashSet);
			final HashSet<?> v = (HashSet<?>)o;
			Assert.assertTrue(v != null);
			Assert.assertSame(1, v.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			final java.util.List<dro.lang.Object> __to = __o.references();
			Assert.assertTrue(__to != null);
			Assert.assertSame(1, __to.size());
		}
		{
			final dro.lang.Object __o = list.get(1);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.String.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.String);
			final java.lang.String s = (java.lang.String)o;
			Assert.assertTrue(true == s.equals("Hello World!"));
			final java.util.List<dro.lang.Object> __by = __o.referenced();
			Assert.assertTrue(__by != null);
			Assert.assertSame(1, __by.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0006() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new HashMap<java.lang.Object,java.lang.Object>(){
			private static final long serialVersionUID = 1L;
			{
				put(new java.lang.String("Hello"), new java.lang.String("World!"));
			}
		});
		Assert.assertTrue(list != null);
		Assert.assertSame(3, list.size());
		{
			final dro.lang.Object __o = list.get(0);
			Assert.assertTrue(__o.returns(dro.lang.Object.Return.Object) instanceof HashMap);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof HashMap);
			final HashMap<?,?> v = (HashMap<?,?>)o;
			Assert.assertTrue(v != null);
			Assert.assertSame(1, v.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.referenced());
			final java.util.List<dro.lang.Object> __to = __o.references();
			Assert.assertTrue(__to != null);
			Assert.assertSame(1, __to.size());
		}
		{
			final dro.lang.Object __o = list.get(1);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.String.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.String);
			final java.lang.String s = (java.lang.String)o;
			Assert.assertTrue(true == s.equals("Hello"));
			final java.util.List<dro.lang.Object> __by = __o.referenced();
			Assert.assertTrue(__by != null);
			Assert.assertSame(1, __by.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
		{
			final dro.lang.Object __o = list.get(2);
			Assert.assertTrue(__o.returns(dro.lang.Class.Return.Class) == java.lang.String.class);
			final java.lang.Object o = __o.returns(dro.lang.Object.Return.Object);
			Assert.assertTrue(o instanceof java.lang.String);
			final java.lang.String s = (java.lang.String)o;
			Assert.assertTrue(true == s.equals("World!"));
			final java.util.List<dro.lang.Object> __by = __o.referenced();
			Assert.assertTrue(__by != null);
			Assert.assertSame(1, __by.size());
			Assert.assertTrue((java.util.List<dro.lang.Object>)null == __o.references());
		}
	}
  //@Test
	public void test_0007() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new HashMap<java.lang.Object,java.lang.Object>(){
			private static final long serialVersionUID = 1L;
			{
				put(new java.lang.Double(1.0d), 2.0d);
			}
		});
		Assert.assertTrue(list != null);
		Assert.assertSame(3, list.size());
	}
  //@Test
	public void test_0008() {
		java.util.List<dro.lang.Object> list = new Serializer().serialise(new HashMap<java.lang.Object,java.lang.Object>(){
			private static final long serialVersionUID = 1L;
			{
				final java.lang.String hello = new java.lang.String("Hello");
				put(hello, new java.lang.String("World!"));
				put(hello, new java.lang.String("There?"));
			}
		});
		Assert.assertTrue(list != null);
		Assert.assertSame(3, list.size());
		for (final dro.lang.Object to: list) {
		  //final java.lang.Class<?> c = to.returns(dro.lang.Class.Return.Class);
			final java.lang.Object o = to.returns(dro.lang.Object.Return.Object);
System.out.println(dro.lang.Class.getName(to.type())+":"+o.toString()+":#"+to.i);
		}
	}
	@Test
	public void test_0009() {
		final java.util.Map<java.lang.String, java.lang.Object> parent = new HashMap<>();
		
		final java.util.Map<java.lang.String, java.lang.Object> child1 = new HashMap<>();
		child1.put("parent", parent);
		
		final java.util.Map<java.lang.String, java.lang.Object> child2 = new HashMap<>();
		child2.put("parent", parent);
		
		final java.util.List<java.util.Map<?,?>> children = new ArrayList<>();
		children.add(child1);
		children.add(child2);
		
		parent.put("children", children);
		
		java.util.List<dro.lang.Object> list = new Serializer().serialise(parent);
		Assert.assertTrue(list != null);
		Assert.assertSame(4, list.size());
		for (final dro.lang.Object to: list) {
		  //final java.lang.Class<?> c = to.returns(dro.lang.Class.Return.Class);
			final java.lang.Object o = to.returns(dro.lang.Object.Return.Object);
		  /*final int hashCode = */dro.lang.Object.hashCode(o);
		}
	}
}