package dro.data;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContextTest {
	@Test
	public void test_0000() {
	  /*final dro.data.Context dc = */new dro.data.Context();
	}
	
	@Test
	public void test_0001() {
		final dro.data.Context dc = new dro.data.Context();
		final dro.Data d = new dro.Data();
		dc.add(d);
		final java.util.Set<dro.Data> set = dc.returns(dro.Data.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(d));
	}
	@Test
	public void test_0002() {
		final dro.data.Context dc = new dro.data.Context();
		final dro.Data d = new dro.Data();
		dc.add(d);
		boolean failed = false;
		try {
			dc.add(d);
		} catch (final RuntimeException e) {
			failed = true;
		}
		Assert.assertEquals(true, failed);
	}
	
	@Test
	public void test_0003() {
		final dro.data.Context dc1  = new dro.data.Context();
		final dro.Data d = new dro.Data();
		d.set("key", "value"); // Note: you are free to change this to another value and the/this test should not fail
		dc1.add(d);
		final dro.data.Context dc2 = new dro.data.Context();
	  //dc1.add(dc2);
	  /*@SuppressWarnings("unchecked")
		final java.util.Set<java.lang.Object> set = (java.util.Set<java.lang.Object>)dc1.keys(dro.Data.Return.Set);
		Assert.assertTrue(1 == set.size());
		Assert.assertTrue(set.contains(dc1));*/
		// And support this mode too, or support this mode instead:
	  //dc2.add(dc1);
		final dro.Data _d = dc2.add(d, dro.Data.Return.Data); // This creates a proxy for d in dc2 but d still lives in dc1 (until it's formally moved)
		Assert.assertNotNull(_d);
		{
			final java.lang.String s = (java.lang.String)_d.get("key");
			Assert.assertNotNull(s);
			Assert.assertEquals((java.lang.String)_d.get("key"), s);
		}
		{
			_d.set("key", "value2");
			final java.lang.String s = (java.lang.String)d.get("key");
			Assert.assertNotNull(s);
			Assert.assertEquals((java.lang.String)_d.get("key"), s);
		}
		{
			d.set("key2", "value2");
			final boolean has = _d.has("key2");
			Assert.assertEquals(true, has);
		}
		{
		  //d.set("key3", "value3");
			final boolean has = _d.has("key3");
			Assert.assertEquals(false, has);
		}
	  //dc1.remove(d); // This moves d from dc1 to dc2 as we have somewhere for d to go as d exists in dc2 as a proxy
		// This makes it a really simply really slick solution
	}
	
	@Test // TODO
	public void test_0004() {
		final dro.data.Context dc  = new dro.data.Context();
		final dro.data.Context dc_ = new dro.data.Context();
	/**/dc.add(dc_); // Intro the two contexts to each other..
	}
	
	@Test
	public void test_0005() {
		final dro.data.Context dc  = new dro.data.Context();
		dc.add(new dro.Data().set("key", "value" ));
		dc.add(new dro.Data().set("key", "value2"));
		dc.add(new dro.Data().set("key", "value23"));
		{
			final java.util.Set<dro.Data> set = dc.returns("key", "EQ", "value*", dro.Data.Return.Set);
			Assert.assertNotNull(set);
			Assert.assertEquals(3, set.size()); // TODO: ensure all other *Test.java use the 'expected' form vs. 'actual'
		}
		{
			final java.util.Set<dro.Data> set = dc.returns("key", "EQ", "value2*", dro.Data.Return.Set);
			Assert.assertNotNull(set);
			Assert.assertEquals(2, set.size()); // TODO: ensure all other *Test.java use the 'expected' form vs. 'actual'
		}
		{
			final java.util.Set<dro.Data> set = dc.returns("key", "EQ", "value*3", dro.Data.Return.Set);
			Assert.assertNotNull(set);
			Assert.assertEquals(1, set.size()); // TODO: ensure all other *Test.java use the 'expected' form vs. 'actual'
		}
		{
			final java.util.Set<dro.Data> set = dc.returns("key", "EQ", "value4", dro.Data.Return.Set);
			Assert.assertNull(set);
		}
		{
			final java.util.Set<dro.Data> set = dc.returns("value", "EQ", "key", dro.Data.Return.Set);
			Assert.assertNull(set);
		}
	}
}