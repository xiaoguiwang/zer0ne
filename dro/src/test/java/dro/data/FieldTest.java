package dro.data;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldTest {
	@Test
	public void test_0000() {
	  /*final dro.data.Field df = */new dro.data.Field();
	}
	
	@Test
	public void test_0001() {
		final dro.Data d = new dro.Data();
		final dro.data.Field df = new dro.data.Field();
		d.add(df);
		final java.util.Set<dro.data.Field> set = d.returns(dro.data.Field.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(df));
	}
}