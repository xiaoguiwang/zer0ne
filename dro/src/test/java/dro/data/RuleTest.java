package dro.data;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RuleTest {
	@Test
	public void test_0000() {
	  /*final dro.data.Rule dr = new dro.data.Rule();*/
	}
	
	@Test
	public void test_0001() {
		final dro.Data d = new dro.Data();
		final dro.data.Rule dr = new dro.data.Rule();
		d.add(dr);
		final java.util.Set<dro.data.Rule> set = new HashSet<>();//d.returns(dro.data.Rule.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(dr));
	}
}