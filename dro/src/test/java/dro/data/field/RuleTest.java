package dro.data.field;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RuleTest {
	@Test
	public void test_0000() {
	  /*final dro.data.field.Rule dfr = new dro.data.field.Rule();*/
	}
	
	@Test
	public void test_0001() {
		final dro.Data d = new dro.Data();
		final dro.data.Field df = new dro.data.Field();
		d.add(df);
		final dro.data.field.Rule dfr = new dro.data.field.Rule();
		df.add(dfr);
		final java.util.Set<dro.data.field.Rule> set = new HashSet<>();//df.returns(dro.data.field.Rule.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(dfr));
	}
}