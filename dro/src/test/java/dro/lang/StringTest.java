package dro.lang;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StringTest {
	private static final java.lang.String[] fieldNames = new java.lang.String[]{"username","password","uid","gid","comment","home","shell"};
	
	private static final java.lang.String _etc_passwd = java.lang.String.join("\n"
	   ,"root:x:0:0:root:/root:/bin/bash"
	   ,"bin:x:1:1:bin:/bin:/sbin/nologin"
	   ,"daemon:x:2:2:daemon:/sbin:/sbin/nologin"
	   ,"adm:x:3:4:adm:/var/adm:/sbin/nologin"
	   ,"lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin"
	   ,"sync:x:5:0:sync:/sbin:/bin/sync"
	   ,"shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown"
	   ,"halt:x:7:0:halt:/sbin:/sbin/halt"
	   ,"mail:x:8:12:mail:/var/spool/mail:/sbin/nologin"
	   ,"operator:x:11:0:operator:/root:/sbin/nologin"
	   ,"games:x:12:100:games:/usr/games:/sbin/nologin"
	   ,"ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin"
	   ,"nobody:x:99:99:Nobody:/:/sbin/nologin"
	   ,"avahi-autoipd:x:170:170:Avahi IPv4LL Stack:/var/lib/avahi-autoipd:/sbin/nologin"
	   ,"dbus:x:81:81:System message bus:/:/sbin/nologin"
	   ,"abrt:x:173:173::/etc/abrt:/sbin/nologin"
	   ,"polkitd:x:999:998:User for polkitd:/:/sbin/nologin"
	   ,"tss:x:59:59:Account used by the trousers package to sandbox the tcsd daemon:/dev/null:/sbin/nologin"
	   ,"postfix:x:89:89::/var/spool/postfix:/sbin/nologin"
	   ,"sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin"
	);
	
	@Test
	public void test_A() {
		System.out.println(dro.lang.String.resolve("Hello ${There} World!"));
	}
	@Test
	public void test_B() {
		System.out.println(dro.lang.String.resolve("Hello ${There} World!", new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("There", "My");
			}
		}));
	}
	
	@Test
	public void test_0000() {
		final java.lang.String[] lines = dro.lang.String.lines(_etc_passwd, "\\n");
		Assert.assertSame(20, lines.length);
		Assert.assertSame(true, lines[19].startsWith("sshd:"));
	}
	
	@Test
	public void test_0001() {
		final java.lang.String[][] fields = dro.lang.String.fields(
			dro.lang.String.lines(java.lang.String.join("\n", _etc_passwd), "\\n"), "\\:"
		);
		Assert.assertSame(7, fields[0].length);
		Assert.assertSame(true, fields[19][0].equals("sshd"));
	}
	
	private java.lang.String[] getFieldIndexValues(final java.lang.String[][] fieldValues, final int i) {
		final java.lang.String[] fieldIndexValues = new java.lang.String[fieldValues.length];
		for (int j = 0; j < fieldIndexValues.length; j++) {
			fieldIndexValues[j] = fieldValues[j][i];
		}
		return fieldIndexValues;
	}
	private java.lang.String[] getFieldNameValues(final java.util.Map<java.lang.String, java.lang.Integer> fieldNameToIndexMap, final java.lang.String[][] fieldValues, final java.lang.String fieldName) {
		final int i = fieldNameToIndexMap.get(fieldName);
		return getFieldIndexValues(fieldValues, i);
	}
	
	private java.lang.String getFieldValue(final java.lang.String[][] fieldValues, final int j, final int i) {
		return fieldValues[j][i];
	}
	private java.lang.String getFieldValue(final java.util.Map<java.lang.String, java.lang.Integer> fieldNameToIndexMap, final java.lang.String[][] fieldValues, final int j, final java.lang.String fieldName) {
		return getFieldValue(fieldValues, j, fieldNameToIndexMap.get(fieldName));
	}
	
	private java.lang.String[] getFieldValues(final java.util.Map<java.lang.String, java.lang.Integer> fieldNameToIndexMap, final java.lang.String[][] fieldValues, final Expression e) {
		final java.lang.String[] fieldNameValues = getFieldNameValues(fieldNameToIndexMap, fieldValues, e.fieldName);
		final java.util.List<java.lang.String> fieldNameValueMatches = new ArrayList<java.lang.String>();
		for (final java.lang.String fieldNameValue: fieldNameValues) {
		  /*final */boolean truth = false;
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (Expression.EQ == e.expression) {
				if (true == fieldNameValue.equals(e.fieldValue)) {
					truth = true;
				}
			} else if (Expression.NE == e.expression) {
				if (false == fieldNameValue.equals(e.fieldValue)) {
					truth = true;
				}
			}
			if (true == truth) {
				fieldNameValueMatches.add(fieldNameValue);
			}
		}
		return fieldNameValueMatches.toArray(new java.lang.String[0]);
	}
	
	@Test
	public void test_0002() {
		final java.lang.String[][] fieldValues = dro.lang.String.fields(
			dro.lang.String.lines(java.lang.String.join("\n", _etc_passwd), "\\n"), "\\:"
		);
		final java.util.Map<java.lang.Integer, java.lang.String> fieldIndexToNameMap = new java.util.HashMap<>();
		final java.util.Map<java.lang.String, java.lang.Integer> fieldNameToIndexMap = new java.util.HashMap<>();
		for (int f = 0; f < fieldNames.length; f++) {
			fieldIndexToNameMap.put(new java.lang.Integer(f), fieldNames[f]);
			fieldNameToIndexMap.put(fieldNames[f], new java.lang.Integer(f));
		}
		final java.lang.String username = getFieldValue(fieldNameToIndexMap, fieldValues, 19, "username");
		Assert.assertEquals("sshd", username);
	}
	
	private static class Expression {
		private static final Expression EQ = new Expression();
		private static final Expression NE = new Expression();
		private final java.lang.String fieldName;// = null;
		private final Expression expression;// = null;
		private final java.lang.String fieldValue;// = null;
		{
		  //fieldName = null;
		  //expression = null;
		  //fieldValue = null;
		}
		private Expression() {
			fieldName = null;
			expression = null;
			fieldValue = null;
		}
		private Expression(final java.lang.String fieldName, final Expression expression, final java.lang.String fieldValue) {
			this.fieldName = fieldName;
			this.expression = expression;
			this.fieldValue = fieldValue;
		}
	}
	// sc wave-sc (next-row) sc dc dc-wave
	@Test
	public void test_0003() {
		final java.lang.String[][] fieldValues = dro.lang.String.fields(
			dro.lang.String.lines(java.lang.String.join("\n", _etc_passwd), "\\n"), "\\:"
		);
		final java.util.Map<java.lang.Integer, java.lang.String> fieldIndexToNameMap = new java.util.HashMap<>();
		final java.util.Map<java.lang.String, java.lang.Integer> fieldNameToIndexMap = new java.util.HashMap<>();
		for (int f = 0; f < fieldNames.length; f++) {
			fieldIndexToNameMap.put(new java.lang.Integer(f), fieldNames[f]);
			fieldNameToIndexMap.put(fieldNames[f], new java.lang.Integer(f));
		}
		{
			final java.lang.String[] gid = getFieldValues(fieldNameToIndexMap, fieldValues, new Expression("gid", Expression.EQ, "0"));
			Assert.assertEquals(5, gid.length);
		}
		{
			final java.lang.String[] gid = getFieldValues(fieldNameToIndexMap, fieldValues, new Expression("gid", Expression.NE, "0"));
			Assert.assertEquals(15, gid.length);
		}
	}
}