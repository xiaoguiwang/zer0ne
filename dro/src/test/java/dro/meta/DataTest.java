package dro.meta;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataTest {
	@Test
	public void test_0000() {
	  /*final dro.meta.Data md = */new dro.meta.Data();
	}
	
	@Test
	public void test_0001() {
	  /*final dro.meta.Data md = */new dro.meta.Data("my-meta-data");
	}
	
	@Test
	public void test_0002() {
		final dro.meta.Data md = new dro.meta.Data("my-meta-data");
		final dro.Data d = new dro.Data(md);
		Assert.assertTrue(md == d.returns(dro.meta.Data.Return.Data));
		{
			final java.util.Set<dro.Data> set = md.returns(dro.Data.Return.Set);
			Assert.assertNotEquals((java.util.Set<?>)null, set);
			Assert.assertEquals(1, set.size());
			Assert.assertEquals(true, set.contains(d));
		}
	  /*{
			final java.util.List<dro.Data> list = md.returns(dro.Data.Return.List);
			Assert.assertNotEquals((java.util.List<?>)null, list);
			Assert.assertEquals(1, list.size());
			Assert.assertEquals(d, list.get(0).returns(dro.Data.Return.Data));
		}*/
	}

}