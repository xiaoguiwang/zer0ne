package dro.meta.data;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContextTest {
	@Test
	public void test_0000() {
	  /*final dro.meta.data.Context mdc = */new dro.meta.data.Context();
	}
	@Test
	public void test_0001() {
		final dro.meta.data.Context mdc = new dro.meta.data.Context();
	  /*final dro.data.Context dc = */new dro.data.Context(mdc);
	}
	@Test
	public void test_0002() {
		final dro.meta.data.Context mdc = new dro.meta.data.Context();
		final dro.data.Context dc = new dro.data.Context(mdc);
		boolean failed = false;
		try {
			mdc.add(dc);
		} catch (final RuntimeException e) {
			failed = true;
		}
		Assert.assertEquals(true, failed);
	}
}