package dro.meta.data;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldTest {
	@Test
	public void test_0000() {
	  /*final dro.meta.data.Field mdf = */new dro.meta.data.Field("my-meta-data-field");
	}
	
	@Test
	public void test_0001() {
		final dro.meta.Data md = new dro.meta.Data("my-meta-data");
		final dro.meta.data.Field mdf = new dro.meta.data.Field("my-meta-data-field");
		md.add(mdf);
		final java.util.Set<dro.meta.data.Field> set = md.returns(dro.meta.data.Field.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(mdf));
	}
}