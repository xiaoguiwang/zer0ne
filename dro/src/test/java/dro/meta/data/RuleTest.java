package dro.meta.data;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RuleTest {
	@Test
	public void test_0000() {
	  /*final dro.meta.data.Rule mdr = new dro.meta.data.Rule();*/
	}
	
	@Test
	public void test_0001() {
		final dro.meta.Data md = new dro.meta.Data();
		final dro.meta.data.Rule mdr = new dro.meta.data.Rule();
		md.add(mdr);
		final java.util.Set<dro.data.Rule> set = new HashSet<>();//md.returns(dro.data.Rule.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(mdr));
	}
}