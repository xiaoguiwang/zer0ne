package dro.meta.data.field;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RuleTest {
	@Test
	public void test_0000() {
	  /*final dro.meta.data.field.Rule mdfr = new dro.meta.data.field.Rule();*/
	}
	
	@Test
	public void test_0001() {
		final dro.meta.Data md = new dro.meta.Data();
		final dro.meta.data.Field mdf = new dro.meta.data.Field();
		md.add(mdf);
		final dro.meta.data.field.Rule mdfr = new dro.meta.data.field.Rule();
		mdf.add(mdfr);
		final java.util.Set<dro.meta.data.field.Rule> set = new HashSet<>();//mdf.returns(dro.meta.data.field.Rule.Return.Set);
		Assert.assertNotEquals((java.util.Set<?>)null, set);
		Assert.assertEquals(1, set.size());
		Assert.assertEquals(true, set.contains(mdfr));
	}
}