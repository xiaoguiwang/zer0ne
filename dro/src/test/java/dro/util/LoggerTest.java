package dro.util;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoggerTest {
	@Test
	public void test_0000() {
		java.util.logging.Logger logger = Logger.getLogger(LoggerTest.class);
		logger.finest("Hello World!");
	}
}