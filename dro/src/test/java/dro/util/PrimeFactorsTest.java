package dro.util;

import java.math.BigInteger;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PrimeFactorsTest {
	
	private static void systemOutPrintln(final Integer i, final List<Integer> primeFactors) {
	  /*final */StringBuffer sb = null;
		if (null != i && 1 < i.intValue()) {
			if (null != primeFactors && 0 < primeFactors.size()) {
				for (final Integer primeFactor: primeFactors) {
					if (null == sb) {
						sb = new StringBuffer();
					} else {
						sb.append(",");
					}
					sb.append(primeFactor);
				}
			}
			if (true == PrimeFactors.primes.contains(i)) {
				System.out.println(i+" is prime");
			} else if (null != primeFactors && 0 < primeFactors.size()) {
				System.out.println(i+" is not prime - prime factors: "+sb.toString());
			}
		}
	}
	private static void systemOutPrintln(final BigInteger i, List<BigInteger> primeFactors) {
	  /*final */StringBuffer sb = null;
		if (null != i && 1 < i.intValue()) {
			if (null != primeFactors && 0 < primeFactors.size()) {
				for (final BigInteger primeFactor: primeFactors) {
					if (null == sb) {
						sb = new StringBuffer();
					} else {
						sb.append(",");
					}
					sb.append(primeFactor.toString());
				}
			}
			if (true == PrimeFactors.__primes.contains(i)) {
				System.out.println(i+" is prime");
			} else if (null != primeFactors && 0 < primeFactors.size()) {
				System.out.println(i+" is not prime - prime factors: "+sb.toString());
			}
		}
	}
	
	@Test
	public void test_1() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(1);
		PrimeFactorsTest.systemOutPrintln(1, primeFactors);
	}
	@Test
	public void test_2() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(2);
		PrimeFactorsTest.systemOutPrintln(2, primeFactors);
	}
	@Test
	public void test_3() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(3);
		PrimeFactorsTest.systemOutPrintln(3, primeFactors);
	}
	@Test
	public void test_4() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(4);
		PrimeFactorsTest.systemOutPrintln(4, primeFactors);
	}
	@Test
	public void test_5() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(5);
		PrimeFactorsTest.systemOutPrintln(5, primeFactors);
	}
	@Test
	public void test_6() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(6);
		PrimeFactorsTest.systemOutPrintln(6, primeFactors);
	}
	@Test
	public void test_7() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(7);
		PrimeFactorsTest.systemOutPrintln(7, primeFactors);
	}
	@Test
	public void test_8() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(8);
		PrimeFactorsTest.systemOutPrintln(8, primeFactors);
	}
	@Test
	public void test_9() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(9);
		PrimeFactorsTest.systemOutPrintln(9, primeFactors);
	}
	@Test
	public void test_10() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(10);
		PrimeFactorsTest.systemOutPrintln(10, primeFactors);
	}
	@Test
	public void test_11() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(11);
		PrimeFactorsTest.systemOutPrintln(11, primeFactors);
	}
	@Test
	public void test_12() { // 12 is not prime - prime factors: 2,2,3
		final List<Integer> primeFactors = PrimeFactors.primeFactors(12);
		PrimeFactorsTest.systemOutPrintln(12, primeFactors);
	}
	@Test // 1000 is not prime - prime factors: 2,2,2,5,5,5
	public void test_1000() {
		final List<Integer> primeFactors = PrimeFactors.primeFactors(1000);
		PrimeFactorsTest.systemOutPrintln(1000, primeFactors);
	}
	@Test // 1000 is not prime - prime factors: 2,2,2,5,5,5
	public void test_BigInteger_1000() {
		final BigInteger BigInteger_1000 = new BigInteger("1000");
		final List<BigInteger> primeFactors = PrimeFactors.primeFactors(BigInteger_1000);
		PrimeFactorsTest.systemOutPrintln(BigInteger_1000, primeFactors);
	}
	@Test // 2147483646 is not prime - prime factors: 2,3,3,7,11,31,151,331
	public void test_Integer_MAXVALUE() {
		final int Integer_MAXVALUE = Integer.MAX_VALUE-1;
		final List<Integer> primeFactors = PrimeFactors.primeFactors(Integer_MAXVALUE);
		PrimeFactorsTest.systemOutPrintln(Integer_MAXVALUE, primeFactors);
	}
	@Test // 2147483646 is not prime - prime factors: 2,3,3,7,11,31,151,331
	public void test_BigInteger_2147483646() {
		final BigInteger BigInteger_2147483646 = new BigInteger("2147483646");
		final List<BigInteger> primeFactors = PrimeFactors.primeFactors(BigInteger_2147483646);
		PrimeFactorsTest.systemOutPrintln(BigInteger_2147483646, primeFactors);
	}
	@Test // 123456789012345678901234567890 is not prime - prime factors: 2,3,3,3,5,7,13,31,37,211,241,2161,3607,3803,2906161
	public void test_BigInteger_BIG() {
		final BigInteger __BigInteger = new BigInteger("123456789012345678901234567890");
		final List<BigInteger> primeFactors = PrimeFactors.primeFactors(__BigInteger);
		PrimeFactorsTest.systemOutPrintln(__BigInteger, primeFactors);
	}
	@Test
	public void test_BigInteger_BIGGER() {
		final BigInteger __BigInteger = new BigInteger("123456789012345678901234567891");
		final List<BigInteger> primeFactors = PrimeFactors.primeFactors(__BigInteger);
		PrimeFactorsTest.systemOutPrintln(__BigInteger, primeFactors);
	}
	
	@Test
	public void test() {
		for (int i = 1; i <= 2147483646; i++) {
			final List<Integer> primeFactors = PrimeFactors.primeFactors(i);
			PrimeFactorsTest.systemOutPrintln(i, primeFactors);
		}
	}
}