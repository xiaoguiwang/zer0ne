package dro.util;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PropertiesTest {
	@Test
	public void test_0000() {
		final dro.util.Properties p = new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				setProperty("Hello", "World!");
			}
		};
		final java.lang.String expected = "World!";
		final java.lang.String actual = dro.lang.String.resolve("${Hello}", p);
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void test_0001() {
		final java.lang.String expected = "Hello.";
		final java.lang.String actual = dro.lang.String.resolve(expected);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void test_0002() {
		final dro.util.Properties p = new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				setProperty("Hello", "World!");
			}
		};
		final java.lang.String expected = "World!";
		final java.lang.String actual = dro.lang.String.resolve("${Hello}", p);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void test_0003() {
		final dro.util.Properties p = new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				setProperty("a1", "b");
				setProperty("b2", "1");
				setProperty("c3", "2");
				setProperty("a12", "x");
			}
		};
		{
			final java.lang.String expected = "2";
			final java.lang.String actual = dro.lang.String.resolve("${c3}", p);
			Assert.assertEquals(expected, actual);
		}
		{
			final java.lang.String expected = "1";
			final java.lang.String actual = dro.lang.String.resolve("${b${c3}}", p);
			Assert.assertEquals(expected, actual);
		}
		{
			final java.lang.String expected = "b";
			final java.lang.String actual = dro.lang.String.resolve("${a${b${c3}}}", p);
			Assert.assertEquals(expected, actual);
		}
		{
			final java.lang.String expected = "x";
			final java.lang.String actual = dro.lang.String.resolve("${a${b2}${c3}}", p);
			Assert.assertEquals(expected, actual);
		}
	}
	@Test
	public void test_0004() {
		final dro.util.Properties p = new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				setProperty("Hello", "${dro.util.PropertiesTest.properties:There}");
			}
		};
		final java.lang.String expected = "World!";
		final java.lang.String actual = dro.lang.String.resolve("${Hello}", p);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void test_0005() throws FileNotFoundException, IOException {
		final java.lang.String expected = "World!";
		final java.lang.String actual = dro.lang.String.resolve("${dro.util.PropertiesTest.properties:There}");
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void test_0006() throws FileNotFoundException, IOException {
		final dro.util.Properties p = new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				setProperty("WhatIsMyPassword", "${password.txt:#}");
			}
		};
		final java.lang.String expected = "ThisIsMyPassword(-;";
		final java.lang.String actual = dro.lang.String.resolve("${WhatIsMyPassword}", p);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void test_0007() throws FileNotFoundException, IOException {
		final dro.util.Properties p = new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				setProperty("WhatIsMyPassword", "${${user.home}/fcl.crs/passwords/password.txt:#}");
			}
		};
		final java.lang.String expected = "ThisIsMyPassword(-;";
		final java.lang.String actual = dro.lang.String.resolve("${WhatIsMyPassword}", p);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void test_0008() throws FileNotFoundException, IOException {
		final java.lang.String expected = "ThisIsMyPassword(-;";
		final java.lang.String actual = dro.lang.String.resolve("${${user.home}/fcl.crs/passwords/password.properties:password}");
		Assert.assertEquals(expected, actual);
	}
}