package dto.activemq;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;

import dro.util.Properties;
import dro.util.State;
/*
Put Message
Get Message
*/
// This is dto.activemq.Adapter
public class Adapter extends dto.Adapter {
  //private static final java.lang.String version = "0.0.1-SNAPSHOT";
	
	/*
	 * Apparently, according to:
	 * 
	 * http://logging.apache.org/log4j/2.x/manual/jmx.html
	 * 
	 * System.setProperty("log4j2.disableJmx", Boolean.TRUE.toString());
	 * 
	 * should address (prevent) the following System.err (but it does not) -
	 * 
	 * log4j:WARN No appenders could be found for logger (org.apache.activemq.broker.jmx.ManagementContext).
	 * log4j:WARN Please initialize the log4j system properly.
	 * log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
	 * 
	 * But then again, having a JMX console could be useful going forward..
	 */
	static {
		System.setProperty("log4j2.disableJmx", Boolean.TRUE.toString());
	}
	
	private static javax.jms.ConnectionFactory createConnectionFactory(final java.lang.String url) {
		return new ActiveMQConnectionFactory(url);
	}
  /*private static javax.jms.ConnectionFactory createConnectionFactory(final InitialContext ic, final javax.naming.Name name) {
		final javax.jms.ConnectionFactory cf;// = null;
		try {
			cf = (javax.jms.ConnectionFactory)ic.lookup(name);
		} catch (final NamingException e) {
			throw new RuntimeException(e);
		}
		return cf;
	}*/
	private static javax.jms.ConnectionFactory createConnectionFactory(final InitialContext ic, final java.lang.String name) {
		final javax.jms.ConnectionFactory cf;// = null;
		try {
			cf = (javax.jms.ConnectionFactory)ic.lookup(name);
		} catch (final NamingException e) {
			throw new RuntimeException(e);
		}
		return cf;
	}
	private static javax.naming.InitialContext newInitialContext() {
		final javax.naming.InitialContext ic;// = null;
		try {
			ic = new InitialContext();
		} catch (final NamingException e) {
			throw new RuntimeException(e);
		}
		return ic;
	}
	// TODO: which properties, specifically? Too many to put in as one-by-one arguments?
	private static javax.naming.InitialContext newInitialContext(final dro.util.Properties properties) {
		final javax.naming.InitialContext ic;// = null;
		try {
			ic = new InitialContext(properties);
		} catch (final NamingException e) {
			throw new RuntimeException(e);
		}
		return ic;
	}
  /*private static javax.jms.Connection createConnection(final javax.jms.ConnectionFactory connectionFactory) {
		final javax.jms.Connection c;// = null;
		try {
			c = connectionFactory.createConnection();
		} catch (final JMSException e) {
			throw new RuntimeException(e);
		}
		return c;
	}*/
  /*private static javax.jms.JMSContext createContext(final javax.jms.ConnectionFactory cf, final java.lang.String userName, final java.lang.String password) {
		return cf.createContext(userName, password);
	}*/
  /*private static javax.jms.JMSContext createContext(final javax.jms.ConnectionFactory cf, final java.lang.String userName, final java.lang.String password, final int sessionMode) {
		return cf.createContext(userName, password, sessionMode);
	}*/
  /*private static javax.jms.JMSContext createContext(final javax.jms.ConnectionFactory cf, final int sessionMode) {
		return cf.createContext(sessionMode);
	}*/
  /*private static javax.jms.Session createSession(final javax.jms.Connection c, final boolean transacted, final int acknowledgeMode) {
		final javax.jms.Session s;// = null;
		try {
			s = c.createSession(true, javax.jms.Session.AUTO_ACKNOWLEDGE);
		} catch (final JMSException e) {
			throw new RuntimeException(e);
		}
		return s;
	}*/
	
	public static final class Server implements Runnable {
		
		private BrokerService service = null;
		private final State.Long state = new State.Long(0L); // TODO: Meaningfully-named literals
		
		public Server(final dto.data.Context.Callback callback) throws InterruptedException {
			final dro.util.Properties p = Properties.properties();
			final java.lang.String url = p.getProperty(dro.lang.Class.getName(Server.class)+"#url"); // --OR-- Server.class, depending, accordingly
			try {
				this.service = BrokerFactory.createBroker(new URI("broker:("+url+")"));
				this.state.set(2L);
			} catch (final URISyntaxException e) {
				throw new RuntimeException(e);
			} catch (final IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			} catch (final Exception e) {
				throw new RuntimeException(e);
			}
			try {
				this.startup();
			} catch (final URISyntaxException e) {
				throw new RuntimeException(e);
			} catch (final Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		public final Adapter.Server startup() throws URISyntaxException, Exception {
			final Adapter.Server server = this;
			Runtime.getRuntime().addShutdownHook(new Thread(){
				@Override
				public void run() {
					server.shutdown(true);
				}
			});
			new Thread(server).start();
			while (3L != this.state.get() && 4L > this.state.get()) {
				Thread.sleep(200L);
			}
			return this;
		}
		
		@Override
		public void run() throws RuntimeException {
			if (2L == this.state.get()) {
				final BrokerService service = this.service;
				new Thread() {
					@Override
					public void run() {
						try {
							service.start();
							state.set(4L); // Note: 3L is error (for now: 2018/Nov/10th)
						} catch (final Exception e) {
							state.set(5L);
							throw new RuntimeException(e);
						}
					}
				}.start();
				while (4L > this.state.get()) {
					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {
					  //e.printStackTrace(); // Silently ignore..
					}
				}
				// We're now running the ActiveMQ BrokerService
				
				// We're now able to do 'stuff' with the running ActiveMQ BrokerService
				final dro.util.Properties p = Properties.properties();
			  /*final */javax.naming.InitialContext ic = null;
				final java.lang.String INITIAL_CONTEXT_FACTORY = dro.lang.Class.getName(javax.naming.Context.class)+"#"+javax.naming.Context.INITIAL_CONTEXT_FACTORY;
				final java.lang.String PROVIDER_URL = dro.lang.Class.getName(javax.naming.Context.class)+"#"+javax.naming.Context.PROVIDER_URL;
			  /*final */javax.jms.ConnectionFactory cf = null;
			  /*final */boolean jndi = false;
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (false == p.getProperty(INITIAL_CONTEXT_FACTORY, false, dro.lang.Return.Boolean) && false == p.getProperty(PROVIDER_URL, false, dro.lang.Return.Boolean)) {
					jndi = false;
				} else if (true == p.getProperty(INITIAL_CONTEXT_FACTORY, false, dro.lang.Return.Boolean) && true == p.getProperty(PROVIDER_URL, false, dro.lang.Return.Boolean)) {
					jndi = true;
				} else {
					throw new IllegalStateException();
				}
			  /*if (false == Boolean.TRUE.booleanValue()) {
				} else */if (false == jndi) {
					ic = Adapter.newInitialContext();
					final java.lang.String url = p.getProperty(dro.lang.Class.getName(Server.class)+"#url");
					cf = Adapter.createConnectionFactory(url);
				} else if (true == jndi) {
					ic = Adapter.newInitialContext(p);
					final java.lang.String name = p.getProperty(dro.lang.Class.getName(Server.class)+"@javax.naming.Context.lookup#name");
				  //final javax.naming.Name name = 
					cf = Adapter.createConnectionFactory(ic, name);
			  /*} else {
					throw new IllegalStateException();*/
				}
				final boolean transacted = true;
			  /*final */javax.jms.Connection c = null;
				final java.lang.String userName = null, password = null;
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == dro.lang.String.isNullOrTrimmedBlank(userName) && true == dro.lang.String.isNullOrTrimmedBlank(password)) {
					try {
						c = cf.createConnection();
					} catch (final JMSException e) {
						throw new RuntimeException(e);
					}
				} else if (false == dro.lang.String.isNullOrTrimmedBlank(userName) && false == dro.lang.String.isNullOrTrimmedBlank(password)) {
					try {
						c = cf.createConnection(userName, password);
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
				} else {
					throw new IllegalStateException();
				}
				final javax.jms.Session s;// = null;
				try {
					s = c.createSession(transacted, javax.jms.Session.AUTO_ACKNOWLEDGE);
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
			  //final int sessionMode;
			  //final javax.jms.JMSContext ctx = Adapter.createContext(cf, userName, password, sessionMode);
				final java.lang.String text;// = null;
				{
				  /*final */javax.jms.Destination d = null;
				  /*if (false == Boolean.TRUE.booleanValue()) {
					} else */if (false == jndi) {
						try {
							d = (javax.jms.Destination)(javax.jms.Queue)s.createQueue("CLIENTS.TO.SERVER");
						} catch (final javax.jms.JMSException e) {
							throw new RuntimeException(e);
						}
					} else if (true == jndi) {
						try {
							d = (javax.jms.Destination)ic.lookup("CLIENTS.TO.SERVER");
						} catch (final javax.naming.NamingException e) {
							throw new RuntimeException(e);
						}
				  /*} else {
						throw new IllegalStateException();*/
					}
				  /*final */javax.jms.MessageConsumer mc = null;
					try {
						mc = s.createConsumer(d);
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					try {
						c.start();
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					final javax.jms.Message m;// = null;
					try {
						m = mc.receive();//receiveNoWait();
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					try {
						text = ((javax.jms.TextMessage)m).getText();
System.err.println("server-recv["+new Date().toString()+"]:"+text);
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					try {
						s.commit();
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
				}
				{
				  /*final */javax.jms.Destination d = null;
				  /*if (false == Boolean.TRUE.booleanValue()) {
					} else */if (false == jndi) {
						try {
							d = (javax.jms.Destination)(javax.jms.Queue)s.createQueue("SERVER.TO.CLIENTS");
						} catch (final javax.jms.JMSException e) {
							throw new RuntimeException(e);
						}
					} else if (true == jndi) {
						try {
							d = (javax.jms.Destination)ic.lookup("SERVER.TO.CLIENTS");
						} catch (final javax.naming.NamingException e) {
							throw new RuntimeException(e);
						}
				  /*} else {
						throw new IllegalStateException();*/
					}
				  /*final */javax.jms.MessageProducer mp = null;
					try {
						mp = s.createProducer(d);
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					final javax.jms.TextMessage m;// = null;
					try {
						m = s.createTextMessage(text+"'");
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					try {
						mp.send(m);
System.out.println("server-sent["+new Date().toString()+"]:"+m.getText());
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
					try {
						s.commit();
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
				}
				try {
					s.close();
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				try {
					c.close();
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				
			  /*while (false == this.shutdown()) {
					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {
					  //e.printStackTrace(System.err) // Silently ignore..
					}
				}*/
			}
		}
		
		private boolean shutdown = false;
		private boolean shutdown() {
			return this.shutdown;
		}
		public void shutdown(final boolean shutdown) {
			if (true == shutdown) {
				this.shutdown = true;
				try {
					this.service.stop();
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	public static final class Client {
		public Client() throws InterruptedException {
			final dro.util.Properties p = Properties.properties();
		  /*final */javax.naming.InitialContext ic = null;
			final java.lang.String INITIAL_CONTEXT_FACTORY = dro.lang.Class.getName(javax.naming.Context.class)+"#"+javax.naming.Context.INITIAL_CONTEXT_FACTORY;
			final java.lang.String PROVIDER_URL            = dro.lang.Class.getName(javax.naming.Context.class)+"#"+javax.naming.Context.PROVIDER_URL;
		  /*final */javax.jms.ConnectionFactory cf = null;
		  /*final */boolean jndi = false;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (false == p.getProperty(INITIAL_CONTEXT_FACTORY, false, dro.lang.Return.Boolean) && false == p.getProperty(PROVIDER_URL, false, dro.lang.Return.Boolean)) {
				jndi = false;
			} else if (true == p.getProperty(INITIAL_CONTEXT_FACTORY, false, dro.lang.Return.Boolean) && true == p.getProperty(PROVIDER_URL, false, dro.lang.Return.Boolean)) {
				jndi = true;
			} else {
				throw new IllegalStateException();
			}
		  /*if (false == Boolean.TRUE.booleanValue()) {
			} else */if (false == jndi) {
				ic = Adapter.newInitialContext();
				final java.lang.String url = p.getProperty(dro.lang.Class.getName(Client.class)+"#url");
				cf = Adapter.createConnectionFactory(url);
			} else if (true == jndi) {
				ic = Adapter.newInitialContext(p);
				final java.lang.String name = p.getProperty(dro.lang.Class.getName(Client.class)+"@javax.naming.Context.lookup#name");
			  //final javax.naming.Name name = 
				cf = Adapter.createConnectionFactory(ic, name);
		  /*} else {
				throw new IllegalStateException();*/
			}
			final boolean transacted = true;
		  /*final */javax.jms.Connection c = null;
			final java.lang.String userName = null, password = null;
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == dro.lang.String.isNullOrTrimmedBlank(userName) && true == dro.lang.String.isNullOrTrimmedBlank(password)) {
				try {
					c = cf.createConnection();
				} catch (final JMSException e) {
					throw new RuntimeException(e);
				}
			} else if (false == dro.lang.String.isNullOrTrimmedBlank(userName) && false == dro.lang.String.isNullOrTrimmedBlank(password)) {
				try {
					c = cf.createConnection(userName, password);
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
			} else {
				throw new IllegalStateException();
			}
			final javax.jms.Session s;// = null;
			try {
				s = c.createSession(transacted, javax.jms.Session.AUTO_ACKNOWLEDGE);
			} catch (final javax.jms.JMSException e) {
				throw new RuntimeException(e);
			}
		  //final int sessionMode;
		  //final javax.jms.JMSContext ctx = Adapter.createContext(cf, userName, password, sessionMode);
			{
			  /*final */javax.jms.Destination d = null;
			  /*if (false == Boolean.TRUE.booleanValue()) {
				} else */if (false == jndi) {
					try {
						d = (javax.jms.Destination)(javax.jms.Queue)s.createQueue("CLIENTS.TO.SERVER");
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
				} else if (true == jndi) {
					try {
						d = (javax.jms.Destination)ic.lookup("CLIENTS.TO.SERVER");
					} catch (final javax.naming.NamingException e) {
						throw new RuntimeException(e);
					}
			  /*} else {
					throw new IllegalStateException();*/
				}
			  /*final */javax.jms.MessageProducer mp = null;
				try {
					mp = s.createProducer(d);
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				try {
					c.start();
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				final javax.jms.TextMessage m;// = null;
				try {
					m = s.createTextMessage("{}");
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				try {
					mp.send(m);
System.out.println("client-sent["+new Date().toString()+"]:"+m.getText());
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				try {
					s.commit();
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
			}
			{
			  /*final */javax.jms.Destination d = null;
			  /*if (false == Boolean.TRUE.booleanValue()) {
				} else */if (false == jndi) {
					try {
						d = (javax.jms.Destination)(javax.jms.Queue)s.createQueue("SERVER.TO.CLIENTS");
					} catch (final javax.jms.JMSException e) {
						throw new RuntimeException(e);
					}
				} else if (true == jndi) {
					try {
						d = (javax.jms.Destination)ic.lookup("SERVER.TO.CLIENTS");
					} catch (final javax.naming.NamingException e) {
						throw new RuntimeException(e);
					}
			  /*} else {
					throw new IllegalStateException();*/
				}
			  /*final */javax.jms.MessageConsumer mc = null;
				try {
					mc = s.createConsumer(d);
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				final javax.jms.Message m;// = null;
				try {
					m = mc.receive();//receiveNoWait();
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				final java.lang.String text;// = null;
				try {
					text = ((javax.jms.TextMessage)m).getText();
System.err.println("client-recv["+new Date().toString()+"]:"+text);
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
				try {
					s.commit();
				} catch (final javax.jms.JMSException e) {
					throw new RuntimeException(e);
				}
			}
			try {
				s.close();
			} catch (final javax.jms.JMSException e) {
				throw new RuntimeException(e);
			}
			try {
				c.close();
			} catch (final javax.jms.JMSException e) {
				throw new RuntimeException(e);
			}
		}
		
		public Client send(final java.lang.String json) {
			return this;
		}
	}
	
  /*public Adapter() {
		final Server server = new Adapter.Server();
	  //final Map<String, Boolean> domains = new HashMap<>();
		if (State.RUNNING == server.state()) {
			final String _sleepInMilliseconds = p.getProperty(dro.lang.Class.getName(Adapter.class)+"#sleepInMilliseconds");
			long sleepInMilliseconds = null == _sleepInMilliseconds ? 10000L : new Long(_sleepInMilliseconds).longValue();
			while (State.RUNNING == server.state()) {
				try {
					Thread.sleep(sleepInMilliseconds);
				} catch (final InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		  //server.shutdown();
		}
	}*/
}