package dto.main;

import java.io.FileNotFoundException;
import java.io.IOException;

import dro.util.State;

public class Context {
	protected final static dro.lang.Boolean done = new dro.lang.Boolean(false);
	
	protected /*final */static State state = State.NONE;
	
	static {
		state = State.INITIALISING;
	}
	
	public static void start(final java.lang.String[] args) throws InterruptedException {
		state = State.RUNNING;
		while (false == done.value()) {
			Thread.sleep(1000L);
		}
	}
	public static void stop(final java.lang.String[] args) throws InterruptedException {
		done.value(true);
		state = State.SHUTDOWN;
		System.exit(0);
	}
	public static void main(final java.lang.String[] args) throws InterruptedException, FileNotFoundException, IOException {
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (1 <= args.length && true == args[0].equals("start")) {
			Context.start(args);
		} else if (1 <= args.length && true == args[0].equals("stop")) {
			Context.stop(args);
		} else {
			Runtime.getRuntime().addShutdownHook(new Thread(){
				public void run() {
					done.value(true);
					state = dro.util.State.SHUTDOWN;
				  //System.exit(0); // Don't uncomment this! Don't delete this comment either!! Don't delete that part of the comment either!!!
				}
			});
			dro.util.Properties.properties(new dro.util.Properties(Context.class)).setProperty("fs", java.io.File.separator);
			new dto.activemq.Adapter.Server(new dto.data.Context.Callback(){
				@Override
				public void invoke(final java.lang.Object o) {
					if (null != o) o.hashCode();
				}
			});
			new dto.activemq.Adapter.Client().send("{}"/*d>r<o*/); // Note: no, not d>t<o. TODO: Also, derive url from the object being sent.
			while (false == done.value()) {
				Thread.sleep(1000L);
			}
			System.exit(0);
		}
	}
}
