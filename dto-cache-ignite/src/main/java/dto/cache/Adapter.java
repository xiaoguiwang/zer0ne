package dto.cache;

// This is dto.cache.Adapter
public class Adapter extends dto.Adapter {
  //private static final java.lang.String version = "0.0.1-SNAPSHOT";
	
	private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Class<?> propertyClass, final java.lang.Object propertyObject) {
	  /*final */java.lang.Object propertyValue = p.getProperty(propertyKey);
		if (null == propertyValue && null == propertyObject) {
			System.clearProperty(propertyKey);
this.properties.remove(propertyKey);
		} else if (propertyClass == java.lang.String.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			System.setProperty(propertyKey, (java.lang.String)propertyValue);
this.properties.setProperty(propertyKey, (java.lang.String)propertyValue);
		} else if (propertyClass == java.lang.Integer.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			if (propertyValue.getClass() == java.lang.Integer.class) {
				System.setProperty(propertyKey, java.lang.Integer.toString((java.lang.Integer)propertyValue));
this.properties.setProperty(propertyKey, java.lang.Integer.toString((java.lang.Integer)propertyValue));
			} else if (propertyValue.getClass() == java.lang.String.class) {
				System.setProperty(propertyKey, (java.lang.String)propertyValue);
this.properties.setProperty(propertyKey, (java.lang.String)propertyValue);
			} else {
				throw new UnsupportedOperationException();
			}
		} else if (propertyClass == java.lang.Boolean.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			if (propertyValue.getClass() == java.lang.Boolean.class) {
				System.setProperty(propertyKey, java.lang.Boolean.toString((java.lang.Boolean)propertyValue));
this.properties.setProperty(propertyKey, java.lang.Boolean.toString((java.lang.Boolean)propertyValue));
			} else if (propertyValue.getClass() == java.lang.String.class) {
				System.setProperty(propertyKey, (java.lang.String)propertyValue);
this.properties.setProperty(propertyKey, (java.lang.String)propertyValue);
			} else {
				throw new UnsupportedOperationException();
			}
		} else {
			throw new IllegalArgumentException();
		}
		return this;
	}
	private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Class<?> propertyClass) {
		return this.property(p, propertyKey, propertyClass, null);
	}
	private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Object propertyObject) {
		return this.property(p, propertyKey, propertyObject.getClass(), propertyObject);
	}
	
	protected java.util.Properties properties = new java.util.Properties();
	
	@Override
	public Adapter properties(final dro.util.Properties p) {
		return this;
	}
	
}