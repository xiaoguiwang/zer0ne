package dto.cache;

import org.apache.ignite.lang.IgniteBiPredicate;

public class BiPredicateForEvent implements IgniteBiPredicate<String, dro.cache.Event> {
	private static final long serialVersionUID = 0L;
	
	private final String key;// = null;
	
	public BiPredicateForEvent(final String key) {
		this.key = key;
	}
	
	@Override
	public boolean apply(final String key, final dro.cache.Event eve) {
		return (null == key && key == this.key) || (null != key && true == key.equals(this.key));
	}
}