package dto.cache;

import java.util.Map;

import org.apache.ignite.lang.IgniteBiPredicate;

public class BiPredicateForStringVsMap implements IgniteBiPredicate<String, Map<String, Object>> {
	private static final long serialVersionUID = 0L;
	
	@Override
	public boolean apply(final String key, final Map<String, Object> map) {
		return true;
	}
}