package dto.cache;

import java.util.UUID;

import org.apache.ignite.events.CacheEvent;
import org.apache.ignite.lang.IgniteBiPredicate;

public class BiPredicateForUuidVsEvent implements IgniteBiPredicate<UUID, CacheEvent> {
	private static final long serialVersionUID = 0L;
	
	@Override
	public boolean apply(final UUID uuid, final CacheEvent e) {
		return true; // Continue listening
	}
}