package dto.cache;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.ignite.client.ClientCache;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import dro.cache.ActivityType;
import dro.cache.BusinessType;
import dro.cache.ContextType;
import dro.cache.ContextsType;
import dro.cache.Event;
import dro.cache.ObjectFactory;

public class Helper {
	private static final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	static {
		dbf.setNamespaceAware(true);
	  //dbf.setIgnoringComments(true);
	  //dbf.setIgnoringElementContentWhitespace(true);
	}
	public static java.lang.String readResource(final Class<?> clazz, final String resource) {
		final InputStream is = clazz.getResourceAsStream(resource);
	  /*final */OutputStream os = null;
		if (null != is) {
			final byte[] ba = new byte[1024*1024]; // 1MB
		  /*final OutputStream */os = new ByteArrayOutputStream();
			try {
				for (int r = 0; r >= 0; ) {
					r = is.read(ba, 0, ba.length);
					if (r >= 0) {
						os.write(ba, 0, r);
					}
				}
				is.close();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
		return null == os ? null : new java.lang.String(((ByteArrayOutputStream)os).toByteArray());
	}
	private static final JAXBContext jc;// = null;
	private static final Unmarshaller ju;// = null;
	static {
		try {
			jc = JAXBContext.newInstance(dro.cache.Event.class);
			ju = jc.createUnmarshaller();
		} catch (final JAXBException e) {
			throw new RuntimeException(e);
		}
	}
	private static final ClientCache<String, dro.cache.Event> eve;// = null;
	private static final ClientCache<String, java.util.Map<String, Object>> evex;// = null;
	static {
		eve = Ignite.getClient().cache("eve");
		evex = Ignite.getClient().cache("evex");
	}
	
	public static void cacheEvent(final Class<?> clazz, final String uuid_as_char, final dro.util.Properties p) {
	  /*final */dro.cache.Event eve = null;
		try {
		  /*final dto.cache.Event */eve = ju.unmarshal(
				dbf.newDocumentBuilder().parse(new InputSource(new StringReader(
					dro.lang.String.resolve(
						Helper.readResource(clazz, clazz.getSimpleName().split("\\.")[0]+".xml"), p
					)
				))),
				dro.cache.Event.class
			).getValue();
		} catch (final JAXBException exc) {
			throw new RuntimeException(exc);
		} catch (final SAXException exc) {
			throw new RuntimeException(exc);
		} catch (final IOException exc) {
			throw new RuntimeException(exc);
		} catch (final ParserConfigurationException exc) {
			throw new RuntimeException(exc);
		}
		Helper.eve.put(uuid_as_char, eve);
	}
	public static void cacheEventX(final Class<?> clazz, final String uuid_as_char, final java.util.Map<String, Object> map) {
	  /*final */java.util.Map<String, Object> __map = null;
		if (null != map && 0 < map.size()) {
			for (final String key: map.keySet()) {
				final Object value = map.get(key);
				if (null != value) {
					if (null == __map) {
					  /*final java.util.Map<String, Object> */__map = new java.util.HashMap<>();
					}
				}
				__map.put(key, value);
			}
			if (null != __map && 0 < __map.size()) {
				Helper.evex.put(uuid_as_char, __map);
			}
		}
	}
	
	private final static ObjectFactory oFactory = new ObjectFactory(); 
	public static ObjectFactory getObjectFactory() {
		return oFactory;
	}
	public static void addContextValueForName(final Event eve, final String name, final String value) { 
	  /*final */ContextsType ctxts = eve.getContexts();
		if (null == ctxts) {
			ctxts = oFactory.createContextsType();
			eve.setContexts(ctxts);
		}
		final List<ContextType> ctxs = ctxts.getContext();
		{
			final ContextType ct = oFactory.createContextType();
		  //ct.setLevel(new BigInteger("0"));
		  //ct.setParentLevel(new BigInteger("0"));
			ct.setName(name);
			ct.setValue(value);
			ctxs.add(ct); 
		} 
	}
	public static String getContextValueForName(final Event eve, final String name) { 
	  /*final */String value = null;
		final List<BusinessType> bts = eve.getBusiness();
		for (final BusinessType bt: bts) {
			final List<ActivityType> activityTypes = bt.getActivity();
			for (final ActivityType at: activityTypes) {
				if (true == name.equals(at.getName())) {
					value = at.getValue();
					break;
				}
			}
		}
		return value;
	}
	public static void addBusinessActivityValueForName(final Event eve, final String name, final String value) { 
		final List<BusinessType> bts = eve.getBusiness();
	  /*final */BusinessType bt = null;
		if (null == bt) {
			bt = oFactory.createBusinessType();
			bts.add(bt);
		}
		final List<ActivityType> ats = bt.getActivity();
		{
			final ActivityType at = oFactory.createActivityType();
		  //at.setIndex(new BigInteger("0"));
			at.setName(name);
			at.setValue(value);
			ats.add(at);
		} 
	}
	public static String getBusinessActivityValueForName(final Event eve, final String name) { 
	  /*final */String value = null;
		final List<BusinessType> bts = eve.getBusiness();
		for (final BusinessType bt: bts) {
			final List<ActivityType> activityTypes = bt.getActivity();
			for (final ActivityType at: activityTypes) {
				if (true == name.equals(at.getName())) {
					value = at.getValue();
					break;
				}
			}
		}
		return value;
	}
}