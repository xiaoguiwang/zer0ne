package dto.cache;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.cache.Cache;

import org.apache.ignite.IgniteEvents;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.client.ClientCache;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.ClientConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.events.EventType;

import dro.util.Logger;

public class Ignite {
	private static final Ignite instance;// = null;
	public static final CacheConfiguration<String, dro.cache.Event> eve;
	public static final CacheConfiguration<String, Map<String, Object>> evex;
	private static final Thread thread;
	
	static {
		instance = new Ignite();
		eve = new CacheConfiguration<String, dro.cache.Event>("eve");
		evex = new CacheConfiguration<String, Map<String, Object>>("evex");
		thread = new Thread();
	}
	
	public static void main(final String[] args) {
		Ignite.startServer(false);
		Ignite.startExpiring();
		final dro.lang.Boolean __running = new dro.lang.Boolean(true);
		Runtime.getRuntime().addShutdownHook(new java.lang.Thread(){
			public void run() {
				__running.value(false);
			}
		}); 
		while (true == __running.value()) {
			try {
				java.lang.Thread.sleep(1000L);
			} catch (final InterruptedException e) {
				// Silently ignore.
			}
		}
		Ignite.stopExpiring();
		Ignite.stopServer();
	}
	
	private static /*final */org.apache.ignite.Ignite server;// = null;
	private static /*final */org.apache.ignite.Ignite thick;// = null;
	public static void startServer(final boolean clientMode) {
		synchronized(instance) {
			if (null == Ignite.server && null == Ignite.thick) {
			  /*final TcpDiscoverySpi spi = new TcpDiscoverySpi();
				final TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
				tcMp.setAddresses(Arrays.asList("FCL5CG9457X1Y"));
				spi.setIpFinder(tcMp);*/
				final IgniteConfiguration config = new IgniteConfiguration();
			  /*config.setDiscoverySpi(spi);*/
				config.setClientMode(clientMode);
				if (false == clientMode) {
					if (null == Ignite.server) {
						config.setIgniteInstanceName("dto-cache-ignite");
						// https://www.javadoc.io/static/org.apache.ignite/ignite-core/2.7.6/org/apache/ignite/configuration/IgniteConfiguration.html#setIncludeEventTypes-int...-
						config.setIncludeEventTypes(EventType.EVT_CACHE_OBJECT_PUT, EventType.EVT_CACHE_OBJECT_REMOVED);
						Ignite.server = Ignition.start(config);
						Ignite.server.getOrCreateCache(eve);
						Ignite.server.getOrCreateCache(evex);
					}
				} else {
					if (null == Ignite.thick) {
						Ignite.thick = Ignition.start(config);
						Ignite.thick.getOrCreateCache(eve);
						Ignite.thick.getOrCreateCache(evex);
						// https://ignite.apache.org/docs/latest/events/listening-to-events
						final IgniteEvents events = Ignite.thick.events();
					  /*final java.util.UUID uuid = */events.remoteListen(new BiPredicateForUuidVsEvent(), new PredicateForEvent(), EventType.EVT_CACHE_OBJECT_PUT, EventType.EVT_CACHE_OBJECT_REMOVED);
					  //events.stopRemoteListen(uuid);
					}
				}
			}
		}
	}
	public static org.apache.ignite.Ignite getIgnite() {
		return null != Ignite.server ? Ignite.server : null != Ignite.thick ? Ignite.thick : null;
	}
	public static void stopServer() {
		synchronized(instance) {
			if (null != Ignite.server) {
				Ignite.server.close();
				Ignite.server = null;
			}
			if (null != Ignite.thick) {
				Ignite.thick.close();
				Ignite.thick = null;
			}
		}
	}
	
	public static void startExpiring() {
		if (null != Ignite.thread) { 
			Ignite.thread.start();
		}
	}
	public static void stopExpiring() {
		if (null != Ignite.thread) { 
			Ignite.thread.stop();
		}
	}
	
	private /*final */org.apache.ignite.client.IgniteClient thin;// = null;
	public static org.apache.ignite.client.IgniteClient getClient() {
		synchronized(instance) {
			if (null == instance.thin) {
				final ClientConfiguration config = new ClientConfiguration().setAddresses("localhost:10800");
				instance.thin = Ignition.startClient(config);
			}
		}
		return instance.thin;
	}
	public static void endClient() {
		synchronized(instance) {
			if (null != instance.thin) {
				try {
					instance.thin.close();
				} catch (final Exception e) {
					Logger.getLogger(Logger.class).log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
	}
	
	public static final class Thread implements Runnable {
		private java.lang.Thread t;// = null;
		public boolean running;// = false;
		public Thread() {
			this.t = null;
			this.running = false;
		}
		public Thread start() {
			this.t = new java.lang.Thread(this);
			this.running = true;
			this.t.start();
			return this;
		}
		@Override
		public void run() {
			final ScanQuery<String, Map<String, Object>> query = new ScanQuery<>(new BiPredicateForStringVsMap());
		  /*final */ClientCache<String, Map<String, Object>> evex = null;
		  /*final */ClientCache<String, dro.cache.Event> eve = null;
			while (true == this.running) {
				try {
					java.lang.Thread.sleep(5000L);
					if (null == evex) {
					  /*final ClientCache<String, Map<String, Object>> */evex = Ignite.getClient().cache("evex").withKeepBinary();
					}
					final List<Cache.Entry<String, Map<String, Object>>> entries = evex.query(query).getAll();
				  /*final */java.util.List<String> keys = null;
					for (final Cache.Entry<String, Map<String, Object>> entry: entries) {
						final String key = entry.getKey();
						final Map<String, Object> map = entry.getValue();
						if (null == map) {
							try {
								throw new IllegalStateException();
							} catch (final IllegalStateException e) {
								e.printStackTrace(System.err);
							}
							continue;
						}
						final Timestamp ts = new Timestamp(System.currentTimeMillis()-1000L*60/*seconds*/*1/*minutes*/);
						final Timestamp created = new Timestamp(((Long)map.get("creation-time")).longValue());
						if (true == created.before(ts)) {
							if (null == keys) {
							  /*final java.util.List<String> */keys = new ArrayList<String>();
							}
							keys.add(key);
						}
					}
					if (null != keys && 0 < keys.size()) {
						if (null == eve) {
						  /*final ClientCache<String, dto.cache.Event> */eve = Ignite.getClient().cache("eve").withKeepBinary();
						}
						for (final String key: keys) {
							Logger.getLogger(Logger.class).info("purging eve/x:entry ["+key+"]..");
							final dro.cache.Event __eve = eve.get(key);
							__eve.hashCode();
							eve.remove(key);
							evex.remove(key);
						}
					  //while (0 < keys.size()) {
					  //	keys.remove(keys.size()-1);
					  //}
						keys.clear();
					  //keys = null;
					}
				} catch (final InterruptedException e) {
					// Silently ignore.
				}
			}
		}
		public void stop() {
			this.running = false;
			this.t = null;
		}
	}
	
	private Ignite() {
	}
}
