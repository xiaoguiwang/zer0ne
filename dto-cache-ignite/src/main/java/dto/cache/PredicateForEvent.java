package dto.cache;

import org.apache.ignite.events.CacheEvent;
import org.apache.ignite.lang.IgnitePredicate;

public class PredicateForEvent implements IgnitePredicate<CacheEvent> {
	private static final long serialVersionUID = 0L;
	
	@Override
	public boolean apply(final CacheEvent e) {
		return true;
	}
}