package dto.cache;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.cache.Cache;
import javax.xml.bind.JAXBElement;

import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteEvents;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.client.ClientCache;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.ClientConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.events.EventType;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.junit.Test;

import dro.cache.ContextType;
import dro.cache.ContextsType;
import dro.cache.Event;
import dro.cache.ObjectFactory;

public class HelperTest {
	@Test // Helper to start a local/embedded Ignite server within JUnit context
	public void test_0001A() throws InterruptedException {
	  /*final */org.apache.ignite.Ignite server = null;
		{
			final IgniteConfiguration config = new IgniteConfiguration();
			// https://www.javadoc.io/static/org.apache.ignite/ignite-core/2.7.6/org/apache/ignite/configuration/IgniteConfiguration.html#setIncludeEventTypes-int...-
			config.setIncludeEventTypes(EventType.EVT_CACHE_OBJECT_PUT, EventType.EVT_CACHE_OBJECT_REMOVED);
		  /*final org.apache.ignite.Ignite */server = Ignition.start(config);
			final CacheConfiguration<String, Event> cc = new CacheConfiguration<String, Event>("eve");
			server.getOrCreateCache(cc);
		}
		while (false == java.lang.Boolean.FALSE.booleanValue()) {
			Thread.sleep(60*1000L);
		}
		if (null != server) {
			server.close();
		}
	}
	@Test
	public void test_0001B() throws Exception {
		final String hostname = Inet4Address.getLocalHost().getHostName();
		final org.apache.ignite.Ignite thick;// = null;
	  /*final */IgniteEvents events = null;
	  /*final */java.util.UUID uuid = null;
		{
			final TcpDiscoverySpi spi = new TcpDiscoverySpi();
			final TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
			tcMp.setAddresses(Arrays.asList(hostname));
			spi.setIpFinder(tcMp);
			final IgniteConfiguration config = new IgniteConfiguration();
			config.setDiscoverySpi(spi);
			config.setClientMode(true); // Listen to Event cache events via thick-client (you can do it within server too)
		  /*final org.apache.ignite.Ignite */thick = Ignition.start(config);
			// https://ignite.apache.org/docs/latest/events/listening-to-events
		  /*final IgniteEvents */events = thick.events(); // Add breakpoints inside the two predicates below for demonstration/illustration of event listening
		  /*final java.util.UUID */uuid = events.remoteListen(new BiPredicateForUuidVsCacheEvent(), new PredicateForEvent(), EventType.EVT_CACHE_OBJECT_PUT, EventType.EVT_CACHE_OBJECT_REMOVED);
		}
		
	  /*final */org.apache.ignite.client.IgniteClient thin = null;
		{
			final ClientConfiguration config = new ClientConfiguration().setAddresses(hostname+":10800");
		  /*final org.apache.ignite.client.IgniteClient */thin = Ignition.startClient(config);
			final ClientCache<String, Event> cc = thin.cache("eve");//.withKeepBinary();
			final ObjectFactory oFactory = Helper.getObjectFactory();
			final JAXBElement<Event> root = oFactory.createEvent(new Event());
			final Event eve = root.getValue();
			final ContextsType ctxt = oFactory.createContextsType();
			final List<ContextType> ctxs = ctxt.getContext();
			{
			final ContextType ctx = oFactory.createContextType();
			  //ctx.setLevel(new BigInteger("0")); 
			  //ctx.setParentLevel(..);
				ctx.setName("host-name");
				ctx.setValue("localhost");
				ctxs.add(ctx);
			}
			eve.setContexts(ctxt);
			cc.put("Event", eve); // Put a Event POJO into the cache (via thin-client)
		}
		if (null != thin) {
			final ScanQuery<String, Event> query = new ScanQuery<>(new BiPredicateForStringVsEvent());
			final ClientCache<String, Event> cc = thin.cache("eve");//.withKeepBinary();
			final List<Cache.Entry<String, Event>> entries = cc.query(query).getAll();
			final java.util.List<String> keys = new ArrayList<String>();
			for (final Cache.Entry<String, Event> entry: entries) {
				final String key = entry.getKey();
				final Event eve = entry.getValue();
				if (null == eve) {
					try {
						throw new IllegalStateException();
					} catch (final IllegalStateException e) {
						e.printStackTrace(System.err);
					}
					continue;
				}
				keys.add(key);
			}
			if (0 < keys.size()) {
				for (final String key: keys) {
					final Event eve = cc.get(key); // Illustrate cache get (deserialisation of POJO) via thin-client
					System.out.println("Event "+Helper.getContextValueForName(eve, "name"));
					cc.remove(key);
				}
			  //while (0 < keys.size()) {
			  //	keys.remove(keys.size()-1);
			  //}
				keys.clear();
			  //keys = null;
			}
		}
		
		if (null != events && null != uuid) {
			events.stopRemoteListen(uuid);
		}
		if (null != thick) {
			thick.close();
		}
		if (null != thin) {
			thin.close();
		}
	}
	
	@Test // Helper to start a local/embedded Ignite server within JUnit context
	public void test_0002A() throws InterruptedException {
		final org.apache.ignite.Ignite server;// = null;
		{
			final IgniteConfiguration config = new IgniteConfiguration();
		  /*final org.apache.ignite.Ignite */server = Ignition.start(config);
			final CacheConfiguration<String, Event> cc = new CacheConfiguration<String, Event>("eve");
			server.getOrCreateCache(cc);
		}
		while (false == java.lang.Boolean.FALSE.booleanValue()) {
			Thread.sleep(60*1000L);
		}
		if (null != server) {
			server.close();
		}
	}
	@Test
	public void test_0002B() throws UnknownHostException {
		final String hostname = Inet4Address.getLocalHost().getHostName();
		final org.apache.ignite.Ignite thick;// = null;
		{
			final TcpDiscoverySpi spi = new TcpDiscoverySpi();
			final TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
			tcMp.setAddresses(Arrays.asList(hostname));
			spi.setIpFinder(tcMp);
			final IgniteConfiguration config = new IgniteConfiguration();
			config.setDiscoverySpi(spi);
			config.setClientMode(true);
		  /*final org.apache.ignite.Ignite */thick = Ignition.start(config);
			final IgniteCache<String, Event> cc = thick.getOrCreateCache("eve");//.withKeepBinary();
			final dro.cache.ObjectFactory objectFactory = new dro.cache.ObjectFactory();
			final Event eve = objectFactory.createEvent();
			final dro.cache.ContextsType contextsType = objectFactory.createContextsType();
			final List<dro.cache.ContextType> listOfContextType = contextsType.getContext();
			final dro.cache.ContextType contextType = objectFactory.createContextType();
		  //contextType.setLevel(new BigInteger("0"));
		  //contextType.setParentLevel(..);
			contextType.setName("this-is-my-name");
			contextType.setValue("this-is-my-value");
			listOfContextType.add(contextType);
			eve.setContexts(contextsType);
			cc.put("eve", eve); // Put a Event POJO into the cache (via thick-client)
		}
		if (null != thick) {
			final ScanQuery<String, Event> query = new ScanQuery<>(new BiPredicateForStringVsEvent());
			final IgniteCache<String, Event> cc = thick.getOrCreateCache("eve");//.withKeepBinary();
			final List<Cache.Entry<String, Event>> entries = cc.query(query).getAll();
			final java.util.List<String> keys = new ArrayList<String>();
			for (final Cache.Entry<String, Event> entry: entries) {
				final String key = entry.getKey();
				final Event eve = entry.getValue();
				if (null == eve) {
					try {
						throw new IllegalStateException();
					} catch (final IllegalStateException e) {
						e.printStackTrace(System.err);
					}
					continue;
				}
				keys.add(key);
			}
			if (0 < keys.size()) {
				for (final String key: keys) {
					final Event eve = cc.get(key); // Illustrate cache get (deserialisation of POJO) via thick-client
					eve.hashCode();
					cc.remove(key);
				}
			  //while (0 < keys.size()) {
			  //	keys.remove(keys.size()-1);
			  //}
				keys.clear();
			  //keys = null;
			}
		}
		if (null != thick) {
			thick.close();
		}
	}
}