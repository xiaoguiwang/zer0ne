package dto.cache;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class PredicateTest {
	@Test
	public void test_0001() {
		final Map<String, Object> map1 = new HashMap<>();
		final Map<String, Object> map2 = new HashMap<>();
		map1.put("map2", map2);
		map2.put("Hello", "World!");
		map2.put("ts", new Timestamp(System.currentTimeMillis()));
		Ignite.getClient().cache("map").put("map1", map1);
		map1.hashCode();
	}
}