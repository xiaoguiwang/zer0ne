package dto.http;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.logging.Level;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

// This is dto.http.Adapter
public class Adapter extends dto.Adapter {
	private static final String className = Adapter.class.getName();
	
	private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	public static class Return {
		public static final dto.http.Adapter.Return Adapter = (dto.http.Adapter.Return)null;
	}
	
  //private static final java.lang.String version = "0.0.1-SNAPSHOT";
	
  /*private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Class<?> propertyClass, final java.lang.Object propertyObject) {
	*//*final *//*java.lang.Object propertyValue = p.getProperty(propertyKey);
		if (null == propertyValue && null == propertyObject) {
			System.clearProperty(propertyKey);
		} else if (propertyClass == java.lang.String.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			System.setProperty(propertyKey, (java.lang.String)propertyValue);
		} else if (propertyClass == java.lang.Integer.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			if (propertyValue.getClass() == java.lang.Integer.class) {
				System.setProperty(propertyKey, java.lang.Integer.toString((java.lang.Integer)propertyValue));
			} else if (propertyValue.getClass() == java.lang.String.class) {
				System.setProperty(propertyKey, (java.lang.String)propertyValue);
			} else {
				throw new UnsupportedOperationException();
			}
		} else if (propertyClass == java.lang.Boolean.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			if (propertyValue.getClass() == java.lang.Boolean.class) {
				System.setProperty(propertyKey, java.lang.Boolean.toString((java.lang.Boolean)propertyValue));
			} else if (propertyValue.getClass() == java.lang.String.class) {
				System.setProperty(propertyKey, (java.lang.String)propertyValue);
			} else {
				throw new UnsupportedOperationException();
			}
		} else {
			throw new IllegalArgumentException();
		}
		return this;
	}*/
  /*private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Class<?> propertyClass) {
		return this.property(p, propertyKey, propertyClass, null);
	}*/
  /*private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Object propertyObject) {
		return this.property(p, propertyKey, propertyObject.getClass(), propertyObject);
	}*/
	
	final CloseableHttpClient httpClient;// = null;
  /*final */HttpClientContext ctx = null;
	
	public Adapter() {
	  //super(); // Implicit super constructor..
		// https://www.baeldung.com/java-7-tls-v12
	  /*final */SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if (null != sslContext) {
			try {
				sslContext.init(null, new TrustManager[]{new X509TrustManager(){
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					@Override
					public void checkClientTrusted(final X509Certificate[] certs, final String authType) {
						return;
					}
					@Override
					public void checkServerTrusted(final X509Certificate[] certs, final String authType) {
						return;
					}
				}}, new SecureRandom());
			} catch (final KeyManagementException e) {
				e.printStackTrace();
			}
		}
		
		@SuppressWarnings("deprecation")
		final SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		
		final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
			.register("https", sf)
			.register("http", new PlainConnectionSocketFactory())
			.build()
		;
		
	  /*final CloseableHttpClient */this.httpClient = HttpClients.custom()
			.setConnectionManager(
				new BasicHttpClientConnectionManager(registry)
			)
			.setSSLSocketFactory(sf)
		  //.setHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
			.build()
		;
	}
	@Override
	public Adapter properties(final dro.util.Properties p) {
		super.properties(p);
		
	  /*final HttpClientContext */this.ctx = HttpClientContext.create();
		
	  /*final */String username = null;
		username = p.getProperty(this.getClass().getName()+"#username");
	  /*final */String passwords = null;
		passwords = p.getProperty(this.getClass().getName()+"#passwords");
	  /*final */String password = null;
		if (null != passwords && 0 < passwords.trim().length()) {
		  //final FileAdapter fileAdapter = new FileAdapter(passwords+java.io.File.separator+scheme+java.io.File.separator+hostname+java.io.File.separator+username);
			
			try {
				password = "";//fileAdapter.returns(FileAdapter.Return.Reader).read();
		  /*} catch (final IOException e) {
				throw new RuntimeException(e);
			} catch (final RuntimeException e) {
				throw new RuntimeException(e);*/
			} finally {
			}
		}
		
	  /*final */CredentialsProvider cp = null;
		if (null != username && null != password && 0 < password.length()) {
			final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
			cp = new BasicCredentialsProvider();
			cp.setCredentials(AuthScope.ANY, credentials);
		}
		if (null != cp) {
			final String scheme = super.properties().getProperty(HttpHost.class.getName()+"#scheme");
		  /*final */URL __url = null;
			try {
				__url = new URL(super.properties().getProperty(Adapter.class.getName()+"#url"));
			} catch (final MalformedURLException e) {
				e.printStackTrace();
			}
		  /*final */String hostname = null;
		  /*final */Integer port = null;
			if (null != __url) {
			  /*final String */hostname = __url.getHost();
			  /*final Integer */port = 0 <= __url.getPort() ? __url.getPort() : null != scheme && 0 <= scheme.trim().length() && scheme.trim().equals("https") ? 443 : 80;
			}
			final HttpHost httpHost = new HttpHost(hostname, port, scheme);
			final AuthCache ac = new BasicAuthCache();
			ac.put(httpHost, new BasicScheme());
			this.ctx.setAuthCache(ac);
			this.ctx.setCredentialsProvider(cp);
		}
		
		return this;
	}
	
	private java.util.Map<String, String> headers = null;
	public Adapter header(final String name, final String value) {
		if (null == this.headers) {
		  /*final java.util.Map<String, String> */this.headers = new java.util.LinkedHashMap<>();
		}
		this.headers.put(name, value);
		return this;
	}
	
	
	private int statusCode;
	public int statusCode() {
		return this.statusCode;
	}
	public Adapter status(final dro.lang.Integer statusCode) {
		statusCode.value(this.statusCode);
		return this;
	}
	
	private java.lang.String data = null;
	public java.lang.String data() {
		return this.data;
	}
	public Adapter data(final dro.lang.String data) {
		data.value(this.data);
		return this;
	}
	
	public String post(final String url, final String data, final dro.lang.String.Return r) throws ClientProtocolException, IOException {
		this.post(url, data); /* this.data */
		return this.data;
	}
	public String post(final String url, final dro.lang.String.Return r) throws ClientProtocolException, IOException {
		return this.post(url, (String)null, r); /* this.data */
	}
	public Adapter post(final String url, final String data, final dto.http.Adapter.Return r) throws ClientProtocolException, IOException {
		this.post(url, data); /* this.data */
		return this;
	}
	public Adapter post(final String url, final dto.http.Adapter.Return r) throws ClientProtocolException, IOException {
		return this.post(url, (String)null, r); /* this.data */
	}
	public void post(final String url, final String data) throws ClientProtocolException, IOException {
		final HttpPost request = new HttpPost(url);
	  //request.setHeader("Accept", "application/json");
	  //request.setHeader("Content-type", "application/json;charset=UTF-8");
		for (final java.util.Map.Entry<java.lang.String, java.lang.String> entry: this.headers.entrySet()) {
		  //final java.lang.String name = entry.getKey();
		  //if (false == "Content-type".equals(name)) {
				request.setHeader(entry.getKey(), entry.getValue());
		  //}
		}
		if (null != data) {
			final HttpEntity httpEntity = new StringEntity(data);
			request.setEntity(httpEntity);
		}
		final CloseableHttpResponse response = this.httpClient.execute(request, this.ctx);
		try {
			final StatusLine sl = response.getStatusLine();
			this.statusCode = sl.getStatusCode();
			if (200 == this.statusCode) {
				logger.log(Level.INFO, sl.getStatusCode()+": "+sl.getReasonPhrase());
				final HttpEntity entity = response.getEntity();
				this.data = EntityUtils.toString(entity, "UTF-8");
			} else {
				logger.log(Level.SEVERE, sl.getStatusCode()+": "+sl.getReasonPhrase());
			}
		} finally {
			response.close();
		}
	}
	
	public String get(final String url, final dro.lang.String.Return r) throws ClientProtocolException, IOException {
		this.get(url); /* this.data */
		return this.data;
	}
	public Adapter get(final String url, final dto.http.Adapter.Return r) throws ClientProtocolException, IOException {
		this.get(url); /* this.data */
		return this;
	}
	public void get(final String url) throws ClientProtocolException, IOException {
		final HttpGet request = new HttpGet(url);
	  //request.setHeader("Content-type", "application/json;charset=UTF-8");
		for (final java.util.Map.Entry<java.lang.String, java.lang.String> entry: this.headers.entrySet()) {
		  //final java.lang.String name = entry.getKey();
		  //if (false == "Content-type".equals(name)) {
				request.setHeader(entry.getKey(), entry.getValue());
		  //}
		}
		final CloseableHttpResponse response = this.httpClient.execute(request, this.ctx);
		try {
			final StatusLine sl = response.getStatusLine();
			this.statusCode = sl.getStatusCode();
			if (200 == this.statusCode) {
				final HttpEntity entity = response.getEntity();
				this.data = EntityUtils.toString(entity, "UTF-8");
				logger.log(Level.INFO, sl.getStatusCode()+": "+sl.getReasonPhrase());
			} else {
				logger.log(Level.SEVERE, sl.getStatusCode()+": "+sl.getReasonPhrase());
			}
		} finally {
			response.close();
		}
	}
	
	public String delete(final String url, final dro.lang.String.Return r) throws ClientProtocolException, IOException {
		this.delete(url); /* this.data */
		return this.data;
	}
	public Adapter delete(final String url, final dto.http.Adapter.Return r) throws ClientProtocolException, IOException {
		this.delete(url); /* this.data */
		return this;
	}
	public void delete(final String url) throws ClientProtocolException, IOException {
		final HttpDelete request = new HttpDelete(url);
	  //request.setHeader("Content-type", "application/json;charset=UTF-8");
		for (final java.util.Map.Entry<java.lang.String, java.lang.String> entry: this.headers.entrySet()) {
		  //final java.lang.String name = entry.getKey();
		  //if (false == "Content-type".equals(name)) {
				request.setHeader(entry.getKey(), entry.getValue());
		  //}
		}
		final CloseableHttpResponse response = this.httpClient.execute(request, this.ctx);
		try {
			final StatusLine sl = response.getStatusLine();
			this.statusCode = sl.getStatusCode();
			if (200 == this.statusCode) {
				final HttpEntity entity = response.getEntity();
				this.data = EntityUtils.toString(entity, "UTF-8");
				logger.log(Level.INFO, sl.getStatusCode()+": "+sl.getReasonPhrase());
			} else {
				logger.log(Level.SEVERE, sl.getStatusCode()+": "+sl.getReasonPhrase());
			}
		} finally {
			response.close();
		}
	}
}