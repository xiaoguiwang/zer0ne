package dto.http;

import java.util.ArrayList;
import java.util.List;

public class Post {
	public static class Attachment {
		private java.lang.Object object = null;
		private java.lang.String mimeType = "text/plain";
		private java.lang.String name = null;
		
		public Attachment(final java.lang.Object object) {
			this(object, null, null);
		}
		public Attachment(final java.lang.Object object, final java.lang.String mimeType) {
			this(object, mimeType, null);
		}
		public Attachment(final java.lang.Object object, final java.lang.String mimeType, final java.lang.String name) {
			this.mimeType(mimeType);
			this.object(object);
			this.name(name);
		}
		
		public Attachment object(final java.lang.Object object) {
			this.object = object;
			return this;
		}
		public java.lang.Object object() {
			return this.object;
		}
		
		public Attachment mimeType(final java.lang.String mimeType) {
			this.mimeType = mimeType;
			return this;
		}
		public java.lang.String mimeType() {
			return this.mimeType;
		}
		
		public Attachment name(final java.lang.String name) {
			this.name = name;
			return this;
		}
		public java.lang.String name() {
			return this.name;
		}
	}
	
	private String from = null;
	private boolean allowReply = false;
	private String replyToAddress = null;
	
	private String subject = null;
	private java.lang.String mimeType = "text/plain";
	private String body = null;
	private List<Post.Attachment> attachments = null;
	
	private String to = null;
	private String cc = null;
	private String bcc = null;
	
	public Post() {
	}
	
	public Post from(final java.lang.String from) {
		this.from = from;
		return this;
	}
	public java.lang.String from() {
		return this.from;
	}
	public Post allowReply(final boolean allowReply) {
		this.allowReply = allowReply;
		return this;
	}
	public boolean allowReply() {
		return this.allowReply;
	}
	public Post replyToAddress(final java.lang.String replyToAddress) {
		this.replyToAddress = replyToAddress;
		return this;
	}
	public java.lang.String replyToAddress() {
		return this.replyToAddress;
	}
	
	public Post subject(final java.lang.String subject) {
		this.subject = subject;
		return this;
	}
	public java.lang.String subject() {
		return this.subject;
	}
	
	public Post body(final java.lang.String body) {
		this.body = body;
		return this;
	}
	public java.lang.String body() {
		return this.body;
	}
	
	public Post mimeType(final java.lang.String mimeType) {
		this.mimeType = mimeType;
		return this;
	}
	public java.lang.String mimeType() {
		return this.mimeType;
	}
	
	public Post attachment(final Post.Attachment attachment) {
		if (null == this.attachments && null != attachment) {
			this.attachments = new ArrayList<Post.Attachment>();
		}
		this.attachments.add(attachment);
		return this;
	}
	public List<Post.Attachment> attachments() {
		return this.attachments;
	}
	
	public Post to(final java.lang.String to) {
		this.to = to;
		return this;
	}
	public Post to(final java.lang.String[] to) {
		if (null != to) {
			this.to = java.lang.String.join(",", to);
		}
		return this;
	}
	public java.lang.String to() {
		return this.to;
	}
	
	public Post cc(final java.lang.String cc) {
		this.cc = cc;
		return this;
	}
	public Post cc(final java.lang.String[] cc) {
		if (null != cc) {
			this.cc = java.lang.String.join(",", cc);
		}
		return this;
	}
	public java.lang.String cc() {
		return this.cc;
	}
	
	public Post bcc(final java.lang.String bcc) {
		this.bcc = bcc;
		return this;
	}
	public Post bcc(final java.lang.String[] bcc) {
		if (null != bcc) {
			this.bcc = java.lang.String.join(",", bcc);
		}
		return this;
	}
	public java.lang.String bcc() {
		return this.bcc;
	}
}