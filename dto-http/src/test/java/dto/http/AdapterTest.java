package dto.http;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.util.Properties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Before
	public void before() {
		try {
			Properties.properties(new Properties(AdapterTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@After
	public void after() {
	}
	
  //final Adapter adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public AdapterTest() {
	  /*final java.lang.Class<?> clazz;// = null;
		try {
			clazz = Class.forName("dto.http.Adapter");
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		try {
			this.adapter = (Adapter)clazz.newInstance();
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		}*/
	}
	
	// https://squareup.com/help/ca/en/article/5068-what-are-square-s-fees
	/* Square’s processing fees are 2.65% per card present transaction,
	 * 2.9% + 30 cents per paid Square Invoice and Square Online sale,
	 * 3.4% + 15 cents per manually entered transaction and
	 * $0.10 for Interac chip & PIN, or tap sales.
	 * That’s it.
	 * These fees apply to all business types, including non-profit organizations
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void test_0000A() throws FileNotFoundException, ClientProtocolException, IOException {
		// https://developer.squareup.com/docs/customers-api/use-the-api/keep-records
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final dro.lang.String data = new dro.lang.String();
		final dro.lang.Integer status = new dro.lang.Integer();
		final String __url = dto.http.Adapter.class.getName()+"#url";
		final String url = p.property(dto.http.Adapter.class.getName()+"#url");
		// https://developer.squareup.com/reference/square/customers-api/create-customer
		{
			final java.lang.String __idempotency_key = dro.lang.String.generateString(45);
			new dto.http.Adapter()
				.properties(p)
				.header("Square-Version", "2021-03-17")
				.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
				.header("Content-Type", "application/json")
				.post(
					p.property(__url, url+"/customers")
					 .property(__url), new JSONObject(){
						private static final long serialVersionUID = 0L;
						{
							put("idempotency_key", __idempotency_key);
						  //put("company_name", "..");
						  //put("email_address", "..");
							put("family_name", "IT From Blighty");
							put("given_name", "IT From Blighty");
						  //put("nickname", "..");
						  //put("phone_number", "+1 (403) 970-6104");
						}
					}.toJSONString(),
					dto.http.Adapter.Return.Adapter
				)
				.status(status)
				.data(data)
			;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
		}
	  /*{
		  "customer": {
		    "id": "GSA67K1YGCSQQ47KSW7J7WX53M",
		    "created_at": "2020-05-27T01:06:18.682Z",
		    "updated_at": "2020-05-27T01:06:18Z",
		    "given_name": "John",
		    "family_name": "Doe",
		    "nickname": "Junior",
		    "company_name": "ACME Inc.",
		    "email_address": "john.doe.jr@acme.com",
		    "phone_number": "+1 (206) 222-3456",
		    "preferences": {
		      "email_unsubscribed": false
		    },
		    "creation_source": "THIRD_PARTY"
		  }
		}*/
	  /*final */String /*customer_*/id = null;
		if (null != data && null != data.value()) {
			try {
				final Object o = new JSONParser().parse(data.value());
				final JSONObject jo = (JSONObject)o;
				final JSONObject customer = (JSONObject)jo.get("customer");
			  /*final String customer_*/id = (String)customer.get("id");
			} catch (final ParseException e) {
				
			}
		}
		if (null != /*customer_*/id) {
			System.out.println("customer#id="+id);
		}
		System.out.println("(post-customer) status: "+status);
		
		if (null != /*customer_*/id) {
			final String /*customer*/_id = /*customer_*/id;
			// https://developer.squareup.com/reference/square/customers-api/create-customer-card
			new dto.http.Adapter()
				.properties(p)
				.header("Square-Version", "2021-03-17")
				.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
				.header("Content-Type", "application/json")
				.post(
					p.property("customer-id", /*customer_*/id)
					 .property(__url, url+"/customers/${customer-id}/cards")
					 .property(__url), new JSONObject(){
						private static final long serialVersionUID = 0L;
						{
							put("card_nonce", "cnon:card-nonce-ok");
						}
					}.toJSONString(),
					dto.http.Adapter.Return.Adapter
				)
				.status(status)
				.data(data)
			;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
		  /*{
			  "card": {
			    "id": "ccof:UCmf2iRjld10NxyN4GB",
			    "card_brand": "VISA",
			    "last_4": "5858",
			    "exp_month": 4,
			    "exp_year": 2023
			  }
			}*/
		  /*final */String /*customer_*/card_id = null;
			if (null != data && null != data.value()) {
				try {
					final Object o = new JSONParser().parse(data.value());
					final JSONObject jo = (JSONObject)o;
					final JSONObject /*customer_*/card = (JSONObject)jo.get("card");
				  /*final String customer_*/card_id = (String)/*customer_*/card.get("id");
				} catch (final ParseException e) {
					
				}
			}
			if (null != /*customer_*/card_id) {
				System.out.println("customer#card#id="+card_id);
			}
			System.out.println("(post-customer-card) status: "+status);
			
			if (null != /*customer_*/card_id) {
				final String /*customer*/_card_id = /*customer_*/card_id;
				final java.lang.String idempotency_key = dro.lang.String.generateString(45);
				// https://developer.squareup.com/reference/square/payments-api/create-payment
				{
					final java.lang.String __idempotency_key = idempotency_key;
					new dto.http.Adapter()
						.properties(p)
						.header("Square-Version", "2021-03-17")
						.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
						.header("Content-Type", "application/json")
						.post(
							p.property(__url, url+"/payments")
							 .property(__url), new JSONObject(){
								private static final long serialVersionUID = 0L;
								{
									put("idempotency_key", __idempotency_key);
									put("amount_money", new JSONObject(){
										private static final long serialVersionUID = 0L;
										{
											put("amount", 100);
											put("currency", "CAD");
										}
									});
									put("source_id", /*customer*/_card_id);
									put("autocomplete", true);
									put("customer_id", /*customer*/_id);
								  //put("location_id", "..");
								  //put("reference_id", "..");
								  //put("note", "..");
								}
							}.toJSONString(),
							dto.http.Adapter.Return.Adapter
						)
						.status(status)
						.data(data)
					;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
				}
			  /*{
				  "payment": {
				    "id": "GQTFp1ZlXdpoW4o6eGiZhbjosiDFf",
				    "created_at": "2019-07-10T13:23:49.154Z",
				    "updated_at": "2019-07-10T13:23:49.446Z",
				    "amount_money": {
				      "amount": 200,
				      "currency": "USD"
				    },
				    "app_fee_money": {
				      "amount": 10,
				      "currency": "USD"
				    },
				    "total_money": {
				      "amount": 200,
				      "currency": "USD"
				    },
				    "approved_money": {
				      "amount": 200,
				      "currency": "USD"
				    },
				    "status": "COMPLETED",
				    "source_type": "CARD",
				    "card_details": {
				      "status": "CAPTURED",
				      "card": {
				        "card_brand": "VISA",
				        "last_4": "1111",
				        "exp_month": 7,
				        "exp_year": 2026,
				        "fingerprint": "sq-1-TpmjbNBMFdibiIjpQI5LiRgNUBC7u1689i0TgHjnlyHEWYB7tnn-K4QbW4ttvtaqXw",
				        "card_type": "DEBIT",
				        "prepaid_type": "PREPAID",
				        "bin": "411111"
				      },
				      "entry_method": "ON_FILE",
				      "cvv_status": "CVV_ACCEPTED",
				      "avs_status": "AVS_ACCEPTED",
				      "auth_result_code": "nsAyY2",
				      "statement_description": "SQ *MY MERCHANT",
				      "card_payment_timeline": {
				        "authorized_at": "2019-07-10T13:23:49.234Z",
				        "captured_at": "2019-07-10T13:23:49.446Z"
				      }
				    },
				    "location_id": "XTI0H92143A39",
				    "order_id": "m2Hr8Hk8A3CTyQQ1k4ynExg92tO3",
				    "reference_id": "123456",
				    "note": "Brief description",
				    "customer_id": "RDX9Z4XTIZR7MRZJUXNY9HUK6I",
				    "receipt_number": "GQTF",
				    "receipt_url": "https://squareup.com/receipt/preview/GQTFp1ZlXdpoW4o6eGiZhbjosiDFf",
				    "version_token": "H8Vnk5Z11SKcueuRti79jGpszSEsSVdhKRrSKCOzILG6o"
				  }
				}*/
			  /*final */String payment_id = null;
				if (null != data && null != data.value()) {
					try {
						final Object o = new JSONParser().parse(data.value());
						final JSONObject jo = (JSONObject)o;
						final JSONObject payment = (JSONObject)jo.get("payment");
					  /*final String */payment_id = (String)payment.get("id");
					} catch (final ParseException e) {
						
					}
				}
				if (null != payment_id) {
					System.out.println("payment#id="+payment_id);
				}
				System.out.println("(post-payment) status: "+status);
				
				if (null != payment_id) {
					// https://developer.squareup.com/reference/square/payments-api/cancel-payment-by-idempotency-key
					// https://developer.squareup.com/reference/square/payments-api/cancel-payment
					// https://developer.squareup.com/reference/square/refunds-api/refund-payment
					{
						final java.lang.String __idempotency_key = dro.lang.String.generateString(45);
						final String __payment_id = payment_id;
						new dto.http.Adapter()
							.properties(p)
							.header("Square-Version", "2021-03-17")
							.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
							.header("Content-Type", "application/json")
							.post(
								p
								//property("payment-id", payment_id)
								//property(__url, url+"/payments/cancel")
								//property(__url, url+"/payments/${payment-id}/cancel")
								 .property(__url, url+"/refunds")
								 .property(__url), new JSONObject(){
									private static final long serialVersionUID = 0L;
									{
										put("idempotency_key", __idempotency_key);
										put("payment_id", __payment_id);
										put("amount_money", new JSONObject(){
											private static final long serialVersionUID = 0L;
											{
												put("amount", 100);
												put("currency", "CAD");
											}
										});
									}
								}.toJSONString(),
								dto.http.Adapter.Return.Adapter
							)
							.status(status)
							.data(data)
						;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
					}
				  /*final */String refund_id = null;
					if (null != data && null != data.value()) {
						try {
							final Object o = new JSONParser().parse(data.value());
							final JSONObject jo = (JSONObject)o;
							final JSONObject refund = (JSONObject)jo.get("refund");
						  /*final String */refund_id = (String)refund.get("id");
						} catch (final ParseException e) {
							
						}
					}
					if (null != refund_id) {
						System.out.println("refund#id="+refund_id);
					}
					System.out.println("(post-refund) status: "+status);
					
					if (null != refund_id) {
						// https://developer.squareup.com/reference/square/refunds-api/get-payment-refund
						{
							new dto.http.Adapter()
								.properties(p)
								.header("Square-Version", "2021-03-17")
								.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
								.header("Content-Type", "application/json")
								.get(
									p.property("refund-id", refund_id)
									 .property(__url, url+"/refunds/${refund-id}")
									 .property(__url),
									dto.http.Adapter.Return.Adapter
								)
								.status(status)
								.data(data)
							;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
						}
						System.out.println("(/get-refund) status: "+status);
					}
				}
				
				// https://developer.squareup.com/reference/square/customers-api/delete-customer-card
				new dto.http.Adapter()
					.properties(p)
					.header("Square-Version", "2021-03-17")
					.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
					.header("Content-Type", "application/json")
					.delete(
						p.property("customer-id", /*customer_*/id)
						 .property("card-id", /*customer_*/card_id)
						 .property(__url, url+"/customers/${customer-id}/cards/${card-id}")
						 .property(__url),
						dto.http.Adapter.Return.Adapter
					)
					.status(status)
					.data(data)
				;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
				System.out.println("(delete-customer-card) status: "+status);
			}
		}
		
		if (null != /*customer_*/id) {
			// https://developer.squareup.com/reference/square/customers-api/delete-customer-card
			new dto.http.Adapter()
				.properties(p)
				.header("Square-Version", "2021-03-17")
				.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
				.header("Content-Type", "application/json")
				.delete(
					p.property("customer-id", /*customer_*/id)
					 .property(__url, url+"/customers/${customer-id}")
					 .property(__url),
					dto.http.Adapter.Return.Adapter
				)
				.status(status)
			;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
			System.out.println("(delete-customer) status: "+status);
		}
	}
	
	@Test
	public void test_0000B() throws FileNotFoundException, ClientProtocolException, IOException {
		// https://developer.squareup.com/docs/customers-api/use-the-api/keep-records
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final dro.lang.String data = new dro.lang.String();
		final dro.lang.Integer status = new dro.lang.Integer();
		final String __url = dto.http.Adapter.class.getName()+"#url";
		final String url = p.property(dto.http.Adapter.class.getName()+"#url");
		// https://developer.squareup.com/reference/square/refunds-api/get-payment-refund
		final String refund_id = "p0m7qpCJIILsvdA2qF2CSUoCMKcZY_zXXAjdrVaf7mmxrbWs0Yh5G7DpH4v4shat2MSByJalO";
		{
			new dto.http.Adapter()
				.properties(p)
				.header("Square-Version", "2021-03-17")
				.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
				.header("Content-Type", "application/json")
				.get(
					p.property("refund-id", refund_id)
					 .property(__url, url+"/refunds/${refund-id}")
					 .property(__url),
					dto.http.Adapter.Return.Adapter
				)
				.status(status)
				.data(data)
			;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
		}
		System.out.println("(get-refund) status: "+status);
	}
	
	@Test
	public void test_0000C() throws FileNotFoundException, ClientProtocolException, IOException {
		// https://developer.squareup.com/docs/customers-api/use-the-api/keep-records
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final dro.lang.String data = new dro.lang.String();
		final dro.lang.Integer status = new dro.lang.Integer();
		final String __url = dto.http.Adapter.class.getName()+"#url";
		final String url = p.property(dto.http.Adapter.class.getName()+"#url");
		// https://developer.squareup.com/reference/square/disputes-api/list-disputes
		{
			new dto.http.Adapter()
				.properties(p)
				.header("Square-Version", "2021-03-17")
				.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
				.header("Content-Type", "application/json")
				.get(
					p.property(__url, url+"/disputes")
					 .property(__url),
					dto.http.Adapter.Return.Adapter
				)
				.status(status)
				.data(data)
			;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
		}
	  /*{
		  "disputes": [
		    {
		      "dispute_id": "OnY1AZwhSi775rbNIK4gv",
		      "amount_money": {
		        "amount": 1000,
		        "currency": "USD"
		      },
		      "reason": "NO_KNOWLEDGE",
		      "state": "EVIDENCE_REQUIRED",
		      "due_at": "2018-10-11T00:00:00.000Z",
		      "disputed_payments": [
		        {
		          "payment_id": "APgIq6RX2jM6DKDhMHiC6QEkuaB"
		        }
		      ],
		      "card_brand": "VISA",
		      "created_at": "2018-10-12T02:20:25.577Z",
		      "updated_at": "2018-10-12T02:20:25.577Z",
		      "brand_dispute_id": "100000809947"
		    }
		  ],
		  "cursor": "G1aSTRm48CLjJsg6Sg3hQN1b1OMaoVuG"
		}*/
		System.out.println("(get-disputes) status: "+status);
	}
	
	@Test
	public void test_0000D() throws FileNotFoundException, ClientProtocolException, IOException {
		// https://developer.squareup.com/docs/customers-api/use-the-api/keep-records
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final dro.lang.String data = new dro.lang.String();
		final dro.lang.Integer status = new dro.lang.Integer();
		final String __url = dto.http.Adapter.class.getName()+"#url";
		final String url = p.property(dto.http.Adapter.class.getName()+"#url");
		// https://developer.squareup.com/reference/square/disputes-api/get-dispute
		final String dispute_id = "..";
		{
			new dto.http.Adapter()
				.properties(p)
				.header("Square-Version", "2021-03-17")
				.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
				.header("Content-Type", "application/json")
				.get(
					p.property("dispute-id", dispute_id)
					 .property(__url, url+"/disputes/${dispute-id}")
					 .property(__url),
					dto.http.Adapter.Return.Adapter
				)
				.status(status)
				.data(data)
			;
System.err.println("data="+(null == data ? "{null}" : 0 == data.trim().length() ? "{blank}" : data));
		}
	  /*{
		  "dispute": {
		    "dispute_id": "XDgyFu7yo1E2S5lQGGpYn",
		    "amount_money": {
		      "amount": 2000,
		      "currency": "USD"
		    },
		    "reason": "NO_KNOWLEDGE",
		    "state": "LOST",
		    "due_at": "2018-11-01T00:00:00.000Z",
		    "disputed_payments": [
		      {
		        "payment_id": "6Ee10wvqhfipStz297mtUhBXvaB"
		      }
		    ],
		    "card_brand": "VISA",
		    "created_at": "2018-10-18T15:59:13.613Z",
		    "updated_at": "2018-10-18T15:59:13.613Z",
		    "brand_dispute_id": "100000282394"
		  }
		}*/
		System.out.println("(get-dispute) status: "+status);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void test_0001() throws FileNotFoundException, ClientProtocolException, IOException {
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final String url = dto.http.Adapter.class.getName()+"#url";
		p.property("service", "message.svc")
		 .property("destinationNumber", "14039706104")
		 .property(url, p.property(url)+"/${destinationNumber}")
		;
		new dto.http.Adapter()
			.properties(p)
			.post(p.property(url), new JSONObject(){
					private static final long serialVersionUID = 0L;
					{
						put("MessageBody", "Hello World!");
					}
				}.toJSONString()
			)
		;
	}
	
	@Test
	public void test_0002() throws FileNotFoundException, ClientProtocolException, IOException, ParseException {
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final String url = dto.http.Adapter.class.getName()+"#url";
		p.property("service", "incoming.svc")
		 .property("messageCount", new Integer(10).toString())
		 .property(url, p.property(url)+"/count/${messageCount}")
		;
		final String data = new dto.http.Adapter()
			.properties(p)
			.get(p.property(url), dro.lang.String.Return.String)
		;
		final Object o = new JSONParser().parse(data);
		final JSONArray ja = (JSONArray)o;
	 /* [{
		"FormattedReceivedDate":"1\/3\/2021",
		"ReceivedDate":"\/Date(1609714595370-0500)\/",
		"MessageNumber":65839761,
		"Message":"Y",
		"Reference":"WXYZ",
		"PhoneNumber":"14039706104",
		"FormattedReceivedTime":"5:56 PM",
		"AccountKey":null,
		"OutgoingMessageID":948348744
		},{
		"FormattedReceivedDate":"10\/12\/2020",
		"ReceivedDate":"\/Date(1602543433470-0400)\/",
		"MessageNumber":63405723,
		"Message":"Hi this is nam",
		"Reference":"",
		"PhoneNumber":"15877031458",
		"FormattedReceivedTime":"6:57 PM",
		"AccountKey":null,
		"OutgoingMessageID":882859394
		},{
		..
		},{
		"FormattedReceivedDate":"8\/19\/2020",
		"ReceivedDate":"\/Date(1597878384453-0400)\/",
		"MessageNumber":61842376,
		"Message":":-)",
		"Reference":"",
		"PhoneNumber":"15877031458",
		"FormattedReceivedTime":"7:06 PM",
		"AccountKey":null,
		"OutgoingMessageID":843989463
		},{
		"FormattedReceivedDate":"8\/14\/2020",
		"ReceivedDate":"\/Date(1597383990487-0400)\/",
		"MessageNumber":61687869,
		"Message":"Okokok",
		"Reference":"",
		"PhoneNumber":"15877031458",
		"FormattedReceivedTime":"1:46 AM",
		"AccountKey":null,
		"OutgoingMessageID":840123100
		}] */
		final JSONObject jo = (JSONObject)ja.get(ja.size()-1);
		final Long messageNumber = (Long)jo.get("MessageNumber");
		System.out.println("MessageNumber="+messageNumber);
	}
	
	@Test
	public void test_0003() throws FileNotFoundException, ClientProtocolException, IOException, ParseException {
		final dro.util.Properties p = new dro.util.Properties(AdapterTest.class);
		final String url = dto.http.Adapter.class.getName()+"#url";
		p.property("service", "incoming.svc")
		 .property("messageNumber", new Integer(10).toString())
		 .property(url, p.property(url)+"/afterId/${messageNumber}")
		;
		final String data = new dto.http.Adapter()
			.properties(p)
			.get(p.property(url), dro.lang.String.Return.String)
		;
		final Object obj = new JSONParser().parse(data);
		obj.hashCode();
	}
}