# readme.md #

https://passwordsgenerator.net/ -- untick 'Include Symbols', tick 'Auto-Select'

https://www.namecheap.com/support/knowledgebase/article.aspx/9422/14/generating-a-csr-on-tomcat-using-a-keytool

keytool -genkey -keysize 2048 -keyalg RSA -alias zer0ne -keystore zer0ne-dto-jetty.jks

--8<--

$ keytool -genkey -keysize 2048 -keyalg RSA -alias zer0ne -keystore zer0ne-dto-jetty.jks
Enter keystore password:  xxxxxx
Re-enter new password: xxxxxx
What is your first and last name?
  [Unknown]: itfromblighty.ca
What is the name of your organizational unit?
  [Unknown]:  IT
What is the name of your organization?
  [Unknown]:  IT From Blighty
What is the name of your City or Locality?
  [Unknown]:  Calgary
What is the name of your State or Province?
  [Unknown]:  Alberta
What is the two-letter country code for this unit?
  [Unknown]:  CA
Is CN=itfromblighty.ca, OU=IT, O=IT From Blighty, L=Calgary, ST=Alberta, C=CA correct?
  [no]:  yes

Enter key password for <zer0ne-dto-jetty>
        (RETURN if same as keystore password):

Warning:
The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard formatusing "keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.jks -deststoretype pkcs12".
-->8--

Alternative one-liner: keytool -genkey -keysize 2048 -keyalg RSA -alias zer0ne -keystore zer0ne-dto-jetty.jks -dname "CN=itfromblighty.ca, OU=IT, O=IT From Blighty, L=Calgary, ST=Alberta, C=CA, Street=.." --Note: Street is optional

--8<--

$ keytool -certreq -alias zer0ne -file itfromblighty_ca.crt -keystore zer0ne-dto-jetty.jks
Enter keystore password:  xxxxxx

Warning:
The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard formatusing "keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.jks -deststoretype pkcs12".
-->8--

https://www.namecheap.com/support/knowledgebase/article.aspx/9441/33/installing-an-ssl-certificate-on-tomcat
https://www.namecheap.com/support/knowledgebase/article.aspx/9780/2238/tomcat-installation-error-keytool-error-javalangexception-failed-to-establish-chain-from-reply

--8<--
$ keytool -import -trustcacerts -file itfromblighty_ca.ca-bundle -keystore zer0ne-dto-jetty.jks
$ keytool -import -trustcacerts -file itfromblighty_ca.crt -alias zer0ne -keystore zer0ne-dto-jetty.jks
$ keytool -list -keystore zer0ne-dto-jetty.jks -v
-->8--

http://commons.apache.org/proper/commons-daemon/download_daemon.cgi
http://www.apache.org/dist/commons/daemon/binaries/windows/

###### ----T-H-E--E-N-D---- ######