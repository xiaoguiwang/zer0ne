package dto.data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortbay.jetty.HttpStatus;

import dro.util.Properties;

public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 0L;
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	public static class Return {
		public static final java.lang.Class<dto.data.Servlet> Class = (java.lang.Class<dto.data.Servlet>)null;
	}
	
	protected static dto.data.Context.Callback callback;// = null;
	
	static {
	  //callback = null;
	}
	
	public static java.lang.Class<Servlet> setCallback(final dto.data.Context.Callback callback, final java.lang.Class<Servlet> c) {
		Servlet.callback = callback;
		return c;
	}
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
	public void init(final ServletConfig servletConfig) throws ServletException {
	  //servletConfig.getInitParameter("my.init.param");
		try {
			this.properties(new Properties(this));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		if (null == Properties.properties()) {
			Properties.properties(this.properties());
		}
	}
	
	private void doMethod(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  /*request.getSession()
		       .getServletContext()
		       .getInitParameter("my.context.param");*/
		boolean authenticated = false;
		final String remoteUser = request.getRemoteUser();
		if (null != remoteUser) {
			authenticated = true;
		}
	  /*final Principal userPrincipal = request.getUserPrincipal();
		if (null != userPrincipal) {
			final String name = userPrincipal.getName();
			if (null != name) {
			authenticated = true;
		}*/
	  /*final HttpSession httpSession = request.getSession();
		if (null != httpSession) {
		}*/
	  /*final String queryString = request.getQueryString();
		String[] pairs = null;
		if (null != queryString) {
			pairs = queryString.split("&");
		}
		if (null != pairs) {
			for (final String pair: pairs) {
				final String[] split = pair.split("=");
				if (true == "id".equals(split[0])) {
					final String id = split[1];
					if (null == id);
				}
			}
		}*/
		final BufferedReader br = request.getReader();
		StringBuffer sb = null;
		String line = "";
		while (line != null) {
			try {
				line = br.readLine();
				if (null == line) {
					break;
				}
				if (null == sb) {
					sb = new StringBuffer();
				}
				sb.append(line);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
		final String json = null == sb ? null : sb.toString();
		if (true == authenticated && false == dro.lang.String.isNullOrTrimmedBlank(json)) {
		  //final Data d = Data.fromJSON(json);
		} else {
			response.setStatus(HttpStatus.ORDINAL_401_Unauthorized);
		}
	}
	
	protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
	}
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
	}
	protected void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
	}
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
		Servlet.callback.invoke(this);
		final java.io.PrintWriter pw = response.getWriter();
		pw.append("Hello World! {dto.data.Servlet}").flush();
		response.setStatus(HttpStatus.ORDINAL_200_OK);
	}
	protected void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
	}
	
	public Servlet() {
	}
}