package dto.jetty;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.bio.SocketConnector;
import org.mortbay.jetty.security.Constraint;
import org.mortbay.jetty.security.ConstraintMapping;
import org.mortbay.jetty.security.HashUserRealm;
import org.mortbay.jetty.security.SecurityHandler;
import org.mortbay.jetty.security.SslSocketConnector;
import org.mortbay.jetty.security.UserRealm;
import org.mortbay.jetty.servlet.ServletHandler;

import dro.util.Properties;
import dro.util.State;
/*
POST
GET with filter is our alternative to exists here..
PUT
[HEAD]? => action, e.g. logout. Instead of PUTing a state change
DELETE
*/
// This is dto.jetty.Adapter
public class Adapter extends dto.Adapter {
	private static final java.lang.String version = "0.0.1-SNAPSHOT";
	
	public static final class Server {
		private static HashUserRealm newHashUserRealm(final java.lang.String realm) {
			return new HashUserRealm(){
				{
					setName(realm);
				}
			};
		}
		private static ConstraintMapping newConstraintMapping(final java.lang.String[] roles, final java.lang.String pathSpec) {
			return new ConstraintMapping(){
				{
					setConstraint(new Constraint(){
						private static final long serialVersionUID = 0L;
						{
							setName(Constraint.__BASIC_AUTH);
							setRoles(roles);
							setAuthenticate(true);
						}
					});
					setPathSpec(pathSpec);
				}
			};
		}
		private static SecurityHandler newSecurityHandlerForRealm(final UserRealm ur, final ConstraintMapping cm) {
			final SecurityHandler sh = new SecurityHandler(){
				{
					setUserRealm(ur);
					setConstraintMappings(new ConstraintMapping[]{cm});
				}
			};
			return sh;
		}
		private static void addUsersToRealm(final java.lang.String protocol, final java.lang.String hostname, final HashUserRealm ur) {
			final dro.util.Properties p = Properties.properties();
			final String passwords = p.getProperty(dro.lang.Class.getName(Client.class)+"#passwords", "passwords", dro.lang.Return.String);
			if (null != passwords && 0 < passwords.trim().length()) {
				final File d = new File(passwords+File.separator+protocol+File.separator+hostname);//+File.separator+ur.getName());
				if (true == d.exists()) {
					final String[] fa = d.list();
					for (final String username: fa) {
						try {
							final String password = new dro.io.File(d.getPath()+File.separator+username)
								.returns(dro.io.File.Return.Reader)
								.read(dro.lang.String.Return.String)
							;
							ur.put(username, password);
						} catch (final IOException e) {
							throw new RuntimeException(e);
						} catch (final RuntimeException e) {
							throw new RuntimeException(e);
						}
						ur.addUserToRole(username, "user");
					}
				}
			}
			return;
		}
		
		private org.mortbay.jetty.Server server = null;
		protected State state;// = State.NONE;
		
		{
			this.state = State.INITIALISING;
		}
		
		public Server(final dto.data.Context.Callback callback) {
			final dro.util.Properties p = Properties.properties();
			final String hostInsecure = p.getProperty(dro.lang.Class.getName(this.getClass())+"@org.mortbay.jetty.Server#hostInsecure");
			if (false == dro.lang.String.isNullOrTrimmedBlank(hostInsecure)) {
				System.setProperty("jetty.host", hostInsecure);
			}
			final String _portInsecure = p.getProperty(dro.lang.Class.getName(this.getClass())+"@org.mortbay.jetty.Server#portInsecure");
			final Integer portInsecure = new Integer((true == dro.lang.String.isNullOrTrimmedBlank(_portInsecure)) ? 80 : Integer.parseInt(_portInsecure));
			this.server = new org.mortbay.jetty.Server(/*this.portInsecure*/);
			{
				final SocketConnector connector = new SocketConnector();
				connector.setPort(portInsecure);
				connector.setHost(hostInsecure);
				this.server.addConnector(connector);
			}
			
			final HashUserRealm hur;// = null;
			final java.lang.String uri = "/zer0ne/api/"+version;
			final java.lang.String[] roles = new java.lang.String[]{"user"};
			final java.lang.String pathSpec = uri;
			final ConstraintMapping cm;// = null;
			final SecurityHandler sh;// = null;
			
			final String _portSecure = p.getProperty(dro.lang.Class.getName(this.getClass())+"@org.mortbay.jetty.Server#portSecure");
			final Integer portSecure = (true == dro.lang.String.isNullOrTrimmedBlank(_portSecure)) ? null : new Integer(_portSecure);
			if (null == portSecure) {
				final java.lang.String realm = "zer0ne: insecure";
				hur = Server.newHashUserRealm(realm);
				Server.addUsersToRealm("http", hostInsecure, hur);
				cm = Server.newConstraintMapping(roles, pathSpec);
				sh = Server.newSecurityHandlerForRealm(hur, cm);
			} else {
				final SslSocketConnector connector = new SslSocketConnector();
				final String hostSecure = p.getProperty(dro.lang.Class.getName(this.getClass())+"@org.mortbay.jetty.Server#hostSecure");
				if (false == dro.lang.String.isNullOrTrimmedBlank(hostSecure)) {
					connector.setHost(hostSecure);
				}
				connector.setPort(portSecure);
				
				final String passwords = p.getProperty(dro.lang.Class.getName(this.getClass())+"#passwords", "passwords", dro.lang.Return.String)+File.separator+"jks";
				if (null != passwords && 0 < passwords.trim().length()) {
					final String keystores = p.getProperty(dro.lang.Class.getName(this.getClass())+"#keystores", "keystores"+File.separator+"jks", dro.lang.Return.String);
					final String keystorePass;// = null;
					try {
						keystorePass = new dro.io.File(passwords+File.separator+"dto-jetty-mortbay-org")
							.returns(dro.io.File.Return.Reader)
							.read(dro.lang.String.Return.String)
						;
					} catch (final IOException e) {
						throw new RuntimeException(e);
					} catch (final RuntimeException e) {
						throw new RuntimeException(e);
					}
					connector.setKeystore(keystores+File.separator+"dto-jetty-mortbay-org.jks");
					connector.setKeyPassword(keystorePass);
					connector.setTruststore(keystores+File.separator+"dto-jetty-mortbay-org.jks");
					connector.setTrustPassword(keystorePass);
				  //connector.setPassword(keystorePass);
					this.server.addConnector(connector);
				}
				
				final java.lang.String realm = "zer0ne: secure";
				hur = Server.newHashUserRealm(realm);
				Server.addUsersToRealm("https", hostSecure, hur);
				cm = Server.newConstraintMapping(roles, pathSpec);
				sh = Server.newSecurityHandlerForRealm(hur, cm);
			}
			this.server.setUserRealms(new UserRealm[]{hur});
			final ServletHandler servletHandler = new ServletHandler();
			dto.meta.data.Servlet.setCallback(callback, dto.meta.data.Servlet.Return.Class);
			servletHandler.addServletWithMapping(dto.meta.data.Servlet.class, uri+"/dto/meta/Data");
			dto.data.Servlet.setCallback(callback, dto.data.Servlet.Return.Class);
			servletHandler.addServletWithMapping(dto.data.Servlet.class, uri+"/dto/Data");
			this.server.setHandlers(new Handler[]{sh, servletHandler});
			
			if (null != this.server) {
				try {
					this.server.start();
					this.state = State.RUNNING;
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		public State state() {
			return this.state;
		}
		
		public void shutdown() {
			if (null != this.server) {
				try {
					this.server.stop();
					this.state = State.SHUTDOWN;
				} catch (final Exception e) {
					this.state = State.NONE;
					throw new RuntimeException(e);
				}
				this.server.destroy();
				this.server = null;
			}
		}
	}
	
	public static final class Client {
		private static TrustManager newTrustManager() {
			return new X509TrustManager(){
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				@Override
				public void checkClientTrusted(final X509Certificate[] certs, final String authType) throws CertificateException {
					return;
				}
				@Override
				public void checkServerTrusted(final X509Certificate[] certs, final String authType) throws CertificateException {
					return;
				}
			};
		}
		private static SSLContext newSSLContext() {
			final SSLContext sslContext;// = null;
			try {
				sslContext = SSLContext.getInstance("SSL");
				sslContext.init(null, new TrustManager[]{Client.newTrustManager()}, new SecureRandom());
			} catch (final KeyManagementException e) {
				throw new RuntimeException(e);
			} catch (final NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
			return sslContext;
		}
		@SuppressWarnings("deprecation")
		private static SSLConnectionSocketFactory newSSLConnectionSocketFactory() {
		  /*final */SSLConnectionSocketFactory sf = null;
			final SSLContext sslContext = Client.newSSLContext();
			if (null != sslContext) {
				sf = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			}
			return sf;
		}
		private static CredentialsProvider newBasicCredentialsProvider(final java.lang.String username, final java.lang.String password) {
			final CredentialsProvider cp = new BasicCredentialsProvider(){
				{
					setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
				}
			};
			return cp;
		}
		private static HttpClientContext newHttpClientContext(final HttpHost httpHost, final java.lang.String username, final java.lang.String password) {
			final HttpClientContext ctx = HttpClientContext.create();
			final AuthCache ac = new BasicAuthCache(){
				{
					put(httpHost, new BasicScheme());
				}
			};
			ctx.setAuthCache(ac);
			ctx.setCredentialsProvider(Client.newBasicCredentialsProvider(username, password));
			return ctx;
		}
		
		private static java.lang.String getUserPassword(final java.lang.String protocol, final java.lang.String hostname, final java.lang.String username) {
		  /*final */String password = "";
			final dro.util.Properties p = Properties.properties();
			final String passwords = p.getProperty(dro.lang.Class.getName(Client.class)+"#passwords", "passwords", dro.lang.Return.String);
			if (null != passwords && 0 < passwords.trim().length()) {
				final dro.io.File f = new dro.io.File(passwords+File.separator+protocol+File.separator+hostname+File.separator+username);
				try {
					password = new String(
						f.returns(dro.io.File.Return.Reader).read(dro.lang.String.Return.String)
					);
				} catch (final IOException e) {
					throw new RuntimeException(e);
				} catch (final RuntimeException e) {
					throw new RuntimeException(e);
				}
			}
			return password;
		}
		
		protected final CloseableHttpClient httpClient;// = null;
		protected final HttpClientContext ctx;// = null;
		protected final java.lang.String url;// = null;
		
		public Client() {
			final dro.util.Properties p = Properties.properties();
			this.url = p.getProperty(dro.lang.Class.getName(Client.class)+"#url");
			final HttpHost httpHost;// = null;
			final String hostname;// = null;
			{
				try {
					hostname = new URL(url).getHost();
				} catch (final MalformedURLException e) {
					throw new RuntimeException(e);
				}
				final int port = 443;
				final java.lang.String protocol = "https";
				httpHost = new HttpHost(hostname, port, protocol);
			}
		  //final String hostname = p.getProperty(dro.lang.Class.getName(Client.class)+"#hostname");
			final String username = p.getProperty(dro.lang.Class.getName(Client.class)+"#username");
			final String password = Client.getUserPassword("https", hostname, username);
			
			// https://www.baeldung.com/java-7-tls-v12
			final SSLConnectionSocketFactory sf = Client.newSSLConnectionSocketFactory();
			final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("https", sf)
				.register("http", new PlainConnectionSocketFactory())
				.build();
			this.httpClient = HttpClients
				.custom()
				.setConnectionManager(new BasicHttpClientConnectionManager(registry)).setSSLSocketFactory(sf)
			  //.setHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
				.build();
			this.ctx = Client.newHttpClientContext(httpHost, username, password);
		}
		
		public Client send(final java.lang.String json) {
			final HttpGet request = new HttpGet(this.url);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			try {
			  //final HttpEntity httpEntity = new StringEntity("");//d.toJSON());
			  //request.setEntity(httpEntity); // For POST
				final CloseableHttpResponse response = this.httpClient.execute(request, this.ctx);
				try {
					final StatusLine sl = response.getStatusLine();
					if (200 == sl.getStatusCode()) {
					  //HttpEntity entity = response.getEntity();
					  //final java.lang.String string = EntityUtils.toString(entity, "UTF-8");
						System.out.println(sl.getStatusCode() + ": " + sl.getReasonPhrase());
					} else {
						System.err.println(sl.getStatusCode() + ": " + sl.getReasonPhrase());
					  //break;
					}
				} finally {
					response.close();
				}
			} catch (final ClientProtocolException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
			return this;
		}
	}
	
  /*public Adapter() {
		final Server server = new Adapter.Server();
		if (State.RUNNING == server.state()) {
			final String _sleepInMilliseconds = p.getProperty(dro.lang.Class.getName(Adapter.class)+"#sleepInMilliseconds");
			long sleepInMilliseconds = null == _sleepInMilliseconds ? 10000L : new Long(_sleepInMilliseconds).longValue();
			while (State.RUNNING == server.state()) {
				try {
					Thread.sleep(sleepInMilliseconds);
				} catch (final InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		  //server.shutdown();
		}
	}*/
}