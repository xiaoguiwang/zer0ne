package dto.meta.data;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortbay.jetty.HttpStatus;

public class Servlet extends dto.data.Servlet {
	private static final long serialVersionUID = 0L;
	
	public static class Return {
		public static final java.lang.Class<dto.data.Servlet> Class = (java.lang.Class<dto.data.Servlet>)null;
	}
	
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //super.doMethod(request, response);
		Servlet.callback.invoke(this);
		final java.io.PrintWriter pw = response.getWriter();
		pw.append("Hello World! {dto.meta.data.Servlet}").flush();
		response.setStatus(HttpStatus.ORDINAL_200_OK);
	}
}