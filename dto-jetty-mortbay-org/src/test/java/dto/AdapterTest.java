package dto;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.util.Properties;
import dto.data.Context;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Before
	public void before() {
		try {
			Properties.properties(new Properties(AdapterTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	final Context adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public AdapterTest() {
		final java.lang.Class<?> clazz;// = null;
		try {
			clazz = Class.forName("dto.jetty.Adapter");
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		try {
			this.adapter = (Context)clazz.newInstance();
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void test_0001() {
	}
	
	@After
	public void after() {
	}
}