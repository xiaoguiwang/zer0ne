package dto.jetty;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.util.Properties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Before
	public void before() {
	}
	
	final Adapter adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public AdapterTest() {
		try {
			Properties.properties(new Properties(AdapterTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		this.adapter = new Adapter();
	}
	
	@Test
	public void test_0001() {
	}
	
	@After
	public void after() {
	}
}