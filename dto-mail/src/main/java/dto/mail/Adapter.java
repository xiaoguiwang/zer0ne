package dto.mail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

//import com.sun.mail.imap.IMAPProvider;
//import com.sun.mail.smtp.SMTPProvider;

import dto.mail.internet.MimeMessage;

// This is dto.mail.Adapter
public class Adapter extends dto.Adapter {
  //private static final java.lang.String version = "0.0.1-SNAPSHOT";
	
	public static final class Protocol {
		public static final Protocol SMTP = new Protocol("smtp");
		public static final Protocol IMAP = new Protocol("imap");
		private final String protocol;// = null;
		{
		  //protocol = null;
		}
		private Protocol(final String protocol) {
			this.protocol = protocol;
		}
		@Override
		public String toString() {
			return this.protocol;
		}
	}
	
	// FIXME! - Ugly!
	private static String parseAddresses(final Address[] address) {
	  /*final */String listAddress = "";
		for (int i = 0; i < (null == address ? 0 : address.length); i++) {
			listAddress += address[i].toString()+", ";
		}
		if (1 < listAddress.length()) {
			listAddress = listAddress.substring(0, listAddress.length()-2);
		}
		return listAddress;
	}
	
	private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Class<?> propertyClass, final java.lang.Object propertyObject) {
	  /*final */java.lang.Object propertyValue = p.getProperty(propertyKey);
		if (null == propertyValue && null == propertyObject) {
			System.clearProperty(propertyKey);
this.properties.remove(propertyKey);
		} else if (propertyClass == java.lang.String.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			System.setProperty(propertyKey, (java.lang.String)propertyValue);
this.properties.setProperty(propertyKey, (java.lang.String)propertyValue);
		} else if (propertyClass == java.lang.Integer.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			if (propertyValue.getClass() == java.lang.Integer.class) {
				System.setProperty(propertyKey, java.lang.Integer.toString((java.lang.Integer)propertyValue));
this.properties.setProperty(propertyKey, java.lang.Integer.toString((java.lang.Integer)propertyValue));
			} else if (propertyValue.getClass() == java.lang.String.class) {
				System.setProperty(propertyKey, (java.lang.String)propertyValue);
this.properties.setProperty(propertyKey, (java.lang.String)propertyValue);
			} else {
				throw new UnsupportedOperationException();
			}
		} else if (propertyClass == java.lang.Boolean.class) {
			propertyValue = null != propertyValue ? propertyValue : propertyObject;
			if (propertyValue.getClass() == java.lang.Boolean.class) {
				System.setProperty(propertyKey, java.lang.Boolean.toString((java.lang.Boolean)propertyValue));
this.properties.setProperty(propertyKey, java.lang.Boolean.toString((java.lang.Boolean)propertyValue));
			} else if (propertyValue.getClass() == java.lang.String.class) {
				System.setProperty(propertyKey, (java.lang.String)propertyValue);
this.properties.setProperty(propertyKey, (java.lang.String)propertyValue);
			} else {
				throw new UnsupportedOperationException();
			}
		} else {
			throw new IllegalArgumentException();
		}
		return this;
	}
	private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Class<?> propertyClass) {
		return this.property(p, propertyKey, propertyClass, null);
	}
	private Adapter property(final dro.util.Properties p, final java.lang.String propertyKey, final java.lang.Object propertyObject) {
		return this.property(p, propertyKey, propertyObject.getClass(), propertyObject);
	}
	
	protected java.util.Properties properties = new java.util.Properties();
  //protected final IMAPProvider imap;// = new IMAPProvider();
  //protected final SMTPProvider smtp;// = new SMTPProvider();
	
	private final Adapter.Protocol protocol;// = null;
	
	public Adapter(final Adapter.Protocol protocol) {
	  //super(); // Implicit super constructor..
		this.protocol = protocol;
	  //this.imap = new IMAPProvider();
	  //this.smtp = new SMTPProvider();
	  //final MailcapCommandMap mailMap = (MailcapCommandMap)CommandMap.getDefaultCommandMap();
	  //mailMap.addMailcap("multipart/mixed;;x-java-content-handler=com.sun.mail.handlers.multipart_mixed"); // double semi-colon seems to be correct according to (other) examples on the internet
	}
	@Override
	public Adapter properties(final dro.util.Properties p) {
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (Adapter.Protocol.SMTP == this.protocol) {
			this.property(p, "mail.smtp.host", "localhost")
			    .property(p, "mail.smtp.port", 25)
			    .property(p, "mail.smtp.auth", java.lang.Boolean.FALSE)
			  //.property(p, "mail.smtps.auth", false)
			    .property(p, "mail.smtp.user", java.lang.String.class)
			    .property(p, "mail.smtp.password", java.lang.String.class)
			    .property(p, "mail.smtp.starttls.enable", java.lang.Boolean.FALSE)
			    .property(p, "mail.smtp.socketFactory.port", java.lang.Integer.class)
			    .property(p, "mail.smtp.socketFactory.class", java.lang.String.class)
			    .property(p, "mail.smtp.socketFactory.fallback", java.lang.Boolean.class);
			;
			if (java.lang.Boolean.TRUE.toString().equals(System.getProperty("mail.smtp.auth"))) {
				final String mail_smtp_user = System.getProperty("mail.smtp.user");
				if (false == dro.lang.String.isNullOrTrimmedBlank(mail_smtp_user)) {
					final String mail_smtp_host = System.getProperty("mail.smtp.host");
					final java.lang.String passwords = p.getProperty(dro.lang.Class.getName(Adapter.class)+"#passwords");
					final java.lang.String mail_smtp_password;// = null;
					try {
						mail_smtp_password = new dro.io.File(
							passwords+dro.io.File.separator+"smtps"+dro.io.File.separator+mail_smtp_host+dro.io.File.separator+mail_smtp_user
						).returns(dro.io.File.Return.Reader).read(dro.lang.String.Return.String).trim();
					  //p.setProperty("mail.smtp.password", mail_smtp_password);
						if (false == dro.lang.String.isNullOrTrimmedBlank(mail_smtp_password)) {
							System.setProperty("mail.smtp.password", mail_smtp_password);
						}
					} catch (final IOException e) {
						throw new RuntimeException(e);
					} catch (final RuntimeException e) {
						throw new RuntimeException(e);
					}
				} else {
					throw new IllegalArgumentException();
				}
			}
		} else if (Adapter.Protocol.IMAP == this.protocol) {
			this.property(p, "mail.imap.host", "localhost")
			    .property(p, "mail.imap.port", 25)
			    .property(p, "mail.imap.auth", java.lang.Boolean.FALSE)
			  //.property(p, "mail.imaps.auth", false)
			    .property(p, "mail.imap.user", java.lang.String.class)
			    .property(p, "mail.imap.password", java.lang.String.class)
			    .property(p, "mail.imap.starttls.enable", java.lang.Boolean.FALSE)
			    .property(p, "mail.imap.socketFactory.port", java.lang.Integer.class)
			    .property(p, "mail.imap.socketFactory.class", java.lang.String.class)
			    .property(p, "mail.imap.socketFactory.fallback", java.lang.Boolean.class);
			if (java.lang.Boolean.TRUE.toString().equals(System.getProperty("mail.imap.auth"))) {
				final String mail_imap_user = System.getProperty("mail.imap.user");
				if (false == dro.lang.String.isNullOrTrimmedBlank(mail_imap_user)) {
					final String mail_imap_host = System.getProperty("mail.imap.host");
					final java.lang.String passwords = p.getProperty(dro.lang.Class.getName(Adapter.class)+"#passwords");
					final java.lang.String mail_imap_password;// = null;
					try {
						mail_imap_password = new dro.io.File(
							passwords+dro.io.File.separator+"imaps"+dro.io.File.separator+mail_imap_host+dro.io.File.separator+mail_imap_user
						).returns(dro.io.File.Return.Reader).read(dro.lang.String.Return.String).trim();
					  //p.setProperty("mail.smtp.password", mail_smtp_password);
						if (false == dro.lang.String.isNullOrTrimmedBlank(mail_imap_password)) {
							System.setProperty("mail.imap.password", mail_imap_password);
						}
					} catch (final IOException e) {
						throw new RuntimeException(e);
					} catch (final RuntimeException e) {
						throw new RuntimeException(e);
					}
				} else {
					throw new IllegalArgumentException();
				}
			}
		} else {
			throw new IllegalArgumentException();
		}
		this.property(p, "mail.debug", true);
		return this;
	}
	
	public Adapter send(final Email email) throws RuntimeException {
		final javax.mail.Authenticator auth;// = null;
		if (true == java.lang.Boolean.TRUE.toString().equals(System.getProperty("mail.smtp.auth"))/* || true == java.lang.Boolean.TRUE.toString().equals(System.getProperty("mail.smtps.auth"))*/) {
			auth = new javax.mail.Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication() {
					final java.lang.String mail_smtp_user     = System.getProperty("mail.smtp.user"    );
					final java.lang.String mail_smtp_password = System.getProperty("mail.smtp.password");
					return new PasswordAuthentication(mail_smtp_user, mail_smtp_password);
				}
			};
		} else {
			auth = null;
		}
		
	  //final Session s = Session.getDefaultInstance(System.getProperties(), auth);
		final Session s;// = null;
		if (null != auth) {
			s = Session.getInstance(/*System.getProperties()*/this.properties, auth);
		} else {
			s = Session.getInstance(/*System.getProperties()*/this.properties);
		}
	  //s.addProvider(this.imap);
	  //s.addProvider(this.smtp);
		
	  /*final */String messageID = null;
		final MimeMessage m = new MimeMessage(s/*, messageID*/);
	  /*try {
			m.addHeader("Content-type", email.mimeType()); // Text or Rich-Text (see MultiPart setContent below for more details on HTML)
			m.addHeader("format", "flowed");
			m.addHeader("Content-Transfer-Encoding", "8bit");
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		}*/
		
		if (null == email.from()) {
			email.from(System.getProperty(dro.lang.Class.getName(javax.mail.internet.MimeMessage.class)+"#from"));
		}
		final java.lang.String[] fromAddressAndName = email.from().split(":");
		try {
			if (2 == fromAddressAndName.length) {
				m.setFrom(new InternetAddress(fromAddressAndName[0], fromAddressAndName[1]));
			} else {
				m.setFrom(new InternetAddress(fromAddressAndName[0]));
			}
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		} catch (final UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		if (null == email.replyToAddress()) {
			email.replyToAddress(System.getProperty(dro.lang.Class.getName(javax.mail.internet.MimeMessage.class)+"#replyToAddress"));
		}
		try {
		  //email.allowReply(..getProperty(dro.lang.Class.getName(javax.mail.internet.MimeMessage.class)+"#allowReply", false, dro.lang.Return.Boolean));
			if (null != email.replyToAddress()) {
				m.setReplyTo(InternetAddress.parse(email.replyToAddress(), email.allowReply()));
			}
			m.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.to(), false));
			if (null != email.cc()) {
				m.setRecipients(Message.RecipientType.CC, InternetAddress.parse(email.cc(), false));
			}
			if (null != email.bcc()) {
				m.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(email.bcc(), false));
			}
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		}
		
		try {
			m.setSubject(email.subject());
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		}
		
		// https://stackoverflow.com/questions/30792143/javamail-classcastexception-when-sending-multipart-messages
		final Multipart multipart = new MimeMultipart(/**/"mixed"/**/);
		final Multipart alternativeMultipart = new MimeMultipart("alternative");
		try {
			final MimeBodyPart bodyPart = new MimeBodyPart();
			if (true == email.mimeType().equals("text/plain")) {
			  //bodyPart.setText(email.body());
				try {
					bodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(email.body().getBytes(), "text/plain")));
				} catch (final MessagingException e) {
					throw new RuntimeException(e);
				}
			} else {
			  //bodyPart.setContent(email.body(), email.mimeType());
				try {
					bodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(email.body().getBytes(), email.mimeType())));
				} catch (final MessagingException e) {
					throw new RuntimeException(e);
				}
			}
			alternativeMultipart.addBodyPart(bodyPart);
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		}
		try {
			final MimeBodyPart bodyPart = new MimeBodyPart();
			bodyPart.setContent(alternativeMultipart);
			multipart.addBodyPart(bodyPart);
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		}
		
		final java.util.List<Email.Attachment> attachments = email.attachments();
		if (null != attachments && 0 < attachments.size()) {
			for (final Email.Attachment attachment: attachments) {
				final BodyPart attachmentPart = new MimeBodyPart();
				final Object attachedContent = attachment.object();
				try {
					if (attachedContent instanceof String) {
						final DataSource source = new FileDataSource((java.lang.String)attachedContent);
						attachmentPart.setDataHandler(new DataHandler(source));
						attachmentPart.setFileName((new java.io.File(((java.lang.String)attachedContent)).getName()));
						multipart.addBodyPart(attachmentPart);
					} else if (attachedContent instanceof URL) {
						final DataSource source = new URLDataSource((URL)attachedContent);
						attachmentPart.setDataHandler(new DataHandler(source));
						attachmentPart.setFileName((new java.io.File(((URL)attachedContent).getFile()).getName()));
						multipart.addBodyPart(attachmentPart);
					} else if (attachedContent instanceof byte[]) {
						final DataSource source = new ByteArrayDataSource((byte[])attachedContent, attachment.mimeType());
						attachmentPart.setDataHandler(new DataHandler(source));
						attachmentPart.setFileName(attachment.name());
						multipart.addBodyPart(attachmentPart);
					}
				} catch (final MessagingException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		try {
			m.setContent(multipart);
			m.setSentDate(new Date());
		/**/m.saveChanges();
		/**/messageID = m.getMessageID(); // e.g. 178178314.2.1612763905615@FCL5CG9457X1Y.FCL.ad.crs
			messageID.hashCode(); // TODO: do this later - track reply to a sign-up, password-reset, or possibly sign-in
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		}
		
		// https://stackoverflow.com/questions/10471073/javamail-returning-smtp-as-transport-instead-of-smtps
	  //Transport t = s.getTransport(/*"smtps"*/);
		try {
			final Thread t = Thread.currentThread();
			final ClassLoader cl = t.getContextClassLoader();
			t.setContextClassLoader(s.getClass().getClassLoader());
			try {
				m.writeTo(System.out);
				Transport.send(m);
messageID = m.getMessageID();
email.setMessageID(messageID);
			} catch (final IOException e) {
				e.printStackTrace(System.err);
			} finally {
				t.setContextClassLoader(cl);
			}
		  /*if (null != System.getProperty("mail.smtp.password")) {
				if (null != System.getProperty("mail.smtp.host")) {
					if (null != System.getProperty("mail.smtp.port")) {
						t.connect(
							System.getProperty("mail.smtp.host"),
							Integer.parseInt(System.getProperty("mail.smtp.port")),
							System.getProperty("mail.smtp.user"),
							System.getProperty("mail.smtp.password")
						);
					} else {
						t.connect(
							System.getProperty("mail.smtp.host"),
							System.getProperty("mail.smtp.user"),
							System.getProperty("mail.smtp.password")
						);
					}
				} else {
					t.connect(
						System.getProperty("mail.smtp.user"),
						System.getProperty("mail.smtp.password")
					);
				}
			}
			t.sendMessage(m, m.getAllRecipients());*/
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		} finally {
		  //t.close();
		}
		
		return this;
	}
	
	public Email[] receive() throws RuntimeException {
	  /*final */java.util.List<dto.mail.Email> emails = null;
		final javax.mail.Authenticator auth;// = null;
		if (true == java.lang.Boolean.TRUE.toString().equals(System.getProperty("mail.imap.auth"))/* || true == java.lang.Boolean.TRUE.toString().equals(System.getProperty("mail.imaps.auth"))*/) {
			auth = new javax.mail.Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication() {
					final java.lang.String mail_imap_user     = System.getProperty("mail.imap.user"    );
					final java.lang.String mail_imap_password = System.getProperty("mail.imap.password");
					return new PasswordAuthentication(mail_imap_user, mail_imap_password);
				}
			};
		} else {
			auth = null;
		}
		
	  //final Session s = Session.getDefaultInstance(System.getProperties(), auth);
		final Session s;// = null;
		if (null != auth) {
			s = Session.getInstance(/*System.getProperties()*/this.properties, auth);
		} else {
			s = Session.getInstance(/*System.getProperties()*/this.properties);
		}
	  //s.addProvider(this.imap);
	  //s.addProvider(this.smtp);
		
	  //Transport t = s.getTransport(/*"imaps"*/);
		try {
			final Thread t = Thread.currentThread();
			final ClassLoader cl = t.getContextClassLoader();
			t.setContextClassLoader(s.getClass().getClassLoader());
			try {
				// https://www.codejava.net/java-ee/javamail/receive-e-mail-messages-from-a-pop3-imap-server
				final Store store = s.getStore("imap");
				store.connect();
				final Folder inbox = store.getFolder("INBOX");
				inbox.open(Folder.READ_WRITE);
				final Message[] ma = inbox.getMessages(/*TODO*/);
				for (int i = 0; i < (null == ma ? 0 : ma.length); i++) {
					final dto.mail.Email email = new dto.mail.Email();
					final Message m = ma[i];
					final Enumeration<Header> headers = m.getAllHeaders();
					while (true == headers.hasMoreElements()) {
						final Header header = headers.nextElement();
						final String name = header.getName();
						if (true == name.equals("References")) { // FIXME!
							final String[] values = header.getValue().split("References:\\s*");
							email.setRepliedMessageID(values[0]); // BEWARE!
						}
					}
					if (null != email.getRepliedMessageID()) {
						email.from(m.getFrom().toString());
						email.subject(m.getSubject());
					  /*final String toList = */Adapter.parseAddresses(m.getRecipients(RecipientType.TO));
					  /*final String ccList = */Adapter.parseAddresses(m.getRecipients(RecipientType.CC));
					  //final String sentDate = m.getSentDate().toString();
						final String contentType = m.getContentType();
					  /*final */String messageContent = null;
						if (contentType.contains("text/plain")
							||
							contentType.contains("text/html")
						) {
							try {
								final Object content = m.getContent();
								if (null != content) {
									messageContent = content.toString();
								}
							} catch (final Exception e) {
								e.printStackTrace(System.err);
							}
						}
						if (null != messageContent) {
							email.body(messageContent);
						}
						if (null == emails) {
							emails = new java.util.ArrayList<Email>();
						}
						emails.add(email);
					}
ma[i].setFlag(Flags.Flag.DELETED, true);
				}
				try {
					inbox.close(true);
				} catch (final FolderClosedException e) {
					// Silently ignore..
				}
				store.close();
			} finally {
				t.setContextClassLoader(cl);
			}
		} catch (final NoSuchProviderException e) {
			e.printStackTrace(System.err);
		} catch (final MessagingException e) {
			throw new RuntimeException(e);
		} finally {
		  //s.close();
		}
		
		return null == emails ? null : emails.toArray(new dto.mail.Email[0]);
	}
}