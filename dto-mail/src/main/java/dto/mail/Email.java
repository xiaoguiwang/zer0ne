package dto.mail;

import java.util.ArrayList;
import java.util.List;

public class Email {
	public static class Attachment {
		private java.lang.Object object = null;
		private java.lang.String mimeType = "text/plain";
		private java.lang.String name = null;
		
		public Attachment(final java.lang.Object object) {
			this(object, null, null);
		}
		public Attachment(final java.lang.Object object, final java.lang.String mimeType) {
			this(object, mimeType, null);
		}
		public Attachment(final java.lang.Object object, final java.lang.String mimeType, final java.lang.String name) {
			this.mimeType(mimeType);
			this.object(object);
			this.name(name);
		}
		
		public Attachment object(final java.lang.Object object) {
			this.object = object;
			return this;
		}
		public java.lang.Object object() {
			return this.object;
		}
		
		public Attachment mimeType(final java.lang.String mimeType) {
			this.mimeType = mimeType;
			return this;
		}
		public java.lang.String mimeType() {
			return this.mimeType;
		}
		
		public Attachment name(final java.lang.String name) {
			this.name = name;
			return this;
		}
		public java.lang.String name() {
			return this.name;
		}
	}
	
	private String from = null;
	private boolean allowReply = false;
	private String replyToAddress = null;
	
	private String subject = null;
	private java.lang.String mimeType = "text/plain";
	private String body = null;
	private List<Email.Attachment> attachments = null;
	
	private String to = null;
	private String cc = null;
	private String bcc = null;
	
	private String messageID = null;
	private String repliedMessageID = null;
	
	public Email() {
	}
	
	public Email from(final java.lang.String from) {
		this.from = from;
		return this;
	}
	public java.lang.String from() {
		return this.from;
	}
	public Email allowReply(final boolean allowReply) {
		this.allowReply = allowReply;
		return this;
	}
	public boolean allowReply() {
		return this.allowReply;
	}
	public Email replyToAddress(final java.lang.String replyToAddress) {
		this.replyToAddress = replyToAddress;
		return this;
	}
	public java.lang.String replyToAddress() {
		return this.replyToAddress;
	}
	
	public Email subject(final java.lang.String subject) {
		this.subject = subject;
		return this;
	}
	public java.lang.String subject() {
		return this.subject;
	}
	
	public Email body(final java.lang.String body) {
		this.body = body;
		return this;
	}
	public java.lang.String body() {
		return this.body;
	}
	
	public Email mimeType(final java.lang.String mimeType) {
		this.mimeType = mimeType;
		return this;
	}
	public java.lang.String mimeType() {
		return this.mimeType;
	}
	
	public Email attachment(final Email.Attachment attachment) {
		if (null == this.attachments && null != attachment) {
			this.attachments = new ArrayList<Email.Attachment>();
		}
		this.attachments.add(attachment);
		return this;
	}
	public List<Email.Attachment> attachments() {
		return this.attachments;
	}
	
	public Email to(final java.lang.String to) {
		this.to = to;
		return this;
	}
	public Email to(final java.lang.String[] to) {
		if (null != to) {
			this.to = java.lang.String.join(",", to);
		}
		return this;
	}
	public java.lang.String to() {
		return this.to;
	}
	
	public Email cc(final java.lang.String cc) {
		this.cc = cc;
		return this;
	}
	public Email cc(final java.lang.String[] cc) {
		if (null != cc) {
			this.cc = java.lang.String.join(",", cc);
		}
		return this;
	}
	public java.lang.String cc() {
		return this.cc;
	}
	
	public Email bcc(final java.lang.String bcc) {
		this.bcc = bcc;
		return this;
	}
	public Email bcc(final java.lang.String[] bcc) {
		if (null != bcc) {
			this.bcc = java.lang.String.join(",", bcc);
		}
		return this;
	}
	public java.lang.String bcc() {
		return this.bcc;
	}
	
	public void setMessageID(final String messageID) {
		this.messageID = messageID;
	}
	public String getMessageID() {
		return this.messageID;
	}

	public void setRepliedMessageID(final String messageID) {
		this.repliedMessageID = messageID;
	}
	public String getRepliedMessageID() {
		return this.repliedMessageID;
	}
}