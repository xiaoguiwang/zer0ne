package dto.mail.internet;

import javax.mail.MessagingException;
import javax.mail.Session;

// https://stackoverflow.com/questions/8297927/message-id-is-being-replaced-when-sending-mail-via-javamail
public class MimeMessage extends javax.mail.internet.MimeMessage {
	protected final String messageID;// = null;
	
	{
	  //messageID = null;
	}
	
	public MimeMessage(final Session session) {
		super(session);
		this.messageID = null;
	}
	public MimeMessage(final Session session, final String messageID) {
		super(session);
		this.messageID = messageID;
	}
	
	protected void updateMessageID() throws MessagingException {
		if (null == this.messageID) {
			super.updateMessageID();
		} else {
			super.setHeader("Message-ID", this.messageID);
		}
	}
}