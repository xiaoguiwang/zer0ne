package dto.mail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.util.Properties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Before
	public void before() {
		try {
			Properties.properties(new Properties(AdapterTest.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
  //final Adapter adapter;// = null;
	
	{
	  //adapter = null;
	}
	
	public AdapterTest() {
	  /*final java.lang.Class<?> clazz;// = null;
		try {
			clazz = Class.forName("dto.mail.Adapter");
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		try {
			this.adapter = (Adapter)clazz.newInstance();
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		}*/
	}
	
	@Test
	public void test_0001() {
		final StringWriter sw = new StringWriter()
			.append("Hello ${preferred-name},\r\n")
			.append("\r\n")
			.append("Thank you for choosing to sign-up at ${http-stub-for-urls}.")
			.append("Please find your sign-up token below, and sign-up link ")
			.append("that will take you back to our web site. You may continue to use the site without confirming your e-mail address but you will ")
			.append("not be able to request a password reset, or place an order without confirmation your e-mail address first\r\n")
			.append("\r\n")
			.append("${http-stub-for-urls}/hci/signing-up?token=${signing-up-token}\r\n")
			.append("\r\n")
			.append("Regards, IT From Blighty\r\n")
			.append("${signature-from-email-address}");
		;
		try {
			new dto.mail.Adapter(Adapter.Protocol.SMTP)
				.properties(new dro.util.Properties(AdapterTest.class))
				.send(new dto.mail.Email()
					.from("emailus@itfromblighty")
					.allowReply(true)
				  //.replyToAddress("emailus@itfromblighty")
					.to("alex@itfromblighty")
				  //.cc("..")
				  //.bcc("..")
					.subject("IT From Blighty - e-mail address confirmation e-mail")
					.body(dro.lang.String.resolve(sw.toString(), new dro.util.Properties(){
							private static final long serialVersionUID = 0L;
						}
						.property("preferred-name", "Alex")
						.property("signing-up-token", "WXYZ")
						.property("signature-from-email-address", "emailus@itfromblighty")
					))
				)
			;
		} catch (final FileNotFoundException e) {
		  //throw new RuntimeException(e);
		} catch (final IOException e) {
		  //throw new RuntimeException(e);
		} catch (final RuntimeException e) {
			throw e;
		}
	}
	
	@Test
	public void test_0002() {
		final StringWriter sw = new StringWriter()
			.append("<html>\n")
			.append("<head>\n")
			.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n")
			.append("</head>\n")
			.append("<body>\n")
			.append("Hello!<br/>\n")
			.append("<p>\n")
			.append("Thank you for choosing to sign-up at ${http-stub-for-urls}. Please find your confirmation token below, and confirmation link ")
			.append("that will take you back to our web site. You may continue to use the site without confirming your e-mail address but you will ")
			.append("not be able to request a password reset, or place an order without confirmation your e-mail address first") // <br/>
			.append("</p>\n")
			.append("<a href=\"${https-stub-for-urls}/hci/signing-up?token=${signing-up-token}\">${https-stub-for-urls}/hci/signing-up?token=${signing-up-token}</a><br/>\n")
			.append("<br/>\n")
			.append("Regards, IT From Blighty<br/>\n")
			.append("<a href=\"mailto:${signature-from-email-address}\">${signature-from-email-address}</a>\n")
			.append("</body>\n")
			.append("</html>\n")
		;
		try {
			new dto.mail.Adapter(Adapter.Protocol.SMTP)
				.properties(new dro.util.Properties(AdapterTest.class))
				.send(new dto.mail.Email()
					.from("emailus@itfromblighty")
					.allowReply(true)
				  //.replyToAddress("emailus@itfromblighty")
					.to("alex@itfromblighty")
				  //.cc("..")
				  //.bcc("..")
					.subject("IT From Blighty - e-mail address confirmation e-mail")
					.mimeType("text/html")//; charset=UTF-8")
					.body(dro.lang.String.resolve(sw.toString(), new dro.util.Properties(){
							private static final long serialVersionUID = 0L;
						}
						.property("to-user#preferred-name", "Alex")
						.property("signing-up-token", "WXYZ")
						.property("http-stub-for-urls", "http://uat.itfromblighty")
						.property("https-stub-for-urls", "https://uat.itfromblighty")
						.property("signature-from-email-address", "emailus@itfromblighty")
					))
				)
			;
		} catch (final FileNotFoundException e) {
		  //throw new RuntimeException(e);
		} catch (final IOException e) {
		  //throw new RuntimeException(e);
		} catch (final RuntimeException e) {
			throw e;
		}
	}
	
	@Test
	public void test_0003() {
		try {
			final Email[] emails = new dto.mail.Adapter(Adapter.Protocol.IMAP)
				.properties(new dro.util.Properties(AdapterTest.class))
				.receive()
			;
			if (null != emails) {
				emails.hashCode();
			}
		} catch (final FileNotFoundException e) {
		  //throw new RuntimeException(e);
		} catch (final IOException e) {
		  //throw new RuntimeException(e);
		} catch (final RuntimeException e) {
			throw e;
		}
	}
	
	@After
	public void after() {
	}
}