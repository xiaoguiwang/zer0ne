package dto.io.sms;

import java.io.StringWriter;

import org.junit.Test;

public class AdapterTest {
	@Test
	public void test_0001() throws InterruptedException {
		final StringWriter sw = new StringWriter()
			.append("Hi!\r\n")
			.append("\r\n")
			.append("Thank you for choosing to sign-up at ${http-stub-for-urls}.\r\n")
			.append("\r\n")
			.append("Please find your confirmation token below, and confirmation link ")
			.append("that will take you back to our web site.\r\n")
			.append("\r\n")
			.append("${https-stub-for-urls}/hci/signing-up?token=${signing-up-token}\r\n")
			.append("\r\n")
			.append("Thanks, IT From Blighty\r\n");
		;
		final dto.io.sms.SmsRequest req = new dto.io.sms.SmsRequest()
			.cellNumber("14039706104")
			.messageBody(dro.lang.String.resolve(sw.toString(), new dro.util.Properties(){
					private static final long serialVersionUID = 0L;
				}
				.property("to-user#preferred-name", "Alex");
				.property("signing-up-token", "WXYZ");
				.property("http-stub-for-urls", "http://uat.itfromblighty");
				.property("https-stub-for-urls", "https://uat.itfromblighty");
			))
			.reference("WXYZ")
			.billToDetail("textus@itfromblighty")
		;
		try {
			new dto.io.sms.MessageAdapter(new dro.util.Properties(AdapterTest.class) {
				private static final long serialVersionUID = 1L;
				{/*
					setProperty("dto.io.sms.MessageAdapter#"+Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
					setProperty("dto.io.sms.MessageAdapter#"+Context.PROVIDER_URL, "http-remoting://localhost:80");
					setProperty("dto.io.sms.MessageAdapter#"+Context.SECURITY_PRINCIPAL, "messenger");
					setProperty("dto.io.sms.MessageAdapter#"+Context.SECURITY_CREDENTIALS, "..");
				  //setProperty("dto.io.sms.MessageAdapter@producer#destination", "jms/queue/sendToSmsGateway");
					setProperty("dto.io.sms.MessageAdapter@producer#destination", "jms/Q"); // java:jboss/jms/Q
			  */}
			}).send(req);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
		Thread.sleep(5000L);
		new Object().hashCode();
	}
}

