package dto;

import java.io.BufferedReader;
import java.io.IOException;

import dto.util.Stream;

// This is dto.Adapter
public class Adapter extends dro.Adapter {
	public static class Return extends dro.Adapter.Return {
		public static final dto.Adapter.Return Adapter = (dto.Adapter.Return)null;
	}
	
	protected final java.util.Map<java.lang.Class<?>, Stream> stream = new java.util.HashMap<>();
	
	protected Stream stream(final java.lang.Class<?> c, final Stream s) {
		return this.stream.put(c, s);
	}
	public Stream stream(final java.lang.Class<?> c) {
		return this.stream.get(c);
	}
	
	public static java.lang.Class<?> getClass(final java.lang.Object o) {
	  /*final */java.lang.Class<?> c = null;
		if (true == o.getClass().isArray()) {
			c = java.lang.Object[].class;
		} else if (true == o instanceof java.util.List) {
			c = java.util.List.class;
		} else if (true == o instanceof java.util.Set) {
			c = java.util.Set.class;
		} else if (true == o instanceof java.util.Map) {
			c = java.util.Map.class;
		} else {
			c = o.getClass();
		}
		return c;
	}
	
	public static java.lang.Object getObjectFromStream(final BufferedReader br, final java.lang.Class<?> c) {
	  /*final */java.lang.Object o = null;
	  /*final */StringBuffer sb = null;
	  /*final */String __line = null;
		try {
			while ((__line = br.readLine()) != null) {
				if (null == sb) {
				  /*final StringBuffer */sb = new StringBuffer();
				}
				sb.append(__line);
			}
		} catch (final IOException e) {
		  //throw new RuntimeException();
			e.printStackTrace(System.err);
		}
		if (null != sb) {
		  /*final java.lang.Object */o = dto.Context.getAdapterFor(/**/dto.Adapter.class/**/).stream(sb.toString().getBytes(), c);
		}
		return o;
	}
	public static java.lang.String getStreamFromObject(final java.lang.Object o, final Stream.interface_Callback sic) {
	  /*final */byte[] ba = null;
		if (null != o) {
		  /*final byte[] */ba = dto.Context.getAdapterFor(/**/dto.Adapter.class/**/).stream(o, sic);
		}
		return null == ba ? null : new java.lang.String(ba);
	}
	public static java.lang.String getStreamFromObject(final java.lang.Object o) {
		return getStreamFromObject(o, (Stream.interface_Callback)null);
	}
	
	{
		this.stream(byte.class, new dto.data.Byte());
		this.stream(java.lang.Byte.class, new dto.data.Byte());
		this.stream(char.class, new dto.data.Character());
		this.stream(java.lang.Character.class, new dto.data.Character());
		this.stream(boolean.class, new dto.data.Boolean());
		this.stream(java.lang.Boolean.class, new dto.data.Boolean());
		this.stream(short.class, new dto.data.Short());
		this.stream(java.lang.Short.class, new dto.data.Short());
		this.stream(int.class, new dto.data.Integer());
		this.stream(java.lang.Integer.class, new dto.data.Integer());
		this.stream(long.class, new dto.data.Long());
		this.stream(java.lang.Long.class, new dto.data.Long());
		this.stream(float.class, new dto.data.Float());
		this.stream(java.lang.Float.class, new dto.data.Float());
		this.stream(double.class, new dto.data.Double());
		this.stream(java.lang.Double.class, new dto.data.Double());
		this.stream(java.math.BigInteger.class, new dto.data.BigInteger());
		this.stream(java.math.BigDecimal.class, new dto.data.BigDecimal());
		this.stream(java.lang.String.class, new dto.data.String());
		this.stream(java.util.Date.class, new dto.data.Date());
	  //this.stream(java.lang.Object[].class, new dto.lang.Array());
	  //this.stream(java.util.List.class, new dto.lang.List());
	  //this.stream(java.util.Set.class, new dto.lang.Set());
	  //this.stream(java.util.Map.class, new dto.lang.Map());
	}
	
	public Adapter() {
	  //super(); // Implicit super constructor..
	}
	
	public byte[] stream(final java.lang.Object o, final Stream.interface_Callback sic) {
	  /*final */byte[] ba = null;
		if (null != o) {
		  /*final byte[] */ba = this.stream(dto.lang.Class.getStreamClass(Adapter.getClass(o))).toStream(o, sic, Stream.Data);
		}
		return ba;
	}
	public byte[] stream(final java.lang.Object o) {
		return this.stream(o, (Stream.interface_Callback)null);
	}
	// From ServletTool
	public java.lang.Object stream(final byte[] stream, final java.lang.Class<?> c) {
	  /*final */java.lang.Object o = null;
		if (null != stream) {
		  /*final java.lang.Object */o = this.stream(dto.lang.Class.getStreamClass(c)).fromStream(stream, Stream.Data);
		}
		return o;
	}
  /*public java.lang.Object fromStream(final ByteArrayInputStream stream, final java.lang.Class<?> c) throws IOException {
	*//*final *//*java.lang.Object o = null;
		if (null != stream) {
		*//*final java.lang.Object *//*o = this.stream(dto.lang.Class.getStreamClass(c)).fromStream(stream, Stream.Data);
		}
		return o;
	}*/
}