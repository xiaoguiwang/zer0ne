package dto;

import java.io.FileNotFoundException;
import java.io.IOException;

import dro.util.Properties;

// This is dto.data.Context
public class Context extends dro.data.Context {
	public static class Lookup {
	}
	
  //private static final Context context;// = null;
	private static /*final */Adapter __adapter = null;
	private static /*final */Lookup lookup = new Lookup(); // TODO: link lookup with Context and adapter gets lookup from context
	
	static {
	  /*context = */new dto.Context();
	}
	
	public static Adapter getAdapterFor(final java.lang.Object o) {
		return __adapter;
	}
	
	public static Lookup getLookupFor(final java.lang.Object o) {
		return lookup;
	}
	public static void resetLookup() {
		lookup = new Lookup();
	}
	
	{
		try {
			if (null == Properties.properties()) {
				Properties.properties(new Properties(dto.Context.class));
			}
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		synchronized(this.getClass()) {
			if (null == __adapter) {
				__adapter = (dto.Adapter)dto.Adapter.newAdapter("dto.json.Adapter");
			}
		}
	}
}