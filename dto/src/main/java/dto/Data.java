package dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dro.util.Reentrant;

// https://www.mkyong.com/java/json-simple-example-read-and-write-json/
// https://stackoverflow.com/questions/3020094/how-should-i-escape-strings-in-json

//import org.apache.commons.codec.binary.Base64;
//final String b64 = o.toString(); byte[] ba = b64.getBytes(); ba = Data.base64.getInstance().decode(ba); String s = new String(ba);

public class Data extends dro.Data {
	private static class Return {
	  //public static final dro.lang.Null   .Return Null    = (dro.lang.Null   .Return)null;
		public static final dro.lang.Boolean.Return Boolean = (dro.lang.Boolean.Return)null;
		public static final dro.lang.String .Return String  = (dro.lang.String .Return)null;
		public static final dro.lang.Number .Return Number  = (dro.lang.Number .Return)null;
	  //public static final dro.lang.Object .Return Object  = (dro.lang.Object .Return)null;
		public static final dro.lang.Array  .Return Array   = (dro.lang.Array  .Return)null;
	  //public static final dro.lang.Class  .Return Class   = (dro.lang.Class  .Return)null;
		
		public static final dro.lang.Set .Return Set  = (dro.lang.Set .Return)null;
		public static final dro.lang.List.Return List = (dro.lang.List.Return)null;
		public static final dro.lang.Map .Return Map  = (dro.lang.Map .Return)null;
		
	  //public static final Data.Return Data = (Data.Return)null;
	}

	private static Reentrant<JSONParser> reentrant = new Reentrant<JSONParser>(){
		@Override
		public JSONParser newInstance() {
			return new JSONParser();
		}
	};
	
  /*private static Reentrant<Base64> base64 = new Reentrant<Base64>(){
		@Override
		public Base64 newInstance() {
			return new Base64();
		}
	};*/
	
  /*private static Object transform(final Null o, dro.lang.Null.Return r) {
		return (Object)null;
	}*/
	private static java.lang.Boolean transform(final java.lang.Boolean o, dro.lang.Boolean.Return r) {
		return (java.lang.Boolean)o;
	}
	private static java.lang.String transform(final java.lang.String o, dro.lang.String.Return r) {
		return (java.lang.String)o;
	}
  /*private static java.lang.Class<?> transform(final java.lang.String o, dro.lang.Class.Return r) {
		final Class<?> c;// = null;
		try {
			c = Class.forName(o);
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return c;
	}*/
  /*private static java.lang.Object transform(final java.lang.Class<?> c, dro.lang.Class.Return r) {
		final java.lang.Object o = null;// = null;
	*//*try {
			o = c.newInstance();
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		}*//*
		return o;
	}*/
	private static java.lang.String transform(final java.lang.Class<?> o, dro.lang.String.Return r) {
		return o.getCanonicalName();
	}
	private static java.lang.Number transform(final java.lang.Number o, dro.lang.Number.Return r) {
	  /*final */java.lang.Object n = null;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (o instanceof java.lang.Short) {
			n = (java.lang.Short)o;
		} else if (o instanceof java.lang.Integer) {
			n = (java.lang.Integer)o;
		} else if (o instanceof java.lang.Long) {
			n = (java.lang.Long)o;
		} else if (o instanceof java.lang.Float) {
			n = (java.lang.Float)o;
		} else if (o instanceof java.lang.Double) {
			n = (java.lang.Double)o;
	  /*} else if (o instanceof java.math.BigInteger) {
			n = (BigInteger)o;*/
	  /*} else if (o instanceof java.math.BigDecimal) {
			n = (BigDecimal)o;*/
		} else {
			throw new IllegalArgumentException();
		}	
		return (java.lang.Number)n;
	}
	
	private static java.lang.Object transformFromJson(final dro.meta.Data md, final java.lang.Object key, java.lang.Object o) {
		final java.lang.Object r;
		if (null == o) {
			r = null;//Data.transform((Null)o, Return.Null);
		} else if (o instanceof java.lang.Boolean) {
			r = Data.transform((java.lang.Boolean)o, Return.Boolean);
		} else if (o instanceof java.lang.String) {
			final java.lang.Object __o = o;//md.get(key);
			if (false == __o instanceof java.lang.Class<?>) {
				r = Data.transform((java.lang.String)o, Return.String);
			} else {
			  //r = Data.transform((java.lang.Class<?>)__o, Return.Class);
				r = (java.lang.Class<?>)__o;
			}
		} else if (o instanceof java.lang.Number) {
			r = Data.transform((java.lang.Number)o, Return.Number);
		} else if (o instanceof JSONArray) {
		  //final java.lang.Object __o = o;//md.get(key); // FIXME! this is never going to work when null==md
		  //if (__o == java.lang.Object[].class) {
				r = Data.transformFromJson(md, key, (JSONArray)o, Return.Array);
		  /*} else if (__o == java.util.Set.class) {
				r = Data.transformFromJson(md, key, (JSONArray)o, Return.Set);
			} else if (__o == java.util.List.class) {
				r = Data.transformFromJson(md, key, (JSONArray)o, Return.List);
		 	} else {
				throw new IllegalArgumentException();
			}*/
		} else if (o instanceof JSONObject) {
			if (null == md) {
				r = Data.transformFromJson(md, key, (JSONObject)o, Return.Map);
			} else {
				if (null == key) {
					r = Data.transformFromJson(md, key, (JSONObject)o, dro.Data.Return.Data);
				} else {
					final java.lang.Object __o = md.get(key);
					if (__o == java.util.Map.class) {
						r = Data.transformFromJson(md, key, (JSONObject)o, Return.Map);
					} else if (__o instanceof dro.Data) {
						final dro.meta.Data __md = (dro.meta.Data)__o;
						r = Data.transformFromJson(__md, key, (JSONObject)o, dro.Data.Return.Data);
					} else {
						throw new IllegalArgumentException();
					}
				}
			}
		} else if (o instanceof java.lang.Object) {
			throw new IllegalArgumentException();
		} else {
			throw new IllegalArgumentException();
		}
		return r;
	}
	
	private static java.lang.Object[] transformFromJson(final dro.meta.Data md, final java.lang.Object key, final JSONArray ja, dro.lang.Array.Return r) {
		final java.lang.Object[] oa = new java.lang.Object[ja.size()];
		for (int i = 0; i < ja.size(); i++) {
			oa[i] = Data.transformFromJson(md, key, ja.get(i));
		}
		return oa;
	}
	private static java.util.Set<java.lang.Object> transformFromJson(final dro.meta.Data md, final java.lang.Object key, final JSONArray ja, dro.lang.Set.Return r) {
		final java.util.Set<java.lang.Object> set = new LinkedHashSet<>();
		for (int i = 0; i < ja.size(); i++) {
			set.add(Data.transformFromJson(md, key, ja.get(i)));
		}
		return set;
	}
	private static java.util.List<java.lang.Object> transformFromJson(final dro.meta.Data md, final java.lang.Object key, final JSONArray ja, dro.lang.List.Return r) {
		final java.util.List<java.lang.Object> list = new ArrayList<>();
		for (int i = 0; i < ja.size(); i++) {
			list.add(Data.transformFromJson(md, key, ja.get(i)));
		}
		return list;
	}
	private static java.util.Map<java.lang.Object, java.lang.Object> transformFromJson(final dro.meta.Data md, final java.lang.Object key, final JSONObject jo, dro.lang.Map.Return r) {
		final java.util.Map<java.lang.Object, java.lang.Object> map = new LinkedHashMap<Object, Object>();
		final java.util.Set<?> keys = jo.keySet();
		for (final java.lang.Object __key: keys) {
			map.put(__key, Data.transformFromJson(md, __key, jo.get(__key)));
		}
		return map;
	}
	private static dro.Data transformFromJson(final dro.meta.Data md, final java.lang.Object key, final JSONObject jo, dro.Data.Return r) {
		final dro.Data __d;// = null;
		if (null == md) {
			__d = new dro.Data(); // new dro.meta.Data();
		} else {
			__d = new dro.Data(md); // new dro.meta.Data(md);
		}
		final java.util.Set<?> keys = jo.keySet();
		for (final java.lang.Object __key: keys) {
			__d.set(__key, Data.transformFromJson(md, __key, jo.get(__key)));
		}
		return __d;
	}
	
	public static dro.Data fromJson(final dro.meta.Data md, final java.lang.String json) {
		final JSONParser p = Data.reentrant.getInstance();
	  /*final */dro.Data d = null;
		try {
			final java.lang.Object o = (java.lang.Object)p.parse(json);
			if (null != o) {
				if (o instanceof java.lang.Boolean) {
				  //final java.lang.Boolean b = Data.transform(o, Return.Boolean);
					throw new IllegalArgumentException();
				} else if (o instanceof java.lang.String) {
				  //final java.lang.String s = Data.transform(o, Return.String);
				  //final java.lang.Class<?> c = Data.transform((Class<?>)o, Return.Class);
					throw new IllegalArgumentException();
				} else if (o instanceof java.lang.Number) {
				  //final java.lang.Number n = Data.transform(o, Return.Number);
					throw new IllegalArgumentException();
				} else if (o instanceof JSONArray) {
					if (false == Boolean.TRUE.booleanValue()) {
					  /*final Object[] oa = */Data.transformFromJson(md, (java.lang.Object)null, (JSONArray)o, Return.Array);
					  /*final Set<Object> set = */Data.transformFromJson(md, (java.lang.Object)null, (JSONArray)o, Return.Set);
					  /*final List<Object> list = */Data.transformFromJson(md, (java.lang.Object)null, (JSONArray)o, Return.List);
					}
					throw new IllegalArgumentException();
				} else if (o instanceof JSONObject) {
					if (null != md) {
						d = new dro.Data(md);
						final java.util.Map<java.lang.Object, java.lang.Object> map = Data.transformFromJson(md, (java.lang.Object)null, (JSONObject)o, Return.Map);
						final java.util.Set<java.lang.Object> keys = map.keySet();
						for (final Object key: keys) {
							d.set(key, map.get(key));
						}
					} else {
						d = Data.transformFromJson(md, (java.lang.Object)null, (JSONObject)o, dro.Data.Return.Data);
					}
				} else if (o instanceof java.lang.Object) {
					throw new IllegalArgumentException();
				} else {
					throw new IllegalArgumentException();
				}
			}
		} catch (final ParseException e) {
			throw new RuntimeException(e);
		}
		return d;
	}
	public static dro.Data fromJson(final java.lang.String json) {
		return Data.fromJson((dro.meta.Data)null, json);
	}
	
	private static java.lang.Object transformToJson(final dro.meta.Data md, final java.lang.Object key, final java.lang.Object o) {
	  /*final */java.lang.Object r = null;
		if (null == o) {
		  //r = null;//Data.transform((Null)o, Return.Null);
		} else if (o instanceof java.lang.Boolean) {
			r = Data.transform((java.lang.Boolean)o, Return.Boolean);
		} else if (o instanceof java.lang.String) {
			r = Data.transform((java.lang.String)o, Return.String);
		} else if (o instanceof java.lang.Number) {
			r = Data.transform((java.lang.Number)o, Return.Number);
		} else if (o instanceof java.lang.Object[]) {
		  //r = Data.transformToJson(md, key, (java.lang.Object[])o, Return.Array);
		} else if (o instanceof java.util.Set<?>) {
		  //r = Data.transformToJson(md, key, (java.util.Set<?>)o, Return.Set);
		} else if (o instanceof java.util.List<?>) {
		  //r = Data.transformToJson(md, key, (java.util.List<?>)o, Return.List);
		} else if (o instanceof java.util.Map<?, ?>) {
		  //r = Data.transformToJson(md, key, (java.util.Map<?, ?>)o, Return.Map);
		} else if (o instanceof java.lang.Class) {
			r = Data.transform((Class<?>)o, Return.String);
		} else if (o instanceof dro.Data) {
		  //r = Data.transformToJson(md, key, (dro.Data)o, Return.Data);
		} else if (o instanceof java.lang.Object) {
			throw new IllegalArgumentException();
		} else {
			throw new IllegalArgumentException();
		}
		return r;
	}
	
  /*@SuppressWarnings("unchecked")
	private static Array transformToJson(final dro.meta.Data md, final java.lang.Object key, final java.lang.Object[] oa, dto.lang.JSONArray.Return r) {
		final JSONArray ja = new JSONArray();
		for (int i = 0; i < oa.length; i++) {
			ja.add(Data.transformToJson(md, key, oa[i]));
		}
		return ja;
	}*/
  /*@SuppressWarnings("unchecked")
	private static JSONArray transformToJson(final dro.meta.Data md, final java.lang.Object key, final java.util.Set<?> set, dto.lang.JSONArray.Return r) {
		final JSONArray ja = new JSONArray();
		final Iterator<?> iter = set.iterator();
		while (true == iter.hasNext()) {
			ja.add(Data.transformToJson(md, key, iter.next()));
		}
		return ja;
	}*/
  /*@SuppressWarnings("unchecked")
	private static JSONArray transformToJson(final dro.meta.Data md, final java.lang.Object key, final java.util.List<?> list, dto.lang.JSONArray.Return r) {
		final JSONArray ja = new JSONArray();
		for (final java.lang.Object o: list) {
			ja.add(Data.transformToJson(md, key, o));
		}
		return ja;
	}*/
  /*@SuppressWarnings("unchecked")
	private static JSONObject transformToJson(final dro.meta.Data md, final java.lang.Object key, final java.util.Map<?, ?> map, dto.lang.JSONObject.Return r) {
		final JSONObject jo = new JSONObject();
		final java.util.Set<?> keys = map.keySet();
		for (final java.lang.Object __key: keys) {
			jo.put(__key, Data.transformToJson(md, key, map.get(__key)));
		}
		return jo;
	}/*
  /*@SuppressWarnings("unchecked")
	private static JSONObject transformToJson(final dro.meta.Data md, final java.lang.Object key, final dro.Data d, dto.lang.JSONObject.Return r) {
		final JSONObject jo = new JSONObject();
		final java.util.Set<?> keys = d.keys(dro.Data.Return.Set);
		for (final java.lang.Object __key: keys) {
			jo.put(__key, Data.transformToJson(md, key, d.get(__key)));
		}
		return jo;
	}*/
	
	@SuppressWarnings("unchecked")
	public static java.lang.String toJson(final dro.Data d) {
		final JSONObject jo = new JSONObject();
		final dro.meta.Data md = (dro.meta.Data)d.get("$dro$meta$Data");
		final java.util.Set<?> keys = d.keys(dro.Data.Return.Set);
		for (final java.lang.Object key: keys) {
			final java.lang.Object o = d.get(key);
			jo.put(key, Data.transformToJson(md, key, o));
		}
		return jo.toJSONString();
	}
	
	private dro.Data d;
	
	{
		d = null;
	}
	
	public Data() {
		this.d = new dro.Data();
	}
	public Data(final dro.Data d) {
		this.d = d;
	}
	
	public Data set(final java.lang.Object key, final java.lang.Object o) {
		this.d.set(key, o);
		return this;
	}
	public java.lang.Object get(final java.lang.Object key) {
		return this.d.get(key);
	}
	
	public java.util.Set<?> keys() {
		return null;//this.d.keys();
	}
	
	public java.lang.String toJson() {
		return Data.toJson(this);
	}
}