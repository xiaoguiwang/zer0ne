package dto.data;

import dto.lang.Primitive;

public class BigDecimal extends Primitive<java.math.BigDecimal> {
	public BigDecimal() {
		super(new java.math.BigDecimal("0.0"));
	}
	
	@Override
	public java.math.BigDecimal newOne(final java.lang.Object o) {
		return null == o ? null: new java.math.BigDecimal(o.toString());
	}
}