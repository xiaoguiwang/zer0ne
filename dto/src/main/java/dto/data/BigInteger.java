package dto.data;

import dto.lang.Primitive;

public class BigInteger extends Primitive<java.math.BigInteger> {
	public BigInteger() {
		super(new java.math.BigInteger("0"));
	}
	
	@Override
	public java.math.BigInteger newOne(final java.lang.Object o) {
		return null == o ? null: new java.math.BigInteger(o.toString());
	}
}