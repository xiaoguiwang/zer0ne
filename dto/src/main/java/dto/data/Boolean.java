package dto.data;

import dto.lang.Primitive;

public class Boolean extends Primitive<java.lang.Boolean> {
	public Boolean() {
		super(true);
	}
	
	@Override
	public java.lang.Boolean newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Boolean(o.toString());
	}
}