package dto.data;

import dto.lang.Primitive;

public class Byte extends Primitive<java.lang.Byte> {
	public Byte() {
		super((byte)0);
	}
	
	@Override
	public java.lang.Byte newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Byte(o.toString());
	}
	
	@Override
	protected java.lang.String toString(final java.lang.Object o) {
		return null == o || null == o.toString() ? null : new java.lang.String(new byte[]{o.toString().getBytes()[0]});
	}
	@Override
	protected java.lang.Object fromBytes(final byte[] ba) {
		return null == ba || 0 == ba.length ? null : new java.lang.Byte(ba[0]);
	}
}