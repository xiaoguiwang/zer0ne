package dto.data;

import dto.lang.Primitive;

public class Character extends Primitive<java.lang.Character> {
	public Character() {
		super((char)0);
	}
	
	@Override
	public java.lang.Character newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Character(o.toString().charAt(0));
	}
	
	@Override
	protected java.lang.String toString(final java.lang.Object o) {
		return null == o || null == o.toString() ? null : new java.lang.String(new char[]{o.toString().charAt(0)});
	}
	@Override
	protected java.lang.Object fromBytes(final byte[] ba) {
		return null == ba || 0 == ba.length ? null : new java.lang.Character(new java.lang.String(ba).charAt(0));
	}
}