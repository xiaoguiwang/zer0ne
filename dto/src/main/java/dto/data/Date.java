package dto.data;

import dto.lang.Primitive;

public class Date extends Primitive<java.util.Date> {
	public Date() {
		super(new java.util.Date(System.currentTimeMillis()));
	}
	
	@Override
	public java.util.Date newOne(final java.lang.Object o) {
		return null == o ? null : new java.util.Date(java.lang.Long.parseLong(o.toString()));
	}
	
	@Override
	protected java.lang.String toString(final java.lang.Object o) {
		return null == o ? null : new java.lang.Long(((java.util.Date)o).getTime()).toString();
	}
	@Override
	protected java.lang.Object fromBytes(final byte[] ba) {
		return null == ba || 0 == ba.length ? null : new java.util.Date(new java.lang.Long(new java.lang.String(ba)));
	}
}