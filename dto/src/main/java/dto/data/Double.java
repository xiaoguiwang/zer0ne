package dto.data;

import dto.lang.Primitive;

public class Double extends Primitive<java.lang.Double> {
	public Double() {
		super(0.0d);
	}
	
	@Override
	public java.lang.Double newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Double(o.toString());
	}
}