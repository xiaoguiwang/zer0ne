package dto.data;

import dto.lang.Primitive;

public class Float extends Primitive<java.lang.Float> {
	public Float() {
		super(0.0f);
	}
	
	@Override
	public java.lang.Float newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Float(o.toString());
	}
}