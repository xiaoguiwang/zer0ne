package dto.data;

import dto.lang.Primitive;
import dto.util.Stream;

public class Integer extends Primitive<java.lang.Integer> {
	public Integer() {
		super((int)0);
	}
	
	@Override
	public java.lang.Integer newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Integer(o.toString());
	}
	
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
	  /*final */byte[] ba = null;
	  	if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (type == Stream.Meta) {
		  /*final byte[] */ba = new java.lang.String("java.lang.Integer").getBytes();
		} else if (type == Stream.Data) {
		  /*final byte[] */ba = ((java.lang.Integer)o).toString().getBytes();
		} else {
			throw new IllegalStateException();
		}
		return ba;
	}
	
	@Override
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
	  /*final */java.lang.Object o = null;
	  	if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (type == Stream.Meta) {
			throw new IllegalStateException();
		} else if (type == Stream.Data) {
		  /*final java.lang.Object */o = new java.lang.Integer(new java.lang.String(stream));
		} else {
			throw new IllegalStateException();
		}
		return o;
	}
}