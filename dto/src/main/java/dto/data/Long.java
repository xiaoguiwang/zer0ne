package dto.data;

import dto.lang.Primitive;

public class Long extends Primitive<java.lang.Long> {
	public Long() {
		super(0L);
	}
	
	@Override
	public java.lang.Long newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Long(o.toString());
	}
}