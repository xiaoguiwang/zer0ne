package dto.data;

import dto.lang.Primitive;

public class Short extends Primitive<java.lang.Short> {
	public Short() {
		super((short)0);
	}
	
	@Override
	public java.lang.Short newOne(final java.lang.Object o) {
		return null == o ? null: new java.lang.Short(o.toString());
	}
}