package dto.data;

import java.io.ByteArrayInputStream;

import dto.lang.Primitive;
import dto.util.Stream;

public class String extends Primitive<java.lang.String> {
	protected final java.lang.String name = java.lang.String.class.getName();
	
	public String() {
		super("");
	}
	
	@Override
	public java.lang.String newOne(final java.lang.Object o) {
		return null == o ? null : o.toString();
	}
	
	@Override
	protected java.lang.String toString(final java.lang.Object o) {
		return null == o ? null : '"'+o.toString()+'"';
	}
	@Override
	protected java.lang.Object fromBytes(final byte[] ba) {
		return null == ba || 0 == ba.length ? null : new java.lang.String('"'+(new java.lang.String(ba))+'"');
	}
	
	@Override
	public java.lang.Object fromStream(final ByteArrayInputStream data, final Stream type) {
	  /*final */java.lang.Object o = null;
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (type == Stream.Meta) {
			throw new IllegalStateException();
		} else if (type == Stream.Data) {
		  /*final */boolean done = false;
		  /*final */StringBuffer sb = null;
		  /*final */int state = 0;
			while (null != data && false == done) {
				final int i = data.read();
				if (i < 0) break;
				final char c = (char)i;
				if (0 == state) {
					if (c == ' ') {
						state = 1;
					}
				}
				if (1 == state && c == '"') {
					state = 2;
				} else if (2 == state) {
					if (null == sb) {
						sb = new StringBuffer();
					}
					sb.append((char)i);
				} else if (3 == state && c == '"') {
					done = true;
				}
			}
		  /*final java.lang.Object */o = null == sb ? null : sb.toString();
		} else {
			throw new IllegalStateException();
		}
		return o;
	}
}