package dto.json;

// This is dto.json.Adapter
public class Adapter extends dto.Adapter {
	public static final String JSON = null;
	
	{
	  //super.stream(java.lang.Object[].class, new dto.lang.Array());
		super.stream(java.lang.Object[].class, new dto.json.Array());
	  //super.stream(java.util.List.class, new dto.lang.List());
		super.stream(java.util.List.class, new dto.json.List());
	  //super.stream(java.util.Set.class, new dto.lang.Set());
		super.stream(java.util.Set.class, new dto.json.Set());
	  //super.stream(java.util.Map.class, new dto.lang.Map());
		super.stream(java.util.Map.class, new dto.json.Map());
	}
	
	public Adapter() {
	  //super(); // Implicit super constructor..
	}
}