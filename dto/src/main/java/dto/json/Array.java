package dto.json;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dto.Adapter;
import dto.util.Source;
import dto.util.Stream;

public class Array extends dto.lang.Array {
	public Array() {
		super();
	}
	
	protected Array length(final int length) {
		return (Array)super.count(length);
	}
	
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
		return null == o ? null : ((JSONArray)this.transform(o, Source.Native)).toJSONString().getBytes();
	}
	@Override
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
	  /*final */java.lang.Object o = null;
		if (null != stream) {
			try {
			  /*final java.lang.Object */o = new JSONParser().parse(new java.lang.String(stream));
			} catch (final ParseException e) {
				// Silently ignore, for now..
				e.printStackTrace(System.err);
			}
		}
		return this.transform(o, Source.JSON);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Source source) {
	  /*final */java.lang.Object r = null;
		if (Source.JSON == source) {
		  /*final */JSONArray ja = (JSONArray)o;
		  /*final */java.lang.Object[] oa = null == ja ? null : new java.lang.Object[ja.size()];
			if (null != oa) {
				for (/*final */int i = 0; i < oa.length; i++) {
					final java.lang.Object __o = ja.get(i);
					oa[i] = null == __o ? null : dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(__o))).transform(__o, Source.JSON);
				}
			}
			r = (java.lang.Object)oa;
		} else if (Source.Native == source) {
		  /*final */java.lang.Object[] oa = (java.lang.Object[])o;
		  /*final */JSONArray ja = null == oa ? null : new JSONArray();
			if (null != oa) {
				for (/*final */java.lang.Object __o : oa) {
					ja.add(null == __o ? null : dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(__o))).transform(__o, Source.Native));
				}
			}
			r = (java.lang.Object)ja;
		}
		return r;
	}
}