package dto.json;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dto.Adapter;
import dto.util.Source;
import dto.util.Stream;

public class List extends dto.lang.List {
	public List() {
		super();
	}
	
	protected List length(final int size) {
		return (List)super.count(size);
	}
	
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream.interface_Callback sic, final Stream type) {
		return null == o ? null : ((JSONArray)this.transform(o, sic, Source.Native)).toJSONString().getBytes();
	}
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
		return this.toStream(o, (Stream.interface_Callback)null, type);
	}
	@Override
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
	  /*final */java.lang.Object o = null;
		if (null != stream) {
			try {
			  /*final java.lang.Object */o = new JSONParser().parse(new java.lang.String(stream));
			} catch (final ParseException e) {
				// Silently ignore, for now..
				e.printStackTrace(System.err);
			}
		}
		return this.transform(o, Source.JSON);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Stream.interface_Callback sic, final Source source) {
	  /*final */java.lang.Object r = null;
		if (Source.JSON == source) {
			final JSONArray ja = (JSONArray)o;
		  /*final */java.util.List<java.lang.Object> list = null == ja ? null : new dto.util.ArrayList<>();
			if (null != list) {
				for (/*final */int i = 0; i < ja.size(); i++) {
					final java.lang.Object __o = ja.get(i);
					list.add(null == __o ? null : dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(__o))).transform(__o, Source.JSON));
				}
			}
			r = (java.lang.Object)list;
		} else if (Source.Native == source) {
		  /*final */java.util.List<?> list = (java.util.List<?>)o;
		  /*final */JSONArray ja = null == list ? null : new JSONArray();
			if (null != list) {
				for (/*final */java.lang.Object __o: list) {
					ja.add(null == __o ? null : dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(__o))).transform(__o, sic, Source.Native));
				}
			}
			r = (java.lang.Object)ja;
		}
		return r;
	}
}