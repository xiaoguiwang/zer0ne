package dto.json;

import java.util.TreeSet;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dto.Adapter;
import dto.util.Source;
import dto.util.Stream;

public class Map extends dto.lang.Map {
	public Map() {
		super();
	}
	
	protected Map length(final int size) {
		return (Map)super.count(size);
	}
	
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream.interface_Callback sic, final Stream type) {
		return null == o ? null : ((JSONObject)this.transform(o, sic, Source.Native)).toJSONString().getBytes();
	}
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
		return this.toStream(o, (Stream.interface_Callback)null, type);
	}
	@Override
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
	  /*final */java.lang.Object o = null;
		if (null != stream) {
			try {
			  /*final java.lang.Object */o = new JSONParser().parse(new java.lang.String(stream));
			} catch (final ParseException e) {
				// Silently ignore, for now..
				e.printStackTrace(System.err);
			}
		}
		return this.transform(o, Source.JSON);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Stream.interface_Callback sic, final Source source) {
	  /*final */java.lang.Object r = null;
		if (Source.JSON == source) {
			final JSONObject jo = (JSONObject)o;
		  /*final */java.util.Map<java.lang.Object, java.lang.Object> map = null == jo ? null : new dto.util.HashMap<>();
			if (null != map) { // TODO: order the key/values as per Source.Native below
				for (final java.util.Map.Entry<java.lang.Object, java.lang.Object> e: (java.util.Set<java.util.Map.Entry<java.lang.Object, java.lang.Object>>)jo.entrySet()) {
					map.put(
						dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(e.getKey()))).transform(e.getKey(), Source.JSON),
						dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(e.getValue()))).transform(e.getValue(), Source.JSON)
					);
				}
			}
			r = (java.lang.Object)map;
		} else if (Source.Native == source) {
		  /*final */java.util.Map<java.lang.Object, java.lang.Object> map = (java.util.Map<java.lang.Object, java.lang.Object>)o;
		  /*final */JSONObject jo = null;
			if (null != map) {
			  /*final JSONObject */jo = new JSONObject();
				if (null != sic) {
					sic.configure("phase", "start");
				}
				for (final Object key: new TreeSet<>(map.keySet())) {
					final Object value = map.get(key);
					if (null == sic || true == (Boolean)sic.process(key)) {
						final java.lang.Object __key = null == sic ? key : sic.translate(key);
						final java.lang.Object __value = null == sic ? value : sic.translate(value);
						jo.put(
							dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(key))).transform(__key, sic, Source.Native),
							dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(value))).transform(__value, sic, Source.Native)
						);
					}
				}
				if (null != sic) {
					sic.configure("phase", "finish");
				}
			}
			r = (java.lang.Object)jo;
		}
		return r;
	}
}