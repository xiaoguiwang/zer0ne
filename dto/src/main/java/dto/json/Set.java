package dto.json;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dto.Adapter;
import dto.util.Source;
import dto.util.Stream;

// Optionally Ordered (meaning fifo/lilo)
// Never Indexed
// Always Unique (by Ref#Id)
public class Set extends dto.lang.Set {
	public Set() {
		super();
	}
	
	protected Set length(final int size) {
		return (Set)super.count(size);
	}
	
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream.interface_Callback sic, final Stream type) {
		return null == o ? null : ((JSONArray)this.transform(o, sic, Source.Native)).toJSONString().getBytes();
	}
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
		return this.toStream(o, (Stream.interface_Callback)null, type);
	}
	@Override
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
	  /*final */java.lang.Object o = null;
		if (null != stream) {
			try {
			  /*final java.lang.Object */o = new JSONParser().parse(new java.lang.String(stream));
			} catch (final ParseException e) {
				// Silently ignore, for now..
				e.printStackTrace(System.err);
			}
		}
		return this.transform(o, Source.JSON);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Source source) {
	  /*final */java.lang.Object r = null;
		if (Source.JSON == source) {
			final JSONArray ja = (JSONArray)o;
		  /*final */java.util.Set<java.lang.Object> set = null == ja ? null : new dto.util.HashSet<>();
			if (null != set) {
				for (/*final */int i = 0; i < ja.size(); i++) {
					final java.lang.Object __o = ja.get(i);
					set.add(null == __o ? null : dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(__o))).transform(__o, Source.JSON));
				}
			}
			r = (java.lang.Object)set;
		} else if (Source.Native == source) {
		  /*final */java.util.Set<?> set = (java.util.Set<?>)o;
		  /*final */JSONArray ja = null == set ? null : new JSONArray();
			if (null != set) {
				for (/*final */java.lang.Object __o: set) {
					ja.add(null == __o ? null : dto.Context.getAdapterFor(this).stream(dto.lang.Class.getStreamClass(Adapter.getClass(__o))).transform(__o, Source.Native));
				}
			}
			r = (java.lang.Object)ja;
		}
		return r;
	}
}