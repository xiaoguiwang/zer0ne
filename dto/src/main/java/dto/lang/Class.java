package dto.lang;

public class Class {
	public static java.lang.Class<?> getStreamClass(final java.lang.Object o) {
	  /*final */java.lang.Class<?> __c = o.getClass();
		if (true == __c.isArray()) {
			__c = java.lang.Object[].class;
		} else if (true == o instanceof java.util.List) {
			__c = java.util.List.class;
		} else if (true == o instanceof java.util.Set) {
			__c = java.util.Set.class;
		} else if (true == o instanceof java.util.Map) {
			__c = java.util.Map.class;
		}
		return __c;
	}
	public static java.lang.Class<?> getStreamClass(final java.lang.Class<?> c) {
	  /*final */java.lang.Class<?> __c = c;
		if (true == c.isArray()) {
			__c = java.lang.Object[].class;
		} else if (c == dto.util.ArrayList.class) { /*FIXME: make sure to register each known class of java.util.List*/
			__c = java.util.List.class;
		} else if (c == dto.util.HashSet.class) { /*FIXME: make sure to register each known class of java.util.Set*/
			__c = java.util.Set.class;
		} else if (c == dto.util.HashMap.class) { /*FIXME: make sure to register each known class of java.util.Map*/
			__c = java.util.Map.class;
		}
		return __c;
	}
	
	public static java.lang.String getTypeReference(final java.lang.Object o) {
		final java.lang.String type_ref;// = null;
		if (true == o instanceof java.lang.Object[]) {
			type_ref = java.lang.Object[].class.getName();
		} else if (true == o instanceof java.util.Set) {
			type_ref = dto.util.HashSet.class.getName();
		} else if (true == o instanceof java.util.List) {
			type_ref = dto.util.ArrayList.class.getName();
		} else if (true == o instanceof java.util.Map) {
			type_ref = dto.util.HashMap.class.getName();
		} else if (true == o instanceof java.lang.Class) {
			type_ref = ((java.lang.Class<?>)o).getName();
		} else {
			type_ref = o.getClass().getName();
		}
		return type_ref;
	}
}