package dto.lang;

import dto.util.Stream;

public class Indexed extends Stream {
	public static final java.lang.String ID = "ID";
	public static final java.lang.String TYPE = "TYPE";
	public static final java.lang.String INDEX = "INDEX";
	public static final java.lang.String TYPE_REF = "TYPE_REF";
	public static final java.lang.String REF_ID = "REF_ID";
	public static final java.lang.String INDEXED = "INDEXED";
	public static final java.lang.String COUNT = "COUNT";
	
	static class Reference {
		public final java.lang.Class<?> c;
		public final java.lang.Object o;
		public /*final */java.lang.Integer id;
		public final java.lang.Integer index;
		public /*final */java.lang.Class<?> cp;
		public /*final */java.lang.Object p;
		public final java.lang.Integer i;
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> cp, final java.lang.Object p) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.cp = cp;
			this.p = p;
			this.i = null;// null != p ? dao.Adapter.access(dto.lang.Class.getStreamClass(p)).insert(p) : null;
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> cp, final java.lang.Object p, final java.lang.Integer rowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.cp = cp;
			this.p = p;
			this.i = rowId;
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> cp, final java.lang.Integer rowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.cp = cp;
		  //this.p = null != cp && null != rowId ? dto.Adapter.stream(dto.lang.Class.getStreamClass(cp)).select(rowId) : null;
			this.i = rowId;
		}
		public Reference delete() {
		  //dto.Adapter.access(dto.lang.Class.getStreamClass(this.cp)).delete(this.i);
			return this;
		}
	}
	
	boolean indexed;// = false;
	int count;// = -1;
	
	{
	  //indexed = false;
	  //count = 0;
	}
	
	protected Indexed(final java.lang.Object o) {
		super(o);
	}
	
	protected Indexed indexed(final boolean indexed) {
		this.indexed = indexed;
		return this;
	}
	protected Indexed count(final int count) {
		this.count = count;
		return this;
	}
}