package dto.lang;

import dto.util.Stream;

public class Keyed extends Stream {
	public static final java.lang.String ID = "ID";
	public static final java.lang.String TYPE = "TYPE";
	public static final java.lang.String INDEX = "INDEX";
	public static final java.lang.String TYPE_KEY = "TYPE_KEY";
	public static final java.lang.String KEY_ID = "KEY_ID";
	public static final java.lang.String TYPE_VAL = "TYPE_VAL";
	public static final java.lang.String VAL_ID = "VAL_ID";
	public static final java.lang.String INDEXED = "INDEXED";
	public static final java.lang.String COUNT = "COUNT";
	
	static class Reference {
		public final java.lang.Class<?> c;
		public final java.lang.Object o;
		public /*final */java.lang.Integer id;
		public final java.lang.Integer index;
		public final java.lang.Class<?> ck;
		public final java.lang.Object ok;
		public final java.lang.Integer ik;
		public /*final */java.lang.Class<?> cv;
		public /*final */java.lang.Object ov;
		public final java.lang.Integer iv;
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Object ok, final java.lang.Class<?> cv, final java.lang.Object ov) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = ok;
			this.ik = null;// null != ck && null != ok ? dto.Adapter.stream(ck).insert(ok) : null;
			this.cv = cv;
			this.ov = ov;
			this.iv = null;// null != ov ? dto.Adapter.stream(dto.lang.Class.getStreamClass(ov)).insert(ov) : null;
		}
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Object ok, final java.lang.Integer kRowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = ok;
			this.ik = kRowId;
			this.cv = null;
			this.ov = null;
			this.iv = null;
		}
	  /*private Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Integer kRowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = null != ck && null != kRowId ? dao.Adapter.access(ck).select(kRowId) : null;
			this.ik = kRowId;
			this.cv = null;
			this.ov = null;
			this.iv = null;
		}*/
		public Reference(final java.lang.Class<?> c, final java.lang.Object o, final java.lang.Integer index, final java.lang.Class<?> ck, final java.lang.Integer kRowId, final java.lang.Class<?> cv, final java.lang.Integer vRowId) {
			this.c = c;
			this.o = o;
			this.id = null;
			this.index = index;
			this.ck = ck;
			this.ok = null;// null != ck && null != kRowId ? dto.Adapter.stream(ck).select(kRowId) : null;
			this.ik = kRowId;
			this.cv = cv;
			this.ov = null;// null != cv && null != vRowId ? dao.Adapter.access(dto.lang.Class.getStreamClass(cv)).select(vRowId) : null;
			this.iv = vRowId;
		}
		public Reference delete() {
		  //dto.Adapter.stream(this.ck).delete(this.ik);
		  //dto.Adapter.stream(this.cv).delete(this.iv); //<==4
			return this;
		}
	}
	
	boolean indexed;// = false;
	int count;// = -1;
	
	{
	  //indexed = false;
	  //count = 0;
	}
	
	protected Keyed(final java.lang.Object o) {
		super(o);
	}
	
	protected Keyed indexed(final boolean indexed) {
		this.indexed = indexed;
		return this;
	}
	protected Keyed count(final int count) {
		this.count = count;
		return this;
	}
}