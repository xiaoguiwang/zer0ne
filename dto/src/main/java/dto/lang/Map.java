package dto.lang;

import dto.util.Source;
import dto.util.Stream;

public class Map extends Keyed {
	public Map() {
		super(new dto.util.HashMap<java.lang.Object, java.lang.Object>(dro.lang.Break.Recursion));
	}
	
	protected Map length(final int size) {
		return (Map)super.count(size);
	}
	
  /*@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
	*//*final *//*byte[] stream = null;
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (Stream.Meta == type) {
			final StringBuffer sb = new StringBuffer("java.util.Map{ ");
		*//*final *//*int i = 0;
			for (final java.util.Map.Entry<?,?> p: ((java.util.Map<?,?>)o).entrySet()) {
				if (i > 0) {
					sb.append(", ");
				}
				sb.append(new java.lang.String(new dto.Adapter().stream(p.getKey())));
				sb.append(": ");
				sb.append(new java.lang.String(new dto.Adapter().stream(p.getValue())));
				i++;
			}
			sb.append(" }");
			stream = sb.toString().getBytes();
		} else if (Stream.Data == type) {
			if (null == o) {
			*//*final byte[] *//*stream = new String("null").getBytes();
			} else {
				final java.util.Map<?, ?> map = (java.util.Map<?, ?>)o;
				final StringBuffer sb = new StringBuffer("{ ");
			*//*final *//*int i = 0;
				for (final java.util.Map.Entry<?, ?> entry: map.entrySet()) {
					if (i > 0) {
						sb.append(", ");
					}
					sb.append(new String(new Adapter().stream(entry.getKey())));
					sb.append(": ");
					sb.append(new String(new Adapter().stream(entry.getValue())));
					i++;
				}
				sb.append(" }");
			*//*final byte[] *//*stream = sb.toString().getBytes();
			}
		} else {
			throw new IllegalStateException();
		}
		return stream;
	}*/
	
  /*@Override
	public java.lang.Object fromStream(final ByteArrayInputStream meta, final ByteArrayInputStream data) throws IOException {
	*//*final *//*java.util.Map<java.lang.Object, java.lang.Object> map = null;
	*//*final *//*java.lang.Class<?> ck = null, cv = null;
	*//*final *//*java.lang.Object ok = null, ov = null;
		if (null != data) {
		*//*final *//*boolean done = false;
		*//*final *//*StringBuffer sb = null;
			int metaState = null == meta ? -1 : 0, dataState = null == meta ? 0 : -1;
			int i = 0, __i = 0;
			while (false == done) {
				boolean flag = true;
				if (null != meta && 0 <= metaState) {
					i = meta.read();
					if (i < 0) break;
					final char c = (char)i;
					if (0 == metaState) {
						if (c == ' ') {
							flag = false;
						} else if (c != ' ') {
							metaState = 1;
						}
					}
					if (1 == metaState) {
						if (c == '{') {
							flag = false;
							if (true == sb.toString().equals("java.util.Map")) {
								map = new java.util.HashMap<java.lang.Object, java.lang.Object>();
							}
							sb.setLength(0);
							metaState = -1; dataState = 0;
						}
					}
					if (2 == metaState) {
						if (c == ' ') {
							flag = false;
						} else {
							metaState = 3;
						}
					}
					if (3 == metaState) {
						if (c == ':') {
							flag = false;
							try {
							*//*final java.lang.Class<?> *//*ck = java.lang.Class.forName(sb.toString());
							} catch (final ClassNotFoundException e) {
								throw new RuntimeException(e);
							}
							sb.setLength(0);
							metaState = -1; dataState = 2;
						} else if (c == '}') {
							flag = false;
							done = true;
						}
					}
					if (4 == metaState) {
						if (c == ' ') {
							meta.mark(0);
							flag = false;
						} else {
							metaState = 5;
						}
					}
					if (5 == metaState) {
						if (c == ' ' || c == '[' || c == '{') {
							meta.reset();
							flag = false;
							try {
							*//*final java.lang.Class<?> *//*cv = java.lang.Class.forName(sb.toString());
							} catch (final ClassNotFoundException e) {
								throw new RuntimeException(e);
							}
							sb.setLength(0);
							metaState = -1; dataState = 3;
						}
					}
					if (6 == metaState) {
						if (c == ' ') {
							flag = false;
						} else {
							metaState = 7;
						}
					}
					if (7 == metaState) {
						if (c == ' ') {
							flag = false;
						} else if (c == ',') {
							flag = false;
							metaState = 2;
						} else if (c == ']' || c == '}') {
							flag = false;
							metaState = -1; dataState = 4;
						}
					}
					if (true == flag) {
						if (null == sb) {
							sb = new StringBuffer();
						}
						sb.append((char)i);
					}
				} else if (0 <= dataState) {
					__i = data.read();
					if (__i < 0) break;
					final char __c = (char)__i;
					if (0 == dataState) {
						if (__c != ' ') {
							dataState = 1;
						}
					}
					if (1 == dataState) {
						if (__c == '{') {
							if (null == map) {
								map = new java.util.HashMap<java.lang.Object, java.lang.Object>();
							}
							if (null != meta) {
								metaState = 2; dataState = -1;
							} else {
								dataState = 2;
							}
						}
					}
					if (2 == dataState) {
						if (__c == ' ' || __c == ',') {
							data.mark(0);
						} else if ((null == meta && null == ck) && (__c == '[' || __c == '{')) {
							data.reset();
							if (__c == '[') {
								ck = dto.util.ArrayList.class;
							} else if (__c == '{') {
								ck = dto.util.HashMap.class;
							}
						} else {
							data.reset();
							ok = new dto.Adapter().fromStream(data, ck);
							if (null != meta) {
								metaState = 4; dataState = -1;
							} else {
								dataState = 3;
							}
						}
					}
					if (3 == dataState) {
						if (__c == ':' || __c == ' ') {
							data.mark(0);
						} else if ((null == meta && null == cv) && (__c == '[' || __c == '{')) {
							data.reset();
							if (__c == '[') {
								cv = dto.util.ArrayList.class;
							} else if (__c == '{') {
								cv = dto.util.HashMap.class;
							}
						} else {
							data.reset();
							ov = new dto.Adapter().fromStream(meta, cv);
							map.put(ok, ov);
							if (null != meta) {
								metaState = 6; dataState = -1;
							} else {
								dataState = 4;
							}
						}
					}
					if (4 == dataState) {
						if (__c == '}') {
							done = true;
						}
					}
				}
			}
		}
		return map;
	}*/
	
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Stream.interface_Callback sic, final Source source) {
		throw new IllegalStateException();
	}
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Source source) {
		return this.transform(o, (Stream.interface_Callback)null, source);
	}
}