package dto.lang;

import dto.util.Source;
import dto.util.Stream;

public abstract class Primitive<T> extends Stream {
	protected final java.lang.String name = null;
	
	public Primitive(final T o) {
		super(o);
	}
	
	public abstract java.lang.Object newOne(final java.lang.Object o);
	
	@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
	  /*final */byte[] ba = null;
	  	if (o.getClass() != super.o.getClass()) {
	  		throw new IllegalArgumentException();
		} else if (type == Stream.Meta) {
		  /*final byte[] */ba = this.name.getBytes();
		} else if (type == Stream.Data) {
			if (null != o) {
				final java.lang.String string = o.toString();
				if (null != string) {
				  /*final byte[] */ba = string.getBytes();
				}
			}
		} else {
			throw new IllegalStateException();
		}
		return ba;
	}
	protected java.lang.String toString(final java.lang.Object o) {
		return null == o ? null : o.toString();
	}
	
	@Override
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
	  /*final */java.lang.Object o = null;
	  	if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (type == Stream.Meta) {
			throw new IllegalStateException();
		} else if (type == Stream.Data) {
		  /*final java.lang.Object */o = this.fromBytes(stream);
		} else {
			throw new IllegalStateException();
		}
		return o;
	}
	@Override
	protected java.lang.Object fromBytes(final byte[] ba) {
		return this.newOne(new java.lang.String(ba));
	}
	
  /*@Override
	public java.lang.Object fromStream(final ByteArrayInputStream data, final Stream type) {
	*//*final *//*java.lang.Object o = null;
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (type == Stream.Meta) {
			throw new IllegalStateException();
		} else if (type == Stream.Data) {
		*//*final *//*boolean done = false;
		*//*final *//*StringBuffer sb = null;
		*//*final *//*int state = 0;
			while (null != data && false == done) {
				final int i = data.read();
				if (i < 0) break;
				final char c = (char)i;
				if (0 == state) {
					if (c != ' ') {
						state = 1;
					}
				}
				if (1 == state) {
					if (c == ',' || c == ' ') {
						done = true;
					} else if (c != ' ') {
						if (null == sb) {
							sb = new StringBuffer();
						}
						sb.append((char)i);
					}
				}
			}
		*//*final java.lang.Object *//*o = null == sb ? null : this.newOne(sb.toString());
		} else {
			throw new IllegalStateException();
		}
		return o;
	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public T transform(final java.lang.Object o, final Source source) {
		return (T)o;
	}
	@SuppressWarnings("unchecked")
	@Override
	public T transform(final java.lang.Object o, final Stream.interface_Callback sic, final Source source) {
		return (T)o;
	}
}