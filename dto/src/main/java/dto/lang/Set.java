package dto.lang;

import dto.util.Source;
import dto.util.Stream;

// Optionally Ordered (meaning fifo/lilo)
// Never Indexed
// Always Unique (by Ref#Id)
public class Set extends Indexed {
	public Set() {
		super(new dto.util.HashSet<java.lang.Object>(dro.lang.Break.Recursion));
	}
	
	protected Set length(final int size) {
		return (Set)super.count(size);
	}
	
  /*@Override
	public byte[] toStream(final java.lang.Object o, final Stream type) {
	*//*final *//*byte[] stream = null;
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (Stream.Meta == type) {
			final StringBuffer sb = new StringBuffer("java.util.Set[ ");
		*//*final *//*int i = 0;
			for (final java.lang.Object p: (java.util.Set<?>)o) {
				if (i > 0) {
					sb.append(", ");
				}
				sb.append(new java.lang.String(new dto.Adapter().stream(p)));
				i++;
			}
			sb.append(" ]");
			stream = sb.toString().getBytes();
		} else if (Stream.Data == type) {
			if (null == o) {
			*//*final byte[] *//*stream = new String("null").getBytes();
			} else {
				final java.util.Set<?> set = (java.util.Set<?>)o;
				final StringBuffer sb = new StringBuffer("[ ");
			*//*final *//*int i = 0;
				for (final java.lang.Object p: set) {
					if (i > 0) {
						sb.append(", ");
					}
					sb.append(new String(new Adapter().stream(p)));
					i++;
				}
				sb.append(" ]");
			*//*final byte[] *//*stream = sb.toString().getBytes();
			}
		} else {
			throw new IllegalStateException();
		}
		return stream;
	}*/
	
  /*@Override
	public java.lang.Object fromStream(final ByteArrayInputStream meta, final ByteArrayInputStream data) throws IOException {
	*//*final *//*java.util.Set<java.lang.Object> set = null;
	*//*final *//*java.lang.Class<?> cp = null;
	*//*final *//*java.lang.Object p = null;
		if (null != data) {
		*//*final *//*boolean done = false;
		*//*final *//*StringBuffer sb = null;
			int metaState = null == meta ? -1 : 0, dataState = null == meta ? 0 : -1;
			int i = 0, __i = 0;
			if (null != meta) meta.mark(0); data.mark(0);
			while (false == done) {
				boolean flag = true;
				if (null != meta && 0 <= metaState) {
					i = meta.read();
					if (i < 0) break;
					final char c = (char)i;
					if (0 == metaState) {
						if (c == ' ') {
							flag = false;
						} else {
							metaState = 1;
						}
					}
					if (1 == metaState) {
						if (c == '[') {
							flag = false;
							if (true == sb.toString().equals("java.util.Set")) {
								set = new java.util.HashSet<java.lang.Object>();
							}
							sb.setLength(0);
							metaState = -1; dataState = 0;
						}
					}
					if (2 == metaState) {
						if (c == ' ') {
							flag = false;
						} else {
							metaState = 3;
						}
					}
					if (3 == metaState) {
						if (c == ' ' || c == ',' || c == '[' || c == '{') {
							if (c == '[' || c == '{') {
								meta.reset();
							}
							flag = false;
							try {
							*//*final java.lang.Class<?> *//*cp = java.lang.Class.forName(sb.toString());
							} catch (final ClassNotFoundException e) {
								throw new RuntimeException(e);
							}
							sb.setLength(0);
							metaState = -1; dataState = 2;
						} else if (c == ']') {
							flag = false;
							metaState = -1; dataState = 3;
						}
					}
					if (true == flag) {
						if (null == sb) {
							sb = new StringBuffer();
						}
						sb.append((char)i);
					}
				} else if (0 <= dataState) {
					__i = data.read();
					if (__i < 0) break;
					final char __c = (char)__i;
					if (0 == dataState) {
						if (__c != ' ') {
							dataState = 1;
						}
					}
					if (1 == dataState) {
						if (__c == '[') {
							if (null == set) {
								set = new java.util.HashSet<java.lang.Object>();
							}
							if (null != meta) {
								metaState = 2; dataState = -1;
							} else {
								dataState = 2;
							}
						}
					}
					if (2 == dataState) {
						if (__c == ' ' || __c == ',') {
							data.mark(0);
						} else if ((null == meta && null == cp) && (__c == '[' || __c == '{')) {
							data.reset();
							if (__c == '[') {
								cp = dto.util.ArrayList.class;
							} else if (__c == '{') {
								cp = dto.util.HashMap.class;
							}
						} else {
							data.reset();
							p = new dto.Adapter().fromStream(data, cp);
							set.add(p);
							if (null != meta) {
								metaState = 2; dataState = -1;
							} else {
								dataState = 3;
							}
						}
					}
					if (3 == dataState) {
						if (__c == ']') {
							done = true;
						}
					}
				}
			}
		}
		return set;
	}*/
	
	
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Stream.interface_Callback sic, final Source source) {
		throw new IllegalStateException();
	}
	@Override
	public java.lang.Object transform(final java.lang.Object o, final Source source) {
		return this.transform(o, (Stream.interface_Callback)null, source);
	}
}