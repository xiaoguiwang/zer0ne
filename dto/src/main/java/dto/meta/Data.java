package dto.meta;

public class Data extends dto.Data {
	public static dro.meta.Data fromJson(final dro.meta.Data md, final java.lang.String json) {
		return (dro.meta.Data)dto.Data.fromJson(md, json);
	}
	public static dro.meta.Data fromJson(final java.lang.String json) {
		return (dro.meta.Data)Data.fromJson((dro.meta.Data)null, json);
	}
	public static java.lang.String toJson(final dro.meta.Data md) {
		return dto.Data.toJson(md);
	}
}