package dto.util;

public class ArrayList<E> extends java.util.ArrayList<E> implements dto.util.interface_List<E>, dro.lang.Id {
	private static final long serialVersionUID = 0L;
	
	public final dro.util.Id id;
	{
		id = new dro.util.Id(this);
	}
	@Override
	public java.lang.Object id() {
		return id;
	}
	
	public ArrayList(final dro.lang.Break.Recursion br) {
		super();
	}
	public ArrayList() {
		super();
	}
	
	@Override
	public boolean add(final E o, final dro.lang.Break.Recursion br) {
	  /*final */boolean added = false;
		added = super.add(o);
		return added;
	}
	
	@Override
	public E set(final int index, final E o, final dro.lang.Break.Recursion br) {
	  /*final */E e = null;
		e = super.set(index, o);
		return e;
	}
	
	@Override
	public E get(final int index, final dro.lang.Break.Recursion br) {
	  /*final */E e = null;
		e = super.get(index);
		return e;
	}
	
	@Override
	public boolean remove(final java.lang.Object o, final dro.lang.Break.Recursion br) {
	  /*final */boolean removed = false;
		removed = super.remove(o);
		return removed;
	}
	@Override
	public E remove(final int index, final dro.lang.Break.Recursion br) {
	  /*final */E o = null;
		o = super.remove(index);
		return o;
	}
}