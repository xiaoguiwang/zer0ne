package dto.util;

public class Date {
	public static boolean inDateRange(final java.util.Date date_from, final Long __from, final java.util.Date date_to, final Long __to) {
	  /*final */boolean inDateRange = false;
	  //final Long __from = (Long)/*service_*/history_line.get("date-from");
	  /*final */java.util.Date from = null;
	  /*if (0 < __from.length()) {
		  /*try {
				from = sdf.parse(
					__from.replace("T", " ").replace("Z", "").substring(0, 19)
				);
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}*/
		from = new java.util.Date(__from.longValue());
	  //final String __to = (String)/*service_*/history_line.get("date-to");
	  //final Long __to = (Long)/*service_*/history_line.get("date-to");
	  /*final */java.util.Date to = null;
	  /*if (0 < __to.length()) {
			try {
				to = sdf.parse(
					__to.replace("T", " ").replace("Z", "").substring(0, 19)
				);
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}*/
		to = new java.util.Date(__to.longValue());
		if ((null == from || null == date_from || (true == from.after(date_from) || true == from.equals(date_from)))
			 &&
			(null == to || null == date_to || (true == to.before(date_to) || true == to.equals(date_to)))
		) {
			inDateRange = true;
		}
		return inDateRange;
	}
}