package dto.util;

public class HashSet<E> extends java.util.HashSet<E> implements dto.util.interface_Set<E> {
	private static final long serialVersionUID = 0L;
	
	public final dro.util.Id id;
	{
		id = new dro.util.Id(this);
	}
	@Override
	public java.lang.Object id() {
		return id;
	}
	
	public HashSet(final dro.lang.Break.Recursion br) {
		super();
	}
	public HashSet() {
		super();
	}
	
	@Override
	public boolean add(final E o, final dro.lang.Break.Recursion br) {
	  /*final */boolean added = false;
		added = super.add(o);
		return added;
	}
	
	@Override
	public boolean remove(final java.lang.Object o, final dro.lang.Break.Recursion br) {
	  /*final */boolean removed = false;
		removed = super.remove(o);
		return removed;
	}
}