package dto.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class Stream {
	public interface interface_Callback {
		public Callback configuration(final java.util.Map<String, Object> configuration);
		public Callback configure(final java.lang.String key, final java.lang.Object value);
		public java.lang.Object process(final java.lang.Object o);
		public java.lang.Object translate(final java.lang.Object o);
	}
	public static class Callback implements Stream.interface_Callback {
		protected java.util.Map<String, Object> configuration;// = null;
		public Callback configuration(final java.util.Map<String, Object> configuration) {
			this.configuration = configuration;
			return this;
		}
		public Callback configure(final java.lang.String key, final java.lang.Object value) {
			if (null == this.configuration) {
				this.configuration = new java.util.HashMap<String, Object>();
			}
			this.configuration.put(key,  value);
			return this;
		}
		@Override
		public java.lang.Object process(final java.lang.Object o) {
		  //throw new UnsupportedOperationException();
			return null;
		}
		@Override
		public java.lang.Object translate(final java.lang.Object o) {
		  //throw new UnsupportedOperationException();
			return o;
		}
	}
	
	public static final Stream Meta = new Stream();
	public static final Stream Data = new Stream();
	
	protected final java.lang.String m;// = null;
	protected final java.lang.Object o;// = null;
	protected final java.lang.Class<?> c;// = null;
	{
	  //m = null;
	  //o = null;
	  //c = null;
	}
	
	protected Stream() {
		m = null;
		o = null;
		c = null;
	}
	protected Stream(final java.lang.Object o) {
		this.m = o.getClass().getName().replaceAll("\\.", "-").toUpperCase();
		this.o = o;
		this.c = o.getClass();
	}
	
	public byte[] toStream(final java.lang.Object o, final Stream.interface_Callback sic, final Stream type) {
		throw new IllegalStateException();
	}
	public byte[] toStream(final java.lang.Object o, final Stream type) {
		return this.toStream(o, (Stream.interface_Callback)null, type);
	}
	
	protected java.lang.String toString(final java.lang.Object o) {
		throw new IllegalStateException();
	}
	protected java.lang.Object fromBytes(final byte[] ba) {
		throw new IllegalStateException();
	}
	
	public java.lang.Object fromStream(final byte[] stream, final Stream type) {
		throw new IllegalStateException();
	}
	public java.lang.Object fromStream(final ByteArrayInputStream stream, final Stream type) throws IOException {
		throw new IllegalStateException();
	}
	public java.lang.Object fromStream(final ByteArrayInputStream meta, final ByteArrayInputStream data) throws IOException {
		throw new IllegalStateException();
	}
	
	public java.lang.Object transform(final java.lang.Object o, final Stream.interface_Callback sic, final Source source) {
		throw new IllegalStateException();
	}
	public java.lang.Object transform(final java.lang.Object o, final Source source) {
		return this.transform(o, (Stream.interface_Callback)null, source);
	}
}