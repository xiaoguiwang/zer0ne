package dto.util;

public interface interface_List<E> extends java.util.List<E>, dro.lang.Id {
	public abstract boolean add(final E e, final dro.lang.Break.Recursion br);
	public abstract E get(final int index, final dro.lang.Break.Recursion br);
	public abstract E set(final int index, final E e, final dro.lang.Break.Recursion br);
	public abstract boolean remove(final java.lang.Object o, final dro.lang.Break.Recursion br);
	public abstract E remove(final int index, final dro.lang.Break.Recursion br);
}