package dto.util;

public interface interface_Map<K, V> extends java.util.Map<K, V>, dro.lang.Id {
	public abstract V put(final K k, final V v, final dro.lang.Break.Recursion br);
	public abstract V get(final java.lang.Object k, final dro.lang.Break.Recursion br);
	public abstract boolean remove(final java.lang.Object k, final java.lang.Object v, final dro.lang.Break.Recursion br);
	public abstract V remove(final java.lang.Object k, final dro.lang.Break.Recursion br);
}