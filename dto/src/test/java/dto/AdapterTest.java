package dto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Test
	public void test_0000() {
		final java.math.BigDecimal n = new java.math.BigDecimal("0.0");
		final byte[] stream = new dto.Adapter().stream(n);
		final java.math.BigDecimal __n = (java.math.BigDecimal)(new dto.Adapter().stream(stream, java.math.BigDecimal.class));
		Assert.assertEquals(n, __n);
	}
	@Test
	public void test_0001() {
		final java.math.BigInteger n = new java.math.BigInteger("0");
		final byte[] stream = new dto.Adapter().stream(n);
		final java.math.BigInteger __n = (java.math.BigInteger)(new dto.Adapter().stream(stream, java.math.BigInteger.class));
		Assert.assertEquals(n, __n);
	}
	@Test
	public void test_0002() {
		final java.lang.Boolean b = new java.lang.Boolean(true);
		final byte[] stream = new dto.Adapter().stream(b);
		final java.lang.Boolean __b = (java.lang.Boolean)(new dto.Adapter().stream(stream, java.lang.Boolean.class));
		Assert.assertEquals(b, __b);
	}
	@Test
	public void test_0003() {
		final java.lang.Byte b = new java.lang.Byte((byte)0);
		final byte[] stream = new dto.Adapter().stream(b);
		final java.lang.Byte __b = (java.lang.Byte)(new dto.Adapter().stream(stream, java.lang.Byte.class));
		Assert.assertEquals(b, __b);
	}
	@Test
	public void test_0004() {
		final java.lang.Character c = new java.lang.Character((char)0);
		final byte[] stream = new dto.Adapter().stream(c);
		final java.lang.Character __c = (java.lang.Character)(new dto.Adapter().stream(stream, java.lang.Character.class));
		Assert.assertEquals(c, __c);
	}
	@Test
	public void test_0005() {
		final java.util.Date d = new java.util.Date(System.currentTimeMillis());
		final byte[] stream = new dto.Adapter().stream(d);
		final java.util.Date __d = (java.util.Date)(new dto.Adapter().stream(stream, java.util.Date.class));
		Assert.assertEquals(d, __d);
	}
	@Test
	public void test_0006() {
		final java.lang.Double n = new java.lang.Double(0.0d);
		final byte[] stream = new dto.Adapter().stream(n);
		final java.lang.Double __n = (java.lang.Double)(new dto.Adapter().stream(stream, java.lang.Double.class));
		Assert.assertEquals(n, __n);
	}
	@Test
	public void test_0007() {
		final java.lang.Float n = new java.lang.Float(0.0f);
		final byte[] stream = new dto.Adapter().stream(n);
		final java.lang.Float __n = (java.lang.Float)(new dto.Adapter().stream(stream, java.lang.Float.class));
		Assert.assertEquals(n, __n);
	}
	@Test
	public void test_0008() {
		final java.lang.Integer n = new java.lang.Integer(0);
		final byte[] stream = new dto.Adapter().stream(n);
		final java.lang.Integer __n = (java.lang.Integer)(new dto.Adapter().stream(stream, java.lang.Integer.class));
		Assert.assertEquals(n, __n);
	}
	@Test
	public void test_0009() {
		final java.lang.Long n = new java.lang.Long(0);
		final byte[] stream = new dto.Adapter().stream(n);
		final java.lang.Long __n = (java.lang.Long)(new dto.Adapter().stream(stream, java.lang.Long.class));
		Assert.assertEquals(n, __n);
	}
	@Test
	public void test_0010() {
		final java.lang.Short n = new java.lang.Short((short)0);
		final byte[] stream = new dto.Adapter().stream(n);
		final java.lang.Short __n = (java.lang.Short)(new dto.Adapter().stream(stream, java.lang.Short.class));
		Assert.assertEquals(n, __n);
	}
  //@Test
	public void test_0011a() throws IOException {
		final java.lang.String s = new java.lang.String("Hello World!");
		final byte[] stream = new dto.Adapter().stream(s);
		final java.lang.String __s = (java.lang.String)(new dto.Adapter().stream(stream, java.lang.String.class));
		Assert.assertEquals(s, __s); // FIXME
	}
	@Test
	public void test_0011b() throws IOException {
		final java.lang.String s = new java.lang.String("Hello World!");
		final ByteArrayInputStream stream = new ByteArrayInputStream(s.getBytes());
	  //final java.lang.String __s = (java.lang.String)(new dto.Adapter().stream(stream, java.lang.String.class));
	  //Assert.assertEquals(s, __s); // FIXME
	}
  /*@Test
	public void test_0011c() throws IOException {
		final java.lang.String s = new java.lang.String("Hello World!");
		final ByteArrayOutputStream stream = new ByteArrayOutputStream();
		byte[] ba = new dto.Adapter().stream(s, stream);
System.out.println(new java.lang.String(ba));
	}*/
	
	@Test
	public void test_0012() {
		final java.lang.Boolean[] ba = new java.lang.Boolean[]{true,false};
		final byte[] stream = new dto.Adapter().stream(ba);
System.out.println(new java.lang.String(stream));
	  //final java.lang.Boolean[] __ba = (java.lang.Boolean[])(new dto.Adapter().stream(stream, java.lang.Boolean[].class));
	  //Assert.assertEquals(ba, __ba); // TODO
	}
	
	@Test
	public void test_0013a() {
		final java.util.List<java.lang.Integer> list = new java.util.ArrayList<java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				add(new java.lang.Integer(0));
				add(new java.lang.Integer(1));
			}
		};
		final byte[] stream = new dto.Adapter().stream(list);
System.out.println(new java.lang.String(stream));
	  //Assert.assertEquals(list, __list); // TODO
	}
	@Test
	public void test_0013b() throws IOException {
		final java.util.List<java.lang.Integer> list = new java.util.ArrayList<java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				add(new java.lang.Integer(0));
				add(new java.lang.Integer(1));
			}
		};
	  /*final */ByteArrayInputStream meta = null;
		{
		  //final byte[] stream = new dto.Adapter().metaStream(list);
//System.out.println(new java.lang.String(stream));
		///*final ByteArrayInputStream */meta = new ByteArrayInputStream(stream);
		}
	  /*final */ByteArrayInputStream data = null;
		{
		  //final byte[] stream = new dto.Adapter().dataStream(list);
//System.out.println(new java.lang.String(stream));
		///*final ByteArrayInputStream */data = new ByteArrayInputStream(stream);
		}
	  //final java.util.List<java.lang.Integer> __list = (java.util.List<java.lang.Integer>)(new dto.Adapter().stream(meta, data, java.util.List.class));
	  //__list.hashCode();
	  //Assert.assertEquals(list, __list); // TODO
	}
	@Test
	public void test_0014() {
		final java.util.Set<java.lang.Integer> set = new java.util.HashSet<java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				add(new java.lang.Integer(0));
				add(new java.lang.Integer(1));
			}
		};
		final byte[] stream = new dto.Adapter().stream(set);
System.out.println(new java.lang.String(stream));
	  //Assert.assertEquals(set, __set); // TODO
	}
	
	@Test
	public void test_0015() {
		final java.util.Map<java.lang.String, java.lang.Integer> map = new java.util.HashMap<java.lang.String, java.lang.Integer>(){
			private static final long serialVersionUID = 0L;
			{
				put("0", new java.lang.Integer(0));
				put("1", new java.lang.Integer(1));
			}
		};
		final byte[] stream = new dto.Adapter().stream(map);
System.out.println(new java.lang.String(stream));
	  //Assert.assertEquals(map, __map); // TODO
	}
	
	@Test
	public void test_0016() {
		final java.util.Map<java.lang.String, java.util.List<java.lang.Integer>> map = new java.util.HashMap<java.lang.String, java.util.List<java.lang.Integer>>(){
			private static final long serialVersionUID = 0L;
			{
				put("0", new java.util.ArrayList<java.lang.Integer>(){
					private static final long serialVersionUID = 0L;
					{
						add(new java.lang.Integer(0));
						add(new java.lang.Integer(1));
					}
				});
				put("10", new java.util.ArrayList<java.lang.Integer>(){
					private static final long serialVersionUID = 0L;
					{
						add(new java.lang.Integer(10));
						add(new java.lang.Integer(11));
					}
				});
			}
		};
		final byte[] stream = new dto.Adapter().stream(map);
System.out.println(new java.lang.String(stream));
	  //Assert.assertEquals(map, __map); // TODO
	}
	@Test
	public void test_0017() throws IOException {
		final java.util.Map<java.lang.String, java.util.List<java.lang.Integer>> map = new java.util.HashMap<java.lang.String, java.util.List<java.lang.Integer>>(){
			private static final long serialVersionUID = 0L;
			{
				put("0", new java.util.ArrayList<java.lang.Integer>(){
					private static final long serialVersionUID = 0L;
					{
						add(new java.lang.Integer(0));
						add(new java.lang.Integer(1));
					}
				});
				put("10", new java.util.ArrayList<java.lang.Integer>(){
					private static final long serialVersionUID = 0L;
					{
						add(new java.lang.Integer(10));
						add(new java.lang.Integer(11));
					}
				});
			}
		};
	  /*final */ByteArrayInputStream meta = null;
		{
		  //final byte[] stream = new dto.Adapter().metaStream(map);
//System.out.println(new java.lang.String(stream));
		///*final ByteArrayInputStream */meta = new ByteArrayInputStream(stream);
		}
	  /*final */ByteArrayInputStream data = null;
		{
		  //final byte[] stream = new dto.Adapter().dataStream(map);
//System.out.println(new java.lang.String(stream));
		///*final ByteArrayInputStream */data = new ByteArrayInputStream(stream);
		}
	  //final java.util.Map<java.lang.String, java.lang.Integer> __map = (java.util.Map<java.lang.String, java.lang.Integer>)(new dto.Adapter().stream(meta, data, java.util.Map.class));
	  //__map.hashCode();
	  //Assert.assertEquals(map, __map); // TODO
	}
}