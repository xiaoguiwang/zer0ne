package dto;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataTest {
	@Test
	public void test_0000() {
	  /*final dto.Data d = */new dto.Data();
	}
	
	@Test
	public void test_0001() {
		final java.lang.String json = dto.Data.toJson(
			new dro.Data().set("Hello", "World!")
		);
		Assert.assertEquals("{\"Hello\":\"World!\"}", json);
	}
	
	@Test
	public void test_0002() {
		final java.lang.String json = dto.Data.toJson(
			new dro.Data().set(
				"Hello", new java.lang.String[]{"There?", "World!"}
			)
		);
		Assert.assertEquals("{\"Hello\":[\"There?\",\"World!\"]}", json);
	}
	
	@Test
	public void test_0003() {
		final java.lang.String json = dto.Data.toJson(
			new dro.Data().set(
				"Hello", new LinkedHashSet<java.lang.String>(){
					private static final long serialVersionUID = 0L;
					{
						add("There?");
						add("World!");
					}
				}
			)
		);
		Assert.assertEquals("{\"Hello\":[\"There?\",\"World!\"]}", json);
	}
	
	@Test
	public void test_0004() {
		final java.lang.String json = dto.Data.toJson(
			new dro.Data().set(
				"Hello", new ArrayList<java.lang.String>(){
					private static final long serialVersionUID = 0L;
					{
						add("There?");
						add("World!");
					}
				}
			)
		);
		Assert.assertEquals("{\"Hello\":[\"There?\",\"World!\"]}", json);
	}
	
	@Test
	public void test_0005() {
		final java.lang.String json = dto.Data.toJson(
			new dro.Data().set(
				"Hello", new dro.Data().set("There?", "World!")
			)
		);
		Assert.assertEquals("{\"Hello\":[\"There?\",\"World!\"]}", json);
	}
	
	
	@Test
	public void test_0006() {
	  /*final */java.lang.String json = dto.Data.toJson(
			new dro.Data().set(
				"Hello", new dro.Data().set("There?", "World!")
			)
		);
		{
			final dro.Data dro = dto.Data.fromJson(json);
			json = dto.Data.toJson(dro);
			Assert.assertEquals("{\"Hello\":{\"There?\":\"World!\"}}", json);
		}
	}
}