package dto;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dro.Serializer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SerializerTest {
  /*public SerializerTest() {
	}*/
  /*@Before
	public void before() {
	}*/
  /*@After
	public void after() {
	}*/
	
	@Test
	public void test_0009() { // Ref. dro.SerializerTest.test_0009
		final java.util.Map<java.lang.String, java.lang.Object> parent = new HashMap<>();
		
		final java.util.Map<java.lang.String, java.lang.Object> child1 = new HashMap<>();
		child1.put("parent", parent);
		
		final java.util.Map<java.lang.String, java.lang.Object> child2 = new HashMap<>();
		child2.put("parent", parent);
		
		final java.util.List<java.util.Map<?,?>> children = new ArrayList<>();
		children.add(child1);
		children.add(child2);
		
		parent.put("children", children);
		
		java.util.List<dro.lang.Object> list = new Serializer().serialise(parent);
		Assert.assertTrue(list != null);
		Assert.assertSame(4, list.size());
		for (final dro.lang.Object to: list) {
		  //final java.lang.Class<?> c = to.returns(dro.lang.Class.Return.Class);
			final java.lang.Object o = to.returns(dro.lang.Object.Return.Object);
		  /*final int hashCode = */dro.lang.Object.hashCode(o);
		}
	  //final java.lang.String json = dto.lang.Object.toJson(list);
//System.out.println("json="+json);
	}
}