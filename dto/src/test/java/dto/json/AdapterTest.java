package dto.json;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdapterTest {
	@Test
	public void test_0000() {
		{
			final java.math.BigDecimal n = new java.math.BigDecimal("0.0");
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.math.BigInteger n = new java.math.BigInteger("0");
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.lang.Boolean b = new java.lang.Boolean("true");
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(b.toString().getBytes(), b.getClass());
			Assert.assertEquals(b, o);
		}
	  /*{
			final java.lang.Byte b = new java.lang.Byte((byte)'A');
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(b.toString().getBytes(), b.getClass());
			Assert.assertEquals(b, o);
		}*/
		{
			final java.lang.Character c = new java.lang.Character((char)'A');
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(c.toString().getBytes(), c.getClass());
			Assert.assertEquals(c, o);
		}
		{
			final java.util.Date date = new java.util.Date(System.currentTimeMillis());
		/**/final java.lang.Object o = dto.Context.getAdapterFor(this).stream(new java.lang.Long(date.getTime()).toString().getBytes(), date.getClass());
			Assert.assertEquals(date, o);
		}
		{
			final java.lang.Double n = new java.lang.Double(123.0d);
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.lang.Float n = new java.lang.Float(123.0f);
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.lang.Integer n = new java.lang.Integer(123);
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.lang.Long n = new java.lang.Long(123L);
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.lang.Short n = new java.lang.Short((short)123);
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(n.toString().getBytes(), n.getClass());
			Assert.assertEquals(n, o);
		}
		{
			final java.lang.String s = new java.lang.String("Hello World!");
			final java.lang.Object o = dto.Context.getAdapterFor(this).stream(s.getBytes(), s.getClass());
		/**/Assert.assertEquals('"'+s+'"', o);
		}
	}
	@Test
	public void test_0001() {
		final java.lang.String json = "[ \"key\", \"value\" ]";
		final java.lang.Object[] oa = (java.lang.Object[])dto.Context.getAdapterFor(this).stream(json.getBytes(), java.lang.Object[].class);
		System.out.println("json:array.length="+oa.length);
	}
	@Test
	public void test_0002() {
		final java.lang.String json = "[ \"key\", \"value\" ]";
		@SuppressWarnings("unchecked")
		final java.util.List<String> list = (java.util.List<String>)dto.Context.getAdapterFor(this).stream(json.getBytes(), java.util.List.class);
		System.out.println("json:list.size()="+list.size());
	}
	@Test
	public void test_0003() {
		final java.lang.String json = "[ \"key\", \"value\" ]";
		@SuppressWarnings("unchecked")
		final java.util.Set<String> set = (java.util.Set<String>)dto.Context.getAdapterFor(this).stream(json.getBytes(), java.util.Set.class);
		System.out.println("json:set.size()="+set.size());
	}
	@Test
	public void test_0004() {
		final java.lang.String json = "{ \"key\": \"value\" }";
		@SuppressWarnings("unchecked")
		final java.util.Map<String, Object> map = (java.util.Map<String, Object>)dto.Context.getAdapterFor(this).stream(json.getBytes(), java.util.Map.class);
		System.out.println("json:map.size()="+map.size());
	}
	@Test
	public void test_0005() {
		final java.lang.String json = "{ \"key\": [ \"value\" ] }";
		@SuppressWarnings("unchecked")
		final java.util.Map<String, Object> map = (java.util.Map<String, Object>)dto.Context.getAdapterFor(this).stream(json.getBytes(), java.util.Map.class);
		System.out.println("json:map.size()="+map.size());
	  //final java.lang.Object[] oa = (java.lang.Object[])map.get("key");
	  //System.out.println("json:array.length="+oa.length);
		@SuppressWarnings("unchecked")
		final java.util.List<String> list = (java.util.List<String>)map.get("key");
		System.out.println("json:list.size()="+list.size());
	  //final java.util.Set<String> set = (java.util.Set<String>)map.get("key");
	  //System.out.println("json:set.size()="+set.size());
	}
	@Test
	public void test_0006() {
		final byte[] ba = dto.Context.getAdapterFor(this).stream(
			new java.lang.String[]{
				"Hello World!"
			}
		);
		final java.lang.String json = new java.lang.String(ba);
		System.out.println("json="+json);
	}
	@Test
	public void test_0007() {
		final byte[] ba = dto.Context.getAdapterFor(this).stream(
			new dto.util.ArrayList<String>(){
				private static final long serialVersionUID = 0L;
				{
					add("Hello World!");
				}
			}
		);
		final java.lang.String json = new java.lang.String(ba);
		System.out.println("json="+json);
	}
	@Test
	public void test_0008() {
		final byte[] ba = dto.Context.getAdapterFor(this).stream(
			new dto.util.HashSet<String>(){
				private static final long serialVersionUID = 0L;
				{
					add("Hello World!");
				}
			}
		);
		final java.lang.String json = new java.lang.String(ba);
		System.out.println("json="+json);
	}
	@Test
	public void test_0009() {
		final byte[] ba = dto.Context.getAdapterFor(this).stream(
			new dto.util.HashMap<String, Object>(){
				private static final long serialVersionUID = 0L;
				{
					put("key", "value");
				}
			}
		);
		final java.lang.String json = new java.lang.String(ba);
		System.out.println("json="+json);
	}
}