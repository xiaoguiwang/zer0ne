package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddressLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		final java.util.List<java.util.Map<String, Object>> addresses = (java.util.List<java.util.Map<String, Object>>)super.property("addresses");
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.removeAttribute("meta-http-equiv");
		final String submit = req.getParameter("submit");
		final Integer address_id = null == req.getParameter("address-id") ? null : Integer.valueOf(req.getParameter("address-id"));
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null != address_id) {
				final java.util.Map<String, Object> address = addresses.get(address_id);
				req.setAttribute("address", address);
			}
		} else if (null != submit && 0 < submit.trim().length()) {
		  /*final */java.util.Map<String, Object> address = (java.util.Map<String, Object>)addresses.get(address_id);
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (true == submit.equals("create") && null == address) {
				// Validate data first!
				address = new dao.util.HashMap<String, Object>();
				address.put("address-id", address_id);
				for (final String key: new String[]{"country","detail","code"}) {
					final String value = (String)req.getParameter(key);
					if (null == value) {
						req.removeAttribute(key);
					} else {
						address.put(key, value);
						req.setAttribute(key, value);
					}
				}
				addresses.add(address);
			} else if (true == submit.equals("update") && null != address) {
				for (final String key: new String[]{"country","detail","code"}) {
					final String value = (String)req.getParameter(key);
					if (null == value && null != address.get(key)) {
						address.remove(key);
						req.removeAttribute(key);
					} else if (null != value && false == value.equals(address.get(key))) {
						address.put(key, value);
						req.setAttribute(key, value);
					}
				}
			} else if (true == submit.equals("delete") && null != address) {
				// TODO: check dependencies (can't delete a address if users have/need it.. etc.
			}
			req.setAttribute("address", address);
		}
	  /*final */String path = req.getPathInfo();
	  /*final String */path = uri.replace("/hci", "");
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}