package hci.j2ee;

@SuppressWarnings("unchecked")
public class Addresses {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> addresses;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Addresses.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */addresses = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("addresses", java.util.List.class);
		if (null == addresses) {
			addresses = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("address-id", 0);
							put("country", "Canada");
							put("detail", "234 Tanner Dr SE\r\nAirdrie\r\nAB");
							put("code", "T4A 1S1");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(addresses, "addresses");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> address = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __address: addresses) {
			if (true == id.equals(__address.get("$id").toString())) {
				address = __address;
				break;
			}
		}
		return address;
	}
	
  /*public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
		Addresses.addRole(group, role_id, Break.Recursion);
		Roles.addToGroup(Roles.roles.get(role_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles");
		if (null == roles) {
		*//*final java.util.Set<java.lang.String> *//*roles = new dao.util.HashSet<java.lang.String>();
			group.put("roles", roles);
		}
		roles.add(role_id);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
		Addresses.removeRole(group, role_id, Break.Recursion);
		Roles.removeFromGroup(Roles.roles.get(role_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles");
		if (null != roles) {
			roles.remove(role_id);
			if (0 == roles.size()) {
				group.remove("roles");
			}
		}
	}
	
	public static void addUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
		Addresses.addUser(group, user_id, Break.Recursion);
		Users.addToGroup(Users.users.get(user_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users");
		if (null == users) {
		*//*final java.util.Set<java.lang.String> *//*users = new dao.util.HashSet<java.lang.String>();
			group.put("users", users);
		}
		users.add(user_id);
	}
	public static void removeUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
		Addresses.removeUser(group, user_id, Break.Recursion);
		Users.removeFromGroup(Users.users.get(user_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removeUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users");
		if (null != users) {
			users.remove(user_id);
			if (0 == users.size()) {
				group.remove("users");
			}
		}
	}
	
	public static boolean hasPermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id) {
	*//*final *//*boolean hasPermission = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != permission_id && 0 < permission_id.trim().length()) {
			final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)group.get("permissions"); // Group permissions
			hasPermission = null != permissions && true == permissions.contains(permission_id);
		}
		if (false == hasPermission) { // keep looking..
			final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // Group roles
			if (null != roles) {
				for (final String role_id: roles) {
					final java.util.Map<java.lang.String, java.lang.Object> role = Roles.roles.get(role_id);
					{
						final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions"); // Group role permissions
						hasPermission = null != permissions && true == permissions.contains(permission_id);
					}
					if (true == hasPermission) break;
				}
			}
		}
		return hasPermission;
	}
	public static boolean hasRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
	*//*final *//*boolean hasRole = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != role_id && 0 < role_id.trim().length()) {
			final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // Group roles
			hasRole = null != roles && true == roles.contains(role_id);
		}
		return hasRole;
	}
	public static boolean hasUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
	*//*final *//*boolean hasUser = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != user_id && 0 < user_id.trim().length()) {
			final java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users"); // Group users
			hasUser = null != users && true == users.contains(user_id);
		}
		return hasUser;
	}*/
}