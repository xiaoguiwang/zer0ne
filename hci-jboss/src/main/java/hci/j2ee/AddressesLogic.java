package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddressesLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final java.util.Map<String, java.util.Map<String, Object>> sessions = (java.util.Map<String, java.util.Map<String, Object>>)super.property("sessions");
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		final java.util.List<java.util.Map<String, Object>> addresses = (java.util.List<java.util.Map<String, Object>>)super.property("addresses");
		req.setAttribute("addresses", addresses);
		
	  /*final */java.util.List<Integer> __addresses = null;
		
	  /*final */java.util.Map<String, Object> __role = null;
	  /*final */java.util.Map<String, Object> __user = null;
	  /*final */java.util.Map<String, Object> __group = null;
	  /*final */java.util.Map<String, Object> __bill = null;
	  /*final */String __bill_key = null;
		
	  /*final */String __role_id = (String)req.getParameter("role-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__role_id)) __role_id = null;
		if (null != __role_id) {
			final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)super.property("roles");
		  /*final java.util.Map<String, Object> */__role = roles.get(__role_id);
			__addresses = (java.util.List<Integer>)__role.get("addresses");
			req.setAttribute("role-id", __role_id);
		}
		
	  /*final */String __user_id = (String)req.getParameter("user-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__user_id)) __user_id = null;
		if (null != __user_id) {
			final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)super.property("users");
		  /*final java.util.Map<String, Object> */__user = users.get(__user_id);
			__addresses = (java.util.List<Integer>)__user.get("addresses");
			req.setAttribute("user-id", __user_id);
		}
		
	  /*final */String __group_id = (String)req.getParameter("group-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__group_id)) __group_id = null;
		if (null != __group_id) {
			final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)super.property("groups");
		  /*final java.util.Map<String, Object> */__group = groups.get(__group_id);
			__addresses = (java.util.List<Integer>)__group.get("addresses");
			req.setAttribute("group-id", __group_id);
		}
		
	  /*final */Integer __bill_id = null == req.getParameter("bill-id") ? null : Integer.parseInt(req.getParameter("bill-id"));
	  /*final String */__bill_key = null != req.getParameter("from") ? "from" : null != req.getParameter("to") ? "to" : null; 
	  /*final */java.util.Map<String, Integer> __address_keys = null;
		if (null != __bill_id) {
			final java.util.List<java.util.Map<String, Object>> bills = (java.util.List<java.util.Map<String, Object>>)super.property("bills");
		  /*final java.util.Map<String, Object> */__bill = bills.get(__bill_id);
		  /*final java.util.Map<String, Integer> */__address_keys = (java.util.Map<String, Integer>)__bill.get("addresses");
			if (null != __address_keys) {
				for (final String address_key: __address_keys.keySet()) {
if (true == address_key.equals("$id")) continue;
					final Integer address_id = __address_keys.get(address_key);
					if (null == __addresses) {
						__addresses = new dao.util.ArrayList<Integer>();
					}
					__addresses.add(address_id);
				}
			}
			req.setAttribute("bill-id", __bill_id);
		}
		
		if (null != __addresses) {
			for (final Integer __address_id: __addresses) {
				final java.util.Map<String, Object> __address = (java.util.Map<String, Object>)addresses.get(__address_id);
				final String id = __address.get("$id").toString();
				final boolean checked;
				if (null == __bill_key) {
					checked = true;
				} else if (null != __address_keys) {
					final Integer address_id = __address_keys.get(__bill_key);
					checked = __address_id == address_id;
				} else {
					throw new IllegalStateException();
				}
				req.setAttribute("address$"+id+"#checked", checked);
			}
		}
		
		req.removeAttribute("meta-http-equiv");
	  /*final */String path = req.getPathInfo();
		final String submit = req.getParameter("submit");
		if (null != submit && 0 < submit.trim().length()) {
		  /*final */boolean complete = false;
			if (true == submit.equals("update")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __address_ids = map.get("address-ids");
				for (int i = 0; i < __address_ids.length; i++) {
					final String checked = req.getParameter("address$"+__address_ids[i]+"#checked");
					final java.util.Map<String, Object> __address = Addresses.getId(__address_ids[i]);
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
				  /*} else if (null != __role_id) {
						if (false == Groups.hasPermission(__address, __role_id)) {
							if (null != checked && true == checked.equals("on")) {
								Groups.addPermission(__address, __role_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Groups.removePermission(__address, __role_id);
							}
						}
						complete = true;
					} else if (null != __user_id) {
						if (false == Groups.hasRole(__address, __user_id)) {
							if (null != checked && true == checked.equals("on")) {
								Groups.addRole(__address, __user_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Groups.removeRole(__address, __user_id);
							}
						}
						complete = true;*/
					} else if (null != __bill_id) {
					///*final */java.util.Map<String, Integer> __address_keys = (java.util.Map<String, Integer>)__bill.get("addresses");
					  //if (false == Bills.hasAddress(__bill, __address_id)) {
							if (null != checked && true == checked.equals("on")) {
							  //Bills.addAddress(__bill, __address_id);
								if (null == __address_keys) {
									__address_keys = new dao.util.HashMap<String, Integer>();
								}
								__address_keys.put(__bill_key, (Integer)__address.get("address-id"));
							}
					  //} else {
							if (null == checked || false == checked.equals("on")) {
							  //Bills.removeAddress(__bill, __address_id);
								if (null != __address_keys) {
									__address_keys.remove(__bill_key);
								}
							}
					  //}
						complete = true;
					} else {
						req.setAttribute("address$"+__address_ids[i]+"#checked", new String("on").equals(checked));
					}
				}
			} else if (true == submit.equals("delete")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __address_ids = map.get("address-ids");
				for (int i = 0; i < __address_ids.length; i++) {
					final String checked = req.getParameter("address$"+__address_ids[i]+"#checked");
					final java.util.Map<String, Object> __address = Addresses.getId(__address_ids[i]);
				  //final Integer __address_id = (Integer)__address.get("address-id");
					if (null != checked && true == checked.equals("on")) {
						addresses.remove(__address);
					}
				}
			}
			if (false == complete) {
			  /*final String */path = req.getPathInfo();
			  /*final String */path = uri.replace("/hci", "");
			} else {
			  /*final String */path = "/index";
			/**/req.setAttribute("users", Users.users); // Hacked, for testing groups off index
				req.setAttribute("signed-in-user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("signed-in-method", ServletTool.get(session, "signed-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />"); // TODO: url encoding
			}
		} else {
		  /*final String */path = req.getPathInfo();
		  /*final String */path = uri.replace("/hci", "");
		}
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}