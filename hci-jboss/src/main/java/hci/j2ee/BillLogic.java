package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Ref. https://squareup.com/login
public class BillLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		final String mode = req.getParameter("mode");
		final String submit = req.getParameter("__submit");
	  /*final */java.util.Map<String, String> results = null;
		
	  /*final */java.util.Map<String, Object> bill = null;
	  /*final */java.util.Map<String, Object> /*bill_*/history = null;
	  /*final */java.util.List<java.util.Map<String, Object>> /*bill_*/lines_history = null;
	  /*final */java.util.Map<String, Object> /*bill_*/line_history = null;
		
		if (null != req.getParameter("for-user-id")) {
			ServletTool.put(session, "for:user#id", true);
			req.setAttribute("users", Users.users);
		}
		if (null != req.getParameter("for-user-id") && null != ServletTool.get(session, "for:user#id")) {
			req.setAttribute("for:user#id", true);
		}
		if (null != req.getParameter("for-line-id")) {
			ServletTool.put(session, "for:line#id", true);
		  //req.setAttribute("service#lines", Services.services);
		}
		if (null != req.getParameter("for-line-id") && null != ServletTool.get(session, "for:line#id")) {
			req.setAttribute("for:line#id", true);
		}
		
	  /*final */String path = uri.replace("/hci", "");
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null == req.getParameter("lines")) {
				if (null == req.getParameter("line")) {
					if (null == req.getParameter("history")) {
						if (true == path.startsWith("/bill") && false == path.startsWith("/bills")) {
						  /*final */Integer bill_id = null;
							if (true == "create".equals(mode)) {
							  /*final java.util.Map<String, Object> */bill = new java.util.HashMap<String, Object>();
								ServletTool.put(session, "bill", bill);
							} else {
								// This is the scenario for view bill
								try {
								  /*final Integer */bill_id = new Integer(req.getParameter("id"));
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
								if (null != bill_id) {
								  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
								}
							}
							if (null != bill) {
								final java.util.List<java.util.Map<String, Object>> /*bill_*/history_list = Bills.getHistoryForBillId(bill_id);
								if (null != /*service_*/history_list) {
								  //req.setAttribute("bill#history", true);
									req.setAttribute("bill@history", true); // TODO: eliminate this in bill-form.jsp
								}
							}
							if (null != req.getParameter("cancel")) {
								req.setAttribute("cancel", req.getParameter("cancel"));
							}
						}
					} else {
						if (true == path.startsWith("/bill") && false == path.startsWith("/bills") && null != req.getParameter("history")) {
							if (null != req.getParameter("for-user-id") && null != ServletTool.get(session, "for:user#id")) {
							  /*final java.util.Map<String, Object> bill_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#history");
								if (null != /*bill_*/history) {
									final String _for_user_id = (String)/*bill_*/history.get("for-user-id");
									if (null != _for_user_id) {
										final java.util.Map<String, Object> _user = Users.users.get(_for_user_id);
										final Integer /*user*/_id = (Integer)_user.get("$id");
										if (null != /*user*/_id) {
											req.setAttribute("user$"+/*user*/_id+"#checked", true);
										}
									}
								}
							}
						  /*final */Integer bill_id = null;
						  /*final */Integer /*bill_*/history_id = null;
							if (null == req.getParameter("bill-id")) {
								try {
								  /*final Integer */bill_id = new Integer(req.getParameter("id"));
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
							} else {
								try {
								  /*final Integer */bill_id = new Integer(req.getParameter("bill-id")); // If you give me a bill-id as well, I will show bill#history (not bill@history list)
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
							///*final */Integer /*bill_*/history_id = null;
								try {
								  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
								if (null != /*bill_*/history_id) {
								  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
								}
							}
							if (false == Boolean.TRUE.booleanValue()) {
								if (null == bill_id && null != /*bill_*/history_id) {
									final Integer /*bill*/_id = (Integer)/*bill_*/history.get("bill-id");
								  /*final java.util.Map<String, Object> */bill = Bills.getById(/*bill*/_id);
								}
							}
							if (null != bill_id) {
							  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
							}
							final String nonce = req.getParameter("sq:nonce");
							if (true == "create".equals(mode)) {
								if (null != ServletTool.get(session, "bill#history")) {
								  /*final java.util.Map<String, Object> bill_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#history");
								} else {
								  /*final java.util.Map<String, Object> bill_*/history = new java.util.HashMap<String, Object>();
									ServletTool.put(session, "bill#history", /*bill_*/history);
								}
							} else if (null == nonce) {
								// This is the scenario for view bill history
								if (null != bill && null == /*bill_*/history) {
									final java.util.List<java.util.Map<String, Object>> /*bill_*/history_list = Bills.getHistoryForBillId(bill_id);
									if (null != /*bill_*/history_list) {
										req.setAttribute("bill@history", true);
									}
								}
						  //} else if (true == "pay".equals(submit)) {
							} else if (false == "refund".equals(submit)) {
								// SquareUp specific implementation (move out to an adapter custom at some point)
								final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
							  /*final */String customer_id = null;
								if (null != user) {
								  /*final String */customer_id = (String)user.get("sq:customer#id");
								}
								if (null == customer_id || 0 == customer_id.trim().length()) {
								  /*final String */customer_id = Users.getCustomerId();
									if (null != customer_id) {
										user.put("sq:customer#id", customer_id);
									}
								}
								if (null != /*bill_*/history_id && null != customer_id && null != nonce) {
								  /*final */String payment_id = (String)/*bill_*/history.get("sq:payment#id");
									if (null == payment_id || 0 == payment_id.trim().length()) {
										// TODO: if/when we're using a card on file, we won't need an sq:nonce at this point - we can just use {user}.{%cards}.{sq:card#id} as the payment source-id
										final java.lang.String idempotency_key = dro.lang.String.generateString(45);
									  /*final String */payment_id = Bills.chargeCustomerCard(/*bill_*/history_id, idempotency_key, customer_id, nonce);
										if (null != payment_id) {
											/*bill_*/history.put("sq:payment#id", payment_id);
											/*bill_*/history.put("$payment", "paid");
										}
									}
								}
							  //if (null != req.getParameter("cancel")) {
									req.setAttribute("cancel", "bills-history");
							  //}
							}
							if (null != req.getParameter("cancel")) {
								req.setAttribute("cancel", req.getParameter("cancel"));
							}
						}
					}
				} else {
					if (null == req.getParameter("history")) {
					} else {
					  /*final */Integer bill_id = null;
						try {
						  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore..
						}
						if (null != bill_id) {
						  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
						}
					  /*final */Integer /*bill_*/history_id = null;
						if (false == "create".equals(mode)) {
							try {
							  /*final Integer bill_*/history_id = new Integer(req.getParameter("history-id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore.. This is the scenario for bill line history show/read ..?
							}
						} else {
							try {
							  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore.. This is the scenario for bill line history show/read ..?
							}
						}
						if (null != /*bill_*/history_id) {
						  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
						}
						if (false == "create".equals(mode)) {
						  /*final */Integer /*bill_history_*/line_id = null;
							try {
							  /*final Integer bill_history_*/line_id = new Integer(req.getParameter("id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore..
							}
							if (null != bill_id && null != /*bill_history_*/line_id) {
							  /*final java.util.Map<String, Object> bill_*/line_history = Bills.getLineHistory(/*bill_*/history_id, line_id);
							}
						} else {
						  /*final java.util.Map<String, Object> bill_*/line_history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#line#history");
						}
if (null != req.getParameter("for-line-id") && null != ServletTool.get(session, "for:line#id")) {
	if (null != /*bill_*/line_history) {
		final String _for_line_id = (String)/*bill_*/line_history.get("service-line-id"); // 1:email
		if (null != _for_line_id) {
			final java.util.Map<String, Object> /*service_*/_line = Services.findServiceLineById(_for_line_id);
			final Integer /*service_line*/_id = (Integer)_line.get("$id");
			if (null != /*line*/_id) {
				req.setAttribute("service-line$"+/*service_line*/_id+"#checked", true);
			}
		}
	}
}
						if (null != req.getParameter("for-line-id")) { // TODO/FIXME: truly/offer all service lines
							final java.util.Map<String, Object> service = Services.get("email");
							req.setAttribute("service", service);
							final java.util.Map<String, Object> service_lines = (java.util.Map<String, Object>)service.get("%lines");
							req.setAttribute("service#lines", service_lines);
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					}
				}
			} else {
				if (null == req.getParameter("history")) {
					throw new IllegalStateException();
				} else {
				  //if (null == req.getParameter("for-line-id")) {
					  /*final */Integer bill_id = null;
						try {
						  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore..
						}
						if (null != bill_id) {
						  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
						}
					  /*final */Integer /*bill_*/history_id = null;
						try {
						  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for bill lines history show/read ..?
						}
						if (null != /*bill_*/history_id) {
						  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
						}
						if (null != /*bill_*/history) {
						  /*final java.util.List<java.util.Map<String, Object>> bill_*/lines_history = (java.util.List<java.util.Map<String, Object>>)/*bill_*/history.get("@lines");
							if (null == bill) {
							  /*final java.util.Map<String, Object> */bill = Bills.getById((Integer)/*bill_*/history.get("bill-id"));
							}
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
				  //}
				}
			}
		} else { // if (null != submit && 0 < submit.trim().length()) {
			if (null == req.getParameter("lines")) {
				if (null == req.getParameter("line")) {
				  /*final */Integer id = null;
					try {
					  /*final Integer */id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore..
					}
					if (null == req.getParameter("history")) {
						if (true == "create".equals(submit)) {
						  /*final */boolean valid = true;
							if (null != ServletTool.get(session, "bill")) {
							  /*final java.util.Map<String, Object> */bill = (java.util.Map<String, Object>)ServletTool.get(session, "bill");
							} else {
							  /*final java.util.Map<String, Object> */bill = new java.util.HashMap<String, Object>();
								ServletTool.put(session, "bill", bill);
							}
							for (final String key: new String[]{"tax-gst"}) {
								final String value = (String)req.getParameter(key);
								if (null != value) {
									if (true == key.equals("tax-gst")) {
										try {
											final Float __value = new Float(value); // 0.0 => "N/A"
											bill.put(key, __value);
										} catch (final NumberFormatException e) {
											if (null == results) {
											  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
											}
											results.put(key, "Not a decimal number");
										  /*final boolean */valid = false;
										}
									} else {
										bill.put(key, value);
									}
									req.setAttribute(key, value);
								}
							}
							if (true == valid) {
							  /*final java.util.Map<String, Object> */bill = new dao.util.HashMap<String, Object>();
								for (final String key: new String[]{"tax-gst"}) {
									final String value = (String)req.getParameter(key);
									if (null != value) {
										if (true == key.equals("tax-gst")) {
											try {
												final Float __value = new Float(value);
												bill.put(key, __value);
											} catch (final NumberFormatException e) {
												// Silently ignore..
											}
										} else {
											bill.put(key, value);
										}
									}
								}
								bill.put("bill-id", Bills.bills.size());
								Bills.bills.add(bill);
								ServletTool.remove(session, "bill");
								req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/bills?session-id="+session_id+"'\" />");
							}
						} else if (true == "update".equals(submit)) {
						  /*final */Integer bill_id = id;
							if (null != bill_id) {
							  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
							}
							if (null != bill) {
								for (final String key: new String[]{"tax-gst"}) {
									final String value = (String)req.getParameter(key);
									if (null == value && null != bill.get(key)) {
										bill.remove(key);
									} else if (null != value && false == value.equals(bill.get(key))) {
										if (true == key.equals("tax-gst")) {
											try {
												float __value = new Float(value);
												bill.put(key, __value);
											} catch (final NumberFormatException e) {
												if (null == results) {
												  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
												}
												results.put(key, "Not a decimal number");
											}
										} else {
											bill.put(key, value);
										}
									}
									req.setAttribute(key, value);
								}
								if (null == results || 0 == results.size()) {
								  /*final String */path = uri.replace("/hci", "");
									req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/bills?session-id="+session_id+"'\" />");
								}
							}
						} else if (true == "delete".equals(submit)) {
						  /*final */Integer bill_id = id;
							if (null != bill_id) {
							  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
							}
							Bills.bills.remove(bill); // Safe-ish
						}
					} else {
					  /*final */Integer bill_id = null;
						if (true == "create".equals(submit)) { // create bill history
						  /*final */boolean valid = true;
							if (null != ServletTool.get(session, "bill#history")) {
							  /*final java.util.Map<String, Object> bill_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#history");
							} else {
							  /*final java.util.Map<String, Object> bill_*/history = new java.util.HashMap<String, Object>();
								ServletTool.put(session, "bill#history", /*bill_*/history);
							}
							for (final String key: new String[]{"bill-id","for-user-id"}) {
								final String value = (String)req.getParameter(false == key.equals("for-user-id") ? key : "bill:history:"+key);
								if (null != value) {
									if (true == key.equals("for-user-id")) {
										if (0 < value.trim().length()) {
										  /*bill_*/history.put(key, value);
										} else {
											if (null == results) {
											  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
											}
											results.put("bill:history:for-user-id", "Please select a user identifier");
										  /*final boolean */valid = false;
										}
									} else {
									  /*final */Object __value = null;
										if (true == key.equals("bill-id")) {
											__value = new Integer(value);
										  /*final java.util.Map<String, Object> */bill = Bills.get((Integer)__value);
											__value = (Integer)bill.get("bill-id");
										} else {
											__value = value;
										}
									  /*bill_*/history.put(key, __value);
									}
								  //req.setAttrbiute("bill:history:"+key, value);
								} else {
									if (true == key.equals("for-user-id")) {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put("bill:history:for-user-id", "Please select a user identifier");
									  /*final boolean */valid = false;
									}
								}
							}
							if (true == valid) {
							  /*final java.util.Map<String, Object> bill_*/history = new dao.util.HashMap<String, Object>();
								for (final String key: new String[]{"bill-id","for-user-id"}) {
									final String value = (String)req.getParameter(false == key.equals("for-user-id") ? key : "bill:history:"+key);
									if (null != value) {
									  /*final */Object __value = null;
										if (true == key.equals("bill-id")) {
											__value = new Integer(value);
										  /*final java.util.Map<String, Object> */bill = Bills.get((Integer)__value);
											__value = (Integer)bill.get("bill-id");
										} else {
											__value = value;
										}
									  /*bill_*/history.put(key, __value);
									}
								  //req.setAttrbiute("bill:history:"+key, value);
								}
							  /*bill_*/history.put("bill-history-id", Bills.history.size());
								Bills.history.add(/*bill_*/history);
								req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/bills?history&session-id="+session_id+"'\" />");
								req.removeAttribute("for:user#id");
								ServletTool.remove(session, "bill#history");
							} else {
								if (null != req.getParameter("cancel")) {
									req.setAttribute("cancel", req.getParameter("cancel"));
								}
							}
						} else if (true == "update".equals(submit)) { // update bill history
						  /*final */boolean valid = true;
						  /*final */Integer /*bill_*/history_id = null;
							try {
							  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore.. This is the scenario for update bill history
							}
							if (null != /*bill_*/history_id) {
							  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
							}
							for (final String key: new String[]{"bill-id","for-user-id"}) {
								final String value = (String)req.getParameter(false == key.equals("for-user-id") ? key : "bill:history:"+key);
								if (null != value) {
									if (true == key.equals("for-user-id")) {
										if (0 < value.trim().length()) {
										  /*bill_*/history.put(key, value);
										} else {
											if (null == results) {
											  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
											}
											results.put("bill:history:for-user-id", "Please select a user identifier");
										  /*final boolean */valid = false;
										}
									} else {
									  /*final */Object __value = null;
										if (true == key.equals("bill-id")) {
											__value = new Integer(value);
										} else {
											__value = value;
										}
									  /*bill_*/history.put(key, __value);
										if (true == key.equals("bill-id")) {
										  /*final Integer */bill_id = new Integer(value);
										  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
										}
									}
								  //req.setAttrbiute("bill:history:"+key, value);
								} else {
									if (true == key.equals("for-user-id")) {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put("bill:history:for-user-id", "Please select a user identifier");
									  /*final boolean */valid = false;
									}
								}
							}
							if (true == valid) {
								for (final String key: new String[]{"bill-id","for-user-id"}) {
									final String value = (String)req.getParameter(false == key.equals("for-user-id") ? key : "bill:history:"+key);
									if (null != value) {
									  /*bill_*/history.put(key, value);
									}
								  //req.setAttrbiute("bill:history:"+key, value);
								}
								req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/bills?history&session-id="+session_id+"'\" />");
								req.removeAttribute("for:user#id");
							} else {
								if (null != req.getParameter("cancel")) {
									req.setAttribute("cancel", req.getParameter("cancel"));
								}
							}
						} else if (null != submit && (true == submit.equals("select") || true == submit.equals("search"))) {
							final java.util.Map<String, String[]> map = req.getParameterMap();
						  /*final */java.util.List<Integer> ids = null;
							if (null != ServletTool.get(session, "for:user#id")) {
								final String[] _user_ids = map.get("user-ids");
								if (null != _user_ids) {
									for (int i = 0; i < (null == _user_ids ? 0 : _user_ids.length); i++) {
										final String checked = req.getParameter("user$"+_user_ids[i]+"#checked");
										if (null != checked && true == checked.equals("on")) {
											if (null == ids) {
											  /*final java.util.List<Integer> */ids = new java.util.ArrayList<>();
											}
											ids.add(new Integer(_user_ids[i]));
										}
									}
								}
							  //final String for_user_id = (String)ServletTool.get(session, "for:user#id");
								// ..
							}
							if (true == "select".equals(submit)) {
								if (null != ServletTool.get(session, "for:user#id")) {
									// This is the scenario for create bill history
									try {
									  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
									} catch (final NumberFormatException e) {
									  // Silently ignore..
									}
									if (null != bill_id) {
									  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
									}
									if (null != ServletTool.get(session, "bill#history")) {
									  /*final java.util.Map<String, Object> bill_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#history");
									} else {
									  /*final java.util.Map<String, Object> bill_*/history = new java.util.HashMap<String, Object>();
										ServletTool.put(session, "bill#history", /*bill_*/history);
									}
									if (null != bill) {
									  /*bill_*/history.put("bill-id", (Integer)bill.get("bill-id"));
									}
								  /*final */Integer /*_user*/_id = null;
									if (null != ids && 1 <= ids.size()) {
									  /*final Integer _user*/_id = ids.toArray(new Integer[0])[0];
									}
									if (null != /*_user*/_id) {
										final java.util.Map<String, Object> user = Users.get(/*_user*/_id);
									  /*bill_*/history.put("for-user-id", user.get("user-id"));
										ServletTool.remove(session, "for:user#id");
									} else {
									  /*bill_*/history.remove("for-user-id");
									}
									if (null != req.getParameter("cancel")) {
										req.setAttribute("cancel", req.getParameter("cancel"));
									}
								}
							} else if (true == "search".equals(submit)) {
								if (null != req.getParameter("lines")) {
									try {
									  /*final Integer */bill_id = new Integer(req.getParameter("id"));
									} catch (final NumberFormatException e) {
									  // Silently ignore..
									}
									if (null != bill_id) {
									  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
									}
									if (null != req.getParameter("cancel")) {
										req.setAttribute("cancel", req.getParameter("cancel"));
									}
								}
							}
							if (null != ids) {
								if (null != req.getParameter("for-user-id")) {
									for (final Integer /*_user*/_id: ids) {
										req.setAttribute("user$"+/*_user*/_id+"#checked", true);
									}
								}
							}
						} else if (true == "refund".equals(submit)) {
						  /*final */Integer /*bill_*/history_id = null;
							try {
							  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore.. This is the scenario for update bill history
							}
							if (null != /*bill_*/history_id) {
							  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
							}
							// SquareUp specific implementation (move out to an adapter custom at some point)
							final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
						  /*final */String payment_id = null;
							if (null != user) {
							  /*final String */payment_id = (String)/*bill_*/history.get("sq:payment#id");
							}
							if (null != payment_id && 0 < payment_id.trim().length()) {
							  /*final */String refund_id = (String)/*bill_*/history.get("sq:refund#id");
								if (null == refund_id || 0 == refund_id.trim().length()) {
									// TODO: if/when we're using a card on file, we won't need an sq:nonce at this point - we can just use {user}.{%cards}.{sq:card#id} as the payment source-id
									final java.lang.String idempotency_key = dro.lang.String.generateString(45);
								  /*final String */refund_id = Bills.refundCustomerCard(/*bill_*/history_id, idempotency_key, payment_id);
									if (null != refund_id) {
										/*bill_*/history.put("sq:refund#id", refund_id);
										/*bill_*/history.remove("sq:payment#id");
										/*bill_*/history.remove("$payment");
									}
								}
							}
						}
					}
				} else {
				  /*final */Integer bill_id = null;
					if (true == "create".equals(submit)) { // create bill line history
					  /*final */boolean valid = true;
						try {
						  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service line
						}
						if (null != bill_id) {
						  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
						}
					  /*final */Integer /*bill_*/history_id = null;
						try {
						  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for bill lines history show/read ..?
						}
						if (null != /*bill_*/history_id) {
						  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
						}
						if (null != ServletTool.get(session, "bill#line#history")) {
						  /*final java.util.Map<String, Object> bill_*/line_history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#line#history");
						} else {
						  /*final java.util.Map<String, Object> bill_*/line_history = new java.util.HashMap<String, Object>();
							ServletTool.put(session, "bill#line#history", /*bill_*/line_history);
						}
						for (final String key: new String[]{"service-line-id","number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
							final String value = (String)req.getParameter(true == key.equals("service-line-id") ? key : "bill:line:history:"+key);
							if (null != value) {
								if (true == key.equals("service-line-id")) {
									if (0 < value.trim().length()) {
									  /*bill_*/line_history.put(key, value);
									} else {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put("service-line-id", "Please select a service line identifier");
									  /*final boolean */valid = false;
									}
								} else {
								  /*final */Object __value = null;
									if (true == key.startsWith("number-of-units@")) {
										if (0 < value.trim().length()) {
											__value = new Float(value);
										}
									} else {
										__value = value;
									}
									if (null != __value) {
									  /*bill_*/line_history.put(key, __value);
									}
								}
							  //req.setAttrbiute("bill:line:history:"+key, value);
							} else {
								if (true == key.equals("service-line-id")) {
									if (null == results) {
									  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
									}
									results.put("bill:line:history:for-line-id", "Please select a service line identifier");
								  /*final boolean */valid = false;
								}
							}
						}
						if (true == valid) {
						  /*final java.util.Map<String, Object> bill_*/line_history = new dao.util.HashMap<String, Object>();
							for (final String key: new String[]{"service-line-id","number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
								final String value = (String)req.getParameter(true == key.equals("service-line-id") ? key : "bill:line:history:"+key);
								if (null != value) {
								  /*final */Object __value = null;
									if (true == key.startsWith("number-of-units@")) {
										if (0 < value.trim().length()) {
											__value = new Float(value);
										}
									} else {
										__value = value;
									}
									if (null != __value) {
									  /*bill_*/line_history.put(key, __value);
									}
								  //req.setAttrbiute("bill:history:"+key, value);
								}
							}
						  /*final java.util.List<java.util.Map<String, Object>> bill_*/lines_history = (java.util.List<java.util.Map<String, Object>>)/*bill_*/history.get("@lines");
							if (null == /*bill_*/lines_history) {
							  /*final java.util.List<java.util.Map<String, Object>> bill_*/lines_history = new dao.util.ArrayList<java.util.Map<String, Object>>();
							  /*bill_*/history.put("@lines", /*bill_*/lines_history);
							}
						  /*bill_*/line_history.put("line-id", /*bill_*/lines_history.size());
						  /*bill_*/lines_history.add(/*bill_*/line_history);
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/bill?lines&history&session-id="+session_id+"&bill-id="+bill_id+"&id="+history_id+"&cancel=bills-history'\" />");
							req.removeAttribute("for:line#id");
							ServletTool.remove(session, "bill#line#history");
						} else {
							if (null != req.getParameter("cancel")) {
								req.setAttribute("cancel", req.getParameter("cancel"));
							}
						}
					} else if (true == "update".equals(submit)) { // update bill line history
					  /*final */boolean valid = true;
						try {
						  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore..
						}
						if (null != bill_id) {
						  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
						}
					  /*final */Integer /*bill_*/history_id = null;
						try {
						  /*final Integer bill_*/history_id = new Integer(req.getParameter("history-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore..
						}
						if (null != /*bill_*/history_id) {
						  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
						}
					  /*final */Integer /*bill_history_*/line_id = null;
						try {
						  /*final Integer bill_history_*/line_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for update bill history
						}
						if (null != /*bill_history_*/line_id) {
						  /*final java.util.Map<String, Object> bill_*/line_history = Bills.getLineHistory(/*bill_*/history_id, /*bill_history_*/line_id);
						}
						for (final String key: new String[]{"service-line-id","number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
							final String value = (String)req.getParameter(true == key.equals("service-line-id") ? key : "bill:line:history:"+key);
							if (null != value) {
							  /*final */Object __value = null;
								if (true == key.startsWith("number-of-units@")) {
									if (0 < value.trim().length()) {
										__value = new Float(value);
									}
								} else {
									__value = value;
								}
								if (null != __value) {
								  /*bill_*/line_history.put(key, __value);
								}
							  //req.setAttrbiute("bill:history:"+key, value);
							} else {
								if (true == key.equals("service-line-id")) {
									if (null == results) {
									  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
									}
									results.put("service-line-id", "Please select a service line identifier");
								  /*final boolean */valid = false;
								}
							}
						}
						if (true == valid) {
							for (final String key: new String[]{"service-line-id","number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
								final String value = (String)req.getParameter(true == key.equals("service-line-id") ? key : "bill:line:history:"+key);
							  /*final */Object __value = null;
								if (true == key.startsWith("number-of-units@")) {
									if (0 < value.trim().length()) {
										__value = new Float(value);
									}
								} else {
									__value = value;
								}
								if (null != __value) {
								  /*bill_*/line_history.put(key, __value);
								}
							  //req.setAttrbiute("bill:line:history:"+key, value);
							}
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/bill?lines&history&session-id="+session_id+"&bill-id="+bill_id+"&id="+history_id+"&cancel=bills-history'\" />");
							req.removeAttribute("for:line#id");
						} else {
							if (null != req.getParameter("cancel")) {
								req.setAttribute("cancel", req.getParameter("cancel"));
							}
						}
					} else if (null != submit && (true == submit.equals("select") || true == submit.equals("search"))) {
						final java.util.Map<String, String[]> map = req.getParameterMap();
					  /*final */java.util.List<Integer> ids = null;
						if (null != ServletTool.get(session, "for:line#id")) {
							final String[] _line_ids = map.get("service-line-ids");
							if (null != _line_ids) {
								for (int i = 0; i < (null == _line_ids ? 0 : _line_ids.length); i++) {
									final String checked = req.getParameter("service-line$"+_line_ids[i]+"#checked");
									if (null != checked && true == checked.equals("on")) {
										if (null == ids) {
										  /*final java.util.List<Integer> */ids = new java.util.ArrayList<>();
										}
										ids.add(new Integer(_line_ids[i]));
									}
								}
							}
						  //final String for_line_id = (String)ServletTool.get(session, "for:line#id");
							// ..
						}
						if (true == "select".equals(submit)) {
							if (null != ServletTool.get(session, "for:line#id")) {
								// This is the scenario for create bill line history
								try {
								  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
								if (null != bill_id) {
								  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
								}
							  /*final */Integer /*bill_*/history_id = null;
								if (false == "create".equals(mode)) {
									try {
									  /*final Integer bill_*/history_id = new Integer(req.getParameter("history-id"));
									} catch (final NumberFormatException e) {
									  // Silently ignore..
									}
								} else {
									try {
									  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
									} catch (final NumberFormatException e) {
									  // Silently ignore..
									}
								}
								if (null != /*bill_*/history_id) {
								  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
								}
								if (false == "create".equals(mode)) {
								  /*final */Integer /*bill_history_*/line_id = null;
									try {
									  /*final Integer bill_history_*/line_id = new Integer(req.getParameter("id"));
									} catch (final NumberFormatException e) {
									  // Silently ignore..
									}
									if (null != bill_id && null != /*bill_history_*/line_id) {
									  /*final java.util.Map<String, Object> bill_*/line_history = Bills.getLineHistory(/*bill_*/history_id, line_id);
									}
								} else {
									if (null != ServletTool.get(session, "bill#line#history")) {
									  /*final java.util.Map<String, Object> bill_*/line_history = (java.util.Map<String, Object>)ServletTool.get(session, "bill#line#history");
									} else {
									  /*final java.util.Map<String, Object> bill_*/line_history = new java.util.HashMap<String, Object>();
										ServletTool.put(session, "bill#line#history", /*bill_*/line_history);
									}
								}
							  /*final */Integer /*_user*/_id = null;
								if (null != ids && 1 <= ids.size()) {
								  /*final Integer _service_line*/_id = ids.toArray(new Integer[0])[0];
								}
								if (null != /*_service_line*/_id) {
									final java.util.Map<String, Object> /*service_*/line = Services.getLineById(/*_service_line*/_id);
								  /*bill_*/line_history.put("service-line-id", /*service_*/line.get("line-id"));
								} else {
								  /*bill_*/line_history.remove("service-line-id");
								}
								req.removeAttribute("for:line#id");
								ServletTool.remove(session, "for:line#id");
								if (null != req.getParameter("cancel")) {
									req.setAttribute("cancel", req.getParameter("cancel"));
								}
							}
						} else if (true == "search".equals(submit)) {
							if (null != req.getParameter("line")) {
								try {
								  /*final Integer */bill_id = new Integer(req.getParameter("bill-id"));
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
								if (null != bill_id) {
								  /*final java.util.Map<String, Object> */bill = Bills.get(bill_id);
								}
							  /*final */Integer /*bill_*/history_id = null;
								try {
								  /*final Integer bill_*/history_id = new Integer(req.getParameter("id"));
								} catch (final NumberFormatException e) {
								  // Silently ignore..
								}
								if (null != /*bill_*/history_id) {
								  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
								}
								if (null != req.getParameter("cancel")) {
									req.setAttribute("cancel", req.getParameter("cancel"));
								}
							}
						}
						if (null != ids) {
							if (null != req.getParameter("for-line-id")) {
								for (final Integer /*_service_line*/_id: ids) {
									req.setAttribute("service-line$"+/*_service_line*/_id+"#checked", true);
								}
							}
						}
					}
				}
			}
		}
		
		if (null != results && 0 < results.size()) {
			req.setAttribute("results", results);
		}
		
		if (null != mode) {
			req.setAttribute("mode", mode);
		}
		
		if (null != bill) {
			req.setAttribute("bill", bill);
		} else if (null == req.getParameter("lines") && null == req.getParameter("line") && null == req.getParameter("history")) {
			req.setAttribute("bill", true);
		}
		if (null != /*bill_*/history) {
			req.setAttribute("bill#history", /*bill_*/history);
		} else if (null == req.getParameter("line") && null != req.getParameter("history")) {
			req.setAttribute("bill#history", true);
		}
		if (null != /*bill_*/lines_history) {
			req.setAttribute("bill#lines#history", /*bill_*/lines_history);
		} else if (null != req.getParameter("lines") && null != req.getParameter("history")) {
			req.setAttribute("bill#lines#history", true);
		}
		if (null != /*bill_*/line_history) {
			req.setAttribute("bill#line#history", /*bill_*/line_history);
		} else if (null != req.getParameter("line") && null != req.getParameter("history")) {
			req.setAttribute("bill#line#history", true);
		}
		
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}