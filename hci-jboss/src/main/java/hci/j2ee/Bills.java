package hci.j2ee;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import hci.pdf.Column;
import hci.pdf.PDFTableGenerator;
import hci.pdf.Table;
import hci.pdf.TableBuilder;

@SuppressWarnings("unchecked")
public class Bills {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> bills;// = null;
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> history;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Bills.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */bills = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("bills", java.util.List.class);
		if (null == bills) {
			bills = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("bill-id", 0);
						  //put("for-user-id", "admin");
							put("tax-gst", 0.05f);
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(bills, "bills");
		}
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */history = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("bills#history", java.util.List.class);
		if (null == history) {
			history = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("bill-history-id", 0);
							put("bill-id", 0);
							put("for-user-id", "admin");
							put("@lines", new dao.util.ArrayList<java.util.HashMap<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", 0);
											put("service-line-id", "1:email");
											put("number-of-units@reserved", 10.0f);
											put("number-of-units@charged", 2.0f);
										}
									});
									add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", 1);
											put("service-line-id", "1:sms-text");
											put("number-of-units@reserved", 10.0f);
											put("number-of-units@charged", 1.0f);
										}
									});
								}
							});
						  //put("$state", "open");
						  //put("$payment", "paid");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(history, "bills#history");
		}
	}
	
	public static java.util.Map<String, Object> get(final Integer /*bill_*/id) {
	  /*final */java.util.Map<String, Object> bill = null;
		if (null != /*bill_*/id) {
			final int intValue = /*bill_*/id.intValue();
			for (final java.util.Map<String, Object> __bill: bills) {
				if (intValue == ((Integer)__bill.get("$id")).intValue()) {
					bill = __bill;
					break;
				}
			}
		}
		return bill;
	}
	public static java.util.Map<String, Object> getById(final Integer /*bill_*/id) {
	  /*final */java.util.Map<String, Object> bill = null;
		if (null != /*bill_*/id) {
			final int intValue = /*bill_*/id.intValue();
			for (final java.util.Map<String, Object> __bill: bills) {
				if (intValue == ((Integer)__bill.get("bill-id")).intValue()) {
					bill = __bill;
					break;
				}
			}
		}
		return bill;
	}
	
	public static java.util.List<java.util.Map<String, Object>> getHistoryForBillId(final Integer /*bill_*/id) {
	  /*final */java.util.List<java.util.Map<String, Object>> /*bill_*/history_list = null;
		final java.util.Map<String, Object> bill = Bills.get(/*bill_*/id);
		if (null != bill) {
			final Integer bill_id = (Integer)bill.get("bill-id");
			for (final java.util.Map<String, Object> /*bill_*/history: Bills.history) {
				if (null != /*bill_*/history.get("bill-id") && /*bill_*/history.get("bill-id").equals(bill_id)) {
					if (null == /*bill_*/history_list) {
					  /*final java.util.List<java.util.Map<String, Object>> bill_*/history_list = new java.util.ArrayList<java.util.Map<String, Object>>();
					}
				  /*bill_*/history_list.add(/*bill_*/history);
				}
			}
		}
		return /*bill_*/history_list;
	}
	public static java.util.Map<String, Object> getHistory(final Integer /*bill_*/history_id) {
	  /*final */java.util.Map<String, Object> /*bill_*/history = null;
		if (null != /*bill_*/history_id) {
			for (final java.util.Map<String, Object> /*bill*/_history: Bills.history) {
				final Integer /*bill*/_history_id = (Integer)/*bill*/_history.get("$id");
				if (null != /*bill*/_history_id && /*bill*/_history_id.equals(/*bill_*/history_id)) {
				  /*bill_*/history = /*bill*/_history;
				}
			}
		}
		return /*bill_*/history;
	}
	public static java.util.Map<String, Object> getHistoryById(final Integer /*bill_*/history_id) {
	  /*final */java.util.Map<String, Object> /*bill_*/history = null;
		if (null != /*bill_*/history_id) {
			for (final java.util.Map<String, Object> /*bill*/_history: Bills.history) {
				final Integer /*bill*/_history_id = (Integer)/*bill*/_history.get("bill-history-id");
				if (null != /*bill*/_history_id && /*bill*/_history_id.equals(/*bill_*/history_id)) {
				  /*bill_*/history = /*bill*/_history;
				}
			}
		}
		return /*bill_*/history;
	}
	public static java.util.Map<String, Object> getLineHistory(final Integer /*bill_*/history_id, final Integer /*bill_history_*/line_id) {
	  /*final */java.util.Map<String, Object> /*bill_*/line_history = null;
		if (null != /*bill_*/history_id) {
		  /*final */int intValue = /*bill_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*bill*/_history: Bills.history) {
				if (intValue == ((Integer)/*bill*/_history.get("$id")).intValue()) {
				  /*final int */intValue = /*bill_history_*/line_id.intValue();
					final java.util.List<java.util.Map<String, Object>> /*bill*/_lines_history = (java.util.List<java.util.Map<String, Object>>)/*bill*/_history.get("@lines");
					for (final java.util.Map<String, Object> /*bill*/_line_history: /*bill*/_lines_history) {
						if (intValue == ((Integer)/*bill*/_line_history.get("$id")).intValue()) {
						  /*bill_*/line_history = /*bill*/_line_history;
							break;
						}
					}
				}
			}
		}
		return /*bill_*/line_history;
	}
	
	public static void remove(final Integer /*bill_*/id) {
		// TODO: check dependencies (can't delete a bill/line if assigned to role/user/group.. etc.
		if (null != /*bill_*/id) {
		  /*final */Integer __index_to_remove = null;
			for (int i = 0; i < bills.size(); i++) {
				final java.util.Map<String, Object> bill = bills.get(i);
				if (null != bill && true == /*bill_*/id.equals((Integer)bill.get("$id"))) {
				  /*final java.util.List<java.util.Map<String, Object>> *//*bill_*//*lines = (java.util.List<java.util.Map<String, Object>>)bill.get("@lines");
					for (int j = *//*bill_*//*lines.size()-1; j >= 0; j--) {
					*//*bill_*//*lines.remove(j);
					}*/
					__index_to_remove = new Integer(i);
					break;
				}
			}
			if (null != __index_to_remove) {
				bills.remove(__index_to_remove.intValue());
			}
		}
	}
	public static void removeHistory(final Integer /*bill_*/history_id) {
		// TODO: check dependencies (can't delete a bill-/line-history if assigned to role/user/group.. etc.
		if (null != /*bill_*/history_id) {
		  /*final */Integer __index_to_remove = null;
			for (int i = 0; i < Bills.history.size(); i++) {
				final java.util.Map<String, Object> /*bill_*/history = Bills.history.get(i);
				if (null != /*bill_*/history && true == /*bill_*/history_id.equals((Integer)/*bill_*/history.get("$id"))) {
				  /*final java.util.List<java.util.Map<String, Object>> *//*bill_*//*lines_history = (java.util.List<java.util.Map<String, Object>>)*//*bill_*//*history.get("@lines");
					for (int j = *//*bill_*//*lines_history.size()-1; j >= 0; j--) {
					*//*bill_*//*lines_history.remove(j);
					}*/
					__index_to_remove = new Integer(i);
					break;
				}
			}
			if (null != __index_to_remove) {
				Bills.history.remove(__index_to_remove.intValue());
			}
		}
	}
	
	public static Integer reserveBill(final String bill_id, final String user_id, final String service_line_id, final Float number_of_units) {
/**/  /*final */java.util.Map<java.lang.String, java.lang.Object> bill = bills.get(Integer.parseInt(bill_id));
		
	  /*final */java.util.Map<java.lang.String, java.lang.Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
			service.hashCode(); // TODO: can user be charged for this service? BEWARE recursion
		} else {
			throw new IllegalStateException();
		}
		
	  /*final */java.util.Map<java.lang.String, java.lang.Object> history = null;
	  /*final */java.util.Map<java.lang.String, java.lang.Object> line = null;
	  /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> lines = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __history: Bills.history) {
			final String for_user_id = (String)__history.get("for-user-id");
			if (null != for_user_id && true == for_user_id.equals(user_id)) {
			  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>>*/ lines = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)__history.get("@lines");
				for (final java.util.Map<java.lang.String, java.lang.Object> __line: lines) {
					final String __service_line_id = (String)__line.get("service-line-id");
					if (null != service_line_id && true == __service_line_id.equals(service_line_id)) {
						line = __line;
						break;
					}
				}
				if (null != line) {
					history = __history;
					break;
				}
			}
		}
		
		if (null == history) {
/**/	  /*final java.util.Map<java.lang.String, java.lang.Object> */history = new dao.util.HashMap<>();
			history.put("bill-history-id", new Integer(Bills.history.size()));
			history.put("bill-id", bill.get("bill-id"));
			history.put("for-user-id", user_id);
			Bills.history.add(history);
		}
		if (null == lines) {
/**/	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */lines = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>();
			history.put("@lines", lines);
		}
		if (null == line) {
/**/	  /*final java.util.Map<java.lang.String, java.lang.Object> */line = new dao.util.HashMap<>();
			line.put("line-id", lines.size());
			line.put("service-line-id", service_line_id);
			line.put("number-of-units:reserved", number_of_units);
			lines.add(line);
		} else {
		  /*final */Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
			// Note: 'fix' for -0.0f;
			final float __number_of_units = number_of_units.floatValue() == -0.0f ? 0.0f : number_of_units.floatValue();
			if (null == number_of_units_reserved) {
				line.put("number-of-units:reserved", new Float(__number_of_units));
			} else {
				line.put("number-of-units:reserved", new Float(__number_of_units+number_of_units_reserved.floatValue()));
			}
		}
		return (Integer)history.get("bill-history-id");
	}
	
	// SquareUp specific implementation (move out to an adapter custom at some point)
	public static String chargeCustomerCard(final Integer /*bill_*/history_id, final String idempotency_key, final String customer_id, final String /*customer_*/card_id_or_nonce) {
	  /*final */java.util.Map<String, Object> /*bill_*/history = null;
		if (null != /*bill_*/history_id) {
		  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
		}
	  /*final */Float cents_amount = null;
		if (null != /*bill_*/history) {
			cents_amount = new Float(((Float)/*bill_*/history.get("total")).floatValue()*100.0f);
		}
	  /*final */dro.util.Properties p = null;
		try {
		  /*final dro.util.Properties */p = new dro.util.Properties(Bills.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		final dro.lang.String data = new dro.lang.String();
		if (null != p && null != idempotency_key & null != cents_amount && null != /*customer_*/card_id_or_nonce && null != customer_id) {
			final dro.lang.Integer status = new dro.lang.Integer();
			final String __url = dto.http.Adapter.class.getName()+"#url";
			final String url = p.property(dto.http.Adapter.class.getName()+"#url");
			// https://developer.squareup.com/reference/square/payments-api/create-payment
			try {
				final int /*cents*/_amount = cents_amount.intValue();
				final java.util.Map<String, Object> /*bill*/_history = /*bill_*/history;
				new dto.http.Adapter()
					.properties(p)
					.header("Square-Version", "2021-03-17")
					.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
					.header("Content-Type", "application/json")
					.post(
						p.property(__url, url+"/payments")
						 .property(__url), new JSONObject(){
							private static final long serialVersionUID = 0L;
							{
								put("idempotency_key", idempotency_key);
								put("amount_money", new JSONObject(){
									private static final long serialVersionUID = 0L;
									{
										put("amount", /*cents*/_amount);
										put("currency", "CAD");
									}
								});
								put("source_id", /*customer_*/card_id_or_nonce);
								put("autocomplete", true);
								put("customer_id", customer_id);
							  //put("location_id", "..");
								put("reference_id", ((Integer)/*bill*/_history.get("bill-history-id")).toString());
							  //put("note", "..");
							}
						}.toJSONString(),
						dto.http.Adapter.Return.Adapter
					)
					.status(status)
					.data(data)
				;
			} catch (final ClientProtocolException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		  /*{
			  "payment": {
			    "id": "GQTFp1ZlXdpoW4o6eGiZhbjosiDFf",
			    "created_at": "2019-07-10T13:23:49.154Z",
			    "updated_at": "2019-07-10T13:23:49.446Z",
			    "amount_money": {
			      "amount": 200,
			      "currency": "USD"
			    },
			    "app_fee_money": {
			      "amount": 10,
			      "currency": "USD"
			    },
			    "total_money": {
			      "amount": 200,
			      "currency": "USD"
			    },
			    "approved_money": {
			      "amount": 200,
			      "currency": "USD"
			    },
			    "status": "COMPLETED",
			    "source_type": "CARD",
			    "card_details": {
			      "status": "CAPTURED",
			      "card": {
			        "card_brand": "VISA",
			        "last_4": "1111",
			        "exp_month": 7,
			        "exp_year": 2026,
			        "fingerprint": "sq-1-TpmjbNBMFdibiIjpQI5LiRgNUBC7u1689i0TgHjnlyHEWYB7tnn-K4QbW4ttvtaqXw",
			        "card_type": "DEBIT",
			        "prepaid_type": "PREPAID",
			        "bin": "411111"
			      },
			      "entry_method": "ON_FILE",
			      "cvv_status": "CVV_ACCEPTED",
			      "avs_status": "AVS_ACCEPTED",
			      "auth_result_code": "nsAyY2",
			      "statement_description": "SQ *MY MERCHANT",
			      "card_payment_timeline": {
			        "authorized_at": "2019-07-10T13:23:49.234Z",
			        "captured_at": "2019-07-10T13:23:49.446Z"
			      }
			    },
			    "location_id": "XTI0H92143A39",
			    "order_id": "m2Hr8Hk8A3CTyQQ1k4ynExg92tO3",
			    "reference_id": "123456",
			    "note": "Brief description",
			    "customer_id": "RDX9Z4XTIZR7MRZJUXNY9HUK6I",
			    "receipt_number": "GQTF",
			    "receipt_url": "https://squareup.com/receipt/preview/GQTFp1ZlXdpoW4o6eGiZhbjosiDFf",
			    "version_token": "H8Vnk5Z11SKcueuRti79jGpszSEsSVdhKRrSKCOzILG6o"
			  }
			}*/
		}
	  /*final */String payment_id = null;
		if (null != data && null != data.value()) {
			try {
				final Object o = new JSONParser().parse(data.value());
				if (null != o) {
					final JSONObject jo = (JSONObject)o;
					if (null != jo) {
						final JSONObject payment = (JSONObject)jo.get("payment");
						if (null != payment) {
						  /*final String */payment_id = (String)payment.get("id");
						}
					}
				}
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}
		return payment_id;
	}
	// SquareUp specific implementation (move out to an adapter custom at some point)
	public static String refundCustomerCard(final Integer /*bill_*/history_id, final String idempotency_key, final String payment_id) {
	  /*final */java.util.Map<String, Object> /*bill_*/history = null;
		if (null != /*bill_*/history_id) {
		  /*final java.util.Map<String, Object> bill_*/history = Bills.getHistory(/*bill_*/history_id);
		}
	  /*final */Float cents_amount = null;
		if (null != /*bill_*/history) {
			cents_amount = new Float(((Float)/*bill_*/history.get("total")).floatValue()*100.0f);
		}
	  /*final */dro.util.Properties p = null;
		try {
		  /*final dro.util.Properties */p = new dro.util.Properties(Bills.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		final dro.lang.String data = new dro.lang.String();
		if (null != p) {
			final dro.lang.Integer status = new dro.lang.Integer();
			final String __url = dto.http.Adapter.class.getName()+"#url";
			final String url = p.property(dto.http.Adapter.class.getName()+"#url");
			// https://developer.squareup.com/reference/square/payments-api/create-payment
			try {
				final int /*cents*/_amount = cents_amount.intValue();
			  //final java.util.Map<String, Object> /*bill*/_history = /*bill_*/history;
				new dto.http.Adapter()
					.properties(p)
					.header("Square-Version", "2021-03-17")
					.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
					.header("Content-Type", "application/json")
					.post(
						p
						//property("payment-id", payment_id)
						//property(__url, url+"/payments/cancel")
						//property(__url, url+"/payments/${payment-id}/cancel")
						 .property(__url, url+"/refunds")
						 .property(__url), new JSONObject(){
							private static final long serialVersionUID = 0L;
							{
								put("idempotency_key", idempotency_key);
								put("payment_id", payment_id);
								put("amount_money", new JSONObject(){
									private static final long serialVersionUID = 0L;
									{
										put("amount", /*cents*/_amount);
										put("currency", "CAD");
									}
								});
							}
						}.toJSONString(),
						dto.http.Adapter.Return.Adapter
					)
					.status(status)
					.data(data)
				;
			} catch (final ClientProtocolException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	  /*final */String refund_id = null;
		if (null != data && null != data.value()) {
			try {
				final Object o = new JSONParser().parse(data.value());
				if (null != o) {
					final JSONObject jo = (JSONObject)o;
					if (null != jo) {
						final JSONObject refund = (JSONObject)jo.get("refund");
						if (null != refund) {
						  /*final String */refund_id = (String)refund.get("id");
						}
					}
				}
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}
		return refund_id;
	}
	
	public static Float chargeBill(final Integer bill_history_id, final String user_id, final String service_line_id, final Float number_of_units) {
	  /*final */Float number_of_units_remaining = null;
	  /*final */java.util.Map<java.lang.String, java.lang.Object> history = null;
	  /*final */java.util.Map<java.lang.String, java.lang.Object> line = null;
	  /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> lines = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __history: Bills.history) {
			if (true == __history.get("bill-history-id").equals(bill_history_id)) {
			  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */lines = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)__history.get("@lines");
				for (final java.util.Map<java.lang.String, java.lang.Object> __line: lines) {
					final String __service_line_id = (String)__line.get("service-line-id");
					if (null != service_line_id && true == __service_line_id.equals(service_line_id)) {
						line = __line;
						break;
					}
				}
				if (null != line) {
					history = __history;
					break;
				}
			}
		}
		if (null != history) {
			if (null != line) {
				if (null != number_of_units) {
				  /*final */Float number_of_units_charged = (Float)line.get("number-of-units:charged");
					if (null == number_of_units_charged) {
						line.put("number-of-units:charged", new Float(number_of_units.floatValue()));
					} else {
						line.put("number-of-units:charged", new Float(number_of_units_charged.floatValue()+number_of_units.floatValue()));
					}
				  /*final */Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
					if (null == number_of_units_reserved) {
						number_of_units_reserved = new Float(0.0f);
					}
				  /*final Float */number_of_units_remaining = new Float(number_of_units_reserved.floatValue()-number_of_units.floatValue());
					if (number_of_units_remaining.floatValue() > 0.0f) {
						line.put("number-of-units:reserved", number_of_units_remaining);
					} else /*if (number_of_units_remaining.floatValue() == 0.0f) */{ // Note: let's make it okay, to be charged for more than was reserved, but we might be interested in a future scenario
					  //line.put("number-of-units:reserved", new Float(0.0f));
						line.remove("number-of-units:reserved");
				  /*} else {
						throw new IllegalStateException();*/
					}
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
			// TODO: somewhere, somehow, figure amount/total to bill:
			// SquareUp specific implementation (move out to an adapter custom at some point)
		  /*final */dro.util.Properties p = null;
			try {
			  /*final dro.util.Properties */p = new dro.util.Properties(Bills.class);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
			final java.util.Map<String, Object> user = null;
		  /*final */String customer_id = null;
			if (null != p) {
				if (null != user) {
				  /*final String */customer_id = (String)user.get("sq:customer#id");
				}
			}
		  /*final */String /*customer_*/card_id = null;
			if (null != customer_id) {
				final java.util.List<java.util.Map<String, Object>> /*customer*/_cards = (java.util.List<java.util.Map<String, Object>>)user.get("%cards");
				for (final java.util.Map<String, Object> /*customer*/_card: /*customer*/_cards) {
					final String /*customer*/_card_id = (String)/*customer*/_card.get("sq:card#id");
					if (null != /*customer*/_card_id && 0 < /*customer*/_card_id.trim().length()) {
					  /*customer_*/card_id = /*customer*/_card_id;
						break; // TODO: allow for more than one card in future..
					}
				}
			}
			if (null != /*customer*/card_id) {
				final dro.lang.String data = new dro.lang.String();
				final dro.lang.Integer status = new dro.lang.Integer();
				final String __url = dto.http.Adapter.class.getName()+"#url";
				final String url = p.property(dto.http.Adapter.class.getName()+"#url");
				// https://developer.squareup.com/reference/square/payments-api/create-payment
				try {
					final java.lang.String idempotency_key = dro.lang.String.generateString(45);
					final String /*customer*/_id = customer_id;
					final String /*customer*/_card_id = /*customer_*/card_id;
					new dto.http.Adapter()
						.properties(p)
						.header("Square-Version", "2021-03-17")
						.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
						.header("Content-Type", "application/json")
						.post(
							p.property(__url, url+"/payments")
							 .property(__url), new JSONObject(){
								private static final long serialVersionUID = 0L;
								{
									put("idempotency_key", idempotency_key);
									put("amount_money", new JSONObject(){
										private static final long serialVersionUID = 0L;
										{
											put("amount", 100);
											put("currency", "CAD");
										}
									});
									put("source_id", /*customer*/_card_id);
									put("autocomplete", true);
									put("customer_id", /*customer*/_id);
								  //put("location_id", "..");
								  //put("reference_id", "..");
								  //put("note", "..");
								}
							}.toJSONString(),
							dto.http.Adapter.Return.Adapter
						)
						.status(status)
						.data(data)
					;
				} catch (final ClientProtocolException e) {
					throw new RuntimeException(e);
				} catch (final IOException e) {
					throw new RuntimeException(e);
				}
			  /*{
				  "payment": {
				    "id": "GQTFp1ZlXdpoW4o6eGiZhbjosiDFf",
				    "created_at": "2019-07-10T13:23:49.154Z",
				    "updated_at": "2019-07-10T13:23:49.446Z",
				    "amount_money": {
				      "amount": 200,
				      "currency": "USD"
				    },
				    "app_fee_money": {
				      "amount": 10,
				      "currency": "USD"
				    },
				    "total_money": {
				      "amount": 200,
				      "currency": "USD"
				    },
				    "approved_money": {
				      "amount": 200,
				      "currency": "USD"
				    },
				    "status": "COMPLETED",
				    "source_type": "CARD",
				    "card_details": {
				      "status": "CAPTURED",
				      "card": {
				        "card_brand": "VISA",
				        "last_4": "1111",
				        "exp_month": 7,
				        "exp_year": 2026,
				        "fingerprint": "sq-1-TpmjbNBMFdibiIjpQI5LiRgNUBC7u1689i0TgHjnlyHEWYB7tnn-K4QbW4ttvtaqXw",
				        "card_type": "DEBIT",
				        "prepaid_type": "PREPAID",
				        "bin": "411111"
				      },
				      "entry_method": "ON_FILE",
				      "cvv_status": "CVV_ACCEPTED",
				      "avs_status": "AVS_ACCEPTED",
				      "auth_result_code": "nsAyY2",
				      "statement_description": "SQ *MY MERCHANT",
				      "card_payment_timeline": {
				        "authorized_at": "2019-07-10T13:23:49.234Z",
				        "captured_at": "2019-07-10T13:23:49.446Z"
				      }
				    },
				    "location_id": "XTI0H92143A39",
				    "order_id": "m2Hr8Hk8A3CTyQQ1k4ynExg92tO3",
				    "reference_id": "123456",
				    "note": "Brief description",
				    "customer_id": "RDX9Z4XTIZR7MRZJUXNY9HUK6I",
				    "receipt_number": "GQTF",
				    "receipt_url": "https://squareup.com/receipt/preview/GQTFp1ZlXdpoW4o6eGiZhbjosiDFf",
				    "version_token": "H8Vnk5Z11SKcueuRti79jGpszSEsSVdhKRrSKCOzILG6o"
				  }
				}*/
			}
		} else {
			throw new IllegalStateException();
		}
		return number_of_units_remaining;
	}
	
	public static String billFormHistoryPDF(final Integer bill_history_id) {
		final java.util.Map<String, Object> history = Bills.history.get(bill_history_id.intValue());
	///*final */Integer bill_history_id = (Integer)history.get("bill-history-id")
	  /*final */String pdfFilename = (String)history.get("bill-history-pdf");
//if (null == pdfFilename) {
		  /*final */PDDocument pdf = new PDDocument();
			// https://pdfbox.apache.org/1.8/cookbook/workingwithmetadata.html
			// https://pdfbox.apache.org/index.html
			// https://github.com/eduardohl/Paginated-PDFBox-Table-Sample/blob/master/src/pdftablesample/PDFSample.java
			final PDDocumentInformation info = new PDDocumentInformation();
			info.setTitle("IT From Blighty - Bill");
			info.setSubject("Bill # 123");
			info.setKeywords("Bill, Service, etc."); // service-ids
			info.setAuthor("IT From Blighty Billing");
			info.setCreator("IT From Blighty");
			info.setCreationDate(Calendar.getInstance());
			pdf.setDocumentInformation(info);
		  /*final PDPage page = new PDPage();
			pdf.addPage(page);*/
		  /*final *//*PDPageContentStream cs = null;
			if (null != pdf) {
				try {
					if (null == cs) {
					*//*final PDPageContentStream *//*cs = new PDPageContentStream(pdf, page);
					}
					final PDFont font = PDType1Font.TIMES_ROMAN;
					cs.beginText();
					cs.setFont(font, 15.0f);
					cs.newLineAtOffset(25, 700);
					cs.showText("IT From Blighty - Bill");
					cs.endText();
				} catch (final IOException e) {
					pdf = null;
				  //throw new RuntimeException(e);
					e.printStackTrace(System.err);
				}
			}
			if (null != pdf) {
				try {
					if (null == cs) {
					*//*final PDPageContentStream *//*cs = new PDPageContentStream(pdf, page);
					}
					final PDImageXObject pdix = PDImageXObject.createFromFile(
						String.join(File.separator, new String[]{
							"C:","Users","Alex.Russell","itfromblighty.ca","temporary","QRCode.jpg"
						}), pdf
					);
					cs.drawImage(pdix, 70, 250);
				} catch (final IOException e) {
					pdf = null;
				  //throw new RuntimeException(e);
					e.printStackTrace(System.err);
				}
			}
			if (null != cs) {
				try {
					cs.close();
				} catch (final IOException e) {
					pdf = null;
				  //throw new RuntimeException(e);
					e.printStackTrace(System.err);
				}
			}*/
			final Integer bill_id = (Integer)history.get("bill-id");
			final java.util.Map<String, Object> bill = Bills.bills.get(bill_id.intValue());
		  /*final */Float tax_gst = null;
			if (null != bill) {
			  /*final Float */tax_gst = (Float)bill.get("tax-gst");
			} else {
			}
			if (null != tax_gst) {
				final String __0 = new java.text.DecimalFormat("#,##0.##").format(100.0f*tax_gst)+"%";
			}
		  //final String for_user_id = (String)history.get("for-user-id");
		  //final String state = (String)history.get("state");
			final java.util.List<java.util.Map<String, Object>> lines = (java.util.List<java.util.Map<String, Object>>)history.get("@lines");
			List<Column> columns = new ArrayList<Column>();
			columns.add(new Column("service-line-id", 120));
			columns.add(new Column("$/uom", 120));
			columns.add(new Column("units:reserved", 120));
			columns.add(new Column("units:charged", 120));
			columns.add(new Column("$", 80));
			final String[][] content = new String[3+lines.size()][5];
		  /*final */float total = 0.0f;
			int col = 1, row = 1;
			for (final java.util.Map<String, Object> line: lines) {
				col = 1;
				final String /*service*/_line_id = (String)line.get("service-line-id");
				content[-1+row][-1+col] = /*service*/_line_id;
				col++;
				final Float number_of_units_reserved = null == line.get("number-of-units@reserved") ? 0.0f : (Float)line.get("number-of-units@reserved");
				final Float number_of_units_charged = null == line.get("number-of-units@charged") ? 0.0f : (Float)line.get("number-of-units@charged");
				final java.util.Map<String, Object> /*service*/_line = hci.j2ee.Services.findServiceLineById(/*service*/_line_id);
				final String unit_of_measure = (String)/*service*/_line.get("unit-of-measure");
				final Float price_per_unit = (Float)/*service*/_line.get("price-per-unit");
				content[-1+row][-1+col] = new java.text.DecimalFormat("#,##0.00").format(price_per_unit)+"/"+unit_of_measure;
				col++;
				content[-1+row][-1+col] = new java.text.DecimalFormat("#,##0.##").format(number_of_units_reserved);
				col++;
				content[-1+row][-1+col] = new java.text.DecimalFormat("#,##0.##").format(number_of_units_charged);
				col++;
			  //total += number_of_units_reserved.floatValue()*price_per_unit.floatValue();
				content[-1+row][-1+col] = new java.text.DecimalFormat("#,##0.00").format(new Float(number_of_units_charged.floatValue()*price_per_unit.floatValue()));
				col++;
				total += number_of_units_charged.floatValue()*price_per_unit.floatValue();
				row++;
			}
			content[-1+row+0][-1+col-2] = "total";
			content[-1+row+0][-1+col-1] = new java.text.DecimalFormat("#,##0.00").format(new Float(total));
			float sub_total = total/(1.0f+tax_gst); new java.text.DecimalFormat("#,##0.##").format(new Float(sub_total));
			content[-1+row+1][-1+col-2] = "sub-total";
			content[-1+row+1][-1+col-1] = new java.text.DecimalFormat("#,##0.##").format(new Float(sub_total));
			float tax_gst_total = sub_total*tax_gst; new java.text.DecimalFormat("#,##0.##").format(new Float(tax_gst_total));
			content[-1+row+2][-1+col-2] = "tax@gst-total";
			content[-1+row+2][-1+col-1] = new java.text.DecimalFormat("#,##0.##").format(new Float(tax_gst_total));
			float tableHeight = PDRectangle.A3.getHeight() - (2 * 20);
		  /*final String */pdfFilename = "C:\\Temp\\"+bill_history_id.toString()+".pdf";
			// https://ttfonts.net/download/25160.htm
			final InputStream is = Bills.class.getResourceAsStream("LucidaConsole.ttf");
		  /*final */PDFont font = null;
			if (null != is) {
				try {
					font = PDType0Font.load(pdf, is, true);
				} catch (final IOException e) {
					pdf = null;
				  //throw new RuntimeException(e);
					e.printStackTrace(System.err);
				}
			}
		  /*final */Table table = null;
			if (null != font) {
			  /*final Table */table = new TableBuilder()
					.setCellMargin(2)
					.setColumns(columns)
					.setContent(content)
					.setHeight(tableHeight)
					.setNumberOfRows(content.length)
					.setRowHeight(15)
					.setMargin(20)
					.setPageSize(PDRectangle.A3)
					.setLandscape(false)
				  //.setTextFont(PDType1Font.HELVETICA)
					.setTextFont(font)
					.setFontSize(10)
					.build();
			}
			if (null != table) {
				try {
					new PDFTableGenerator().drawTable(pdf, table);
				} catch (final IOException e) {
					pdf = null;
				  //throw new RuntimeException(e);
					e.printStackTrace(System.err);
				}
			}
			if (null != pdf) {
				try {
					pdf.save(pdfFilename);
					pdf.close();
				} catch (final IOException e) {
					pdfFilename = null;
				  //throw new RuntimeException(e);
				}
			  //final String jpgFilename = null;
				if (null != pdfFilename) {
					history.put("bill-history-pdf", pdfFilename);
				} else if (null != history.get("bill-history-pdf")) {
					history.remove("bill-history-pdf");
				}
			}
//}
		return pdfFilename;
	}
	
  /*public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
		Addresses.addRole(group, role_id, Break.Recursion);
		Roles.addToGroup(Roles.roles.get(role_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles");
		if (null == roles) {
		*//*final java.util.Set<java.lang.String> *//*roles = new dao.util.HashSet<java.lang.String>();
			group.put("roles", roles);
		}
		roles.add(role_id);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
		Addresses.removeRole(group, role_id, Break.Recursion);
		Roles.removeFromGroup(Roles.roles.get(role_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles");
		if (null != roles) {
			roles.remove(role_id);
			if (0 == roles.size()) {
				group.remove("roles");
			}
		}
	}
	
	public static void addUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
		Addresses.addUser(group, user_id, Break.Recursion);
		Users.addToGroup(Users.users.get(user_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users");
		if (null == users) {
		*//*final java.util.Set<java.lang.String> *//*users = new dao.util.HashSet<java.lang.String>();
			group.put("users", users);
		}
		users.add(user_id);
	}
	public static void removeUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
		Addresses.removeUser(group, user_id, Break.Recursion);
		Users.removeFromGroup(Users.users.get(user_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removeUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id, final dro.lang.Break.Recursion br) {
	*//*final *//*java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users");
		if (null != users) {
			users.remove(user_id);
			if (0 == users.size()) {
				group.remove("users");
			}
		}
	}
	
	public static boolean hasPermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id) {
	*//*final *//*boolean hasPermission = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != permission_id && 0 < permission_id.trim().length()) {
			final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)group.get("permissions"); // Group permissions
			hasPermission = null != permissions && true == permissions.contains(permission_id);
		}
		if (false == hasPermission) { // keep looking..
			final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // Group roles
			if (null != roles) {
				for (final String role_id: roles) {
					final java.util.Map<java.lang.String, java.lang.Object> role = Roles.roles.get(role_id);
					{
						final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions"); // Group role permissions
						hasPermission = null != permissions && true == permissions.contains(permission_id);
					}
					if (true == hasPermission) break;
				}
			}
		}
		return hasPermission;
	}
	public static boolean hasRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
	*//*final *//*boolean hasRole = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != role_id && 0 < role_id.trim().length()) {
			final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // Group roles
			hasRole = null != roles && true == roles.contains(role_id);
		}
		return hasRole;
	}
	public static boolean hasUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
	*//*final *//*boolean hasUser = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != user_id && 0 < user_id.trim().length()) {
			final java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users"); // Group users
			hasUser = null != users && true == users.contains(user_id);
		}
		return hasUser;
	}*/
}