package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BillsLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
	  /*final */java.util.List<Integer> __bills = null;
	  /*final */java.util.List<java.util.Map<String, Object>> /*bills_*/history = null;
	  /*final */java.util.List<java.util.Map<String, Object>> /*bill_*/lines_history = null;
	  /*final */java.util.Map<String, Object> /*bill_*/line_history = null;
		
	  /*final */java.util.Map<String, Object> __role = null;
	  /*final */java.util.Map<String, Object> __user = null;
	  /*final */java.util.Map<String, Object> __group = null;
		
	  /*final */String __role_id = (String)req.getParameter("role-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__role_id)) __role_id = null;
		if (null != __role_id) {
			final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)super.property("roles");
		  /*final java.util.Map<String, Object> */__role = roles.get(__role_id);
			__bills = (java.util.List<Integer>)__role.get("bills");
			req.setAttribute("role-id", __role_id);
		}
		
	  /*final */String __user_id = (String)req.getParameter("user-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__user_id)) __user_id = null;
		if (null != __user_id) {
			final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)super.property("users");
		  /*final java.util.Map<String, Object> */__user = users.get(__user_id);
			__bills = (java.util.List<Integer>)__user.get("bills");
			req.setAttribute("user-id", __user_id);
		}
		
	  /*final */String __group_id = (String)req.getParameter("group-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__group_id)) __group_id = null;
		if (null != __group_id) {
			final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)super.property("groups");
		  /*final java.util.Map<String, Object> */__group = groups.get(__group_id);
			__bills = (java.util.List<Integer>)__group.get("bills");
			req.setAttribute("group-id", __group_id);
		}
		
		final String mode = req.getParameter("mode");
		final String submit = req.getParameter("__submit");
		
	  /*final */String path = req.getPathInfo();
	  /*final String */path = uri.replace("/hci", "");
		
	  /*final */java.util.List<java.util.Map<String, Object>> bills = null;
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null == req.getParameter("lines")) {
				if (null == req.getParameter("line")) {
					if (null == req.getParameter("history")) {
						if (true == path.startsWith("/bills")) {
						  /*final java.util.List<java.util.Map<String, Object>> */bills = Bills.bills;
						}
					} else {
						if (true == path.startsWith("/bills") && null != req.getParameter("history")) {
						  /*final java.util.List<java.util.Map<String, Object>> bills_*/history = Bills.history;
						}
					}
				} else {
					if (null == req.getParameter("history")) {
					} else {
					}
				}
			} else {
				if (null == req.getParameter("history")) {
				  /*final java.util.Lsit<java.util.Map<String, Object>> */bills = Bills.bills;
					if (null != bills) {
						for (final java.util.Map<String, Object> bill: bills) {
							final String id = bill.get("$id").toString();
							req.setAttribute("bill$"+id+"#checked", true);
						}
					}
				} else {
				  /*final java.util.List<java.util.Map<String, Object>> bills_*/history = Bills.history;
					if (null != /*bills_*/history) { 
						for (final java.util.Map<String, Object> /*bill*/_history: /*bills_*/history) {
							final String id = /*__bill*/_history.get("$id").toString();
							req.setAttribute("bill-history$"+id+"#checked", true);
						}
					}
				}
			}
		} else { // if (null != submit && 0 < submit.trim().length()) {
			if (true == "re-open".equals(submit) || true == "close".equals(submit) || true == "delete".equals(submit)) {
				final boolean delete = true == "delete".equals(submit);
				final java.util.Map<String, String[]> map = req.getParameterMap();
				if (null == req.getParameter("history")) {
					final java.lang.String[] __bill_ids = map.get("bill-ids");
					for (int i = 0; i < __bill_ids.length; i++) {
					  //final int __bill_id = new Integer(__bill_ids[i]);
						final String checked = req.getParameter("bill$"+__bill_ids[i]+"#checked");
						if (null != checked && true == checked.equals("on")) {
							if (true == delete) {
								Bills.remove(new Integer(__bill_ids[i]));
							} else {
								req.setAttribute("bill$"+__bill_ids[i]+"#checked", true);
							}
						}
					}
				  /*final java.util.List<java.util.Map<String, Object>> */bills = Bills.bills;
				} else {
				  /*final java.util.List<java.util.Map<String, Object>> bills_*/history = Bills.history;
					final boolean reopen = true == "re-open".equals(submit);
					final boolean close = true == "close".equals(submit);
					final java.lang.String[] __bill_history_ids = map.get("bill-history-ids");
					for (int i = 0; i < __bill_history_ids.length; i++) {
						final int __bill_history_id = new Integer(__bill_history_ids[i]);
						final String checked = req.getParameter("bill-history$"+__bill_history_id+"#checked");
						if (null != checked && true == checked.equals("on")) {
							if (true == reopen || true == close) {
								for (final java.util.Map<java.lang.String, java.lang.Object> __history: history) {
									if (__bill_history_id == ((Integer)__history.get("$id")).intValue()) {
										if (true == reopen) {
										  //__history.remove("$state");
											__history.put("$state", "open"); // null or "open" means open
										}
										if (true == close) {
											__history.put("$state", "closed"); // only "closed" means closed (re. null or "open" means open)
											final String pdfFilename = Bills.billFormHistoryPDF((Integer)__history.get("bill-history-id"));
											pdfFilename.hashCode();
										}
									}
								}
							} else if (true == delete) {
								Bills.removeHistory(__bill_history_id);
							}
						}
					}
				}
			}
		}
		
		if (null != mode) {
			req.setAttribute("mode", mode);
		}
		
		if (null != bills) {
			req.setAttribute("bills", bills);
		}
		if (null != /*bills_*/history) {
			req.setAttribute("bills#history", /*bills_*/history);
		}
		
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}