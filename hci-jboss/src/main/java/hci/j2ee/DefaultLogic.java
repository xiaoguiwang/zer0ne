package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		@SuppressWarnings("unchecked")
		final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
		req.setAttribute("user", user);
		
		ServletTool.processSignLogic(req, this);
		
	  /*final */String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
	  /*final */RequestDispatcher requestDispatcher = null;
	  /*final */String path = req.getPathInfo();
		if (null != path && true == path.endsWith(".jsp")) {
		  /*final String */path = uri.replace("/hci", "")+".jsp";
		  /*final RequestDispatcher */requestDispatcher = req.getRequestDispatcher(path);
		} else {
		  /*final String */uri = uri.replace("/hci", "")+".jsp";
		  /*final RequestDispatcher */requestDispatcher = req.getRequestDispatcher(uri);
		}
		requestDispatcher.include(req, resp);
	}
}