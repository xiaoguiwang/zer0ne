package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EmailLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.setAttribute("emails", Emails.emails);
		if (null != req.getParameter("history")) {
			req.setAttribute("emails#history", Emails.history);
		}
		
		req.removeAttribute("meta-http-equiv");
		final String submit = req.getParameter("submit");
		final String __email_id = req.getParameter("email-id");
	  /*final */Integer email_id = null;
		try {
			email_id = null == __email_id ? null : Integer.valueOf(__email_id);
		} catch (final NumberFormatException exc) {
			// Silently ignore (for now) which we can, as we're reasonably safe with create, update, delete..
		}
	  /*final */java.util.Map<String, Object> email = null;
		if (null != email_id) {
			ServletTool.put(session, "email-id", email_id);
		} else {
			email_id = (Integer)ServletTool.get(session, "email-id");
		}
		if (null != email_id) {
			if (null == req.getParameter("history")) {
			  /*final java.util.Map<String, Object> */email = (java.util.Map<String, Object>)Emails.emails.get(email_id);
			} else {
			  /*final java.util.Map<String, Object> */email = (java.util.Map<String, Object>)Emails.history.get(email_id);
			}
		}
		
		if (null != req.getParameter("to-user-ids")) {
			ServletTool.put(session, "to-user-ids", true);
		}
		if (null != req.getParameter("from-user-id")) {
			ServletTool.put(session, "from-user-id", true);
		}
		
		if (false == java.lang.Boolean.TRUE.booleanValue()) {
		} else if (null != submit && true == submit.equals("create") && null == email) {
			// Validate data first!
		  /*final java.util.Map<String, Object> */email = new dao.util.HashMap<String, Object>();
			email.put("email-id", new Integer(Emails.emails.size()));
			for (final String key: new String[]{"subject","message"}) {
				final String value = (String)req.getParameter(key);
				if (null == value) {
					req.removeAttribute(key);
				} else {
					email.put(key, value);
					req.setAttribute(key, value);
				}
			}
			Emails.emails.add(email);
		} else if (null != submit && true == submit.equals("update") && null != email) {
			for (final String key: new String[]{"subject","message"}) {
				final String value = (String)req.getParameter(key);
				if (null == value && null != email.get(key)) {
					email.remove(key);
					req.removeAttribute(key);
				} else if (null != value && false == value.equals(email.get(key))) {
					email.put(key, value);
					req.setAttribute(key, value);
				}
			}
		} else if (null != submit && true == submit.equals("delete") && null != email) {
			// TODO: check dependencies (can't delete an e-mail if bills have/need it.. etc.
			Emails.emails.remove(email.get("email-id"));
			req.removeAttribute("email");
		} else if (null != submit && (true == submit.equals("select") || true == submit.equals("search"))) {
			final java.util.Map<String, String[]> map = req.getParameterMap();
			final String[] __user_ids = map.get("user-ids");
			{
			  /*final */java.util.List<String> ids = null;
				for (int i = 0; i < (null == __user_ids ? 0 : __user_ids.length); i++) {
					final String checked = req.getParameter("user$"+__user_ids[i]+"#checked");
					final java.util.Map<String, Object> __user = Users.getId(__user_ids[i]);
					if (null != checked && true == checked.equals("on")) {
						if (null == ids) {
							ids = new java.util.ArrayList<String>();
						}
						ids.add((String)__user.get("user-id"));
					}
				}
				if (null != ServletTool.get(session, "to-user-ids")) {
					if (null != ids) {
						ServletTool.put(session, "to:user#ids", ids.toArray(new String[0]));
					}
					ServletTool.remove(session, "to-user-ids");
				} else if (null != ServletTool.get(session, "from-user-id")) {
					if (null != ids) {
						ServletTool.put(session, "from:user#id", ids.toArray(new String[0])[0]);
					}
					ServletTool.remove(session, "from-user-id");
				}
			}
			final String from_user_id = (String)ServletTool.get(session, "from:user#id");
			final String[] to_user_ids = (String[])ServletTool.get(session, "to:user#ids");
			if (null != from_user_id && null != to_user_ids) {
			  /*final */java.util.Map<String, String[]> unresolve = null;
				if (null == submit || false == submit.trim().equals("send")) {
				  /*final java.util.Map<String, String[]> */unresolve = Emails.checkEmail("user", email_id.toString()/*FIXME!*/, from_user_id, to_user_ids, null);
				} else {
					final java.util.Map<String, Object> results = Emails.sendEmail("user", email_id.toString()/*FIXME!*/, from_user_id, to_user_ids, null);
					for (final String user_id: results.keySet()) {
						final Object o = results.get(user_id);
						if (true == o instanceof String[]) {
							if (null == unresolve) {
								unresolve = new java.util.LinkedHashMap<String, String[]>();
							}
							unresolve.put(user_id, (String[])o);
						}
					}
				}
				if (null != unresolve & unresolve.size() > 0+1) { // to account for (temporary "") to include all replacements (not just unresolved ones) in e-mails
					ServletTool.put(session, "email:unresolve", unresolve);
					req.setAttribute("email:unresolve", unresolve);
				} else {
					ServletTool.remove(session, "email:unresolve");
				}
			} else {
				ServletTool.remove(session, "email:unresolve");
			}
		}
		if ((null != req.getParameter("to-user-ids"))
			||
			(null != req.getParameter("from-user-id"))
			||
			(null != submit && true == submit.contains("search"))
		) {
			req.setAttribute("to:user#ids", true);
			req.setAttribute("from:user#id", true);
			req.setAttribute("users", Users.users); // We could filter the users here.. but we do it (once) in users-form.jsp for now - we will move to doing it in UsersLogic.java when we can nest that, everywhere (including, obviously this EmailLogic.java)
		}
		// TODO: move users search logic into UsersLogic and call it's Logic class
		if (null == submit || true == submit.equals("search")) { 
			req.setAttribute("user-id", req.getParameter("user-id"));
			req.setAttribute("email", req.getParameter("email"));
			req.setAttribute("sms", req.getParameter("sms"));
			req.setAttribute("passphrase", req.getParameter("passphrase"));
		  /*final */String[] ids = null;
			if (null != ServletTool.get(session, "to-user-ids") && null != ServletTool.get(session, "to:user#ids")) {
				req.setAttribute("confirmed-email", true);
				req.setAttribute("force:confirmed-email", true);
				req.setAttribute("confirmed-sms", ServletTool.getParameterOrAttribute(req, "confirmed-sms", (Boolean)null));
			  /*final String[] */ids = (String[])ServletTool.get(session, "to:user#ids");
			} else if (null != ServletTool.get(session, "from-user-id") && null != ServletTool.get(session, "from:user#id")) {
			  /*final String[] */ids = new String[]{(String)ServletTool.get(session, "from:user#id")};
			}
			if (null != ids) {
				for (final String __user_id: ids) {
					final java.util.Map<String, Object> __user = (java.util.Map<String, Object>)Users.users.get(__user_id);
					final String id = __user.get("$id").toString();
					req.setAttribute("user$"+id+"#checked", true);
				}
			}
		}
		if (null == req.getParameter("history")) {
			req.setAttribute("email", email);
		} else {
			req.setAttribute("email#history", email);
		}
	  /*final */String path = req.getPathInfo();
	  /*final String */path = uri.replace("/hci", "");
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}