package hci.j2ee;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Base64;

import dto.cache.Helper;

@SuppressWarnings("unchecked")
public class Emails {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> emails;// = null;
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> history;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Emails.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */emails = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("emails", java.util.List.class);
		if (null == emails) {
			emails = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					// Sign-up + confirm token:
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("email-id", 0);
							put("subject", "IT From Blighty - sign-up e-mail");
							put("message", new StringWriter()
								.append("<html>\n")
								.append("<head>\n")
								.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n")
								.append("</head>\n")
								.append("<body>\n")
								.append("Hello!<br/>\n")
								.append("<p>\n")
								.append("Thank you for choosing to sign-up at ${http-stub-for-urls}. Please find your confirmation token below, and confirmation link ")
								.append("that will take you back to our web site. You may continue to use the site without confirming your e-mail address but you will ")
								.append("not be able to request a password reset, or place an order without confirmation your e-mail address first") // <br/>
								.append("</p>\n")
								.append("<a href=\"${https-stub-for-urls}/hci/signing-up?token=${signing-up-token}\">${https-stub-for-urls}/hci/signing-up?token=${signing-up-token}</a><br/>\n")
								.append("<br/>\n")
								.append("Regards, IT From Blighty<br/>\n")
								.append("<a href=\"mailto:${signature-from-email-address}\">${signature-from-email-address}</a>\n")
								.append("</body>\n")
								.append("</html>\n")
								.toString()
							);
						}
					});
					// Sign-in + confirm token:
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("email-id", 1);
							put("subject", "IT From Blighty - sign-in e-mail");
							put("message", new StringWriter()
								.append("<html>\n")
								.append("<head>\n")
								.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n")
								.append("</head>\n")
								.append("<body>\n")
								.append("Hello ${to-user#preferred-name},<br/>\n")
								.append("<p>\n")
								.append("You have requested to sign-in to ${http-stub-for-urls}. Please find your secure token below, and secure link ")
								.append("that will take you back to our web site.") // <br/>
								.append("</p>\n")
								.append("<a href=\"${https-stub-for-urls}/hci/signing-in?token=${sign-in-token}\">${https-stub-for-urls}/hci/signing-in?token=${sign-in-token}</a><br/>\n")
								.append("<br/>\n")
								.append("Regards, IT From Blighty<br/>\n")
								.append("<a href=\"mailto:${signature-from-email-address}\">${signature-from-email-address}</a>\n")
								.append("</body>\n")
								.append("</html>\n")
								.toString()
							);
						}
					});
					// Sign-in + password-reset token:
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("email-id", 2);
							put("subject", "IT From Blighty - password-reset e-mail");
							put("message", new StringWriter()
								.append("<html>\n")
								.append("<head>\n")
								.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n")
								.append("</head>\n")
								.append("<body>\n")
								.append("Hello ${to-user#preferred-name},<br/>\n")
								.append("<p>\n")
								.append("You have requested to reset your password at ${http-stub-for-urls}. Please find your confirmation token below, and confirmation link ")
								.append("that will take you back to our web site. You may continue to use the site without resetting your password") // <br/>
								.append("</p>\n")
								.append("<a href=\"${https-stub-for-urls}/hci/sign-in?token=${password-reset-token}\">${https-stub-for-urls}/hci/sign-in?token=${password-reset-token}</a><br/>\n")
								.append("<br/>\n")
								.append("Regards, IT From Blighty<br/>\n")
								.append("<a href=\"mailto:${signature-from-email-address}\">${signature-from-email-address}</a>\n")
								.append("</body>\n")
								.append("</html>\n")
								.toString()
							);
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(emails, "emails");
		}
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */history = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("emails#history", java.util.List.class);
		if (null == history) {
			history = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("email-history-id", 0);
							put("email-id", 0);
							put("from-user-id", "admin");
							put("properties", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
								private static final long serialVersionUID = 0L;
								{
									put("to-user#preferred-name", "Alex");
									put("signing-up-token", "WXYZ");
									put("http-stub-for-urls", "http://uat.itfromblighty");
									put("https-stub-for-urls", "https://uat.itfromblighty");
									put("signature-from-email-address", "emailus@itfromblighty");
								}
							});
							put("to-user-id", "arussell");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(history, "emails#history");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> email = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __email: emails) {
			if (true == id.equals(__email.get("$id").toString())) {
				email = __email;
				break;
			}
		}
		return email;
	}
	public static java.util.Map<java.lang.String, java.lang.Object> getEmailById(final java.lang.String email_id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> email = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __email: emails) {
			if (true == email_id.equals(__email.get("email-id").toString())) {
				email = __email;
				break;
			}
		}
		return email;
	}
	
	private static /*final */java.lang.Thread thread;// = null;
	
	public static void startup() {
		final java.util.Map<String, Object> service = Services.services.get("email");
	  //service.put("$state", "startup");
		service.put("$status:text", "startup");
		service.put("$status:date+time", sdf.format(new java.util.Date()));
		final java.util.Map<String, Object> __service = service;
		try {
			Emails.thread = new java.lang.Thread(){
				private final dto.mail.Adapter adapter;// = null;
				{
				  //__service.put("$state", "starting");
					__service.put("$status:text", "starting");
					__service.put("$status:date+time", sdf.format(new java.util.Date()));
					try {
						adapter = new dto.mail.Adapter(dto.mail.Adapter.Protocol.IMAP)
							.properties(new dro.util.Properties(Emails.class))
						;
					} catch (final FileNotFoundException e) {
						throw new RuntimeException(e);
					} catch (final IOException e) {
						throw new RuntimeException(e);
					}
				}
				@Override
				public void run() {
				  //__service.put("$state", "started");
					__service.put("$status:text", "started");
					__service.put("$status:date+time", sdf.format(new java.util.Date()));
				  /*final */String state = (String)__service.get("$state");
					if (null != state && true == state.equals("running")) {
						__service.put("$status:text", "running");
					}
					long elapsed = 0;
					while (null != adapter && false == Emails.shutdown) {
					  /*if (null != __service.get("$state")) {
							if (true == ((String)__service.get("$state")).equals("shutdown")) {
							  //__service.put("$state", "shutdown");
								__service.put("$status:text", "shutdown");
								__service.put("$status:date+time", sdf.format(new java.util.Date()));
								break;
							}
						}*/
						if (null != __service.get("$state")) {
							if (null != state && true == state.equals("stopped")) { // <== future
								if (true == ((String)__service.get("$state")).equals("running")) { // <== current
								/**/__service.put("$state", "starting"); // this transition to an unsupported state is okay as we're about to switch to a supported state anyway..
									__service.put("$status:text", "starting");
									__service.put("$status:date+time", sdf.format(new java.util.Date()));
									state = "running";
									__service.put("$state", state);
								}
							}
						}
						if (null != state && true == state.equals("running") && elapsed >= 60*1000L) {
							elapsed = 0L;
							__service.put("$status:date+time", sdf.format(new java.util.Date()));
							try {
								final dto.mail.Email[] emails = adapter.receive();
								if (null != emails) {
									for (int e = 0; e < emails.length; e++) {
										final String repliedMessageID = emails[e].getRepliedMessageID();
										if (null != repliedMessageID) {
										  /*final */java.util.Map<String, Object> __history = null;
											synchronized(Emails.history) {
												for (final java.util.Map<String, Object> history: Emails.history) {
													if (null != history.get("$message-id")) {
														final String messageID = (String)history.get("$message-id");
														if (null != messageID && true == messageID.equals(repliedMessageID)) {
														  /*final java.util.Map<String, Object> */__history = new dao.util.HashMap<>();
															__history.put("email-history-id", new Integer(Emails.history.size()));
														  //__history.put("email-history-id'", ..); // For replies we could link to the original e-mail..
															if (null != emails[e].from()) {
																__history.put("from-email", emails[e].from());
															}
															if (null != emails[e].body()) {
																__history.put("body", emails[e].body());
															}
															if (null != emails[e].to()) {
																__history.put("to-email", emails[e].to());
															}
															final String token = (String)history.get("$token");
															if (null != token) {
																java.util.Map<String, Object> map = Tokens.getToken(token);
																synchronized(map) {
																	map.put("$confirmed", Boolean.TRUE);
																/**/map.put("$date+time", System.currentTimeMillis()); // Note: this (for now) is overwriting when it was sent
																}
															}
															break;
														}
													}
												}
												if (null != __history) {
													Emails.history.add(__history);
final String event_name = "password-reset"; // FIXME
final Long creation_time = new Long(System.currentTimeMillis());
final String uuid_as_char = ServletTool.generateString(8);
final String from = emails[e].from();
//final String to = emails[e].to();
final String __subject = emails[e].subject();
final String body = emails[e].body();
final String repliedMessageId = emails[e].getRepliedMessageID();
final Integer email_history_id = (Integer)__history.get("email-history-id");
Helper.cacheEvent(Emails.class, uuid_as_char, new dro.util.Properties(){
	private static final long serialVersionUID = 0L;
	{
		property("event-name", event_name);
		property("creation-time", creation_time.toString());
		property("uuid-as-char", uuid_as_char);
		property("from", from);
	  //property("to", to);
		property("subject", __subject);
	  //property("mime-type", "text/html");
		property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
		property("message-id-in-base64", Base64.getEncoder().encodeToString(repliedMessageId.getBytes()));
		property("type", "email:inbound");
		property("email-history-id", email_history_id.toString());
	}
});
Helper.cacheEventX(Emails.class, uuid_as_char, new java.util.HashMap<String, Object>(){
	private static final long serialVersionUID = 0L;
	{
		put("event-name", event_name);
		put("creation-time", creation_time);
		put("uuid-as-char", uuid_as_char);
		put("process-time", new Long(System.currentTimeMillis()));
		put("type", "email:inbound");
	}
});
												}
											}
										}
									}
								}
								__service.put("$status:date+time", sdf.format(new java.util.Date()));
							} finally {
							}
						}
						if (null != __service.get("$state")) {
							if (null != state && true == state.equals("running")) { // <== future
								if (true == ((String)__service.get("$state")).equals("stopped")) { // <== current
								/**/__service.put("$state", "stopping"); // this transition to an unsupported state is okay as we're about to switch to a supported state anyway..
									__service.put("$status:text", "stopped");
									__service.put("$status:date+time", sdf.format(new java.util.Date()));
									state = "stopped";
									__service.put("$state", state);
								}
							}
						}
						try {
							Thread.sleep(1000L);
							elapsed += 1000L;
						} catch (final InterruptedException e) {
							// Silently ignore..
						}
					}
					if (null == adapter) {
					  //__service.put("$state", "crashed");
						__service.put("$status:text", "crashed");
						__service.put("$status:date+time", sdf.format(new java.util.Date()));
					}
				}
			};
			Emails.thread.start();
	  /*} catch (final IOException exc) {
			__service.put("state", -1);
			__service.put("error", exc.getMessage());*/
		} finally {
		}
	}
	
	@Deprecated // To warn callers re. issue with email_id (String) being an (Integer) index into a list (i.e. not fixed when items are deleted!
	public static java.util.Map<String, String[]> checkEmail(final String action, final String email_id, final String from_user_id, final String[] to_user_ids, final java.util.Map<String, String[]> replacements) {
	  /*final */java.util.Map<String, String[]> unresolve = null;
		// FIXME! hate the idea of looking up in a list by index where target/intention might change..
		final java.util.Map<String, Object> __from_user = Users.users.get(from_user_id);
		final java.util.Map<String, Object> email = Emails.emails.get(Integer.parseInt(email_id));
	  //final String subject = (String)email.get("subject");
		final String message = (String)email.get("message");
		int replacement_index = 0;
		for (final String user_id: to_user_ids) {
			final java.util.Map<String, Object> __user = Users.users.get(user_id);
			final dro.util.Properties q = new dro.util.Properties();
			{
				final String[] unresolved = dro.lang.String.unresolved(message, (dro.util.Properties)null);
				if (null != unresolved) {
if (null == unresolve) {
	unresolve = new java.util.LinkedHashMap<String, String[]>();
}
unresolve.put("", unresolved);
					for (final String key: unresolved) {
						final String[] sa = null == key ? null : key.split("#", 2);
						if (2 == sa.length) {
							if (false == Boolean.TRUE.booleanValue()) {
							} else if (true == sa[0].equals("to-user")) {
								if (null != __user.get(sa[1])) {
									q.property(key, __user.get(sa[1]).toString());
								} else if (null != __from_user.get("properties")) {
									final java.util.HashMap<String, String> __prop = (java.util.HashMap<String, String>)__from_user.get("properties");
									if (null != __prop.get(sa[1])) {
										q.property(key, __prop.get(sa[1]).toString());
									}
								}
							} else if (true == sa[0].equals("role")) {
							  //q.property(key, __role.get(sa[1]).toString());
							} else if (true == sa[0].equals("group")) {
							  //q.property(key, __group.get(sa[1]).toString());
							} else {
							  //throw new IllegalArgumentException();
							}
						} else {
							if (false == Boolean.TRUE.booleanValue()) {
							} else if (null != __from_user.get("properties")) {
								final java.util.HashMap<String, String> __prop = (java.util.HashMap<String, String>)__from_user.get("properties");
								if (null != __prop.get(sa[0])) {
									q.property(key, __prop.get(sa[0]).toString());
								}
							} else {
							  //throw new IllegalArgumentException();
							}
						}
					}
				}
			}
			if (null != replacements) {
			  /*final */int __replacement_index = replacement_index;
				for (final String key: replacements.keySet()) {
					final String[] values = replacements.get(key);
					if (1 == values.length) {
						__replacement_index = 0;
					}
					final String value = values[__replacement_index];
					q.property(key, value);
				}
			}
			final String[] unresolved = dro.lang.String.unresolved(message, q);
			if (null != unresolved) {
				if (null == unresolve) {
					unresolve = new java.util.LinkedHashMap<String, String[]>();
				}
				unresolve.put(user_id, unresolved);
			}
		}
		return unresolve;
	}
	@Deprecated // To warn callers re. issue with email_id (String) being an (Integer) index into a list (i.e. not fixed when items are deleted!
	public static java.util.Map<String, Object> sendEmail(final String action, final String email_id, final String from_user_id, final String[] to_user_ids, final java.util.Map<String, String[]> replacements) {
	  /*final */java.util.Map<String, Object> results = null;
		// FIXME! hate the idea of looking up in a list by index where target/intention might change..
		final java.util.Map<String, Object> __from_user = Users.users.get(from_user_id);
		final java.util.Map<String, Object> email = Emails.emails.get(Integer.parseInt(email_id));
		final String subject = (String)email.get("subject");
		final String message = (String)email.get("message");
		int replacement_index = 0;
		for (final String user_id: to_user_ids) {
			final java.util.Map<String, Object> __user = Users.users.get(user_id);
			final String transaction_id = ServletTool.generateString(8);
			__user.put(action+"-email-transaction-id", transaction_id);
			__user.put(action+"-email-state", 0);
		  /*new java.lang.Thread()*/{
			  /*private final */dto.mail.Adapter adapter = null;
				try {
					__user.put(action+"-email-state", 1);
					adapter = new dto.mail.Adapter(dto.mail.Adapter.Protocol.SMTP)
						.properties(new dro.util.Properties(Emails.class))
					;
					__user.put(action+"-email-state", 2);
				} catch (final Exception e) {
					__user.put(action+"-email-state", -1);
					__user.put(action+"-email-error", e.getMessage());
				}
			  /*@Override
				public void run() */if (null != adapter) {
					__user.put(action+"-email-state", 3);
					try {
					  //final dro.util.Properties p = new dro.util.Properties(Emails.class);
					  //final String url = dto.mail.Adapter.class.getName()+"#url";
					  //p.property("service", "message.svc")
					  // .property(url, p.property(url)+"/${destinationNumber}")
					  //;
						final dro.util.Properties q = new dro.util.Properties();
						{
							final String[] unresolved = dro.lang.String.unresolved(message, (dro.util.Properties)null);
							if (null != unresolved) {
if (null == results) {
	results = new java.util.LinkedHashMap<String, Object>();
}
results.put("", unresolved);
								for (final String key: unresolved) {
									final String[] sa = null == key ? null : key.split("#", 2);
									if (2 == sa.length) {
										if (false == Boolean.TRUE.booleanValue()) {
										} else if (true == sa[0].equals("to-user")) {
											if (null != __user.get(sa[1])) {
												q.property(key, __user.get(sa[1]).toString());
											} else if (null != __from_user.get("properties")) {
												final java.util.HashMap<String, String> __prop = (java.util.HashMap<String, String>)__from_user.get("properties");
												if (null != __prop.get(sa[1])) {
													q.property(key, __prop.get(sa[1]).toString());
												}
											}
										} else if (true == sa[0].equals("role")) {
										  //q.property(key, __role.get(sa[1]).toString());
										} else if (true == sa[0].equals("group")) {
										  //q.property(key, __group.get(sa[1]).toString());
										} else {
										  //throw new IllegalArgumentException();
										}
									} else {
										if (false == Boolean.TRUE.booleanValue()) {
										} else if (null != __from_user.get("properties")) {
											final java.util.HashMap<String, String> __prop = (java.util.HashMap<String, String>)__from_user.get("properties");
											if (null != __prop.get(sa[0])) {
												q.property(key, __prop.get(sa[0]).toString());
											}
										} else {
										  //throw new IllegalArgumentException();
										}
									}
								}
							}
						}
						if (null != replacements) {
						  /*final */int __replacement_index = replacement_index;
							for (final String key: replacements.keySet()) {
								final String[] values = replacements.get(key);
								if (1 == values.length) {
									__replacement_index = 0;
								}
								final String value = values[__replacement_index];
								q.property(key, value);
							}
						}
						final String[] unresolved = dro.lang.String.unresolved(message, q);
						if (null == results) {
							results = new java.util.LinkedHashMap<String, Object>();
						}
						if (null != unresolved) {
							results.put(user_id, unresolved);
						} else {
							final java.util.Map<String, Object> history = new dao.util.HashMap<>();
							history.put("email-history-id", new Integer(Emails.history.size()));
							history.put("email-id", email.get("email-id"));
							history.put("from-user-id", /**/"admin");
							final java.util.Map<String, Object> properties = new dao.util.HashMap<String, Object>(){
								private static final long serialVersionUID = 0L;
								{
									for (final Object key: q.keySet()) {
										put((String)key, q.get(key).toString());
									}
								}
							};
							history.put("properties", properties);
							history.put("to-user-id", user_id);
final String event_name = action;
final Long creation_time = new Long(System.currentTimeMillis());
final String uuid_as_char = ServletTool.generateString(8);
final String from = "emailus@mail.itfromblighty";
final Boolean allow_reply = true;
final String to = (String)__user.get("email");
final String __subject = dro.lang.String.resolve(subject, q);
final String body = dro.lang.String.resolve(message, q);
							final dto.mail.Email __email = new dto.mail.Email()
								.from(from) // TODO
								.allowReply(allow_reply)
							  //.replyToAddress("emailus@itfromblighty")
								.to(to)
							  //.cc("..")
							  //.bcc("..")
								.subject(__subject)
								.mimeType("text/html")//; charset=UTF-8")
								.body(body);
							if (true == Services.checkService("1:e-mail", from_user_id)) {
								final Integer reservation_id = Services.reserveService("1:e-mail", from_user_id, 1.0f);
								if (null != reservation_id) {
									try {
										/*this.*/adapter.send(__email);
										history.put("$state", "success");
									} catch (final RuntimeException e) {
										history.put("$state", "failure");
										history.put("$state:failure", e.toString());
									}
									Services.consumeService("1:e-mail", from_user_id/*, reservation_id*/, 1.0f);
								  /*Float number_of_units = */Services.unreserveService("1:e-mail", from_user_id, new Float(1.0f));
								}
							}
/**/						history.put("$message-id", __email.getMessageID());
							Emails.history.add(history); // If not sent (re. checkService) then provide reason why
final dro.util.Properties p = new dro.util.Properties(){
	private static final long serialVersionUID = 0L;
	{
		property("event-name", event_name);
		property("creation-time", creation_time.toString());
		property("uuid-as-char", uuid_as_char);
		property("from", from);
		property("allow-reply", allow_reply.toString());
		property("to", to);
		property("subject", __subject);
		property("mime-type", "text/html");
		property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
		property("message-id-in-base64", Base64.getEncoder().encodeToString(__email.getMessageID().getBytes()));
		property("type", "email:outbound");
		property("email-history-id", history.get("email-history-id").toString());
	}
};
final String state = (String)history.get("$state");
if (null != state) {
	p.property("state", (String)history.get("$state"));
	if (true == state.equals("failure")) {
		p.property("state:failure", (String)history.get("$state:failure"));
	}
}
Helper.cacheEvent(Emails.class, uuid_as_char, p);
Helper.cacheEventX(Emails.class, uuid_as_char, new java.util.HashMap<String, Object>(){
	private static final long serialVersionUID = 0L;
	{
		put("event-name", event_name);
		put("creation-time", creation_time);
		put("uuid-as-char", uuid_as_char);
		put("process-time", new Long(System.currentTimeMillis()));
		put("type", "email:outbound");
	}
});
							results.put(user_id, history);
							__user.put(action+"-email-state", 4);
							__user.remove(action+"-email-transaction-id");
						}
				  /*} catch (final ClientProtocolException exc) {
						__user.put(action+"-email-state", -3);
						__user.put(action+"-email-error", exc.getMessage());*/
				  /*} catch (final FileNotFoundException exc) {
						__user.put(action+"-email-state", -3);
						__user.put(action+"-email-error", exc.getMessage());*/
				  /*} catch (final IOException exc) {
						__user.put(action+"-email-state", -3);
						__user.put(action+"-email-error", exc.getMessage());*/
					} finally {
					}
				}
			}/*.start();*/
		}
		return results;
	}
	
	private static boolean shutdown = false;
	
	public static void shutdown() {
		if (null != Emails.thread) {
			Emails.shutdown = true;
		}
	}
}