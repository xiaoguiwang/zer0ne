package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EmailsLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.setAttribute("emails", Emails.emails);
		if (null != req.getParameter("history")) {
			req.setAttribute("emails#history", Emails.history);
		}
		
	  /*final java.util.List<java.util.Map<String, Object>> __emails = emails;
		if (null != __emails) {
			for (final java.util.Map<String, Object> __email: __emails) {
				final String id = __email.get("$id").toString();
				req.setAttribute("email$"+id+"#checked", true);
			}
		}*/
		
		req.removeAttribute("meta-http-equiv");
	  /*final */String path = req.getPathInfo();
		final String submit = req.getParameter("submit");
		if (null != submit && 0 < submit.trim().length()) {
		  /*final */boolean complete = false;
			if (true == submit.equals("update")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __email_ids = map.get("email-ids");
				for (int i = 0; i < __email_ids.length; i++) {
					final String checked = req.getParameter("email$"+__email_ids[i]+"#checked");
					final java.util.Map<String, Object> __email = Emails.getId(__email_ids[i]);
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
				  /*} else if (null != __role_id) {
						if (false == Groups.hasPermission(__email, __role_id)) {
							if (null != checked && true == checked.equals("on")) {
								Groups.addPermission(__email, __role_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Groups.removePermission(__email, __role_id);
							}
						}
						complete = true;
					} else if (null != __user_id) {
						if (false == Groups.hasRole(__email, __user_id)) {
							if (null != checked && true == checked.equals("on")) {
								Groups.addRole(__email, __user_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Groups.removeRole(__email, __user_id);
							}
						}
						complete = true;*/
					} else if (null != __email) {
					///*final */java.util.Map<String, Integer> __email_keys = (java.util.Map<String, Integer>)__bill.get("emails");
					  //if (false == Bills.hasAddress(__bill, __email_id)) {
							if (null != checked && true == checked.equals("on")) {
							  //Bills.addAddress(__bill, __email_id);
							}
					  //} else {
							if (null == checked || false == checked.equals("on")) {
							  //Bills.removeAddress(__bill, __email_id);
							}
					  //}
						complete = true;
					} else {
						req.setAttribute("email$"+__email_ids[i]+"#checked", new String("on").equals(checked));
					}
				}
			} else if (true == submit.equals("delete")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __email_ids = map.get("email-ids");
				for (int i = 0; i < __email_ids.length; i++) {
					final String checked = req.getParameter("email$"+__email_ids[i]+"#checked");
					final java.util.Map<String, Object> __email = Emails.getId(__email_ids[i]);
				  //final Integer __email_id = (Integer)__email.get("email-id");
					if (null != checked && true == checked.equals("on")) {
						Emails.emails.remove(__email/*_id.intValue()*/);
					}
				}
			}
			if (false == complete) {
			  /*final String */path = req.getPathInfo();
			  /*final String */path = uri.replace("/hci", "");
			} else {
			  /*final String */path = "/index";
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />");
			}
		} else {
		  /*final String */path = req.getPathInfo();
		  /*final String */path = uri.replace("/hci", "");
		}
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}