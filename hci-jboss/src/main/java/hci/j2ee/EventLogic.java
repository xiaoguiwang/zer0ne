package hci.j2ee;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.cache.Cache;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.client.ClientCache;

import dto.cache.Ignite;
import dto.cache.BiPredicateForStringVsMap;

public class EventLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  /*final */java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> events = null;
		if (null == events) {
		  /*final java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> */events = new java.util.HashMap<String, java.util.Map<String, Object>>();
		}
		
		final String uuid_as_char = req.getParameter("event-id");
		
		final ScanQuery<String, Map<String, Object>> queryx = new ScanQuery<>(new BiPredicateForStringVsMap());
	  /*final */ClientCache<String, Map<String, Object>> evex = null;
		if (null == evex) {
		  /*final ClientCache<String, Map<String, Object>> */evex = Ignite.getClient().getOrCreateCache("evex");
		}
		final List<Cache.Entry<String, Map<String, Object>>> entries = evex.query(queryx).getAll();
		for (final Cache.Entry<String, Map<String, Object>> entry: entries) {
			final String key = entry.getKey();
			if (true == key.equals(uuid_as_char)) {
				final Map<String, Object> map = entry.getValue();
				if (null == map) {
					try {
						throw new IllegalStateException();
					} catch (final IllegalStateException e) {
						e.printStackTrace(System.err);
					}
					continue;
				}
			  /*final */java.util.Map<String, Object> __map = null;
				if (null != map && 0 < map.size()) {
					for (final String __key: map.keySet()) {
						final Object value = map.get(__key);
						if (null != value) {
							if (null == __map) {
							  /*final java.util.Map<String, Object> */__map = new java.util.HashMap<>();
							}
							__map.put(__key, value);
						}
					}
					if (null != __map && 0 < __map.size()) {
						req.setAttribute("event", __map);
					}
				}
			}
		}
		
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("event.jsp");
		requestDispatcher.include(req, resp);
	}
}