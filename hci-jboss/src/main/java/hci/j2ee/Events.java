package hci.j2ee;

import java.text.SimpleDateFormat;

import dro.cache.ActivityType;
import dro.cache.BusinessType;
import dro.cache.ContextType;
import dro.cache.ContextsType;
import dto.cache.Ignite;

public class Events {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
  //public static /*final */java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> events;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Events.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	}
	
	private static /*final */java.lang.Thread thread;// = null;
	
	@SuppressWarnings("unchecked")
	public static void startup() {
	  /*final */java.util.Map<String, Object> /*service_*/lines = null;
		final java.util.Map<String, Object> service = Services.services.get("events");
		if (null != service) {
		  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
		}
		
	  //service.put("$state", "startup");
		service.put("$status:text", "startup");
		service.put("$status:date+time", sdf.format(new java.util.Date()));
		final java.util.Map<String, Object> __lines = lines;
		try {
			Events.thread = new java.lang.Thread(){
				private final dto.cache.Adapter adapter;// = null;
				private final dro.util.Properties p;// = null;
				{
					for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
						final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
					  //__line.put("$state", "starting");
						__line.put("$status:text", "starting");
						__line.put("$status:date+time", sdf.format(new java.util.Date()));
					}
					p = new dro.util.Properties(Events.class);
					adapter = new dto.cache.Adapter()
						.properties(p)
					;
				}
				@Override
				public void run() {
				  /*final */java.util.Map<String, String> states = null;
					for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
						final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
					  /*final */String state = (String)__line.get("$state");
						if (null != state) {
							if (null == states) {
							  /*final java.util.Map<String, String> */states = new java.util.HashMap<>();
							}
							states.put(/*service_*/line_id, state);
						}
					}
					for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
						final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
					  //__line.put("$state", "started");
						__line.put("$status:text", "started");
						__line.put("$status:date+time", sdf.format(new java.util.Date()));
					  /*final */String state = null;
						if (null != states) {
						  /*final String */state = states.get(line_id);
						}
						if (null != state && true == state.equals("running")) {
							__line.put("$status:text", "running");
						}
					}
					long elapsed = 0;
					while (null != adapter && false == Events.shutdown) {
					  /*if (null != __service.get("$state")) {
							if (true == ((String)__service.get("$state")).equals("shutdown")) {
							  //__service.put("$state", "shutdown");
								__service.put("$status:text", "shutdown");
								__service.put("$status:date+time", sdf.format(new java.util.Date()));
								break;
							}
						}*/
						for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
							final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
						  /*final */String state = null;
							if (null != states) {
							  /*final String */state = states.get(line_id);
							}
							if (null != state && null != __line.get("$state")) {
								if (true == state.equals("stopped")) { // <== current
									if (true == ((String)__line.get("$state")).equals("running")) { // <== future
									/**/__line.put("$state", "starting"); // this transition to an unsupported state is okay as we're about to switch to a supported state anyway..
										__line.put("$status:text", "starting");
										__line.put("$status:date+time", sdf.format(new java.util.Date()));
										state = "running";
										states.put(line_id, state);
										if (false == Boolean.TRUE.booleanValue()) {
										} else if (true == line_id.equals("ignite")) {
											Ignite.startServer(true);
										} else if (true == line_id.equals("match")) {
										} else if (true == line_id.equals("purge")) {
											Ignite.startExpiring();
										} else {
											throw new IllegalArgumentException();
										}
										__line.put("$state", state);
									}
								}
							}
						}
						for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
							final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
						  /*final */String state = null;
							if (null != states) {
							  /*final String */state = states.get(line_id);
							}
							if (null != state && true == state.equals("running") && elapsed >= 60*1000L) {
								elapsed = 0L;
								__line.put("$status:date+time", sdf.format(new java.util.Date()));
								try {
								} finally {
								}
							}
						}
						for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
							final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
						  /*final */String state = null;
							if (null != states) {
							  /*final String */state = states.get(line_id);
							}
							if (null != state && null != __line.get("$state")) {
								if (true == state.equals("running")) { // <== current
									if (true == ((String)__line.get("$state")).equals("stopped")) { // <== future
									/**/__line.put("$state", "stopping"); // this transition to an unsupported state is okay as we're about to switch to a supported state anyway..
										__line.put("$status:text", "stopped");
										__line.put("$status:date+time", sdf.format(new java.util.Date()));
										state = "stopped";
										states.put(line_id, state);
										if (false == Boolean.TRUE.booleanValue()) {
										} else if (true == line_id.equals("ignite")) {
											Ignite.stopServer();
										} else if (true == line_id.equals("match")) {
										} else if (true == line_id.equals("purge")) {
											Ignite.stopExpiring();
										} else {
											throw new IllegalArgumentException();
										}
										__line.put("$state", state);
									}
								}
							}
						}
						try {
							Thread.sleep(1000L);
							elapsed += 1000L;
						} catch (final InterruptedException e) {
							// Silently ignore..
						}
					}
					if (null == adapter) {
						for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
							final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(line_id);
						  //__line.put("$state", "crashed");
							__line.put("$status:text", "crashed");
							__line.put("$status:date+time", sdf.format(new java.util.Date()));
						}
					}
				}
			};
			Events.thread.start();
		} catch (final Exception exc) {
			for (final String line_id: __lines.keySet()) {
if (true == line_id.equals("$id")) continue;
				final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)lines.get(line_id);
			  //__line.put("$status:error", e.getMessage());
			  //__line.put("$state", "crashed");
				__line.put("$status:text", "crashed");
				__line.put("$status:date+time", sdf.format(new java.util.Date()));
			}
		}
	}
	
	// There are three parts of service=events:
	// 1. ignite cache (core) itself
	// 2. the eod purging
	// 3. matching engine
	// The three run in this order of 'precedence' - eod can't run with cache/core, and matching shouldn't run if eod isn't running
	
	public static ContextType getContextByName(final dro.cache.Event eve, final String name) {
	  /*final */ContextType __ctx = null;
		if (null != eve) {
			final ContextsType ctxs = eve.getContexts();
			for (final ContextType ctx: ctxs.getContext()) {
				if (true == ctx.getName().equals(name)) {
					__ctx = ctx;
					break;
				}
			}
		}
		return __ctx;
	}
	
	public static ActivityType getBusinessActivityByName(final dro.cache.Event eve, final String name) {
	  /*final */ActivityType __act = null;
		if (null != eve) {
			for (final BusinessType bus: eve.getBusiness()) {
				for (final ActivityType act: bus.getActivity()) {
					if (true == act.getName().equals(name)) {
						__act = act;
						break;
					}
				}
			}
		}
		return __act;
	}
	
	private static boolean shutdown = false;
	
	public static void shutdown() {
		if (null != Events.thread) {
			Events.shutdown = true;
		}
	}
}