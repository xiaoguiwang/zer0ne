package hci.j2ee;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.cache.Cache;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.client.ClientCache;

import dro.cache.ActivityType;
import dro.cache.ContextType;
import dto.cache.BiPredicateForEvent;
import dto.cache.Ignite;
import dto.cache.BiPredicateForStringVsMap;

public class EventsLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
	  /*final */java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> events = null;
		if (null == events) {
		  /*final java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> */events = new java.util.HashMap<String, java.util.Map<String, Object>>();
		}
		
		final ScanQuery<String, Map<String, Object>> queryx = new ScanQuery<>(new BiPredicateForStringVsMap());
	  /*final */ClientCache<String, Map<String, Object>> evex = null;
		if (null == evex) {
			try {
			  /*final ClientCache<String, Map<String, Object>> */evex = Ignite.getClient().getOrCreateCache("evex");
			} catch (final Exception e) {
			  //throw new RuntimeException(e); // TODO: reflect in services, later
			}
		}
		
		final String submit = req.getParameter("__submit");
		if (null == submit) {
			if (null != evex) {
			  /*final */ClientCache<String, dro.cache.Event> eve = null;
			  /*final */ScanQuery<String, dro.cache.Event> query = null;
				final List<Cache.Entry<String, Map<String, Object>>> entries = evex.query(queryx).getAll();
			  /*final */Map<Integer, String> indexes = null;
			  /*final */Map<String, Integer> uuids = null;
			  /*final */Map<String, String> messageIds = null;
			  /*final */Map<String, String> repliedMessageIds = null;
			  /*final */int index = 0;
				for (final Cache.Entry<String, Map<String, Object>> entry: entries) {
					final String key = entry.getKey();
					final Map<String, Object> map = entry.getValue();
					if (null == map) {
						try {
							throw new IllegalStateException();
						} catch (final IllegalStateException e) {
							e.printStackTrace(System.err);
						}
						continue;
					}
					map.put("$id", index);
					if (null == indexes) {
					  /*final Map<Integer, String> */indexes = new HashMap<>();
						session.put("%indexes", indexes);
					}
					indexes.put(new Integer(index), (String)map.get("uuid-as-char"));
					if (null == uuids) {
					  /*final Map<String, Integer> */uuids = new HashMap<>();
						session.put("%uuids", uuids);
					}
					uuids.put((String)map.get("uuid-as-char"), new Integer(index));
					if (true == ((String)map.get("event-name")).equals("password-reset")
						||
						true == ((String)map.get("event-name")).equals("signing-up")
					) {
						if (null == eve) {
						  /*final ClientCache<String, dto.cache.Event> */eve = Ignite.getClient().getOrCreateCache("eve");
						}
					  //if (null == query) {
						  /*final ScanQuery<String, dro.cache.Event> */query = new ScanQuery<>(new BiPredicateForEvent(key));
					  //}
					  /*final */dro.cache.Event xjb = null;
						final List<Cache.Entry<String, dro.cache.Event>> __xjbs = eve.query(query).getAll();
						if (1 == __xjbs.size()) {
						} else {
							throw new IllegalStateException();
						}
						for (final Cache.Entry<String, dro.cache.Event> __xjb: __xjbs) {
							xjb = __xjb.getValue();
						  //break;
						}
						if (null != xjb) {
						  /*final */ContextType ctx = Events.getContextByName(xjb, "type");
							if (null != ctx) {
								final String type = null == ctx ? "" : ctx.getValue();
								if (false == Boolean.TRUE.booleanValue()) {
								} else if (true == type.equals("email:outbound") || true == type.equals("email:inbound")) {
									map.put("$type", type);
								  /*final ContextType */ctx = Events.getContextByName(xjb, "state");
									final String state = null == ctx ? null : ctx.getValue();
									if (null != state) {
										map.put("$state", state);
									}
								  /*final ContextType */ctx = Events.getContextByName(xjb, "email-history-id");
									final Integer history_id = null == ctx ? null : new Integer(Integer.parseInt(ctx.getValue()));
									if (null != history_id) {
										map.put("email-history-id", history_id);
									}
									final ActivityType bus = Events.getBusinessActivityByName(xjb, "message-id-in-base64");
									final String value = null == bus ? null : bus.getValue();
									if (null != value && 0 < value.length()) {
										try {
											final String messageId = new String(Base64.getDecoder().decode(value));
											if (true == type.equals("email:outbound")) {
												map.put("message-id", messageId);
												if (null == messageIds) {
												  /*final Map<String, Integer> */messageIds = new HashMap<>();
													session.put("%message-ids", messageIds);
												}
												messageIds.put(value, key);
											}
											if (true == type.equals("email:inbound")) {
												map.put("replied-message-id", messageId);
												if (null == repliedMessageIds) {
												  /*final Map<String, Integer> */repliedMessageIds = new HashMap<>();
													session.put("%replied-message-ids", repliedMessageIds);
												}
												repliedMessageIds.put(value, key);
											}
										} catch (final IllegalArgumentException e) { // From Base64.getDecoder().decode(..)
											throw e;
										}
									}
								} else if (true == type.equals("sms:outbound") || true == type.equals("sms:inbound")) {
									map.put("$type", type);
								  /*final ContextType */ctx = Events.getContextByName(xjb, "state");
									final String state = null == ctx ? null : ctx.getValue();
									if (null != state) {
										map.put("$state", state);
									}
								  /*final ContextType */ctx = Events.getContextByName(xjb, "text-history-id");
									final Integer history_id = null == ctx ? null : new Integer(Integer.parseInt(ctx.getValue()));
									if (null != history_id) {
										map.put("text-history-id", history_id);
									}
									final ActivityType bus = Events.getBusinessActivityByName(xjb, "message-id");
									final String value = null == bus ? null : bus.getValue();
									if (null != value && 0 < value.length()) {
										if (true == type.equals("sms:outbound")) {
											map.put("message-id", value);
											if (null == messageIds) {
											  /*final Map<String, Integer> */messageIds = new HashMap<>();
											  session.put("%message-ids", messageIds);
											}
											messageIds.put(value, key);
										}
										if (true == type.equals("sms:inbound")) {
											map.put("replied-message-id", value);
											if (null == repliedMessageIds) {
											  /*final Map<String, Integer> */repliedMessageIds = new HashMap<>();
												session.put("%replied-message-ids", repliedMessageIds);
											}
											repliedMessageIds.put(value, key);
										}
									}
								} else {
								  //throw new IllegalArgumentException();
								}
							} else {
								try {
									throw new IllegalStateException();
								} catch (final IllegalStateException e) {
									e.printStackTrace(System.err);
								}
								continue;
							}
						} else {
							try {
								throw new IllegalStateException();
							} catch (final IllegalStateException e) {
								e.printStackTrace(System.err);
							}
							continue;
						}
					}
					final String state = (String)map.get("$state");
					if (null != state) {
						map.put("$state", state);
						if (true == state.equals("failure")) {
							final String failure = (String)map.get("state:failure");
							if (null != failure) {
								map.put("$state:failure", failure);
							}
						}
					}
					if (null == events) {
					  /*final java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> */events = new java.util.HashMap<String, java.util.Map<String, Object>>();
					}
					events.put(key, map);
					index++;
				}
				if (null != repliedMessageIds) {
					for (final String repliedMessageId: repliedMessageIds.keySet()) {
						if (null != messageIds && true == messageIds.containsKey(repliedMessageId)) {
							final String key = repliedMessageIds.get(repliedMessageId);
							final Map<String, Object> map = events.get(key);
							final String __key = messageIds.get(repliedMessageId);
							map.put("$matched", Boolean.TRUE);
							map.put("$matched-message-id:key", __key);
							final Map<String, Object> __map = events.get(__key);
							__map.put("$matched", Boolean.TRUE);
							__map.put("$matched-message-id:key", key);
						}
					}
				}
			}
			session.put("%events", events);
		} else if (true == submit.endsWith("acknowledge")) {
			final Boolean pack = submit.startsWith("+");
			final Boolean nack = submit.startsWith("-");
		  /*final java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> */events = (java.util.HashMap<String, java.util.Map<String, Object>>)session.get("%events");
			final java.util.Map<String, String[]> map = req.getParameterMap();
			final java.lang.String[] __event_ids = map.get("event-ids");
		  /*final */Map<Integer, String> indexes = null;
			for (int i = 0; i < __event_ids.length; i++) {
				final String checked = req.getParameter("event$"+__event_ids[i]+"#checked");
				if (null == indexes) {
				  /*final Map<Integer, String> */indexes = (Map<Integer, String>)session.get("%indexes");
				}
				final String key = indexes.get(new Integer(Integer.parseInt(__event_ids[i])));
				final java.util.Map<String, Object> event = events.get(key);
				if (null != checked && true == checked.equals("on")) {
					event.put("$ack", true == pack ? "+ve" : true == nack ? "-ve" : "Ack");
					Ignite.getClient().getOrCreateCache("evex").put(key, event);
					req.setAttribute("event$"+__event_ids[i]+"#checked", true);
				} else {
					event.remove("$ack");
					Ignite.getClient().getOrCreateCache("evex").put(key, event);
					req.removeAttribute("event$"+__event_ids[i]+"#checked");
				}
			}
		} else if (true == submit.endsWith("delete")) {
		  /*final java.util.Map<String, java.util.Map<java.lang.String, java.lang.Object>> */events = (java.util.HashMap<String, java.util.Map<String, Object>>)session.get("%events");
			final java.util.Map<String, String[]> map = req.getParameterMap();
			final java.lang.String[] __event_ids = map.get("event-ids");
		  /*final */Map<Integer, String> indexes = null;
			for (int i = 0; i < __event_ids.length; i++) {
				final String checked = req.getParameter("event$"+__event_ids[i]+"#checked");
				if (null == indexes) {
				  /*final Map<Integer, String> */indexes = (Map<Integer, String>)session.get("%indexes");
				}
				final String key = indexes.get(new Integer(Integer.parseInt(__event_ids[i])));
				final java.util.Map<String, Object> event = events.get(key);
				if (null != checked && true == checked.equals("on")) {
					events.remove(key);
					Ignite.getClient().getOrCreateCache("evex").remove(key);
					Ignite.getClient().getOrCreateCache("eve").remove(key);
				}
			}
		}
		
		req.setAttribute("events", events);
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/events.jsp");
		requestDispatcher.include(req, resp);
	}
}