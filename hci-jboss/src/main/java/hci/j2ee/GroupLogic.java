package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GroupLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)super.property("groups");
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.removeAttribute("meta-http-equiv");
		final String submit = req.getParameter("submit");
		final String group_id = req.getParameter("group-id");
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null != group_id) {
				final java.util.Map<String, Object> group = groups.get(group_id);
				req.setAttribute("group", group);
			}
		} else if (null != submit && 0 < submit.trim().length()) {
		  /*final */java.util.Map<String, Object> group = (java.util.Map<String, Object>)groups.get(group_id);
		  /*final */boolean complete = true;
		  //final java.util.Map<String, String> results = new java.util.HashMap<>();
		  //req.setAttribute("results", results);
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (true == submit.equals("create") && null == group) {
				// Validate data first!
				group = new dao.util.HashMap<String, Object>();
				group.put("group-id", group_id);
				for (final String key: new String[]{"name","description","organisation","registration"}) {
					final String value = (String)req.getParameter(key);
					if (true == key.equals("name") || true == key.equals("description") || true == key.equals("registration")) {
						if (null == value) {
							req.removeAttribute(key);
						} else {
							group.put(key, value);
							req.setAttribute(key, value);
						}
					} else if (true == key.equals("organisation")) {
						if (null == value) {
							req.removeAttribute(key);
						} else {
							boolean checked = null != value && true == value.equals("on");
							if (true == checked) {
								group.put(key, checked);
								req.setAttribute(key, checked);
							} else {
								req.removeAttribute(key);
							}
						}
					}
				}
				groups.put(group_id, group);
			} else if (true == submit.equals("update") && null != group) {
				for (final String key: new String[]{"name","description","organisation","registration"}) {
					final String value = (String)req.getParameter(key);
					if (true == key.equals("name") || true == key.equals("description") || true == key.equals("registration")) {
						if (null == value) {
							if (null != group.get(key)) { // FIXME!
								group.remove(key);
							}
							req.removeAttribute(key);
						} else {
							group.put(key, value);
							req.setAttribute(key, value);
						}
					} else if (true == key.equals("organisation")) {
						if (null == value) {
							if (null != group.get(key)) { // FIXME!
								group.remove(key);
							}
							req.removeAttribute(key);
						} else {
							boolean checked = null != value && true == value.equals("on");
							if (true == checked) {
								group.put(key, checked);
								req.setAttribute(key, checked);
							} else {
								if (null != group.get(key)) { // FIXME!
									group.remove(key);
								}
								req.removeAttribute(key);
							}
						}
					}
				}
			} else if (true == submit.equals("delete") && null != group) {
				// TODO: check dependencies (can't delete a group if users are members.. etc.
			}
			req.setAttribute("group", group);
		}
	  /*final */String path = req.getPathInfo();
	  /*final String */path = uri.replace("/hci", "");
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}