package hci.j2ee;

import dro.lang.Break;

@SuppressWarnings("unchecked")
public class Groups {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> groups;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Groups.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> */groups = (java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("groups", java.util.Map.class);
		if (null == groups) {
			groups = new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("admins", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("group-id", "admins");
							put("name", "Administrators");
							put("description", "The Administrators group");
						}
					});
					put("users", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("group-id", "users");
							put("name", "Users");
							put("description", "The Users group");
						}
					});
					put("itfromblighty", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("group-id", "admins@itfromblighty");
							put("name", "IT From Blighty Administrators");
							put("description", "The IT From Blighty Administrators group");
							put("organisation", true);
							put("registration", "BRN123");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(groups, "groups");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> group = null;
		for (final java.lang.String group_id: groups.keySet()) {
		  /*final */java.util.Map<java.lang.String, java.lang.Object> __group = groups.get(group_id);
			if (true == id.equals(__group.get("$id").toString())) {
				group = __group;
				break;
			}
		}
		return group;
	}
	
	public static void addPermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id) {
		Groups.addPermission(group, permission_id, Break.Recursion);
		Permissions.addToGroup(Permissions.permissions.get(permission_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addPermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)group.get("permissions");
		if (null == permissions) {
		  /*final java.util.Set<java.lang.String> */permissions = new dao.util.HashSet<java.lang.String>();
			group.put("permissions", permissions);
		}
		permissions.add(permission_id);
	}
	public static void removePermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id) {
		Groups.removePermission(group, permission_id, Break.Recursion);
		Permissions.removeFromGroup(Permissions.permissions.get(permission_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removePermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)group.get("permissions");
		if (null != permissions) {
			permissions.remove(permission_id);
			if (0 == permissions.size()) {
				group.remove("permissions");
			}
		}
	}
	
	public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
		Groups.addRole(group, role_id, Break.Recursion);
		Roles.addToGroup(Roles.roles.get(role_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles");
		if (null == roles) {
		  /*final java.util.Set<java.lang.String> */roles = new dao.util.HashSet<java.lang.String>();
			group.put("roles", roles);
		}
		roles.add(role_id);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
		Groups.removeRole(group, role_id, Break.Recursion);
		Roles.removeFromGroup(Roles.roles.get(role_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles");
		if (null != roles) {
			roles.remove(role_id);
			if (0 == roles.size()) {
				group.remove("roles");
			}
		}
	}
	
	public static void addUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
		Groups.addUser(group, user_id, Break.Recursion);
		Users.addToGroup(Users.users.get(user_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void addUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users");
		if (null == users) {
		  /*final java.util.Set<java.lang.String> */users = new dao.util.HashSet<java.lang.String>();
			group.put("users", users);
		}
		users.add(user_id);
	}
	public static void removeUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
		Groups.removeUser(group, user_id, Break.Recursion);
		Users.removeFromGroup(Users.users.get(user_id), (String)group.get("group-id"), Break.Recursion);
	}
	public static void removeUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users");
		if (null != users) {
			users.remove(user_id);
			if (0 == users.size()) {
				group.remove("users");
			}
		}
	}
	
	public static boolean hasPermission(final java.util.Map<java.lang.String, java.lang.Object> group, final String permission_id) {
	  /*final */boolean hasPermission = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group) {
			if (null != permission_id && 0 < permission_id.trim().length()) {
				final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)group.get("permissions"); // Group permissions
				hasPermission = null != permissions && true == permissions.contains(permission_id);
			}
			if (false == hasPermission) { // keep looking..
				final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // Group roles
				if (null != roles) {
					for (final String role_id: roles) {
						final java.util.Map<java.lang.String, java.lang.Object> role = Roles.roles.get(role_id);
						{
							final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions"); // Group role permissions
							hasPermission = null != permissions && true == permissions.contains(permission_id);
						}
						if (true == hasPermission) break;
					}
				}
			}
		}
		return hasPermission;
	}
	public static boolean hasRole(final java.util.Map<java.lang.String, java.lang.Object> group, final String role_id) {
	  /*final */boolean hasRole = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != role_id && 0 < role_id.trim().length()) {
			final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // Group roles
			hasRole = null != roles && true == roles.contains(role_id);
		}
		return hasRole;
	}
	public static boolean hasUser(final java.util.Map<java.lang.String, java.lang.Object> group, final String user_id) {
	  /*final */boolean hasUser = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != group && null != user_id && 0 < user_id.trim().length()) {
			final java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)group.get("users"); // Group users
			hasUser = null != users && true == users.contains(user_id);
		}
		return hasUser;
	}
}