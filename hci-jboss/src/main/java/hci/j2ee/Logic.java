package hci.j2ee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.Properties;

abstract public class Logic {
	Properties properties;// = null;
	
	{
	  //properties = null;
	}
	
	public Logic() {
	}
	public Logic(final Properties properties) {
		this.properties = properties;
	}
	
	public Logic properties(final Properties properties) {
		this.properties = properties;
		return this;
	}
	public Logic property(final String key, final Object value) {
		if (null == this.properties) {
			this.properties = new Properties();
		}
		this.properties.put(key, value);
		return this;
	}
	public Object property(final String key) {
	  /*final */Object value = null;
		if (null != this.properties) {
			value = this.properties.get(key);
		}
		return value;
	}
	
	public abstract void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException;
}