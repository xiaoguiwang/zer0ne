package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PermissionLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		final java.util.Map<String, java.util.Map<String, Object>> permissions = (java.util.Map<String, java.util.Map<String, Object>>)super.property("permissions");
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.removeAttribute("meta-http-equiv");
		final String submit = req.getParameter("submit");
		final String permission_id = req.getParameter("permission-id");
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null != permission_id) {
				final java.util.Map<String, Object> permission = permissions.get(permission_id);
				req.setAttribute("permission", permission);
			}
		} else if (null != submit && 0 < submit.trim().length()) {
		  /*final */java.util.Map<String, Object> permission = (java.util.Map<String, Object>)permissions.get(permission_id);
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (true == submit.equals("create") && null == permission) {
				// Validate data first!
				permission = new dao.util.HashMap<String, Object>();
				permission.put("permission-id", permission_id);
				for (final String key: new String[]{"name","description"}) {
					final String value = (String)req.getParameter(key);
					if (null == value) {
						req.removeAttribute(key);
					} else {
						permission.put(key, value);
						req.setAttribute(key, value);
					}
				}
				Permissions.permissions.put(permission_id, permission);
			} else if (true == submit.equals("update") && null != permission) {
				for (final String key: new String[]{"name","description"}) {
					final String value = (String)req.getParameter(key);
					if (null == value && null != permission.get(key)) {
						permission.remove(key);
						req.removeAttribute(key);
					} else if (null != value && false == value.equals(permission.get(key))) {
						permission.put(key, value);
						req.setAttribute(key, value);
					}
				}
			} else if (true == submit.equals("delete") && null != permission) {
				// TODO: check dependencies (can't delete a permission if it's granted to a role/user/group. etc.
			}
			req.setAttribute("permission", permission);
		}
	  /*final */String path = req.getPathInfo();
	  /*final String */path = uri.replace("/hci", "");
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}