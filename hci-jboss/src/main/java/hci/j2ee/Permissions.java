package hci.j2ee;

import dro.lang.Break;

@SuppressWarnings("unchecked")
public class Permissions {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> permissions;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Permissions.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> */permissions = (java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("permissions", java.util.Map.class);
		if (null == permissions) {
			permissions = new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("any-user-create", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("permission-id", "any-user-create");
							put("name", "Any User Create");
							put("description", "Any User Create permission");
						}
					});
					put("any-user-read", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("permission-id", "any-user-read");
							put("name", "Any User Read");
							put("description", "Any User Read permission");
						}
					});
					put("any-user-update", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("permission-id", "any-user-update");
							put("name", "Any User Update");
							put("description", "Any User Update permission");
						}
					});
					put("any-user-delete", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("permission-id", "any-user-delete");
							put("name", "Any User Delete");
							put("description", "Any User Delete permission");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(permissions, "permissions");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> permission = null;
		for (final java.lang.String permission_id: permissions.keySet()) {
		  /*final */java.util.Map<java.lang.String, java.lang.Object> __permission = permissions.get(permission_id);
			if (true == id.equals(__permission.get("$id").toString())) {
				permission = __permission;
				break;
			}
		}
		return permission;
	}
	
	public static void addToRole(final java.util.Map<java.lang.String, java.lang.Object> permission, final String role_id) {
		final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)Roles.roles.get(role_id);
		if (null != role) {
			Permissions.addToRole(permission, role_id, Break.Recursion);
			Roles.addPermission(role, (java.lang.String)permission.get("permission-id"), Break.Recursion);
		}
	}
	public static void addToRole(final java.util.Map<java.lang.String, java.lang.Object> permission, final String role_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)permission.get("roles");
		if (null == roles) {
		  /*final java.util.Set<java.lang.String> */roles = new dao.util.HashSet<java.lang.String>();
			permission.put("roles", roles);
		}
		roles.add(role_id);
	}
	public static void removeFromRole(final java.util.Map<java.lang.String, java.lang.Object> permission, final String role_id) {
		final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)Roles.roles.get(role_id);
		if (null != role) {
			Permissions.removeFromRole(permission, role_id, Break.Recursion);
			Roles.removePermission(role, (java.lang.String)permission.get("permission-id"), Break.Recursion);
		}
	}
	public static void removeFromRole(final java.util.Map<java.lang.String, java.lang.Object> permission, final String role_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)permission.get("roles");
		if (null != roles) {
			roles.remove(role_id);
			if (0 == roles.size()) {
				permission.remove("roles");
			}
		}
	}
	
	public static void addToUser(final java.util.Map<java.lang.String, java.lang.Object> permission, final String user_id) {
		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)Users.users.get(user_id);
		if (null != user) {
			Permissions.addToUser(permission, user_id, Break.Recursion);
			Users.addPermission(user, (java.lang.String)permission.get("permission-id"), Break.Recursion);
		}
	}
	public static void addToUser(final java.util.Map<java.lang.String, java.lang.Object> permission, final String user_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)permission.get("users");
		if (null == users) {
		  /*final java.util.Set<java.lang.String> */users = new dao.util.HashSet<java.lang.String>();
			permission.put("users", users);
		}
		users.add(user_id);
	}
	public static void removeFromUser(final java.util.Map<java.lang.String, java.lang.Object> permission, final String user_id) {
		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)Users.users.get(user_id);
		if (null != user) {
			Permissions.removeFromUser(permission, user_id, Break.Recursion);
			Users.removePermission(user, (java.lang.String)permission.get("permission-id"), Break.Recursion);
		}
	}
	public static void removeFromUser(final java.util.Map<java.lang.String, java.lang.Object> permission, final String user_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)permission.get("users");
		if (null != users) {
			users.remove(user_id);
			if (0 == users.size()) {
				permission.remove("users"); //<==1
			}
		}
	}
	
	public static void addToGroup(final java.util.Map<java.lang.String, java.lang.Object> permission, final String group_id) {
		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)Groups.groups.get(group_id);
		if (null != group) {
			Permissions.addToGroup(permission, group_id, Break.Recursion);
			Groups.addPermission(group, (java.lang.String)permission.get("permission-id"), Break.Recursion);
		}
	}
	public static void addToGroup(final java.util.Map<java.lang.String, java.lang.Object> permission, final String group_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)permission.get("groups");
		if (null == groups) {
		  /*final java.util.Set<java.lang.String> */groups = new dao.util.HashSet<java.lang.String>();
			permission.put("groups", groups);
		}
		groups.add(group_id);
	}
	public static void removeFromGroup(final java.util.Map<java.lang.String, java.lang.Object> permission, final String group_id) {
		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)Groups.groups.get(group_id);
		if (null != group) {
			Permissions.removeFromGroup(permission, group_id, Break.Recursion);
			Groups.removePermission(group, (java.lang.String)permission.get("permission-id"), Break.Recursion);
		}
	}
	public static void removeFromGroup(final java.util.Map<java.lang.String, java.lang.Object> permission, final String group_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)permission.get("groups");
		if (null != groups) {
			groups.remove(group_id);
			if (0 == groups.size()) {
				permission.remove("groups");
			}
		}
	}
}