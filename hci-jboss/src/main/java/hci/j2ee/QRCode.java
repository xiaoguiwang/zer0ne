package hci.j2ee;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

// https://www.baeldung.com/java-generating-barcodes-qr-codes
public class QRCode {
	public static BufferedImage generateQRCodeImage(final String barcodeText) throws Exception {
		final QRCodeWriter barcodeWriter = new QRCodeWriter();
		final BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200);
		return MatrixToImageWriter.toBufferedImage(bitMatrix);
	}
	public static byte[] toJpegBytes(final BufferedImage image) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpeg", baos);
		return baos.toByteArray();
	}
	public static void writeToFile(final byte[] ba, final String path) throws IOException {
	  /*final */FileOutputStream fis = null;
		try {
		  /*final FileOutputStream */fis = new FileOutputStream(path);
			fis.write(ba);
		} finally {
			if (null != fis) {
				fis.close();
			}
		}
	}
}