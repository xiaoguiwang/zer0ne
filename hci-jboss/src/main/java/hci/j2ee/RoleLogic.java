package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RoleLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)super.property("roles");
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.removeAttribute("meta-http-equiv");
		final String submit = req.getParameter("submit");
		final String role_id = req.getParameter("role-id");
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null != role_id) {
				final java.util.Map<String, Object> role = roles.get(role_id);
				req.setAttribute("role", role);
			}
		} else if (null != submit && 0 < submit.trim().length()) {
		  /*final */java.util.Map<String, Object> role = (java.util.Map<String, Object>)roles.get(role_id);
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (true == submit.equals("create") && null == role) {
				// Validate data first!
				role = new dao.util.HashMap<String, Object>();
				role.put("role-id", role_id);
				for (final String key: new String[]{"name","description"}) {
					final String value = (String)req.getParameter(key);
					if (null == value) {
						req.removeAttribute(key);
					} else {
						role.put(key, value);
						req.setAttribute(key, value);
					}
				}
				Roles.roles.put(role_id, role);
			} else if (true == submit.equals("update") && null != role) {
				for (final String key: new String[]{"name","description"}) {
					final String value = (String)req.getParameter(key);
					if (null == value && null != role.get(key)) {
						role.remove(key);
						req.removeAttribute(key);
					} else if (null != value && false == value.equals(role.get(key))) {
						role.put(key, value);
						req.setAttribute(key, value);
					}
				}
			} else if (true == submit.equals("delete") && null != role) {
				// TODO: check dependencies (can't delete a role if it's given to a user/group.. etc.
			}
			req.setAttribute("role", role);
		}
	  /*final */String path = req.getPathInfo();
	  /*final String */path = uri.replace("/hci", "");
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}