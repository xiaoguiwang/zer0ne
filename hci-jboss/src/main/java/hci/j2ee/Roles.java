package hci.j2ee;

import dro.lang.Break;

@SuppressWarnings("unchecked")
public class Roles {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> roles;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Roles.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> */roles = (java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("roles", java.util.Map.class);
		if (null == roles) {
			roles = new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("admin", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("role-id", "admin");
							put("name", "Administration");
							put("description", "The Administration role");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(roles, "roles");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> role = null;
		for (final java.lang.String role_id: roles.keySet()) {
		  /*final */java.util.Map<java.lang.String, java.lang.Object> __role = roles.get(role_id);
			if (true == id.equals(__role.get("$id").toString())) {
				role = __role;
				break;
			}
		}
		return role;
	}
	
	public static void addPermission(final java.util.Map<java.lang.String, java.lang.Object> role, final String permission_id) {
		Roles.addPermission(role, permission_id, Break.Recursion);
		Permissions.addToRole(Permissions.permissions.get(permission_id), (String)role.get("role-id"), Break.Recursion);
	}
	public static void addPermission(final java.util.Map<java.lang.String, java.lang.Object> role, final String permission_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions");
		if (null == permissions) {
		  /*final java.util.Set<java.lang.String> */permissions = new dao.util.HashSet<java.lang.String>();
			role.put("permissions", permissions);
		}
		permissions.add(permission_id);
	}
	public static void removePermission(final java.util.Map<java.lang.String, java.lang.Object> role, final String permission_id) {
		Roles.removePermission(role, permission_id, Break.Recursion);
		Permissions.removeFromRole(Permissions.permissions.get(permission_id), (String)role.get("role-id"), Break.Recursion);
	}
	public static void removePermission(final java.util.Map<java.lang.String, java.lang.Object> role, final String permission_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions");
		if (null != permissions) {
			permissions.remove(permission_id);
			if (0 == permissions.size()) {
				role.remove("permissions");
			}
		}
	}
	
	public static void addToUser(final java.util.Map<java.lang.String, java.lang.Object> role, final String user_id) {
		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)Users.users.get(user_id);
		if (null != user) {
			Roles.addToUser(role, user_id, Break.Recursion);
			Users.addRole(user, (String)role.get("role-id"), Break.Recursion);
		}
	}
	public static void addToUser(final java.util.Map<java.lang.String, java.lang.Object> role, final String user_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)role.get("users");
		if (null == users) {
		  /*final java.util.Set<java.lang.String> */users = new dao.util.HashSet<java.lang.String>();
			role.put("users", users);
		}
		users.add(user_id);
	}
	public static void removeFromUser(final java.util.Map<java.lang.String, java.lang.Object> role, final String user_id) {
		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)Users.users.get(user_id);
		if (null != user) {
			Roles.removeFromUser(role, user_id, Break.Recursion);
			Users.removeRole(user, (String)role.get("role-id"), Break.Recursion);
		}
	}
	public static void removeFromUser(final java.util.Map<java.lang.String, java.lang.Object> role, final String user_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> users = (java.util.Set<java.lang.String>)role.get("users");
		if (null != users) {
			users.remove(user_id);
			if (0 == users.size()) {
				role.remove("users");
			}
		}
	}
	
	public static void addToGroup(final java.util.Map<java.lang.String, java.lang.Object> role, final String group_id) {
		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)Groups.groups.get(group_id);
		if (null != group) {
			Roles.addToGroup(role, group_id, Break.Recursion);
			Groups.addRole(group, (String)role.get("role-id"), Break.Recursion);
		}
	}
	public static void addToGroup(final java.util.Map<java.lang.String, java.lang.Object> role, final String group_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)role.get("groups");
		if (null == groups) {
		  /*final java.util.Set<java.lang.String> */groups = new dao.util.HashSet<java.lang.String>();
			role.put("groups", groups);
		}
		groups.add(group_id);
	}
	public static void removeFromGroup(final java.util.Map<java.lang.String, java.lang.Object> role, final String group_id) {
		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)Groups.groups.get(group_id);
		if (null != group) {
			Roles.removeFromGroup(role, group_id, Break.Recursion);
			Groups.removeRole(group, (String)role.get("role-id"), Break.Recursion);
		}
	}
	public static void removeFromGroup(final java.util.Map<java.lang.String, java.lang.Object> role, final String group_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)role.get("groups");
		if (null != groups) {
			groups.remove(group_id);
			if (0 == groups.size()) {
				role.remove("groups");
			}
		}
	}
	
	public static boolean hasPermission(final java.util.Map<java.lang.String, java.lang.Object> role, final String permission_id) {
	  /*final */boolean hasPermission = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != role) {
			if (null != permission_id && 0 < permission_id.trim().length()) {
				final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions"); // Role permissions
				hasPermission = null != permissions && true == permissions.contains(permission_id);
			}
		}
		return hasPermission;
	}
}