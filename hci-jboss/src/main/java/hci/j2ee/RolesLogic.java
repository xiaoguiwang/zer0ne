package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RolesLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final java.util.Map<String, java.util.Map<String, Object>> sessions = (java.util.Map<String, java.util.Map<String, Object>>)super.property("sessions");
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)super.property("roles");
		req.setAttribute("roles", roles);
		
	  /*final */java.util.Set<String> __roles = null;
		
	  /*final */java.util.Map<String, Object> __permission = null;
	  /*final */java.util.Map<String, Object> __user = null;
	  /*final */java.util.Map<String, Object> __group = null;
		
	  /*final */String __permission_id = (String)req.getParameter("permission-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__permission_id)) __permission_id = null;
		if (null != __permission_id) {
			final java.util.Map<String, java.util.Map<String, Object>> permissions = (java.util.Map<String, java.util.Map<String, Object>>)super.property("permissions");
		  /*final java.util.Map<String, Object> */__permission = permissions.get(__permission_id);
			__roles = (java.util.Set<String>)__permission.get("roles");
			req.setAttribute("permission-id", __permission_id);
		}
		
	  /*final */String __user_id = (String)req.getParameter("user-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__user_id)) __user_id = null;
		if (null != __user_id) {
			final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)super.property("users");
		  /*final java.util.Map<String, Object> */__user = users.get(__user_id);
			__roles = (java.util.Set<String>)__user.get("roles");
			req.setAttribute("user-id", __user_id);
		}
		
	  /*final */String __group_id = (String)req.getParameter("group-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__group_id)) __group_id = null;
		if (null != __group_id) {
			final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)super.property("groups");
		  /*final java.util.Map<String, Object> */__group = groups.get(__group_id);
			__roles = (java.util.Set<String>)__group.get("roles");
			req.setAttribute("group-id", __group_id);
		}
		
		if (null != __roles) {
			for (final String __role_id: __roles) {
				final java.util.Map<String, Object> __role = (java.util.Map<String, Object>)roles.get(__role_id);
				final String id = __role.get("$id").toString();
				req.setAttribute("role$"+id+"#checked", true);
			}
		}
		
		req.removeAttribute("meta-http-equiv");
	  /*final */String path = req.getPathInfo();
		final String submit = req.getParameter("submit");
		if (null != submit && 0 < submit.trim().length()) {
		  /*final */boolean complete = false;
			if (true == submit.equals("update")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __role_ids = map.get("role-ids");
				for (int i = 0; i < __role_ids.length; i++) {
					final String checked = req.getParameter("role$"+__role_ids[i]+"#checked");
					final java.util.Map<String, Object> __role = Roles.getId(__role_ids[i]);
					final String __role_id = (String)__role.get("role-id");
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
					} else if (null != __permission_id) {
						if (false == Roles.hasPermission(__role, __permission_id)) {
							if (null != checked && true == checked.equals("on")) {
								Roles.addPermission(__role, __permission_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Roles.removePermission(__role, __permission_id);
							}
						}
						complete = true;
					} else if (null != __user_id) {
						if (false == Users.hasRole(__user, __role_id)) {
							if (null != checked && true == checked.equals("on")) {
								Roles.addToUser(__role, __user_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Roles.removeFromUser(__role, __user_id);
							}
						}
						complete = true;
					} else if (null != __group_id) {
						if (false == Groups.hasRole(__group, __role_id)) {
							if (null != checked && true == checked.equals("on")) {
								Roles.addToGroup(__role, __group_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Roles.removeFromGroup(__role, __group_id);
							}
						}
						complete = true;
					} else {
						req.setAttribute("role$"+__role_ids[i]+"#checked", new String("on").equals(checked));
					}
				}
			} else if (true == submit.equals("delete")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __role_ids = map.get("role-ids");
				for (int i = 0; i < __role_ids.length; i++) {
					final String checked = req.getParameter("role$"+__role_ids[i]+"#checked");
					final java.util.Map<String, Object> __role = Roles.getId(__role_ids[i]);
					final String __role_id = (String)__role.get("role-id");
					if (null != checked && true == checked.equals("on")) {
						roles.remove(__role_id);
					}
				}
			} else {
				throw new IllegalStateException();
			}
			if (false == complete) {
			  /*final String */path = req.getPathInfo();
			  /*final String */path = uri.replace("/hci", "");
			} else {
			  /*final String */path = "/index";
			/**/req.setAttribute("users", Users.users); // Hacked, for testing users off index
				req.setAttribute("signed-in-user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("signed-in-method", ServletTool.get(session, "signed-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />"); // TODO: url encoding
			}
		} else {
		  /*final String */path = req.getPathInfo();
		  /*final String */path = uri.replace("/hci", "");
		}
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}