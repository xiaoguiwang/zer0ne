package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServiceLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		final String mode = req.getParameter("mode");
		final String submit = req.getParameter("__submit");
	  /*final */java.util.Map<String, String> results = null;
		
	  /*final */java.util.Map<String, Object> service = null;
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
	  /*final */java.util.Map<String, Object> /*service_*/lines = null;
	  /*final */java.util.Map<String, Object> /*service_*/lines_history = null;
	  /*final */java.util.Map<String, Object> /*service_*/line = null;
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		
		if (null != req.getParameter("for-user-id")) {
			ServletTool.put(session, "for:user#id", true);
			req.setAttribute("users", Users.users);
		}
		if (null != req.getParameter("for-user-id") && null != ServletTool.get(session, "for:user#id")) {
			req.setAttribute("for:user#id", true);
		}
		if (null != req.getParameter("for-line-id")) {
			req.setAttribute("for:line#id", true);
		}
		
	  /*final */String path = uri.replace("/hci", "");
		
		if (null == submit || 0 == submit.trim().length()) {
			if (true == "create".equals(mode) && null == ServletTool.get(session, "for:user#id") && null == req.getParameter("for-line-id")) {
				if (null == req.getParameter("line")) {
					if (null == req.getParameter("history")) {
						// This is the scenario for create service
					  //if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", "services");
					  //}
					} else {
						// This is the scenario for create service history
					  /*final */Integer service_id = null;
						try {
						  /*final Integer */service_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service line
						}
						if (null != service_id) {
						  /*final java.util.Map<String, Object> */service = Services.get(service_id);
						}
					  /*final java.util.Map<String, Object> service_*/history = new java.util.HashMap<String, Object>();
						if (null != service) {
						  /*service_*/history.put("service-id", (String)service.get("service-id"));
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					}
				} else {
					if (null == req.getParameter("history")) {
					  /*final */Integer service_id = null;
						try {
						  /*final Integer */service_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service line
						}
						if (null != service_id) {
						  /*final java.util.Map<String, Object> */service = Services.get(service_id);
						}
						req.setAttribute("service#line", true);
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					} else {
					  /*final */Integer /*service_*/history_id = null;
						try {
						  /*final Integer service_*/history_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service line history
						}
						if (null != /*service_*/history_id) {
						  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
						}
						if (null != /*service_*/history) {
						  /*final java.util.Map<String, Object> */service = Services.get((String)history.get("service-id"));
						}
						req.setAttribute("service#line#history", true);
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					}
				}
			} else if (null != req.getParameter("lines")) {
				if (null == req.getParameter("history") && null == req.getParameter("history-id")) {
				  /*final */Integer service_id = null;
					try {
					  /*final Integer */service_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for show/read service lines
					}
					if (null != service_id) {
					  /*final java.util.Map<String, Object> */service = Services.get(service_id);
					}
					if (null != service) {
					  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
					} else {
						throw new IllegalStateException();
					}
				  //if (null != req.getParameter("cancel")) {
						req.setAttribute("cancel", "services");
				  //}
				} else {
				  /*final */Integer /*service_*/history_id = null;
					if (null == req.getParameter("for-line-id")) {
						try {
						  /*final Integer service_*/history_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for service lines history show/read ..?
						}
						if (null != /*service_*/history_id) {
						  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
						}
						if (null != /*service_*/history) {
						  /*final java.util.Map<String, Object> service_*/lines_history = (java.util.Map<String, Object>)/*service_*/history.get("%lines");
						  /*final java.util.Map<String, Object> */service = Services.get((String)/*service_*/history.get("service-id"));
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					} else {
						try {
						  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for show/read ..?
						}
						if (null != /*service_*/history_id) {
							final java.util.Map<String, Object> /*service*/_history = Services.getHistoryById(/*service_*/history_id);
							if (null != /*service*/_history) {
							  /*final java.util.Map<String, Object> */service = Services.get((String)/*service*/_history.get("service-id"));
								if (null != service) {
									if (null != req.getParameter("for-line-id")) {
									  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
										final Integer id = (Integer)ServletTool.get(session, "service:line#id");
										if (null != id) {
											final int intValue = id.intValue();
											for (final String /*service*/_line_id: /*service_*/lines.keySet()) {
if (true == /*service*/_line_id.equals("$id")) continue;
												final java.util.Map<String, Object> /*service*/_line = (java.util.Map<String, Object>)/*service_*/lines.get(/*service*/_line_id);
												final Integer /*service_line*/_id = (Integer)/*service*/_line.get("$id");
												if (null != /*service_line*/_id && /*service_line*/_id.intValue() == intValue) {
													req.setAttribute("service-line$"+/*service_line*/_id+"#checked", true);
												}
											}
										}
									}
								}
							}
						}
					  //if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", "service-history");
					  //}
					}
				}
			} else {
				if (null == req.getParameter("line")) {
					if (null != ServletTool.get(session, "for:user#id")) {
					  /*final java.util.Map<String, Object> service_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "service#history");
						if (null != /*service_*/history) {
							final Integer /*user*/_id = (Integer)/*service_*/history.get("for-user-id");
							if (null != /*user*/_id) {
								req.setAttribute("user$"+/*user*/_id+"#checked", true);
							}
						}
					} else {
						if (null == req.getParameter("service-id")) { // Not filled in on first-pass but won't sustain proper create-cycle like service line history
						  /*final */Integer service_id = null;
							try {
							  /*final Integer */service_id = new Integer(req.getParameter("id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore..
							}
							if (null != service_id) {
							  /*final java.util.Map<String, Object> */service = Services.get(service_id);
							}
							if (null != service) {
								final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = Services.getHistoryForServiceId(service_id);
								if (null != /*service_*/history_list) {
									req.setAttribute("service@history", true);
								}
							}
							if (null != req.getParameter("cancel")) {
								req.setAttribute("cancel", req.getParameter("cancel"));
							}
						} else {
						  /*final */Integer service_id = null;
							try {
							  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore..
							}
							if (null != service_id) {
							  /*final java.util.Map<String, Object> */service = Services.get(service_id);
							}
						  /*final */Integer /*service_*/history_id = null;
							try {
							  /*final Integer service_*/history_id = new Integer(req.getParameter("id"));
							} catch (final NumberFormatException e) {
							  // Silently ignore.. This is the scenario for create service
							}
							if (null != /*service_*/history_id) {
							  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
							}
							if (null != req.getParameter("cancel")) {
								req.setAttribute("cancel", req.getParameter("cancel"));
							}
						}
					}
				} else {
					if (null == req.getParameter("history")) {
					  /*final */Integer service_id = null;
						try {
						  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore..
						}
						if (null != service_id) {
						  /*final java.util.Map<String, Object> */service = Services.get(service_id);
						}
					  /*final */Integer /*service_*/line_id = null;
						try {
						  /*final Integer service_*/line_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore..
						}
						if (null != service_id && null != /*service_*/line_id) {
						  /*final java.util.Map<String, Object> service_*/line = Services.getLine(service_id, line_id);
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					} else {
					  /*final */Integer service_id = null;
						try {
						  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service line history
						}
						if (null != service_id) {
						  /*final java.util.Map<String, Object> */service = Services.get(service_id);
						}
					  /*final */Integer /*service_*/history_id = null;
						try {
						  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service
						}
						if (null != /*service_*/history_id) {
						  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
						}
					  /*final */Integer /*service_history_*/line_id = null;
						try {
						  /*final Integer service_history_*/line_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service
						}
						if (null != /*service_*/history_id && null != /*service_history_*/line_id) {
						  /*final java.util.Map<String, Object> service_*/line_history = Services.getLineHistoryById(/*service_*/history_id, /*service_history_*/line_id);
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					}
				}
			}
		} else if (null != submit && "create".equals(submit)) {
			if (null == req.getParameter("line")) {
				if (null == req.getParameter("history")) { // This is the scenario for create service
				  /*final */boolean valid = true;
					final String service_id = req.getParameter("service-id");
				  /*final java.util.Map<String, Object> */service = new java.util.HashMap<String, Object>();
					if (null != service_id) {
						service.put("service-id", service_id);
					}
					if (null == service_id || 0 == service_id.trim().length()) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service-id", "Please enter a service identifier");
					  /*final boolean */valid = false;
					} else if (null != Services.get(service_id)) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service-id", "Please enter a service identifier that does not exist");
					  /*final boolean */valid = false;
					}
					for (final String key: new String[]{"description","$state"}) {
						final String value = (String)req.getParameter("service:"+key);
						if (null != value) {
						  //service.put(key, value);
							req.setAttribute("service:"+key, value);
						}
					}
					if (true == valid) {
					  /*final java.util.Map<String, Object> */service = new dao.util.HashMap<String, Object>();
						service.put("service-id", service_id);
						for (final String key: new String[]{"description","$state"}) {
							final String value = (String)req.getParameter("service:"+key);
							if (null != value) {
								service.put(key, value);
							  //req.setAttribute("service:"+key, value);
							}
						}
						Services.services.put(service_id, service);
					  /*final String */path = uri.replace("/hci", "");
						req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/services?session-id="+session_id+"'\" />");
					}
				} else { // This is the scenario for create service history
					if (null != ServletTool.get(session, "service#history")) {
					  /*final java.util.Map<String, Object> service_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "service#history");
					} else {
					  /*final java.util.Map<String, Object> service_*/history = new java.util.HashMap<String, Object>();
						ServletTool.put(session, "service#history", /*service_*/history);
					}
				  /*final */boolean valid = true;
					final String service_id = req.getParameter("service:history:service-id");
					if (null == service_id || 0 == service_id.trim().length()) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service-id", "Please select a service identifier");
					  /*final boolean */valid = false;
					} else {
					  /*service_*/history.put("service-id", service_id/*(String)service.get("service-id")*/);
					  /*final java.util.Map<String, Object> */service = Services.get(service_id);
					}
					for (final String key: new String[]{"for-user-id"}) {
						final String value = (String)req.getParameter("service:history:"+key);
						if (null != value) {
							if (0 < value.trim().length()) {
							  /*service_*/history.put(key, value);
							} else {
								if (null == results) {
								  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
								}
								results.put("service:history:for-user-id", "Please select a user identifier");
							  /*final boolean */valid = false;
							}
						  //req.setAttribute("service:history:"+key, value);
						}
					}
					if (true == valid) {
					  /*final java.util.Map<String, Object> service_*/history = new dao.util.HashMap<String, Object>();
					  /*service_*/history.put("service-id", service_id);
						for (final String key: new String[]{"for-user-id"}) {
							final String value = (String)req.getParameter("service:history:"+key);
							if (null != value) {
							  /*service_*/history.put(key, value);
							}
						  //req.setAttribute("service:history:"+key, value);
						}
					  /*service_*/history.put("service-history-id", Services.history.size());
						Services.history.add(/*service_*/history);
						final Integer /*service*/_id = (Integer)service.get("$id");
						req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?history&session-id="+session_id+"&id="+/*service*/_id+"&cancel=services'\" />");
						ServletTool.remove(session, "service#history");
					}
				}
			} else {
				if (null == req.getParameter("history")) { // This is the scenario for create service line 100%
				  /*final */Integer service_id = null;
					try {
					  /*final Integer */service_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore..
					}
					if (null != service_id) {
					  /*final java.util.Map<String, Object> */service = Services.get(service_id);
					}
					if (null != service) {
					  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
					}
					if (null != ServletTool.get(session, "service#line")) {
					  /*final java.util.Map<String, Object> service_*/line = (java.util.Map<String, Object>)ServletTool.get(session, "service#line");
					} else {
					  /*final java.util.Map<String, Object> service_*/line = new java.util.HashMap<>();
					}
				  /*final */boolean valid = true;
					final String line_id = req.getParameter("service:line-id");
				  /*final java.util.Map<String, Object> service_*/line = new java.util.HashMap<String, Object>();
					if (null != line_id && 0 < line_id.trim().length()) {
					  /*service_*/line.put("line-id", line_id);
					}
					if (null == /*service_*/line_id || 0 == /*service_*/line_id.trim().length()) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service:line-id", "Please enter a service line identifier");
					  /*final boolean */valid = false;
					} else if (null != /*service_*/lines && null != /*service_*/lines.get(/*service_*/line_id)) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service:line-id", "Please enter a service line identifier that does not exist");
					  /*final boolean */valid = false;
					}
					for (final String key: new String[]{"description","$state","unit-of-measure","price-per-unit"}) {
						final String value = (String)req.getParameter("service:line:"+key);
						if (null != value) {
							if (true == key.equals("price-per-unit") && 0 < value.trim().length()) {
								try {
									float __value = new Float(value);
								  /*service_*/line.put(key, __value);
								} catch (final NumberFormatException e) {
									if (null == results) {
									  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
									}
									results.put("service:line:"+key, "Not a decimal number");
								  /*final boolean */valid = false;
								}
							} else {
							///*service_*/line.put(key, value);
							}
							req.setAttribute("service:line:"+key, value);
						}
					}
					if (true == valid) {
					  /*final java.util.Map<String, Object> service_*/line = new dao.util.HashMap<String, Object>();
						line.put("line-id", line_id);
						for (final String key: new String[]{"description","$state","unit-of-measure","price-per-unit"}) {
							final String value = (String)req.getParameter("service:line:"+key);
							if (null != value) {
								if (true == key.equals("price-per-unit")) {
									try {
										float __value = new Float(value);
										line.put(key, __value);
									} catch (final NumberFormatException e) {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put("service:line:"+key, "Not a decimal number");
									}
								} else {
								  /*service_*/line.put(key, value);
								}
							  //req.setAttribute("service:line:"+key, value);
							}
						}
						if (null == lines) {
						  /*final java.util.Map<String, Object> service_*/lines = new dao.util.HashMap<String, Object>();
						  /*service_*/lines.put(/*service_*/line_id, line); // If/When lines is new, don't add it to service prior to adding first service line to the service..
							service.put("%lines", lines);
						} else {
						  /*service_*/lines.put(/*service_*/line_id, line);
						}
					  /*service_*/lines.put(/*service_*/line_id, /*service_*/line);
						ServletTool.remove(session, "service#line");
					  /*final String */path = uri.replace("/hci", "");
						if (true == "service".equals(req.getParameter("cancel"))) {
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/services?session-id="+session_id+"'\" />");
						} else if (true == "service-lines".equals(req.getParameter("cancel"))) {
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?lines&session-id="+session_id+"&id="+service_id+"'\" />");
						}
					}
				  /*final java.util.Map<String, Object> service_*/lines = null;
					if (null != req.getParameter("cancel")) {
						req.setAttribute("cancel", req.getParameter("cancel"));
					}
				} else { // This is the scenario for create service line history
				  /*final */Integer service_id = null;
					try {
					  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service
					}
					if (null != service_id) {
					  /*final java.util.Map<String, Object> */service = Services.get(service_id);
					}
				  /*final */Integer /*service_*/history_id = null;
					try {
					  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service
					}
					if (null != /*service_*/history_id) {
					  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
					}
					if (null != /*service_*/history) {
					  /*final java.util.Map<String, Object> service_*/lines_history = (java.util.Map<String, Object>)/*service_*/history.get("%lines");
					}
					if (null != ServletTool.get(session, "service#line#history")) {
					  /*final java.util.Map<String, Object> service_*/line_history = (java.util.Map<String, Object>)ServletTool.get(session, "service#line#history");
					} else {
					  /*final java.util.Map<String, Object> service_*/line_history = new java.util.HashMap<>();
					}
				  /*final */boolean valid = true;
					final String /*service_*/line_id = req.getParameter("service-line-id");
					if (null == /*service_history_*/line_id || 0 == /*service_history_*/line_id.trim().length()) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service-line-id", "Please select a service line identifier");
					  /*final boolean */valid = false;
					} else if (null != /*service_*/lines_history && null != /*service_*/lines_history.get(/*service_*/line_id)) {
						if (null == results) {
						  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
						}
						results.put("service-line-id", "Please select a service line identifier that has no history");
					  /*final boolean */valid = false;
					}
				  /*final */String /*service*/_line_id = null;
					for (final String key: new String[]{"line-id","number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
						final String value = (String)req.getParameter(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key);
						if (null != value) {
							if (true == "line-id".equals(key)) {
							  /*final String service*/_line_id = value;
							}
							if (true == key.startsWith("number-of-units@")) {
								try {
									float __value = new Float(value);
									final String[] sa = key.split("@", 2);
									if (true == sa[1].equals("reserved")) {
									  /*service_*/line_history.put(key, __value);
									} else {
									  /*final */java.util.Map<Long, Float> map =  (java.util.Map<Long, Float>)/*service_*/line_history.get("%"+sa[1]);
										if (null == map) {
										  /*final java.util.Map<Long, Float> */map =  new java.util.HashMap<Long, Float>();
										  /*service_*/line_history.put("%"+sa[1], map);
										}
										map.put(new Long(System.currentTimeMillis()), __value);
									}
								} catch (final NumberFormatException e) {
									if (null == results) {
									  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
									}
									results.put(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key, "Not a decimal number");
								  /*final boolean */valid = false;
								}
							} else {
							  /*service_*/line_history.put(true == key.equals("line-id") ? "line-id" : key, value);
							}
						  //req.setAttribute(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key, value);
						}
					}
					if (true == valid) {
						final java.util.Map<String, Object> /*service*/_line_history = new dao.util.HashMap<String, Object>();
					  /*service*/_line_history.put("line-id", /*service_history*/_line_id);
						for (final String key: new String[]{"number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
							final String value = (String)req.getParameter(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key);
							if (null != value) {
								if (true == "line-id".equals(key)) {
								  /*final String service*/_line_id = value;
								}
								if (true == key.startsWith("number-of-units@")) {
									try {
										float __value = new Float(value);
										final String[] sa = key.split("@", 2);
										if (true == sa[1].equals("reserved")) {
										  /*service_*/line_history.put(key, __value);
										} else {
										  /*final */java.util.Map<Long, Float> map =  (java.util.Map<Long, Float>)/*service_*/line_history.get("%"+sa[1]);
											if (null == map) {
											  /*final java.util.Map<Long, Float> */map =  new java.util.HashMap<Long, Float>();
											  /*service_*/line_history.put("%"+sa[1], map);
											}
											map.put(new Long(System.currentTimeMillis()), __value);
										}
									} catch (final NumberFormatException e) {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key, "Not a decimal number");
									  /*final boolean */valid = false;
									}
								} else {
								  /*service_*/line_history.put(true == key.equals("line-id") ? "line-id" : key, value);
								}
							  //req.setAttribute(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key, value);
							}
						}
					  /*service_*/lines_history.put(/*service_history_*/line_id, /*service_*/line_history);
						ServletTool.remove(session, "service#line#history");
					  /*final String */path = uri.replace("/hci", "");
						if (true == "service-history".equals(req.getParameter("cancel"))) {
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?line&history&session-id="+session_id+"'\" />");
						} else if (true == "service-history-lines".equals(req.getParameter("cancel"))) {
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?lines&history&session-id="+session_id+"&id="+service_id+"'\" />");
						}
					}
				  /*final java.util.Map<String, Object> service_*/lines_history = null;
					if (null != req.getParameter("cancel")) {
						req.setAttribute("cancel", req.getParameter("cancel"));
					}
				}
			}
		} else if (null != submit && (true == submit.equals("select") || true == submit.equals("search"))) {
			final java.util.Map<String, String[]> map = req.getParameterMap();
		  /*final */java.util.List<Integer> ids = null;
			if (null != ServletTool.get(session, "for:user#id")) {
				final String[] _user_ids = map.get("user-ids");
				if (null != _user_ids) {
					for (int i = 0; i < (null == _user_ids ? 0 : _user_ids.length); i++) {
						final String checked = req.getParameter("user$"+_user_ids[i]+"#checked");
						if (null != checked && true == checked.equals("on")) {
							if (null == ids) {
							  /*final java.util.List<Integer> */ids = new java.util.ArrayList<>();
							}
							ids.add(new Integer(_user_ids[i]));
						}
					}
				}
			  //final String for_user_id = (String)ServletTool.get(session, "for:user#id");
				// ..
			}
			if (null != req.getParameter("for-line-id")) {
				final String[] /*service*/_line_ids = map.get("service-line-ids");
				if (null != /*service*/_line_ids) {
					for (int i = 0; i < (null == /*service*/_line_ids ? 0 : /*service*/_line_ids.length); i++) {
						final String checked = req.getParameter("service-line$"+/*service*/_line_ids[i]+"#checked");
						if (null != checked && true == checked.equals("on")) {
							if (null == ids) {
							  /*final java.util.List<Integer> */ids = new java.util.ArrayList<>();
							}
							ids.add(new Integer(/*service*/_line_ids[i]));
						}
					}
				}
			  //final String for_line_id = (String)ServletTool.get(session, "service:line#id");
				// ..
			}
		  /*final */Integer service_id = null;
			if (true == "select".equals(submit)) {
				if (null != ServletTool.get(session, "for:user#id")) {
					// This is the scenario for create service history
					try {
					  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service line
					}
					if (null != service_id) {
					  /*final java.util.Map<String, Object> */service = Services.get(service_id);
					}
					if (null != ServletTool.get(session, "service#history")) {
					  /*final java.util.Map<String, Object> service_*/history = (java.util.Map<String, Object>)ServletTool.get(session, "service#history");
					} else {
					  /*final java.util.Map<String, Object> service_*/history = new java.util.HashMap<String, Object>();
						ServletTool.put(session, "service#history", /*service_*/history);
					}
					if (null != service) {
					  /*service_*/history.put("service-id", (String)service.get("service-id"));
					}
					if (null != ids && 1 <= ids.size()) {
						ServletTool.put(session, "for:user#id", ids.toArray(new Integer[0])[0]);
					}
					final Integer id = (Integer)ServletTool.get(session, "for:user#id");
					if (null != id) {
						final java.util.Map<String, Object> user = Users.get(id);
					  /*service_*/history.put("for-user-id", user.get("user-id"));
						ServletTool.remove(session, "for:user#id");
					}
				  //}
				  //if (null != req.getParameter("cancel")) {
				  //	req.setAttribute("cancel", req.getParameter("cancel"));
				  //}
				}
				if (null != req.getParameter("for-line-id")) {
					try {
					  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service line history
					}
					if (null != service_id) {
					  /*final java.util.Map<String, Object> */service = Services.get(service_id);
					}
				  /*final */Integer /*service_*/history_id = null;
					try {
					  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service
					}
					if (null != /*service_*/history_id) {
					  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
					}
					if (null != ids && 1 <= ids.size()) {
						ServletTool.put(session, "service:line#id", ids.toArray(new Integer[0])[0]);
					}
					// This should be the same as read (not create, update, or delete, or search/select) service line history
				  /*final */Integer /*service_history_*/line_id = null;
					try {
					  /*final Integer service_history_*/line_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service
					}
					if (null != /*service_*/history_id && null != /*service_history_*/line_id) {
					  /*final java.util.Map<String, Object> service_*/line_history = Services.getLineHistoryById(/*service_*/history_id, /*service_history_*/line_id);
					}
					if (null == /*service_*/line_history) { // FIXME: conditional hack, for create scenario, for now
						if (null != ServletTool.get(session, "service#line#history")) {
						  /*final java.util.Map<String, Object> service_*/line_history = (java.util.Map<String, Object>)ServletTool.get(session, "service#line#history");
						} else {
						  /*final java.util.Map<String, Object> service_*/line_history = new java.util.HashMap<>();
						}
					}
				  //if (null != /*service_*/line_history) {
						final Integer id = (Integer)ServletTool.get(session, "service:line#id");
						if (null != id) {
							final java.util.Map<String, Object> /*service*/_line = Services.getLine(service_id, id);
						  /*service_*/line_history.put("line-id", /*service*/_line.get("line-id"));
						}
				  //}
				  //if (null != req.getParameter("cancel")) {
				  //	req.setAttribute("cancel", req.getParameter("cancel"));
				  //}
				}
			} else {
				if (true == "search".equals(submit)) {
					if (null != req.getParameter("lines")) {
						try {
						  /*final Integer */service_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
						  // Silently ignore.. This is the scenario for create service
						}
						if (null != service_id) {
						  /*final java.util.Map<String, Object> */service = Services.get(service_id);
						}
						if (null != service) {
						  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
						}
						if (null != req.getParameter("cancel")) {
							req.setAttribute("cancel", req.getParameter("cancel"));
						}
					}
				}
				if (null != ids) {
					if (null != req.getParameter("for-user-id")) {
						for (final Integer id: ids) {
							req.setAttribute("user$"+id+"#checked", true);
						}
					}
					if (null != req.getParameter("for-line-id")) {
						for (final Integer id: ids) {
							req.setAttribute("service-line$"+id+"#checked", true);
						}
					}
				}
			}
		} else if (null != submit && "update".equals(submit)) {
			if (null == req.getParameter("line")) {
			  /*final */Integer /*service_*/id = null;
				try {
				  /*final Integer service_*/id = new Integer(req.getParameter("id"));
				} catch (final NumberFormatException e) {
					// Silently ignore..
				}
				if (null != /*service_*/id) {
				  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
				}
// TODO: push service into session like service line, so we can move away from request attributes
				if (null != service) {
					for (final String key: new String[]{/*"service-id",*/"description","$state"}) { // TODO: allow service-id to change thus relying on references being to service $id
						final String value = (String)req.getParameter(true == key.equals("service-id") ? key : "service:"+key);
						if (null != value) {
							service.put(key, value);
							req.setAttribute("service:"+key, value);
						} else if (true == key.equals("service-id")) {
							if (null == results) {
							  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
							}
							results.put(true == key.equals("service-id") ? key : "service:"+key, "Please enter a service identifier");
						}
					}
					if (null == results || 0 == results.size()) {
					  /*final String */path = uri.replace("/hci", "");
						req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/services?session-id="+session_id+"'\" />");
					}
				} else {
					throw new IllegalStateException(); // TODO: suppose we could get here via a page revisit
				}
				if (null != req.getParameter("cancel")) {
					req.setAttribute("cancel", req.getParameter("cancel"));
				}
			} else {
				if (null == req.getParameter("history")) {
				  /*final */Integer /*service_*/id = null;
					try {
					  /*final Integer service_*/id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != /*service_*/id) {
					  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
					}
				  /*final */Integer /*service_*/line_id = null;
					try {
					  /*final Integer service_*/line_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != service && null != line_id) {
					  /*final java.util.Map<String, Object> service_*/line = Services.getLine(/*service_*/id, /*service_*/line_id);
					}
					if (null != /*service_*/line) {
						for (final String key: new String[]{/*"line-id",*/"description","$state","unit-of-measure","price-per-unit"}) {
							final String value = (String)req.getParameter(true == key.equals("line-id") ? "service:"+key : "service:line:"+key);
							if (null != value && false == value.equals(/*service_*/line.get(key))) {
								if (true == key.equals("price-per-unit")) {
									try {
										float __value = new Float(value);
									  /*service_*/line.put(key, __value);
									} catch (final NumberFormatException e) {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put(true == key.equals("line-id") ? "service:"+key : "service:line:"+key, "Not a decimal number");
									}
								} else {
								  /*service_*/line.put(key, value);
								}
								req.setAttribute(true == key.equals("line-id") ? "service:"+key : "service:line:"+key, value);
							} else if (true == key.equals("line-id")) {
								if (null == results) {
								  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
								}
								results.put(true == key.equals("line-id") ? "service:"+key : "service:line:"+key, "Please enter a service line identifier");
							}
						}
						if (null == results || 0 == results.size()) {
						  /*final String */path = uri.replace("/hci", "");
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?lines&session-id="+session_id+"&id="+/*service_*/id+"'\" />");
						}
					} else {
						throw new IllegalStateException(); // TODO: suppose we could get here via a page revisit
					}
					if (null != req.getParameter("cancel")) {
						req.setAttribute("cancel", req.getParameter("cancel"));
					}
				} else {
				  /*final */Integer /*service_*/id = null;
					try {
					  /*final Integer service_*/id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != /*service_*/id) {
					  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
					}
				  /*final */Integer /*service_*/history_id = null;
					try {
					  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service
					}
					if (null != /*service_*/history_id) {
					  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
					}
				  /*final */Integer /*service_*/line_id = null;
					try {
					  /*final Integer service_*/line_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != /*service_*/history_id && null != /*service_history_*/line_id) {
					  /*final java.util.Map<String, Object> service_*/line_history = Services.getLineHistoryById(/*service_*/history_id, /*service_history_*/line_id);
					}
					if (null == /*service_*/line_history) { // FIXME: conditional hack, for create scenario, for now
						if (null != ServletTool.get(session, "service#line#history")) {
						  /*final java.util.Map<String, Object> service_*/line_history = (java.util.Map<String, Object>)ServletTool.get(session, "service#line#history");
						} else {
						  /*final java.util.Map<String, Object> service_*/line_history = new java.util.HashMap<>();
						}
					}
					if (null != /*service_*/line_history) {
						for (final String key: new String[]{"line-id","number-of-units@reserved","number-of-units@consumed","number-of-units@charged"}) {
							final String value = (String)req.getParameter(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key);
							if (null != value && false == value.equals(/*service_*/line_history.get(key))) {
								if (true == key.startsWith("number-of-units@")) {
									try {
										float __value = new Float(value);
										final String[] sa = key.split("@", 2);
										if (true == sa[1].equals("reserved")) {
										  /*service_*/line_history.put(key, __value);
										} else {
										  /*final */java.util.Map<Long, Float> map =  (java.util.Map<Long, Float>)/*service_*/line_history.get("%"+sa[1]);
											if (null == map) {
											  /*final java.util.Map<Long, Float> */map =  new java.util.HashMap<Long, Float>();
											  /*service_*/line_history.put("%"+sa[1], map);
											}
											map.put(new Long(System.currentTimeMillis()), __value);
										}
									} catch (final NumberFormatException e) {
										if (null == results) {
										  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
										}
										results.put(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key, "Not a decimal number");
									}
								} else {
								  /*service_*/line_history.put(true == key.equals("line-id") ? "line-id" : key, value);
								}
							  //req.setAttribute(true == key.equals("line-id") ? "service-"+key : "service:line:history:"+key, value);
							}
						}
						if (null == results || 0 == results.size()) {
							ServletTool.remove(session, "service#line#history");
						  /*final String */path = uri.replace("/hci", "");
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?lines&history&session-id="+session_id+"&service-id="+id+"&id="+/*service_*/history_id+"'\" />");
						} else {
							ServletTool.put(session, "service#line#history", /*service_*/line_history);
						}
					} else {
						throw new IllegalStateException(); // TODO: suppose we could get here via a page revisit
					}
					if (null != req.getParameter("cancel")) {
						req.setAttribute("cancel", req.getParameter("cancel"));
					}
				}
			}
		} else if (null != submit && true == submit.startsWith("state:")) {
			final String value = submit.split(":",2)[1];
			if (null == req.getParameter("service-id")) {
			  /*final */Integer /*service_*/id = null;
				try {
				  /*final Integer service_*/id = new Integer(req.getParameter("id"));
				} catch (final NumberFormatException e) {
					// Silently ignore..
				}
				if (null != /*service_*/id) {
				  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
				}
			} else {
			  /*final */Integer /*service_*/id = null;
				try {
				  /*final Integer service_*/id = new Integer(req.getParameter("service-id"));
				} catch (final NumberFormatException e) {
					// Silently ignore..
				}
				if (null != /*service_*/id) {
				  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
				}
			  /*final */Integer /*service_*/line_id = null;
				try {
				  /*final Integer service_*/line_id = new Integer(req.getParameter("id"));
				} catch (final NumberFormatException e) {
					// Silently ignore..
				}
				if (null != service && null != line_id) {
				  /*final java.util.Map<String, Object> service_*/line = Services.getLine(/*service_*/id, /*service_*/line_id);
				}
			}
			if (null != service) {
				if (null != value && false == value.equals(service.get("$state"))) {
					service.put("$state", value);
					req.setAttribute("service:$state", value);
				}
			}
			if (null != /*service_*/line) {
				if (null != value && false == value.equals(/*service_*/line.get("$state"))) {
				  /*service_*/line.put("$state", value);
					req.setAttribute("service:line:$state", value);
				}
			}
		} else if (true == submit.equals("charge")) {
			final java.util.Map<String, String[]> map = req.getParameterMap();
			final String[] __service_line_ids = map.get("service-line-history-ids");
			if (null != __service_line_ids) {
				for (int i = 0; i < __service_line_ids.length; i++) {
					final String checked = req.getParameter("service-line-history$"+__service_line_ids[i]+"#checked");
					if (null != checked && true == checked.equals("on")) {
						for (final java.util.Map<String, Object> __history: Services.history) {
							final java.util.Map<String, Object> __lines = (java.util.Map<String, Object>)__history.get("%lines");
							for (final String __line_id: __lines.keySet()) {
if (true == __line_id.equals("$id")) continue;
								final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)__lines.get(__line_id); 
								if (Integer.parseInt(__service_line_ids[i]) == ((Integer)__line.get("$id")).intValue()) {
									final String user_id = (String)__history.get("for-user-id");
									Services.chargeForConsumedServices((Integer)__history.get("service-history-id"), (String)__line.get("line-id"), user_id);
								}
							}
						}
					}
				}
			} else {
			  /*final */Integer service_id = null;
				try {
				  /*final Integer */service_id = new Integer(req.getParameter("service-id"));
				} catch (final NumberFormatException e) {
				  // Silently ignore.. This is the scenario for create service line history
				}
				if (null != service_id) {
				  /*final java.util.Map<String, Object> */service = Services.get(service_id);
				}
			  /*final */Integer /*service_*/history_id = null;
				try {
				  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
				} catch (final NumberFormatException e) {
				  // Silently ignore.. This is the scenario for create service
				}
				if (null != /*service_*/history_id) {
				  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
				}
			  /*final */Integer /*service_history_*/line_id = null;
				try {
				  /*final Integer service_history_*/line_id = new Integer(req.getParameter("id"));
				} catch (final NumberFormatException e) {
				  // Silently ignore.. This is the scenario for create service
				}
				if (null != /*service_*/history_id && null != /*service_history_*/line_id) {
				  /*final java.util.Map<String, Object> service_*/line_history = Services.getLineHistoryById(/*service_*/history_id, /*service_history_*/line_id);
				}
				if (null != /*service_*/history) {
					final String user_id = (String)/*service_*/history.get("for-user-id");
					final java.util.Map<String, Object> __lines = (java.util.Map<String, Object>)/*service_*/history.get("%lines");
					for (final String __line_id: __lines.keySet()) {
if (true == __line_id.equals("$id")) continue;
						// FIXME: not ALL lines - we might only be looking at one line!
						Services.chargeForConsumedServices((Integer)/*service_*/history.get("service-history-id"), __line_id, user_id);
					}
				}
				if (null != req.getParameter("cancel")) {
					req.setAttribute("cancel", req.getParameter("cancel"));
				}
			}
		} else if (null != submit && "delete".equals(submit)) {
			if (null == req.getParameter("lines")) {
				if (null == req.getParameter("line")) {
					if (null == req.getParameter("history")) { // delete service
					  /*final */Integer id = null;
						try {
						  /*final Integer */id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
							// Silently ignore..
						}
						if (null != id) {
						  /*final java.util.Map<String, Object> */service = Services.get(id);
						}
						if (null != service) {
							Services.remove((Integer)service.get("$id"));
						}
					  /*final String */path = uri.replace("/hci", "");
						req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/services?session-id="+session_id+"'\" />");
					} else { // delete service history
						final java.util.Map<String, String[]> map = req.getParameterMap();
						final String[] /*__service*/_history_ids = map.get("service-history-ids");
						for (int i = 0; i < /*__service*/_history_ids.length; i++) {
							final String checked = req.getParameter("service-history$"+/*__service*/_history_ids[i]+"#checked");
							if (null != checked && true == checked.equals("on")) {
								final java.util.Map<String, Object> /*__service*/_history = Services.getHistoryById(new Integer(/*__service*/_history_ids[i]));
								if (null != /*__service*/_history) { // Note: until we prevent duplicate form submission, we have to check we've not already deleted the service
									final Integer /*__service_history*/_id = (Integer)/*__service*/_history.get("service-history-id");
									if (null != /*__service_history*/_id) {
										Services.removeHistory(/*service_history*/_id);
									}
								}
							}
						}
					  /*final */Integer service_id = null;
						try {
						  /*final Integer */service_id = new Integer(req.getParameter("id"));
						} catch (final NumberFormatException e) {
							// Silently ignore..
						}
						if (null != service_id) {
						  /*final java.util.Map<String, Object> */service = Services.get(service_id);
						}
						if (null != service) {
							final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = Services.getHistoryForServiceId(service_id);
							if (null != /*service_*/history_list) {
								req.setAttribute("service#history", true);
								req.setAttribute("service@history", true); // TODO: eliminate this in service-form.jsp
							}
						}
					}
				} else if (null == req.getParameter("history")) { // delete service#line
				  /*final */Integer /*service_*/id = null;
					try {
					  /*final Integer service_*/id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != /*service_*/id) {
					  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
					}
				  /*final */Integer /*service_*/line_id = null;
					try {
					  /*final Integer service_*/line_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != service && null != line_id) {
					  /*final java.util.Map<String, Object> service_*/line = Services.getLine(/*service_*/id, /*service_*/line_id);
					}
					if (null != /*service_*/line) {
						Services.removeLine(/*service_*/id, /*service_*/line_id);
					}
				  /*final String */path = uri.replace("/hci", "");
					req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"1;url='/hci/service?lines&session-id="+session_id+"&id="+/*service_*/id+"'\" />");
				} else {
				  /*final */Integer /*service_*/id = null;
					try {
					  /*final Integer service_*/id = new Integer(req.getParameter("service-id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != /*service_*/id) {
					  /*final java.util.Map<String, Object> */service = Services.get(/*service_*/id);
					}
				  /*final */Integer /*service_*/history_id = null;
					try {
					  /*final Integer service_*/history_id = new Integer(req.getParameter("history-id"));
					} catch (final NumberFormatException e) {
					  // Silently ignore.. This is the scenario for create service
					}
					if (null != /*service_*/history_id) {
					  /*final java.util.Map<String, Object> service_*/history = Services.getHistoryById(/*service_*/history_id);
					}
				  /*final */Integer /*service_*/line_id = null;
					try {
					  /*final Integer service_*/line_id = new Integer(req.getParameter("id"));
					} catch (final NumberFormatException e) {
						// Silently ignore..
					}
					if (null != /*service_*/history_id && null != /*service_history_*/line_id) {
					  /*final java.util.Map<String, Object> service_*/line_history = Services.getLineHistoryById(/*service_*/history_id, /*service_history_*/line_id);
					}
					if (null != /*service_*/line_history) {
						Services.removeLineHistoryById(/*service_*/history_id, /*service_history_*/line_id);
					}
				}
			} else if (null == req.getParameter("line")) {
			  /*final */Integer id = null;
				try {
				  /*final Integer service_*/id = new Integer(req.getParameter("id"));
				} catch (final NumberFormatException e) {
					// Silently ignore..
				}
				if (null != id) {
				  /*final java.util.Map<String, Object> */service = Services.get(id);
				}
				if (null != service) {
					final java.util.Map<String, String[]> map = req.getParameterMap();
					final String[] /*__service*/_line_ids = map.get("service-line-ids");
					for (int i = 0; i < /*__service*/_line_ids.length; i++) {
						final String checked = req.getParameter("service-line$"+/*__service*/_line_ids[i]+"#checked");
						final java.util.Map<String, Object> /*__service*/_line = Services.getLine(/*service_*/id, new Integer(/*__service*/_line_ids[i]));
						if (null != checked && true == checked.equals("on")) {
							if (null != /*__service*/_line) { // Note: until we prevent duplicate form submission, we have to check we've not already deleted the service
								Services.removeLine(/*service_*/id, (Integer)/*__service*/_line.get("$id"));
							}
						}
					}
				  /*final java.util.Map<String, Object> service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
				}
			} else {
				throw new IllegalStateException();
			}
		}
		
		if (null != results && 0 < results.size()) {
			req.setAttribute("results", results);
		}
		
		if (null != mode) {
			req.setAttribute("mode", mode);
		}
		
		if (null != service) {
			req.setAttribute("service", service);
		} else if (null == req.getParameter("lines") && null == req.getParameter("line") && null == req.getParameter("history")) {
			req.setAttribute("service", true);
		}
		if (null != /*service_*/history) {
			req.setAttribute("service#history", /*service_*/history);
		} else if (null == req.getParameter("line") && null != req.getParameter("history")) {
			req.setAttribute("service#history", true);
		}
		if (null != /*service_*/lines) {
			req.setAttribute("service#lines", /*service_*/lines);
		} else if (null != req.getParameter("lines") && null == req.getParameter("history")) {
			req.setAttribute("service#lines", true);
		}
		if (null != /*service_*/lines_history) {
			req.setAttribute("service#lines#history", /*service_*/lines_history);
		} else if (null != req.getParameter("lines") && null != req.getParameter("history")) {
			req.setAttribute("service#lines#history", true);
		}
		if (null != /*service_*/line) {
			req.setAttribute("service#line", /*service_*/line);
		} else if (null != req.getParameter("line") && null == req.getParameter("history")) {
			req.setAttribute("service#line", true);
		}
		if (null != /*service_*/line_history) {
			req.setAttribute("service#line#history", /*service_*/line_history);
		} else if (null != req.getParameter("line") && null != req.getParameter("history")) {
			req.setAttribute("service#line#history", true);
		}
		
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
//resp.setContentType("application/json");
		requestDispatcher.include(req, resp);
	}
}