package hci.j2ee;

@SuppressWarnings("unchecked")
public class Services {
	private static final Object me = new Object(); 
	public static /*final */java.util.Map<String, java.util.Map<String, Object>> services;// = null;
	public static /*final */java.util.List<java.util.Map<String, Object>> history;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Services.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.Map<java.util.Map<String, Object>> */services = (java.util.Map<String, java.util.Map<String, Object>>)dao.Context.getAdapterFor(me).alias("services", java.util.Map.class);
		if (null == services) {
			services = new dao.util.HashMap<String, java.util.Map<String, Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("email", new dao.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-id", "email");
							put("description", "send (text/html) e-mails");
							put("$state", "stopped");
							put("%lines", new dao.util.HashMap<String, java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									put("1:email", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "1:email");
											put("description", "send-1-email");
											put("$state", "available");
											put("unit-of-measure", "1-email");
											put("price-per-unit", 0.05f);
										}
									});
									put("5:emails", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "5:emails");
											put("description", "send-5-emails");
											put("$state", "available");
											put("unit-of-measure", "5-emails");
											put("price-per-unit", 0.20f);
										}
									});
								}
							});
						}
					});
					put("sms", new dao.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-id", "sms");
							put("description", "send (sms/mms) texts");
							put("$state", "stopped");
							put("%lines", new dao.util.HashMap<String, java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									put("1:sms-text", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "1:sms-text");
											put("description", "send-1-sms-text");
											put("$state", "available");
											put("unit-of-measure", "1-sms-text");
											put("price-per-unit", 0.10f);
										}
									});
									put("5:sms-texts", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "5:sms-texts");
											put("description", "send-5-sms-texts");
											put("$state", "available");
											put("unit-of-measure", "5-sms-texts");
											put("price-per-unit", 0.45f);
										}
									});
								}
							});
						}
					});
					put("events", new dao.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-id", "events");
							put("description", "ignite cache for events");
							put("$state", "available");
							put("%lines", new dao.util.HashMap<String, java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									put("ignite", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "ignite");
											put("description", "ignite cache");
											put("$state", "stopped");
										}
									});
									put("purge", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "purge");
											put("description", "purge events older than oldest validity");
											put("$state", "stopped");
										}
									});
									put("match", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "match");
											put("description", "match events for certain events");
											put("$state", "stopped");
										}
									});
								}
							});
						}
					});
					put("health-check", new dao.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-id", "health-check");
							put("description", "health check for web/e-mail services");
							put("$state", "available");
							put("%lines", new dao.util.HashMap<String, java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									put("whois", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "whois");
											put("description", "whois for domain registration check");
											put("$state", "stopped");
										}
									});
									put("dns", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "dns");
											put("description", "dns-lookup for ip resolution check");
											put("$state", "stopped");
										}
									});
									put("ping", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "ping");
											put("description", "ping for ip address reachability check");
											put("$state", "stopped");
										}
									});
									put("wget", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "wget");
											put("description", "wget for web service availability check");
											put("$state", "stopped");
										}
									});
									put("openssl:s_client:443", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "openssl:s_client:443");
											put("description", "openssl s_client ssl certificate validity check");
											put("$state", "stopped");
										}
									});
								}
							});
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(services, "services");
		}
	  /*final java.util.List<java.util.Map<String, Object>> */history = (java.util.List<java.util.Map<String, Object>>)dao.Context.getAdapterFor(me).alias("services#history", java.util.List.class);
		if (null == history) {
			history = new dao.util.ArrayList<java.util.Map<String, Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-history-id", 0);
							put("service-id", "email");
							put("for-user-id", "admin");
							put("%lines", new dao.util.HashMap<String, Object>(){
								private static final long serialVersionUID = 0L;
								{
									put("1:email", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "1:email");
											put("number-of-units:reserved", 10.0f); // Optional
											put("%consumed", new dao.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
												{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
													put(new Long(System.currentTimeMillis()-1L), new Float(1.0f));
												}
											});
											put("%charged", new dao.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
												{
													put(new Long(System.currentTimeMillis()-2L), new Float(1.0f));
												}
											});
										}
									});
								}
							});
							put("bill-history-id", 0);
						}
					});
					// Services.history.get(<n>).get("service-history-id",..,"%lines").get(<service-id>).get("service-line-id",..,"%consumed")
					add(new dao.util.HashMap<String, Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("service-history-id", 1);
							put("service-id", "sms");
							put("for-user-id", "admin");
							put("%lines", new dao.util.HashMap<String, Object>(){
								private static final long serialVersionUID = 0L;
								{
									put("1:sms-text", new dao.util.HashMap<String, Object>(){
										private static final long serialVersionUID = 0L;
										{
											put("line-id", "1:sms-text");
											put("number-of-units:reserved", 10.0f); // Optional
											put("%consumed", new dao.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
												{
													put(new Long(System.currentTimeMillis()-0L), new Float(1.0f));
												}
											});
											put("%charged", new dao.util.HashMap<Long, Float>(){
												private static final long serialVersionUID = 0L;
												{
													put(new Long(System.currentTimeMillis()-1L), new Float(1.0f));
												}
											});
										}
									});
								}
							});
							put("bill-history-id", 0);
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(history, "services#history");
		}
	}
	
	public static /*final */java.util.Map<String, java.util.List<String>> state_transitions;// = null;
	
	static {
	  /*final java.util.Map<String, java.util.List<String>> */state_transitions = new java.util.HashMap<String, java.util.List<String>>(){
			private static final long serialVersionUID = 0L;
			{
				put("running", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("stopped");
					}
				});
				put("stopped", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("running");
					}
				});
				put("available", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("unavailable");
					}
				});
				put("unavailable", new java.util.ArrayList<String>() {
					private static final long serialVersionUID = 0L;
					{
						add("available");
					}
				});
			}
		};
	}
	
	public static java.util.Map<String, Object> get(final String /*service_*/id) {
	  /*final */java.util.Map<String, Object> service = null;
		if (null != /*service_*/id && 0 < /*service_*/id.length()) {
		  /*final java.util.Map<String, Object> */service = services.get(/*service_*/id);
		}
		return service;
	}
	public static java.util.Map<String, Object> get(final Integer /*service_*/id) {
	  /*final */java.util.Map<String, Object> service = null;
		if (null != /*service_*/id) {
			final int intValue = /*service_*/id.intValue();
			for (final String service_id: services.keySet()) {
if (true == service_id.contains("$id")) continue;
				final java.util.Map<String, Object> __service = services.get(service_id);
				if (intValue == ((Integer)__service.get("$id")).intValue()) {
					service = __service;
					break;
				}
			}
		}
		return service;
	}
	
	public static java.util.Map<String, Object> getLine(final Integer id, final Integer line_id) {
	  /*final */java.util.Map<String, Object> line = null;
		if (null != id && null != line_id) {
			final java.util.Map<String, Object> service = Services.get(id);
			if (null != service) {
				final int intValue = line_id.intValue();
				final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)service.get("%lines");
				if (null != lines) {
					for (final String key: lines.keySet()) {
if (true == key.contains("$id")) continue;
						final java.util.Map<String, Object> __line = (java.util.Map<String, Object>)lines.get(key);
						if (intValue == ((Integer)__line.get("$id")).intValue()) {
						  /*service_*/line = /*service*/__line;
							break;
						}
					}
				}
			}
		}
		return /*service_*/line;
	}
	public static java.util.Map<String, Object> getLineById(final Integer /*service_*/line_id) {
	  /*final */java.util.Map<String, Object> line = null;
		if (null != line_id) {
			for (final String service_id: services.keySet()) {
if (true == service_id.contains("$id")) continue;
				final java.util.Map<String, Object> __service = services.get(service_id);
				final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__service.get("%lines");
				if (null != lines && 0 < lines.size()) {
					final int intValue = /*service_*/line_id.intValue();
						for (final String /*service*/_line_id: lines.keySet()) {
if (true == /*service*/_line_id.contains("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line = (java.util.Map<String, Object>)/*service_*/lines.get(/*service*/_line_id);
						if (intValue == ((Integer)/*service*/_line.get("$id")).intValue()) {
						  /*service_*/line = /*service*/_line;
							break;
						}
					}
				}
			}
		}
		return /*service_*/line;
	}
	
	public static java.util.Map<String, Object> findServiceByLineId(final String service_line_id) {
	  /*final */java.util.Map<String, Object> service = null;
		for (final String key: services.keySet()) {
			final java.util.Map<String, Object> __service = services.get(key);
			final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__service.get("%lines");
			if (null != lines && 0 < lines.size() && true == lines.containsKey(service_line_id)) {
				service = __service;
				break;
			}
		}
		return service;
	}
	public static java.util.Map<String, Object> findHistoryByServiceId(final String service_id) {
	  /*final */java.util.Map<String, Object> history = null;
		if (null != service_id) {
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String __service_id = (String)__history.get("service-id");
				if (true == service_id.equals(__service_id)) {
					history = __history;
					break;
				}
			}
		}
		return history;
	}
	public static java.util.Map<String, Object> findServiceLineById(final String service_line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line = null;
		for (final String key: services.keySet()) {
			final java.util.Map<String, Object> __service = services.get(key);
			final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__service.get("%lines");
			if (null != lines && 0 < lines.size() && true == lines.containsKey(service_line_id)) {
				line = (java.util.Map<String, Object>)lines.get(service_line_id);
				break;
			}
		}
		return /*service_*/line;
	}
	public static java.util.Map<String, Object> findHistoryLineById(final String service_history_line_id) {
	  /*final */java.util.Map<String, Object> /*service_history_*/line = null;
		for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
			final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			if (null != lines && 0 < lines.size() && true == lines.containsKey(service_history_line_id)) {
				line = (java.util.Map<String, Object>)lines.get(service_history_line_id);
				break;
			}
		}
		return /*service_history_*/line;
	}
	public static java.util.Map<String, Object> findHistoryById(final Integer /*service_history_*/id) {
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
		if (null != /*service_history_*/id) {
			final int intValue = id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				final Integer __id = (Integer)/*service*/_history.get("$id");
				if (null != __id && intValue == __id.intValue()) {
				  /*service_*/history = /*service*/_history;
					break;
				}
			}
		}
		return /*service_*/history;
	}
	public static java.util.Map<String, Object> findLineHistoryById(final Integer /*service_line_history_*/id) {
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		if (null != /*service_line_history_*/id) {
			final int intValue = id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				final java.util.Map<String, Object> /*service_history_*/lines = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
				for (final String line_id: /*service_history_*/lines.keySet()) {
if (true == line_id.equals("$id")) continue;
					final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service_history_*/lines.get(line_id);
					final Integer __id = (Integer)/*service*/_line_history.get("$id");
					if (null != __id && intValue == __id.intValue()) {
					  /*service_*/line_history = /*service*/_line_history;
						break;
					}
				}
			}
		}
		return /*service_*/line_history;
	}
	public static java.util.Map<String, Object> findHistoryByLineId(final Integer /*service_history_line_*/id) {
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
		if (null != /*service_history_line_*/id) {
			final int intValue = /*service_history_line_*/id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
				for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
if (true == /*service_history*/_line_id.equals("$id")) continue;
					final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
					final Integer __id = (Integer)/*service*/_line_history.get("$id");
					if (null != __id && intValue == __id.intValue()) {
					  /*service_*/history = /*service*/_history;
						break;
					}
					break;
				}
			}
		}
		return /*service_*/history;
	}
	
	public static boolean checkService(final String service_line_id, final String user_id) {
		return null != Services.findServiceByLineId(service_line_id);
	}
	public static Integer reserveService(final String service_line_id, final String user_id, final Float number_of_units) {
	  /*final */Integer id = null;
		final java.util.Map<String, Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
			// Find (an open) or start a (new) bill: (find a way to configure service provider (user) against service against bill)
			final Integer bill_history_id = Bills.reserveBill("0"/*FIXME*/, user_id, service_line_id, number_of_units);
/**/	  /*final */java.util.Map<String, Object> history = null;
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String for_user_id = (String)__history.get("for-user-id");
				if (null != for_user_id && true == for_user_id.equals(user_id)) {
					final String __service_line_id = (String)__history.get("line-id");
					if (null != service_line_id && true == __service_line_id.equals(service.get("line-id"))) {
						history = __history;
						break;
					}
				}
			}
			if (null == history) {
/**/		  /*final java.util.Map<String, Object> */history = new /*dao.util.HashMap<>*/java.util.HashMap<>(); // Hack!
				history.put("service-history-id", new Integer(Services.history.size()));
				history.put("service-id", service.get("service-id"));
				history.put("for-user-id", user_id);
			  //history.put("number-of-units:reserved", number_of_units);
				history.put("bill-history-id", bill_history_id);
				history.put("%lines", new dao.util.HashMap<String, Object>(){
					private static final long serialVersionUID = 0L;
					{
						put(service_line_id, new dao.util.HashMap<String, Object>(){
							private static final long serialVersionUID = 0L;
							{
								put("line-id", service_line_id);
								put("number-of-units:reserved", number_of_units);
							}
						});
					}
				});
				Services.history.add(history);
			} else {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
			  /*final */Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
				if (null != number_of_units_reserved && null != number_of_units) {
					line.put("number-of-units:reserved", new Float(number_of_units_reserved.floatValue()+number_of_units.floatValue()));
				} else {
					line.put("number-of-units:reserved", number_of_units);
				}
			}
		  /*final Integer */id = (Integer)history.get("service-history-id");
		} else {
			throw new IllegalArgumentException();
		}
		return id;
	}
	public static void consumeService(final String service_line_id, final String user_id, final Float number_of_units) {
		final java.util.Map<String, Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
		  /*final */java.util.Map<String, Object> history = null;
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String for_user_id = (String)__history.get("for-user-id");
				if (null != for_user_id && true == for_user_id.equals(user_id)) {
					final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__history.get("%lines");
					if (true == lines.containsKey(service_line_id)) {
						history = __history;
						break;
					}
				}
			}
			// Eek! You have to reserve service (history) to get a bill-history-id..
			if (null != history) {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
			  /*final */Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
				if (null != number_of_units_reserved && null != number_of_units) {
				  /*final */java.util.Map<Long, Object> consumed = (java.util.Map<Long, Object>)line.get("%consumed");
					if (null == consumed) {
					  /*final java.util.Map<Long, Object> */consumed = new dao.util.HashMap<>();
						line.put("%consumed", consumed);
					}
					final Long ms = new Long(System.currentTimeMillis());
					consumed.put(ms, number_of_units);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	public static void consumeService(final String service_id, final String user_id) {
		Services.consumeService(service_id, user_id, (Float)null);
	}
	public static void chargeForConsumedServices(final Integer service_history_id, final String service_line_id, final String user_id) {
	  /*final */java.util.Map<String, Object> history = null;
		for (final java.util.Map<String, Object> __history: Services.history) {
			if (((Integer)__history.get("service-history-id")).intValue() == service_history_id.intValue()) {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__history.get("%lines");
				if (true == lines.containsKey(service_line_id)) {
					history = __history;
					break;
				}
			}
		}
		if (null != history) {
		///*final */String service_id = (String)history.get("service-id");
		  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
		  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
		///*final */String user_id = (String)history.get("for-user-id");
		  /*final */java.util.Map<Long, Float> consumed = (java.util.Map<Long, Float>)line.get("%consumed");
		  /*final */java.util.Map<Long, Float> charged = (java.util.Map<Long, Float>)line.get("%charged");
		  /*final */float number_of_units = 0.0f;
			if (null != consumed && 0 < consumed.size()-1) {
			  /*final */java.util.List<Long> keys = new java.util.ArrayList<>();
				for (final Object o: consumed.keySet()) {
if (true == o.toString().equals("$id")) continue;
					final Long ms = (Long)o;
					if (null == charged) {
					  /*final java.util.Map<Long, Float> */charged = new dao.util.HashMap<Long, Float>();
						line.put("%charged", charged);
					}
					number_of_units += ((Float)consumed.get(ms)).floatValue();
					charged.put(ms, consumed.get(ms));
					if (null == keys) {
						keys = new java.util.ArrayList<Long>();
					}
					keys.add(ms);
				}
				for (final Long ms: keys) {
					consumed.remove(ms);
				}
			  //consumed.clear(); consumed = null;
			  //line.remove("%consumed");
				final Integer bill_history_id = (Integer)history.get("bill-history-id");
			  /*final Float number_of_units_remaining = */Bills.chargeBill(bill_history_id, user_id, service_line_id, number_of_units);
//if (number_of_units_reserved.floatValue()-number_of_units.floatValue() != number_of_units_remaining.floatValue()) {
//		throw new IllegalStateException(); // This should never happen but identifies a discrepancy between Services and Bills i.e. (likely) something broke part-way through
//}
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	// README: pre-book (get an upfront reservation on the bill) and bulk blocking (e.g. 100 e-mails)
	//         don't intermingle very well with unreserveService, as when we unreserve a reservation
	//         we set the service reservation to zero, but that would wipe any upfront bookings..
	public static void unreserveService(final String service_line_id, final String user_id, final Float number_of_units) {
		final java.util.Map<String, Object> service = Services.findServiceByLineId(service_line_id);
		if (null != service) {
		  /*final */java.util.Map<String, Object> history = null;
			for (final java.util.Map<String, Object> __history: Services.history) {
				final String for_user_id = (String)__history.get("for-user-id");
				if (null != for_user_id && true == for_user_id.equals(user_id)) {
					final java.util.Map<String, Object> lines = (java.util.Map<String, Object>)__history.get("%lines");
					if (true == lines.containsKey(service_line_id)) {
						history = __history;
						break;
					}
				}
			}
			if (null != history) {
			  /*final */java.util.Map<String, Object> lines = (java.util.Map<String, Object>)history.get("%lines");
			  /*final */java.util.Map<String, Object> line = (java.util.Map<String, Object>)lines.get(service_line_id);
				final Float number_of_units_reserved = (Float)line.get("number-of-units:reserved");
			  /*final */Float number_of_units_to_unreserve = number_of_units;
				if (null == number_of_units_to_unreserve) {
				  /*final Float */number_of_units_to_unreserve = (Float)line.get("number-of-units:reserved");
				}
				Bills./*un*/reserveBill("0"/*FIXME*/, user_id, service_line_id, -(number_of_units_to_unreserve.floatValue()));
				final Float number_of_units_remaining = new Float(number_of_units_reserved.floatValue()-number_of_units_to_unreserve.floatValue());
				if (number_of_units_remaining.floatValue() > 0.0f) {
					line.put("number-of-units:reserved", number_of_units_remaining);
				} else if (number_of_units_remaining.floatValue() == 0.0f) {
				  //line.put("number-of-units:reserved", new Float(0.0f));
					line.remove("number-of-units:reserved");
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	public static void unreserveService(final String service_id, final String user_id) {
		Services.unreserveService(service_id, user_id, (Float)null);
	}
	
	public static java.util.List<String> getNextStates(final String state) {
		return Services.state_transitions.get(state);
	}
	
	public static void remove(final Integer /*service_*/id) {
		// TODO: check dependencies (can't delete a service/line if assigned to role/user/group.. etc.
		final java.util.Map<String, Object> service = Services.get(id);
		if (null != service) {
			final java.util.Map<String, Object> /*service_*/lines = (java.util.Map<String, Object>)service.get("%lines");
			if (null != /*service_*/lines) {
			  /*final */java.util.List<String> line_ids = null;
				for (final String /*service_*/line_id: /*service_*/lines.keySet()) {
if (true == line_id.equals("$id")) continue;
				///*final java.util.Map<String, Object> service_*/line = (java.util.Map<String, Object>)/*service_*/lines.get(line_id);
					if (null == /*service_*/line_ids) {
					  /*final java.util.List<String> service_*/line_ids = new java.util.ArrayList<>();
					}
				  /*service_*/line_ids.add(/*service_*/line_id);
				}
				// TODO: is this really necessary, won't dao.util.HashMap do this anyway for free?
				if (null != /*service_*/line_ids) {
					for (final String /*service_*/line_id: /*service_*/line_ids) {
					  /*service_*/lines.remove(/*service_*/line_id);
					}
				}
				service.remove("%lines");
			}
			Services.services.remove((String)service.get("service-id"));
		}
	}
	public static void removeLine(final Integer /*service_*/id, Integer /*service_*/line_id) {
		// TODO: check dependencies (can't delete a service/line if assigned to role/user/group.. etc.
		final java.util.Map<String, Object> service = Services.get(/*service_*/id);
		if (null != service && null != /*service_*/line_id) {
			final int intValue = /*service_*/line_id.intValue();
			final java.util.Map<String, Object> /*service*/_lines = (java.util.Map<String, Object>)service.get("%lines");
			if (null != /*service*/_lines) {
			  /*final */String /*service_*/line_id_to_remove = null;
				for (final String /*service*/_line_id: /*service*/_lines.keySet()) {
if (true == /*service*/_line_id.equals("$id")) continue;
					final java.util.Map<String, Object> /*service*/_line = (java.util.Map<String, Object>)/*service*/_lines.get(/*service*/_line_id);
					if (null != /*service*/_line && null != /*service*/_line.get("$id")) {
						if (intValue == ((Integer)/*service*/_line.get("$id")).intValue()) {
						  /*service_*/line_id_to_remove = (String)/*service*/_line.get("line-id");
							break;
						}
					}
				}
				if (null != /*service_*/line_id_to_remove) {
				  /*service*/_lines.remove(/*service_*/line_id_to_remove);
				}
			}
		}
	}
	public static java.util.List<java.util.Map<String, Object>> getHistoryForServiceId(final Integer /*service_*/id) {
	  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/history_list = null;
		final java.util.Map<String, Object> service = Services.get(/*service_*/id);
		if (null != service) {
			final String service_id = (String)service.get("service-id");
			for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
				if (null != history.get("service-id") && /*service_*/history.get("service-id").equals(service_id)) {
					if (null == /*service_*/history_list) {
					  /*final java.util.List<java.util.Map<String, Object>> service_*/history_list = new java.util.ArrayList<java.util.Map<String, Object>>();
					}
				  /*service_*/history_list.add(/*service_*/history);
				}
			}
		}
		return /*service_*/history_list;
	}
	public static java.util.Map<String, Object> getHistoryById(final Integer /*service_*/history_id) {
	  /*final */java.util.Map<String, Object> /*service_*/history = null;
		if (null != /*service_*/history_id) {
			final int intValue = /*service_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (intValue == ((Integer)/*service*/_history.get("$id")).intValue()) {
				  /*service_*/history = /*service*/_history;
					break;
				}
			}
		}
		return /*service_*/history;
	}
	public static void removeHistory(final Integer /*service_history_*/id) {
		// TODO: check dependencies (can't delete a service/history if assigned to role/user/group.. etc.
		if (null != /*service_history_*/id) {
			final int intValue = id.intValue();
		  /*final */Integer /*service_history*/_id_to_remove = null;
			for (final java.util.Map<String, Object> /*service_*/history: Services.history) {
				final Integer /*service_history*/_id = (Integer)/*service_*/history.get("service-history-id");
				if (null != /*service_history*/_id && /*service_history*/_id.intValue() == intValue) {
				  /*service_history*/_id_to_remove = /*service_history*/_id;
					break;
				}
			}
			if (null != /*service_history*/_id_to_remove) {
				Services.history.remove(/*service_history*/_id_to_remove.intValue()); // FIXME/TODO: now we're hooped!
			}
		}
	}
	public static java.util.Map<String, Object> getLineHistoryById(final Integer /*service_*/history_id, final Integer /*service_history_*/line_id) {
	  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
		if (null != /*service_*/history_id) {
		  /*final */int intValue = /*service_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (intValue == ((Integer)/*service*/_history.get("$id")).intValue()) {
				  /*final int */intValue = /*service_history_*/line_id.intValue();
					final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
					for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
if (true == /*service_history*/_line_id.equals("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
						if (intValue == ((Integer)/*service*/_line_history.get("$id")).intValue()) {
						  /*service_*/line_history = /*service*/_line_history;
							break;
						}
					}
				}
			}
		}
		return /*service_*/line_history;
	}
	public static void removeLineHistoryById(final Integer /*service_*/history_id, final Integer /*service_history_*/line_id) {
		if (null != /*service_*/history_id) {
		  /*final */int intValue = /*service_*/history_id.intValue();
			for (final java.util.Map<String, Object> /*service*/_history: Services.history) {
				if (intValue == ((Integer)/*service*/_history.get("$id")).intValue()) {
				  /*final int */intValue = /*service_history_*/line_id.intValue();
					final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines");
				  /*final */String /*service_history_*/line_id_to_remove = null;
					for (final String /*service_history*/_line_id: /*service*/_lines_history.keySet()) {
if (true == /*service_history*/_line_id.equals("$id")) continue;
						final java.util.Map<String, Object> /*service*/_line_history = (java.util.Map<String, Object>)/*service*/_lines_history.get(/*service_history*/_line_id);
						if (intValue == ((Integer)/*service*/_line_history.get("$id")).intValue()) {
						  /*service_history_*/line_id_to_remove = /*service_history*/_line_id;
							break;
						}
					}
					if (null != /*service_history_*/line_id_to_remove) {
					  /*service*/_lines_history.remove(/*service_history_*/line_id_to_remove);
					}
				}
			}
		}
	}
}