package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServicesLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
	  /*final */java.util.List<String> __services = new java.util.ArrayList<>(Services.services.keySet());
	  /*final */java.util.List<java.util.Map<String, Object>> /*services_*/history = null;
		
	  /*final */java.util.Map<String, Object> __role = null;
	  /*final */java.util.Map<String, Object> __user = null;
	  /*final */java.util.Map<String, Object> __group = null;
		
	  /*final */String __role_id = (String)req.getParameter("role-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__role_id)) __role_id = null;
		if (null != __role_id) {
			final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)super.property("roles");
		  /*final java.util.Map<String, Object> */__role = roles.get(__role_id);
			__services = (java.util.List<String>)__role.get("services");
			req.setAttribute("role-id", __role_id);
		}
		
	  /*final */String __user_id = (String)req.getParameter("user-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__user_id)) __user_id = null;
		if (null != __user_id) {
			final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)super.property("users");
		  /*final java.util.Map<String, Object> */__user = users.get(__user_id);
			__services = (java.util.List<String>)__user.get("services");
			req.setAttribute("user-id", __user_id);
		}
		
	  /*final */String __group_id = (String)req.getParameter("group-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__group_id)) __group_id = null;
		if (null != __group_id) {
			final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)super.property("groups");
		  /*final java.util.Map<String, Object> */__group = groups.get(__group_id);
			__services = (java.util.List<String>)__group.get("services");
			req.setAttribute("group-id", __group_id);
		}
		
		final String mode = req.getParameter("mode");
		final String submit = req.getParameter("__submit");
		
	  /*final */java.util.Map<String, Object> services = null;
		
	  /*final */String path = uri.replace("/hci", "");
		
		if (null == submit || 0 == submit.trim().length()) {
			if (null == req.getParameter("history")) {
			  /*final java.util.List<String> */__services = new java.util.ArrayList<>(Services.services.keySet());
				for (final String __service_id: __services) {
if (true == __service_id.equals("$id")) continue;
					final java.util.Map<String, Object> __service = (java.util.Map<String, Object>)Services.services.get(__service_id);
				  //final String id = __service.get("$id").toString();
				  //req.setAttribute("service$"+id+"#checked", false);
					if (null == services) {
					  /*final java.util.Map<String, Object> */services = new java.util.HashMap<String, Object>();
					}
					services.put(__service_id, __service);
				}
			} else {
			  /*final java.util.List<java.util.Map<String, Object>> */history = Services.history;
			}
		} else if (null != submit && true == "delete".equals(submit)) {
			if (null == req.getParameter("history")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final String[] __service_ids = map.get("service-ids");
				for (int i = 0; i < __service_ids.length; i++) {
					final String checked = req.getParameter("service$"+__service_ids[i]+"#checked");
					final java.util.Map<String, Object> __service = Services.get(new Integer(__service_ids[i]));
					if (null != checked && true == checked.equals("on")) {
						if (null != __service) { // Note: until we prevent duplicate form submission, we have to check we've not already deleted the service
							Services.remove((Integer)__service.get("$id"));
						}
					}
				}
				req.setAttribute("services", Services.services);
			} else {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final String[] /*__service*/_history_ids = map.get("service-history-ids");
				for (int i = 0; i < /*__service*/_history_ids.length; i++) {
					final String checked = req.getParameter("service-history$"+/*__service*/_history_ids[i]+"#checked");
					if (null != checked && true == checked.equals("on")) {
						final java.util.Map<String, Object> /*__service*/_history = Services.getHistoryById(new Integer(/*__service*/_history_ids[i]));
						if (null != /*__service*/_history) { // Note: until we prevent duplicate form submission, we have to check we've not already deleted the service
							final Integer /*__service_history*/_id = (Integer)/*__service*/_history.get("service-history-id");
							if (null != /*__service_history*/_id) {
								Services.history.remove(/*__service_history*/_id.intValue());
							}
						}
					}
				}
				req.setAttribute("services#history", Services.history);
			}
		} // Good, to here.
		
		else if (null == req.getParameter("history")) { // services
			if (null != submit && 0 < submit.trim().length()) {
			  /*final */boolean complete = false;
				if (true == submit.equals("update") || true == submit.equals("delete")) {
					final java.util.Map<String, String[]> map = req.getParameterMap();
					final String[] __service_ids = map.get("service-ids");
					for (int i = 0; i < __service_ids.length; i++) {
						final String checked = req.getParameter("service$"+__service_ids[i]+"#checked");
						final java.util.Map<String, Object> __service = Services.get(new Integer(__service_ids[i]));
					  //final String __service_id = (String)__service.get("service-id");
						if (false == java.lang.Boolean.TRUE.booleanValue()) {
						} else if (true == submit.equals("update")) {
							if (false == java.lang.Boolean.TRUE.booleanValue()) {
						  /*} else if (null != __role_id) {
								if (false == Groups.hasPermission(__service, __role_id)) {
									if (null != checked && true == checked.equals("on")) {
										Groups.addPermission(__service, __role_id);
									}
								} else {
									if (null == checked || false == checked.equals("on")) {
										Groups.removePermission(__service, __role_id);
									}
								}
								complete = true;
							} else if (null != __user_id) {
								if (false == Groups.hasRole(__service, __user_id)) {
									if (null != checked && true == checked.equals("on")) {
										Groups.addRole(__service, __user_id);
									}
								} else {
									if (null == checked || false == checked.equals("on")) {
										Groups.removeRole(__service, __user_id);
									}
								}
								complete = true;
							} else if (null != __group_id) {
								if (false == Groups.hasUser(__service, __group_id)) {
									if (null != checked && true == checked.equals("on")) {
										Groups.addUser(__service, __group_id);
									}
								} else {
									if (null == checked || false == checked.equals("on")) {
										Groups.removeUser(__service, __group_id);
									}
								}
								complete = true;*/
							}
						} else if (true == submit.equals("delete")) {
							if (null != checked && true == checked.equals("on")) {
								Services.remove((Integer)__service.get("$id"));
							}
						} else {
							throw new IllegalStateException();
						}
					}
				  /*final java.util.List<String> */__services = new java.util.ArrayList<>(Services.services.keySet());
						for (final String __service_id: __services) {
if (true == __service_id.equals("$id")) continue;
						final java.util.Map<String, Object> __service = (java.util.Map<String, Object>)Services.services.get(__service_id);
					  //final String id = __service.get("$id").toString();
					  //req.setAttribute("service$"+id+"#checked", false);
						if (null == services) {
						  /*final java.util.Map<String, Object> */services = new java.util.HashMap<String, Object>();
						}
						services.put(__service_id, __service);
					}
				}
				if (false == complete) {
				  /*final String */path = uri.replace("/hci", "");
				} else {
				  /*final String */path = "/index";
					req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />"); // TODO: url encoding
				}
			}
		} else { // services#history
			if (null == submit || 0 == submit.trim().length()) {
			  /*final java.util.List<java.util.Map<String, Object>> */history = Services.history;
			} else if (true == submit.equals("delete")) {
			  //Services.history.remove(/*service_*/history_id);
			}
		}
		
		if (null != mode) {
			req.setAttribute("mode", mode);
		}
		
		if (null != services) {
			req.setAttribute("services", services);
		}
		if (null != /*services_*/history) {
			req.setAttribute("services#history", /*services_*/history);
		}
		
	  /*final String */path = uri.replace("/hci", "");
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}