package hci.j2ee;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static hci.j2ee.ServletTool.SIGN_IN_METHODS;

import org.apache.http.HttpStatus;

public class SignInLogic extends Logic {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		final Map<String, String[]> pm = req.getParameterMap();
		final String[] sa = (String[])pm.get("json");
		final Boolean json = null == sa || 0 == sa.length ? null : new Boolean(sa[0]);
if (null != json && true == json) {
	  /*final */String key = null;
		if (null != ServletTool.get(session, "$password-reset-token")) {
			key = "password-reset-token";
		} else {
			throw new IllegalStateException();
		}
		t.put("String:"+key, (String)ServletTool.get(session, "$"+key));
		final java.util.Map<String, Object> token = Tokens.getToken((String)t.get("String:"+key));
		final Boolean confirmed = (Boolean)token.get("$confirmed");
		final String response = "{ \"token\": \""+(String)t.get("String:"+key)+"\", \"confirmed\": "+(null != confirmed && true == confirmed)+" }";
		resp.setContentType("application/json");
		resp.getWriter().print(response);
} else {/**/
		
		ServletTool.processSignLogic(req, this);
		
		final String token = req.getParameter("token");
		if (null != req.getParameter("step")) {
			r.put("String:sign-in-step", req.getParameter("step"));
			try {
				t.put("Integer:sign-in-step", Integer.parseInt((String)r.get("String:sign-in-step")));
			} catch (final NumberFormatException e) {
				// Silently ignore.. (for now)
			}
		} else if (null == ServletTool.get(session, "sign-in-step")) {
			t.put("Integer:sign-in-step", 00);
			if (null != token) {
				final java.util.Map<String, Object> map = (java.util.Map<String, Object>)Tokens.tokens.get(token);
				if (null != map && null != map.get("$type")) {
					if (true == ((String)map.get("$type")).equals("1-time-sign-in")) {
						ServletTool.put(session, "user-id", map.get("user-id"));
						t.put("Integer:sign-in-step", 22);
					}
					if (true == ((String)map.get("$type")).equals("password-reset")) {
						ServletTool.put(session, "user-id", map.get("user-id"));
						t.put("Integer:sign-in-step", 21);
					}
				}
			}
		} else {
			s.put("Integer:sign-in-step", (Integer)ServletTool.get(session, "sign-in-step"));
			t.put("Integer:sign-in-step", s.get("Integer:sign-in-step"));
			if (null != token) {
				final java.util.Map<String, Object> map = (java.util.Map<String, Object>)Tokens.tokens.get(token);
				if (null != map) {
					ServletTool.put(session, "user-id", map.get("user-id"));
					t.put("Integer:sign-in-step", 31);
				}
			}
		}
		
	  /*final */String sign_id = req.getParameter("sign-id");
		if (null == sign_id) {
			sign_id = (String)ServletTool.get(session, "sign-id");
		}
		{
		  /*final */Cookie cookie = null;
			final Cookie[] __cookies = req.getCookies();
			if (null != __cookies) {
				for (final Cookie __cookie: __cookies) {
					if (true == __cookie.getName().equals("itfromblighty")) {
						cookie = __cookie;
					}
				}
			}
		///*final */String sign_id = req.getParameter("sign-id");
		  /*final */String checked = req.getParameter("remember-me");
		  /*final */Boolean remember_me = null;
			if (null != checked) {
			  /*final Boolean */remember_me = new Boolean(true);
				if (0 < checked.trim().length()) {
					if (null == cookie) {
						final String value = "sign-id="+sign_id+";remember-me="+remember_me;
					  /*final Cookie */cookie = new Cookie("itfromblighty", Base64.getEncoder().encodeToString(value.getBytes()));
						final URL url = new URL(req.getRequestURL().toString());
					  //final String port = -1 == url.getPort() ? "" : ":"+url.getPort();
						cookie.setDomain(url.getHost()/*+port*/);
						cookie.setPath(/*url.getPath()*/"/hci");
					}
					cookie.setMaxAge(60/*seconds*/ * 60/*minutes*/ * 24/*hours*/ * 30/*days*/);
				} else {
					cookie.setValue("sign-id="+sign_id+";remember-me="+remember_me);
					cookie.setMaxAge(0);
				}
				resp.addCookie(cookie);
			} else {
				if (null != cookie) {
					final String submit = req.getParameter("__submit");
					if (null == submit) {
						final String base64 = cookie.getValue();
						final String value = new String(Base64.getDecoder().decode(base64));
						if (null != value) {
							final String[] pairs = value.split(";");
							for (final String pair: pairs) {
								final String[] __pair = pair.split("=", 2);
								if (false == Boolean.TRUE.booleanValue()) {
								} else if (true == __pair[0].equals("sign-id") && 2 == __pair.length) {
									sign_id = __pair[1];
								} else if (true == __pair[0].equals("remember-me") && 2 == __pair.length) {
									remember_me = new Boolean(__pair[1]);
								} else {
								  //throw new IllegalArgumentException();
								}
							}
						}
						if (true == remember_me) {
							cookie.setMaxAge(60/*seconds*/ * 60/*minutes*/ * 24/*hours*/ * 30/*days*/);
						} else {
							cookie.setMaxAge(0);
						}
					} else {
						cookie.setMaxAge(0);
					}
					resp.addCookie(cookie);
				}
			}
			req.setAttribute("sign-id", sign_id);
			if (null != remember_me) {
				req.setAttribute("remember-me", remember_me);
			} else {
			  //req.removeAttribute("remember-me"); // Unnecessary
			}
		}
		
	  /*final */String sign_in_method = req.getParameter("sign-in-method");
		if (null == sign_in_method) {
			if (false == req.getMethod().equals("GET") || null == req.getParameter("token")) {
				sign_in_method = (String)ServletTool.get(session, "sign-in-method");
			} else {
				sign_in_method = "html-form"; // Fake the sign-in-method as this came from a URL sent in an e-mail/sms
			}
		}
		
		if (00 == (Integer)t.get("Integer:sign-in-step")) { // Initial form pre-fill
			
			final String submit = req.getParameter("__submit");
			if (null == submit) {
				ServletTool.put(session, "sign-in-step", 00);
			} else if (true == submit.trim().toLowerCase().equals("continue")) {
				t.put("Integer:sign-in-step", 10);
				ServletTool.put(session, "sign-in-step", 10);
			} else if (0 <= submit.trim().toLowerCase().indexOf("1-time")) {
				t.put("Integer:sign-in-step", 12);
				ServletTool.put(session, "sign-in-step", 12);
			} else if (0 <= submit.trim().toLowerCase().indexOf("forgot")) {
				t.put("Integer:sign-in-step", 11);
				ServletTool.put(session, "sign-in-step", 11);
			} else {
				throw new IllegalStateException();
			}
			
		}
		
	  /*final */Integer sign_in_step = (Integer)t.get("Integer:sign-in-step");
		
		if (10 == sign_in_step || 11 == sign_in_step || 12 == sign_in_step) { // Form-filled validation..
		
			if (true == SIGN_IN_METHODS.contains(sign_in_method)) {
				if (true == "default".equals(sign_in_method)) {
					sign_in_method = "html-form";
				}
				if (true == "html-form".equals(sign_in_method)) {
					
					final String submit = req.getParameter("__submit");
					if (null == submit) {
						req.setAttribute("sign-id", ServletTool.get(session, "sign-id"));
						req.setAttribute("password", ServletTool.get(session, "password"));
						ServletTool.put(session, "sign-in-step", sign_in_step);
						req.setAttribute("sign-in-step", sign_in_step);
					} else if (
							(true == submit.trim().toLowerCase().equals("continue"))
							 ||
							(0 <= submit.trim().toLowerCase().indexOf("1-time"))
							 ||
							(0 <= submit.trim().toLowerCase().indexOf("reset"))
							 ||
							(0 <= submit.trim().toLowerCase().indexOf("forgot"))
						) {
						
					///*final String */sign_id = req.getParameter("sign-id");
						if (null == sign_id) {
						  /*final String */sign_id = (String)ServletTool.get(session, "sign-id");
						}
						final java.util.Map<String, Object> user = Users.findUser(sign_id);
						final java.util.Map<String, String> results = new java.util.HashMap<>();
						if (0 > submit.trim().toLowerCase().indexOf("forgot") || 0 > submit.trim().toLowerCase().indexOf("1-time")) {
							if (null == sign_id || 0 == sign_id.trim().length()) {
								results.put("sign-id", "please specify a user-id, e-mail address, or sms-capable phone number");
							}
						}
					  /*final */String password = null;
						if (10 == sign_in_step && 0 > submit.trim().toLowerCase().indexOf("forgot")) {
						  /*final String */password = req.getParameter("password");
							if (null == password || 0 == password.trim().length()) {
								results.put("password", "please specify a password");
							} else {
								if (null != user && null != password && true == password.equals(user.get("password"))) {
								  //ServletTool.put(session, "sign-in-method", sign_in_method);
									ServletTool.put(session, "sign-id", sign_id);
									ServletTool.put(session, "password", password);
								} else {
									if (null != user) {
										Integer sign_in_attempts_failed = null == user.get("sign-in-attempts-failed") ? 0 : (Integer)user.get("sign-in-attempts-failed");
										user.put("sign-in-attempts-failed", sign_in_attempts_failed+1);
									}
									results.put("password", "sign-id and specified password do not match");
								}
							}
						} else { // if (11 == sign_in_step || 0 <= submit.trim().toLowerCase().indexOf("forgot")) {
							if (0 <= submit.trim().toLowerCase().indexOf("1-time")) {
								sign_in_step = 12;
								t.put("Integer:sign-in-step", sign_in_step);
								ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
							} else if (11 != sign_in_step) { // It may be that sign_in_step is 10 but we clicked 'cancel' from password-reset screen..
								sign_in_step = 11;
								t.put("Integer:sign-in-step", sign_in_step);
								ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
							}
							// Used if we hit cancel in password-reset screen
							ServletTool.put(session, "sign-id", sign_id);
							ServletTool.put(session, "password", req.getParameter("password"));
							if (null != user) {
							} else {
							  //results.put("{sign-id}", "<no-such-sign-id>"); // Never show this! Lie
							}
						}
						if (null == results || 0 == results.size()) {
							ServletTool.put(session, "sign-in-method", sign_in_method); // Done in ServletTool.processSignLogic
						  //ServletTool.put(session, "sign-id", sign_id);
						  //ServletTool.put(session, "password", password);
							if (10 == sign_in_step) {
								t.put("Integer:sign-in-step", 20);
							} else if (0 <= submit.trim().toLowerCase().indexOf("1-time")) { // if (12 == sign_in_step) {
								t.put("Integer:sign-in-step", 22);
							} else if (0 <= submit.trim().toLowerCase().indexOf("reset")) { // if (11 == sign_in_step) {
								t.put("Integer:sign-in-step", 21);
							}
							ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
						} else {
							if (null != sign_in_method) {
								req.setAttribute("sign-in-method", sign_in_method);
							}
							if (null != sign_id) {
								req.setAttribute("sign-id", sign_id);
							}
							if (null != password) {
								req.setAttribute("password", password);
							}
							req.setAttribute("results", results);
						}
					}
					
				} else if (true == "basic-auth".equals(sign_in_method)) {
					
					final String[] values = ServletTool.doAuthorization(req);
				  /*final String */sign_id = null == values || values.length < 1 ? null : values[0];
					req.setAttribute("sign-id", sign_id);
					final String password = null == values || values.length < 2 ? null : values[1];
					
					final java.util.Map<String, Object> user = Users.findUser(sign_id);
					
					if (null != user && true == password.equals(user.get("password"))) {
						
						ServletTool.put(session, "sign-in-method", sign_in_method);
						ServletTool.put(session, "sign-id", sign_id);
						ServletTool.put(session, "password", password);
						
					  /*final */String passphrase = (String)user.get("passphrase");
						if (null == passphrase || 0 == passphrase.length()) {
							passphrase = ServletTool.generateString(16);
							req.setAttribute("passphrase", passphrase);
							ServletTool.put(session, "passphrase-generated", Boolean.TRUE);
						} else {
							ServletTool.put(session, "passphrase-generated", Boolean.FALSE);
						}
						ServletTool.put(session, "passphrase", passphrase);
						req.setAttribute("passphrase-generated", ServletTool.get(session, "passphrase-generated"));
						if (Boolean.FALSE == ServletTool.get(session, "passphrase-generated")) {
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signing-in?session-id="+session_id+"'\" />");
						}
						final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signing-in.jsp");
						requestDispatcher.include(req, resp);
					
					} else {
						
						if (null != user) {
							Integer sign_in_attempts_failed = null == user.get("sign-in-attempts-failed") ? 0 : (Integer)user.get("sign-in-attempts-failed");
							user.put("sign-in-attempts-failed", sign_in_attempts_failed+1);
						}
						final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.jsp");
						requestDispatcher.include(req, resp);
						resp.setHeader("WWW-Authenticate", "Basic realm=\"Zer0ne\""); // TODO: switch realm (here and elsewhere) to append a sequence/unique number so that a second logic (in a different session-id in a different tab) can be a different user - i.e. browser sends different basic auth credentials accordingly
						resp.setStatus(HttpStatus.SC_UNAUTHORIZED);
						
					}
					
				} else if (true == "client-ssl".equals(sign_in_method)) {
					
					throw new UnsupportedOperationException(); // For now
					
				} else {
					
					throw new IllegalArgumentException();
					
				}
			}
		}
		
		if (20 == (Integer)t.get("Integer:sign-in-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signing-in?session-id="+session_id+"'\" />");
			// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
	  /*final Integer */sign_in_step = (Integer)t.get("Integer:sign-in-step");
		// Ensure nothing comes from session (this is a new empty session)
		if (21 == sign_in_step || 22 == sign_in_step) { // Form-filled validation..
			
			final String submit = req.getParameter("__submit");
			if (null == submit) {
				if (null == req.getAttribute("sign-id")) {
				  /*final String */sign_id = (String)ServletTool.get(session, "sign-id");
					req.setAttribute("sign-id", sign_id); // Auto form-fill (for show)
				}
		  //} else if (0 <= submit.trim().toLowerCase().indexOf("forgot")) {
			} else if (
				(0 <= submit.trim().toLowerCase().indexOf("1-time"))
				 ||
				(0 <= submit.trim().toLowerCase().indexOf("reset"))
			) {
				
				if (null == sign_id) {
				  /*final String */sign_id = (String)ServletTool.get(session, "sign-id");
				}
			  /*final */java.util.Map<String, Object> user = null;
				final java.util.Map<String, String> results = new java.util.HashMap<>();
				if (null == sign_id || 0 == sign_id.trim().length()) {
					results.put("sign-id", "please specify a user-id, e-mail address, or sms-capable phone number");
				} else {
				  /*final java.util.Map<String, Object> */user = Users.findUser(sign_id);
				}
				if (null == user) {
					results.put("{sign-id}", "<no-such-sign-id>"); // Never show this! Lie
				}
				if (null == results || 0 == results.size()) {
					
					ServletTool.put(session, "sign-id", sign_id);
					
				  /*final */Long number = null; // Java/Eclipse is too dumb to recognize that try or catch must succeed and complains final number may be uninitialised.. 
					try {
						number = Long.parseLong(sign_id);
					} catch (final NumberFormatException e) {
					  //number = null;
					}
					
					if (0 <= submit.trim().toLowerCase().indexOf("1-time")) {
						t.put("String:1-time-sign-in-token", ServletTool.generateString(4));
					}
					if (0 <= submit.trim().toLowerCase().indexOf("reset")) {
						t.put("String:password-reset-token", ServletTool.generateString(4));
					}
					
				  /*final */String type = null;
					final String __refresh = req.getParameter("refresh");
					final Integer refresh = null == __refresh || 0 == __refresh.trim().length() ? 0 : Integer.parseInt(__refresh);
				  /*final */Integer state = null;
					if (0 == refresh) {
						if (null == number) {
						  /*final String */type = "email";
						} else {
						  /*final String */type = "sms";
						}
					}
					if (0 == refresh) {
						final dao.util.HashMap<String, Object> map = new dao.util.HashMap<>();
					  //if (21 == sign_in_step) {
							map.put("token-id", t.get("String:password-reset-token"));
					  //}
						map.put("user-id", user.get("user-id"));
						map.put("$type", type);
						map.put("$date+time", System.currentTimeMillis());
						map.put("$confirmed", Boolean.FALSE);
					  //if (21 == sign_in_step) {
							ServletTool.put(session, "$password-reset-token", t.get("String:password-reset-token"));
							synchronized(Tokens.tokens) {
								Tokens.tokens.put((String)t.get("String:password-reset-token"), map);
							}
					  //}
					}
					if (0 == refresh) {
					  //user.put("confirmed-"+type, Boolean.FALSE); // Beware, same field is used in SigningUpLogic.java
					}
					if (0 == refresh) {
						final String user_id = (String)user.get("user-id");
					  /*final */java.util.Map<String, Object> __results = null;
						final String template;// = null; 
					  //if (21 == sign_in_step) {
						  /*final String */template = "password-reset";
							if (null == number) {
							  /*final java.util.Map<String, Object> */__results = Emails.sendEmail(template, "2"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
									private static final long serialVersionUID = 0L;
									{
										put(template+"-token", new String[]{(String)t.get("String:"+template+"-token")});
									}
								});
							} else {
							  /*final java.util.Map<String, Object> */__results = Texts.sendText(template, "2"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
									private static final long serialVersionUID = 0L;
									{
										put(template+"-token", new String[]{(String)t.get("String:"+template+"-token")});
									}
								});
							}
					  //} else {
					  //	throw new IllegalStateException();
					  //}
						if (null != __results && 0+1 < __results.size()) {
							for (final String __user_id: __results.keySet()) {
if (true == __user_id.equals("")) continue;
								final Object o = __results.get(__user_id);
								if (true == o instanceof java.util.Map) {
									final java.util.Map<String, Object> history = (java.util.Map<String, Object>)o;
									history.put("$token", t.get("String:"+template+"-token"));
								}
							}
						}
					}
					try {
						Thread.sleep(1000L*(long)Math.pow(2.0, refresh));
					} catch (final InterruptedException e) {
					  //e.printStackTrace(); // Silently ignore..
					}
					if (21 == sign_in_step) {
					  /*final Integer */state = (Integer)user.get("password-reset-"+type+"-state");
					}
					if (state < 0 || refresh >= 5) {
						if (0 <= submit.trim().toLowerCase().indexOf("1-time")) {
							t.put("Integer:sign-in-step", -22);
						}
						if (0 <= submit.trim().toLowerCase().indexOf("reset")) {
							t.put("Integer:sign-in-step", -21);
						}
						ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
					} else if (state >= 4) {
						if (0 <= submit.trim().toLowerCase().indexOf("1-time")) {
							t.put("Integer:sign-in-step", +32); // TODO: redirect to signing-in.jsp (instead of sign_in.jsp)
						}
						if (0 <= submit.trim().toLowerCase().indexOf("reset")) {
							t.put("Integer:sign-in-step", +31);
						}
						ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
					} else {
						req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/signing-in?refresh="+(1+refresh)+"&session-id="+session_id+"'\" />");
					}
						
				} else {
					
					if (null != sign_id) {
						req.setAttribute("sign-id", sign_id);
					}
					req.setAttribute("results", results);
					
				}
			}
			
		}
		
		if (-21 == (Integer)t.get("Integer:sign-in-step")) {
			
			// TODO: timeout sending password-reset e-mail/sms?
			
		}
		
	  /*final Integer */sign_in_step = (Integer)t.get("Integer:sign-in-step");
		
		if (31 == sign_in_step || 32 == sign_in_step) {
			
			final String submit = req.getParameter("__submit");
			if (null == submit || true == submit.trim().toLowerCase().contains("continue")) {
				final java.util.Map<String, String> results = new java.util.HashMap<>();
			  /*final */String user_id = (String)ServletTool.get(session, "user-id");
			  /*final */java.util.Map<String, Object> map = null;
				if (null == token || 0 == token.trim().length()) {
					if (null != submit) {
						results.put("token", "please enter a token");
					}
				} else {
				  /*final java.util.Map<String, Object> */map = (java.util.Map<String, Object>)Tokens.tokens.get(token);
				}
				if (null == map) {
					if (null != submit && false == results.containsKey("token")) {
						results.put("token", "please enter a valid token");
					}
				} else {
				  //final Long ts = (Long)map.get("$date+ms");
				  /*final String */user_id = (String)map.get("user-id");
					final String type = (String)map.get("$type");
					final java.util.Map<String, Object> user = Users.users.get(user_id);
if (32 == sign_in_step) {
	if (null != user.get("1-time-sign-in-"+type+"-state")) {
		user.remove("1-time-sign-in-"+type+"-state");
	}
}
if (31 == sign_in_step) {
	if (null != user.get("reset-password-"+type+"-state")) {
		user.remove("reset-password-"+type+"-state");
	}
	if (null != map.get("password")) {
					user.put("password", (String)map.get("password"));
		map.remove("password");
	}
}
				  //final Boolean confirmed = (Boolean)map.get("$confirmed");
					map.put("$confirmed", Boolean.FALSE);
//tokens.remove(token); // Let's keep tokens for analysis
					if (null == submit) {
						if (32 == sign_in_step) {
							t.put("Integer:sign-in-step", 42);
						}
						if (31 == sign_in_step) {
							t.put("Integer:sign-in-step", 41);
						}
					} else {
						if (0 <= submit.trim().toLowerCase().indexOf("reset")) {
							ServletTool.put(session, "user", user);
							req.setAttribute("user", user); // We'd like to, not, and just use req.setAttribute(*, ..) but it seems header.jsp still needs <user> and 'sign-out' is hooked on "user" (not "signed-in-user")
							req.setAttribute("user-id", user_id);
							req.setAttribute("preferred-name", (String)user.get("preferred-name"));
							req.setAttribute("email", (String)user.get("email"));
							req.setAttribute("confirm-email", (String)user.get("confirm-email"));
							req.setAttribute("sms", (String)user.get("sms"));
							req.setAttribute("confirm-sms", (String)user.get("confirm-sms"));
							if (null != submit && 0 <= submit.trim().toLowerCase().indexOf("reset")) {
								final String password = req.getParameter("password");
								if (null != password) {
									req.setAttribute("password", password);
								}
								final String validate = req.getParameter("password-validate");
								if (null != validate) {
									req.setAttribute("password-validate", validate);
								}
								if (null == password || 0 == password.trim().length()) {
									results.put("password", "please provide a password");
								} else if (null == validate || 0 == validate.length() || false == validate.equals(password)) {
									results.put("password-validate", "passwords do not match");
								} else {
//Tokens.tokens.remove(token); // Let's keep tokens for analysis
									user.put("password", password);
									req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/sign-in?session-id="+session_id+"'\" />");
								}
							}
							t.put("Integer:sign-in-step", 51);
						  //req.setAttribute("passphrase", (String)user.get("passphrase")); // We don't want passphrase..
						}
						if (0 <= submit.trim().toLowerCase().indexOf("1-time")) {
							t.put("Integer:sign-in-step", 52);
						}
					}
					ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
					req.setAttribute("token", token);
				}
				if (null != results && 0 < results.size()) {
					if (null != user_id) {
						req.setAttribute("sign-id", user_id);
					}
					if (null != token) {
						req.setAttribute("token", token);
					}
					req.setAttribute("results", results);
				}
			}
			
		}
		
	  /*final Integer */sign_in_step = (Integer)t.get("Integer:sign-in-step");
		
		if (41 == sign_in_step || 42 == sign_in_step) {
			if (null == req.getAttribute("sign-id")) {
			  /*final String */sign_id = (String)ServletTool.get(session, "sign-id");
				req.setAttribute("sign-id", sign_id); // Auto form-fill (for show)
			}
			
		  /*final String */sign_id = (String)ServletTool.get(session, "sign-id"); // TODO: is this enough protection?
			final java.util.Map<String, Object> user = Users.findUser(sign_id);
			if (null != user) {
				ServletTool.put(session, "signing-in-method", ServletTool.get(session, "sign-in-method"));
ServletTool.put(session, "signed-in-method", ServletTool.get(session, "sign-in-method"));
			  /*if (null != user.get("sign-in-this-date+time")) {
					user.put("sign-in-last-successful", user.get("sign-in-this-date+time"));
				}
				user.put("sign-i7n-this-date+time", sdf.format(new java.util.Date()));*/
				ServletTool.put(session, "user", user);
			}
			
			if (42 == sign_in_step) {
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signing-in?session-id="+session_id+"'\" />");
				// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			}
			if (41 == sign_in_step) {
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/user?session-id="+session_id+"'\" />");
				// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			}
			
		}
		
	  /*final Integer */sign_in_step = (Integer)t.get("Integer:sign-in-step");
		
		final String path;// = null;
		if (51 == sign_in_step || 52 == sign_in_step) {
			path = "/signed-in.jsp";
			
		  /*final String */sign_id = (String)ServletTool.get(session, "sign-id"); // TODO: is this enough protection?
			final java.util.Map<String, Object> user = Users.findUser(sign_id);
			if (null != user) {
				ServletTool.put(session, "signing-in-method", ServletTool.get(session, "sign-in-method"));
ServletTool.put(session, "signed-in-method", ServletTool.get(session, "sign-in-method"));
				if (null != user.get("sign-in-this-date+time")) {
					user.put("sign-in-last-successful", user.get("sign-in-this-date+time"));
				}
				user.put("sign-in-this-date+time", sdf.format(new java.util.Date()));
				ServletTool.put(session, "user", user);
			}
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signed-in?session-id="+session_id+"'\" />");
			// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
		} else {
			path = "/sign-in.jsp";
		}
		
		req.setAttribute("sign-in-step", (Integer)t.get("Integer:sign-in-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
		requestDispatcher.include(req, resp);
	}

}/**/

}