package hci.j2ee;

import static hci.j2ee.ServletTool.SIGN_IN_METHODS;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignOutLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		ServletTool.processSignLogic(req, this);
		
		final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		if (null != req.getParameter("step")) {
			r.put("String:sign-out-step", req.getParameter("step"));
			try {
				t.put("Integer:sign-out-step", Integer.parseInt((String)r.get("String:sign-out-step")));
			} catch (final NumberFormatException e) {
				// Silently ignore.. (for now)
			}
		} else if (null == ServletTool.get(session, "sign-out-step")) {
			t.put("Integer:sign-out-step", /*00*/10);
@SuppressWarnings("unchecked")
final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
if (null != user) {
req.setAttribute("sign-id", user.get("user-id")); // TODO: carry sign-id all the way through the sign-in lifecycle
}
		} else {
			s.put("Integer:sign-out-step", (Integer)ServletTool.get(session, "sign-out-step"));
			t.put("Integer:sign-out-step", s.get("Integer:sign-out-step"));
		}
		
		if (00 == (Integer)t.get("Integer:sign-out-step")) { // Initial form pre-fill
			
			final String submit = req.getParameter("submit");
			if (null == submit) {
				req.setAttribute("sign-id", ServletTool.get(session, "user-id"));
				t.put("Integer:sign-out-step", 10);
				ServletTool.put(session, "sign-out-step", t.get("Integer:sign-out-step"));
			} else if (true == submit.trim().toLowerCase().equals("continue")) {
				req.setAttribute("sign-id", ServletTool.get(session, "user-id"));
				t.put("Integer:sign-out-step", 20);
				ServletTool.put(session, "sign-out-step", t.get("Integer:sign-out-step"));
			} else {
				throw new IllegalStateException();
			}
			
		}
		
	  /*final */String sign_out_method = (String)ServletTool.get(session, "signed-in-method");
		if (null == sign_out_method) {
/*final String */sign_out_method = (String)ServletTool.get(session, "signed-up-method");
		}
		
		if (10 == (Integer)t.get("Integer:sign-out-step")) { // Form-filled validation..
			
			if (null != sign_out_method && true == SIGN_IN_METHODS.contains(sign_out_method)) {
				
				@SuppressWarnings("unchecked")
				final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
				
				if (null != user && true == "html-form".equals(sign_out_method)) {
					
					req.setAttribute("sign-id", ServletTool.get(session, "user-id"));
					ServletTool.put(session, "sign-out-method", ServletTool.get(session, "signed-in-method"));
				  //t.put("Integer:sign-out-step", 20); // Don't do this otherwise we'll skip "Are you sure?"
					ServletTool.put(session, "sign-out-step", /*t.get("Integer:sign-out-step")*/20);
					
				} else if (null != user && true == "basic-auth".equals(sign_out_method)) {
					
					final String[] values = ServletTool.doAuthorization(req);
				  //final String username = null == values || values.length < 1 ? null : values[0];
					final String passphrase = null == values || values.length < 2 ? null : values[1];
					//if (null != user && null != passphrase && true == passphrase.equals(user.get("passphrase"))) { // passphrase isn't necessarily in user (it might be temporary in session) so we can't perform this check here..
					if (null != user && null != passphrase) {
						ServletTool.put(session, "sign-out-method", ServletTool.get(session, "signed-in-method"));
					  //t.put("Integer:sign-out-step", 20); // Don't do this otherwise we'll skip "Are you sure?"
						ServletTool.put(session, "sign-out-step", /*t.get("Integer:sign-out-step")*/20);
					} else {
						throw new IllegalStateException(); // ??
					}
					
				} else if (null != user && true == "client-ssl".equals(sign_out_method)) {
					
					throw new UnsupportedOperationException(); // For now
					
				} else {
					throw new UnsupportedOperationException();
				}
			}
			
		}
		
		if (20 == (Integer)t.get("Integer:sign-out-step")) {
			
			if (null != sign_out_method) {
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signing-out?session-id="+session_id+"'\" />");
				// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			}
			
		}
		
		req.setAttribute("sign-out-step", (Integer)t.get("Integer:sign-out-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/sign-out.jsp");
		requestDispatcher.include(req, resp);
	}
}