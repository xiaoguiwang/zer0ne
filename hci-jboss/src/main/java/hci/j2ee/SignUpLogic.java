package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignUpLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		ServletTool.processSignLogic(req, this);
		
		final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		if (null != req.getParameter("step")) {
			r.put("String:sign-up-step", req.getParameter("step"));
			try {
				t.put("Integer:sign-up-step", Integer.parseInt((String)r.get("String:sign-up-step")));
			} catch (final NumberFormatException e) {
				// Silently ignore.. (for now)
			}
		} else if (null == ServletTool.get(session, "sign-up-step")) {
			t.put("Integer:sign-up-step", 0);
		} else {
			s.put("Integer:sign-up-step", (Integer)ServletTool.get(session, "sign-up-step"));
			t.put("Integer:sign-up-step", s.get("Integer:sign-up-step"));
		}
		
		if (5 == (Integer)t.get("Integer:sign-up-step")) { // Safety first!
			t.put("Integer:sign-up-step", 2);
			ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
		}
		
		if (4 == (Integer)t.get("Integer:sign-up-step")) { // Safety first!
			t.put("Integer:sign-up-step", 2);
			ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
		}
		
		if (3 == (Integer)t.get("Integer:sign-up-step")) { // We hit cancel on signing-up.jsp
			req.setAttribute("user-id", ServletTool.get(session, "user-id"));
			req.setAttribute("email", ServletTool.get(session, "email"));
			req.setAttribute("sms", ServletTool.get(session, "sms"));
			req.setAttribute("password", ServletTool.get(session, "password"));
		  //req.setAttribute("password-validate", ServletTool.get(session, "password-validate"));
			t.put("Integer:sign-up-step", 2);
			ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
		}
		
		if (0 == (Integer)t.get("Integer:sign-up-step")) { // Initial form pre-fill, generate user-id if necessary/configured..
			
			final String submit = req.getParameter("submit");
			if (null == submit) {
				final String user_id = ServletTool.generateString(8).toLowerCase(); 
				ServletTool.put(session, "user-id", user_id);
				req.setAttribute("user-id", user_id);
				ServletTool.put(session, "sign-up-step", 0);
			} else if (true == submit.trim().toLowerCase().equals("continue")) {
				t.put("Integer:sign-up-step", 1);
			} else {
				throw new IllegalStateException();
			}
			
		}
		
		if (1 == (Integer)t.get("Integer:sign-up-step")) { // Form-filled validation..
			
			final String submit = req.getParameter("submit");
			if (null == submit) {
			} else if (true == submit.trim().toLowerCase().equals("continue")) {
				
			  /*final */String user_id = req.getParameter("user-id"); // If allowed (to use manually-keyed i.e. not-generated)
				if (null == user_id) {
				  /*final String */user_id = (String)ServletTool.get(session, "user-id");
				} else {
					ServletTool.put(session, "user-id", user_id);
				}
				final java.util.Map<String, String> results = new java.util.HashMap<>();
				if (null == user_id || 0 == user_id.trim().length()) { // TODO: user is already taken
					results.put("user-id", "please specify a (non-blank) user-id");
				}
				final String email = req.getParameter("email");
				final String[] __email = null == email ? null : email.split("@", 3); // Not 2, because if we get 3 we can error on 3: "simple validation" (for now)..
				final String sms = req.getParameter("sms");
			  /*final */String __sms = sms.replaceAll("\\+","").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("-", "").replaceAll(" ", ""); // "simple validation" (for now)
				try {
				  /*final Long number = */Long.parseLong(__sms);
				} catch (final NumberFormatException e) {
					__sms = null;
				}
				if (
					(null == __email || 2 > __email.length || 2 < __email.length)
					&&
					(null == __sms)
				) {
					results.put("email", "please specify a valid e-mail or (sms-capable) phone number");
				}
				final String password = req.getParameter("password");
			  /*if (null == password || 0 == password.trim().length()) { // TODO: "too short"
					results.put("password", "please specify a password");
				}*/
				final String validate = req.getParameter("password-validate");
			  /*if (null == validate || 0 == validate.trim().length()) {
				}*/
				if (null != password && null != validate) {
					if (0 < password.trim().length() || 0 < validate.trim().length()) {
						if (false == password.trim().equals(validate.trim())) {
							results.put("password-validate", "passwords do not match");
						}
					}
				} else {
					throw new IllegalStateException();
				}
				if (null == results || 0 == results.size()) {
					ServletTool.put(session, "user-id", user_id);
					ServletTool.put(session, "email", email);
					ServletTool.put(session, "sms", sms);
					ServletTool.put(session, "password", password);
					req.setAttribute("sign-id", user_id);
					t.put("Integer:sign-up-step", 2);
					ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
				} else {
					if (null != user_id) {
						req.setAttribute("user-id", user_id);
					}
					if (null != email) {
						req.setAttribute("email", email);
					}
					if (null != sms) {
						req.setAttribute("sms", sms);
					}
					if (null != password) {
						req.setAttribute("password", password);
					}
					if (null != validate) {
						req.setAttribute("password-validate", validate);
					}
					req.setAttribute("results", results);
				}
				
			} else {
				throw new IllegalStateException();
			}
			
		}
		
		if (2 == (Integer)t.get("Integer:sign-up-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signing-up?session-id="+session_id+"'\" />");
			// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		req.setAttribute("sign-up-step", (Integer)t.get("Integer:sign-up-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/sign-up.jsp");
		requestDispatcher.include(req, resp);
	}
}