package hci.j2ee;

import static hci.j2ee.ServletTool.SIGN_IN_METHODS;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignedInLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		ServletTool.processSignLogic(req, this);
		
	  //final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		if (null == ServletTool.get(session, "sign-in-step")) {
			t.put("Integer:sign-in-step", 00);
		} else {
			s.put("Integer:sign-in-step", (Integer)ServletTool.get(session, "sign-in-step"));
			t.put("Integer:sign-in-step", s.get("Integer:sign-in-step"));
		}
		
		if (00 == (Integer)t.get("Integer:sign-in-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/sign-in?session-id="+session_id+"'\" />");
			
		}
		
	  /*final */boolean remove_session_sign_in_step = false;
		
		if (40 == (Integer)t.get("Integer:sign-in-step")) {
			
		  /*final */String signing_in_method = (String)ServletTool.get(session, "sign-in-method"); 
			if (true == SIGN_IN_METHODS.contains(signing_in_method)) {
				final String signed_in_method = (String)ServletTool.get(session, "signing-in-method"); 
				final String sign_id = (String)ServletTool.get(session, "sign-id");
				final java.util.Map<String, Object> user = Users.findUser(sign_id);
				
				if (true == "html-form".equals(signing_in_method)) {
					
					ServletTool.put(session, "signed-in-method", ServletTool.remove(session, "signing-in-method"));
					ServletTool.put(session, "sign-in-last-successful", user.get("sign-in-last-successful"));
					ServletTool.put(session, "user", user);
					if (null != user.get("sign-in-attempts-failed")) {
						ServletTool.put(session, "sign-in-attempts-failed", user.remove("sign-in-attempts-failed"));
					}
					ServletTool.remove(session, "sign-in-attempts-failed");
					t.put("Integer:sign-in-step", 50);
					ServletTool.remove(session, "password");
				  //ServletTool.remove(session, "sign-in-step");
				  /*final boolean */remove_session_sign_in_step = true;
					
				} else if (true == "basic-auth".equals(signed_in_method)) {
					
					final String[] values = ServletTool.doAuthorization(req);
					final String username = null == values || values.length < 1 ? null : values[0];
					final String passphrase = null == values || values.length < 2 ? null : values[1];
					
					if (
						(null != username && true == username.equals(ServletTool.get(session, "passphrase")))
						||
						(null != passphrase && true == passphrase.equals(ServletTool.get(session, "passphrase")))
					) {
					
						ServletTool.put(session, "signed-in-method", ServletTool.remove(session, "signing-in-method"));
						ServletTool.put(session, "sign-in-last-successful", user.get("sign-in-last-successful"));
						if (null != user.get("sign-in-attempts-failed")) {
							ServletTool.put(session, "sign-in-attempts-failed", user.remove("sign-in-attempts-failed"));
						}
						req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
						final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-in.jsp");
						requestDispatcher.include(req, resp);
						
						ServletTool.remove(session, "sign-in-attempts-failed");
					  //ServletTool.remove(session, "sign-in-step");
					  /*final boolean */remove_session_sign_in_step = true;
						
					} else {
						
						throw new IllegalStateException();
						
					}
					
				} else if (true == "client-ssl".equals(signed_in_method)) {
					
					throw new UnsupportedOperationException(); // For now
					
				} else {
					throw new IllegalArgumentException();
				}
				
				ServletTool.put(session, "user", user);
				
			} else {
				throw new IllegalArgumentException();
			}
			
		}
		
		if (51 == (Integer)t.get("Integer:sign-in-step") || 50 == (Integer)t.get("Integer:sign-in-step")) {
			req.setAttribute("sign-id", ServletTool.get(session, "sign-id"));
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
			// Render previous input`, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		req.setAttribute("sign-in-step", t.get("Integer:sign-in-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-in.jsp");
		requestDispatcher.include(req, resp);
		if (true == remove_session_sign_in_step) {
			ServletTool.remove(session, "sign-in-step");
		}
	}
}