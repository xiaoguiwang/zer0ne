package hci.j2ee;

import static hci.j2ee.ServletTool.SIGN_IN_METHODS;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignedOutLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		ServletTool.processSignLogic(req, this);
		
	  //final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		if (null == ServletTool.get(session, "sign-out-step")) {
			t.put("Integer:sign-out-step", 00);
		} else {
			s.put("Integer:sign-out-step", (Integer)ServletTool.get(session, "sign-out-step"));
			t.put("Integer:sign-out-step", s.get("Integer:sign-out-step"));
		}
		
		if (00 == (Integer)t.get("Integer:sign-out-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/sign-out?session-id="+session_id+"'\" />");
			
		}
		
	  /*final */boolean remove_session_sign_out_step = false;
		
		if (40 == (Integer)t.get("Integer:sign-out-step")) {
			
			final String signed_out_method = (String)ServletTool.get(session, "signing-out-method"); 
			if (true == SIGN_IN_METHODS.contains(signed_out_method)) {
				
				@SuppressWarnings("unchecked")
				final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
				
				if (null != user && true == "html-form".equals(signed_out_method)) {
				
					ServletTool.remove(session, "user");
					ServletTool.remove(session, "signing-out-method");
ServletTool.remove(session, "signed-in-method");
					ServletTool.remove(session, "sign-in-last-successful");
req.setAttribute("sign-id", ServletTool.remove(session, "sign-id"));
req.setAttribute("sign-id", user.get("user-id"));
					t.put("Integer:sign-out-step", 50);
					ServletTool.put(session, "sign-out-step", t.get("Integer:sign-out-step"));
				  //ServletTool.remove(session, "sign-out-step");
				  /*final boolean */remove_session_sign_out_step = true;
					
				} else if (null != user && true == "basic-auth".equals(signed_out_method)) {
					
					final String[] values = ServletTool.doAuthorization(req);
				  //final String username = null == values || values.length < 1 ? null : values[0];
					final String passphrase = null == values || values.length < 2 ? null : values[1];
				  //if (null != user && null != passphrase && true == passphrase.equals(user.get("passphrase"))) { // passphrase isn't necessarily in user (it might be temporary in session) so we can't perform this check here..
					if (null != user && null != passphrase) {
						ServletTool.remove(session, "user");
						ServletTool.remove(session, "signing-out-method");
ServletTool.remove(session, "signed-in-method");
						ServletTool.remove(session, "sign-in-last-successful");
req.setAttribute("sign-id", ServletTool.remove(session, "sign-id"));
req.setAttribute("sign-id", user.get("user-id"));
						t.put("Integer:sign-out-step", 50);
						ServletTool.put(session, "sign-out-step", t.get("Integer:sign-out-step"));
					  //ServletTool.remove(session, "sign-out-step");
					  /*final boolean */remove_session_sign_out_step = true;
					} else {
						throw new IllegalStateException(); // ??
					}
					
				} else if (null != user && true == "client-ssl".equals(signed_out_method)) {
					
					throw new UnsupportedOperationException(); // For now
					
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		}
		
		if (50 == (Integer)t.get("Integer:sign-out-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
			// Render previous input`, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		req.setAttribute("sign-out-step", t.get("Integer:sign-out-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-out.jsp");
		requestDispatcher.include(req, resp);
		if (true == remove_session_sign_out_step) {
			ServletTool.remove(session, "sign-out-step");
		}
	}
}