package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignedUpLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		ServletTool.processSignLogic(req, this);
		
	  //final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		if (null == ServletTool.get(session, "sign-up-step")) {
			t.put("Integer:sign-up-step", 0);
		} else {
			s.put("Integer:sign-up-step", (Integer)ServletTool.get(session, "sign-up-step"));
			t.put("Integer:sign-up-step", s.get("Integer:sign-up-step"));
		}
		
		if (0 == (Integer)t.get("Integer:sign-up-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/sign-up?session-id="+session_id+"'\" />");
			
		}
		
	  /*final */boolean remove_session_sign_up_step = false;
			
		if (4 == (Integer)t.get("Integer:sign-up-step")) {
			
			final String user_id = (String)ServletTool.get(session, "user-id");
			if (null != user_id) {
				
			  /*final */java.util.Map<String, Object> user = (
					null == user_id || 0 == user_id.trim().length()
					?
					null
					:
					Users.users.get(user_id)
				);
				if (null != user) {
					
					ServletTool.put(session, "user", user);
					req.setAttribute("sign-id", user_id);
					t.put("Integer:sign-up-step", 5);
					ServletTool.put(session, "signed-up-method", "html-form");
ServletTool.put(session, "signed-in-method", "html-form");
				  //ServletTool.remove(session, "sign-up-step");
				  /*final boolean */remove_session_sign_up_step = true;
					
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new IllegalStateException();
			}
			
		}
		
		if (5 == (Integer)t.get("Integer:sign-up-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
			// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		req.setAttribute("sign-up-step", t.get("Integer:sign-up-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-up.jsp");
		requestDispatcher.include(req, resp);
		if (true == remove_session_sign_up_step) {
			ServletTool.remove(session, "sign-up-step");
		}
	}
}