package hci.j2ee;

import static hci.j2ee.ServletTool.SIGN_IN_METHODS;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import dro.util.Properties;

public class SigningInLogic extends Logic {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public SigningInLogic properties(final Properties properties) {
		return (SigningInLogic)super.properties(properties);
	}
	@Override
	public SigningInLogic property(final String key, final Object value) {
		return (SigningInLogic)super.property(key, value);
	}
	
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
	  //final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		final Map<String, String[]> pm = req.getParameterMap();
		final String[] sa = (String[])pm.get("json");
		final Boolean json = null == sa || 0 == sa.length ? null : new Boolean(sa[0]);
if (null != json && true == json) {
	  /*final */String key = null;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (null != ServletTool.get(session, "$1-time-sign-in-token")) {
			key = "1-time-sign-in-token";
		} else if (null != ServletTool.get(session, "$signing-in-token")) {
			key = "signing-in-token";
		} else {
			throw new IllegalStateException();
		}
		t.put("String:"+key, (String)ServletTool.get(session, "$"+key));
		final java.util.Map<String, Object> token = Tokens.getToken((String)t.get("String:"+key));
		final Boolean confirmed = (Boolean)token.get("$confirmed");
		final String response = "{ \"token\": \""+(String)t.get("String:"+key)+"\", \"confirmed\": "+(null != confirmed && true == confirmed)+" }";
		resp.setContentType("application/json");
		resp.getWriter().print(response);
} else {/**/
		ServletTool.processSignLogic(req, this);
		
		if (null == ServletTool.get(session, "sign-in-step")) {
			t.put("Integer:sign-in-step", 00);
		} else {
			s.put("Integer:sign-in-step", (Integer)ServletTool.get(session, "sign-in-step"));
			t.put("Integer:sign-in-step", s.get("Integer:sign-in-step"));
		}
		
		final String token = req.getParameter("token");
		if (null != token) {
			final java.util.Map<String, Object> map = (java.util.Map<String, Object>)Tokens.tokens.get(token);
			if (null != map) {
				ServletTool.put(session, "user-id", map.get("user-id"));
				t.put("Integer:sign-in-step", 30);
			}
		}
		
		if (00 == (Integer)t.get("Integer:sign-in-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/sign-in?session-id="+session_id+"'\" />");
			
		}
		
		final Integer sign_in_step = (Integer)t.get("Integer:sign-in-step");
			
		if (20 == sign_in_step || 22 == sign_in_step) {
			
		  /*final */String signing_in_method = (String)ServletTool.get(session, "sign-in-method"); 
			if (true == SIGN_IN_METHODS.contains(signing_in_method)) {
				if (true == "html-form".equals(signing_in_method)) {
					
					// What TODO if/when we're getting Denial Of Serviced?
					// Or: user already signed-in?
					// Or: e-mail not registered?
					// Or: sms-capable phone number not registered?
					final String sign_id = (String)ServletTool.get(session, "sign-id");
					final java.util.Map<String, Object> user = Users.findUser(sign_id);
					
					if (null != user) {
						if (true == Boolean.TRUE.booleanValue()) {
							
							t.put("Integer:sign-in-step", +40);
							ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
							
						} else {
							
							final String user_id = (String)user.get("user-id");
							
						  /*final */Long number = null; // Java/Eclipse is too dumb to recognize that try or catch must succeed and complains final number may be uninitialised.. 
							try {
								number = Long.parseLong(sign_id);
							} catch (final NumberFormatException e) {
							  //number = null;
							}
							
							t.put("String:signing-in-token", ServletTool.generateString(4));
							
						  /*final */String type = null;
							final String __refresh = req.getParameter("refresh");
							final Integer refresh = null == __refresh || 0 == __refresh.trim().length() ? 0 : Integer.parseInt(__refresh);
						  /*final */Integer state = null;
							if (0 == refresh) {
								if (null == number) {
								  /*final String */type = "email";
								} else {
								  /*final String */type = "sms";
								}
							}
							if (0 == refresh) {
								final dao.util.HashMap<String, Object> map = new dao.util.HashMap<>();
								if (false == Boolean.TRUE.booleanValue()) {
								} else if (22 == sign_in_step) {
									map.put("token-id", t.get("String:1-time-sign-in-token"));
								} else if (20 == sign_in_step) {
									map.put("token-id", t.get("String:signing-in-token"));
								} else {
									throw new IllegalStateException();
								}
								map.put("user-id", user_id);
								map.put("$type", type);
								map.put("$date+time", System.currentTimeMillis());
								map.put("$confirmed", Boolean.FALSE);
								synchronized(Tokens.tokens) {
									Tokens.tokens.put((String)t.get("String:signing-in-token"), map);
								}
							}	
							if (0 == refresh) {
							  //user.put("confirmed-"+type, Boolean.FALSE); // Beware, same field is used in SigningUpLogic.java and SignInLogic.java
							}
							if (0 == refresh) {
							  /*final */java.util.Map<String, Object> __results = null;
							  /*final */String template = null; 
								if (false == Boolean.TRUE.booleanValue()) {
								} else if (22 == sign_in_step) {
									final String __template = "1-time-sign-in";
								  /*final String */template = __template;
									if (null == number) {
									  /*final java.util.Map<String, Object> */__results = Emails.sendEmail(template, "1"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
											private static final long serialVersionUID = 0L;
											{
												put(__template+"-token", new String[]{(String)t.get("String:"+__template+"-token")});
											}
										});
									} else {
									  /*final java.util.Map<String, Object> */__results = Texts.sendText(template, "1"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
											private static final long serialVersionUID = 0L;
											{
												put(__template+"-token", new String[]{(String)t.get("String:"+__template+"-token")});
											}
										});
									}
								} else if (20 == sign_in_step) {
									final String __template = "signing-in";
								  /*final String */template = __template;
									if (null == number) {
									  /*final java.util.Map<String, Object> */__results = Emails.sendEmail(template, "2"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
											private static final long serialVersionUID = 0L;
											{
												put(__template+"-token", new String[]{(String)t.get("String:"+__template+"-token")});
											}
										});
									} else {
									  /*final java.util.Map<String, Object> */__results = Texts.sendText(template, "2"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
											private static final long serialVersionUID = 0L;
											{
												put(__template+"-token", new String[]{(String)t.get("String:"+__template+"-token")});
											}
										});
									}
								} else {
									throw new IllegalStateException();
								}
								if (null != __results && 0+1 < __results.size()) {
									for (final String __user_id: __results.keySet()) {
if (true == __user_id.equals("")) continue;
										final Object o = __results.get(__user_id);
										if (true == o instanceof java.util.Map) {
											final java.util.Map<String, Object> history = (java.util.Map<String, Object>)__results.get(__user_id);
											history.put("$token", t.get("String:"+template+"-token"));
										}
									}
								}
							}
							try {
								Thread.sleep(1000L*(long)Math.pow(2.0, refresh));
							} catch (final InterruptedException e) {
							  //e.printStackTrace(); // Silently ignore..
							}
						  /*final Integer */state = (Integer)user.get("signing-in-"+type+"-state");
							if (state < 0 || refresh >= 5) {
								t.put("Integer:sign-in-step", -20);
								ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
							} else if (state >= 4) {
								t.put("Integer:sign-in-step", +30);
								ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
							} else {
								req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/signing-in?refresh="+(1+refresh)+"&session-id="+session_id+"'\" />");
							}
							
						}
						
					} else {
						
					  //results.put("{sign-id}", "<sign-id-does-not-exist"); // Never show this! Lie
						
					}
					
				} else if (true == signing_in_method.equals("basic-auth")) {
					
					final String[] values = ServletTool.doAuthorization(req);
					final String username = null == values || values.length < 1 ? null : values[0];
					final String passphrase = null == values || values.length < 2 ? null : values[1];
					
					final String sign_id = (String)ServletTool.get(session, "sign-id");
					final java.util.Map<String, Object> user = Users.findUser(sign_id);
					
					if (null != user) {
						
					  //req.setAttribute("passphrase-generated", ServletTool.get(session, "passphrase-generated"));
					  //req.setAttribute("passphrase", ServletTool.get(session, "passphrase"));
						
						if (
							(null != username && true == username.equals(ServletTool.get(session, "passphrase")))
							||
							(null != passphrase && true == passphrase.equals(ServletTool.get(session, "passphrase")))
						) {
							
							t.put("Integer:sign-in-step", +40);
							ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
							
						} else {
						
							// FIXME: with respect to this 40/50 pattern at bottom of this method block
						  //req.removeAttribute("meta-http-equiv");
						  //req.setAttribute("passphrase-generated", ServletTool.get(session, "passphrase-generated"));
						  //req.setAttribute("passphrase", ServletTool.get(session, "passphrase")); // In case user clicks "cancel"
							final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signing-in.jsp"); // TODO a href to click
							requestDispatcher.include(req, resp);
							resp.setHeader("WWW-Authenticate", "Basic realm=\"Zer0ne\"");
							resp.setStatus(HttpStatus.SC_UNAUTHORIZED);
							
						}
						
					} else {
						throw new IllegalStateException();
					}
				
				} else if (true == "client-ssl".equals(signing_in_method)) {
					
				  //ServletTool.put(session, "signing-in-method", ServletTool.get(session, "sign-in-method"));
				  //..
					
					throw new UnsupportedOperationException(); // For now
					
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalStateException();
			}
		}
		
		if (-20 == (Integer)t.get("Integer:sign-in-step")) {
			
			// TODO: timeout sending sign-in e-mail/sms?
			
		}
		
		if (+22 == (Integer)t.get("Integer:sign-in-step")) {
			if (null == req.getAttribute("sign-id")) {
				final String sign_id = (String)ServletTool.get(session, "sign-id");
				req.setAttribute("sign-id", sign_id); // Auto form-fill (for show)
			}
		}
		
		// Unlike 21 and 22 in SignInLogic, here we can take from session (not just token - besides, we have no token here)
		if (30 == (Integer)t.get("Integer:sign-in-step")) { // Landing page for URL that was sent
			
			final String submit = req.getParameter("__submit");
			if (null == submit || true == submit.trim().toLowerCase().contains("continue")) {
				final java.util.Map<String, String> results = new java.util.HashMap<>();
			  /*final */String user_id = (String)ServletTool.get(session, "user-id");
			  /*final */java.util.Map<String, Object> map = null;
				if (null == token || 0 == token.trim().length()) {
					if (null != submit) {
						results.put("token", "please enter a token");
					}
				} else {
				  /*final java.util.Map<String, Object> */map = (java.util.Map<String, Object>)Tokens.tokens.get(token);
				}
				if (null == map) {
					if (null != submit && false == results.containsKey("token")) {
						results.put("token", "please enter a valid token");
					}
				} else {
				  //final Long ms = (Long)map.get("date+time"); // For expiry (TODO)
				  /*final String */user_id = (String)map.get("user-id");
					final String type = (String)map.get("$type");
					final java.util.Map<String, Object> user = Users.users.get(user_id);
if (null != user.get("signing-in-"+type+"-state")) {
user.remove("signing-in-"+type+"-state");
}
				  //final Boolean confirmed = (Boolean)map.get("$confirmed");
					map.put("$confirmed", Boolean.TRUE);
//Tokens.tokens.remove(token); // Let's keep tokens for analysis
					if (null == submit) {
						t.put("Integer:sign-in-step", 40);
					} else {
						ServletTool.put(session, "user", user);
						t.put("Integer:sign-in-step", 50);
					}
					ServletTool.put(session, "sign-in-step", t.get("Integer:sign-in-step"));
					req.setAttribute("token", token);
				}
				if (null != results && 0 < results.size()) {
					if (null != user_id) {
						req.setAttribute("sign-id", user_id); // TODO: e-mail or sms depending on token $type 
					}
					if (null != token) {
						req.setAttribute("token", token);
					}
					req.setAttribute("results", results);
				}
			}
		}
		
		if (40 == (Integer)t.get("Integer:sign-in-step")) {
			if (null == req.getAttribute("sign-id")) {
				final String sign_id = (String)ServletTool.get(session, "sign-id");
				req.setAttribute("sign-id", sign_id); // Auto form-fill (for show)
			}
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signed-in?session-id="+session_id+"'\" />");
			// Render previous input`, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		if (41 == (Integer)t.get("Integer:sign-in-step") || 42 == (Integer)t.get("Integer:sign-in-step")) {
		}
		
		final String path;// = null;
		if (50 == (Integer)t.get("Integer:sign-in-step")) { // TODO: protect me!
			path = "/signed-in.jsp";
			
			final String sign_id = (String)ServletTool.get(session, "sign-id"); // TODO: is this enough protection?
			final java.util.Map<String, Object> user = Users.findUser(sign_id);
			if (null != user) {
				ServletTool.put(session, "signing-in-method", ServletTool.get(session, "sign-in-method"));
ServletTool.put(session, "signed-in-method", ServletTool.get(session, "sign-in-method"));
				if (null != user.get("sign-in-this-date+time")) {
					user.put("sign-in-last-successful", user.get("sign-in-this-date+time"));
				}
				user.put("sign-in-this-date+time", sdf.format(new java.util.Date()));
				ServletTool.put(session, "user", user);
			}
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
			// Render previous input`, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
		} else {
			path = "/signing-in.jsp";
		}
		
		req.setAttribute("sign-in-step", t.get("Integer:sign-in-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
		requestDispatcher.include(req, resp);
	}
	
}/**/
	
}