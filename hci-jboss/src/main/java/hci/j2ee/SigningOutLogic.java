package hci.j2ee;

import static hci.j2ee.ServletTool.SIGN_IN_METHODS;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SigningOutLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String) req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		ServletTool.processSignLogic(req, this);
		
	  //final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		if (null == ServletTool.get(session, "sign-out-step")) {
			t.put("Integer:sign-out-step", 00);
		} else {
			s.put("Integer:sign-out-step", (Integer)ServletTool.get(session, "sign-out-step"));
			t.put("Integer:sign-out-step", s.get("Integer:sign-out-step"));
		}
		
		if (00 == (Integer)t.get("Integer:sign-out-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/sign-out?session-id="+session_id+"'\" />");
			
		}
		
		final String signing_out_method = (String)ServletTool.get(session, "sign-out-method");
		
		if (20 == (Integer)t.get("Integer:sign-out-step")) {
			
			if (true == SIGN_IN_METHODS.contains(signing_out_method)) {
				
				@SuppressWarnings("unchecked")
				final java.util.Map<String, Object> user = (java.util.Map<String, Object>)ServletTool.get(session, "user");
				
				if (null != user && true == "html-form".equals(signing_out_method)) {
					
					req.setAttribute("sign-id", user.get("user-id"));
				  //ServletTool.remove(session, "signed-in-method");
					ServletTool.put(session, "signing-out-method", ServletTool.remove(session, "sign-out-method"));
					t.put("Integer:sign-out-step", 40);
					ServletTool.put(session, "sign-out-step", t.get("Integer:sign-out-step"));
					
				} else if (null != user && true == "basic-auth".equals(signing_out_method)) {
					
					final String[] values = ServletTool.doAuthorization(req);
				  //final String username = null == values || values.length < 1 ? null : values[0];
					final String passphrase = null == values || values.length < 2 ? null : values[1];
				  //if (null != user && null != passphrase && true == passphrase.equals(user.get("passphrase"))) { // passphrase isn't necessarily in user (it might be temporary in session) so we can't perform this check here..
					if (null != user && null != passphrase) {
					  //ServletTool.remove(session, "signed-in-method");
						ServletTool.put(session, "signing-out-method", ServletTool.remove(session, "sign-out-method"));
						t.put("Integer:sign-out-step", 40);
						ServletTool.put(session, "sign-out-step", t.get("Integer:sign-out-step"));
					} else {
						throw new IllegalStateException(); // ??
					}
					
				} else if (null != user && true == "client-ssl".equals(signing_out_method)) {
					
					throw new UnsupportedOperationException(); // For now
					
				} else {
					throw new IllegalArgumentException();
				}
				
			} else {
				throw new IllegalStateException();
			}
		}
		
		if (40 == (Integer)t.get("Integer:sign-out-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signed-out?session-id="+session_id+"'\" />");
			// Render previous input`, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		final String path;// = null;
		if (50 == (Integer)t.get("Integer:sign-out-step")) {
			path = "/signed-out.jsp";
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
			// Render previous input`, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
		} else {
			path = "/signing-out.jsp";
		}
		
		req.setAttribute("sign-out-step", (Integer)t.get("Integer:sign-out-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
		requestDispatcher.include(req, resp);
	}
}