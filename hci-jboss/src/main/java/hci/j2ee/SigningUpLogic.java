package hci.j2ee;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SigningUpLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
	  //final java.util.Map<String, Object> r = new java.util.HashMap<>(); // Request
		final java.util.Map<String, Object> s = new java.util.HashMap<>(); // Session
		final java.util.Map<String, Object> t = new java.util.HashMap<>(); // This
		
		final Map<String, String[]> pm = req.getParameterMap();
		final String[] sa = (String[])pm.get("json");
		final Boolean json = null == sa || 0 == sa.length ? null : new Boolean(sa[0]);
if (null != json && true == json) {
	  /*final */String key = null;
		if (null != ServletTool.get(session, "$signing-up-token")) {
			key = "signing-up-token";
		} else {
			throw new IllegalStateException();
		}
		t.put("String:"+key, (String)ServletTool.get(session, "$"+key));
		final java.util.Map<String, Object> token = Tokens.getToken((String)t.get("String:"+key));
		final Boolean confirmed = (Boolean)token.get("$confirmed");
		final String response = "{ \"token\": \""+(String)t.get("String:"+key)+"\", \"confirmed\": "+(null != confirmed && true == confirmed)+" }";
		resp.setContentType("application/json");
		resp.getWriter().print(response);
} else {/**/
		
		ServletTool.processSignLogic(req, this);
		
		if (null == ServletTool.get(session, "sign-up-step")) {
			t.put("Integer:sign-up-step", 0);
		} else {
			s.put("Integer:sign-up-step", (Integer)ServletTool.get(session, "sign-up-step"));
			t.put("Integer:sign-up-step", s.get("Integer:sign-up-step"));
		}
		
		final String token = req.getParameter("token");
		if (null != token) {
			final java.util.Map<String, Object> map = Tokens.tokens.get(token);
			if (null != map) {
				ServletTool.put(session, "user-id", map.get("user-id"));
				ServletTool.put(session, "email", map.get("email"));
				ServletTool.put(session, "sms", map.get("sms"));
				ServletTool.put(session, "password", map.get("password"));
				t.put("Integer:sign-up-step", 3);
			}
		}
		
		if (0 == (Integer)t.get("Integer:sign-up-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/sign-up?session-id="+session_id+"'\" />");
			
		}
		
		if (+2 == (Integer)t.get("Integer:sign-up-step")) {
			
			final String submit = req.getParameter("__submit");
			if (null == submit) {
				if (null == token) {
					
					// What TODO if/when we're getting Denial Of Serviced?
					// Or: user already signed-up?
					// Or: e-mail already registered?
					// Or: sms-capable phone number already registered?
					final String user_id = (String)ServletTool.get(session, "user-id");
				  /*final */java.util.Map<String, Object> user = (
						null == user_id || 0 == user_id.trim().length()
						?
						null
						:
						Users.users.get(user_id)
					);
					
					if (null == user) {
						
					  /*final java.util.Map<String, Object> */user = new dao.util.HashMap<String, Object>();
						user.put("user-id", user_id);
						final String email = (String)ServletTool.get(session, "email");
						user.put("email", email);
						final String sms = (String)ServletTool.get(session, "sms");
						user.put("sms", sms);
						final String password = (String)ServletTool.get(session, "password");
						user.put("password", password);
					  //user.put("passphrase", password+"'");
						Users.users.put(user_id, user);
						
						t.put("String:signing-up-token", ServletTool.generateString(4));
						
					  /*final */String type = null;
						final String __refresh = req.getParameter("refresh");
						final Integer refresh = null == __refresh || 0 == __refresh.trim().length() ? 0 : Integer.parseInt(__refresh);
					  /*final */Integer state = null;
						if (0 == refresh) {
							if (null != email && 0 < email.trim().length()) {
							  /*final String */type = "email";
							} else if (null != sms && 0 < sms.trim().length()) {
							  /*final String */type = "sms";
							}
						}
						if (0 == refresh) {
							final dao.util.HashMap<String, Object> map = new dao.util.HashMap<>();
							map.put("token-id", t.get("String:signing-up-token"));
							map.put("user-id", user_id);
							map.put("email", email);
							map.put("sms", sms);
							map.put("$type", type);
							map.put("password", password);
							map.put("$date+time", System.currentTimeMillis());
							map.put("$confirmed", Boolean.FALSE);
							ServletTool.put(session, "$signing-up-token", t.get("String:signing-up-token"));
							Tokens.tokens.put((String)t.get("String:signing-up-token"), map);
						}
						if (0 == refresh) {
						  //user.put("confirmed-"+type, Boolean.FALSE);
						}
						if (0 == refresh) {
							final String template = "signing-up";
						  /*final */java.util.Map<String, Object> results = null;
							if (null != email && 0 < email.trim().length()) {
							  /*final java.util.Map<String, Object> */results = Emails.sendEmail(template, "0"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
									private static final long serialVersionUID = 0L;
									{
										put(template+"-token", new String[]{(String)t.get("String:"+template+"-token")});
									}
								});
							}
							if (null != sms && 0 < sms.trim().length()) {
							  /*final java.util.Map<String, Object> */results = Texts.sendText(template, "0"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
									private static final long serialVersionUID = 0L;
									{
										put(template+"-token", new String[]{(String)t.get("String:"+template+"-token")});
									}
								});
							}
							if (null != results && 0+1 < results.size()) {
								for (final String __user_id: results.keySet()) {
if (true == __user_id.equals("")) continue;
									final Object o = results.get(__user_id);
									if (true == o instanceof java.util.Map) {
										final java.util.Map<String, Object> history = (java.util.Map<String, Object>)o;
										history.put("$token", t.get("String:"+template+"-token"));
									}
								}
							}
						}
						try {
							Thread.sleep(1000L*(long)Math.pow(2.0, refresh));
						} catch (final InterruptedException e) {
						  //e.printStackTrace(); // Silently ignore..
						}
					  /*final Integer */state = (Integer)user.get("signing-up-"+type+"-state");
						if (state < 0 || refresh >= 5) {
							t.put("Integer:sign-up-step", -2);
							ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
						} else if (state >= 4) {
							t.put("Integer:sign-up-step", +3);
							ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
						} else {
							req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/signing-up?refresh="+(1+refresh)+"&session-id="+session_id+"'\" />");
						}
						
					} else {
						
					  //results.put("{sign-id}", "<duplicate-sign-id>"); // Never show this! Lie
						
					}
					
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new IllegalStateException();
			}
			
		}
		
		if (-2 == (Integer)t.get("Integer:sign-up-step")) {
			
			// TODO: timeout sending sign-up e-mail/sms?
			
		}
		
		if (3 == (Integer)t.get("Integer:sign-up-step")) { // Landing page for URL that was sent
			
			final String submit = req.getParameter("submit");
			if (null == submit || true == submit.trim().toLowerCase().contains("continue")) {
				final java.util.Map<String, String> results = new java.util.HashMap<>();
			  /*final */String user_id = (String)ServletTool.get(session, "user-id");
			  /*final */java.util.Map<String, Object> map = null;
				if (null == token || 0 == token.trim().length()) {
					if (null != submit) {
						results.put("token", "please enter a token");
					}
				} else {
				  /*final dao.util.HashMap<String, Object> */map = Tokens.tokens.get(token);
				}
				if (null == map) {
					if (null != submit && false == results.containsKey("token")) {
						results.put("token", "please enter a valid token");
					}
				} else {
				  //final Long ts = (Long)map.get("$date+time"); // For expiry (TODO)
				  /*final String */user_id = (String)map.get("user-id");
					final String type = (String)map.get("$type");
					final java.util.Map<String, Object> user = Users.users.get(user_id);
if (null != user.get("signing-up-"+type+"-state")) {
	user.remove("signing-up-"+type+"-state");
}
if (null != map.get("password")) {
					user.put("password", (String)map.get("password"));
	map.remove("password");
}
				  //final Boolean confirmed = (Boolean)map.get("$confirmed");
					map.put("$confirmed", Boolean.TRUE);
//tokens.remove(token); // Let's keep tokens for analysis
					if (null == submit) {
						t.put("Integer:sign-up-step", 4);
					} else {
						ServletTool.put(session, "user", user);
						t.put("Integer:sign-up-step", 5);
					}
					ServletTool.put(session, "sign-up-step", t.get("Integer:sign-up-step"));
					req.setAttribute("token", token);
				}
				if (null != results && 0 <= results.size()) {
					if (null != user_id) {
						req.setAttribute("sign-id", user_id);
					}
					if (null != token) {
						req.setAttribute("token", token);
					}
					req.setAttribute("results", results);
				}
				
			}
		}
		
		if (4 == (Integer)t.get("Integer:sign-up-step")) {
			
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/signed-up?session-id="+session_id+"'\" />");
			// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
			
		}
		
		final String path;// = null;
		if (5 == (Integer)t.get("Integer:sign-up-step")) {
			path = "/signed-up.jsp";
			ServletTool.put(session, "signed-up-method", "html-form");
ServletTool.put(session, "signed-in-method", "html-form");
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/index?session-id="+session_id+"'\" />");
			// Render previous input, disabled potentially, while/until meta http-equiv kicks in.. (perhaps render "next" hyper-link just in case)
		} else {
			path = "/signing-up.jsp";
		}
		
		req.setAttribute("sign-up-step", t.get("Integer:sign-up-step"));
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
		requestDispatcher.include(req, resp);
	}
	
}/**/
	
}