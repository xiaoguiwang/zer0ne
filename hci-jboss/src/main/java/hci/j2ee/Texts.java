package hci.j2ee;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Base64;

import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dto.cache.Helper;

@SuppressWarnings("unchecked")
public class Texts {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> texts;// = null;
	public static /*final */java.util.List<java.util.Map<java.lang.String, java.lang.Object>> history;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Texts.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */texts = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("texts", java.util.List.class);
		if (null == texts) {
			texts = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					// Sign-up + confirm token:
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("text-id", 0);
							put("message", new StringWriter()
								.append("Hi!\r\n")
								.append("\r\n")
								.append("Thank you for choosing to sign-up at ${http-stub-for-urls}.\r\n")
								.append("\r\n")
								.append("Please find your confirmation token below, and confirmation link ")
								.append("that will take you back to our web site.\r\n")
								.append("\r\n")
								.append("${https-stub-for-urls}/hci/signing-up?token=${signing-up-token}\r\n")
								.append("\r\n")
								.append("Thanks, IT From Blighty\r\n")
								.toString()
							);
						}
					});
					// Sign-in + confirm token:
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("text-id", 1);
							put("message", new StringWriter()
								.append("Hi ${to-user#preferred-name},\r\n")
								.append("\r\n")
								.append("You have requested to sign-in to ${http-stub-for-urls}.\r\n")
								.append("\r\n")
								.append("Please find your secure token below, and secure link ")
								.append("that will take you back to our web site.\r\n")
								.append("\r\n")
								.append("${https-stub-for-urls}/hci/signing-in?token=${signing-in-token}\r\n")
								.append("\r\n")
								.append("Thanks, IT From Blighty\r\n")
								.toString()
							);
						}
					});
					// Sign-in + password-reset token:
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("text-id", 2);
							put("message", new StringWriter()
								.append("Hi ${to-user#preferred-name},\r\n")
								.append("\r\n")
								.append("You have requested to reset your password at ${http-stub-for-urls}.\r\n")
								.append("\r\n")
								.append("Please find your confirmation token below, and confirmation link ")
								.append("that will take you back to our web site.\r\n")
								.append("\r\n")
								.append("${http-stub-for-urls}/hci/sign-in?token=${password-reset-token}\r\n")
								.append("\r\n")
								.append("Thanks, IT From Blighty\r\n")
								.toString()
							);
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(texts, "texts");
		}
	  /*final java.util.List<java.util.Map<java.lang.String, java.lang.Object>> */history = (java.util.List<java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("texts#history", java.util.List.class);
		if (null == history) {
			history = new dao.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("text-history-id", 0);
							put("text-id", 0);
							put("from-user-id", "admin");
							put("properties", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
								private static final long serialVersionUID = 0L;
								{
									put("to-user#preferred-name", "Alex");
									put("signing-up-token", "WXYZ");
									put("http-stub-for-urls", "http://uat.itfromblighty");
									put("https-stub-for-urls", "https://uat.itfromblighty");
								}
							});
							put("to-user-id", "arussell");
						}
					});
				}
			};
			dao.Context.getAdapterFor(me).alias(history, "texts#history");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> text = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __text: texts) {
			if (true == id.equals(__text.get("$id").toString())) {
				text = __text;
				break;
			}
		}
		return text;
	}
	public static java.util.Map<java.lang.String, java.lang.Object> getTextById(final java.lang.String text_id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> text = null;
		for (final java.util.Map<java.lang.String, java.lang.Object> __text: texts) {
			if (true == text_id.equals(__text.get("text-id").toString())) {
				text = __text;
				break;
			}
		}
		return text;
	}
	
	private static /*final */java.lang.Thread thread;// = null;
	
	public static void startup() {
		final java.util.Map<String, Object> service = Services.services.get("sms");
	  //service.put("$state", "startup");
		service.put("$status:text", "startup");
		service.put("$status:date+time", sdf.format(new java.util.Date()));
		final java.util.Map<String, Object> __service = service;
		try {
			Texts.thread = new java.lang.Thread(){
				private final dto.http.Adapter adapter;// = null;
				private final dro.util.Properties p;// = null;
				{
				  //__service.put("$state", "starting");
					__service.put("$status:text", "starting");
					__service.put("$status:date+time", sdf.format(new java.util.Date()));
					p = new dro.util.Properties(Texts.class);
					adapter = new dto.http.Adapter()
						.properties(p)
					;
				}
				@Override
				public void run() {
				  //__service.put("$state", "started");
					__service.put("$status:text", "started");
					__service.put("$status:date+time", sdf.format(new java.util.Date()));
				  /*final */String state = (String)__service.get("$state");
					if (null != state && true == state.equals("running")) {
						__service.put("$status:text", "running");
					}
					long elapsed = 0;
					while (null != adapter && false == Texts.shutdown) {
					  /*if (null != __service.get("$state")) {
							if (true == ((String)__service.get("$state")).equals("shutdown")) {
							  //__service.put("$state", "shutdown");
								__service.put("$status:text", "shutdown");
								__service.put("$status:date+time", sdf.format(new java.util.Date()));
								break;
							}
						}*/
						if (null != __service.get("$state")) {
							if (null != state && true == state.equals("stopped")) { // <== current
								if (true == ((String)__service.get("$state")).equals("running")) { // <== future
								/**/__service.put("$state", "starting"); // this transition to an unsupported state is okay as we're about to switch to a supported state anyway..
									__service.put("$status:text", "starting");
									__service.put("$status:date+time", sdf.format(new java.util.Date()));
									state = "running";
									__service.put("$state", state);
								}
							}
						}
						if (null != state && true == state.equals("running") && elapsed >= 60*1000L) {
							elapsed = 0L;
							__service.put("$status:date+time", sdf.format(new java.util.Date()));
							try {
								if (null == __service.get("$after-id")) {
									// http://smsgateway.ca/docs/index.html?javascript#getincomingmessages
									final String url = dto.http.Adapter.class.getName()+"#url";
									p.property("service", "incoming.svc")
									 .property("messageCount", new Integer(1).toString())
									 .property("$url", p.property(url)+"/count/${messageCount}")
									;
									final String data = this.adapter.get(p.property("$url"), dro.lang.String.Return.String);
								  /*final */Object o = null;
									try {
										o = new JSONParser().parse(data);
									} catch (final ParseException exc) {
									  //__service.put("$status:error", e.getMessage());
									}
									if (null != o) {
										final JSONArray ja = (JSONArray)o;
										final JSONObject jo = (JSONObject)ja.get(ja.size()-1);
										final Long messageNumber = (Long)jo.get("MessageNumber");
										__service.put("$after-id", messageNumber);
									}
								} else {
									// http://smsgateway.ca/docs/index.html?javascript#getincomingmessagesafteridextended
								  /*final */Long messageNumber = (Long)__service.get("$after-id");
									final String url = dto.http.Adapter.class.getName()+"#url";
									p.property("service", "incoming.svc")
									 .property("messageNumber", messageNumber.toString())
									 .property("$url", p.property(url)+"/afterIdExtended/${messageNumber}")
									;
								  /*final */Object o = null;
									final String data = this.adapter.get(p.property("$url"), dro.lang.String.Return.String);
									if (null != data) {
										try {
											o = new JSONParser().parse(data);
										} catch (final ParseException e) {
										  //__service.put("$status:error", e.getMessage());
										}
									}
									final JSONArray ja = (JSONArray)o;
									if (null != ja && 0 < ja.size()) { // FIXME!
										final JSONObject jo = (JSONObject)ja.get(ja.size()-1);
									  /*final Long */messageNumber = (Long)jo.get("MessageNumber");
										__service.put("$after-id", messageNumber);
										final Long outgoingMessageID = (Long)jo.get("OutgoingMessageID");
										if (null != outgoingMessageID) {
											synchronized(Texts.history) {
											  /*final */java.util.Map<String, Object> __history = null;
												for (final java.util.Map<String, Object> history: Texts.history) {
													final Long messageID = (Long)history.get("$message-id");
													if (null != messageID && messageID.longValue() == outgoingMessageID.longValue()) {
													  /*final java.util.Map<String, Object> */__history = new dao.util.HashMap<>();
														__history.put("text-history-id", new Integer(Texts.history.size()));
													  //__history.put("text-history-id'", history.get("text-history-id")); // For replies we could link to the original text..
														if (null != jo.get("PhoneNumber")) {
															__history.put("from-sms", (String)jo.get("PhoneNumber"));
														}
														if (null != jo.get("MessageBody")) {
															__history.put("message-body", (String)jo.get("MessageBody"));
														}
													  //history.put("to-sms", ..);
														final String token = (String)history.get("$token");
														if (null != token) {
															final java.util.Map<String, Object> map = Tokens.getToken(token);
															synchronized(map) {
																map.put("$confirmed", Boolean.TRUE);
															/**/map.put("$date+time", System.currentTimeMillis()); // Note: this (for now) is overwriting when it was sent
															}
														}
														break;
													}
												}
												if (null != __history) {
													Texts.history.add(__history);
final String event_name = "password-reset"; // FIXME
final Long creation_time = new Long(System.currentTimeMillis());
final String uuid_as_char = ServletTool.generateString(8);
final String from = (String)jo.get("PhoneNumber");
//final String to = "admin"
final String body = (String)jo.get("MessageBody");
final Long outgoingMessageId = (Long)jo.get("OutgoingMessageID");
final Integer text_history_id = (Integer)__history.get("text-history-id");
final dro.util.Properties __p = new dro.util.Properties(){
	private static final long serialVersionUID = 0L;
	{
		property("event-name", event_name);
		property("creation-time", creation_time.toString());
		property("uuid-as-char", uuid_as_char);
		property("from", from);
	  //property("to", to);
		property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
		property("message-id", outgoingMessageId.toString());
		property("type", "sms:inbound");
		property("text-history-id", text_history_id.toString());
	}
};
Helper.cacheEvent(Texts.class, uuid_as_char, __p);
Helper.cacheEventX(Texts.class, uuid_as_char, new java.util.HashMap<String, Object>(){
	private static final long serialVersionUID = 0L;
	{
		put("event-name", event_name);
		put("creation-time", creation_time);
		put("uuid-as-char", uuid_as_char);
		put("process-time", new Long(System.currentTimeMillis()));
		put("type", "sms:inbound");
	}
});
												}
											}
										}
									}
								}
								__service.put("$status:date+time", sdf.format(new java.util.Date()));
							} catch (final ClientProtocolException e) {
							  //__service.put("$status:error", e.getMessage());
							} catch (final FileNotFoundException e) {
							  //__service.put("$status:error", e.getMessage());
							} catch (final IOException e) {
							  //__service.put("$status:error", e.getMessage());
							} finally {
							}
						}
						if (null != state && null != __service.get("$state")) {
							if (true == state.equals("running")) { // <== current
								if (true == ((String)__service.get("$state")).equals("stopped")) { // <== future
								/**/__service.put("$state", "stopping"); // this transition to an unsupported state is okay as we're about to switch to a supported state anyway..
									__service.put("$status:text", "stopped");
									__service.put("$status:date+time", sdf.format(new java.util.Date()));
									state = "stopped";
									__service.put("$state", state);
								}
							}
						}
						try {
							Thread.sleep(1000L);
							elapsed += 1000L;
						} catch (final InterruptedException e) {
							// Silently ignore..
						}
					}
					if (null == adapter) {
					  //__service.put("$state", "crashed");
						__service.put("$status:text", "crashed");
						__service.put("$status:date+time", sdf.format(new java.util.Date()));
					}
				}
			};
			Texts.thread.start();
		} catch (final IOException exc) {
		  //__service.put("$status:error", e.getMessage());
		  //__service.put("$state", "crashed");
			__service.put("$status:text", "crashed");
			__service.put("$status:date+time", sdf.format(new java.util.Date()));
		}
	}
	
	public static java.util.Map<String, String[]> checkText(final String action, final String text_id, final String from_user_id, final String[] to_user_ids, final java.util.Map<String, String[]> replacements) {
	  /*final */java.util.Map<String, String[]> unresolve = null;
		// FIXME! hate the idea of looking up in a list by index where target/intention might change..
		final java.util.Map<String, Object> __from_user = Users.users.get(from_user_id);
		final java.util.Map<String, Object> text = Texts.texts.get(Integer.parseInt(text_id));
		final String message = (String)text.get("message");
		int replacement_index = 0;
		for (final String user_id: to_user_ids) {
			final java.util.Map<String, Object> __user = Users.users.get(user_id);
			final dro.util.Properties q = new dro.util.Properties();
			{
				final String[] unresolved = dro.lang.String.unresolved(message, (dro.util.Properties)null);
				if (null != unresolved) {
if (null == unresolve) {
	unresolve = new java.util.LinkedHashMap<String, String[]>();
}
unresolve.put("", unresolved);
					for (final String key: unresolved) {
						final String[] sa = null == key ? null : key.split("#", 2);
						if (2 == sa.length) {
							if (false == Boolean.TRUE.booleanValue()) {
							} else if (true == sa[0].equals("to-user")) {
								if (null != __user.get(sa[1])) {
									q.property(key, __user.get(sa[1]).toString());
								}
							} else if (true == sa[0].equals("role")) {
							  //q.property(key, __role.get(sa[1]).toString());
							} else if (true == sa[0].equals("group")) {
							  //q.property(key, __group.get(sa[1]).toString());
							} else {
							  //throw new IllegalArgumentException();
							}
						} else {
							if (false == Boolean.TRUE.booleanValue()) {
							} else if (null != __from_user.get("properties")) {
								final java.util.HashMap<String, String> __prop = (java.util.HashMap<String, String>)__from_user.get("properties");
								if (null != __prop.get(sa[0])) {
									q.property(key, __prop.get(sa[0]).toString());
								}
							} else {
							  //throw new IllegalArgumentException();
							}
						}
					}
				}
			}
			if (null != replacements) {
			  /*final */int __replacement_index = replacement_index;
				for (final String key: replacements.keySet()) {
					final String[] values = replacements.get(key);
					if (1 == values.length) {
						__replacement_index = 0;
					}
					final String value = values[__replacement_index];
					q.property(key, value);
				}
			}
			final String[] unresolved = dro.lang.String.unresolved(message, q);
			if (null != unresolved) {
				if (null == unresolve) {
					unresolve = new java.util.LinkedHashMap<String, String[]>();
				}
				unresolve.put(user_id, unresolved);
			}
		}
		return unresolve;
	}
	
	@Deprecated // To warn callers re. issue with text_id (String) being an (Integer) index into a list (i.e. not fixed when items are deleted!
	public static java.util.Map<String, Object> sendText(final String action, final String text_id, final String from_user_id, final String[] to_user_ids, final java.util.Map<String, String[]> replacements) {
	  /*final */java.util.Map<String, Object> results = null;
		// FIXME! hate the idea of looking up in a list by index where target/intention might change..
		final java.util.Map<String, Object> __from_user = Users.users.get(from_user_id);
		final java.util.Map<String, Object> text = Texts.texts.get(Integer.parseInt(text_id));
		final String message = (String)text.get("message");
		int replacement_index = 0;
		for (final String user_id: to_user_ids) {
			final java.util.Map<String, Object> __user = Users.users.get(user_id);
			final String transaction_id = ServletTool.generateString(8);
			__user.put(action+"-sms-transaction-id", transaction_id);
			__user.put(action+"-sms-state", 0);
		  /*new java.lang.Thread()*/{
			  /*private final */dto.http.Adapter adapter = null;
				try {
					__user.put(action+"-sms-state", 1);
					adapter = new dto.http.Adapter()
						.properties(new dro.util.Properties(Texts.class))
					;
					__user.put(action+"-sms-state", 2);
				} catch (final IOException exc) {
					__user.put(action+"-sms-state", -1);
					__user.put(action+"-sms-error", exc.getMessage());
				}
			  /*@Override
				public void run() */if (null != adapter) {
					__user.put(action+"-sms-state", 3);
					try {
						// http://smsgateway.ca/docs/index.html?javascript#sending-a-sms-message
						final dro.util.Properties p = new dro.util.Properties(Texts.class);
						final String url = dto.http.Adapter.class.getName()+"#url";
						p.property("service", "message.svc")
						 .property("destinationNumber", (String)__user.get("sms"))
						 .property(url, p.property(url)+"/${destinationNumber}/Extended") // https://secure.smsgateway.ca/docs/index.html#sendmessageextended
						;
						final dro.util.Properties q = new dro.util.Properties();
						{
 							final String[] unresolved = dro.lang.String.unresolved(message, (dro.util.Properties)null);
							if (null != unresolved) {
if (null == results) {
	results = new java.util.LinkedHashMap<String, Object>();
}
results.put("", unresolved);
								for (final String key: unresolved) {
									final String[] sa = null == key ? null : key.split("#", 2);
									if (2 == sa.length) {
										if (false == Boolean.TRUE.booleanValue()) {
										} else if (true == sa[0].equals("to-user")) {
											if (null != __user.get(sa[1])) {
												q.property(key, __user.get(sa[1]).toString());
											}
										} else if (true == sa[0].equals("role")) {
										  //q.property(key, __role.get(sa[1]).toString());
										} else if (true == sa[0].equals("group")) {
										  //q.property(key, __group.get(sa[1]).toString());
										} else {
										  //throw new IllegalArgumentException();
										}
									} else {
										if (false == Boolean.TRUE.booleanValue()) {
										} else if (null != __from_user.get("properties")) {
											final java.util.HashMap<String, String> __prop = (java.util.HashMap<String, String>)__from_user.get("properties");
											if (null != __prop.get(sa[0])) {
												q.property(key, __prop.get(sa[0]).toString());
											}
										} else {
										  //throw new IllegalArgumentException();
										}
									}
								}
							}
						}
						if (null != replacements) {
						  /*final */int __replacement_index = replacement_index;
							for (final String key: replacements.keySet()) {
								final String[] values = replacements.get(key);
								if (1 == values.length) {
									__replacement_index = 0;
								}
								final String value = values[__replacement_index];
								q.property(key, value);
							}
						}
						final String[] unresolved = dro.lang.String.unresolved(message, q);
						if (null == results) {
							results = new java.util.LinkedHashMap<String, Object>();
						}
						if (null != unresolved) {
							results.put(user_id, unresolved);
						} else {
							final java.util.Map<String, Object> history = new dao.util.HashMap<>();
							history.put("text-history-id", new Integer(Texts.history.size()));
							history.put("text-id", text.get("text-id"));
							history.put("from-user-id", /**/"admin");
							final java.util.Map<String, Object> properties = new dao.util.HashMap<String, Object>(){
								private static final long serialVersionUID = 0L;
								{
									for (final Object key: q.keySet()) {
										put((String)key, q.get(key).toString());
									}
								}
							};
							history.put("properties", properties);
							history.put("to-user-id", user_id);
						  /*final */Object o = null;
final String event_name = action;
final Long creation_time = new Long(System.currentTimeMillis());
final String uuid_as_char = ServletTool.generateString(8);
final String from = "admin#"; // TODO: fix this ambiguous multi-purpose nature 
final String to = (String)__user.get("sms");
final String body = dro.lang.String.resolve(message, q);
							if (true == Services.checkService("1:sms", from_user_id)) {
								final Integer reservation_id = Services.reserveService("1:sms", from_user_id, 1.0f);
								if (null != reservation_id) {
									try {
										final String __data = /*this.*/adapter.post(
											p.property(url),
											body,
											dro.lang.String.Return.String
										);
										try {
											o = new JSONParser().parse(__data);
											history.put("$state", "success");
										} catch (final ParseException e) {
											throw new RuntimeException(e);
										}
									} catch (final RuntimeException e) {
										history.put("$state", "failure");
										history.put("$state:failure", e.toString());
									}
									if (null != o) {
									  /*final */JSONObject jo = (JSONObject)o;
									  /*final JSONObject */jo = (JSONObject)jo.get("SendMessageWithReferenceExtendedResult");
										final Long messageID = (Long)jo.get("MessageID");
										if (null != messageID) {
/**/										history.put("$message-id", messageID);
										}
										Services.consumeService("1:sms", from_user_id/*, reservation_id*/, 1.0f);
									  /*Float numnber_of_units = */Services.unreserveService("1:sms", from_user_id/*, reservation_id*/);
									}
								}
							}
							Texts.history.add(history); // If not sent (re. checkService) then provide reason why
final dro.util.Properties __p = new dro.util.Properties(){
	private static final long serialVersionUID = 0L;
	{
		property("event-name", event_name);
		property("creation-time", creation_time.toString());
		property("uuid-as-char", uuid_as_char);
		property("from", from);
		property("to", to);
		property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
 /*FIX*/property("message-id", null == history.get("$message-id") ? "" : ((Long)history.get("$message-id")).toString());
		property("type", "sms:outbound");
		property("text-history-id", history.get("text-history-id").toString());
	}
};
final String state = (String)history.get("$state");
if (null != state) {
	__p.property("state", (String)history.get("$state"));
	if (true == state.equals("failure")) {
		__p.property("state:failure", (String)history.get("$state:failure"));
	}
}
Helper.cacheEvent(Texts.class, uuid_as_char, __p);
Helper.cacheEventX(Texts.class, uuid_as_char, new java.util.HashMap<String, Object>(){
	private static final long serialVersionUID = 0L;
	{
		put("event-name", event_name);
		put("creation-time", creation_time);
		put("uuid-as-char", uuid_as_char);
		put("process-time", new Long(System.currentTimeMillis()));
		put("type", "sms:outbound");
	}
});
							results.put(user_id, history);
							__user.put(action+"-sms-state", 4);
							__user.remove(action+"-sms-transaction-id");
						}
					} catch (final ClientProtocolException exc) {
						__user.put(action+"-sms-state", -3);
						__user.put(action+"-sms-error", exc.getMessage());
					} catch (final FileNotFoundException exc) {
						__user.put(action+"-sms-state", -3);
						__user.put(action+"-sms-error", exc.getMessage());
					} catch (final IOException exc) {
						__user.put(action+"-sms-state", -3);
						__user.put(action+"-sms-error", exc.getMessage());
					} finally {
					}
				}
			}/*.start();*/
		}
		return results;
	}
	
	private static boolean shutdown = false;
	
	public static void shutdown() {
		if (null != Texts.thread) {
			Texts.shutdown = true;
		}
	}
}