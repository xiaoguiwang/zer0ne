package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TextsLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		req.setAttribute("texts", Texts.texts);
		if (null != req.getParameter("history")) {
			req.setAttribute("texts#history", Texts.history);
		}
		
	  /*final java.util.List<java.util.Map<String, Object>> __texts = texts;
		if (null != __texts) {
			for (final java.util.Map<String, Object> __text: __texts) {
				final String id = __text.get("$id").toString();
				req.setAttribute("text$"+id+"#checked", true);
			}
		}*/
		
		req.removeAttribute("meta-http-equiv");
	  /*final */String path = req.getPathInfo();
		final String submit = req.getParameter("submit");
		if (null != submit && 0 < submit.trim().length()) {
		  /*final */boolean complete = false;
			if (true == submit.equals("update")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __text_ids = map.get("text-ids");
				for (int i = 0; i < __text_ids.length; i++) {
					final String checked = req.getParameter("text$"+__text_ids[i]+"#checked");
					final java.util.Map<String, Object> __text = Texts.getId(__text_ids[i]);
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
				  /*} else if (null != __role_id) {
						if (false == Groups.hasPermission(__text, __role_id)) {
							if (null != checked && true == checked.equals("on")) {
								Groups.addPermission(__text, __role_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Groups.removePermission(__text, __role_id);
							}
						}
						complete = true;
					} else if (null != __user_id) {
						if (false == Groups.hasRole(__text, __user_id)) {
							if (null != checked && true == checked.equals("on")) {
								Groups.addRole(__text, __user_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Groups.removeRole(__text, __user_id);
							}
						}
						complete = true;*/
					} else if (null != __text) {
					///*final */java.util.Map<String, Integer> __text_keys = (java.util.Map<String, Integer>)__bill.get("texts");
					  //if (false == Bills.hasAddress(__bill, __text_id)) {
							if (null != checked && true == checked.equals("on")) {
							  //Bills.addAddress(__bill, __text_id);
							}
					  //} else {
							if (null == checked || false == checked.equals("on")) {
							  //Bills.removeAddress(__bill, __text_id);
							}
					  //}
						complete = true;
					} else {
						req.setAttribute("text$"+__text_ids[i]+"#checked", new String("on").equals(checked));
					}
				}
			} else if (true == submit.equals("delete")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __text_ids = map.get("text-ids");
				for (int i = 0; i < __text_ids.length; i++) {
					final String checked = req.getParameter("text$"+__text_ids[i]+"#checked");
					final java.util.Map<String, Object> __text = Texts.getId(__text_ids[i]);
				  //final Integer __text_id = (Integer)__text.get("text-id");
					if (null != checked && true == checked.equals("on")) {
						Texts.texts.remove(__text/*_id.intValue()*/);
					}
				}
			}
			if (false == complete) {
			  /*final String */path = req.getPathInfo();
			  /*final String */path = uri.replace("/hci", "");
			} else {
			  /*final String */path = "/index";
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />");
			}
		} else {
		  /*final String */path = req.getPathInfo();
		  /*final String */path = uri.replace("/hci", "");
		}
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}