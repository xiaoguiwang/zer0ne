package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenConfirmLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)super.property("users");
		
		@SuppressWarnings("unchecked")
		final java.util.Map<String, java.util.Map<String, Object>> tokens = (java.util.Map<String, java.util.Map<String, Object>>)super.property("tokens");
		
		req.setAttribute("html-form", true);
		final String token = null == req.getParameter("token") ? "" : req.getParameter("token");
		req.setAttribute("token", token);
	  //final String type = null == req.getParameter("token-type") ? "" : req.getParameter("token-type");
	  //req.setAttribute("token-type", type);
		final java.util.Map<String, Object> map = (
			null == token || 0 == token.trim().length() || false == tokens.containsKey(token)
			?
			null
			:
			tokens.get(token)
		);
		final String user_id = (String)map.get("user-id");
		final java.util.Map<String, Object> user = (
			null == user_id || 0 == user_id.trim().length()
			?
			null
			:
			users.get(user_id)
		);
		if (null != token && null != user) {
			req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='/hci/confirm?redirect=/hci/confirming&html-form&user-id="+user_id+"&token="+token+"'\" />"); // TODO: url encoding
			final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/confirm.jsp");
			requestDispatcher.include(req, resp);
		} else {
			throw new IllegalStateException();
		}
	}
}