package hci.j2ee;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenConfirmedLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)super.property("users");
		
		final Map<String, String> qs = ServletTool.getQueryStringAsKeyValueMap(req);
		
	  /*final */Boolean html_form = Boolean.FALSE;
		final String __html_form = qs.get("html-form");
		if (true == qs.containsKey("html-form") && 0 == __html_form.length()) {
			html_form = Boolean.TRUE;
		}
		
		if (true == html_form) {
			
			@SuppressWarnings("unchecked")
			final java.util.Map<String, java.util.Map<String, Object>> tokens = (java.util.Map<String, java.util.Map<String, Object>>)super.property("tokens");
			
			req.setAttribute("html-form", true);
			final String token = null == req.getParameter("token") ? "" : req.getParameter("token");
			req.setAttribute("token", token);
		  //final String type = null == req.getParameter("token-type") ? "" : req.getParameter("token-type");
		  //req.setAttribute("token-type", type);
			final java.util.Map<String, Object> map = (
				null == token || 0 == token.trim().length() || false == tokens.containsKey(token)
				?
				null
				:
				tokens.get(token)
			);
			final String user_id = (String)map.get("user-id");
			final java.util.Map<String, Object> user = (
				null == user_id || 0 == user_id.trim().length()
				?
				null
				:
				users.get(user_id)
			);
		  /*final */Boolean confirmed = false;
			if (null != map) {
				final String type = (String)map.get("token-type");
				if (null != type && 0 < type.trim().length()) {
				  /*final Boolean */confirmed = (Boolean)user.get("confirmed-"+type);
				}
			}
			if (null != confirmed && true == confirmed) {
				tokens.remove(token);
				req.removeAttribute("meta-http-equiv");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/confirmed.jsp");
				requestDispatcher.include(req, resp);
			} else {
				throw new IllegalStateException();
			}
			
		} else {
			
			throw new UnsupportedOperationException();
			
		}
	}
}