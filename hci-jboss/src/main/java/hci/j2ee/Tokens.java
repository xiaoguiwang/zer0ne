package hci.j2ee;

import java.text.SimpleDateFormat;

@SuppressWarnings("unchecked")
public class Tokens {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> tokens;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Tokens.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> */tokens = (java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("tokens", java.util.Map.class);
		if (null == tokens) {
			tokens = new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
			  /*{
					put("abc123", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("username", "arussell");
							put("token", "email");
						}
					});
					put("xyz123", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("username", "rwang");
							put("token", "sms");
						}
					});
				}*/
			};
			dao.Context.getAdapterFor(me).alias(tokens, "tokens");
		}
	}
	
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> token = null;
		for (final String token_id: tokens.keySet()) {
if (true == token_id.equals("$id")) continue;
			final java.util.Map<java.lang.String, java.lang.Object> __token = tokens.get(token_id);
			if (true == id.equals(__token.get("$id").toString())) {
				token = __token;
				break;
			}
		}
		return token;
	}
	public static java.util.Map<String, Object> getToken(final String byValue) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> token = null;
		if (null != byValue && 0 < byValue.trim().length()) {
			synchronized(tokens) {
				for (final String token_id: tokens.keySet()) {
if (true == token_id.equals("$id")) continue;
				  /*final */java.util.Map<java.lang.String, java.lang.Object> __token = tokens.get(token_id);
					if (true == token_id.equals(byValue)) {
						token = __token;
						break;
					}
				}
			}
		}
		return token;
	}
	
	private static /*final */java.lang.Thread thread;// = null;
	private static final java.util.Map<String, Object> service = new java.util.HashMap<String, Object>();
	
	public static void startup() {
		final java.util.Map<String, Object> __service = service;
		Tokens.service.put("starting", sdf.format(new java.util.Date()));
		try {
			Tokens.thread = new java.lang.Thread(){
				{
					__service.put("state", 1);
					__service.put("state", 2);
				}
				@Override
				public void run() {
					__service.put("started", sdf.format(new java.util.Date()));
					__service.remove("starting");
					while (null == __service.get("stopping")) {
						__service.put("state", 3);
						try {
							for (final String key: Tokens.tokens.keySet()) {
								final java.util.Map<String, Object> map = Tokens.tokens.get(key);
								final Boolean expired = (Boolean)map.get("$expired");
								if (null == expired || false == expired) {
									final Boolean confirmed = (Boolean)map.get("$confirmed");
									if (null == confirmed || false == confirmed) {
										final Long ms = (Long)map.get("$date+time");
										if (ms > System.currentTimeMillis()-60L*1000L/*seconds*/) {
										  //final String type = (String)map.get("$type");
											map.put("$expired", Boolean.TRUE);
										}
									}
								} else {
									final Long ms = (Long)map.get("$date+time");
									if (ms > System.currentTimeMillis()-60L*1000L/*seconds*/*10L/*minutes*/) {
									  //final String type = (String)map.get("$type");
										map.remove(key);
									}
								}
							}
							__service.put("last-check", sdf.format(new java.util.Date()));
							__service.put("state", 4);
						} catch (final Exception exc) {
							__service.put("state", -3);
							__service.put("error", exc.getMessage());
						} finally {
						}
						try {
							Thread.sleep(60*1000L);
						} catch (final InterruptedException e) {
							// Silently ignore..
						}
					}
					__service.put("stopped", sdf.format(new java.util.Date()));
					__service.remove("stopping");
				}
			};
			Tokens.thread.start();
		} catch (final Exception e) {
			__service.put("state", -1);
			__service.put("error", e.getMessage());
		}
	}
	
	public static void shutdown() {
		if (null != Tokens.thread) {
			Tokens.service.put("stopping", sdf.format(new java.util.Date()));
		}
	}
}