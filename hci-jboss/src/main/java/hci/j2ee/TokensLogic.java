package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokensLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
	  //req.removeAttribute("meta-http-equiv");
	  /*final */String path = req.getPathInfo();
		final String submit = req.getParameter("submit");
		if (null != submit && 0 < submit.trim().length()) {
		  /*final */boolean complete = false;
			if (true == submit.equals("update")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __token_ids = map.get("token-ids");
				for (int i = 0; i < __token_ids.length; i++) {
					final String checked = req.getParameter("token$"+__token_ids[i]+"#checked");
					final java.util.Map<String, Object> __token = Tokens.getId(__token_ids[i]);
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
					} else if (null != __token) {
						if (null != checked && true == checked.equals("on")) {
						}
						if (null == checked || false == checked.equals("on")) {
						}
						complete = true;
					} else {
						req.setAttribute("token$"+__token_ids[i]+"#checked", new String("on").equals(checked));
					}
				}
			} else if (true == submit.equals("delete")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __token_ids = map.get("token-ids");
				for (int i = 0; i < __token_ids.length; i++) {
					final String checked = req.getParameter("token$"+__token_ids[i]+"#checked");
					final java.util.Map<String, Object> __token = Tokens.getId(__token_ids[i]);
				  //final Integer __address_id = (Integer)__address.get("address-id");
					if (null != checked && true == checked.equals("on")) {
						Tokens.tokens.remove(__token.get("token-id"));
					}
				}
			}
			if (false == complete) {
			  /*final String */path = req.getPathInfo();
			  /*final String */path = uri.replace("/hci", "");
			} else {
			  /*final String */path = "/index";
			/**/req.setAttribute("users", Users.users); // Hacked, for testing groups off index
				req.setAttribute("signed-in-user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("signed-in-method", ServletTool.get(session, "signed-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />");
			}
		} else {
		  /*final String */path = req.getPathInfo();
		  /*final String */path = uri.replace("/hci", "");
		}
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}