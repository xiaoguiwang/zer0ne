package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserLogic extends Logic {
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
		final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		final String mode = req.getParameter("mode");
		final String submit = req.getParameter("__submit");
	  /*final */java.util.Map<String, String> results = null;
		
	  /*final */String user_id = (String)req.getParameter("user-id");
		if (null == user_id) {
			if (null == req.getParameter("mode") && null == req.getAttribute("mode")) {
				user_id = (String)ServletTool.get(session, "user-id");
			}
		} else {
			ServletTool.put(session, "user-id", user_id);
		}
		if (null != user_id) {
			req.setAttribute("user-id", user_id);
		} else {
		  //req.removeAttribute("user-id");
		}
		
		if ((null == submit || 0 == submit.trim().length()) && (null == req.getParameter("mode") || false == req.getParameter("mode").equals("create"))) {
			final java.util.Map<String, Object> user = Users.users.get(user_id);
			if (null != req.getParameter("reconfirm-email") || null != req.getParameter("reconfirm-sms")) {
				final java.util.Map<String, java.util.Map<String, Object>> tokens = hci.j2ee.Tokens.tokens;
				
			  /*final */String type = null;
				if (null != req.getParameter("reconfirm-email")) {
				  /*final String */type = "email";
				}
				if (null != req.getParameter("reconfirm-sms")) {
				  /*final String */type = "sms";
				}
				if (null != type) {
					user.put("confirmed-"+type, Boolean.FALSE);
					
					final String token = ServletTool.generateString(4);
					
					final dao.util.HashMap<String, Object> map = new dao.util.HashMap<>();
					map.put("user-id", user_id);
					map.put("date+time", System.currentTimeMillis());
					
					tokens.put(token, map);
					
					ServletTool.put(session, type+"-token", token);
					
					if (true == type.equals("email")) {
						Emails.sendEmail("signing-up", "0"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
							private static final long serialVersionUID = 0L;
							{
								put("signing-up-token", new String[]{token});
							}
						});
					}
					
					if (true == type.equals("sms")) {
						Texts.sendText("signing-up", "0"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
							private static final long serialVersionUID = 0L;
							{
								put("signing-up-token", new String[]{token});
							}
						});
					}
				}
			}
			if (null != req.getParameter("reset-password")) {
				final java.util.Map<String, java.util.Map<String, Object>> tokens = hci.j2ee.Tokens.tokens;
				
			  /*final */String type = null;
				if (true == req.getParameter("reset-password").equals("email")) {
				  /*final String */type = "email";
				}
				if (true == req.getParameter("reset-password").equals("sms")) {
				  /*final String */type = "sms";
				}
				if (null != type) {
					user.put("reset-password:"+type, Boolean.FALSE);
					
					final String token = ServletTool.generateString(4);
					
					final dao.util.HashMap<String, Object> map = new dao.util.HashMap<>();
					map.put("user-id", user_id);
					map.put("date+time", System.currentTimeMillis());
					
					tokens.put(token, map);
					
					ServletTool.put(session, type+"-token", token);
					
					if (true == type.equals("email")) {
						Emails.sendEmail("password-reset-password", "1"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
							private static final long serialVersionUID = 0L;
							{
								put("password-reset-token", new String[]{token});
							}
						});
					}
					
					if (true == type.equals("sms")) {
						Texts.sendText("password-reset-password", "1"/*FIXME!*/, "admin", new String[]{user_id}, new java.util.HashMap<String, String[]>(){
							private static final long serialVersionUID = 0L;
							{
								put("password-reset-token", new String[]{token});
							}
						});
					}
				}
			}
			req.setAttribute("user", user);
		} else if (null != submit && 0 < submit.trim().length()) {
		  /*final */ java.util.Map<String, Object> user = (java.util.Map<String, Object>)Users.users.get(user_id);
		  /*final */boolean complete = true;
		  /*final java.util.Map<String, String> */results = new java.util.HashMap<>();
			if (false == java.lang.Boolean.TRUE.booleanValue()) {
			} else if (true == submit.equals("create") && null == user) {
				// Validate data first!
				user = new dao.util.HashMap<String, Object>();
				user.put("user-id", user_id);
				if (null == user_id || 0 == user_id.trim().length()) {
					results.put("user-id", "please specify a user-id");
				}
				for (final String key: new String[]{"preferred-name","email","confirmed-email","sms","confirmed-sms","password","passphrase"}) {
				  /*final */Object value = (String)req.getParameter(key);
					if (true == key.equals("preferred-name")) {
						final String preferred_name = (String)value;
						if (null == preferred_name) { // || 0 == preferred_name.trim().length()) {
							results.put("preferred-name", "please specify a preferred name");
						}
					} else if (true == key.equals("password") && null != req.getParameter("password-validate")) {
						final String password = (String)value;
						if (null == password || 0 == password.trim().length()) {
							results.put("password", "please provide a password");
						} else {
							final String validate = (String)req.getParameter("password-validate");
							if (null != validate && 0 < ((String)value).trim().length() && false == ((String)value).trim().equals(validate.trim())) {
								results.put("password-validate", "passwords do not match");
								complete = false;
							}
						}
					} else if (true == key.equals("email")) {
						final String email = (String)value;
						if (null == email || 0 == email.trim().length()) {
							results.put("email", "please specify an e-mail address");
						}
					} else if (true == key.equals("sms")) {
						final String sms = (String)value;
						if (null == sms || 0 == sms.trim().length()) {
							results.put("sms", "please specify an sms-capable phone number");
						}
					} else if (true == key.equals("confirmed-email") || true == key.equals("confirmed-sms")) {
						final String checked = req.getParameter(key);
						value = new Boolean(null != checked && true == checked.equals("on"));
					}
					if (null == value) {
						req.removeAttribute(key);
					} else {
						boolean remove = true;
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (true == value instanceof String) {
							if (0 < ((String)value).length()) {
								remove = false;
							}
						} else if (true == value instanceof Boolean) {
							remove = false;
						} else {
							throw new IllegalArgumentException();
						}
						if (true == remove) {
							req.removeAttribute(key);
						} else {
							user.put(key, value);
							req.setAttribute(key, value);
						}
					}
				}
				Users.users.put(user_id, user);
			} else if (true == submit.equals("update") && null != user) {
				for (final String key: new String[]{"preferred-name","email","confirmed-email","sms","confirmed-sms","password","passphrase","sq:nonce"}) {
				  /*final */Object value = (String)req.getParameter(key);
					if (true == key.equals("preferred-name")) {
						final String preferred_name = (String)value;
						if (null == preferred_name) { // || 0 == preferred_name.trim().length()) {
							results.put("preferred-name", "please specify a preferred name");
						}
					} else if (true == key.equals("password") && null != req.getParameter("password-validate")) {
						final String password = (String)value;
						if (null == password || 0 == password.trim().length()) {
							results.put("password", "please provide a password");
						} else {
							final String validate = (String)req.getParameter("password-validate");
							if (null != validate && 0 < ((String)value).trim().length() && false == ((String)value).trim().equals(validate.trim())) {
								results.put("password", "user-id and password provided do not match");
								complete = false;
							}
						}
					} else if (true == key.equals("email")) {
						final String email = (String)value;
						if (null == email || 0 == email.trim().length()) {
							results.put("email", "please specify an e-mail address");
						}
					} else if (true == key.equals("sms")) {
						final String sms = (String)value;
						if (null == sms || 0 == sms.trim().length()) {
							results.put("sms", "please specify an sms-capable phone number");
						}
					} else if (true == key.equals("confirmed-email") || true == key.equals("confirmed-sms")) {
						final String checked = req.getParameter(key);
						value = new Boolean(null != checked && true == checked.equals("on"));
					}
					if (null == value && null != user.get(key)) {
					  //req.removeAttribute(key);
						user.remove(key);
					} else if (null != value && false == value.equals(user.get(key))) {
						if (true == key.equals("password") && null != req.getParameter("password-validate")) {
							final String validate = (String)req.getParameter("password-validate");
							if (null != validate && false == ((String)value).trim().equals(validate.trim())) {
								complete = false;
							}
						}
						boolean remove = true;
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (true == value instanceof String) {
							if (0 < ((String)value).length()) {
								remove = false;
							}
						} else if (true == value instanceof Boolean) {
							remove = false;
						} else {
							throw new IllegalArgumentException();
						}
						if (true == remove) {
							req.removeAttribute(key);
						} else {
							user.put(key, value);
							req.setAttribute(key, value);
						}
					} else if (true == key.equals("sq:nonce")) {
					  /*final */String customer_id = (String)user.get("sq:customer#id");
						if (null == customer_id || 0 == customer_id.trim().length()) {
						  /*final String */customer_id = Users.getCustomerId();
							if (null != customer_id && 0 < customer_id.trim().length()) {
								user.put("sq:customer#id", customer_id);
							}
						}
						final java.util.List<java.util.Map<String, Object>> cards = (java.util.List<java.util.Map<String, Object>>)user.get("%cards");
						for (final java.util.Map<String, Object> card: cards) {
							final String nonce = req.getParameter("sq:nonce");
						  /*final */String card_id = (String)card.get("sq:card#id");
							if (null == card_id || 0 == card_id.trim().length()) {
							  /*final String */card_id = Users.getCustomerCardId(customer_id, nonce);
								if (null != card_id && 0 < card_id.trim().length()) {
									card.put("sq:card#id", card_id);
								}
							}
						  //final String obfuscated = (String)card.get("obfuscated"); // XXXX-XXXX-XXXX-1234
						  //final String nickname = (String)card.get("nickname");
							break; // TODO: allow for more than one card in future..
						}
					}
				}
			} else if (true == submit.equals("delete") && null != user) {
				Users.users.remove(user_id); // FIXME! Currently, we're assuming no dependencies, for testing user sign-up/remove
			}
			req.setAttribute("user", user);
		}
		
		if (null != results && 0 < results.size()) {
			req.setAttribute("results", results);
		}
		
		if (null != mode) {
			req.setAttribute("mode", mode);
		}
		
	  /*final */String path = req.getPathInfo();
		final String url = req.getRequestURL().toString();
		if (null == submit || false == submit.equals("delete") || false == url.endsWith("/user")) {
		  /*final String */path = uri.replace("/hci", "");
		} else {
		  /*final String */path = uri.replace("/hci", "").replace("user", "users");
		}
		final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
		requestDispatcher.include(req, resp);
	}
}