package hci.j2ee;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import dro.lang.Break;

@SuppressWarnings("unchecked")
public class Users {
	private static final java.lang.Object me = new java.lang.Object(); 
	public static /*final */java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> users;// = null;
	
	static {
	  /*try {
			Properties.properties(new Properties(Users.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}*/
	  /*final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> */users = (java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>)dao.Context.getAdapterFor(me).alias("users", java.util.Map.class);
		if (null == users) {
			users = new dao.util.HashMap<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>>(){
				private static final long serialVersionUID = 0L;
				{
					put("admin", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("user-id", "admin");
							put("password", "1epinards");
							put("properties", new dao.util.HashMap<java.lang.String, java.lang.String>(){
								private static final long serialVersionUID = 0L;
								{
									put("http-stub-for-urls", "http://uat.itfromblighty");
									put("https-stub-for-urls", "http://uat.itfromblighty");
									put("signature-from-email-address", "do-not-email-us@itfromblighty");
								}
							});
							put("sq:customer#id", "");
							put("%cards", new dao.util.ArrayList<java.util.Map<String, Object>>(){
								private static final long serialVersionUID = 0L;
								{
									add(new dao.util.HashMap<java.lang.String, java.lang.Object>(){
										private static final long serialVersionUID = 0L;
										{
										  //put("sq:nonce", "");
											put("sq:card#id", "");
											put("obfuscated", ""); // XXXX-XXXX-XXXX-1234
											put("nickname", "");
										}
									});
								}
							});
						}
					});
					put("arussell", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("user-id", "arussell");
							put("preferred-name", "Alex");
							put("email", "alex@itfromblighty");
							put("confirmed-email", true);
							put("sms", "14039706104");
							put("confirmed-sms", true);
							put("password", "1epinards");
						}
					});
					put("rwang", new dao.util.HashMap<java.lang.String, java.lang.Object>(){
						private static final long serialVersionUID = 0L;
						{
							put("user-id", "rwang");
							put("preferred-name", "Lixing");
							put("email", "wanglixing@gmail.com");
							put("confirmed-email", true);
							put("sms", "15878882808");
							put("confirmed-sms", false);
							put("password", "Pas5w0rd");
						}
					});
					
				}
			};
			dao.Context.getAdapterFor(me).alias(users, "users");
		}
	  /*final java.util.Map<java.lang.String, java.lang.Object> permission = Permissions.permissions.get("any-user-update");
		final java.util.Map<java.lang.String, java.lang.Object> role = Roles.roles.get("admin");
		final java.util.Map<java.lang.String, java.lang.Object> user = Users.users.get("admin");
		final java.util.Map<java.lang.String, java.lang.Object> group = Groups.groups.get("admins");
		Permissions.addToRole(permission, "admin");
		Roles.addToGroup(role, "admins");
		Users.addToGroup(user, "admins");
		// --OR--
	  //Groups.addUser(group, "admin");
	  //Groups.addRole(group, "admin");
	  //Roles.addPermission(role, "any-user-update");*/
	}

	// SquareUp specific implementation (move out to an adapter custom at some point)
	public static String getCustomerId() {
	  /*final */dro.util.Properties p = null;
		try {
		  /*final dro.util.Properties */p = new dro.util.Properties(Bills.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		final dro.lang.String data = new dro.lang.String();
		if (null != p) {
			final dro.lang.Integer status = new dro.lang.Integer();
			final String __url = dto.http.Adapter.class.getName()+"#url";
			final String url = p.property(dto.http.Adapter.class.getName()+"#url");
			// https://developer.squareup.com/reference/square/customers-api/create-customer
			try {
				final java.lang.String idempotency_key = dro.lang.String.generateString(45);
				new dto.http.Adapter()
					.properties(p)
					.header("Square-Version", "2021-03-17")
					.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
					.header("Content-Type", "application/json")
					.post(
						p.property(__url, url+"/customers")
						 .property(__url), new JSONObject(){
							private static final long serialVersionUID = 0L;
							{
								put("idempotency_key", idempotency_key);
							  //put("company_name", "..");
							  //put("email_address", "..");
								put("family_name", "IT From Blighty");
								put("given_name", "IT From Blighty");
							  //put("nickname", "..");
							  //put("phone_number", "+1 (403) 970-6104");
							}
						}.toJSONString(),
						dto.http.Adapter.Return.Adapter
					)
					.status(status)
					.data(data)
				;
			  /*{
				  "customer": {
				    "id": "GSA67K1YGCSQQ47KSW7J7WX53M",
				    "created_at": "2020-05-27T01:06:18.682Z",
				    "updated_at": "2020-05-27T01:06:18Z",
				    "given_name": "John",
				    "family_name": "Doe",
				    "nickname": "Junior",
				    "company_name": "ACME Inc.",
				    "email_address": "john.doe.jr@acme.com",
				    "phone_number": "+1 (206) 222-3456",
				    "preferences": {
				      "email_unsubscribed": false
				    },
				    "creation_source": "THIRD_PARTY"
				  }
				}*/
			} catch (ClientProtocolException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	  /*final */String /*customer_*/id = null;
		if (null != data && null != data.value()) {
			try {
				final Object o = new JSONParser().parse(data.value());
				if (null != o) {
					final JSONObject jo = (JSONObject)o;
					if (null != jo) {
						final JSONObject customer = (JSONObject)jo.get("customer");
						if (null != customer) {
						  /*final String customer_*/id = (String)customer.get("id");
						}
					}
				}
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}
		return /*customer_*/id;
	}
	// SquareUp specific implementation (move out to an adapter custom at some point)
	// "cnon:card-nonce-ok"
	public static String getCustomerCardId(final String /*customer_*/id, final String /*customer_*/card_nonce) {
	  /*final */dro.util.Properties p = null;
		try {
		  /*final dro.util.Properties */p = new dro.util.Properties(Bills.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		final dro.lang.String data = new dro.lang.String();
		if (null != p) {
			final dro.lang.Integer status = new dro.lang.Integer();
			final String __url = dto.http.Adapter.class.getName()+"#url";
			final String url = p.property(dto.http.Adapter.class.getName()+"#url");
			// https://developer.squareup.com/reference/square/customers-api/create-customer-card
			try {
				new dto.http.Adapter()
					.properties(p)
					.header("Square-Version", "2021-03-17")
					.header("Authorization", "Bearer EAAAEGYfBCYBok0nuIMPj49TN2EUDkQsNLLkcitDxHn34d97ADsZFiBWM9IfD-My")
					.header("Content-Type", "application/json")
					.post(
						p.property("customer-id", /*customer_*/id)
						 .property(__url, url+"/customers/${customer-id}/cards")
						 .property(__url), new JSONObject(){
							private static final long serialVersionUID = 0L;
							{
								put("card_nonce", /*customer_*/card_nonce);
							}
						}.toJSONString(),
						dto.http.Adapter.Return.Adapter
					)
					.status(status)
					.data(data)
				;
			  /*{
				  "card": {
				    "id": "ccof:UCmf2iRjld10NxyN4GB",
				    "card_brand": "VISA",
				    "last_4": "5858",
				    "exp_month": 4,
				    "exp_year": 2023
				  }
				}*/
			} catch (final ClientProtocolException e) {
				throw new RuntimeException(e);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	  /*final */String /*customer_*/card_id = null;
		if (null != data && null != data.value()) {
			try {
				final Object o = new JSONParser().parse(data.value());
				if (null != o) {
					final JSONObject jo = (JSONObject)o;
					if (null != jo) {
						final JSONObject /*customer_*/card = (JSONObject)jo.get("card");
						if (null != card) {
						  /*final String customer_*/card_id = (String)/*customer_*/card.get("id");
						}
					}
				}
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}
		return /*customer_*/card_id;
	}
	
	public static java.util.Map<String, Object> get(final Integer id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> user = null;
		for (final java.lang.String user_id: users.keySet()) {
			final java.util.Map<java.lang.String, java.lang.Object> __user = users.get(user_id);
			if (true == id.equals(__user.get("$id"))) {
				user = __user;
				break;
			}
		}
		return user;
	}
	public static java.util.Map<java.lang.String, java.lang.Object> get(final java.lang.String id) {
		return Users.getId(id);
	}
	public static java.util.Map<java.lang.String, java.lang.Object> getId(final java.lang.String id) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> user = null;
		for (final java.lang.String user_id: users.keySet()) {
			final java.util.Map<java.lang.String, java.lang.Object> __user = users.get(user_id);
			if (true == id.equals(__user.get("$id").toString())) {
				user = __user;
				break;
			}
		}
		return user;
	}
	public static java.util.Map<java.lang.String, java.lang.Object> findUser(final java.lang.String bySign) {
	  /*final */java.util.Map<java.lang.String, java.lang.Object> user = null;
		if (null != bySign && 0 < bySign.trim().length()) {
			for (final java.lang.String user_id: users.keySet()) {
if (true == user_id.equals("$id")) continue;
			  /*final */java.util.Map<java.lang.String, java.lang.Object> __user = users.get(user_id);
				if (true == __user.get("user-id").equals(bySign)) {
					user = __user;
					break;
				} else if (null != __user.get("email") && true == __user.get("email").equals(bySign)) {
					user = __user;
					break;
				} else if (null != __user.get("sms") && true == __user.get("sms").equals(bySign)) {
					user = __user;
					break;
				}
			}
		}
		return user;
	}
	
	public static void addPermission(final java.util.Map<java.lang.String, java.lang.Object> user, final String permission_id) {
		Users.addPermission(user, permission_id, Break.Recursion);
		Permissions.addToUser(Permissions.permissions.get(permission_id), (String)user.get("user-id"), Break.Recursion);
	}
	public static void addPermission(final java.util.Map<java.lang.String, java.lang.Object> user, final String permission_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)user.get("permissions");
		if (null == permissions) {
		  /*final java.util.Set<java.lang.String> */permissions = new dao.util.HashSet<java.lang.String>();
			user.put("permissions", permissions);
		}
		permissions.add(permission_id);
	}
	public static void removePermission(final java.util.Map<java.lang.String, java.lang.Object> user, final String permission_id) {
		Users.removePermission(user, permission_id, Break.Recursion);
		Permissions.removeFromUser(Permissions.permissions.get(permission_id), (String)user.get("user-id"), Break.Recursion);
	}
	public static void removePermission(final java.util.Map<java.lang.String, java.lang.Object> user, final String permission_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)user.get("permissions");
		if (null != permissions) {
			permissions.remove(permission_id);
			if (0 == permissions.size()) {
				user.remove("permissions");
			}
		}
	}
	
	public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> user, final String role_id) {
		Users.addRole(user, role_id, Break.Recursion);
		Roles.addToUser(Roles.roles.get(role_id), (String)user.get("user-id"), Break.Recursion);
	}
	public static void addRole(final java.util.Map<java.lang.String, java.lang.Object> user, final String role_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)user.get("roles");
		if (null == roles) {
		  /*final java.util.Set<java.lang.String> */roles = new dao.util.HashSet<java.lang.String>();
			user.put("roles", roles);
		}
		roles.add(role_id);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> user, final String role_id) {
		Users.removeRole(user, role_id, Break.Recursion);
		Roles.removeFromUser(Roles.roles.get(role_id), (String)user.get("user-id"), Break.Recursion);
	}
	public static void removeRole(final java.util.Map<java.lang.String, java.lang.Object> user, final String role_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)user.get("roles");
		if (null != roles) {
			roles.remove(role_id);
			if (0 == roles.size()) {
				user.remove("roles");
			}
		}
	}
	
	public static void addToGroup(final java.util.Map<java.lang.String, java.lang.Object> user, final String group_id) {
		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)Groups.groups.get(group_id);
		if (null != group) {
			Users.addToGroup(user, group_id, Break.Recursion);
			Groups.addUser(group, (String)user.get("user-id"), Break.Recursion);
		}
	}
	public static void addToGroup(final java.util.Map<java.lang.String, java.lang.Object> user, final String group_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)user.get("groups");
		if (null == groups) {
		  /*final java.util.Set<java.lang.String> */groups = new dao.util.HashSet<java.lang.String>();
			user.put("groups", groups);
		}
		groups.add(group_id);
	}
	public static void removeFromGroup(final java.util.Map<java.lang.String, java.lang.Object> user, final String group_id) {
		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)Users.users.get(group_id);
		if (null != group) {
			Users.removeFromGroup(user, group_id, Break.Recursion);
			Groups.removeUser(group, (String)user.get("user-id"), Break.Recursion);
		}
	}
	public static void removeFromGroup(final java.util.Map<java.lang.String, java.lang.Object> user, final String group_id, final dro.lang.Break.Recursion br) {
	  /*final */java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)user.get("groups");
		if (null != groups) {
			groups.remove(group_id);
			if (0 == groups.size()) {
				user.remove("groups");
			}
		}
	}
	
	public static boolean hasPermission(final java.util.Map<java.lang.String, java.lang.Object> user, final String permission_id) {
	  /*final */boolean hasPermission = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != user) {
			if (null != permission_id && 0 < permission_id.trim().length()) {
				final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)user.get("permissions"); // User permissions
				hasPermission = null != permissions && true == permissions.contains(permission_id);
			}
			if (false == hasPermission) { // keep looking..
				final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)user.get("roles"); // User roles
				if (null != roles) {
					for (final String role_id: roles) {
						final java.util.Map<java.lang.String, java.lang.Object> role = Roles.roles.get(role_id);
						{
							final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions"); // User role permissions
							hasPermission = null != permissions && true == permissions.contains(permission_id);
						}
						if (true == hasPermission) break;
					}
				}
			}
			if (false == hasPermission) { // keep looking..
				final java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)user.get("groups"); // User groups
				if (null != groups) {
					for (final String group_id: groups) {
						final java.util.Map<java.lang.String, java.lang.Object> group = Groups.groups.get(group_id);
						{
							final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)group.get("permissions"); // User group permissions
							hasPermission = null != permissions && true == permissions.contains(permission_id);
						}
						if (true == hasPermission) break;
						final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // User group roles
						if (null != roles) {
							for (final String role_id: roles) {
								final java.util.Map<java.lang.String, java.lang.Object> role = Roles.roles.get(role_id);
								{
									final java.util.Set<java.lang.String> permissions = (java.util.Set<java.lang.String>)role.get("permissions"); // User group role permissions9
									hasPermission = null != permissions && true == permissions.contains(permission_id);
								}
								if (true == hasPermission) break;
							}
						}
						if (true == hasPermission) break;
					}
				}
			}
		}
		return hasPermission;
	}
	public static boolean hasRole(final java.util.Map<java.lang.String, java.lang.Object> user, final String role_id) {
	  /*final */boolean hasRole = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != user && null != role_id && 0 < role_id.trim().length()) {
			final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)user.get("roles"); // User roles
			hasRole = null != roles && true == roles.contains(role_id);
		}
		if (false == hasRole) { // keep looking..
			final java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)user.get("groups"); // User groups
			if (null != groups) {
				for (final String group_id: groups) {
					final java.util.Map<java.lang.String, java.lang.Object> group = Groups.groups.get(group_id);
					{
						final java.util.Set<java.lang.String> roles = (java.util.Set<java.lang.String>)group.get("roles"); // User group roles
						hasRole = null != roles && true == roles.contains(role_id);
					}
					if (true == hasRole) break;
				}
			}
		}
		return hasRole;
	}
	public static boolean inGroup(final java.util.Map<java.lang.String, java.lang.Object> user, final String group_id) {
	  /*final */boolean inGroup = false; // might default to true, in an allow only disavowed by explicit deny..
		if (null != user && null != group_id && 0 < group_id.trim().length()) {
			final java.util.Set<java.lang.String> groups = (java.util.Set<java.lang.String>)user.get("groups"); // User groups
			inGroup = null != groups && true == groups.contains(group_id);
		}
		return inGroup;
	}
}