package hci.j2ee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UsersLogic extends Logic {
	@SuppressWarnings("unchecked")
	@Override
	public void logic(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final String session_id = (String)req.getAttribute("session-id");
	  //final java.util.Map<String, Object> session = Sessions.sessions.get(session_id);
		
		final String uri = null == req.getAttribute("uri") ? req.getRequestURI().toString() : (String)req.getAttribute("uri");
		
		final java.util.Map<String, java.util.Map<String, Object>> users = hci.j2ee.Users.users;
		req.setAttribute("users", users);
		
	  /*final */java.util.Set<String> __users = null;
		
	  /*final */java.util.Map<String, Object> __permission = null;
	  /*final */java.util.Map<String, Object> __role = null;
	  /*final */java.util.Map<String, Object> __group = null;
		
	  /*final */String __permission_id = (String)req.getParameter("permission-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__permission_id)) __permission_id = null;
		if (null != __permission_id) {
			final java.util.Map<String, java.util.Map<String, Object>> permissions = (java.util.Map<String, java.util.Map<String, Object>>)super.property("permissions");
		  /*final java.util.Map<String, Object> */__permission = permissions.get(__permission_id);
			__users = (java.util.Set<String>)__permission.get("users");
		}
		req.setAttribute("permission-id", __permission_id);
		
	  /*final */String __role_id = (String)req.getParameter("role-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__role_id)) __role_id = null;
		if (null != __role_id) {
			final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)super.property("roles");
		  /*final java.util.Map<String, Object> */__role = roles.get(__role_id);
			__users = (java.util.Set<String>)__role.get("users");
		}
		req.setAttribute("role-id", __role_id);
		
	  /*final */String __group_id = (String)req.getParameter("group-id");
if (true == dro.lang.String.isNullOrTrimmedBlank(__group_id)) __group_id = null;
		if (null != __group_id) {
			final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)super.property("groups");
		  /*final java.util.Map<String, Object> */__group = groups.get(__group_id);
			__users = (java.util.Set<String>)__group.get("users");
		}
		req.setAttribute("group-id", __group_id);
		
		if (null != __users) {
			for (final String __user_id: __users) {
				final java.util.Map<String, Object> __user = (java.util.Map<String, Object>)Users.users.get(__user_id);
				final String id = __user.get("$id").toString();
				req.setAttribute("user$"+id+"#checked", true);
			}
		}
		
		req.removeAttribute("meta-http-equiv");
	  /*final */String path = req.getPathInfo();
		final String submit = req.getParameter("__submit");
		if (null != submit && 0 < submit.trim().length()) {
		  /*final */boolean complete = false;
			if (true == submit.equals("update")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __user_ids = map.get("user-ids");
				for (int i = 0; i < __user_ids.length; i++) {
					final String checked = req.getParameter("user$"+__user_ids[i]+"#checked");
					final java.util.Map<String, Object> __user = Users.getId(__user_ids[i]);
					if (false == java.lang.Boolean.TRUE.booleanValue()) {
					} else if (null != __permission_id) {
						if (false == Users.hasPermission(__user, __permission_id)) {
							if (null != checked && true == checked.equals("on")) {
								Users.addPermission(__user, __permission_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Users.removePermission(__user, __permission_id);
							}
						}
						complete = true;
					} else if (null != __role_id) {
						if (false == Users.hasRole(__user, __role_id)) {
							if (null != checked && true == checked.equals("on")) {
								Users.addRole(__user, __role_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Users.removeRole(__user, __role_id);
							}
						}
						complete = true;
					} else if (null != __group_id) {
						if (false == Users.inGroup(__user, __group_id)) {
							if (null != checked && true == checked.equals("on")) {
								Users.addToGroup(__user, __group_id);
							}
						} else {
							if (null == checked || false == checked.equals("on")) {
								Users.removeFromGroup(__user, __group_id);
							}
						}
						complete = true;
					} else {
						req.setAttribute("user$"+__user_ids[i]+"#checked", new String("on").equals(checked));
					}
				}
			} else if (true == submit.equals("delete")) {
				final java.util.Map<String, String[]> map = req.getParameterMap();
				final java.lang.String[] __user_ids = map.get("user-ids");
				for (int i = 0; i < __user_ids.length; i++) {
					final String checked = req.getParameter("user$"+__user_ids[i]+"#checked");
					final java.util.Map<String, Object> __user = Users.getId(__user_ids[i]);
					final String __user_id = (String)__user.get("user-id");
					if (null != checked && true == checked.equals("on")) {
						users.remove(__user_id);
					}
				}
			} else {
				throw new IllegalStateException();
			}
			if (false == complete) {
			  /*final String */path = req.getPathInfo();
			  /*final String */path = uri.replace("/hci", "");
			} else {
			  /*final String */path = "/index";
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"0;url='/hci/index?session-id="+session_id+"'\" />"); // TODO: url encoding
			}
		} else {
		  /*final String */path = req.getPathInfo();
		  /*final String */path = uri.replace("/hci", "");
		}
		final String url = req.getRequestURL().toString();
		if (true == url.endsWith("/user") || true == url.endsWith("/users")) {
			final RequestDispatcher requestDispatcher = req.getRequestDispatcher(path+".jsp");
			requestDispatcher.include(req, resp);
		}
	}
}