package hci.jboss;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

// https://www.journaldev.com/1933/java-servlet-filter-example-tutorial
public class Filter implements javax.servlet.Filter {
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	public class CharResponseWrapper extends HttpServletResponseWrapper {
		private CharArrayWriter output;
		public String toString() {
			return output.toString();
		}
		public CharResponseWrapper(final HttpServletResponse response){
			super(response);
			this.output = new CharArrayWriter();
		}
		public PrintWriter getWriter(){
			return new PrintWriter(this.output);
		}
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	
	@Override
	public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain) throws IOException, ServletException {
		final PrintWriter out = resp.getWriter();
		final CharResponseWrapper wrapper = new CharResponseWrapper((HttpServletResponse)resp);
		chain.doFilter(req, wrapper);
		final CharArrayWriter caw = new CharArrayWriter();
		caw.write(wrapper.toString());
		resp.setContentLength(caw.toString().length());
final String copy = caw.toString();
		out.write(caw.toString());
		out.close();
	}
	
	@Override
	public void destroy() {
	}
}