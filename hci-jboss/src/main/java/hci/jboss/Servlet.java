package hci.jboss;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.Properties;
import hci.j2ee.AddressLogic;
import hci.j2ee.AddressesLogic;
import hci.j2ee.BillLogic;
import hci.j2ee.BillsLogic;
import hci.j2ee.DefaultLogic;
import hci.j2ee.EmailLogic;
import hci.j2ee.Emails;
import hci.j2ee.EmailsLogic;
import hci.j2ee.EventLogic;
import hci.j2ee.Events;
import hci.j2ee.EventsLogic;
import hci.j2ee.GroupLogic;
import hci.j2ee.Groups;
import hci.j2ee.GroupsLogic;
import hci.j2ee.PermissionLogic;
import hci.j2ee.Permissions;
import hci.j2ee.PermissionsLogic;
import hci.j2ee.RoleLogic;
import hci.j2ee.Roles;
import hci.j2ee.RolesLogic;
import hci.j2ee.ServiceLogic;
import hci.j2ee.ServicesLogic;
import hci.j2ee.ServletTool;
import hci.j2ee.Sessions;
import hci.j2ee.SessionsLogic;
import hci.j2ee.SignInLogic;
import hci.j2ee.SignOutLogic;
import hci.j2ee.SignUpLogic;
import hci.j2ee.SignedInLogic;
import hci.j2ee.SignedOutLogic;
import hci.j2ee.SignedUpLogic;
import hci.j2ee.SigningInLogic;
import hci.j2ee.SigningOutLogic;
import hci.j2ee.SigningUpLogic;
import hci.j2ee.TextLogic;
import hci.j2ee.Texts;
import hci.j2ee.TextsLogic;
import hci.j2ee.TokenConfirmLogic;
import hci.j2ee.TokenConfirmedLogic;
import hci.j2ee.TokenConfirmingLogic;
import hci.j2ee.Tokens;
import hci.j2ee.TokensLogic;
import hci.j2ee.UserLogic;
import hci.j2ee.Users;
import hci.j2ee.UsersLogic;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
  //private static dao.Context context;// = null;
	
	static {
		try {
			if (null == Properties.properties()) {
				Properties.properties(new Properties(Servlet.class));
			}
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	  /*context = */new dao.Context();
	}
	
	public final Map<String, String> map = new HashMap<>();
///*private final */Ignite ignite = null;
	
	{
	  /*try {
			final TcpDiscoverySpi spi = new TcpDiscoverySpi();
			final TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
			tcMp.setAddresses(Arrays.asList("5CG9457X95s"));
			spi.setIpFinder(tcMp);
			final IgniteConfiguration config = new IgniteConfiguration();
			config.setClientMode(true);
			config.setDiscoverySpi(spi);
		*//*final Ignite *//*ignite = Ignition.start(config);
		} catch (final Exception e) {
			e.printStackTrace(System.err);
		}*/
		final java.util.Map<String, Object> admin_role = Roles.roles.get("admin");
		final java.util.Map<String, Object> admin_user = Users.users.get("admin");
		final java.util.Map<String, Object> admins_group = Groups.groups.get("admins");
		final java.util.Map<String, Object> any_user_create_permission = Permissions.permissions.get("any-user-create");
		Roles.addPermission(admin_role, (String)any_user_create_permission.get("permission-id"));
		final java.util.Map<String, Object> any_user_read_permission = Permissions.permissions.get("any-user-read");
		Roles.addPermission(admin_role, (String)any_user_read_permission.get("permission-id"));
		final java.util.Map<String, Object> any_user_update_permission = Permissions.permissions.get("any-user-update");
		Roles.addPermission(admin_role, (String)any_user_update_permission.get("permission-id"));
		final java.util.Map<String, Object> any_user_delete_permission = Permissions.permissions.get("any-user-delete");
		Roles.addPermission(admin_role, (String)any_user_delete_permission.get("permission-id"));
		Groups.addRole(admins_group, (String)admin_role.get("role-id"));
		Groups.addUser(admins_group, (String)admin_user.get("user-id"));
		final java.util.Map<String, Object> arussell = Users.users.get("arussell");
		final java.util.Map<String, Object> users_group = Groups.groups.get("users");
		Groups.addPermission(users_group, (String)any_user_read_permission.get("permission-id"));
		Groups.addUser(users_group, (String)arussell.get("user-id")); // Somehow, something, has blown away arussell's user-id (check map remove)
	}
	
	@Override
	public void init() {
		Events.startup();
		Emails.startup();
		Texts.startup();
		Tokens.startup();
		Sessions.startup();
	}
	@Override
	public void destroy() {
		Sessions.shutdown();
		Tokens.shutdown();
		Texts.shutdown();
		Emails.shutdown();
		Events.shutdown();
	}
	
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	
	// https://stackoverflow.com/questions/8933054/how-to-read-and-copy-the-http-servlet-response-output-stream-content-for-logging
	protected void doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
	  /*final */java.util.Map<String, Object> session = null;
	  /*final */String session_id = ServletTool.getSessionId(req, resp);
		if (null != session_id) {
			if (false == Sessions.sessions.containsKey(session_id)) {
			  //session = new dao.util.HashMap<String, Object>();
				session = new java.util.HashMap<String, Object>();
				session.put("$date+time", System.currentTimeMillis());
				Sessions.sessions.put(session_id, session);
			} else {
				session = Sessions.sessions.get(session_id);
			}
			final Map<String, String[]> pm = req.getParameterMap();
			final String[] sa = (String[])pm.get("js");
			if (null != sa && 1 <= sa.length) {
				ServletTool.put(session, "js", new Boolean(pm.get("js")[0]));
				return;
			}
			req.setAttribute("session-id", session_id); // Required by almost/every page
		} else {
			req.removeAttribute("session-id"); // Hint: this should never happen..
		}
		
		if (null != session) {
			final String sign_in_method = (String)ServletTool.get(session, "sign-in-method");
			if (null != sign_in_method) {
				req.setAttribute("sign-in-method", sign_in_method);
			}
			if (null != ServletTool.get(session, "user")) {
				req.setAttribute("signed-in-method", (String)ServletTool.get(session, "signed-in-method"));
			}
		}
		
		final String url = null == req.getAttribute("url") ? req.getRequestURL().toString() : (String)req.getAttribute("url");
		final Map<String, String> qs = ServletTool.getQueryStringAsKeyValueMap(req);
	  /*final */Boolean redirect = null;
		final String __redirect = qs.get("redirect");
		if (true == qs.containsKey("redirect") && 0 < __redirect.length()) {
			if (true == __redirect.equals("true") || (true == __redirect.equals("false"))) {
				redirect = new Boolean(__redirect);
			}
		}
		redirect = null == req.getAttribute("redirect") ? redirect : (Boolean)req.getAttribute("redirect"); // Always allow override by req
	  //resp.setHeader("Pragma", "no-cache");
	  //final boolean basic = null == authorization ? false : authorization.toLowerCase().startsWith("basic");
	  //req.setAttribute("a-href-append", ServletTool.href((String)req.getAttribute("a-href-append"), "session-id="+session_id)); // TODO: url encoding
	/*//if (null == dts.get(session_id)) {
			final String previous_dts = dts.get(session_id);
			req.setAttribute("previous-dts", previous_dts);
	  //}
		dts.put(session_id, new Long(System.currentTimeMillis()).toString());*/
		
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == url.endsWith("/hci/sign-up")) {
			if (null != __redirect) { // Not used (IIRC)
				req.setAttribute("user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/sign-up.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SignUpLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/signing-up")) {
			if (null != __redirect) {
				req.setAttribute("user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("sign-in-method", ServletTool.get(session, "sign-in-method"));
				req.setAttribute("alternative-sign-in-method", ServletTool.get(session, "alternative-sign-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signing-up.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SigningUpLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/confirm")) {
			if (null != __redirect) {
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/confirm.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new TokenConfirmLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/confirming")) {
			if (null != __redirect) {
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/confirming.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new TokenConfirmingLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/confirmed")) {
			if (null != __redirect) {
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/confirmed.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new TokenConfirmedLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/signed-up")) {
			if (null != __redirect) { // Not used (IIRC)
			  //req.setAttribute("user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-up.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SignedUpLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/sign-in")) {
			if (null != __redirect) { // Not used (IIRC)
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/sign-in.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SignInLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/signing-in")) {
			if (null != __redirect) {
				req.setAttribute("user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("sign-in-method", ServletTool.get(session, "sign-in-method"));
				req.setAttribute("alternative-sign-in-method", ServletTool.get(session, "alternative-sign-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signing-in.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SigningInLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/signed-in")) {
			if (null != __redirect) { // Used by basic-auth (only IIRC)
				req.setAttribute("user", ServletTool.get(session, "user"));
				req.setAttribute("signed-in-method", ServletTool.get(session, "signed-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-in.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SignedInLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/sign-out")) {
			if (null != __redirect) { // Not used (IIRC)
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/sign-out.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SignOutLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/signing-out")) {
			if (null != __redirect) {
				req.setAttribute("user", ServletTool.get(session, "user"));
				req.setAttribute("signed-in-method", ServletTool.get(session, "signed-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signing-out.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SigningOutLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/signed-out")) {
			if (null != __redirect) {
				req.setAttribute("user-id", ServletTool.get(session, "user-id"));
				req.setAttribute("sign-in-method", ServletTool.get(session, "sign-in-method"));
				req.setAttribute("alternative-sign-in-method", ServletTool.get(session, "alternative-sign-in-method"));
				req.setAttribute("meta-http-equiv", "<meta http-equiv=\"refresh\" content=\"2;url='"+__redirect+"'\" />");
				final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/signed-out.jsp");
				requestDispatcher.include(req, resp);
			} else {
				new SignedOutLogic()
					.logic(req, resp);
			}
		} else if (true == url.endsWith("/hci/users")) {
			req.setAttribute("permission-id", req.getParameter("permission-id"));
			req.setAttribute("role-id", req.getParameter("role-id"));
			req.setAttribute("group-id", req.getParameter("group-id"));
			new UsersLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/user")) {
			new UserLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/bills")) {
			req.setAttribute("role-id", req.getParameter("role-id"));
			req.setAttribute("user-id", req.getParameter("user-id"));
			req.setAttribute("group-id", req.getParameter("group-id"));
			new BillsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/bill")) {
			final String bill_id = req.getParameter("bill-id");
			session.put("bill-id", bill_id);
			req.setAttribute("bill-mode", req.getParameter("bill-mode"));
			new BillLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/addresses")) {
			req.setAttribute("role-id", req.getParameter("role-id"));
			req.setAttribute("user-id", req.getParameter("user-id"));
			req.setAttribute("group-id", req.getParameter("group-id"));
			req.setAttribute("bill-id", req.getParameter("bill-id"));
			if (null != req.getParameter("from")) {
				req.setAttribute("from", req.getParameter("from"));
			}
			if (null != req.getParameter("to")) {
				req.setAttribute("to", req.getParameter("to"));
			}
			new AddressesLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/address")) {
			final String address_id = req.getParameter("address-id");
			session.put("address-id", address_id);
			req.setAttribute("address-mode", req.getParameter("address-mode"));
			new AddressLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/groups")) {
			req.setAttribute("permission-id", req.getParameter("permission-id"));
			req.setAttribute("role-id", req.getParameter("role-id"));
			req.setAttribute("user-id", req.getParameter("user-id"));
			new GroupsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/group")) {
			final String group_id = req.getParameter("group-id");
			session.put("group-id", group_id);
			req.setAttribute("group-mode", req.getParameter("group-mode"));
			new GroupLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/roles")) {
			req.setAttribute("permission-id", req.getParameter("permission-id"));
			req.setAttribute("user-id", req.getParameter("user-id"));
			req.setAttribute("group-id", req.getParameter("group-id"));
			new RolesLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/role")) {
		final String role_id = req.getParameter("role-id");
			session.put("role-id", role_id);
			req.setAttribute("role-mode", req.getParameter("role-mode"));
			new RoleLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/permissions")) {
			req.setAttribute("role-id", req.getParameter("role-id"));
			req.setAttribute("user-id", req.getParameter("user-id"));
			req.setAttribute("group-id", req.getParameter("group-id"));
			new PermissionsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/permission")) {
			final String permission_id = req.getParameter("permission-id");
			session.put("permission-id", permission_id);
			req.setAttribute("permission-mode", req.getParameter("permission-mode"));
			new PermissionLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/texts")) {
		  //req.setAttribute("role-id", req.getParameter("role-id"));
		  //req.setAttribute("user-id", req.getParameter("user-id"));
		  //req.setAttribute("group-id", req.getParameter("group-id"));
			req.setAttribute("text-mode", req.getParameter("text-mode"));
			new TextsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/text")) {
			req.setAttribute("text-mode", req.getParameter("text-mode"));
			new TextLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/emails")) {
		  //req.setAttribute("role-id", req.getParameter("role-id"));
		  //req.setAttribute("user-id", req.getParameter("user-id"));
		  //req.setAttribute("group-id", req.getParameter("group-id"));
			req.setAttribute("email-mode", req.getParameter("email-mode"));
			new EmailsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/email")) {
			req.setAttribute("email-mode", req.getParameter("email-mode"));
			new EmailLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/tokens")) {
			new TokensLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/sessions")) {
			new SessionsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/services")) {
			new ServicesLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/service")) {
			new ServiceLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/events")) {
			new EventsLogic()
				.logic(req, resp);
		} else if (true == url.endsWith("/hci/event")) {
			new EventLogic()
				.logic(req, resp);
		} else if (true == url.contains("/hci/") && false == url.endsWith(".jsp")) {
			if (null != qs && true == qs.containsKey("sign-in-method") && true == qs.get("sign-in-method").equals("basic-auth")) {
				new SignInLogic()
					.logic(req, resp);
			} else {
				if (null == ServletTool.get(session, "user")) {
					ServletTool.put(session, "signed-in-method", "html-form");
					final String user_id = "admin";
					ServletTool.put(session, "sign-id", user_id);
					final java.util.Map<String, Object> user = Users.users.get(user_id);
					if (null != user.get("sign-in-this-date+time")) {
						user.put("sign-in-last-successful", user.get("sign-in-this-date+time"));
					}
					user.put("sign-in-this-date+time", sdf.format(new java.util.Date()));
					ServletTool.put(session, "sign-in-last-successful", user.get("sign-in-last-successful"));
					if (null != user.get("sign-in-attempts-failed")) {
						ServletTool.put(session, "sign-in-attempts-failed", user.remove("sign-in-attempts-failed"));
					}
					ServletTool.put(session, "user", user);
				}
				new DefaultLogic()
					.logic(req, resp);
			}
		} else {
			throw new IllegalStateException();
		}
	  //response.setStatus(HttpStatus.SC_OK);
	}
}