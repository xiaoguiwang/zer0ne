<!-- 404.jsp -->
<%@ page import="hci.j2ee.ServletTool" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
<title>IT From Blighty [408.jsp]</title>
<link rel="stylesheet" type="text/css" href="/hci/css/index.css?<%= ServletTool.getVersion() %>">
<style>
<% {
	final String font_family = null == request.getParameter("font-family") ? "Lucida Console, monospace" : request.getParameter("font-family");
	final String font_size   = null == request.getParameter("font-size"  ) ? "20px" : request.getParameter("font-size"  );
	final String font_weight = null == request.getParameter("font-weight") ? "bold" : request.getParameter("font-weight");
%>
td { position: relative; <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
body { font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
input { font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
select { text-align-last: center; padding-left: 29px; } font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
textarea { resize: none; font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>; outline: none; }
<% } %>
</style>
<!--script src="/hci/js/index.js"></script-->
</head>
<body>

<table border=0 id="id#table-1" cellpadding=10 cellspacing=0 height="100%" width="100%" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
<tr valign="middle">
<td align="center">
<table border=0 id="id#table-2" cellpadding=0 cellspacing=4 height="100%" width="100%">
<tr valign="middle">
<td align="center">
<table border=0 id="id#table-3" cellpadding=4 cellspacing=0 height="100%" width="100%">
<tr valign="middle">
<td align="center">

<table border=0 height="100%" width="100%">
<tr height="1%">
<td>
<table border=0>
<tr>
<td nowrap width="1%">
&nbsp;
</td>
<td nowrap width="98%">
</td>
<td nowrap width="1%">
</td>
</tr>
</table>
</td>
</tr>

<tr height="98%">
<td id="id#td" align="center">
<h1>IT From Blighty</h1>
</td>
</tr>

<tr height="1%">
<td align="center">

<font size="-1">408: Request Timeout</font>
<br/>
<font size="-1">&nbsp;</font>
<br/>
<font size="-1">&nbsp;</font>

</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</body>
</html>