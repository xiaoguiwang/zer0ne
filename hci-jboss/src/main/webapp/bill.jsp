<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<!-- bill.jsp -->
<% {
  /*final */String jsp = "IT From Blighty [bill.jsp]";
	final String[] split = request.getRequestURL().toString().split("/");
  /*final */int i = split.length;
	if (null == split[i-1] || 0 == split[i-1].trim().length()) {
		i = i-1;
	}
	if (true == split[i-1].startsWith("bill")) {
	  /*final String */jsp = "IT From Blighty ["+split[i-1]+"]";
	} else {
	  /*final String */jsp = "IT From Blighty..";
	}
	request.setAttribute("$html$head$title", jsp);
} %>
<%@ include file='/fragments/header.jsp' %>
<% {
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	if (true == redirecting) { %>
<h1>IT From Blighty</h1>
<% 	} else {
		{
			final String session_id = (String)request.getAttribute("session-id");
		  /*final */String __lines = request.getParameter("lines");
		  /*final */String __line = request.getParameter("line");
		  /*final */String __history = request.getParameter("history");
		  /*final */Integer __bill_id = null;
		  /*final */Integer __history_id = null;
		  /*final */Integer __line_id = null;
		  /*final */Integer __id = null;
			final String __mode = (String)request.getParameter("mode");
			final String qs_mode = null == __mode ? "" : "&mode="+__mode;
		  //if (null != __bill) {
				if (null != __lines) {
					if (null != __history) {
						final Object o = request.getAttribute("bill#lines#history");
						if (null != o) {
							if (true == o instanceof Boolean) {
							} else {
								final java.util.Map<String, Object> /*bill_*/history = (java.util.Map<String, Object>)request.getAttribute("bill#history");
								__id = (Integer)/*bill_*/history.get("$id");
							}
						}
					}
					__lines = "lines";
				} else {
					if (null != __line) {
						if (null != __history) {
							final Object o = request.getAttribute("bill#line#history");
							if (null != o) {
								if (true == o instanceof Boolean) {
									final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
									__bill_id = (Integer)bill.get("$id");
									final java.util.Map<String, Object> /*bill_*/history = (java.util.Map<String, Object>)request.getAttribute("bill#history");
									__id = (Integer)/*bill_*/history.get("$id");
								} else {
									if (null == request.getParameter("for-line-id")) {
										final java.util.Map<String, Object> /*bill_*/line_history = (java.util.Map<String, Object>)o;
										__id = (Integer)/*bill_*/line_history.get("$id");
									} else {
										final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
										__bill_id = (Integer)bill.get("$id");
										final java.util.Map<String, Object> /*bill_*/history = (java.util.Map<String, Object>)request.getAttribute("bill#history");
										__history_id = (Integer)/*bill_*/history.get("$id");
										final java.util.Map<String, Object> /*bill_*/line_history = (java.util.Map<String, Object>)request.getAttribute("bill#line#history");
										__id = (Integer)/*bill_*/line_history.get("$id");
									}
								}
							}
						}
					} else {
						if (null != __history) {
							final Object o = request.getAttribute("bill#history");
							if (null != o) {
								if (true == o instanceof Boolean) {
								} else {
									if (false == "create".equals(__mode)) { // ?!
										final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
										if (null != bill) {
								 			__bill_id = (Integer)bill.get("$id");
										}
										final java.util.Map<String, Object> /*bill_*/history = (java.util.Map<String, Object>)o;
										__id = (Integer)/*bill_*/history.get("$id");
									} else {
										final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
										if (null != bill) {
								 			__id = (Integer)bill.get("$id");
										}
									}
								}
							}
						} else {
							final Object o = request.getAttribute("bill");
							if (null != o) {
								if (true == o instanceof Boolean) {
								} else {
									final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)o;
									__id = (Integer)bill.get("$id");
								}
							}
						}
					}
				}
		  //}
			final String qs_lines = null == __lines ? "" : "lines&";
			final String qs_line = null == __line ? "" : "line&";
			final String qs_history = null == __history ? "" : "history&";
			final String qs_bill_id = null == __bill_id ? "" : "&bill-id="+__bill_id;
			final String qs_line_id = null == __line_id ? "" : "&line-id="+__line_id;
			final String qs_history_id = null == __history_id ? "" : "&history-id="+__history_id;
			final String qs_id = null == __id ? "" : "&id="+__id;
			final String __cancel = (String)request.getParameter("cancel");
			final String qs_cancel = null == __cancel ? "" : "&cancel="+__cancel; %>
<form id="id#form-bill" name="form-bill" method="POST" action="/hci/bill?<%= qs_lines %><%= qs_line %><%= qs_history %>session-id=<%= session_id %><%= qs_bill_id %><%= qs_line_id %><%= qs_history_id %><%= qs_id %><%= qs_mode %><%= qs_cancel %>">
<% 		}
		if (null != request.getAttribute("bill#line#history")) { %>
<%@include file='/fragments/bill-line-form-history.jsp' %>
<% 		} else 
		if (null != request.getAttribute("bill#lines#history")) { %>
<%@include file='/fragments/bill-lines-form-history.jsp' %>
<% 		} else
		if (null != request.getAttribute("bill#history")) { %>
<%@include file='/fragments/bill-form-history.jsp' %>
<% 		} else
		if (null != request.getAttribute("bill")) { %>
<%@include file='/fragments/bill-form.jsp' %>
<% 		} %>
</form>
<% 	}
} %>
<%@include file='/fragments/footer.jsp' %>