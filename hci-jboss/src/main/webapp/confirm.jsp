<!-- confirm.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty - Token confirm"); %>
<%@include file='/fragments/header.jsp' %>
<%
final Boolean confirm_email = null == request.getAttribute("confirm-email") ? false : (Boolean)request.getAttribute("confirm-email");
final Boolean confirm_sms = null == request.getAttribute("confirm-sms") ? false : (Boolean)request.getAttribute("confirm-sms");
%>
confirm:
<br/>
<% if (false == confirm_email && false == confirm_sms) { %>
<a href="/hci/confirm?email">e-mail</a>
|
<a href="/hci/confirm?sms">sms</a>
<% } %>
<% if (true == confirm_email || true == confirm_sms) { %>
<br/>
<%@include file='/fragments/confirm-form.jsp' %>
<% } %>
<%@include file='/fragments/footer.jsp' %>