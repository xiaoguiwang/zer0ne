<!-- fragments/address-form.jsp -->
<%
final java.util.Map<String, Object> signed_in_user = (java.util.Map<String, Object>)request.getAttribute("signed-in-user");
final java.util.Map<String, Object> address = (java.util.Map<String, Object>)request.getAttribute("address");
final Integer address_id;
final String country, code, detail;
final java.util.Set<String> roles, users, groups;
if (null == address) {
	address_id = null;
	country = "";
	code = "";
	detail = "";
	roles = null;
	users = null;
	groups = null;
} else {
	address_id = null == address.get("address-id") ? null : (Integer)address.get("address-id");
	country = null == address.get("country") ? "" : (String)address.get("country");
	code = null == address.get("code") ? "" : (String)address.get("code");
	detail = null == address.get("detail") ? "" : (String)address.get("detail");
	roles = (java.util.Set<String>)address.get("roles");
	users = (java.util.Set<String>)address.get("users");
	groups = (java.util.Set<String>)address.get("groups");
}
final String mode = null == request.getAttribute("address-mode") ? "" : (String)request.getAttribute("address-mode");
final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<form id="id#form-address" name="form-address" method="POST" action="/hci/address?session-id=<%= (String)request.getAttribute("session-id") %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>address-id:</td>
<td width="12%" align="center" nowrap><input id="id#address" name="address-id" type="text" value="<%= null == address_id ? "" : address_id.toString() %>"/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("address-id") ? "&nbsp;" : results.get("address-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>country: </td>
<td align="center" nowrap><input id="id#country" name="country" type="text" value="<%= country %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("country") ? "&nbsp;" : results.get("country") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>detail: </td>
<td align="center" nowrap><textarea id="id#detail" name="detail" cols="18" rows="4"><%= detail %></textarea>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("detail") ? "&nbsp;" : results.get("detail") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>zip/post-code: </td>
<td align="center" nowrap><input id="id#code" name="code" type="text" value="<%= code %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("code") ? "&nbsp;" : results.get("code") %></td>
<td></td>
</tr>
<% if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align="right" nowrap>roles: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>users: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>groups: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<% } else { %>
<tr>
<td align="right" nowrap><a href="/hci/roles?session-id=<%= (String)request.getAttribute("session-id") %>&address-id=<%= address.get("address-id").toString() %>">roles</a>: </td>
<td align="center" nowrap>
<% if (null != roles) { %>
<% 	for (final String role_id: roles) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/users?session-id=<%= (String)request.getAttribute("session-id") %>&address-id=<%= address.get("address-id").toString() %>">users</a>: </td>
<td align="center" nowrap>
<% if (null != users) { %>
<% 	for (final String user_id: users) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= (String)request.getAttribute("session-id") %>&user-id=<%= user_id %>"><%= (String)user.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/groups?session-id=<%= (String)request.getAttribute("session-id") %>&address-id=<%= address.get("address-id").toString() %>">groups</a>: </td>
<td align="center" nowrap>
<% if (null != groups) { %>
<% 	for (final String group_id: groups) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= group_id %>"><%= (String)group.get("group-id") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
<% } // if (true == mode.equals("create")) %>
</tr>
</table>
<br/>
<% if (null == address) { %>
<% 	if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-address-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 	} %>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } %>
|
<a href="/hci/addresses?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>