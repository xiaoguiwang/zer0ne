<!-- fragments/addresses-form.jsp -->
<%
final java.util.List<java.util.Map<String, Object>> addresses = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("addresses");
final String __role_id = (String)request.getAttribute("role-id");
final String qs_role_id = null == __role_id ? "" : "&role-id="+__role_id;
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+__user_id;
final String __group_id = (String)request.getAttribute("group-id");
final String qs_group_id = null == __group_id ? "" : "&group-id="+__group_id;
final Integer __bill_id = (Integer)request.getAttribute("bill-id");
final String __bill_key = null != request.getAttribute("from") ? "from" : null != request.getAttribute("to") ? "to" : null; 
final String qs_bill = null == __bill_id ? "" : "&bill-id="+__bill_id.toString()+"&"+__bill_key;
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != __role_id) { %>
Update role addresses:
<% } else if (null != __user_id) { %>
Update user addresses:
<% } else if (null != __group_id) { %>
Update group addresses:
<% } else if (null != __bill_id) { %>
Update bill#<%= __bill_id %> <%= __bill_key %>-address:
<% } else { %>
Update addresses:
<% } %>
<br/>
<br/>
<form id="id#form-addresses" name="form-addresses" method="POST" action="/hci/addresses?session-id=<%= (String)request.getAttribute("session-id") %><%= qs_role_id %><%= qs_user_id %><%= qs_group_id %><%= qs_bill %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>address-id</td>
<td>country</td>
<td>detail</td>
<td>code</td>
</tr>
<% for (final java.util.Map<String, Object> address: addresses) {
	final Integer address_id = (Integer)address.get("address-id");
	final Integer id = (Integer)address.get("$id");
	final Boolean checked = null == request.getAttribute("address$"+id+"#checked") ? false : (Boolean)request.getAttribute("address$"+id+"#checked");
	final String country = null == address.get("country") ? "" : (String)address.get("country");
  /*final */String detail = ((String)address.get("detail")).replaceAll("\r\n", ", ").replaceAll(",,", ",");
	final int length = detail.length();
	detail = detail.substring(0, Math.min(length-1, 41));
	if (length > 41-1) {
		detail = detail+".."; // TODO: get this into a ServletTool thingy
	}
	final String code = null == address.get("code") ? "" : (String)address.get("code");
%>
<tr>
<td><%= id %><input id="id#address-ids" name="address-ids" type="hidden" value="<%= id %>"/></td>
<% final String checked_address = false == checked ? "" : " checked"; %>
<td><input id="id#address$<%= id %>#checked" name="address$<%= id %>#checked" type="checkbox"<%= checked_address %>/></td>
<td><a href="/hci/address?session-id=<%= (String)request.getAttribute("session-id") %>&address-id=<%= address_id %>"><%= address_id %></a></td>
<td><%= country %></td>
<td><%= detail %></td>
<td><%= code %></td>
</tr>
<% } %>
</table>
<br/>
<% if (null != __role_id || null != __user_id || null != __group_id || null != __bill_id) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% } %>
|
<a href="/hci/index?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>