<!-- fragments/bill-form-history.jsp -->
<% if (null == request.getAttribute("for:user#id")) {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
  /*final */Integer bill_id = null == bill ? null : (Integer)bill.get("$id");
  /*final */java.util.List<java.util.Map<String, Object>> /*bill_*/history_list = null;
	final Object o = request.getAttribute("bill#history");
  /*final */java.util.Map<String, Object> /*bill_*/history = null;
	if (true == o instanceof Boolean) {
		if (null != bill) {
		  /*final java.util.List<java.util.Map<String, Object>> bill_*/history_list = hci.j2ee.Bills.getHistoryForBillId((Integer)bill.get("$id"));
		}
	} else {
	  /*final java.util.Map<String, Object> bill_*/history = (java.util.Map<String, Object>)o;
		if (null == bill_id) {
			bill_id = null == /*bill_*/history ? null : (Integer)/*bill_*/history.get("bill-id");
		}
	}
	final String mode = (String)request.getAttribute("mode");
	final String cancel = (String)request.getAttribute("cancel");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
	final java.util.List<String> roles = null;
	final java.util.List<String> users = null;
	final java.util.List<String> groups = null;
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create bill history:
<% 	} else if (false == "delete".equals(mode)) { %>
Bill history:
<% 	} else { %>
Delete bill history:
<% 	} %>
<br/>
<br/>
<% 	if ((null == request.getAttribute("bill#history")) || (null != /*bill_*/history_list && 0 < /*bill_*/history_list.size())) { %>

<% 	if (null != request.getParameter("id")) { %><input name='id' type='hidden' value='<%= request.getParameter("id") %>'/><% } %>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>history-id</td>
<td>bill-id</td>
<td>for-user-id</td>
<td></td>
</tr>
<% 		for (final java.util.Map<String, Object> /*bill*/_history: /*bill_*/history_list) {
			final Integer id = (Integer)/*bill*/_history.get("$id");
 			final Boolean checked = null == request.getAttribute("bill-history$"+id+"#checked") ? false : (Boolean)request.getAttribute("bill-history$"+id+"#checked"); %>
<tr>
<td><%= id %><input id='id#bill-history-ids' name='bill-history-ids' type='hidden' value='<%= id %>'/></td>
<% 			final String checked_bill_history = false == checked ? "" : " checked"; %>
<td nowrap><input id='id#bill-history$<%= id %>#checked' name='bill-history$<%= id %>#checked' type='checkbox'<%= checked_bill_history %>/></td>
<% 			{
				final String key = "bill-history-id";
				final Integer value = null == /*bill*/_history ? null : (Integer)/*bill*/_history.get(key); %>
<td><a href='/hci/bill?history&session-id=<%= session_id %>&bill-id=<%= bill.get("$id") %>&id=<%= id %>&cancel=bill-history'><%= value %></a></td>
<% 			}
			{
				final String key = "bill-id";
				final String label = "bill-id";
				final String value = null == /*bill*/_history ? null : ((Integer)/*bill*/_history.get(label)).toString(); %>
<td><%= value %></td>
<% 			}
			{
				final String key = "bill:history:for-user-id";
				final String label = "for-user-id"; 
				final String value = null == /*bill*/_history ? null : (String)/*bill*/_history.get(label); %>
<td><%= value %></td>
<% 			}
			{
				final java.util.Map<String, Object> /*bill*/_lines_history = (java.util.Map<String, Object>)/*bill*/_history.get("%lines"); %>
<td>
<% 				if (null != /*bill*/_lines_history && 0 < /*bill*/_lines_history.size()) { %>
<a href="/hci/bill?lines&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= id %>&cancel=bill-history">lines</a>
<% 				} %>
</td>
<% 			}
			{
				final java.util.List<java.util.Map<String, Object>> /*bill*/_lines_history = (java.util.List<java.util.Map<String, Object>>)/*bill*/_history.get("@lines"); %>
<td>
<% 				if (null != /*bill*/_lines_history && 0 < /*bill*/_lines_history.size()) { %>
<a href="/hci/bill?lines&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= id %>&cancel=bill-history">lines</a>
<% 				} %>
</td>
<% 			} %>
</tr>
<% 		} %>
</table>
<br/>
<% 		if (false == "create".equals(mode)) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= bill_id %>&mode=create&cancel=bill-history">create</a>
|
<% 		}
		if (true == "create".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
|
<% 		} else { %>
<input id="id#submit" name="__submit" type="submit" value="delete"/>
|
<% 		} %>
<% 		if (true == "bills".equals(cancel)) { %>
<a href="/hci/bills?session-id=<%= session_id %>">cancel</a>
<% 		} else { %>
<a href="/hci/bill?session-id=<%= session_id %>&id=<%= bill_id %>&cancel=bills">cancel</a>
<% 		} %>

<% 	} else { %>

<% 		if (true == "create".equals(mode)) {
			if (null == request.getAttribute("bill#history")) { %>
<input name='id' type='hidden' value='<%= (Integer)bill.get("$id") %>'/>
<% 			} else if (null != bill) { %>
<input name='bill-id' type='hidden' value='<%= (Integer)bill.get("$id") %>'/>
<input name='id' type='hidden' value='<%= (Integer)/*bill_*/history.get("$id") %>'/>
<% 			} %>
<% 		} else { %>
<input name='id' type='hidden' value='<%= (Integer)/*bill_*/history.get("$id") %>'/>
<% 		} %>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<% 		{
			final String key = "bill-history-id";
			final Integer value = null == /*bill_*/history ? null : (Integer)/*bill_*/history.get(key); %>
<td width='44%' align='right' nowrap><%= key %>: </td>
<td width='12%' align='left' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td width='44%' align='left'></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
<tr>
<% 		{
			final String key = "bill-id";
			final String label = "bill-id";
			final String value = null == bill ? null : ((Integer)bill.get("bill-id")).toString(); %>
<td align='right' nowrap><%= label %>: </td>
<td width='center' nowrap><input name='<%= key %>' type='hidden' value='<%= value %>'/><input id='id#<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
<tr>
<% 		{
			final String key = "bill:history:for-user-id";
			final String label = "for-user-id"; 
			final String value = null == history ? null : (String)/*service_*/history.get(label); %>
<td align='right' nowrap><% if (true == "create".equals(mode)) { %><a href="/hci/bill?for-user-id&history&session-id=<%= session_id %><%= null == bill ? "" : "&id="+(Integer)bill.get("$id") %>&mode=<%= mode %>&cancel=bill-history"><% } %>for:user#id<% if (true == "create".equals(mode)) { %></a><% } %>: </td>
<td align='left' nowrap><input name='<%= key %>' type='hidden' value='<%= null == value ? "" : value %>'/><%= null == value ? "" : value %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
<tr>
<% 		{
			final String key = "$state";
			final String label = "$state";
			final String value = null == /*bill_*/history ? "open" : null == /*bill_*/history.get(label) ? "open" : (String)/*bill_*/history.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (true == "create".equals(mode)) { %><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>'/><% } else { %><%= null == value ? "" : value %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
<tr>
<% 		{
			final String key = "$payment";
			final String label = "$payment";
			final String value = null == /*bill_*/history ? null : null == /*bill_*/history.get(label) ? null : (String)/*bill_*/history.get(label); %>
<td align='right' nowrap><% if (false == "payment".equals(mode)) { %><a href='/hci/bill?history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= (Integer)history.get("$id") %>&mode=payment&cancel=bill-history'><% } %><%= label %><% if (false == "payment".equals(mode)) { %></a><% } %>: </td>
<td align='left' nowrap><%= null == value ? "" : value %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>

<% 	/*final */Float total = null;
	final Float	tax_gst = null == bill || null == bill.get("tax-gst") ? null : (Float)bill.get("tax-gst");
	if (null != /*bill_*/history) {
		final java.util.List<java.util.Map<String, Object>> /*bill_*/lines_history = (java.util.List<java.util.Map<String, Object>>)/*bill_*/history.get("@lines");
		if (null != /*bill_*/lines_history) {
			for (final java.util.Map<String, Object> /*bill_*/line_history: /*bill_*/lines_history) {
				final String /*service_*/line_id = null == /*bill_*/line_history.get("service-line-id") ? "" : (String)/*bill_*/line_history.get("service-line-id");
				final Float number_of_units_reserved = null == /*bill_*/line_history.get("number-of-units@reserved") ? 0.0f : (Float)/*bill_*/line_history.get("number-of-units@reserved");
				final Float number_of_units_charged = null == /*bill_*/line_history.get("number-of-units@charged") ? 0.0f : (Float)/*bill_*/line_history.get("number-of-units@charged");
				final java.util.Map<String, Object> service = hci.j2ee.Services.findServiceByLineId(/*service_*/line_id);
				final java.util.Map<String, Object> /*service*/_line = hci.j2ee.Services.findServiceLineById(/*service_*/line_id);
			  //final String unit_of_measure = (String)/*service*/_line.get("unit-of-measure");
				final Float price_per_unit = (Float)/*service*/_line.get("price-per-unit");
				if (null == total) {
					total = new Float(0.0f);
				}
			  //total += number_of_units_reserved*price_per_unit;
				total += number_of_units_charged*price_per_unit;
			}
		}
	}
	/*bill_*/history.put("total", total);
	final Float sub_total = null == total || null == tax_gst ? null : new Float(total.floatValue()/(1.0f+tax_gst.floatValue()));
	final Float tax_gst_total = null == sub_total || null == tax_gst ? null : new Float(sub_total.floatValue()*tax_gst.floatValue()); %>

<tr>
<td></td>
<td align='right' nowrap>total: </td>
<td><%= null == total ? "" : new java.text.DecimalFormat("#,##0.00").format(total) %></td>
</tr>
<tr>
<td></td>
<td align='right' nowrap>sub-total: </td>
<td><%= null == sub_total ? "" : new java.text.DecimalFormat("#,##0.##").format(sub_total) %></td>
</tr>
<tr>
<td></td>
<td align='right' nowrap>tax@gst-total: </td>
<td><%= null == tax_gst_total ? "" : new java.text.DecimalFormat("#,##0.##").format(tax_gst_total) %></td>
</tr>

<% 		final String font_family = null == request.getParameter("font-family") ? "Lucida Console, monospace" : request.getParameter("font-family");
		final String font_size   = null == request.getParameter("font-size"  ) ? "20px" : request.getParameter("font-size");
		final String font_weight = null == request.getParameter("font-weight") ? "bold" : request.getParameter("font-weight"); %>
<% 		if (true == "payment".equals(mode) && null == /*bill_*/history.get("$payment")) {
			final java.util.Map<String, Object> user = hci.j2ee.Users.users.get("admin");
			final java.util.Map<String, Object> card = null == user ? null : null == user.get("card") ? null : (java.util.Map<String, Object>)user.get("card"); %>
<% 			if (true == Boolean.TRUE.booleanValue()) { %>
<div id="form-container">
<tr>
<td><input id='id#sq:nonce' name='sq:nonce' type='hidden' value=''/></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr valign='top'>
<% 				final String applicationId = "sandbox-sq0idb-89JrHkt1aZ21YOrp_s24Lw"; %>
<!-- https://developer.squareup.com/docs/payment-form/what-it-does -->
<!-- https://developer.squareup.com/docs/payment-form/payment-form-walkthrough -->
<script type="text/javascript" src="https://js.squareupsandbox.com/v2/paymentform"></script><!-- TODO: variable, for prod to be non-sandbox -->
<link rel="stylesheet" type="text/css" href="/hci/css/sqpaymentform.css?<%= ServletTool.getVersion() %>">
<script type="text/javascript">
<!-- https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid -->
function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}
const idempotency_key = uuidv4();
const paymentForm = new SqPaymentForm({
	applicationId: '<%= applicationId %>',
	inputClass: 'sq-input',
	autoBuild: false,
	inputStyles: [{
		fontFamily: '<%= font_family %>',
		fontSize: '<%= font_size %>',
		fontWeight: '<%= font_weight %>',
	  //lineHeight: '24px',
		padding: '1px',
	  //placeholderColor: '#a0a0a0',
	  //backgroundColor: 'transparent',
	}],
	cardNumber: {
		elementId: 'sq-card-number',
	  //placeholder: 'Card Number'
	},
	cvv: {
		elementId: 'sq-cvv',
	  //placeholder: 'CVV'
	},
	expirationDate: {
		elementId: 'sq-expiration-date',
	  //placeholder: 'MM/YY'
	},
	// https://developer.squareup.com/docs/payment-form/cookbook/sqpaymentform-customization
	// "For processing credit cards for a seller in the United States (US), Canada (CA), and the United Kingdom (GB), the postal code (postalCode) is a required payment field. For sellers taking payments in Japan, you can remove the postal code requirement by setting the postalCode configuration field to false"
	// https://developer.squareup.com/docs/payment-form/cookbook/customize-form-styles
	// 
	postalCode: {
		elementId: 'sq-postal-code',
	  //placeholder: 'Postal'
	},
	callbacks: {
		cardNonceResponseReceived: function (errors, nonce, cardData) {
			if (errors) {
				console.error('Encountered errors:');
				errors.forEach(function (error) {
					console.error('  ' + error.message);
				});
				alert('Encountered errors, check browser developer console for more details');
				return;
			}
			var e = document.getElementById('id#sq:nonce');
			if (e !== undefined && e !== null) {
				e.value = nonce;
			}
			var f = document.getElementById('id#form-bill');
			if (f !== undefined && f !== null) {
				f.submit();
			}
		}
	}
});
paymentForm.build();
function onGetCardNonce(event) {
	event.preventDefault();
	paymentForm.requestCardNonce();
}
</script>
<% 			} %>
<% 			{
				final String key = "user:card-number";
				final String label = "card-number";
				final String value = null == card ? null : null == card.get("number") ? null : (String)card.get("number"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=20 maxlength=19 value='<%= null == value ? "" : value %>'/><% } else { %><div id="sq-card-number"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<tr valign='top'>
<% 			{
				final String key = "user:card-expiry";
				final String label = "card-expiry";
				final String value = null == card ? null : null == card.get("expiry") ? null : (String)card.get("expiry"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=6 maxlength=5 value='<%= null == value ? "" : value %>'/><% } else { %><div class="third" id="sq-expiration-date"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<tr valign='top'>
<% 			{
				final String key = "user:card-security";
				final String label = "card-security";
				final String value = null == card ? null : null == card.get("security") ? null : (String)card.get("security"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=6 maxlength=3 value='<%= null == value ? "" : value %>'/><% } else { %><div class="third" id="sq-cvv"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<tr valign='top'>
<% 			{
				final String key = "user:postal";
				final String label = "postal";
				final String value = null == card ? null : null == card.get("postal") ? null : (String)card.get("postal"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=8 maxlength=8 value='<%= null == value ? "" : value %>'/><% } else { %><div class="third" id="sq-postal-code"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<% 		} %>

<% 		if (false == Boolean.TRUE.booleanValue() && true == "create".equals(mode)) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align='right' nowrap>roles: </td>
<td align='left' nowrap></td>
<td></td>
</tr>
<tr>
<td align='right' nowrap>users: </td>
<td align='left' nowrap></td>
<td></td>
</tr>
<tr>
<td align='right' nowrap>groups: </td>
<td align='left' nowrap></td>
<td></td>
</tr>
<% 		} else if (false == Boolean.TRUE.booleanValue()) { // disable until we need or know we don't need, this: %>
<tr>
<td align='right' nowrap><a href="/hci/roles?session-id=<%= session_id %>&service-history-id=<%= history.get("service-history-id").toString() %>">roles</a>: </td>
<td align='left' nowrap>
<% 			if (null != roles) { %>
<% 				for (final String role_id: roles) { %>
<% 					final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= session_id %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 				} %>
<% 			} %>
</td>
<td></td>
</tr>
<tr>
<td align='right' nowrap><a href="/hci/users?session-id=<%= session_id %>&service-history-id=<%= history.get("service-history-id").toString() %>">users</a>: </td>
<td align='left' nowrap>
<% 			if (null != users) { %>
<% 				for (final String user_id: users) { %>
<% 					final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= session_id %>&user-id=<%= user_id %>"><%= (String)user.get("name") %></a><br/>
<% 				} %>
<% 			} %>
</td>
<td></td>
</tr>
<tr>
<td align='right' nowrap><a href="/hci/groups?session-id=<%= session_id %>&service-history-id=<%= history.get("service-history-id").toString() %>">groups</a>: </td>
<td align='left' nowrap>
<% 			if (null != groups) { %>
<% 				for (final String group_id: groups) { %>
<% 					final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= session_id %>&group-id=<%= group_id %>"><%= (String)group.get("group-id") %></a><br/>
<% 				} %>
<% 			} %>
<% 		} // true == "create".equals(mode)) { %>
</td>
<td></td>
</tr>
</table>
<br/>
<% 		if (null != /*bill_*/history && false == "create".equals(mode)) {
			if (false == "payment".equals(mode)) { %>
<a href="/hci/bill?line&history&session-id=<%= session_id %>&id=<%= (Integer)/*bill_*/history.get("$id") %>&mode=create&cancel=bill">create line</a>
|
<% 			} else { %>
<!--a href="/hci/bill?line&history&session-id=<%= session_id %>&id=<%= (Integer)/*bill_*/history.get("$id") %>&mode=cards&cancel=bill">update card/s</a-->
<% 			} %>
<% 		} %>
<% 		if (true == "create".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
<% 		} else if (false == "payment".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 		} else {
			if (null == /*bill_*/history.get("$payment")) {
				if (true == Boolean.TRUE.booleanValue()) { %>
	<button id="sq-creditcard" __class="button-credit-card" style="font-family: <%= font_family %>; font-size: <%= font_size %>; font-weight: <%= font_weight %>" onclick="onGetCardNonce(event)">pay</button>
</div>
<% 				} else { %>
<input id="id#submit:pay" name="__submit" type="submit" value="pay"/>
<% 				}
			} else { %>
				<input id="id#submit:refund" name="__submit" type="submit" value="refund"/>
<% 			}
		} %>
|
<% 		if (true == "create".equals(mode)) {
			if (true == "bill".equals(cancel) && null != bill_id) { %>
<a href="/hci/bill?session-id=<%= session_id %>&id=<%= bill_id %>&cancel=bills">cancel</a>
<% 			} else if (true == "bill-history".equals(cancel) && null != bill_id) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= bill_id %>&cancel=bills">cancel</a>
<% 			} else if (true == "bills-history".equals(cancel) || null == bill_id) { %>
<a href="/hci/bills?history&session-id=<%= session_id %>">cancel</a>
<% 			}
		} else if (true == "bill".equals(cancel)) { %>
<a href="/hci/bill?session-id=<%= session_id %>&id=<%= bill_id %>">cancel</a>
<% 		} else if (true == "bills".equals(cancel)) { %>
<a href="/hci/bills?session-id=<%= session_id %>">cancel</a>
<% 		} else if (true == "bills-history".equals(cancel)) { %>
<a href="/hci/bills?history&session-id=<%= session_id %>">cancel</a>
<% 		} else if (true == "bill-history".equals(cancel)) {
			if (false == "payment".equals(mode)) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= bill_id %>">cancel</a>
<% 			} else { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= (Integer)history.get("$id") %>&cancel=bill-history">cancel</a>
<% 			}
		} %>

<% 	} %>

<% } else { %>
<%@include file='/fragments/users-form.jsp' %>
<% } %>