<!-- fragments/bill-form.jsp -->
<% if (null == request.getAttribute("bill#history")) {
	final String session_id = (String)request.getAttribute("session-id");
	final Object o = request.getAttribute("bill");
  /*final */java.util.Map<String, Object> bill = null;
	if (true == o instanceof Boolean) {
	} else {
	  /*final java.util.Map<String, Object> */bill = (java.util.Map<String, Object>)o;
	}
	final Integer /*bill_*/id = null == bill ? null :(Integer)bill.get("$id");
	final Boolean /*bill_*/history = (Boolean)request.getAttribute("bill@history");
	final String mode = (String)request.getAttribute("mode");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
	final java.util.List<String> roles = null;
	final java.util.List<String> users = null;
	final java.util.List<String> groups = null;
	final java.util.Map<String, Integer> addresses = null;
	final java.util.List<String> address_keys = null;
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create bill:
<% 	} else if (false == "delete".equals(mode)) { %>
Update bill:
<% 	} else { %>
Delete bill:
<% 	} %>
<br/>
<br/>

<table border=0 cellpadding=4 cellspacing=4>
<tr>
<% 	{
		final String key = "bill-id";
		final String value = null == bill ? null : null == bill.get(key) ? null : ((Integer)bill.get(key)).toString(); %>
<td width='44%' align='right' nowrap><%= key %>: </td>
<td width='12%' align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td width='44%' align='left'><input id='id' name='id' type='hidden' value='<%= null == /*bill_*/id ? "" : /*bill_*/id %>'/></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<% 	{
		final String key = "tax-gst";
		final String label = "tax%gst";
		final String value = null == bill ? null : null == bill.get(key) ? "" : ((Float)bill.get(key)).toString(); %>
<td align='right' nowrap><%= label %>: </td>
<td align='center' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value || 0 == value.trim().length() ? "N/A" : new java.text.DecimalFormat("#,##0.##").format(100.0f*new Float(value).floatValue()) %>'/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 	} %>
</tr>
<% 	if (false == Boolean.TRUE.booleanValue()) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align='right' nowrap>roles: </td>
<td align='center' nowrap></td>
<td></td>
</tr>
<tr>
<td align='right' nowrap>users: </td>
<td align='center' nowrap></td>
<td></td>
</tr>
<tr>
<td align='right' nowrap>groups: </td>
<td align='center' nowrap></td>
<td></td>
</tr>
<% 	} else if (false == Boolean.TRUE.booleanValue()) { %>
<tr>
<td align='right' nowrap><a href="/hci/roles?session-id=<%= session_id %>&bill-id=<%= bill.get("bill-id").toString() %>">roles</a>: </td>
<td align='center' nowrap>
<% 		if (null != roles) {
			for (final String role_id: roles) {
				final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= session_id %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 			}
		} %>
</td>
<td></td>
</tr>
<tr>
<td align='right' nowrap><a href="/hci/users?session-id=<%= session_id %>&bill-id=<%= bill.get("bill-id").toString() %>">users</a>: </td>
<td align='center' nowrap>
<% 		if (null != users) {
			for (final String user_id: users) {
				final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= session_id %>&user-id=<%= user_id %>"><%= (String)user.get("name") %></a><br/>
<% 			}
		} %>
</td>
<td></td>
</tr>
<tr>
<td align='right' nowrap><a href="/hci/groups?session-id=<%= session_id %>&bill-id=<%= bill.get("bill-id").toString() %>">groups</a>: </td>
<td align='center' nowrap>
<% 		if (null != groups) {
			for (final String group_id: groups) {
				final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= session_id %>&group-id=<%= group_id %>"><%= (String)group.get("group-id") %></a><br/>
<% 			}
		} %>
</td>
<td></td>
</tr>
<% 		if (null != addresses) {
			for (final String address_key: address_keys) {
if (true == address_key.equals("$id")) continue;
				final Integer address_id = addresses.get(address_key); %>
<tr>
<td align='right' nowrap><a href="/hci/addresses?session-id=<%= session_id %>&bill-id=<%= bill.get("bill-id").toString() %>&<%= address_key %>"><%= address_key %>-address</a>: </td>
<td align='center' nowrap>
<% 				if (null != address_id) {
					final java.util.Map<String, Object> address = (java.util.Map<String, Object>)hci.j2ee.Addresses.addresses.get(address_id);
				  /*final */String detail = (String)address.get("detail");
					final int length = detail.length();
					detail = detail.substring(0, Math.min(length-1, 21));
					if (length > 21-1) {
						detail = detail+".."; // TODO: get this into a ServletTool thingy
					} %>
<a href="/hci/address?session-id=<%= session_id %>&bill-id=<%= bill.get("bill-id").toString() %>&address-id=<%= address_id.toString() %>&<%= address_key %>"><%= address.get("address-id").toString() %>=<%= detail %></a>
<% 				} %>
</td>
<td></td>
</tr>
<% 			}
		}
	} // if (true == mode.equals("create")) %>
</table>
<br/>
<% if (false == "create".equals(mode)) {
	if (null == /*bill_*/history) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= id %>&mode=create&cancel=bill">create history</a>
<% 	} else { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= id %>&cancel=bill">history</a>
<%  } %>
|
<% } %>
<% 	if (true == "create".equals(mode)) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-bill-create")) { %>
<input id='id#submit' name='__submit' type='submit' value='create'/>
<% 	} else { %>
<input id='id#submit' name='__submit' type='submit' value='update'/>
|
<input id='id#submit' name='__submit' type='submit' value='delete'/>
<% 	} %>
|
<a href="/hci/bills?session-id=<%= session_id %>">cancel</a>
<% } else { %>
<%@include file='/fragments/bill-form-history.jsp' %>
<% } %>