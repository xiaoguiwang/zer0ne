<!-- fragments/bill-line-form-history.jsp -->
<!-- bill history of line: e.g. price increase/decrease, promotion, etc. -->
<% if (null == request.getAttribute("for:line#id")) {
	final String session_id = (String)request.getAttribute("session-id");
  /*final */java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
	final Integer bill_id = null == bill ? null : (Integer)bill.get("$id");
  /*final */java.util.Map<String, Object> /*bill_*/history = (java.util.Map<String, Object>)request.getAttribute("bill#history");
	final Integer /*bill_*/history_id = (Integer)/*bill_*/history.get("$id");
  /*final */Integer /*bill_history_line_*/id = null;
	final Object o = request.getAttribute("bill#line#history");
  /*final */java.util.Map<String, Object> /*bill_*/line_history = null;
	if (true == o instanceof Boolean) {
	} else {
	  /*final java.util.Map<String, Object> bill_*/line_history = (java.util.Map<String, Object>)o;
	  /*final Integer bill_history_line_*/id = (Integer)/*bill_*/line_history.get("$id");
	}
	final String mode = (String)request.getAttribute("mode");
	final String qs_mode = null == mode ? "" : "&mode="+mode;
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create bill line history:
<% 	} else if (false == "delete".equals(mode)) { %>
Update bill line history:
<% 	} else { %>
Delete bill line history:
<% 	} %>
<br/>
<br/>
<% 	if (null != /*bill_history_line_*/id) { %><input name='id' type='hidden' value='<%= /*bill_history_line_*/id %>'/><% } %>
<input name='history-id' type='hidden' value='<%= /*bill_*/history_id %>'/>
<input name='bill-id' type='hidden' value='<%= bill_id %>'/>
<input name='line' type='hidden'/>
<input name='history' type='hidden'/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<% 	{
		final String key = "bill-history-id";
		final Integer value = null == /*bill_*/history ? null : (Integer)/*bill_*/history.get(key); %>
<td width='44%' align='right' nowrap><%= key %>:</td>
<td width='12%' align='left' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td width='44%' align='left'></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = "bill-id";
		final String value = null == bill ? null : ((Integer)bill.get("bill-id")).toString(); %>
<td align="right" nowrap><%= key %>: </td>
<td align="left" nowrap><input id='id#bill-id' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = /*"bill:history:*/"for-user-id";
		final String label = "for:user#id"; 
		final String value = null == /*bill_*/history ? null : (String)/*bill_*/history.get(key); // should be label %>
<td align="right" nowrap>for:user#id: </td>
<td align="left" nowrap><%= null == value ? "" : value %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("bill:history:for-user-id") ? "&nbsp;" : results.get("bill:history:for-user-id") %></td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = "service-line-id";
		final String value = null == /*bill_*/line_history ? null : (String)/*bill_*/line_history.get(key);
		if (null == /*bill_*/line_history) { %>
<td align="right" nowrap><a href="/hci/bill?for-line-id&line&history&session-id=<%= session_id %>&bill-id=<%= bill.get("$id") %>&id=<%= (Integer)/*bill_*/history.get("$id") %><%= qs_mode %>&cancel=bill-line-history">service:line#id</a>: </td>
<% 		} else { %>
<td align="right" nowrap><a href="/hci/bill?for-line-id&line&history&session-id=<%= session_id %>&bill-id=<%= bill.get("$id") %>&history-id=<%= (Integer)/*bill_*/history.get("$id") %>&id=<%= (Integer)/*bill_*/line_history.get("$id") %><%= qs_mode %>&cancel=bill-line-history">service:line#id</a>: </td>
<% 		} %>
<td align="left" nowrap><input id='id#service-line-id' name='service-line-id' type='hidden' value='<%= null == value ? "" : value %>'/><%= null == value ? "" : value %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("service-line-id") ? "&nbsp;" : results.get("service-line-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>(reserved) number-of-units: </td>
<% 	final Float number_of_units_reserved = null == /*bill_*/line_history ? null : null == /*bill_*/line_history.get("number-of-units@reserved") ? null : (Float)/*bill_*/line_history.get("number-of-units@reserved"); %>
<td align="left" nowrap><input id="id#bill:line:history:number-of-units@reserved" name="bill:line:history:number-of-units@reserved" type="text" value="<%= null == /*bill_line_history_*/number_of_units_reserved ? "" : /*bill_line_history_*/number_of_units_reserved %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("bill:line:history:number-of-units@reserved") ? "&nbsp;" : results.get("bill:line:history:number-of-units@reserved") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>(consumed) number-of-units: </td>
<% 	final Float number_of_units_consumed = null == /*bill_*/line_history ? null : null == /*bill_*/line_history.get("number-of-units@consumed") ? null : (Float)/*bill_*/line_history.get("number-of-units@consumed"); %>
<td align="left" nowrap><input id="id#bill:line:history:number-of-units@consumed" name="bill:line:history:number-of-units@consumed" type="text" value="<%= null == /*bill_line_history_*/number_of_units_consumed ? "" : /*bill_line_history_*/number_of_units_consumed %>"/></td>
<td></td>
</tr>
<% 	// if (null != results && true == results.containsKey("bill:line:history:number-of-units@consumed")) { %>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("bill:line:history:number-of-units@consumed") ? "&nbsp;" : results.get("bill:line:history:number-of-units@consumed") %></td>
<td></td>
</tr>
<% 	// } %>
<tr>
<td align="right" nowrap>(charged) number-of-units: </td>
<% 	final Float number_of_units_charged = null == /*bill_*/line_history ? null : null == /*bill_*/line_history.get("number-of-units@charged") ? null : (Float)/*bill_*/line_history.get("number-of-units@charged"); %>
<td align="left" nowrap><input id="id#bill:line:history:number-of-units@charged" name="bill:line:history:number-of-units@charged" type="text" value="<%= null == /*bill_line_history_*/number_of_units_charged ? "" : /*bill_line_history_*/number_of_units_charged %>"/></td>
<td></td>
</tr>
<% 	// if (null != results && true == results.containsKey("bill:line:history:number-of-units@charged")) { %>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("bill:line:history:number-of-units@charged") ? "&nbsp;" : results.get("bill:line:history:number-of-units@charged") %></td>
<td></td>
</tr>
<% 	// } %>
</table>
<br/>
<% 	if (true == "create".equals(mode) || null == /*bill_history_line_*/id) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
<% 	} else { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
|
<input id="id#submit" name="__submit" type="submit" value="charge"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	} %>
|
<% 	if (true == "bill-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= bill_id %>">cancel</a>
<% 	} else if (true == "bill-lines-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bill?lines&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= (Integer)/*bill_*/history.get("$id") %>&cancel=bill-lines-history">cancel</a>
<% 	} else if (true == "bill-line-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bill?line&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&history-id=<%= (Integer)/*bill_*/history.get("$id") %>&id=<%= (Integer)/*bill_*/line_history.get("$id") %>&cancel=bill-lines-history">cancel</a>
<% 	} else { %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 	} %>

<% } else { %>
<%@include file='/fragments/service-lines-form.jsp' %>
<% } %>