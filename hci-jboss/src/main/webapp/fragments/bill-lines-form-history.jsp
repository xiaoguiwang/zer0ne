<!-- fragments/bill-lines-form-history.jsp -->
<!-- bill history of lines: e.g. price increase/decrease, promotion, etc. -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)request.getAttribute("bill");
	final Integer bill_id = null == bill ? null : (Integer)bill.get("$id");
	final java.util.Map<String, Object> /*bill_*/history = (java.util.Map<String, Object>)request.getAttribute("bill#history");
	final java.util.List<java.util.Map<String, Object>> /*bill_*/lines_history = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("bill#lines#history");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else { %>
Bill lines history:
<% 	} %>
<br/>
<br/>
<input name='bill-id' type='hidden' value='<%= bill_id %>'/>
<input name='lines' type='hidden'/>
<input name='history' type='hidden'/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>for-user#id</td>
<td>service-line-id</td>
<td>units:</td>
<td>reserved</td>
<td>consumed</td>
<td>charged</td>
<td>line#id</td>
</tr>
<% 	if (null != /*bill_*/lines_history && 0 < /*bill_*/lines_history.size()) {
		for (final java.util.Map<String, Object> /*bill_*/line_history: /*bill_*/lines_history) { %>
<tr>
<% 			final Integer id = (Integer)/*bill_*/line_history.get("$id"); %>
<td><%= id %><input id="id#bill-line-history-ids" name="bill-line-history-ids" type="hidden" value="<%= id %>"/></td>
<% 			final Boolean checked = null == request.getAttribute("bill-line-history$"+id+"#checked") ? false : (Boolean)request.getAttribute("bill-line-history$"+id+"#checked");
			final String checked_bill_line_history = false == checked ? "" : " checked"; %>
<td><input id="id#bill-line-history$<%= id %>#checked" name="bill-line-history$<%= id %>#checked" type="checkbox"<%= checked_bill_line_history %>/></td>
<% 			final String for_user_id = null == /*bill_*/history ? "" : (String)/*bill_*/history.get("for-user-id"); %>
<td><%= for_user_id %></td>
<td><%= null == /*bill_*/line_history ? "" : (String)/*bill_*/line_history.get("service-line-id") %></td>
<td></td>
<% 			final Float number_of_units_reserved = null == /*bill_*/line_history.get("number-of-units@reserved") ? null : (Float)/*bill_*/line_history.get("number-of-units@reserved"); %>
<td><%= null == number_of_units_reserved ? "" : new java.text.DecimalFormat("#,##0.#").format(number_of_units_reserved) %></td>
<% 			final Float number_of_units_consumed = null == /*bill_*/line_history.get("number-of-units@consumed") ? null : (Float)/*bill_*/line_history.get("number-of-units@consumed"); %>
<td><%= null == number_of_units_consumed ? "" : new java.text.DecimalFormat("#,##0.#").format(number_of_units_consumed) %></td>
<% 			final Float number_of_units_charged = null == /*bill_*/line_history.get("number-of-units@charged") ? null : (Float)/*bill_*/line_history.get("number-of-units@charged"); %>
<td><%= null == number_of_units_charged ? "" : new java.text.DecimalFormat("#,##0.#").format(number_of_units_charged) %></td>
<td><a href="/hci/bill?line&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&history-id=<%= (Integer)/*bill_*/history.get("$id") %>&id=<%= id %>&cancel=bill-lines-history"><%= id %></a></td>
</tr>
<%
	  //final java.util.Map<String, Object> service = hci.j2ee.Services.services.get(service_id);
	  //final String unit_of_measure = (String)service.get("unit-of-measure");
	  //final Float price_per_unit = (Float)service.get("price-per-unit");
%>
<% 		}
	} %>
</table>
<br/>

<a href="/hci/bill?line&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= /*bill_*/history.get("$id") %>&mode=create&cancel=bill-lines-history">create</a>
|
<% 	if (null != /*bill_*/lines_history && 0+1 < /*bill_*/lines_history.size()) { %>
<input id="id#submit" name="__submit" type="submit" value="charge"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
|
<% 	} %>
<% 	if (true == "bill-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= bill_id %>&cancel=bill-history">cancel</a>
<% 	} else if (true == "bill-line-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bill?line&history&session-id=<%= session_id %>&id=<%= bill_id %>&cancel=bills-history">cancel</a>
<% 	} else if (true == "bills-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bills?history&session-id=<%= session_id %>">cancel</a>
<% 	} else if (true == "services-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/bills?history&session-id=<%= session_id %>">cancel</a>
<% 	} %>
<% } %>