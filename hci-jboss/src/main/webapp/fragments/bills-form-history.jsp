<!-- fragments/bills-form-history.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<%
final String session_id = (String)request.getAttribute("session-id");
final String __role_id = (String)request.getAttribute("role-id");
final String qs_role_id = null == __role_id ? "" : "&role-id="+__role_id;
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+__user_id;
final String __group_id = (String)request.getAttribute("group-id");
final String qs_group_id = null == __group_id ? "" : "&group-id="+__group_id;
final Integer __bill_id = (Integer)request.getAttribute("bill-id");
final String __bill_key = null != request.getAttribute("from") ? "from" : null != request.getAttribute("to") ? "to" : null; 
final String qs_bill = null == __bill_id ? "" : "&bill-id="+__bill_id.toString()+"&"+__bill_key;
final String __history = (String)request.getAttribute("history");
final String qs_history = null == __history ? "" : "&history";
final String mode = (String)request.getAttribute("mode");
final java.util.List<java.util.Map<String, Object>> /*bills_*/history = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("bills#history");
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != __role_id) { %>
Update role bills:
<% } else if (null != __user_id) { %>
Update user bills:
<% } else if (null != __group_id) { %>
Update group bills:
<% } else if (null != __bill_id) { %>
Update bill#<%= __bill_id %>:
<% } else { %>
Bills history:
<% } %>
<br/>
<br/>
<form id="id#form-bills" name="form-bills" method="POST" action="/hci/bills?history&session-id=<%= session_id %><%= qs_role_id %><%= qs_user_id %><%= qs_group_id %><%= qs_bill %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>history#id</td>
<td>bill#id</td>
<td>for-user#id</td>
<td>total</td>
<td>sub-total</td>
<td>tax@gst-total</td>
<td>state</td>
<td>payment</td>
<td></td>
</tr>
<% /*final */int open = 0; %>
<% /*final */int closed = 0; %>
<% if (null != history && 0+0 < history.size()) { %>
<% 	for (final java.util.Map<String, Object> /*bills*/_history: history) {
		final Integer bill_history_id = (Integer)/*bills*/_history.get("bill-history-id");
		final Integer id = (Integer)/*bills*/_history.get("$id");
		final Boolean checked = null == request.getAttribute("bill-history$"+id+"#checked") ? false : (Boolean)request.getAttribute("bill-history$"+id+"#checked");
		final Integer bill_id = (Integer)/*bills*/_history.get("bill-id");
		final java.util.Map<String, Object> bill = (java.util.Map<String, Object>)hci.j2ee.Bills.bills.get(bill_id);
		final String for_user_id = null == /*bills*/_history.get("for-user-id") ? "" : (String)/*bills*/_history.get("for-user-id"); %>
<tr>
<td><%= id %><input id="id#bill-history-ids" name="bill-history-ids" type="hidden" value="<%= id %>"/></td>
<% 		final String checked_bill_history = false == checked ? "" : " checked"; %>
<td><input id="id#bill-history$<%= id %>#checked" name="bill-history$<%= id %>#checked" type="checkbox"<%= checked_bill_history %>/></td>
<td><a href="/hci/bill?history&session-id=<%= session_id %>&bill-id=<%= (Integer)bill.get("$id") %>&id=<%= id %>&cancel=bills-history"><%= bill_history_id.toString() %></a></td>
<td><%= bill_id %></td>
<td><%= for_user_id %></td>
<% 	/*final */float total = 0.0f;
		final float	tax_gst = (Float)bill.get("tax-gst");
		final java.util.List<java.util.Map<String, Object>> /*bill_*/lines_history = (java.util.List<java.util.Map<String, Object>>)/*bills*/_history.get("@lines");
		if (null != /*bill_*/lines_history) {
			for (final java.util.Map<String, Object> /*bill_*/line: /*bill_*/lines_history) {
				final String /*service_*/line_id = null == line.get("service-line-id") ? "" : (String)/*bill_*/line.get("service-line-id");
				final Float number_of_units_reserved = null == line.get("number-of-units@reserved") ? 0.0f : (Float)/*bill_*/line.get("number-of-units@reserved");
				final Float number_of_units_charged = null == line.get("number-of-units@charged") ? 0.0f : (Float)/*bill_*/line.get("number-of-units@charged");
				final java.util.Map<String, Object> service = hci.j2ee.Services.findServiceByLineId(/*service_*/line_id);
				final java.util.Map<String, Object> /*service*/_line = hci.j2ee.Services.findServiceLineById(/*service_*/line_id);
			  //final String unit_of_measure = (String)/*service*/_line.get("unit-of-measure");
				final Float price_per_unit = (Float)/*service*/_line.get("price-per-unit");
			  //total += number_of_units_reserved*price_per_unit;
				total += number_of_units_charged*price_per_unit;
			}
		}
		/*bill*/_history.put("total", total);
		float sub_total = total/(1.0f+tax_gst);
		float tax_gst_total = sub_total*tax_gst; %>
<td><%= new java.text.DecimalFormat("#,##0.00").format(new Float(total)) %></td>
<td><%= new java.text.DecimalFormat("#,##0.##").format(new Float(sub_total)) %></td>
<td><%= new java.text.DecimalFormat("#,##0.##").format(new Float(tax_gst_total)) %></td>
<% 		final String state = null == /*bills*/_history.get("$state") ? "open" : (String)/*bills*/_history.get("$state"); %>
<% 		if (true == "open".equals(state)) open++; %>
<% 		if (true == "closed".equals(state)) closed++; %>
<td><%= state %></td>
<% 		final String payment = null == /*bills*/_history.get("$payment") ? null : (String)/*bills*/_history.get("$payment"); %>
<td><%= null == payment ? "" : payment %></td>
<% 		final java.util.List<java.util.Map<String, Object>> /*bill*/_lines_history = (java.util.List<java.util.Map<String, Object>>)/*bill*/_history.get("@lines"); %>
<td><% if (null != /*bill*/_lines_history && 0 < /*bill*/_lines_history.size()) { %><a href="/hci/bill?lines&history&session-id=<%= session_id %>&id=<%= id %>&cancel=bills-history">lines</a><% } %></td>
<% 	} %>
</tr>
</table>
<br/>
<a href="/hci/bill?history&session-id=<%= session_id %>&mode=create&cancel=bills-history">create</a>
|
<% 	if (null != __role_id || null != __user_id || null != __group_id || null != __bill_id || null != __bill_id) { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
<% 	} else { %>
<% 		if (0 < closed) { %>
<input id="id#submit" name="__submit" type="submit" value="re-open"/>
|
<% 		} %>
<% 		if (0 < open) { %>
<input id="id#submit" name="__submit" type="submit" value="close"/>
|
<% 		} %>
<input id="id#submit" name="__submit" type="submit" value="delete"/>
|
<% 	} %>
<% } %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>