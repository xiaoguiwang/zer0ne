<!-- fragments/bills-form.jsp -->
<% if (null == request.getAttribute("bills#history")) {
	final String session_id = (String)request.getAttribute("session-id");
	final String __role_id = (String)request.getAttribute("role-id");
	final String qs_role_id = null == __role_id ? "" : "&role-id="+(String)request.getAttribute("role-id");
	final String __user_id = (String)request.getAttribute("user-id");
	final String qs_user_id = null == __user_id ? "" : "&user-id="+(String)request.getAttribute("user-id");
	final String __group_id = (String)request.getAttribute("group-id");
	final String qs_group_id = null == __group_id ? "" : "&group-id="+(String)request.getAttribute("group-id");
	final java.util.List<java.util.Map<String, Object>> bills = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("bills");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (null != __role_id) { %>
Update role bills:
<% 	} else if (null != __user_id) { %>
Update user bills:
<% 	} else if (null != __group_id) { %>
Update group bills:
<% 	} else { %>
Bills:
<% 	} %>
<br/>
<br/>
<form id="id#form-bills" name="form-bills" method="POST" action="/hci/bills?session-id=<%= (String)request.getAttribute("session-id") %><%= qs_role_id %><%= qs_user_id %><%= qs_group_id %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>bill-id</td>
<td>tax%gst</td>
<td></td>
</tr>
<% 	for (final java.util.Map<String, Object> bill: bills) {
		final Integer bill_id = (Integer)bill.get("bill-id");
		final Integer id = (Integer)bill.get("$id");
		final Boolean checked = null == request.getAttribute("bill$"+id+"#checked") ? false : (Boolean)request.getAttribute("bill$"+id+"#checked");
		final Float tax_gst = null == bill.get("tax-gst") ? 0.0f : (Float)bill.get("tax-gst");
%>
<tr>
<td><%= id %><input id="id#bill-ids" name="bill-ids" type="hidden" value="<%= id %>"/></td>
<% 		final String checked_bill = false == checked ? "" : " checked"; %>
<td><input id="id#bill$<%= id %>#checked" name="bill$<%= id %>#checked" type="checkbox"<%= checked_bill %>/></td>
<td><a href="/hci/bill?session-id=<%= (String)request.getAttribute("session-id") %>&id=<%= id %>"><%= bill_id %></a></td>
<td><%= new java.text.DecimalFormat("#,##0.##").format(100.0f*tax_gst)+"%" %></td>
<% 		final java.util.List<java.util.Map<String, Object>> /*bill_*/history_list = hci.j2ee.Bills.getHistoryForBillId(id); %>
<td><% if (null != /*bill_*/history_list && 0 < /*bill_*/history_list.size()) { %><a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= id %>&cancel=bills">history</a><% } %></td>
</tr>
<% 	} %>
</table>
<br/>
<a href="/hci/bill?session-id=<%= session_id %>&mode=create&cancel=bills">create</a>
|
<% 	if (null != __role_id || null != __user_id || null != __group_id) { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
<% 	} else { %>
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	} %>
|
<a href="/hci/index?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>
<% } else { %>
<%@include file='/fragments/bills-form-history.jsp' %>
<% } %>