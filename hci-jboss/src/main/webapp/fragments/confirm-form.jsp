<!-- fragments/confirm-form.jsp -->
<%@ page import="java.util.Map" %>
<%
final String user_id = null == request.getAttribute("user-id") ? "" : (String)request.getAttribute("user-id");
final String password = null == request.getAttribute("password") ? "" : (String)request.getAttribute("password");
final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<form id="id#form" name="form" method="GET" action="/hci/confirm?html-form">
<table>
<tr>
<td>user-id</td>
<td><input id="id#user" name="user-id" type="text" value="<%= user_id %>"/></td>
</tr>
<tr>
<td></td>
<td><%= null == results || false == results.containsKey("user-id") ? "&nbsp;" : results.get("user-id") %></td>
</tr>
<!--
<tr>
<td>password</td>
<td><input id="id#password" name="password" type="password" value="<%= password %>"/></td>
</tr>
-->
<tr>
<td></td>
<td><%= null == results || false == results.containsKey("password") ? "&nbsp;" : results.get("password") %></td>
</tr>
<tr>
<td><input name="html-form" type="hidden" value=""/></td>
<td><input id="id#submit" name="submit" type="submit" value="confirm"/>
|
<a href="/hci/confirm">cancel</a>
</td>
</tr>
</table>
</form>