<!-- fragments/email-form-history.jsp -->
<%
final String session_id = (String)request.getAttribute("session-id");
final java.util.Map<String, Object> history = (java.util.Map<String, Object>)request.getAttribute("email#history");
final Integer email_history_id, email_id;
final String from_user_id, to_user_id;
/*final */String message;
final java.util.Set<String> roles, users, groups;
if (null == history) {
	email_history_id = null;
	from_user_id = "";
	email_id = null;
	message = "";
	to_user_id = "";
	roles = null;
	users = null;
	groups = null;
} else {
	email_history_id = null == history.get("email-history-id") ? null : (Integer)history.get("email-history-id");
	from_user_id = null == history.get("from-user-id") ? "" : (String)history.get("from-user-id");
	email_id = (Integer)history.get("email-id");
	final java.util.Map<String, Object> template = hci.j2ee.Emails.emails.get(email_id);
	message = null == template.get("message") ? "" : (String)template.get("message");
	if (0 < message.trim().length()) {
		final java.util.Map<String, Object> properties = (java.util.Map<String, Object>)history.get("properties");
		final dro.util.Properties p = new dro.util.Properties();
		for (final String key: properties.keySet()) {
			p.put(key, properties.get(key)); // BEWARE: only primitives please! (and flat structure)
		}
		message = dro.lang.String.resolve(message, p);
	}
	to_user_id = null == history.get("to-user-id") ? "" : (String)history.get("to-user-id");
	roles = (java.util.Set<String>)history.get("roles");
	users = (java.util.Set<String>)history.get("users");
	groups = (java.util.Set<String>)history.get("groups");
}
final String mode = null == request.getAttribute("email-mode") ? "" : (String)request.getAttribute("email-mode");
final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>email-history-id:</td>
<td width="12%" align="left" nowrap><input id="id#email-history" name="email-history-id" type="text" value="<%= null == email_history_id ? "" : email_history_id.toString() %>" disabled/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("email-history-id") ? "&nbsp;" : results.get("email-history-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>from-user-id: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#from-user-id" name="from-user-id" type="text" value="<%= from_user_id %>"/><% } else { %><%= from_user_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("from-user-id") ? "&nbsp;" : results.get("from-user-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>email-id: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#email-id" name="email-id" type="text" value="<%= null == email_id ? "" : email_id.toString() %>"/><% } else { %><%= email_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("email-id") ? "&nbsp;" : results.get("email-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>message: </td>
<td align="left" nowrap><textarea id="id#message" __name="message" cols="80" rows="16" disabled><%= message %></textarea>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("message") ? "&nbsp;" : results.get("message") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>to-user-id: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#to-user-id" name="to-user-id" type="text" value="<%= to_user_id %>"/><% } else { %><%= to_user_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("to-user-id") ? "&nbsp;" : results.get("to-user-id") %></td>
<td></td>
</tr>
<% if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align="right" nowrap>roles: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>users: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>groups: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<% } else if (false == Boolean.TRUE.booleanValue()) { // disable until we need or know we don't need, this: %>
<tr>
<td align="right" nowrap><a href="/hci/roles?session-id=<%= session_id %>&email-history-id=<%= history.get("email-history-id").toString() %>">roles</a>: </td>
<td align="left" nowrap>
<% if (null != roles) { %>
<% 	for (final String role_id: roles) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/users?session-id=<%= session_id %>&email-history-id=<%= history.get("email-history-id").toString() %>">users</a>: </td>
<td align="left" nowrap>
<% if (null != users) { %>
<% 	for (final String user_id: users) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= session_id %>&user-id=<%= user_id %>"><%= (String)user.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/groups?session-id=<%= session_id %>&email-history-id=<%= history.get("email-history-id").toString() %>">groups</a>: </td>
<td align="left" nowrap>
<% if (null != groups) { %>
<% 	for (final String group_id: groups) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= session_id %>&group-id=<%= group_id %>"><%= (String)group.get("group-id") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
<% } // if (true == mode.equals("create")) %>
</tr>
</table>
<br/>
<% if (null == history) { %>
<% 	if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-email-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 	} %>
<% } else if (false == mode.equals("send")) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="send"/>
<% } %>
|
<a href="/hci/emails?session-id=<%= session_id %>">cancel</a>