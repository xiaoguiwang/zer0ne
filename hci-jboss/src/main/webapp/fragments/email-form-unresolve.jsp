<!-- fragments/email-form-unresolve.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<%
final String session_id = (String)request.getAttribute("session-id");
final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
final java.util.Map<String, String[]> unresolve = (java.util.Map<String, String[]>)__session.get("email:unresolve");
/*final */java.util.Set<String> set = new java.util.LinkedHashSet<>();
for (final String user_id: unresolve.keySet()) {
	final String[] __unresolve = unresolve.get(user_id);
	for (final String key: __unresolve) {
		set.add(key);
	}
}
/*final */int colspan = set.size();
%>
<table border=0 cellpadding=0 cellspacing=14>
<tr>
<td __width="44%" align="right" nowrap></td>
<td colspan="<%= colspan %>" width="12%" align="left" nowrap></td>
<td width="44%"></td>
</tr>
<tr>
<td></td>
<% for (final String key: set) { %>
<td align="center"><div><span class="rotate"><%= "${"+key+"}" %></span></div></td>
<% } %>
<td></td>
</tr>
<tr>
<td align="right">user#id:</td>
<td></td>
<td></td>
</tr>
<% for (final String user_id: unresolve.keySet()) {
/**/if (true == user_id.equals("")) continue;
	final String[] __unresolve = unresolve.get(user_id);
  /*final */java.util.Set<String> __set = new java.util.HashSet<>();
	for (final String key: __unresolve) {
		__set.add(key);
	} %>
<tr>
<td align="right"><%= user_id %></td>
<% 	for (final String key: set) { %>
<td align="center"><%= false == __set.contains(key) ? "Y" : "X" %></td>
<% 	} %>
<td></td>
</tr>
<% } %>
</table>