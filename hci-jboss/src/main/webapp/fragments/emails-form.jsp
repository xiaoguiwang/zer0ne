<!-- fragments/emails-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% if (null == request.getAttribute("emails#history")) {
final String session_id = (String)request.getAttribute("session-id");
final java.util.List<java.util.Map<String, Object>> emails = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("emails");
final String __role_id = (String)request.getAttribute("role-id");
final String qs_role_id = null == __role_id ? "" : "&role-id="+__role_id;
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+__user_id;
final String __group_id = (String)request.getAttribute("group-id");
final String qs_group_id = null == __group_id ? "" : "&group-id="+__group_id;
final Integer __bill_id = (Integer)request.getAttribute("bill-id");
final String __bill_key = null != request.getAttribute("from") ? "from" : null != request.getAttribute("to") ? "to" : null; 
final String qs_bill = null == __bill_id ? "" : "&bill-id="+__bill_id.toString()+"&"+__bill_key;
final String __email_id = (String)request.getAttribute("email-id");
final String qs_email_id = null == __email_id ? "" : "&email-id="+__email_id;
final String mode = null == request.getAttribute("email-mode") ? "" : (String)request.getAttribute("email-mode");
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != __role_id) { %>
Update role e-mails:
<% } else if (null != __user_id) { %>
Update user e-mails:
<% } else if (null != __group_id) { %>
Update group e-mails:
<% } else if (null != __email_id) { %>
Update email#<%= __email_id %>:
<% } else { %>
Update e-mails:
<% } %>
<br/>
<br/>
<form id="id#form-emails" name="form-emails" method="POST" action="/hci/emails?session-id=<%= session_id %><%= qs_role_id %><%= qs_user_id %><%= qs_group_id %><%= qs_bill %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>email-id</td>
<% if (true == mode.equals("send")) { %><td>from-user-id</td><% } %>
<td>message</td>
<% if (true == mode.equals("send")) { %><td>to-user-id</td><% } %>
</tr>
<% for (final java.util.Map<String, Object> email: emails) {
	final Integer email_id = (Integer)email.get("email-id");
	final Integer id = (Integer)email.get("$id");
	final Boolean checked = null == request.getAttribute("email$"+id+"#checked") ? false : (Boolean)request.getAttribute("email$"+id+"#checked");
	final String from_user_id = null == email.get("from-user-id") ? "" : (String)email.get("from-user-id");
  /*final */String subject = null == email.get("subject") ? "" : (String)email.get("subject");
	{
		final int length = subject.length();
		subject = subject.substring(0, Math.min(length, 61));
		if (length > 61-1) {
			subject = subject+".."; // TODO: get this into a ServletTool thingy
		}
	}
  /*final */String message = null == email.get("message") ? "" : ((String)email.get("message")).replaceAll("\r\n", ", ").replaceAll(",,", ",");
	{
		final int length = message.length();
		message = message.substring(0, Math.min(length, 81));
		message = message.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		if (length > 81-1) {
			message = message+".."; // TODO: get this into a ServletTool thingy
		}
	}
	final String to_user_id = null == email.get("to-user-id") ? "" : (String)email.get("to-user-id");
%>
<tr>
<td><%= id %><input id="id#email-ids" name="email-ids" type="hidden" value="<%= id %>"/></td>
<% final String checked_email = false == checked ? "" : " checked"; %>
<td><input id="id#email$<%= id %>#checked" name="email$<%= id %>#checked" type="checkbox"<%= checked_email %>/></td>
<td><a href="/hci/email?session-id=<%= session_id %>&email-id=<%= email_id %>"><%= email_id %></a></td>
<% if (true == mode.equals("create") && false == mode.equals("send")) { %><td><%= from_user_id %></td><% } %>
<td><%= message %></td>
<% if (true == mode.equals("create") && false == mode.equals("send")) { %><td><%= to_user_id %></td><% } %>
</tr>
<% } %>
</table>
<br/>
<% if (null != __role_id || null != __user_id || null != __group_id || null != __bill_id || null != __email_id) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% } %>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
</form>
<% } else { %>
<%@include file='/fragments/emails-form-history.jsp' %>
<% } %>