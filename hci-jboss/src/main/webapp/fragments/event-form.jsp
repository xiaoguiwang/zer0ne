<!-- fragments/event-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, Object> signed_in_user = (java.util.Map<String, Object>)request.getAttribute("signed-in-user");
	final java.util.Map<String, Object> event = (java.util.Map<String, Object>)request.getAttribute("event");
	final String event_name,uuid_as_char;
	final Long creation_time,process_time;
	final String type;
	if (null == event) {
		event_name = null;
		creation_time = null;
		uuid_as_char = null;
		process_time = null;
		type = null;
	} else {
		event_name = null == event.get("event-name") ? null : (String)event.get("event-name");
		creation_time = null == event.get("creation-time") ? null : (Long)event.get("creation-time");
		uuid_as_char = null == event.get("uuid-as-char") ? null : (String)event.get("uuid-as-char");
		process_time = null == event.get("process-time") ? null : (Long)event.get("process-time");
		type = null == event.get("type") ? null : (String)event.get("type");
	}
	final Integer __id = (Integer)event.get("$id");
	final String event_id = (String)event.get("uuid-as-char");
	final String state = (String)event.get("$state");
  /*final */String match = null;
	if (true == event.get("event-name").equals("password-reset")) {
		if (true == event.containsKey("$matched-message-id:key")) {
		  /*final String */match = (String)event.get("$matched-message-id:key");
		}
	}
	final String ack = (String)event.get("$ack");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
Event:
<br/>
<br/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>$id</td>
<td width="12%" align="center" nowrap><input type="text" value="<%= null == __id ? "" : __id.toString() %>" disabled/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td align="right" nowrap>(uuid-as-char) event#id: </td>
<td align="center" nowrap><input id="id#event-id" name="event-id" type="text" value="<%= null == event_id ? "" : event_id %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>event-name:</td>
<td align="center" nowrap><input id="id#event-name" name="event-name" type="text" value="<%= null == event_name ? "" : event_name %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>creation-time: </td>
<td align="center" nowrap><input id="id#creation-time" name="creation-time" type="text" value="<%= creation_time.toString() %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>process-time: </td>
<td align="center" nowrap><input id="id#process-time" name="process-time" type="text" value="<%= process_time.toString() %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>type: </td>
<td align="center" nowrap><input id="id#type" name="type" type="text" value="<%= null == type ? "" : type %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>$state: </td>
<td align="center" nowrap><input id="id#state" name="state" type="text" value="<%= null == state ? "" : state %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>$match: </td>
<td align="center" nowrap><input id="id#match" name="match" type="text" value="<%= null == match ? "" : match %>"/></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>$ack: </td>
<td align="center" nowrap><input id="id#ack" name="ack" type="text" value="<%= null == ack ? "" : ack %>"/></td>
<td></td>
</tr>
</table>
<br/>
<a href="/hci/events?session-id=<%= session_id %>">cancel</a>
<% } %>