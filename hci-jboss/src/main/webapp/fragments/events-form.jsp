<!-- fragments/events-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, java.util.Map<String, Object>> events = (java.util.Map<String, java.util.Map<String, Object>>)request.getAttribute("events");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
Events:
<br/>
<br/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>event#id</td><!--Note: event#id is uuid-as-char -->
<td>event-name</td>
<td>creation-time</td>
<td>process-time</td>
<td>type</td>
<td>$state</td>
<td>$match</td>
<td>$ack</td>
</tr>
<% 	for (final String event_id: events.keySet()) { // Note: event-id is uuid-as-char
//if (true == event_id.equals("$id")) continue; // Not needed - events are not underpinned by dao.util.HashMap
		final java.util.Map<String, Object> event = events.get(event_id);
		final Integer __id = (Integer)event.get("$id"); %>
<tr>
<td><%= __id %><input id="id#event-ids" name="event-ids" type="hidden" value="<%= __id %>"/></td>
<% 		final Boolean checked = null == request.getAttribute("event$"+__id+"#checked") ? false : (Boolean)request.getAttribute("event$"+__id+"#checked");
		final String checked_event = false == checked ? "" : " checked"; %>
<td><input id="id#event$<%= __id %>#checked" name="event$<%= __id %>#checked" type="checkbox"<%= checked_event %>/></td>
<td><a href="/hci/event?session-id=<%= session_id %>&event-id=<%= event_id %>"><%= event_id %></a></td> <!--Note: event-id is uuid-as-char --> 
<% 		final String name = (String)event.get("event-name"); %>
<td><%= name %></td>
<% 		final String creation_time = ((Long)event.get("creation-time")).toString(); %>
<td><%= creation_time %></td>
<% 		final String process_time = ((Long)event.get("process-time")).toString(); %>
<td><%= process_time  %></td>
<% 		final String type = (String)event.get("$type"); %>
<td><%= type %></td>
<td><%= null == event.get("$state") ? "" : (String)event.get("$state") %></td>
<%
/*final */String match = null;
if (true == event.get("event-name").equals("password-reset")) {
	if (true == event.containsKey("$matched-message-id:key")) {
	  /*final String */match = (String)event.get("$matched-message-id:key");
	}
}
%>
<td><%= null == match ? "" : match %></td>
<td><%= null == event.get("$ack") ? "" : (String)event.get("$ack") %></td>
<% 		if (null != event.get("email-history-id")) { %>
<td><a href="/hci/email?history&session-id=<%= session_id %>&email-id=<%= event.get("email-history-id").toString() %>">view</a></td>
<% 		} else if (null != event.get("text-history-id")) { %>
<td><a href="/hci/text?history&session-id=<%= session_id %>&text-id=<%= event.get("text-history-id").toString() %>">view</a></td>
<% 		} else { %>
<td></td>
<% 		} %>
</tr>
<% 	} %>
</table>
<br/>
<input id="id#submit" name="__submit" type="submit" value="acknowledge"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% } %>