<!-- fragments/footer.jsp -->
<%@ page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = ServletTool.getSessionById(session_id);
	final String sign_in_last_successful = null == __session ? null : (String)__session.get("sign-in-last-successful");
	final java.util.Map<String, Object> user = null == __session ? null : (java.util.Map<String, Object>)__session.get("user");
%>
</td>
</tr>
<tr height="1%">

<td align="center">
<table>
<tr>
<td colspan=3 align="center" width="100%" nowrap>
<%
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	final String url = request.getRequestURL().toString();
	if (false == redirecting) { %>
<font size="-1">session-id: <%= null == session_id ? "{null}" : session_id %></font>
</td>
<td width="1%"></td>
</tr>
<tr>
<td colspan=3 align="center" width="100%" nowrap>
<font size="-1">
sign-id:
<%= null == __session ? "{null}" : null == __session.get("sign-id") ? "{null}" : 0 == ((String)__session.get("sign-id")).trim().length() ? "{blank}" : (String)__session.get("sign-id") %>
<%= null == sign_in_last_successful ? "" : "| last signed-in: "+sign_in_last_successful %>
</font>
</td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" align="center" nowrap>
<font size="-1">
user.id: <%= null == user ? "{null}" : null == (String)user.get("user-id") ? "{null}" : 0 == ((String)user.get("user-id")).trim().length() ? "{blank}" : (String)user.get("user-id") %>
</font>
<% 	} else { %>
<font size="-1">Redirecting..</font>
<br/>
<font size="-1">&nbsp;</font>
<br/>
<font size="-1">&nbsp;</font>
<% 	} %>
</td>
<td width="20%" align="right" nowrap><font size="-2"><span id="id#orientation"></span></font>
</td>
</tr>
</table>
</td>

</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</body>
</html>
<% } %>