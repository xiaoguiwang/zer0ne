<!-- fragments/group-form.jsp -->
<%
final java.util.Map<String, Object> signed_in_user = (java.util.Map<String, Object>)request.getAttribute("signed-in-user");
final java.util.Map<String, Object> group = (java.util.Map<String, Object>)request.getAttribute("group");
final String group_id, name, description;
final boolean organisation;
final String registration;
final java.util.Set<String> permissions, roles, users;
if (null == group) {
	group_id = "";
	name = "";
	description = "";
	organisation = false;
	registration = "";
	permissions = null;
	roles = null;
	users = null;
} else {
	group_id = null == group.get("group-id") ? "" : (String)group.get("group-id");
	name = null == group.get("name") ? "" : (String)group.get("name");
	description = null == group.get("description") ? "" : (String)group.get("description");
	organisation = null == group.get("organisation") ? false : (Boolean)group.get("organisation");
	registration = false == organisation ? "" : null == group.get("registration") ? "" : (String)group.get("registration");
	permissions = (java.util.Set<String>)group.get("permissions");
	roles = (java.util.Set<String>)group.get("roles");
	users = (java.util.Set<String>)group.get("users");
}
final String mode = null == request.getAttribute("group-mode") ? "" : (String)request.getAttribute("group-mode");
final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<form id="id#form-group" name="form-group" method="POST" action="/hci/group?session-id=<%= (String)request.getAttribute("session-id") %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>group-id:</td>
<td width="12%" align="center" nowrap><input id="id#group" name="group-id" type="text" value="<%= group_id %>"/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("group-id") ? "&nbsp;" : results.get("group-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>name: </td>
<td align="center" nowrap><input id="id#name" name="name" type="text" value="<%= name %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("name") ? "&nbsp;" : results.get("name") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>description: </td>
<td align="center" nowrap><input id="id#description" name="description" type="text" value="<%= description %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("description") ? "&nbsp;" : results.get("description") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>organisation: </td>
<td align="center" nowrap><input id="id#organisation" name="organisation" type="checkbox"<%= true == organisation ? " checked" : "" %>/></td>
<td></td>
</tr>
<% if (true == organisation) { %>
<tr>
<td align="right" nowrap>registration: </td>
<td align="center" nowrap><input id="id#registration" name="registration" type="text" value="<%= registration %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("registration") ? "&nbsp;" : results.get("registration") %></td>
<td></td>
</tr>
<% } %>
<% if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align="right" nowrap>permissions: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>users: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>roles: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<% } else { %>
<tr>
<td align="right" nowrap><a href="/hci/permissions?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= (String)group.get("group-id") %>">permissions</a>: </td>
<td align="center" nowrap>
<% if (null != permissions) { %>
<% 	for (final String permission_id: permissions) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> permission = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Permissions.permissions.get(permission_id); %>
<a href="/hci/permission?session-id=<%= (String)request.getAttribute("session-id") %>&permission-id=<%= permission_id %>"><%= (String)permission.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/roles?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= (String)group.get("group-id") %>">roles</a>: </td>
<td align="center" nowrap>
<% if (null != roles) { %>
<% 	for (final String role_id: roles) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/users?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= (String)group.get("group-id") %>">users</a>: </td>
<td align="center" nowrap>
<% if (null != users) { %>
<% 	for (final String user_id: users) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= (String)request.getAttribute("session-id") %>&user-id=<%= user_id %>"><%= (String)user.get("user-id") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
<% } // if (true == mode.equals("create")) %>
</tr>
</table>
<br/>
<% if (null == group) { %>
<% 	if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-group-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 	} %>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } %>
|
<a href="/hci/groups?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>