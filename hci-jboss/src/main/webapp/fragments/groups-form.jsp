<!-- fragments/groups-form.jsp -->
<%
final java.util.Map<String, java.util.Map<String, Object>> groups = (java.util.Map<String, java.util.Map<String, Object>>)request.getAttribute("groups");
final String __permission_id = (String)request.getAttribute("permission-id");
final String qs_permission_id = null == __permission_id ? "" : "&permission-id="+(String)request.getAttribute("permission-id");
final String __role_id = (String)request.getAttribute("role-id");
final String qs_role_id = null == __role_id ? "" : "&role-id="+(String)request.getAttribute("role-id");
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+(String)request.getAttribute("user-id");
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != __permission_id) { %>
Update permission of groups:
<% } else if (null != __role_id) { %>
Update role for groups:
<% } else if (null != __user_id) { %>
Update user groups:
<% } else { %>
Update groups:
<% } %>
<br/>
<br/>
<form id="id#form-groups" name="form-groups" method="POST" action="/hci/groups?session-id=<%= (String)request.getAttribute("session-id") %><%= qs_permission_id %><%= qs_role_id %><%= qs_user_id %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>group-id</td>
<td>name</td>
<td>description</td>
<td>organisation</td>
</tr>
<% for (final String group_id: groups.keySet()) {
if (true == group_id.equals("$id")) continue;
	final java.util.Map<String, Object> group = groups.get(group_id);
	final Integer id = (Integer)group.get("$id");
	final Boolean checked = null == request.getAttribute("group$"+id+"#checked") ? false : (Boolean)request.getAttribute("group$"+id+"#checked");
	final String name = null == group.get("name") ? "" : (String)group.get("name");
	final String description = null == group.get("description") ? "" : (String)group.get("description");
	final boolean organisation = null == group.get("organisation") ? false : (boolean)group.get("organisation");
%>
<tr>
<td><%= id %><input id="id#group-ids" name="group-ids" type="hidden" value="<%= id %>"/></td>
<% final String checked_group = false == checked ? "" : " checked"; %>
<td><input id="id#group$<%= id %>#checked" name="group$<%= id %>#checked" type="checkbox"<%= checked_group %>/></td>
<td><a href="/hci/group?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= group_id %>"><%= group_id %></a></td>
<td><%= name %></td>
<td><%= description %></td>
<td><%= organisation %></td>
</tr>
<% } %>
</table>
<br/>
<% if (null != __permission_id || null != __role_id || null != __user_id) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% } %>
|
<a href="/hci/index?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>