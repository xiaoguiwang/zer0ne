<!-- fragments/permissions-form.jsp -->
<%
final java.util.Map<String, java.util.Map<String, Object>> permissions = (java.util.Map<String, java.util.Map<String, Object>>)request.getAttribute("permissions");
final String __role_id = (String)request.getAttribute("role-id");
final String qs_role_id = null == __role_id ? "" : "&role-id="+(String)request.getAttribute("role-id");
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+(String)request.getAttribute("user-id");
final String __group_id = (String)request.getAttribute("group-id");
final String qs_group_id = null == __group_id ? "" : "&group-id="+(String)request.getAttribute("group-id");
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != __role_id) { %>
Update permissions for role:
<% } else if (null != __user_id) { %>
Update permissions of user:
<% } else if (null != __group_id) { %>
Update permissions of group:
<% } else { %>
Update permissions:
<% } %>
<br/>
<br/>
<form id="id#form-permissions" name="form-permissions" method="POST" action="/hci/permissions?session-id=<%= (String)request.getAttribute("session-id") %><%= qs_role_id %><%= qs_user_id %><%= qs_group_id %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>permission-id</td>
<td>name</td>
<td>description</td>
</tr>
<% for (final String permission_id: permissions.keySet()) {
if (true == permission_id.equals("$id")) continue;
	final java.util.Map<String, Object> permission = permissions.get(permission_id);
	final Integer id = (Integer)permission.get("$id");
	final Boolean checked = null == request.getAttribute("permission$"+id+"#checked") ? false : (Boolean)request.getAttribute("permission$"+id+"#checked");
	final String name = null == permission.get("name") ? "" : (String)permission.get("name");
	final String description = null == permission.get("description") ? "" : (String)permission.get("description");
%>
<tr>
<td><%= id %><input id="id#permission-ids" name="permission-ids" type="hidden" value="<%= id %>"/></td>
<% final String checked_permission = false == checked ? "" : " checked"; %>
<td><input id="id#permission$<%= id %>#checked" name="permission$<%= id %>#checked" type="checkbox"<%= checked_permission %>/></td>
<td><a href="/hci/permission?session-id=<%= (String)request.getAttribute("session-id") %>&permission-id=<%= permission_id %>"><%= permission_id %></a></td>
<td><%= name %></td>
<td><%= description %></td>
</tr>
<% } %>
</table>
<br/>
<% if (null != __role_id || null != __user_id || null != __group_id) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% } %>
|
<a href="/hci/index?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>