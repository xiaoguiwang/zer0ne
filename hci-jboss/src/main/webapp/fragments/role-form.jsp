<!-- fragments/role-form.jsp -->
<%
final java.util.Map<String, Object> signed_in_user = (java.util.Map<String, Object>)request.getAttribute("signed-in-user");
final java.util.Map<String, Object> role = (java.util.Map<String, Object>)request.getAttribute("role");
final String role_id, name, description;
final java.util.Set<String> permissions, users, groups;
if (null == role) {
	role_id = "";
	name = "";
	description = "";
	permissions = null;
	users = null;
	groups = null;
} else {
	role_id = null == role.get("role-id") ? "" : (String)role.get("role-id");
	name = null == role.get("name") ? "" : (String)role.get("name");
	description = null == role.get("description") ? "" : (String)role.get("description");
	permissions = (java.util.Set<String>)role.get("permissions");
	users = (java.util.Set<String>)role.get("users");
	groups = (java.util.Set<String>)role.get("groups");
}
final String mode = null == request.getAttribute("role-mode") ? "" : (String)request.getAttribute("role-mode");
final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<form id="id#form-role" name="form-role" method="POST" action="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>role-id: </td>
<td width="12%" align="center" nowrap><input id="id#role" name="role-id" type="text" value="<%= role_id %>"/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("role-id") ? "&nbsp;" : results.get("role-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>name: </td>
<td align="center" nowrap><input id="id#name" name="name" type="text" value="<%= name %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("name") ? "&nbsp;" : results.get("name") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>description: </td>
<td align="center" nowrap><input id="id#description" name="description" type="text" value="<%= description %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("description") ? "&nbsp;" : results.get("description") %></td>
<td></td>
</tr>
<% if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/group %>
<tr>
<td align="right" nowrap>permissions: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>users: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>groups: </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<% } else { %>
<tr>
<td align="right" nowrap><a href="/hci/permissions?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= (String)role.get("role-id") %>">permissions</a>: </td>
<td align="center" nowrap>
<% if (null != permissions) { %>
<% 	for (final String permission_id: permissions) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> permission = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Permissions.permissions.get(permission_id); %>
<a href="/hci/permission?session-id=<%= (String)request.getAttribute("session-id") %>&permission-id=<%= permission_id %>"><%= (String)permission.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/users?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= (String)role.get("role-id") %>">users</a>: </td>
<td align="center" nowrap>
<% if (null != users) { %>
<% 	for (final String user_id: users) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= (String)request.getAttribute("session-id") %>&user-id=<%= user_id %>"><%= (String)user.get("user-id") %></a><br/>
<% 	} %>
<% } %>
</td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/groups?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= (String)role.get("role-id") %>">groups</a>: </td>
<td align="center" nowrap>
<% if (null != groups) { %>
<% 	for (final String group_id: groups) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= group_id %>"><%= (String)group.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
</tr>
<% } // if (true == mode.equals("create")) %>
</table>
<br/>
<% if (null == role) { %>
<% 	if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-role-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 	} %>
<% } else  { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } %>
|
<a href="/hci/roles?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>