<!-- fragments/roles-form.jsp -->
<%
final java.util.Map<String, java.util.Map<String, Object>> roles = (java.util.Map<String, java.util.Map<String, Object>>)request.getAttribute("roles");
final String __permission_id = (String)request.getAttribute("permission-id");
final String qs_permission_id = null == __permission_id ? "" : "&permission-id="+(String)request.getAttribute("permission-id");
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+(String)request.getAttribute("user-id");
final String __group_id = (String)request.getAttribute("group-id");
final String qs_group_id = null == __group_id ? "" : "&group-id="+(String)request.getAttribute("group-id");
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != request.getAttribute("permission-id")) { %>
Update permission for roles:
<% } else if (null != request.getAttribute("user-id")) { %>
Update user roles:
<% } else if (null != request.getAttribute("group-id")) { %>
Update group for roles:
<% } else { %>
Update roles:
<% } %>
<br/>
<br/>
<form id="id#form-roles" name="form-roles" method="POST" action="/hci/roles?session-id=<%= (String)request.getAttribute("session-id") %><%= qs_permission_id %><%= qs_user_id %><%= qs_group_id %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>role-id</td>
<td>name</td>
<td>description</td>
</tr>
<% for (final String role_id: roles.keySet()) {
if (true == role_id.equals("$id")) continue;
	final java.util.Map<String, Object> role = roles.get(role_id);
	final Integer id = (Integer)role.get("$id");
	final Boolean checked = null == request.getAttribute("role$"+id+"#checked") ? false : (Boolean)request.getAttribute("role$"+id+"#checked");
	final String name = null == role.get("name") ? "" : (String)role.get("name");
	final String description = null == role.get("description") ? "" : (String)role.get("description");
%>
<tr>
<td><%= id %><input id="id#role-ids" name="role-ids" type="hidden" value="<%= id %>"/></td>
<% final String checked_role = false == checked ? "" : " checked"; %>
<td><input id="id#role$<%= id %>#checked" name="role$<%= id %>#checked" type="checkbox"<%= checked_role %>/></td>
<td><a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= role_id %></a></td>
<td><%= name %></td>
<td><%= description %></td>
</tr>
<% } %>
</table>
<br/>
<% if (null != __permission_id || null != __user_id || null != __group_id) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% } %>
|
<a href="/hci/index?session-id=<%= (String)request.getAttribute("session-id") %>">cancel</a>
</form>