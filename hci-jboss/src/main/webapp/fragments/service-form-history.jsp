<!-- fragments/service-form-history.jsp -->
<!-- service history e.g. e-mail, sms, and started/stopped, and in future including success/failure events -->
<% if (null == request.getAttribute("for:user#id")) {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
	final Integer service_id = null == service ? null : (Integer)service.get("$id");
  /*final */java.util.List<java.util.Map<String, Object>> /*service_*/history_list = null;
	final Object o = request.getAttribute("service#history");
  /*final */java.util.Map<String, Object> /*service_*/history = null;
	if (true == o instanceof Boolean) {
		if (null != service) {
		  /*final java.util.List<java.util.Map<String, Object>> service_*/history_list = hci.j2ee.Services.getHistoryForServiceId((Integer)service.get("$id"));
		}
	} else {
	  /*final java.util.Map<String, Object> service_*/history = (java.util.Map<String, Object>)o;
	}
	final String mode = (String)request.getAttribute("mode");
	final String cancel = (String)request.getAttribute("cancel");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create service history:
<% 	} else if (false == "delete".equals(mode)) { %>
Service history:
<% 	} else { %>
Delete service history:
<% 	} %>
<br/>
<br/>
<% 	if ((null == request.getAttribute("service#history")) || (null != /*service_*/history_list && 0 < /*service_*/history_list.size())) { %>

<% 	if (null != request.getParameter("id")) { %><input name='id' type='hidden' value='<%= request.getParameter("id") %>'/><% } %>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>history-id</td>
<td>service-id</td>
<td>for-user-id</td>
<td></td>
</tr>
<% 		for (final java.util.Map<String, Object> /*service*/_history: /*service_*/history_list) {
			final Integer id = (Integer)/*service*/_history.get("$id");
 			final Boolean checked = null == request.getAttribute("service-history$"+id+"#checked") ? false : (Boolean)request.getAttribute("service-history$"+id+"#checked"); %>
<tr>
<td><%= id %><input id="id#service-history-ids" name="service-history-ids" type="hidden" value="<%= id %>"/></td>
<% 			final String checked_service_history = false == checked ? "" : " checked"; %>
<td nowrap><input id="id#service-history$<%= id %>#checked" name="service-history$<%= id %>#checked" type="checkbox"<%= checked_service_history %>/></td>
<% 			{
				final String key = "service-history-id";
				final Integer value = null == /*service*/_history ? null : (Integer)/*service*/_history.get(key); %>
<td><a href="/hci/service?history&session-id=<%= session_id %>&service-id=<%= service.get("$id") %>&id=<%= id %>&cancel=service-history"><%= value %></a></td>
<% 			}
			{
				final String key = "service:history:service-id";
				final String label = "service-id";
				final String value = null == /*service*/_history ? null : (String)/*service*/_history.get(label); %>
<td><%= value %></td>
<% 			}
			{
				final String key = "service:history:for-user-id";
				final String label = "for-user-id"; 
				final String value = null == /*service*/_history ? null : (String)/*service*/_history.get(label); %>
<td><%= value %></td>
<% 			}
			{
				final java.util.Map<String, Object> /*service*/_lines_history = (java.util.Map<String, Object>)/*service*/_history.get("%lines"); %>
<td>
<% 				if (null != /*service*/_lines_history && 0 < /*service*/_lines_history.size()) { %>
<a href="/hci/service?lines&history&session-id=<%= session_id %>&service-id=<%= service_id %>&id=<%= id %>&cancel=service-history">lines</a>
<% 				} %>
</td>
<% 			} %>
</tr>
<% 		} %>
</table>
<br/>
<% 		if (false == "create".equals(mode)) { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= service_id %>&mode=create&cancel=service-history">create</a>
|
<% 		}
		if (true == "create".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
|
<% 		} else { %>
<input id="id#submit" name="__submit" type="submit" value="delete"/>
|
<% 		} %>
<% 		if (true == "services".equals(cancel)) { %>
<a href="/hci/services?session-id=<%= session_id %>">cancel</a>
<% 		} else { %>
<a href="/hci/service?session-id=<%= session_id %>&id=<%= service_id %>&cancel=services">cancel</a>
<% 		} %>

<% 	} else { %>

<input name='id' type='hidden' value='<%= (Integer)service.get("$id") %>'/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<% 		{
			final String key = "service-history-id";
			final Integer value = null == /*service_*/history ? null : (Integer)/*service_*/history.get(key); %>
<td width='44%' align='right' nowrap><%= key %>: </td>
<td width='12%' align='left' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td width='44%' align='left'></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
<tr>
<% 		{
			final String key = "service:history:service-id";
			final String label = "service-id";
			final String value = null == /*service_*/history ? null : (String)/*service_*/history.get(label); %>
<td align='right' nowrap><%= label %>: </td>
<td width='center' nowrap><input name='<%= key %>' type='hidden' value='<%= value %>'/><input id='id#<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='center' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
<tr>
<% 		{
			final String key = "service:history:for-user-id";
			final String label = "for-user-id"; 
			final String value = null == /*service_*/history ? null : (String)/*service_*/history.get(label); %>
<td align='right' nowrap><a href="/hci/service?for-user-id&history&session-id=<%= session_id %><%= null == service ? "" : "&id="+(Integer)service.get("$id") %>&mode=<%= mode %>&cancel=service-history">for:user#id</a>: </td>
<td align='left' nowrap><input name='<%= key %>' type='hidden' value='<%= null == value ? "" : value %>'/><%= null == value ? "" : value %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 		} %>
</tr>
</table>
<br/>
<% 		if (null != /*service_*/history && false == "create".equals(mode)) { %>
<a href="/hci/service?line&history&session-id=<%= session_id %>&id=<%= (Integer)/*service_*/history.get("$id") %>&mode=create&cancel=service">create line</a>
|
<% 		} %>
<% 		if (true == "create".equals(mode)) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
<% 		} else { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 		} %>
|
<% 		if (true == "create".equals(mode)) {
			if (true == "service".equals(cancel)) { %>
<a href="/hci/service?session-id=<%= session_id %>&id=<%= service_id %>&cancel=services">cancel</a>
<% 			} else { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= service_id %>&cancel=services">cancel</a>
<% 			}
		} else if (true == "services".equals(cancel)) { %>
<a href="/hci/services?session-id=<%= session_id %>">cancel</a>
<% 		} else if (true == "services-history".equals(cancel)) { %>
<a href="/hci/services?history&session-id=<%= session_id %>">cancel</a>
<% 		} %>

<% 	} %>

<% } else { %>
<%@include file='/fragments/users-form.jsp' %>
<% } %>