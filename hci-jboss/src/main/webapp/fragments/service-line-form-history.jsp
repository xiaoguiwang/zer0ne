<!-- fragments/service-line-form-history.jsp -->
<!-- service history of line: e.g. price increase/decrease, promotion, etc. -->
<% {
	final String session_id = (String)request.getAttribute("session-id");
  /*final */java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
	final Integer service_id = null == service ? null : (Integer)service.get("$id");
  /*final */java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)request.getAttribute("service#history");
	final Integer /*service_*/history_id = (Integer)/*service_*/history.get("$id");
  /*final */Integer /*service_history_line_*/id = null;
	final Object o = request.getAttribute("service#line#history");
  /*final */java.util.Map<String, Object> /*service_*/line_history = null;
	if (true == o instanceof Boolean) {
	} else {
	  /*final java.util.Map<String, Object> service_*/line_history = (java.util.Map<String, Object>)o;
	  /*final Integer service_history_line_*/id = (Integer)/*service_*/line_history.get("$id");
	}
	final String mode = (String)request.getAttribute("mode");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (true == "create".equals(mode)) { %>
Create service line history:
<% 	} else if (false == "delete".equals(mode)) { %>
Update service line history:
<script src="echarts/dist/echarts.min.js"></script>
<% 	} else { %>
Delete service line history:
<% 	} %>
<br/>
<br/>
<% if (null != /*service_history_line_*/id) { %><input name='id' type='hidden' value='<%= /*service_history_line_*/id %>'/><% } %>
<input name='history-id' type='hidden' value='<%= /*service_*/history_id %>'/>
<input name='service-id' type='hidden' value='<%= service_id %>'/>
<input name='line' type='hidden'/>
<input name='history' type='hidden'/>
<table border=0 cellpadding=4 cellspacing=4>
<%//final Integer id = (Integer)/*service_*/line_history.get("$id");
	final Float /*service_history_*/number_of_units_reserved = null == /*service_*/line_history ? null : null == /*service_*/line_history.get("number-of-units@reserved") ? null : (Float)/*service_*/line_history.get("number-of-units@reserved");
	final java.util.Map<Long, Float> /*service_history_*/consumed = null == /*service_*/line_history ? null : null == /*service_*/line_history.get("%consumed") ? null : (java.util.Map<Long, Float>)/*service_*/line_history.get("%consumed");
	final Float /*service_history_*/number_of_units_consumed = null == /*service_history_*/consumed ? null : new Float(/*service_history_*/consumed.size()-1);
	final java.util.Map<Long, Float> /*service_history_*/charged = null == /*service_*/line_history ? null : null == /*service_*/line_history.get("%charged") ? null : (java.util.Map<Long, Float>)/*service_*/line_history.get("%charged");
	final Float /*service_history_*/number_of_units_charged = null == /*service_history_*/charged ? null : new Float(/*service_history_*/charged.size()-1); %>
<tr>
<% 	{
		final String key = "service-history-id";
		final Integer value = null == /*service_*/history ? null : (Integer)/*service_*/history.get(key); %>
<td width='44%' align='right' nowrap><%= key %>:</td>
<td width='12%' align='left' nowrap><input id='id#<%= key %>' name='<%= key %>' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td width='44%' align='left'></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = "service-id";
		final String value = null == service ? null : (String)service.get("service-id"); %>
<td align="right" nowrap><%= key %>: </td>
<td align="left" nowrap><input id='id#service-id' type='text' value='<%= null == value ? "" : value %>' disabled/></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = /*"service:history:*/"for-user-id";
		final String label = "for:user#id"; 
		final String value = null == /*service_*/history ? null : (String)/*service_*/history.get(key); // should be label %>
<td align="right" nowrap>for:user#id: </td>
<td align="left" nowrap><%= null == value ? "" : value %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("service:history:for-user-id") ? "&nbsp;" : results.get("service:history:for-user-id") %></td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = "line-id";
		final String value = null == /*service_*/line_history ? null : (String)/*service_*/line_history.get(key); %>
<td align="right" nowrap><a href="/hci/service?for-line-id&lines&session-id=<%= session_id %><%= null == /*service_*/history ? "" : "&history-id="+(Integer)/*service_*/history.get("$id") %>&id=<%= service.get("$id") %>&cancel=service-line-history">service:line#id</a>: </td>
<td align="left" nowrap><input id='id#service-line-id' name='service-line-id' type='hidden' value='<%= null == value ? "" : value %>'/><%= null == value ? "" : value %></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("service-line-id") ? "&nbsp;" : results.get("service-line-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>(reserved) number-of-units: </td>
<td align="left" nowrap><input id="id#service:line:history:number-of-units@reserved" name="service:line:history:number-of-units@reserved" type="text" value="<%= null == /*service_line_history_*/number_of_units_reserved ? "" : /*service_line_history_*/number_of_units_reserved %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("service:line:history:number-of-units@reserved") ? "&nbsp;" : results.get("service:line:history:number-of-units@reserved") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>(consumed) number-of-units: </td>
<td align="left" nowrap><input id="id#service:line:history:number-of-units@consumed" name="service:line:history:number-of-units@consumed" type="text" value="<%= null == /*service_line_history_*/number_of_units_consumed ? "" : /*service_line_history_*/number_of_units_consumed %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td><div id='echart' style='background-color: #fcfcfc; width: 600px; height: 150px;'></div></td>
<td></td>
</tr>
<% 	if ((null == /*service_line_history_*/consumed || 0 == /*service_line_history_*/consumed.size()-1) || (null != results && true == results.containsKey("service:line:history:number-of-units@consumed"))) { %>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("service:line:history:number-of-units@consumed") ? "&nbsp;" : results.get("service:line:history:number-of-units@consumed") %></td>
<td></td>
</tr>
<% 	}
	if (null != /*service_line_history_*/consumed) { %>
<script type="text/javascript">
	// https://echarts.apache.org/examples/en/editor.html?c=scatter-single-axis
	var echart = echarts.init(document.getElementById('echart'));
	var time = [];
	var type = ['consumed'];
	var data = [];
</script>
<% 		int i = 0;
		for (final Object k: /*service_line_history_*/consumed.keySet()) {
if (true == k.toString().equals("$id")) continue;
			final Long ms = (Long)k; %>
<script type="text/javascript">
	time.push(<%= i %>);
</script>
<% 			final Float /*service_line_history_*/number_of_units = ((Float)/*service_line_history_*/consumed.get(ms)).floatValue();
%>
<script type="text/javascript">
	data.push([0,<%= i %>,<%= /*service_line_history_*/number_of_units %>]);
</script>
<tr>
<td align="right" nowrap>%consumed{<%= ms.toString() %>}: </td>
<td align="left" nowrap><%= null == /*service_line_history_*/number_of_units ? "" : new java.text.DecimalFormat("#,##0.0").format(/*service_line_history_*/number_of_units) %></td>
<td></td>
</tr>
<% 			i++;
		}
	} %>
<script type="text/javascript">
	var option = {
		tooltip : {
			position : 'top'
		},
		title : [],
		singleAxis: [],
		series: []
	};
	type.forEach(function(type, idx) {
	  //option.title.push({
	  //	textBaseline : 'middle',
	  //	top : (idx + 0.5) * 100 / 1 + '%',
	  //	text : type
	  //});
		option.singleAxis.push({
			left : 150,
			type : 'category',
			boundaryGap : false,
			data : time,
			top : (idx * 100 / 1 - 25) + '%',
			height : (100 / 1 - 10) + '%',
			axisLabel : {
				interval : 0
			}
		});
		option.series.push({
			singleAxisIndex : idx,
			coordinateSystem : 'singleAxis',
			type : 'scatter',
			data : [],
			symbolSize : function(dataItem) {
				return dataItem[1] * 4;
			}
		});
	});
	data.forEach(function(dataItem) {
		option.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
	});
	echart.setOption(option);
</script>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>(charged) number-of-units: </td>
<td align="left" nowrap><input id="id#service:line:history:number-of-units@charged" name="service:line:history:number-of-units@charged" type="text" value="<%= null == /*service_line_history_*/number_of_units_charged ? "" : /*service_line_history_*/number_of_units_charged %>"/></td>
<td></td>
</tr>
<% 	if ((null == /*service_line_history_*/charged || 0 == /*service_line_history_*/charged.size()-1) || (null != results && true == results.containsKey("service:line:history:number-of-units@charged"))) { %>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("service:line:history:number-of-units@charged") ? "&nbsp;" : results.get("service:line:history:number-of-units@charged") %></td>
<td></td>
</tr>
<% 	}
	if (null != /*service_line_history_*/charged) {
		for (final Object k: /*service_line_history_*/charged.keySet()) {
if (true == k.toString().equals("$id")) continue;
			final Long ms = (Long)k;
			final Float /*service_line_history_*/number_of_units = ((Float)/*service_line_history_*/charged.get(ms)).floatValue();
%>
<tr>
<td align="right" nowrap>%charged{<%= ms.toString() %>}: </td>
<td align="left" nowrap><%= null == /*service_line_history_*/number_of_units ? "" : new java.text.DecimalFormat("#,##0.0").format(/*service_line_history_*/number_of_units) %></td>
<td></td>
</tr>
<% 		}
	} %>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<% 	{
		final String key = "bill-history-id";
		final Integer value = null == /*service_*/history ? null : (Integer)/*service_*/history.get("bill-history-id");
		final java.util.Map<String, Object> /*bill*/_history = hci.j2ee.Bills.getHistoryById(value);
		final Integer bill_id = (Integer)/*bill*/_history.get("bill-id");
		final java.util.Map<String, Object> bill = hci.j2ee.Bills.getById(bill_id); %>
<td align="right" nowrap>bill-history#id: </td>
<td align="left" nowrap><a href="/hci/bill?history&session-id=<%= session_id %>&bill-id=<%= bill.get("$id") %>&id=<%= /*bill*/_history.get("$id") %>"><%= null == value ? "" : value %></a></td>
<td></td>
<% 	} %>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("bill-history-id") ? "&nbsp;" : results.get("bill-history-id") %></td>
<td></td>
</tr>
</table>
<br/>
<% 	if (true == "create".equals(mode) || null == /*service_history_line_*/id) { %>
<input id="id#submit" name="__submit" type="submit" value="create"/>
<% 	} else { %>
<input id="id#submit" name="__submit" type="submit" value="update"/>
|
<input id="id#submit" name="__submit" type="submit" value="charge"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	} %>
|
<% 	if ("service-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= service_id %>">cancel</a>
<% 	} else if ("service-lines-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/service?lines&history&session-id=<%= session_id %>&service-id=<%= service_id %>&id=<%= (Integer)/*service_*/history.get("$id") %>&cancel=service-lines-history">cancel</a>
<% 	} else { %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 	} %>
<% } %>