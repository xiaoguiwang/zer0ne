<!-- fragments/service-lines-form-history.jsp -->
<!-- service history of lines: e.g. price increase/decrease, promotion, etc. -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
	final Integer service_id = null == service ? null : (Integer)service.get("$id");
	final java.util.Map<String, Object> /*service_*/history = (java.util.Map<String, Object>)request.getAttribute("service#history");
	final java.util.Map<String, Object> /*service_*/lines_history = (java.util.Map<String, Object>)request.getAttribute("service#lines#history");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else { %>
Service lines history:
<% 	} %>
<br/>
<br/>
<input name='service-id' type='hidden' value='<%= service_id %>'/>
<input name='lines' type='hidden'/>
<input name='history' type='hidden'/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>service-id</td>
<td>for-user#id</td>
<td>line-id</td>
<td>units:</td>
<td>reserved</td>
<td>consumed</td>
<td>charged</td>
<td>bill-history#id</td>
</tr>
<% 	if (null != /*service_*/lines_history && 0+1 < /*service_*/lines_history.size()) {
		for (final String /*service_history_*/line_id: /*service_*/lines_history.keySet()) {
if (true == /*service_history_*/line_id.equals("$id")) continue; %>
<tr>
<% 			final java.util.Map<String, Object> /*service_*/line_history = (java.util.Map<String, Object>)/*service_*/lines_history.get(/*service_history_*/line_id);
			final Integer id = (Integer)/*service_*/line_history.get("$id"); %>
<td><%= id %><input id="id#service-line-history-ids" name="service-line-history-ids" type="hidden" value="<%= id %>"/></td>
<% 			final Boolean checked = null == request.getAttribute("service-line-history$"+id+"#checked") ? false : (Boolean)request.getAttribute("service-line-history$"+id+"#checked");
			final String checked_service_line_history = false == checked ? "" : " checked"; %>
<td><input id="id#service-line-history$<%= id %>#checked" name="service-line-history$<%= id %>#checked" type="checkbox"<%= checked_service_line_history %>/></td>
<td><%= null == /*service_*/history ? "" : (String)/*service_*/history.get("service-id") %></td>
<% 			final String for_user_id = null == /*service_*/history ? "" : (String)/*service_*/history.get("for-user-id"); %>
<td><%= for_user_id %></td>
<td><a href="/hci/service?line&history&session-id=<%= session_id %>&service-id=<%= service_id %>&history-id=<%= (Integer)/*service_*/history.get("$id") %>&id=<%= id %>&cancel=service-lines-history"><%= /*service_history_*/line_id %></a></td>
<td></td>
<% 			final Float number_of_units_reserved = null == /*service_*/line_history.get("number-of-units@reserved") ? 0.0f : (Float)/*service_*/line_history.get("number-of-units@reserved"); %>
<td><%= new java.text.DecimalFormat("#,##0.#").format(number_of_units_reserved) %></td>
<% 			final java.util.Map<Long, Float> consumed = (java.util.Map<Long, Float>)/*service_*/line_history.get("%consumed");
			final Float number_of_units_consumed = new Float(consumed.size()-1); %>
<td><%= new java.text.DecimalFormat("#,##0.#").format(number_of_units_consumed) %></td>
<% 			final java.util.Map<Long, Float> charged = (java.util.Map<Long, Float>)/*service_*/line_history.get("%charged");
			final Float number_of_units_charged = new Float(charged.size()-1); %>
<td><%= new java.text.DecimalFormat("#,##0.#").format(number_of_units_charged) %></td>
<% 			final Integer bill_history_id = null == /*service_*/history ? null : (Integer)/*service_*/history.get("bill-history-id");
			final java.util.Map<String, Object> /*bill*/_history = hci.j2ee.Bills.getHistoryById(bill_history_id);
			final Integer bill_id = (Integer)/*bill*/_history.get("bill-id");
			final java.util.Map<String, Object> bill = hci.j2ee.Bills.getById(bill_id); %>
<td><% 		if (null == bill_history_id) { %>N/A<% } else { %><a href="/hci/bill?history&session-id=<%= session_id %>&bill-id=<%= bill.get("$id") %>&id=<%= /*bill*/_history.get("$id") %>"><%= bill_history_id %></a><% } %></td>
</tr>
<% 		}
	} %>
</table>
<br/>
<a href="/hci/service?line&history&session-id=<%= session_id %>&id=<%= /*service_*/history.get("$id") %>&mode=create&cancel=service-lines-history">create</a>
|
<% 	if (null != /*service_*/lines_history && 0+1 < /*service_*/lines_history.size()) { %>
<input id="id#submit" name="__submit" type="submit" value="charge"/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
|
<% 	} %>
<% 	if (true == "service-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= service_id %>&cancel=service-history">cancel</a>
<% 	} else if (true == "services-history".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/services?history&session-id=<%= session_id %>">cancel</a>
<% 	} %>
<% } %>