<!-- fragments/service-lines-form.jsp -->
<!-- services lines list e.g. 1 e-mail and unit price, 5 e-mails and bulk price -->
<%@page import="hci.j2ee.ServletTool" %>
<%@page import="hci.j2ee.Services"%>
<% if (null == request.getAttribute("service#lines#history")) {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> service = (java.util.Map<String, Object>)request.getAttribute("service");
	final Integer service_id = null == service ? null : (Integer)service.get("$id");
	final Integer id = null == request.getParameter("id") ? null : new Integer(request.getParameter("id")); // This was service.get("$id") so might have broken service history - in which case, fix this/that in ServiceLogic, rather than breaking bill line history / BillLogic
	final java.util.Map<String, Object> /*service_*/lines = (java.util.Map<String, Object>)request.getAttribute("service#lines");
	final Integer /*service_*/history_id = null == request.getParameter("history-id") ? null : new Integer(request.getParameter("history-id"));
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
	final String uri = request.getRequestURI().toString();
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (null == request.getAttribute("for:line#id")) { %>
Update service lines:
<% 	} else { %>
Select service lines:
<% 	} %>
<br/>
<br/>
<% 	if (null != request.getParameter("for-line-id")) { %><input name='for-line-id' type='hidden'/><% } %>
<% 	if (null != request.getParameter("lines")) { %><input name='lines' type='hidden'/><% } %>
<% 	if (null != request.getParameter("bill-id")) { %><input name='bill-id' type='hidden' value='<%= request.getParameter("bill-id") %>'/><% } %>
<% 	if (null != history_id) { %><input name='history-id' type='hidden' value='<%= history_id %>'/><% } %>
<input name='id' type='hidden' value='<%= id %>'/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>line-id</td>
<td>description</td>
<td>$state</td>
<td nowrap>unit-of-measure</td>
<td nowrap>price-per-unit</td>
</tr>
<% 	{
		final String search_line_id = ServletTool.getParameterOrAttribute(request, "line-id", "");
		final String search_description = ServletTool.getParameterOrAttribute(request, "description", "");
		final String search_state = ServletTool.getParameterOrAttribute(request, "state", "");
		final String search_unit_of_measure = ServletTool.getParameterOrAttribute(request, "unit-of-measure", "");
		final String search_price_per_unit = ServletTool.getParameterOrAttribute(request, "price-per-unit", "");
%>
<tr>
<td></td>
<td></td>
<td><input id="id#line-id" name="line-id" type="text" value="<%= search_line_id %>"/></td>
<td><input id="id#description" name="description" type="text" value="<%= search_description %>"/></td>
<td><input id="id#state" name="state" type="text" value="<%= search_state %>"/></td>
<td><input id="id#unit-of-measure" name="unit-of-measure" type="text" value="<%= search_unit_of_measure %>"/></td>
<td><input id="id#price-per-unit" name="price-per-unit" type="text" value="<%= search_price_per_unit %>"/></td>
</tr>
<%/*final */int count = 0; %>
<% 		for (final String /*service_*/line_id: /*service_*/lines.keySet()) {
if (true == line_id.equals("$id")) continue;
			final java.util.Map<String, Object> /*service_*/line = (java.util.Map<String, Object>)/*service_*/lines.get(line_id);
			final Integer __id = (Integer)/*service_*/line.get("$id");
 			final Boolean checked = null == request.getAttribute("service-line$"+__id+"#checked") ? false : (Boolean)request.getAttribute("service-line$"+__id+"#checked");
			final boolean enabled = true;// == self_id.equals(user_id) || true == Users.hasPermission(self, "..");
		  //if (true == enabled) {
				count++;
		  //}
			final String disabled = false == enabled ? " disabled" : "";
			if (null != search_line_id && 0 < search_line_id.trim().length() && false == line_id.contains(search_line_id)) continue;
			final String /*service_line_*/description = (String)/*service_*/line.get("description");
			if (null != search_description && 0 < search_description.trim().length() && false == description.contains(search_description)) continue;
			final String /*service_line_*/state = (String)/*service_*/line.get("$state");
			if (null != search_state && 0 < search_state.trim().length() && false == state.contains(search_state)) continue;
			final String /*service_line_*/unit_of_measure = (String)/*service_*/line.get("unit-of-measure");
			if (null != search_unit_of_measure && 0 < search_unit_of_measure.trim().length() && false == unit_of_measure.contains(search_unit_of_measure)) continue;
			final Float /*service_line_*/price_per_unit = null == /*service_*/line.get("price-per-unit") ? null : (Float)/*service_*/line.get("price-per-unit");
		  /*final */Float __price_per_unit = null;
			try {
			  /*final Float */__price_per_unit = new Float(search_price_per_unit);
			} catch (final NumberFormatException e) {
				// Silently ignore..
			}
			if (null != search_price_per_unit && 0 < search_price_per_unit.trim().length() && null != price_per_unit && null != __price_per_unit && price_per_unit.floatValue() != __price_per_unit.floatValue()) continue;
%>
<tr>
<td><%= __id %><input id="id#service-line-ids" name="service-line-ids" type="hidden" value="<%= __id %>"/></td>
<% 			final String checked_service_line = false == checked ? "" : " checked"; %>
<td nowrap><input id="id#service-line$<%= __id %>#checked" name="service-line$<%= __id %>#checked" type="checkbox"<%= checked_service_line %>/></td>
<td><% if (null != service_id) { %><a href="/hci/service?line&session-id=<%= session_id %>&service-id=<%= service_id %>&id=<%= __id %>&cancel=service-lines"><% } %><%= /*service_*/line_id %><% if (true == uri.startsWith("/service")) { %></a><% } %></td>
<td nowrap><%= /*service_line_*/description %></td>
<td><%= null == /*service_line_*/state ? "" : /*service_line_*/state %></td>
<td><%= null == /*service_line_*/unit_of_measure ? "" : /*service_line_*/unit_of_measure %></td>
<td><%= null == /*service_line_*/price_per_unit ? "" : new java.text.DecimalFormat("#,##0.00").format(/*service_line_*/price_per_unit) %></td>
</tr>
<% 		} %>
</table>
<br/>
<a href="/hci/service?line&session-id=<%= session_id %>&id=<%= service_id %>&mode=create&cancel=service-lines">create line</a>
|
<% 		if (null != request.getParameter("for-line-id")) { %>
<% 			if (null != lines && 0+1 < lines.size()) { %>
<input id="id#submit" name="__submit" type="submit" value="search"<%= 0 == count ? " disabled" : "" %>/>
|
<input id="id#submit" name="__submit" type="submit" value="select"<%= 0 == count ? " disabled" : "" %>/>
<% 			} %>
<% 		} else { %>
<input id="id#submit" name="__submit" type="submit" value="search"<%= 0 == count ? " disabled" : "" %>/>
|
<input id="id#submit" name="__submit" type="submit" value="delete"<%= 0 == count ? " disabled" : "" %>/>
<% 		} %>
|
<% 		if (true == "service-line-history".equals(request.getAttribute("cancel"))) {
			final String mode = request.getParameter("mode"); %>
<a href="/hci/service?line&history&session-id=<%= session_id %>&id=<%= service_id %>&mode=<%= mode %>">cancel</a>
<% 		} else if (true == "service".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/service?session-id=<%= session_id %>&id=<%= service_id %>">cancel</a>
<% 		} else if (true == "services".equals(request.getAttribute("cancel"))) { %>
<a href="/hci/services?session-id=<%= session_id %>">cancel</a>
<% 		} else if (true == "bill-line-history".equals(request.getAttribute("cancel"))) {
			final String mode = request.getParameter("mode");
			final String qs_mode = null == mode ? "" : "&mode="+mode;
			if (null == request.getParameter("history-id")) {
				final Integer bill_id = new Integer(request.getParameter("bill-id"));
				final Integer /*bill*/_history_id = new Integer(request.getParameter("id")); %>
<a href="/hci/bill?line&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&id=<%= _history_id %><%= qs_mode %>">cancel</a>
<% 			} else {
				final Integer bill_id = new Integer(request.getParameter("bill-id"));
				final Integer /*bill*/_history_id = new Integer(request.getParameter("history-id"));
				final Integer /*bill_history*/_line_id = new Integer(request.getParameter("id")); %>
<a href="/hci/bill?line&history&session-id=<%= session_id %>&bill-id=<%= bill_id %>&history-id=<%= _history_id %>&id=<%= _line_id %><%= qs_mode %>">cancel</a>
<% 			} %>
<% 		} else { %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 		}
	} %> 
<% } else { %>
<%@include file='/fragments/service-lines-form-history.jsp' %>
<% } %>