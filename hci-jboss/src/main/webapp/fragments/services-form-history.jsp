<!-- fragments/services-form-history.jsp -->
<!-- services history e.g. e-mail, sms, and started/stopped, and in future including success/failure events -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.List<java.util.Map<String, Object>> histories = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("services#history");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else { %>
Services history:
<% 	} %>
<br/>
<br/>
<input name='history' type='hidden'/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>history-id</td>
<td>service-id</td>
<td>for-user-id</td>
<td>description</td>
<td></td>
</tr>
<% 	for (final java.util.Map<String, Object> /*service_*/history: /*service_*/histories) {
		final Integer id = (Integer)/*service_*/history.get("$id"); %>
<tr>
<td><%= id %><input id="id#service-history-ids" name="service-history-ids" type="hidden" value="<%= id %>"/></td>
<% 		final Boolean checked = null == request.getAttribute("service-history$"+id+"#checked") ? false : (Boolean)request.getAttribute("service-history$"+id+"#checked");
		final String checked_service_history = false == checked ? "" : " checked"; %>
<td><input id="id#service-history$<%= id %>#checked" name="service-history$<%= id %>#checked" type="checkbox"<%= checked_service_history %>/></td>
<% 		final Integer /*service_*/history_id = (Integer)history.get("service-history-id");
		final String service_id = (String)/*service_*/history.get("service-id");
		final java.util.Map<String, Object> service = hci.j2ee.Services.get(service_id);
		final Integer __service_id = (Integer)service.get("$id"); %>
<td><a href="/hci/service?history&session-id=<%= session_id %>&service-id=<%= __service_id %>&id=<%= id %>&cancel=services-history"><%= /*service_*/history_id %></a></td>
<td><%= service_id %></td>
<% 		final String for_user_id = (String)/*service_*/history.get("for-user-id"); %>
<td><%= null == for_user_id ? "{null}" : 0 == for_user_id.trim().length() ? "{blank}" : for_user_id %></td>
<% 		final String description = (String)service.get("description"); %>
<td><%= null == description ? "{null}" : 0 == description.trim().length() ? "{blank}" : description %></td>
<% 		final java.util.Map<String, Object> /*service_*/lines_history = (java.util.Map<String, Object>)/*service_*/history.get("%lines"); %>
<td><% if (null != /*service_*/lines_history && 0 < /*service_*/lines_history.size()) { %><a href="/hci/service?lines&history&session-id=<%= session_id %>&id=<%= id %>&cancel=services-history">lines</a><% } %></td>
<% 	} %>
</table>
<br/>
<!--
<a href="/hci/service?history&session-id=<%= session_id %>&mode=create">create</a>
|-->
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	if (null != request.getParameter("cancel")) { %>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 	} %>
<% } %>