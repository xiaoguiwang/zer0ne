<!-- fragments/services-form.jsp -->
<!-- services list e.g. e-mail, sms, and state i.e. started/stopped -->
<%@page import="hci.j2ee.ServletTool" %>
<% if (null == request.getAttribute("services#history")) {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> services = (java.util.Map<String, Object>)request.getAttribute("services");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else { %>
Services:
<% 	} %>
<br/>
<br/>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>service-id</td>
<td>description</td>
<td>$state</td>
<td></td>
<td></td>
</tr>
<% 	for (final String service_id: services.keySet()) {
if (true == service_id.equals("$id")) continue; %>
<tr>
<% 		final java.util.Map<String, Object> service = (java.util.Map<String, Object>)services.get(service_id);
		final Integer id = (Integer)service.get("$id"); %>
<td><%= id %><input id="id#service-ids" name="service-ids" type="hidden" value="<%= id.toString() %>"/></td>
<% 		final Boolean checked = null == request.getAttribute("service$"+id.toString()+"#checked") ? false : (Boolean)request.getAttribute("service$"+id.toString()+"#checked");
		final String checked_service = false == checked ? "" : " checked"; %>
<td><input id="id#service$<%= id.toString() %>#checked" name="service$<%= id.toString() %>#checked" type="checkbox"<%= checked_service %>/></td>
<% 	  //final String service_id = (String)service.get("service-id"); %>
<td><a href="/hci/service?session-id=<%= session_id %>&id=<%= id.toString() %>&cancel=services"><%= null == service_id ? "" : service_id %></a></td>
<% 		final String description = (String)service.get("description"); %>
<td><%= null == description ? "" : description %></td>
<% 		final String state = (String)service.get("$state"); %>
<td><%= null == state ? "" : state %></td>
<td><a href="/hci/service?lines&session-id=<%= session_id %>&id=<%= id.toString() %>&cancel=services">lines</a></td>
<% 		final java.util.List<java.util.Map<String, Object>> /*service_*/history_list = hci.j2ee.Services.getHistoryForServiceId((Integer)service.get("$id")); %>
<td><% if (null != /*service_*/history_list && 0 < /*service_*/history_list.size()) { %><a href="/hci/service?history&session-id=<%= session_id %>&id=<%= id.toString() %>&cancel=services">history</a><% } %></td>
</tr>
<% 	} %>
</table>
<br/>
<a href="/hci/service?session-id=<%= session_id %>&mode=create">create</a>
|
<input id="id#submit" name="__submit" type="submit" value="delete"/>
<% 	if (null != request.getParameter("cancel")) { %>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 	} %>
<% } else { %>
<%@include file='/fragments/services-form-history.jsp' %>
<% } %>