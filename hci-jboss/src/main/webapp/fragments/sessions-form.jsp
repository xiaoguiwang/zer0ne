<!-- fragments/sessions-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
  //final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, java.util.Map<String, Object>> sessions = hci.j2ee.Sessions.sessions;
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
Inquire sessions:
<br/>
<br/>
<form id="id#form-sessions" name="form-sessions" method="POST" action="/hci/sessions?session-id=<%= session_id %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>date/time</td>
</tr>
<% 	for (final String id: sessions.keySet()) {
if (true == id.equals("$id")) continue;
		final java.util.Map<String, Object> __session = sessions.get(id);
		final Integer __id = (Integer)__session.get("$id");
		final Boolean checked = null == request.getAttribute("session$"+__id+"#checked") ? false : (Boolean)request.getAttribute("session$"+__id+"#checked");
		final Long ms = null == __session.get("$date+time") ? 0L : (Long)__session.get("$date+time");
%>
<tr>
<td><%= __id %><input id="id#session-ids" name="session-ids" type="hidden" value="<%= __id %>"/></td>
<% 		final String checked_session = false == checked ? "" : " checked"; %>
<td><input id="id#session$<%= id %>#checked" name="session$<%= id %>#checked" type="checkbox"<%= checked_session %>/></td>
<td><a href="/hci/session?session-id=<%= session_id %>&id=<%= id %>"><%= id %></a></td>
<td><%= ms %></td>
</tr>
<% 	} %>
</table>
<br/>
<% 	if (true == Boolean.TRUE.booleanValue()) { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% 	} %>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
</form>
<% } %>