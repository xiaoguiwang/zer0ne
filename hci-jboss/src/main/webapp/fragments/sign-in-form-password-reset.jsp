<!-- fragments/sign-in-form-password-reset.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<%@page import="java.util.Map" %>
<%
{
	final String sign_id = null == request.getAttribute("sign-id") ? "" : (String)request.getAttribute("sign-id");
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table>
<tr>
<td>sign-id: </td>
<td><input id="id#sign" name="sign-id" type="text" value="<%= sign_id %>"/></td>
</tr>
<tr>
<td></td>
<td><%= null == results || false == results.containsKey("sign-id") ? "&nbsp;" : results.get("sign-id") %></td>
</tr>
<tr>
<td></td>
<td><input id="id#submit" name="__submit" type="submit" value="reset-password"/>
|
<a href="/hci/sign-in?step=10&session-id=<%= session_id %>">cancel</a>
</td>
</tr>
</table>
<% } %>