<!-- fragments/sign-in-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String sign_id = null == request.getAttribute("sign-id") ? "" : (String)request.getAttribute("sign-id");
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final Boolean remember_me = null == request.getAttribute("remember-me") ? false : (Boolean)request.getAttribute("remember-me");
	final String checked_remember_me = false == remember_me ? "" : " checked";
	final String password = null == request.getAttribute("password") ? "" : (String)request.getAttribute("password");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table border=0 align="center" height="100%" width="100%" cellpadding=4 cellspacing=4 >
<tr>
<td width="44%" align="right" nowrap>sign-id: </td>
<td width="12%" align="center" nowrap><input style="position: relative; z-index: 9999" id="id#sign" name="sign-id" type="text" value="<%= sign_id %>"/></td>
<script>
{
	var e = document.getElementById('id#sign');
	if (e !== undefined && e !== null) {
		e.focus();
	}
}
</script>
<td width="44%" align="left">
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("sign-id") ? "&nbsp;" : results.get("sign-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><input id="id#remember-me" name="remember-me" type="checkbox"<%= checked_remember_me %>/> remember me? </td>
<td align="center" nowrap></td>
<td></td>
</tr>
<tr>
<td></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>password: </td>
<td align="center" nowrap><input id="id#password" name="password" type="password" value="<%= password %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("password") ? "&nbsp;" : results.get("password") %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><input id="id#submit" name="__submit" type="submit" value="continue"/>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
</td>
<td></td>
</tr>
<tr>
<td><br/></td>
<td align="center" nowrap>
<input id="id#forgot-password" name="__submit" type="submit" value="forgot password"/>
|
<input id="id#1-time-sign-in" name="__submit" type="submit" value="1-time sign-in"/>
</td>
<td></td>
</tr>
</table>
<% } %>