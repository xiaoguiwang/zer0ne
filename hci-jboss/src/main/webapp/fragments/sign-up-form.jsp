<!-- fragments/sign-up-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String user_id = null == request.getAttribute("user-id") ? "" : (String)request.getAttribute("user-id");
	final String email = null == request.getAttribute("email") ? "" : (String)request.getAttribute("email");
	final String sms = null == request.getAttribute("sms") ? "" : (String)request.getAttribute("sms");
	final String password = null == request.getAttribute("password") ? "" : (String)request.getAttribute("password");
	final String validate = null == request.getAttribute("password-validate") ? "" : (String)request.getAttribute("password-validate");
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table border=0 height="100%" width="100%" align="center">
<tr>
<td width="44%" align="right" nowrap>user#id: </td>
<td width="12%" align="center" nowrap><input id="id#user" type="text" value="<%= user_id %>" disabled/><input name="user-id" type="hidden" value="<%= user_id %>"/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("user-id") ? "&nbsp;" : results.get("user-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>e-mail address: </td>
<td align="center" nowrap><input id="id#email" name="email" type="text" value="<%= email %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("email") ? "&nbsp;" : results.get("email") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>sms-capable phone number: </td>
<td align="center" nowrap><input id="id#sms" name="sms" type="text" value="<%= sms %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("sms") ? "&nbsp;" : results.get("sms") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>password: </td>
<td align="center" nowrap><input id="id#password" name="password" type="password" value="<%= password %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("password") ? "&nbsp;" : results.get("password") %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><input id="id#password-validate" name="password-validate" type="password" value="<%= validate %>"/></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("password-validate") ? "&nbsp;" : results.get("password-validate") %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><input id="id#submit" name="submit" type="submit" value="continue"/>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
</td>
<td></td>
</tr>
</table>
<% } %>