<!-- fragments/signing-in-form-token.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<%
{
	final String sign_id = null == request.getAttribute("sign-id") ? "" : (String)request.getAttribute("sign-id");
	final String token = null == request.getAttribute("token") ? "" : (String)request.getAttribute("token");
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<script>
var counter = 0;
var session_id = "<%= session_id %>";
function __submit() {
	var e = document.getElementById('id#form');
	if (e !== undefined && e !== null) {
		e.submit();
	}
}
function __poller() {
	var httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
//window.alert('server-side:json='+this.responseText);
			var token = JSON.parse(this.responseText);
			if (true == token['confirmed']) {
				var e = document.getElementById('id#token');
				if (e !== undefined && e !== null) {
					e.value = token['token']; // TODO: disable token field/input
				  /*var timeout = */window.setTimeout(__submit, 2000);
				}
			} else {
				counter++;
				if (counter < 90) {
				  /*var timeout = */window.setTimeout(__poller, 2000);
				}
			}
		}
	};
	httpRequest.open('GET', 'http://localhost.itfromblighty:8080/hci/signing-in?json=true&session-id='+session_id, true);
  //httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send();
}
/*var timeout = */window.setTimeout(__poller, 200);
</script>
<table>
<tr>
<td align="right">sign-id: <input name="sign-id" type="hidden" value="<%= sign_id %>"/></td>
<td align="center"><input id="id#sign" type="text" value="<%= sign_id %>" disabled/></td>
</tr>
<tr>
<td></td>
<td align="center"><%= null == results || false == results.containsKey("sign-id") ? "&nbsp;" : results.get("sign-id") %></td>
</tr>
<tr>
<td align="right">token: </td>
<td align="center"><input id="id#token" name="token" type="text" value="<%= token %>"/></td>
</tr>
<tr>
<td></td>
<td align="center"><%= null == results || false == results.containsKey("token") ? "&nbsp;" : results.get("token") %></td>
</tr>
<tr>
<td><input name="token" type="hidden" value=""/></td>
<td align="center"><input id="id#submit" name="__submit" type="submit" value="token"/>
|
<a href="/hci/sign-in?session-id=<%= session_id %>">cancel</a>
</td>
</tr>
</table>
<% } %>