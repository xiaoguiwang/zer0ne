<!-- fragments/text-form-history.jsp -->
<%
final String session_id = (String)request.getAttribute("session-id");
final java.util.Map<String, Object> history = (java.util.Map<String, Object>)request.getAttribute("text#history");
final Integer text_history_id, text_id;
final String from_user_id, to_user_id;
/*final */String message;
final java.util.Set<String> roles, users, groups;
if (null == history) {
	text_history_id = null;
	from_user_id = "";
	text_id = null;
	message = "";
	to_user_id = "";
	roles = null;
	users = null;
	groups = null;
} else {
	text_history_id = null == history.get("text-history-id") ? null : (Integer)history.get("text-history-id");
	from_user_id = null == history.get("from-user-id") ? "" : (String)history.get("from-user-id");
	text_id = (Integer)history.get("text-id");
	final java.util.Map<String, Object> template = hci.j2ee.Texts.texts.get(text_id);
	message = null == template.get("message") ? "" : (String)template.get("message");
	if (0 < message.trim().length()) {
		final java.util.Map<String, Object> properties = (java.util.Map<String, Object>)history.get("properties");
		final dro.util.Properties p = new dro.util.Properties();
		for (final String key: properties.keySet()) {
			p.put(key, properties.get(key)); // BEWARE: only primitives please! (and flat structure)
		}
		message = dro.lang.String.resolve(message, p);
	}
	to_user_id = null == history.get("to-user-id") ? "" : (String)history.get("to-user-id");
	roles = (java.util.Set<String>)history.get("roles");
	users = (java.util.Set<String>)history.get("users");
	groups = (java.util.Set<String>)history.get("groups");
}
final String mode = null == request.getAttribute("text-mode") ? "" : (String)request.getAttribute("text-mode");
final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>text-history-id:</td>
<td width="12%" align="left" nowrap><input id="id#text-history" name="text-history-id" type="text" value="<%= null == text_history_id ? "" : text_history_id.toString() %>" disabled/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("text-history-id") ? "&nbsp;" : results.get("text-history-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>from-user-id: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#from-user-id" name="from-user-id" type="text" value="<%= from_user_id %>"/><% } else { %><%= from_user_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("from-user-id") ? "&nbsp;" : results.get("from-user-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>text-id: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#text-id" name="text-id" type="text" value="<%= null == text_id ? "" : text_id.toString() %>"/><% } else { %><%= text_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("text-id") ? "&nbsp;" : results.get("text-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>message: </td>
<td align="left" nowrap><textarea id="id#message" __name="message" cols="40" rows="12" disabled><%= message %></textarea>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("message") ? "&nbsp;" : results.get("message") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>to-user-id: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#to-user-id" name="to-user-id" type="text" value="<%= to_user_id %>"/><% } else { %><%= to_user_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("to-user-id") ? "&nbsp;" : results.get("to-user-id") %></td>
<td></td>
</tr>
<% if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align="right" nowrap>roles: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>users: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>groups: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<% } else if (false == Boolean.TRUE.booleanValue()) { // disable until we need or know we don't need, this: %>
<tr>
<td align="right" nowrap><a href="/hci/roles?session-id=<%= session_id %>&text-history-id=<%= history.get("text-history-id").toString() %>">roles</a>: </td>
<td align="left" nowrap>
<% if (null != roles) { %>
<% 	for (final String role_id: roles) { %>
<% 		final java.util.Map<String, Object> role = (java.util.Map<String, Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/users?session-id=<%= session_id %>&text-history-id=<%= history.get("text-history-id").toString() %>">users</a>: </td>
<td align="left" nowrap>
<% if (null != users) { %>
<% 	for (final String user_id: users) { %>
<% 		final java.util.Map<String, Object> user = (java.util.Map<String, Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= session_id %>&user-id=<%= user_id %>"><%= (String)user.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/groups?session-id=<%= session_id %>&text-history-id=<%= history.get("text-history-id").toString() %>">groups</a>: </td>
<td align="left" nowrap>
<% if (null != groups) { %>
<% 	for (final String group_id: groups) { %>
<% 		final java.util.Map<String, Object> group = (java.util.Map<String, Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= session_id %>&group-id=<%= group_id %>"><%= (String)group.get("group-id") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
<% } // if (true == mode.equals("create")) %>
</tr>
</table>
<br/>
<% if (null == history) { %>
<% 	if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-text-history-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 	} %>
<% } else if (false == mode.equals("send")) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="send"/>
<% } %>
|
<a href="/hci/texts?history&session-id=<%= session_id %>">cancel</a>