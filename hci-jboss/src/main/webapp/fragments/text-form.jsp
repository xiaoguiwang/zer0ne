<!-- fragments/text-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% if (null == request.getAttribute("texts#history") && null == request.getAttribute("to:user#ids") && null == request.getAttribute("from:user#id") && null == request.getAttribute("text:unresolve")) {
final String session_id = (String)request.getAttribute("session-id");
final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
final java.util.Map<String, Object> text = (java.util.Map<String, Object>)request.getAttribute("text");
final Integer text_id;
final String from_user_id, message;
final String to_user_ids;
final java.util.Set<String> roles, users, groups;
if (null == text) {
	text_id = null;
	from_user_id = "";
	message = "";
	to_user_ids = "";
	roles = null;
	users = null;
	groups = null;
} else {
	text_id = (Integer)text.get("text-id");
	from_user_id = null == ServletTool.get(__session, "from:user#id") ? "" : (String)ServletTool.get(__session, "from:user#id");
	message = null == text.get("message") ? "" : (String)text.get("message");
	final java.util.Map<String, String[]> unresolve = (java.util.Map<String, String[]>)ServletTool.get(__session, "text:unresolve");
  /*final */StringBuffer sb = null;
	final String[] ids = (String[])ServletTool.get(__session, "to:user#ids");
	if (null != ids) {
		for (int i = 0; i < ids.length; i++) {
			if (null == sb) {
				sb = new StringBuffer();
			} else {
				sb.append(",");
			}
			final boolean unresolved = null != unresolve && null != unresolve.get(ids[i]);
			if (true == unresolved) {
				sb.append("<del>");
			}
			sb.append(ids[i]);
			if (true == unresolved) {
				sb.append("</del>");
			}
		}
	}
	to_user_ids = null == sb ? "" : sb.toString();
	roles = (java.util.Set<String>)text.get("roles");
	users = (java.util.Set<String>)text.get("users");
	groups = (java.util.Set<String>)text.get("groups");
}
final String mode = null == request.getAttribute("text-mode") ? "" : (String)request.getAttribute("text-mode");
final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>text-id: <input id="id#text" name="text-id" type="hidden" value="<%= null == text_id ? "" : text_id.toString() %>"/></td>
<td width="12%" align="left" nowrap><input id="id#text" __name="text-id" type="text" value="<%= null == text_id ? "" : text_id.toString() %>" disabled/></td>
<td width="44%" align="left"></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("text-id") ? "&nbsp;" : results.get("text-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/text?from-user-id&session-id=<%= session_id %>&text-id=<%= text.get("text-id").toString() %>">from:user#id</a>: </td>
<td align="left" nowrap><% if (true == mode.equals("send")) { %><input id="id#from-user-id" name="from-user-id" type="text" value="<%= from_user_id %>"/><% } else { %><%= from_user_id %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("from-user-id") ? "&nbsp;" : results.get("from-user-id") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>message: </td>
<td align="left" nowrap><textarea id="id#message" name="message" cols="40" rows="12"><%= message %></textarea></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("message") ? "&nbsp;" : results.get("message") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/text?to-user-ids&session-id=<%= session_id %>&text-id=<%= text.get("text-id").toString() %>">to:user#ids</a>: </td>
<td align="left" nowrap><% if (null == to_user_ids) { %><input id="id#to-user-ids" name="to-user-ids" type="text" value=""/><% } else { %><%= to_user_ids %><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="left" nowrap><%= null == results || false == results.containsKey("to-user-ids") ? "&nbsp;" : results.get("to-user-ids") %></td>
<td></td>
</tr>
<% if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/user/role %>
<tr>
<td align="right" nowrap>roles: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>users: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>groups: </td>
<td align="left" nowrap></td>
<td></td>
</tr>
<% } else { %>
<tr>
<td align="right" nowrap><a href="/hci/roles?session-id=<%= session_id %>&text-id=<%= text.get("text-id").toString() %>">roles</a>: </td>
<td align="left" nowrap>
<% if (null != roles) { %>
<% 	for (final String role_id: roles) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/users?session-id=<%= session_id %>&text-id=<%= text.get("text-id").toString() %>">users</a>: </td>
<td align="left" nowrap>
<% if (null != users) { %>
<% 	for (final String user_id: users) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> user = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Users.users.get(user_id); %>
<a href="/hci/user?session-id=<%= session_id %>&user-id=<%= user_id %>"><%= (String)user.get("name") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/groups?session-id=<%= session_id %>&text-id=<%= text.get("text-id").toString() %>">groups</a>: </td>
<td align="left" nowrap>
<% if (null != groups) { %>
<% 	for (final String group_id: groups) { %>
<% 		final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= session_id %>&group-id=<%= group_id %>"><%= (String)group.get("group-id") %></a><br/>
<% 	} %>
<% } %>
</td>
<td></td>
<% } // if (true == mode.equals("create")) %>
</tr>
</table>
<br/>
<% if (null == text) { %>
<% 	if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-text-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 	} %>
<% } else if (false == mode.equals("send")) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } %>
<% if (null != to_user_ids && 0 < to_user_ids.trim().length()) { %>
|
<input id="id#submit" name="submit" type="submit" value="send"/>
<% } %>
|
<a href="/hci/texts?session-id=<%= session_id %>">cancel</a>
<% } else if (null != request.getAttribute("to:user#ids") || null != request.getAttribute("from:user#id")) { %>
<%@include file='/fragments/users-form.jsp' %>
<% } else if (null != request.getAttribute("text:unresolve")) { %>
<%@include file='/fragments/text-form-unresolve.jsp' %>
<% } else { %>
<%@include file='/fragments/text-form-history.jsp' %>
<% } %>