<!-- fragments/texts-form-history.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<%
final String session_id = (String)request.getAttribute("session-id");
final java.util.List<java.util.Map<String, Object>> history = (java.util.List<java.util.Map<String, Object>>)request.getAttribute("texts#history");
final String __role_id = (String)request.getAttribute("role-id");
final String qs_role_id = null == __role_id ? "" : "&role-id="+__role_id;
final String __user_id = (String)request.getAttribute("user-id");
final String qs_user_id = null == __user_id ? "" : "&user-id="+__user_id;
final String __group_id = (String)request.getAttribute("group-id");
final String qs_group_id = null == __group_id ? "" : "&group-id="+__group_id;
final Integer __bill_id = (Integer)request.getAttribute("bill-id");
final String __bill_key = null != request.getAttribute("from") ? "from" : null != request.getAttribute("to") ? "to" : null; 
final String qs_bill = null == __bill_id ? "" : "&bill-id="+__bill_id.toString()+"&"+__bill_key;
final String __history = (String)request.getAttribute("history");
final String qs_history = null == __history ? "" : "&history";
final String __text_id = (String)request.getAttribute("text-id");
final String qs_text_id = null == __text_id ? "" : "&text-id="+__text_id;
final String mode = null == request.getAttribute("text-mode") ? "" : (String)request.getAttribute("text-mode");
//final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% } else if (null != __role_id) { %>
Update role texts:
<% } else if (null != __user_id) { %>
Update user texts:
<% } else if (null != __group_id) { %>
Update group texts:
<% } else if (null != __text_id) { %>
Update text#<%= __text_id %>:
<% } else { %>
Update texts:
<% } %>
<br/>
<br/>
<form id="id#form-texts" name="form-texts" method="POST" action="/hci/texts?history&session-id=<%= session_id %><%= qs_role_id %><%= qs_user_id %><%= qs_group_id %><%= qs_bill %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>text-id</td>
<td>from-user-id</td>
<td>message</td>
<td>to-user-id</td>
</tr>
<% for (final java.util.Map<String, Object> text: history) {
	final Integer text_history_id = (Integer)text.get("text-history-id");
	final Integer id = (Integer)text.get("$id");
	final Boolean checked = null == request.getAttribute("text$"+id+"#checked") ? false : (Boolean)request.getAttribute("text$"+id+"#checked");
	final String from_user_id = null == text.get("from-user-id") ? "" : (String)text.get("from-user-id");
	final Integer text_id = (Integer)text.get("text-id");
	final java.util.Map<String, Object> template = hci.j2ee.Texts.texts.get(text_id);
  /*final */String message = null == template.get("message") ? "" : (String)template.get("message");
	if (0 < message.trim().length()) {
		final java.util.Map<String, Object> properties = (java.util.Map<String, Object>)text.get("properties");
		final dro.util.Properties p = new dro.util.Properties();
		for (final String key: properties.keySet()) {
			p.put(key, properties.get(key)); // BEWARE: only primitives please! (and flat structure)
		}
		message = dro.lang.String.resolve(message, p);
		message = message.replaceAll("\r\n", ", ").replaceAll(",,", ",").replaceAll(", ,", ",");
	}
	final int length = message.length();
	message = message.substring(0, Math.min(length, 61));
	if (length > 61-1) {
		message = message+".."; // TODO: get this into a ServletTool thingy
	}
	final String to_user_id = null == text.get("to-user-id") ? "" : (String)text.get("to-user-id");
%>
<tr>
<td><%= id %><input id="id#text-ids" name="text-ids" type="hidden" value="<%= id %>"/></td>
<% final String checked_text = false == checked ? "" : " checked"; %>
<td><input id="id#text$<%= id %>#checked" name="text$<%= id %>#checked" type="checkbox"<%= checked_text %>/></td>
<td><a href="/hci/text?history&session-id=<%= session_id %>&text-id=<%= text_history_id %>"><%= text_history_id %></a></td>
<td><%= from_user_id %></td>
<td><%= message %></td>
<td><%= to_user_id %></td>
</tr>
<% } %>
</table>
<br/>
<% if (null != __role_id || null != __user_id || null != __group_id || null != __bill_id || null != __text_id) { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% } else { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% } %>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
</form>