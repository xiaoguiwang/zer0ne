<!-- fragments/tokens-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, java.util.Map<String, Object>> tokens = hci.j2ee.Tokens.tokens;
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
Inquire tokens:
<br/>
<br/>
<form id="id#form-tokens" name="form-tokens" method="POST" action="/hci/tokens?session-id=<%= session_id %>">
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>date/time</td>
</tr>
<% 	for (final String token_id: tokens.keySet()) {
		if (true == token_id.equals("$id")) continue;
		final java.util.Map<String, Object> token = tokens.get(token_id);
		final Integer id = (Integer)token.get("$id");
		final Boolean checked = null == request.getAttribute("token$"+id+"#checked") ? false : (Boolean)request.getAttribute("token$"+id+"#checked");
		final String type = null == token.get("$type") ? "" : (String)token.get("$type");
		final Long ms = null == token.get("$date+time") ? 0L : (Long)token.get("$date+time"); // System.currentTimeMillis();
		final Boolean confirmed = null == token.get("$confirmed") ? null : (Boolean)token.get("$confirmed");
%>
<tr>
<td><%= id %><input id="id#token-ids" name="token-ids" type="hidden" value="<%= id %>"/></td>
<% 		final String checked_token = false == checked ? "" : " checked"; %>
<td><input id="id#token$<%= id %>#checked" name="token$<%= id %>#checked" type="checkbox"<%= checked_token %>/></td>
<td><a href="/hci/token?session-id=<%= session_id %>&token-id=<%= token_id %>"><%= token_id %></a></td>
<td><%= ms %></td>
</tr>
<% 	} %>
</table>
<br/>
<% 	if (true == Boolean.TRUE.booleanValue()) { %>
<input id="id#submit" name="submit" type="submit" value="delete"/>
<% 	} %>
|
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
</form>
<% } %>