<!-- fragments/user-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<% {
	final java.util.Map<String, Object> signed_in_user = (java.util.Map<String, Object>)request.getAttribute("signed-in-user");
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
	final java.util.Map<String, Object> user = (java.util.Map<String, Object>)request.getAttribute("user");
	final String user_id, preferred_name, email, checked_confirmed_email, sms, checked_confirmed_sms, password, validate, passphrase;
	final Boolean password_reset = null != request.getAttribute("password-reset");
	final String token = null == request.getAttribute("token") ? "" : (String)request.getAttribute("token");
	final String disabled_user_id = false == password_reset ? "" : " disabled";
	final String disabled_preferred_name = false == password_reset ? "" : " disabled";
	final String disabled_email = false == password_reset ? "" : " disabled";
	final String disabled_confirmed_email = false == password_reset ? "" : " disabled";
	final String disabled_sms = false == password_reset ? "" : " disabled";
	final String disabled_confirmed_sms = false == password_reset ? "" : " disabled";
	final Boolean confirmed_email, confirmed_sms;
	final java.util.Set<String> permissions, roles, groups;
	if (null == user) {
		user_id = "";
		preferred_name = "";
		email = "";
		confirmed_email = false;
		sms = "";
		confirmed_sms = false;
		password = "";
		validate = "";
		passphrase = "";
		permissions = null;
		roles = null;
		groups = null;
	} else {
		user_id = (String)request.getAttribute("user-id");
		preferred_name = null == request.getAttribute("preferred-name") ? "" : (String)request.getAttribute("preferred-name");
		email = null == user.get("email") ? "" : (String)user.get("email");
		confirmed_email = null == user.get("confirmed-email") ? false : (Boolean)user.get("confirmed-email");
		sms = null == user.get("sms") ? "" : (String)user.get("sms");
		confirmed_sms = null == user.get("confirmed-sms") ? false : (Boolean)user.get("confirmed-sms");
		password = null != request.getAttribute("password") ? (String)request.getAttribute("password") : true == password_reset ? "" : null == user.get("password") ? "" : (String)user.get("password");
		validate = null != request.getAttribute("password-validate") ? (String)request.getAttribute("password-validate") : "";
		passphrase = null != request.getAttribute("passphrase") ? (String)request.getAttribute("passphrase") : null == user.get("passphrase") ? "" : (String)user.get("passphrase");
		permissions = (java.util.Set<String>)user.get("permissions");
		roles = (java.util.Set<String>)user.get("roles");
		groups = (java.util.Set<String>)user.get("groups");
	}
  /*final String */checked_confirmed_email = null == confirmed_email || false == confirmed_email ? "" : " checked";
  /*final String */checked_confirmed_sms = null == confirmed_sms || false == confirmed_sms ? "" : " checked";
	final String mode = null == request.getAttribute("mode") ? "" : (String)request.getAttribute("mode");
	final java.util.Map<String, String> results = null == request.getAttribute("results") ? null : (java.util.Map<String, String>)request.getAttribute("results");
%>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td width="44%" align="right" nowrap>user-id: </td>
<td width="12%" align="center" nowrap><input id="id#user" name="user-id" type="text" value="<%= user_id %>"<%= disabled_user_id %>/></td>
<td width="2%" align="center"></td>
<td width="42%" align="left"></td>
</tr>
<% 	if (false == password_reset) { %>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("user-id") ? "&nbsp;" : results.get("user-id") %></td>
<td></td>
<td></td>
</tr>
<% 	} else { %>
<tr><td></td><td>&nbsp;</td><td></td></tr>
<% 	} %>
<tr>
<td align="right" nowrap>preferred-name: </td>
<td align="center" nowrap><input id="id#preferred-name" name="preferred-name" type="text" value="<%= preferred_name %>"<%= disabled_preferred_name %>/></td>
<td></td>
<td></td>
</tr>
<% 	if (false == password_reset) { %>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("preferred-name") ? "&nbsp;" : results.get("preferred-name") %></td>
<td></td>
<td></td>
</tr>
<% 	} else { %>
<tr><td></td><td>&nbsp;</td><td></td></tr>
<% 	} %>
<tr>
<td align="right" nowrap><% if (false == password_reset) { %><a href="/hci/user?reconfirm-email&session-id=<%= session_id %>">re/confirm</a> <% } %>e-mail: </td>
<td align="center" nowrap><input id="id#email" name="email" type="text" value="<%= email %>"<%= disabled_email %>/></td>
<td align="center"><% if (false == password_reset) { %><input id="id#reset-password:email" name="reset-password" type="radio" value="email" checked/><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("email") ? "&nbsp;" : results.get("email") %></td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>confirmed: </td>
<td align="center" nowrap><input id="id#confirmed-email" name="confirmed-email" type="checkbox"<%= checked_confirmed_email %><%= disabled_confirmed_email %>/></td>
<td align="center"><% if (false == password_reset) { %>|<% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("confirmed-email") ? "&nbsp;" : results.get("confirmed-email") %></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><% if (false == password_reset) { %><a href="/hci/user?reconfirm-sms&session-id=<%= session_id %>">re/confirm</a> <% } %>sms: </td>
<td align="center" nowrap><input id="id#sms" name="sms" type="text" value="<%= sms %>"<%= disabled_sms %>/></td>
<td align="center"><% if (false == password_reset) { %><input id="id#reset-password:sms" name="reset-password" type="radio" value="sms"/><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("sms") ? "&nbsp;" : results.get("sms") %></td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>confirmed: </td>
<td align="center" nowrap><input id="id#confirmed-sms" name="confirmed-sms" type="checkbox"<%= checked_confirmed_sms %><%= disabled_confirmed_sms %>/></td>
<td align="center"><% if (false == password_reset) { %>|<% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("confirmed-sms") ? "&nbsp;" : results.get("confirmed-sms") %></td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><input id="id#token" name="token" type="hidden" value="<%= token %>"/><input id="id#submit" name="submit" type="submit" value="reset"/> password: </td>
<td align="center" nowrap>
<% 	if ((true == mode.equals("create")) || (null != user && true == ((String)user.get("user-id")).equals(user_id)) || (true == password_reset) || (false == Boolean.TRUE.booleanValue()/*TODO: admin permission*/)) { %>
<input id="id#password" name="password" type="password" value="<%= password %>"/>
<% 	} else { %>
&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;
<% 	} %>
</td>
<td align="center"><% if (false == password_reset) { %>&bull;<% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("password") ? "&nbsp;" : results.get("password") %></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><input id="id#password-validate" name="password-validate" type="password" value="<%= validate %>"/></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("password-validate") ? "&nbsp;" : results.get("password-validate") %></td>
<td></td>
<td></td>
</tr>
<% 	if (false == password_reset) { %>
<tr>
<td align="right" nowrap>passphrase: </td>
<td align="center" nowrap>
<input id="id#passphrase" name="passphrase" type="text" value="<%= passphrase %>"/>
</td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td align="center" nowrap><%= null == results || false == results.containsKey("passphrase") ? "&nbsp;" : results.get("passphrase") %></td>
<td></td>
<td></td>
</tr>
<% 	} %>
<% 	if (true == password_reset) { %>
<% 	} else if (true == mode.equals("create")) { // TODO: enable this with a temporary permission so we can build it up as we go instead of create first then permission/role/group %>
<tr>
<td align="right" nowrap>permissions: </td>
<td align="center" nowrap></td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>roles: </td>
<td align="center" nowrap></td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap>groups: </td>
<td align="center" nowrap></td>
<td></td>
<td></td>
</tr>
<% 	} else if (null != user) { %>
<tr>
<td align="right" nowrap><a href="/hci/permissions?session-id=<%= session_id %>&user-id=<%= (String)user.get("user-id") %>">permissions</a>: </td>
<td align="center" nowrap>
<% 		if (null != permissions) { %>
<% 			for (final String permission_id: permissions) { %>
<% 				final java.util.Map<java.lang.String, java.lang.Object> permission = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Permissions.permissions.get(permission_id); %>
<a href="/hci/permission?session-id=<%= (String)request.getAttribute("session-id") %>&permission-id=<%= permission_id %>"><%= (String)permission.get("name") %></a><br/>
<% 			} %>
<% 		} %>
</td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/roles?session-id=<%= (String)request.getAttribute("session-id") %>&user-id=<%= (String)user.get("user-id") %>">roles</a>: </td>
<td align="center" nowrap>
<% 		if (null != roles) { %>
<% 			for (final String role_id: roles) { %>
<% 				final java.util.Map<java.lang.String, java.lang.Object> role = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Roles.roles.get(role_id); %>
<a href="/hci/role?session-id=<%= (String)request.getAttribute("session-id") %>&role-id=<%= role_id %>"><%= (String)role.get("name") %></a><br/>
<% 			} %>
<% 		} %>
</td>
<td></td>
<td></td>
</tr>
<tr>
<td align="right" nowrap><a href="/hci/groups?session-id=<%= (String)request.getAttribute("session-id") %>&user-id=<%= (String)user.get("user-id") %>">groups</a>: </td>
<td align="center" nowrap>
<% 		if (null != groups) { %>
<% 			for (final String group_id: groups) { %>
<% 				final java.util.Map<java.lang.String, java.lang.Object> group = (java.util.Map<java.lang.String, java.lang.Object>)hci.j2ee.Groups.groups.get(group_id); %>
<a href="/hci/group?session-id=<%= (String)request.getAttribute("session-id") %>&group-id=<%= group_id %>"><%= (String)group.get("name") %></a><br/>
<% 			} %>
<% 		} %>
</td>
<td></td>
<td></td>
</tr>
<% 	} // if (true == password_reset) { %>
<% 	if (true == Boolean.TRUE.booleanValue()) {
		final String font_family = null == request.getParameter("font-family") ? "Lucida Console, monospace" : request.getParameter("font-family");
		final String font_size   = null == request.getParameter("font-size"  ) ? "20px" : request.getParameter("font-size");
		final String font_weight = null == request.getParameter("font-weight") ? "bold" : request.getParameter("font-weight"); %>
<% 		if (true == "payment".equals(mode)) {
		  //final java.util.Map<String, Object> user = hci.j2ee.Users.users.get("admin");
			final java.util.Map<String, Object> card = null == user ? null : null == user.get("card") ? null : (java.util.Map<String, Object>)user.get("card"); %>
<% 			if (true == Boolean.TRUE.booleanValue()) { %>
<div id="form-container">
<tr>
<td><input id='id#sq:nonce' name='sq:nonce' type='hidden' value=''/></td>
<td>&nbsp;</td>
<td></td>
</tr>
<tr valign='top'>
<% 				final String applicationId = "sandbox-sq0idb-89JrHkt1aZ21YOrp_s24Lw"; %>
<!-- https://developer.squareup.com/docs/payment-form/what-it-does -->
<!-- https://developer.squareup.com/docs/payment-form/payment-form-walkthrough -->
<script type="text/javascript" src="https://js.squareupsandbox.com/v2/paymentform"></script><!-- TODO: variable, for prod to be non-sandbox -->
<link rel="stylesheet" type="text/css" href="/hci/css/sqpaymentform.css?<%= ServletTool.getVersion() %>">
<script type="text/javascript">
<!-- https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid -->
function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}
const idempotency_key = uuidv4();
const paymentForm = new SqPaymentForm({
	applicationId: '<%= applicationId %>',
	inputClass: 'sq-input',
	autoBuild: false,
	inputStyles: [{
		fontFamily: '<%= font_family %>',
		fontSize: '<%= font_size %>',
		fontWeight: '<%= font_weight %>',
	  //lineHeight: '24px',
		padding: '1px',
	  //placeholderColor: '#a0a0a0',
	  //backgroundColor: 'transparent',
	}],
	cardNumber: {
		elementId: 'sq-card-number',
	  //placeholder: 'Card Number'
	},
	cvv: {
		elementId: 'sq-cvv',
	  //placeholder: 'CVV'
	},
	expirationDate: {
		elementId: 'sq-expiration-date',
	  //placeholder: 'MM/YY'
	},
	// https://developer.squareup.com/docs/payment-form/cookbook/sqpaymentform-customization
	// "For processing credit cards for a seller in the United States (US), Canada (CA), and the United Kingdom (GB), the postal code (postalCode) is a required payment field. For sellers taking payments in Japan, you can remove the postal code requirement by setting the postalCode configuration field to false"
	// https://developer.squareup.com/docs/payment-form/cookbook/customize-form-styles
	// 
	postalCode: {
		elementId: 'sq-postal-code',
	  //placeholder: 'Postal'
	},
	callbacks: {
		cardNonceResponseReceived: function (errors, nonce, cardData) {
			if (errors) {
				console.error('Encountered errors:');
				errors.forEach(function (error) {
					console.error('  ' + error.message);
				});
				alert('Encountered errors, check browser developer console for more details');
				return;
			}
			var e = document.getElementById('id#sq:nonce');
			if (e !== undefined && e !== null) {
				e.value = nonce;
			}
			var f = document.getElementById('id#form-user');
			if (f !== undefined && f !== null) {
				f.submit();
			}
		}
	}
});
paymentForm.build();
function onGetCardNonce(event) {
	event.preventDefault();
	paymentForm.requestCardNonce();
}
</script>
<% 			} %>
<% 			{
				final String key = "user:card-number";
				final String label = "card-number";
				final String value = null == card ? null : null == card.get("number") ? null : (String)card.get("number"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=20 maxlength=19 value='<%= null == value ? "" : value %>'/><% } else { %><div id="sq-card-number"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<tr valign='top'>
<% 			{
				final String key = "user:card-expiry";
				final String label = "card-expiry";
				final String value = null == card ? null : null == card.get("expiry") ? null : (String)card.get("expiry"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=6 maxlength=5 value='<%= null == value ? "" : value %>'/><% } else { %><div class="third" id="sq-expiration-date"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<tr valign='top'>
<% 			{
				final String key = "user:card-security";
				final String label = "card-security";
				final String value = null == card ? null : null == card.get("security") ? null : (String)card.get("security"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=6 maxlength=3 value='<%= null == value ? "" : value %>'/><% } else { %><div class="third" id="sq-cvv"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<tr valign='top'>
<% 			{
				final String key = "user:postal";
				final String label = "postal";
				final String value = null == card ? null : null == card.get("postal") ? null : (String)card.get("postal"); %>
<td align='right' nowrap><%= label %>: </td>
<td align='left' nowrap><% if (false == Boolean.TRUE.booleanValue()) { %><input id='id#<%= key %>' name='<%= key %>' type='text' size=8 maxlength=8 value='<%= null == value ? "" : value %>'/><% } else { %><div class="third" id="sq-postal-code"></div><% } %></td>
<td></td>
</tr>
<tr>
<td></td>
<td align='left' nowrap><%= null == results || false == results.containsKey(key) ? "&nbsp;" : results.get(key) %></td>
<td></td>
<% 			} %>
</tr>
<% 		}
	}
} %>
</table>