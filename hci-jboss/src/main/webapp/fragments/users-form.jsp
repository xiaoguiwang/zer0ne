<!-- fragments/users-form.jsp -->
<%@page import="hci.j2ee.ServletTool" %>
<%@page import="hci.j2ee.Users"%>
<% {
	final String session_id = (String)request.getAttribute("session-id");
	final java.util.Map<String, Object> __session = ServletTool.getSessionById(session_id);
	final java.util.Map<String, java.util.Map<String, Object>> users = (java.util.Map<String, java.util.Map<String, Object>>)request.getAttribute("users");
	final String __permission_id = (String)request.getAttribute("permission-id");
	final String qs_permission_id = null == __permission_id ? "" : "&permission-id="+(String)request.getAttribute("permission-id");
	final String __role_id = (String)request.getAttribute("role-id");
	final String qs_role_id = null == __role_id ? "" : "&role-id="+(String)request.getAttribute("role-id");
	final String __group_id = (String)request.getAttribute("group-id");
	final String qs_group_id = null == __group_id ? "" : "&group-id="+(String)request.getAttribute("group-id");
  //final Map<String, String> results = null == request.getAttribute("results") ? null : (Map<String, String>)request.getAttribute("results");
%>
<% 	if (false == java.lang.Boolean.TRUE.booleanValue()) { %>
<% 	} else if (null != request.getAttribute("permission-id")) { %>
Update permission of users:
<% 	} else if (null != request.getAttribute("role-id")) { %>
Update role for users:
<% 	} else if (null != request.getAttribute("group-id")) { %>
Update group for users:
<% 	} else if (null == request.getAttribute("to:user#ids") && null == request.getAttribute("from:user#id") && null == request.getAttribute("for:user#id")) { %>
Update users:
<% 	} else { %>
Select users:
<% 	} %>
<br/>
<br/>
<% 	if (null != request.getParameter("history")) { %><input name='history' type='hidden'/><% } %>
<% 	if (null != request.getAttribute("service") && null != request.getParameter("id")) { final String service_id = (String)request.getParameter("id"); %><input name='service-id' type='hidden' value='<%= service_id %>'/><% } %>
<% 	if (null != request.getAttribute("bill") && null != request.getParameter("id")) { final Integer bill_id = new Integer(request.getParameter("id")); %><input name='bill-id' type='hidden' value='<%= bill_id %>'/><% } %>
<table border=0 cellpadding=4 cellspacing=4>
<tr>
<td>$id</td>
<td></td>
<td>user-id</td>
<td>preferred-name</td>
<td>e-mail</td>
<td>confirmed</td>
<td>sms</td>
<td>confirmed</td>
<td>password</td>
<td>passphrase</td>
<td>sign-in date+time</td>
<td>sign-in attempts failed</td>
</tr>
<% 	{
		final String search_user_id = ServletTool.getParameterOrAttribute(request, "user-id", "");
		final String search_preferred_name = ServletTool.getParameterOrAttribute(request, "preferred-name", "");
		final String search_email = ServletTool.getParameterOrAttribute(request, "email", "");
		final Boolean search_confirmed_email = ServletTool.getParameterOrAttribute(request, "confirmed-email", (Boolean)null);
		final String checked_search_confirmed_email = null == search_confirmed_email || false == search_confirmed_email ? "" : " checked";
		final Boolean force_confirmed_email = ServletTool.getParameterOrAttribute(request, "force:confirmed-email", false);
		final String disabled_confirmed_email = true == force_confirmed_email ? " disabled" : "";
		final String search_sms = ServletTool.getParameterOrAttribute(request, "sms", "");
		final Boolean search_confirmed_sms = ServletTool.getParameterOrAttribute(request, "confirmed-sms", (Boolean)null);
		final String checked_search_confirmed_sms = null == search_confirmed_sms || false == search_confirmed_sms ? "" : " checked";
		final Boolean force_confirmed_sms = ServletTool.getParameterOrAttribute(request, "force:confirmed-sms", false);
		final String disabled_confirmed_sms = true == force_confirmed_sms ?" disabled" : "";
		final String search_passphrase = ServletTool.getParameterOrAttribute(request, "passphrase", "");
%>
<tr>
<td></td>
<td></td>
<td><input id="id#user-id" name="user-id" type="text" value="<%= search_user_id %>"/></td>
<td><input id="id#preferred-name" name="preferred-name" type="text" value="<%= search_preferred_name %>"/></td>
<td><input id="id#email" name="email" type="text" value="<%= search_email %>"/></td>
<td><input id="id#confirmed-email" name="confirmed-email" type="checkbox"<%= checked_search_confirmed_email %><%= disabled_confirmed_email %>/></td>
<td><input id="id#sms" name="sms" type="text" value="<%= search_sms %>"/></td>
<td><input id="id#confirmed-sms" name="confirmed-sms" type="checkbox"<%= checked_search_confirmed_sms %><%= disabled_confirmed_sms %>/></td>
<td></td>
<td><input id="id#passphrase" name="passphrase" type="text"<%= search_passphrase %>/></td>
<td></td>
<td></td>
</tr>
<% 		final java.util.Map<String, Object> self = (java.util.Map<String, Object>)__session.get("user"); %>
<% 		final String self_id = null == self || null == self.get("user-id") ? "" : (String)self.get("user-id"); %>
<% 	  /*final */int count = 0; %>
<% 		for (final String user_id: users.keySet()) {
if (true == user_id.equals("$id")) continue;
			final java.util.Map<String, Object> user = users.get(user_id);
			final Integer id = (Integer)user.get("$id");
			final Boolean checked = null == request.getAttribute("user$"+id+"#checked") ? false : (Boolean)request.getAttribute("user$"+id+"#checked");
			final boolean enabled = true;// == self_id.equals(user_id) || true == Users.hasPermission(self, "any-user-update");
		  //if (true == enabled) {
				count++;
		  //}
			final String disabled = false == enabled ? " disabled" : "";
		  //final String user_id = null == user.get("user-id") ? "" : (String)user.get("user-id");
			if (null != search_user_id && 0 < search_user_id.trim().length() && false == user_id.contains(search_user_id)) continue;
			final String preferred_name = null == user.get("preferred-name") ? "" : (String)user.get("preferred-name");
			if (null != search_preferred_name && 0 < search_preferred_name.trim().length() && false == preferred_name.contains(search_preferred_name)) continue;
			final String email = null == user.get("email") ? "" : (String)user.get("email");
			if (null != search_email && 0 < search_email.trim().length() && false == email.contains(search_email)) continue;
			final Boolean __confirmed_email = null == user.get("confirmed-email") ? null : (Boolean)user.get("confirmed-email");
			final String confirmed_email = null == __confirmed_email ? "N/A" : __confirmed_email.toString();
			if (null != search_confirmed_email && false == search_confirmed_email.equals(__confirmed_email)) continue;
			final String sms = null == user.get("sms") ? "" : (String)user.get("sms");
			if (null != search_sms && 0 < search_sms.trim().length() && false == sms.contains(search_sms)) continue;
			final Boolean __confirmed_sms = null == user.get("confirmed-sms") ? null : (Boolean)user.get("confirmed-sms");
			final String confirmed_sms = null == __confirmed_sms ? "N/A" : __confirmed_sms.toString();
			if (null != search_confirmed_sms && false == search_confirmed_sms.equals(__confirmed_sms)) continue;
			final String password = (String)user.get("password");
			final String passphrase = null == user.get("passphrase") ? "{undefined}" : (String)user.get("passphrase");
			if (null != search_passphrase && 0 < search_passphrase.trim().length() && false == passphrase.contains(search_passphrase)) continue;
			final String sign_in_this_date_time = null == user.get("sign-in-this-date+time") ? "{never}" : (String)user.get("sign-in-this-date+time");
			final Integer sign_in_attempts_failed = null == user.get("sign-in-attempts-failed") ? 0 : (Integer)user.get("sign-in-attempts-failed");
%>
<tr>
<td><%= id %><input id="id#user-ids" name="user-ids" type="hidden" value="<%= id %>"/></td>
<% 			final String checked_user = false == checked ? "" : " checked"; %>
<td><input id="id#user$<%= id %>#checked" name="user$<%= id %>#checked" type="checkbox"<%= checked_user %><%= disabled %>/></td>
<td><a href="/hci/user?session-id=<%= session_id %>&user-id=<%= user_id %>"><%= user_id %></a></td>
<td><%= preferred_name %></td>
<td><%= email %></td>
<td><%= confirmed_email %></td>
<td><%= sms %></td>
<td><%= confirmed_sms %></td>
<td>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</td>
<td><%= passphrase %></td>
<td><%= sign_in_this_date_time %></td>
<td><%= sign_in_attempts_failed.toString() %></td>
</tr>
<% 		} %>
</table>
<br/>
<% 		if (null != __permission_id || null != __role_id || null != __group_id) { %>
<input id="id#submit" name="__submit" type="submit" value="update"<%= 0 == count ? " disabled" : "" %>/>
<% 		} else if (null == request.getAttribute("to:user#ids") && null == request.getAttribute("from:user#id") && null == request.getAttribute("for:user#id")) { %>
<input id="id#submit" name="__submit" type="submit" value="delete"<%= 0 == count ? " disabled" : "" %>/>
<% 		} else { %>
<input id="id#submit" name="__submit" type="submit" value="search"<%= 0 == count ? " disabled" : "" %>/>
|
<input id="id#submit" name="__submit" type="submit" value="select"<%= 0 == count ? " disabled" : "" %>/>
<% 		} %>
|
<% 		if ("service".equals(request.getAttribute("cancel"))) {
			final String id = request.getParameter("id");
			final String mode = request.getParameter("mode");
			if (null != request.getParameter("service")) { %>
<a href="/hci/service?history&session-id=<%= session_id %>&id=<%= id %>&mode=<%= mode %>">cancel</a>
<% 			} else if (null != request.getParameter("bill")) { %>
<a href="/hci/bill?history&session-id=<%= session_id %>&id=<%= id %>&mode=<%= mode %>">cancel</a>
<% 			} %>
<% 		} else { %>
<a href="/hci/index?session-id=<%= session_id %>">cancel</a>
<% 		}
	}
} %>