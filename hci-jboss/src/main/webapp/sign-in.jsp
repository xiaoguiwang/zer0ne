<!-- sign-in.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [sign-in]"); %>
<%@include file='/fragments/header.jsp' %>
<br/>
<% {
	final Integer sign_in_step = (Integer)request.getAttribute("sign-in-step");
	if (00 == sign_in_step || 10 == sign_in_step || 11 == sign_in_step || 12 == sign_in_step || +21 == sign_in_step || +22 == sign_in_step || 31 == sign_in_step || 32 == sign_in_step || 41 == sign_in_step || 42 == sign_in_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id); %>
<form id="id#form" name="form" method="POST" action="/hci/sign-in?session-id=<%= session_id %>">
<% 	}
	if (00 == sign_in_step || 10 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form.jsp' %>
<% 	} else if (11 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form-password-reset.jsp' %>
<% 	} else if (12 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form-1-time.jsp' %>
<% 	}
	if (00 == sign_in_step || 10 == sign_in_step || 11 == sign_in_step) {
	} else if (+20 == sign_in_step) {
		final String sign_id = (String)request.getAttribute("sign-id"); %>
Signing-in <%= sign_id %>..
<% 	} else if (-21 == sign_in_step) { %>
Timeout sending password-reset e-mail/text: FIXME!
<% 	} else if (+21 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form-password-reset.jsp' %>
<% 	} else if (+22 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form-1-time.jsp' %>
<% 	} else if (31 == sign_in_step || 41 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form-password-reset-token.jsp' %>
<% 	} else if (32 == sign_in_step || 42 == sign_in_step) { %>
<%@include file='/fragments/sign-in-form-1-time-token.jsp' %>
<% 	} %>
<% 	if (00 == sign_in_step || 10 == sign_in_step || 11 == sign_in_step || 12 == sign_in_step || +21 == sign_in_step || +22 == sign_in_step || 31 == sign_in_step || 32 == sign_in_step || 41 == sign_in_step || 42 == sign_in_step) { %>
</form>
<% 	} else if (+20 == sign_in_step || -21 == sign_in_step || -22 == sign_in_step) {
	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>