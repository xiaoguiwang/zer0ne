<!-- sign-out.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [sign-out]"); %>
<%@include file='/fragments/header.jsp' %>
<% {
	final Integer sign_out_step = (Integer)request.getAttribute("sign-out-step");
	if (null == sign_out_step) { %>
Redirecting.. (right place wrong time)
<% 	} else if (10 == sign_out_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final String sign_id = (String)request.getAttribute("sign-id"); %>
<!-- TODO: make into a form with continue and cancel-->
Are you sure? - <a href="/hci/signing-out?step=20&session-id=<%= session_id %>">sign-out</a> (<%= sign_id %>)
<% 	} else if (20 == sign_out_step) {
		final String sign_id = (String)request.getAttribute("sign-id"); %>
Signing-out <%= sign_id %>..
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>