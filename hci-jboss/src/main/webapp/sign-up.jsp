<!-- sign-up.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [sign-up]"); %>
<%@include file='/fragments/header.jsp' %>
<br/>
<% {
	final Integer sign_up_step = (Integer)request.getAttribute("sign-up-step");
	if (0 == sign_up_step || 1 == sign_up_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id); %>
<form id="id#form" name="form" method="POST" action="/hci/sign-up?session-id=<%= session_id %>">
<% 	}
	if (0 == sign_up_step || 1 == sign_up_step) { %>
<%@include file='/fragments/sign-up-form.jsp' %>
<% 	}
	if (0 == sign_up_step || 1 == sign_up_step) { %>
</form>
<% 	}
	if (0 == sign_up_step || 1 == sign_up_step) {
	} else if (2 == sign_up_step) {
		final String sign_id = (String)request.getAttribute("sign-id"); %>
Signing-up <%= sign_id %>..
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>