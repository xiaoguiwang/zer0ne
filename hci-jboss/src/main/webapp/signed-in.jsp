<!-- signed-in.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [signed-in]"); %>
<%@include file='/fragments/header.jsp' %>
<font size=-1>&nbsp;</font>
<br/>
<br/>
<% {
	final Integer sign_in_step = (Integer)request.getAttribute("sign-in-step");
	if (50 == sign_in_step || 51 == sign_in_step || 42 == sign_in_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final java.util.Map<String, Object> __session = ServletTool.getSessionById(session_id);
	  //final String sign_id = (String)__session.get("sign-id");
		final Integer sign_in_attempts_failed = (Integer)__session.get("sign-in-attempts-failed");
		final String sign_id = (String)request.getAttribute("sign-id");
%>
Signed-in <%= sign_id %>!
<br/>
<br/>
<font size=-1><%= null == sign_in_attempts_failed ? "&nbsp;" : "sign in attempts failed = "+sign_in_attempts_failed.toString() %></font>
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>