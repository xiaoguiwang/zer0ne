<!-- signed-out.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [signed-out]"); %>
<%@include file='/fragments/header.jsp' %>
<% {
	final Integer sign_out_step = (Integer)request.getAttribute("sign-out-step");
	if (00 == sign_out_step) { %>
Redirecting.. (right place wrong time)
<% 	} else if (40 == sign_out_step || 50 == sign_out_step) {
		final String sign_id = (String)request.getAttribute("sign-id");
%>
Signed-out! (<%= sign_id %>)
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>