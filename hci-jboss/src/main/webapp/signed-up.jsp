<!-- signed-up.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [signed-up]"); %>
<%@include file='/fragments/header.jsp' %>
<% {
	final Integer sign_up_step = (Integer)request.getAttribute("sign-up-step");
	if (5 == sign_up_step) {
		final String sign_id = null == request.getAttribute("sign-id") ? "" : (String)request.getAttribute("sign-id"); %>
Signed-up <%= sign_id %>!
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>