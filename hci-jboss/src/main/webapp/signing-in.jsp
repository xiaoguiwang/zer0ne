<!-- signing-in.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [signing-in]"); %>
<%@include file='/fragments/header.jsp' %>
<font size=-1>&nbsp;</font>
<br/>
<% {
	final Integer sign_in_step = (Integer)request.getAttribute("sign-in-step");
	if (00 == sign_in_step) { %>
Redirecting.. (right place wrong time)
<% 	} else if (-20 == sign_in_step) { %>
Timeout sending sign-in e-mail/text: FIXME!
<% 	} else if (+20 == sign_in_step || (40 == sign_in_step && null == request.getAttribute("token"))) {
		final String sign_id = (String)request.getAttribute("sign-id"); %>
Signing-in <%= sign_id %>..
<% 	} else if (30 == sign_in_step || 40 == sign_in_step || 42 == sign_in_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id);
		final String sign_in_method = (String)__session.get("sign-in-method");
		if (false == sign_in_method.equals("basic-auth")) { %>
<form id="id#form" name="form" method="POST" action="/hci/signing-in?session-id=<%= session_id %>">
<% 		} else {
			final java.util.Map<String, Object> user = (java.util.Map<String, Object>)__session.get("user");
			if (null == user) {
				final Boolean generated = (Boolean)request.getAttribute("passphrase-generated");
				final String passphrase = (String)request.getAttribute("passphrase"); %>
<br/>
<br/>j
<% 				if (false == generated) { %>
Enter your <i>passphrase</i> into either field in the next popup..
<% 				} else { %>
Copy the <i>passphrase</i> &quot;<%= passphrase %>&quot; (without quotes) and<br/>
<br/>
Paste it into either field in the next popup<br/>
<br/>
After you click &apos;<a href="/hci/signing-in?session-id=<%= session_id %>&sign-in-method=basic-auth">sign-in</a>&apos;
<% 				}
			} else { %>
<br/>
<br/>
<font size=-1>&nbsp;</font>
<% 			}
		}
	}
	if (30 == sign_in_step || (40 == sign_in_step && null != request.getAttribute("token")) || 22 == sign_in_step) { %>
<%@include file='/fragments/signing-in-form-token.jsp' %>
<% 	}
	if (00 == sign_in_step) {
	} else if (-20 == sign_in_step) {
	} else if (+20 == sign_in_step) {
	} else if (30 == sign_in_step || (40 == sign_in_step && null != request.getAttribute("token")) || 22 ==sign_in_step || 42 == sign_in_step) { %>
</form>
<% 	} else if (40 == sign_in_step && null == request.getAttribute("token")) {
	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>