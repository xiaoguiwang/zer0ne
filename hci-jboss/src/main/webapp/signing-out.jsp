<!-- signing-out.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [signing-out]"); %>
<%@include file='/fragments/header.jsp' %>
<% {
	final Integer sign_out_step = (Integer)request.getAttribute("sign-out-step");
	if (00 == sign_out_step || 10 == sign_out_step) { %>
Redirecting.. (right place wrong time)
<% 	} else if (20 == sign_out_step || 40 == sign_out_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final java.util.Map<String, Object> __session = ServletTool.getSessionById(session_id);
		final String sign_id = (String)request.getAttribute("sign-id"); %>
Signing-out <%= sign_id %>..
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>