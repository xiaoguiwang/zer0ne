<!-- signing-up.jsp -->
<% request.setAttribute("$html$head$title", "IT From Blighty [signing-up]"); %>
<%@include file='/fragments/header.jsp' %>
<br/>
<% {
	final Integer sign_up_step = (Integer)request.getAttribute("sign-up-step");
	if (0 == sign_up_step) { %>
Redirecting.. (right place wrong time)
<% 	} else if (-2 == sign_up_step) { %>
Timeout sending sign-up e-mail/text: FIXME!
<% 	} else if (+2 == sign_up_step) {
		final String sign_id = (String)request.getAttribute("sign-id"); %>
Signing-up <%= sign_id %>..
<% 	} else if (3 == sign_up_step || 4 == sign_up_step) {
		final String session_id = (String)request.getAttribute("session-id");
		final java.util.Map<String, Object> __session = null == session_id ? null : (java.util.Map<String, Object>)ServletTool.getSessionById(session_id); %>
<form id="id#form" name="form" method="POST" action="/hci/signing-up?session-id=<%= session_id %>">
<% 	}
	if (3 == sign_up_step || 4 == sign_up_step) { %>
<%@include file='/fragments/signing-up-form-token.jsp' %>
<% 	}
	if (0 == sign_up_step) {
	} else if (-2 == sign_up_step) {
	} else if (+2 == sign_up_step) {
	} else if (3 == sign_up_step || 4 == sign_up_step) { %>
</form>
<% 	} else {
		throw new IllegalStateException();
	}
} %>
<%@include file='/fragments/footer.jsp' %>