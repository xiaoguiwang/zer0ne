<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<!-- tokens.jsp -->
<%
/*final */String jsp = "IT From Blighty [tokens.jsp]";
final String[] split = request.getRequestURL().toString().split("/");
 /*final */int i = split.length;
if (null == split[i-1] || 0 == split[i-1].trim().length()) {
	i = i-1;
}
if (true == split[i-1].startsWith("tokens")) {
  /*final String */jsp = "IT From Blighty ["+split[i-1]+"]";
} else {
  /*final String */jsp = "IT From Blighty..";
}
%>
<% request.setAttribute("$html$head$title", jsp); %>
<%@ include file='/fragments/header.jsp' %>
<% {
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	if (true == redirecting) { %>
<h1>IT From Blighty</h1>
<% 	} else { %>
<%@include file='/fragments/tokens-form.jsp' %>
<% 	} %>
<% } %>
<%@include file='/fragments/footer.jsp' %>