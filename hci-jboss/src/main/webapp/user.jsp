<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<!-- user.jsp -->
<% {
  /*final */String jsp = "IT From Blighty [user.jsp]";
	final String[] split = request.getRequestURL().toString().split("/");
  /*final */int i = split.length;
	if (null == split[i-1] || 0 == split[i-1].trim().length()) {
		i = i-1;
	}
	if (true == split[i-1].startsWith("user")) {
	  /*final String */jsp = "IT From Blighty ["+split[i-1]+"]";
	} else {
	  /*final String */jsp = "IT From Blighty..";
	}
	request.setAttribute("$html$head$title", jsp);
} %>
<%@ include file='/fragments/header.jsp' %>
<% {
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	if (true == redirecting) { %>
<h1>IT From Blighty</h1>
<% 	} else {
		{
			final String session_id = (String)request.getAttribute("session-id"); %>
<form id="id#form-user" name="form-user" method="POST" action="/hci/user?session-id=<%= session_id %>">
<% 		} %>
<%@include file='/fragments/user-form.jsp' %>
<br/>
<% 		final java.util.Map<String, Object> user = (java.util.Map<String, Object>)request.getAttribute("user");
		final String mode = null == request.getAttribute("mode") ? "" : (String)request.getAttribute("mode");
		if (null == user) {
			if (true == mode.equals("create")) { // if (true == hci.j2ee.Users.hasPermission(signed_in_user, "any-user-create")) { %>
<input id="id#submit" name="submit" type="submit" value="create"/>
<% 			}
		} else { %>
<input id="id#submit" name="submit" type="submit" value="update"/>
<% 		} %>
|
<% 		final String session_id = (String)request.getAttribute("session-id"); %>
<a href="/hci/users?session-id=<%= session_id %>">cancel</a>
</form>
<% 	} %>
<% } %>
<%@include file='/fragments/footer.jsp' %>