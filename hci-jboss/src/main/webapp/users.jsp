<%@ page contentType="text/html;charset=UTF-8" session="false" %>
<!-- users.jsp -->
<% {
  /*final */String jsp = "IT From Blighty [users.jsp]";
	final String[] split = request.getRequestURL().toString().split("/");
  /*final */int i = split.length;
	if (null == split[i-1] || 0 == split[i-1].trim().length()) {
		i = i-1;
	}
	if (true == split[i-1].startsWith("users")) {
	  /*final String */jsp = "IT From Blighty ["+split[i-1]+"]";
	} else {
	  /*final String */jsp = "IT From Blighty..";
	}
	request.setAttribute("$html$head$title", jsp);
} %>
<%@include file='/fragments/header.jsp' %>
<% {
	final boolean redirecting = null == request.getAttribute("redirecting") ? false : (Boolean)request.getAttribute("redirecting");
	if (true == redirecting) { %>
<h1>IT From Blighty</h1>
<% 	} else { %>
<% 		{
			final String session_id = (String)request.getAttribute("session-id");
		  //final java.util.Map<String, Object> __session = ServletTool.getSessionById(session_id);
			final String __permission_id = (String)request.getAttribute("permission-id");
			final String qs_permission_id = null == __permission_id ? "" : "&permission-id="+(String)request.getAttribute("permission-id");
			final String __role_id = (String)request.getAttribute("role-id");
			final String qs_role_id = null == __role_id ? "" : "&role-id="+(String)request.getAttribute("role-id");
			final String __group_id = (String)request.getAttribute("group-id");
			final String qs_group_id = null == __group_id ? "" : "&group-id="+(String)request.getAttribute("group-id");
%>
<form id="id#form-users" name="form-users" method="POST" action="/hci/users?session-id=<%= session_id %><%= qs_permission_id %><%= qs_role_id %><%= qs_group_id %>">
<% 		} %>
<%@include file='/fragments/users-form.jsp' %>
</form>
<% 	} %>
<% } %>
<%@include file='/fragments/footer.jsp' %>