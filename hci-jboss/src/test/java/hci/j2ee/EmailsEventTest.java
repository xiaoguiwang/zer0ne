package hci.j2ee;

import java.util.Base64;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.cache.Helper;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmailsEventTest {
	@Test
	public void test_outbound_0001() {
		final String event_name = "password-reset";
		final Long creation_time = new Long(System.currentTimeMillis());
		final String uuid_as_char = ServletTool.generateString(8);
		final String from = "emailus@mail.itfromblighty";
		final Boolean allow_reply = true;
		final String to = "alex@mail.itfromblighty";
		final String __subject = "IT From Blighty - E-mail JUnit for dto-cache-ignite testing..";
		final String body = "Testing, testing, 1-2-3";
		Helper.cacheEvent(Emails.class, uuid_as_char, new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("event-name", event_name);
				property("creation-time", creation_time.toString());
				property("uuid-as-char", uuid_as_char);
				property("from", from);
				property("allow-reply", allow_reply.toString());
				property("to", to);
				property("subject", __subject);
				property("mime-type", "text/html");
				property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
				property("message-id-in-base64", Base64.getEncoder().encodeToString("<message-id:email#0123000001>".getBytes()));
				property("type", "email:outbound");
				property("email-history-id", new Integer(0).toString());
				property("state", "success");
			}
		});
		Helper.cacheEventX(Emails.class, uuid_as_char, new java.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("event-name", event_name);
				put("creation-time", creation_time);
				put("uuid-as-char", uuid_as_char);
				put("process-time", new Long(System.currentTimeMillis()));
				put("type", "email:outbound");
			}
		});
	}
	
	@Test
	public void test_outbound_0002() {
		final String event_name = "password-reset";
		final Long creation_time = new Long(System.currentTimeMillis());
		final String uuid_as_char = ServletTool.generateString(8);
		final String from = "emailus@mail.itfromblighty";
		final Boolean allow_reply = true;
		final String to = "alex@mail.itfromblighty";
		final String __subject = "IT From Blighty - E-mail JUnit for dto-cache-ignite testing..";
		final String body = "Testing, testing, 1-2-3";
		Helper.cacheEvent(Emails.class, uuid_as_char, new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("event-name", event_name);
				property("creation-time", creation_time.toString());
				property("uuid-as-char", uuid_as_char);
				property("from", from);
				property("allow-reply", allow_reply.toString());
				property("to", to);
				property("subject", __subject);
				property("mime-type", "text/html");
				property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
				property("message-id-in-base64", Base64.getEncoder().encodeToString("<message-id:email#0123000002>".getBytes()));
				property("type", "email:outbound");
				property("email-history-id", new Integer(1).toString());
				property("state", "failure");
				property("state:failure", new RuntimeException().toString());
			}
		});
		Helper.cacheEventX(Emails.class, uuid_as_char, new java.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("event-name", event_name);
				put("creation-time", creation_time);
				put("uuid-as-char", uuid_as_char);
				put("process-time", new Long(System.currentTimeMillis()));
				put("type", "email:outbound");
			}
		});
	}
	
	@Test
	public void test_inbound_0003() {
		final String event_name = "password-reset";
		final Long creation_time = new Long(System.currentTimeMillis());
		final String uuid_as_char = ServletTool.generateString(8);
		final String from = "alex@mail.itfromblighty";
	  //final String to = "emailus@mail.itfromblighty";
		final String __subject = "RE: IT From Blighty - E-mail JUnit for dto-cache-ignite testing..";
		final String body = "Y";
		final String repliedMessageId = "<message-id:email#0123000001>";
		Helper.cacheEvent(Emails.class, uuid_as_char, new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("event-name", event_name);
				property("creation-time", creation_time.toString());
				property("uuid-as-char", uuid_as_char);
				property("from", from);
			  //property("to", to);
				property("subject", __subject);
			  //property("mime-type", "text/html");
				property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
				property("message-id-in-base64", Base64.getEncoder().encodeToString(repliedMessageId.getBytes()));
				property("type", "email:inbound");
				property("email-history-id", new Integer(2).toString());
				property("state", "success");
			}
		});
		Helper.cacheEventX(Emails.class, uuid_as_char, new java.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("event-name", event_name);
				put("creation-time", creation_time);
				put("uuid-as-char", uuid_as_char);
				put("process-time", new Long(System.currentTimeMillis()));
				put("type", "email:inbound");
			}
		});
	}
}