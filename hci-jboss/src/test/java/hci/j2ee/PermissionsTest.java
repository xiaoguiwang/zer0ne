package hci.j2ee;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PermissionsTest {
	static {
		final java.io.File file = new java.io.File("C:\\Users\\alex.russell\\itfromblighty.ca\\worktrees\\zer0ne\\release\\0.0.1\\hci-jboss\\dao-sqlite.db");
		if (true == file.exists()) {
			file.delete();
		}
	}
	
	private static final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> users = Users.users;
	private static final java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Object>> permissions = Permissions.permissions;
	
	@Test
	public void test_0001() {
		final java.util.Map<java.lang.String, java.lang.Object> permission = permissions.get("any-user-update");
		final java.util.Map<java.lang.String, java.lang.Object> user = users.get("admin");
		Permissions.addToUser(permission, (String)user.get("user-id"));
		Permissions.removeFromUser(permission, (String)user.get("user-id"));
	}
}