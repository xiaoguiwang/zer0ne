package hci.j2ee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServletToolTest {
	@Test
	public void test0001() {
		final String token = ServletTool.generateString(16);
		System.out.println("token="+token);
	}
	
	// https://www.viralpatel.net/java-url-encoder-decoder/
	@Test
	public void test0002() {
		final String encodedString = ServletTool.encodeString("url?text=Hello World!");
		System.out.println("encoded: "+encodedString);
	}
	// https://www.viralpatel.net/java-url-encoder-decoder/
	@Test
	public void test0003() {
		final String encodedString = ServletTool.encodeString("url?text=Hello World!");
		final String decodedString = ServletTool.decodeString(encodedString);
		System.out.println("decoded: "+decodedString);
	}
	/*
	 * CREATE TABLE "ITFROMBLIGHTY".OBJECT
	 *  ID UNIQUE/PRIMARY KEY IS NOT NULL
	 *  TYPE IS NOT NULL
	 * 
	 * CREATE TABLE "ITFROMBLIGHTY".OBJECT_FIELD
	 *  ID UNIQUE/PRIMARY KEY IS NOT NULL
	 *  OBJECT_ID DEFAULT NULL
	 *  TYPE IS NOT NULL
	 *  NAME IS NOT NULL
	 * 
	 * CREATE TABLE "ITFROMBLIGHTY".OBJECT_FIELD_VALUE
	 *  ID UNIQUE/PRIMARY KEY IS NOT NULL
	 *  OBJECT_ID DEFAULT NULL --> but cannot be NULL if/when OBJECT_FIELD_ID is NULL
	 *  OBJECT_FIELD_ID DEFAULT NULL --> but cannot be NULL if/when OBJECT_ID is NULL
	 *  VALUE DEFAULT NULL
	 */
	private static final void recurse(final Object o, final Object k, final Object v, final Class<?> clazz) {
		if (null == o && null == k) {
			System.out.println(v.getClass()+"#"+v.hashCode()+"==>");
		} else if (null != o) {
			System.out.println(o.getClass()+"#"+o.hashCode()+"-->");
		}
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == v instanceof Integer) {
			if (null != k) {
				System.out.print(k.getClass()+"#"+k.hashCode()+":"+k.toString()+"=");
			}
			System.out.println(v.getClass()+"#"+v.hashCode()+"="+v);
		} else if (true == v instanceof String) {
			if (null != k) {
				System.out.print(k.getClass()+"#"+k.hashCode()+":"+k.toString()+"=");
			}
			System.out.println(v.getClass()+"#"+v.hashCode()+"="+v);
		} else if (true == v instanceof Object[]) {
			if (null != o) {
				System.out.println(o.getClass()+"#"+o.hashCode()+"-->");
			}
			for (final Object __v: (Object[])v) {
				ServletToolTest.recurse(v, null, __v, List.class);
			}
		} else if (true == v instanceof List) {
			if (null != k) {
				System.out.print(k.getClass()+"#"+k.hashCode()+":"+k.toString()+"=");
			}
			for (final Object __v: (List<?>)v) {
				ServletToolTest.recurse(v, null, __v, List.class);
			}
		} else if (true == v instanceof Map) {
			if (null != k) {
				System.out.print(k.getClass()+"#"+k.hashCode()+":"+k.toString()+"=");
			}
			final Map<?, ?> map = (Map<?, ?>)v;
			for (final Object __k: map.keySet()) {
				final Object __v = map.get(__k);
				ServletToolTest.recurse(v, __k, __v, Map.class);
			}
		} else {
			// Not supported
		}
	}
	private static final void recurse(final List<?> list) {
		ServletToolTest.recurse(null, null, list, list.getClass());
	}
	private static final void recurse(final Map<?, ?> map) {
		ServletToolTest.recurse(null, null, map, map.getClass());
	}
	private static final void recurse(final Object[] oa) {
		ServletToolTest.recurse(null, null, oa, oa.getClass());
	}
	private static final void recurse(final Object o) {
		ServletToolTest.recurse(null, null, o, o.getClass());
	}
	
	@Before
	public void before() {
	  //System.out.println();
	}
	
	@Test
	public void test0004() {
		ServletToolTest.recurse(new Integer(0));
	}
	@Test
	public void test0005() {
		ServletToolTest.recurse(new String("Hello World!"));
	}
	@Test
	public void test0006() {
		final List<Integer> list = new ArrayList<>();
		list.add(new Integer(0));
		list.add(new Integer(1));
		list.add(new Integer(2));
		ServletToolTest.recurse(list);
	}
	@Test
	public void test0007() {
		final Map<String, String> map = new HashMap<>();
		map.put("Hello", "World!");
		map.put("Apple", "Banana");
		ServletToolTest.recurse(map);
	}
	@Test
	public void test0008() {
		final String[] sa = new String[]{"Hello","World!"};
		ServletToolTest.recurse(sa);
	}
	@Test
	public void test0009() {
		final Map<String, List<String>> map = new HashMap<>();
		final List<String> list = new ArrayList<>();
		list.add("There");
		list.add("World!");
		map.put("MyList", list);
		ServletToolTest.recurse(map);
	}
	@Test
	public void test0010() {
		final Map<String, Map<String, String>> map = new HashMap<>();
		final Map<String, String> map2 = new HashMap<>();
		map2.put("There", "World!");
		map.put("MyMap", map2);
		ServletToolTest.recurse(map);
	}
	/*
	@Test
	public void test0011() {
		final TcpDiscoverySpi spi = new TcpDiscoverySpi();
		final TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
		tcMp.setAddresses(Arrays.asList("localhost"));
		spi.setIpFinder(tcMp);
		final IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setClientMode(true);
		cfg.setDiscoverySpi(spi);
		final Ignite ignite = Ignition.start(cfg);
		final IgniteCache<String, String> cache = ignite.getOrCreateCache("HelloWorld");
		
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	*//*final *//*String dts = null;
		if (true == cache.containsKey("sdf")) {
			dts = cache.get("sdf");
			System.out.println("dts="+dts);
		}
		dts = sdf.format(new Date()); // 2018-10-25-16.27.49.059
		cache.put("sdf", dts);
		ignite.close();
	}
	*/
	@Test
	public void test0012() {
		final String version = ServletTool.getVersion();
		System.out.println("version="+version);
	}
	
	@After
	public void after() {
		System.out.println();
	}
}