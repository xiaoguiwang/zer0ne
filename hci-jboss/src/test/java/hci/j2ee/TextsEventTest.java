package hci.j2ee;

import java.util.Base64;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.cache.Helper;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TextsEventTest {
	@Test
	public void test_outbound_0001() {
		final String event_name = "password-reset";
		final Long creation_time = new Long(System.currentTimeMillis());
		final String uuid_as_char = ServletTool.generateString(8);
		final String from = "admin"; // TODO: fix this ambiguous multi-purpose nature 
		final String to = "arussell";
		final String body = "Testing, testing, 1-2-3";
		Helper.cacheEvent(Texts.class, uuid_as_char, new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("event-name", event_name);
				property("creation-time", creation_time.toString());
				property("uuid-as-char", uuid_as_char);
				property("from", from);
				property("to", to);
				property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
				property("message-id", new Long("0123000001").toString());
				property("type", "sms:outbound");
				property("text-history-id", new Integer(0).toString());
				property("state", "success");
			}
		});
		Helper.cacheEventX(Texts.class, uuid_as_char, new java.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("event-name", event_name);
				put("creation-time", creation_time);
				put("uuid-as-char", uuid_as_char);
				put("process-time", new Long(System.currentTimeMillis()));
				put("type", "sms:outbound");
			}
		});
	}
	
	@Test
	public void test_outbound_0002() {
		final String event_name = "password-reset";
		final Long creation_time = new Long(System.currentTimeMillis());
		final String uuid_as_char = ServletTool.generateString(8);
		final String from = "admin"; // TODO: fix this ambiguous multi-purpose nature 
		final String to = "arussell";
		final String body = "Testing, testing, 1-2-3";
		Helper.cacheEvent(Texts.class, uuid_as_char, new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("event-name", event_name);
				property("creation-time", creation_time.toString());
				property("uuid-as-char", uuid_as_char);
				property("from", from);
				property("to", to);
				property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
				property("message-id", new Long("0123000002").toString());
				property("type", "sms:outbound");
				property("text-history-id", new Integer(1).toString());
				property("state", "failure");
				property("state:failure", new RuntimeException().toString());
			}
		});
		Helper.cacheEventX(Texts.class, uuid_as_char, new java.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("event-name", event_name);
				put("creation-time", creation_time);
				put("uuid-as-char", uuid_as_char);
				put("process-time", new Long(System.currentTimeMillis()));
				put("type", "sms:outbound");
			}
		});
	}
	
	@Test
	public void test_inbound_0003() {
		final String event_name = "password-reset";
		final Long creation_time = new Long(System.currentTimeMillis());
		final String uuid_as_char = ServletTool.generateString(8);
		final String from = "arussell";
	  //final String to = "admin"
		final String body = "Y";
		final Long outgoingMessageId = new Long("0123000001");
		Helper.cacheEvent(Texts.class, uuid_as_char, new dro.util.Properties(){
			private static final long serialVersionUID = 0L;
			{
				property("event-name", event_name);
				property("creation-time", creation_time.toString());
				property("uuid-as-char", uuid_as_char);
				property("from", from);
			  //property("to", to);
				property("body-in-base64", Base64.getEncoder().encodeToString(body.getBytes()));
				property("message-id", outgoingMessageId.toString());
				property("type", "sms:inbound");
				property("text-history-id", new Integer(2).toString());
				property("state", "success");
			}
		});
		Helper.cacheEventX(Texts.class, uuid_as_char, new java.util.HashMap<String, Object>(){
			private static final long serialVersionUID = 0L;
			{
				put("event-name", event_name);
				put("creation-time", creation_time);
				put("uuid-as-char", uuid_as_char);
				put("process-time", new Long(System.currentTimeMillis()));
				put("type", "sms:inbound");
			}
		});
	}
}