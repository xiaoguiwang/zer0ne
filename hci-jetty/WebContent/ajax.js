// ajax.js

// Sign-up! (POST)
function AjaxPostRequest(user, pass, same) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState && 200 == this.status) {
		  //AjaxPostResponse(this);
		}
	}
	x.open('POST', '/zer0ne/hci/0.0.1-SNAPSHOT/user/sign/up?user='+user+"&session='"+getJSessionId()+"'", false);
	try {
		x.send('{ "pass": ["'+pass+'","'+same+'"] }');
	} catch (error) {
	}
	return x;
}
function AjaxPostResponse(x) {
	var xml  = x.responseXML;
}

//Sign-in! (GET)
function AjaxGetRequest(user, pass) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState && 200 == this.status) {
		  //AjaxGetResponse(this);
		}
	}
	x.open('GET', '/zer0ne/hci/0.0.1-SNAPSHOT/user/sign/in?user='+user+'&pass='+pass+"&session='"+getJSessionId()+"'", false);
	try {
		x.send();
	} catch (error) {
	}
	return x;
}
function AjaxGetResponse(x) {
	var xml  = x.responseXML;
}

//Sign-up! (POST)
function AjaxPutRequest(user, json) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState && 200 == this.status) {
		  //AjaxPutResponse(this);
		}
	}
	x.open('PUT', '/zer0ne/hci/0.0.1-SNAPSHOT/user/set/up?user='+user+"&session='"+getJSessionId()+"'", false);
	try {
		x.send(json);
	} catch (error) {
	}
	return x;
}
function AjaxPutResponse(x) {
	var xml  = x.responseXML;
}

//Sign-out! (HEAD)
function AjaxHeadRequest(user) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState && 200 == this.status) {
		  //AjaxHeadResponse(this);
		}
	}
	x.open('HEAD', '/zer0ne/hci/0.0.1-SNAPSHOT/user/sign/out?user='+user+"&session='"+getJSessionId()+"'", false);
	try {
		x.send();
	} catch (error) {
	}
	return x;
}
function AjaxHeadResponse(x) {
	var xml  = x.responseXML;
}