// calendar.js

// For example: "Fri Jul 27 2018 12:37:03 GMT-0600 (Mountain Daylight Time)"
// Know: new Date("Jul 1 2018").toString() = "Sun Jul 01 2018 00:00:00 GMT-0600 (Mountain Daylight Time)"

var Calendar = function(date) {
	var __date = undefined;
	
	var __year = null;
	var __yearIsLeap = undefined;
	var __month = null; // Starting from 1 - will be new Date().getMonth()+1
	var __dayOfMonth = null; // This is new Date().getDate()
	var __daysInMonth = undefined;
	var __dayOfWeek = null; // This is new Date().getDay()+1
	
	var __mode = null; // DAYS, MONTHS, YEARS
	
	var __htmlElementId = undefined;
	var __visible = undefined;
	var __cssStyleShow = '';
  //var __hidden = undefined;
	var __cssStyleHide = '';
	
	this.htmlElementId = function(htmlElementId) {
		var __return = undefined;
		if (undefined === htmlElementId) {
			if (undefined === this.__htmlElementId) {
				this.__htmlElementId = this.variableName()+"#id";
			}
			__return = this.__htmlElementId;
		} else {
			this.__htmlElementId = htmlElementId;
			__return = this;
		}
		return __return;
	}
	this.variableName = function(variableName) {
		var __return = undefined;
		if (undefined === variableName) {
			__return = this.__variableName;
		} else {
			this.__variableName = variableName;
			__return = this;
		}
		return __return;
	};
	this.visible = function(visible) {
		var __return = undefined;
		if (undefined === visible) {
			if (undefined === this.__visible) {
				this.__visible = false;
			}
			__return = this.__visible;
		} else {
			this.__visible = visible;
			__return = this;
		}
		return __return;
	};
	// https://www.w3schools.com/css/css_positioning.asp
	this.cssStyleShow = function(style) {
		var __return = undefined;
		if (undefined === style) {
			if (undefined === this.__cssStyleShow) {
				// https://css-tricks.com/quick-css-trick-how-to-center-an-object-exactly-in-the-center/
				this.__cssStyleShow = 'position: absolute; top: 50px; left: 50%; transform: translate(-50%, 0px) rotate(0deg); border: solid black 2px';
			}
			__return = this.__cssStyleShow;
		} else {
			this.__cssStyleShow = style;
			__return = this;
		}
		return __return;
	}
  /*this.hidden = function(hidden) {
		var __return = undefined;
		if (undefined === hidden) {
			this.__hidden = false;
			__return = this.__hidden;
		} else {
			this.__hidden = hidden;
			__return = this;
		}
		return __return;
	};
	this.hidden(false);*/
	this.cssStyleHide = function(style) {
		var __return = undefined;
		if (undefined === style) {
			if (undefined === this.__cssStyleHide) {
				this.__cssStyleHide = '';
			}
			__return = this.__cssStylehide;
		} else {
			this.__cssStyleHide = style;
			__return = this;
		}
		return __return;
	}
	
	this.show = function(dr) {
		if (true == this.visible()) {
			this.ontopof(dr)
		}
	};
	this.showToggleHide = function(dr) {
		this.visible(!this.visible()).render();
		if (true == this.visible()) {
			this.ontopof(dr);
		}
	};
	
	this.picked = function(picked) {
		var __return = undefined;
		if (undefined === picked) {
			if (undefined === this.__picked) {
				this.__picked = this.clone();
			}
			__return = this.__picked;
		} else {
			__return = this.picked().setYear(picked.getYear()).setMonth(picked.getMonth()).setDayOfMonth(picked.getDayOfMonth());
		}
		return __return;
	};
	this.pickDay = function(year, month, dayOfMonth) {
		this.picked().setYear(year).setMonth(month).setDayOfMonth(dayOfMonth);
		return this.setDayOfMonth(dayOfMonth);
	};
	this.pickYesterday = function() {
		var calendar = Calendar.today.shiftDays(-1);
		this.setYear(calendar.getYear()).setMonth(calendar.getMonth()).setDayOfMonth(calendar.getDayOfMonth());
		this.picked().setYear(calendar.getYear()).setMonth(calendar.getMonth()).setDayOfMonth(calendar.getDayOfMonth());
		return this;
	}
	this.pickToday = function() {
		var calendar = Calendar.today;
		this.setYear(calendar.getYear()).setMonth(calendar.getMonth()).setDayOfMonth(calendar.getDayOfMonth());
		this.picked().setYear(calendar.getYear()).setMonth(calendar.getMonth()).setDayOfMonth(calendar.getDayOfMonth());
		return this;
	}
	this.pickTomorrow = function() {
		var calendar = Calendar.today.shiftDays(+1);
		this.setYear(calendar.getYear()).setMonth(calendar.getMonth()).setDayOfMonth(calendar.getDayOfMonth());
		this.picked().setYear(calendar.getYear()).setMonth(calendar.getMonth()).setDayOfMonth(calendar.getDayOfMonth());
		return this;
	}
	this.choose = function() {
		document.getElementById(this.variableName()+"Picked#id").value = this.getDateAsString();
		return this;
	}
	
	this.setDate = function(date) {
		if (undefined === date) {
		} else {
			if (null == date) {
				this.__date = new Date();
			} else {
				this.__date = date;
			}
			this.setYear(this.__date.getFullYear()).setMonth(this.__date.getMonth()+1).setDayOfMonth(this.__date.getDate());
		}
		return this;
	};
	
	this.clone = function(clone) {
		var __clone = null;
		if (undefined === clone) {
			__clone = new Calendar().setYear(this.getYear()).setMonth(this.getMonth()).setDayOfMonth(this.getDayOfMonth());
		} else if (null == clone) {
			window.alert('calendar.clone(null) // constructor is not supported');
		} else {
			__clone = new Calendar(clone);
		}
		return __clone;
	};
	
	this.getDate = function() {
		return this.__date;
	};
	// https://www.w3schools.com/js/js_date_formats.asp
	// "2015-03-25T12:00:00Z"
	this.getDateAsString = function() {
		return this.getYear()+'-'+this.getFullMonth()+'-'+this.getDayOfMonth()+'T'+"00"+':'+"00"+':'+"00";
	}
	
	this.isYearLeap = function() {
		return this.__yearIsLeap;
	};
	this.setYear = function(year) {
		if (year != this.__year) {
			this.__year = year;
			this.__yearIsLeap = false;
			if (true == (0 == (this.__year % 4))) {
				this.__yearIsLeap = true;
				if (true == (0 == (this.__year % 100))) {
					this.__yearIsLeap = false;
					if (true == (0 == (this.__year % 400))) {
						this.__yearIsLeap = true;
					}
				}
			}
		}
		return this;
	};
	this.getYear = function() {
		return this.__year;
	};
	this.getYearLabel = function() {
		var yearLabel = this.__year;
		if (true == this.__yearIsLeap) {
			yearLabel = yearLabel+'*';
		}
		return yearLabel;
	};
	
	this.getMonthName = function() {
		return Constants.months[(this.__month-1).toString()];
	};
	this.getMonth = function(plusOrMinusMonths) {
		var month = this.__month;
		if (undefined !== plusOrMinusMonths) {
			month = month+plusOrMinusMonths;
			if (month < 1) {
				month = month+12;
			} else if (month > 12) {
				month = month-12;
			}
		}
		return month;
	};
	this.getFullMonth = function() {
		var month = this.getMonth();
		var fullMonth = "";
		if (month < 10) {
			fullMonth = "0";
		}
		return fullMonth+month;
	};
	this.setMonth = function(month) {
		var year = this.__year;
		if (month < 1) {
			month = month+12;
			year = year-1;
		} else if (month > 12) {
			month = month-12;
			year = year+1;
		}
		this.__month = month;
		this.__year = year;
		this.__daysInMonth = null;
		if (1 == this.__month) {
			this.__daysInMonth = Constants.daysInMonth[this.getMonthName()];
		} else if (2 == this.__month) {
			if (true == this.__yearIsLeap) {
				this.__daysInMonth = 29;
			} else {
				this.__daysInMonth = 28;
			}
		} else if (3 <= this.__month) {
			this.__daysInMonth = Constants.daysInMonth[this.getMonthName()];
		}
		return this;
	};
	
	this.setDayOfMonth = function(dayOfMonth) {
		var year = this.__year;
		var month = this.__month;
		if (dayOfMonth < 1) {
			month = month-1;
			if (month < 1) {
				month = month+12;
				year = year-1;
			}
			var lastMonth = new Calendar().setYear(year).setMonth(month);
			this.setYear(year).setMonth(month).setDayOfMonth(lastMonth.getDaysInMonth());
		} else if (dayOfMonth > this.__daysInMonth) {
			month = month+1;
			if (month > 12) {
				month = month-12;
				year = year+1;
			}
			var nextMonth = new Calendar().setYear(year).setMonth(month);
			this.setYear(year).setMonth(month).setDayOfMonth(1);
		} else {
			this.__dayOfMonth = dayOfMonth;
			this.__dayOfWeek = new Date(this.getMonthName()+' '+this.getDayOfMonth()+' '+this.getYear()).getDay()+1;
		}
		return this;
	};
	this.getDayOfMonth = function() {
		var dayOfMonth = null;
		if (null != this.__daysInMonth && this.__dayOfMonth <= this.__daysInMonth) {
			dayOfMonth = this.__dayOfMonth;
		}
		return dayOfMonth;
	};
	this.getDaysInMonth = function(plusOrMinusMonths) {
		var daysInMonth = this.__daysInMonth;
		if (undefined !== plusOrMinusMonths) {
			var month = this.getMonth(plusOrMinusMonths);
			var monthName = Constants.months[(month-1).toString()];
			daysInMonth = Constants.daysInMonth[monthName];
		}
		return daysInMonth;
	}
	this.getDaysLastMonth = function() {
		var theYear = this.__year;
		var lastMonth = this.__month-1;
		if (0 == lastMonth) {
			theYear = theYear-1;
			lastMonth = 12;
		}
		return new Calendar().setYear(theYear).setMonth(lastMonth).getDaysInMonth();
	}
	this.getDayOfMonthLabel = function() {
		var dayOfMonthLabel = null;
		var dayOfMonth = this.__dayOfMonth;
		if (1 == dayOfMonth || 21 == dayOfMonth || 31 == dayOfMonth) {
			dayOfMonthLabel = dayOfMonth+"st";
		} else if (2 == dayOfMonth || 22 == dayOfMonth) {
			dayOfMonthLabel = dayOfMonth+"nd";
		} else if (3 == dayOfMonth || 23 == dayOfMonth) {
			dayOfMonthLabel = dayOfMonth+"rd";
		} else {
			dayOfMonthLabel = dayOfMonth+"th";
		}
		return dayOfMonthLabel;
	};
	
	this.getDayOfWeek = function() {
		return this.__dayOfWeek;
	};
	this.getDayOfWeekName = function() {
		return Constants.daysOfWeek[this.__dayOfWeek-1];
	};
	this.getDayOfWeekOffset = function() {
		return Constants.daysOfWeekOffset[
			new Calendar().setYear(this.getYear()).setMonth(this.getMonth()).setDayOfMonth(1).getDayOfWeekName()
		];
	};
	
	this.shiftYears = function(plusOrMinusYears) {
		return new Calendar().setYear(this.getYear()+plusOrMinusYears).setMonth(this.getMonth()).setDayOfMonth(this.getDayOfMonth()).setMode(this.getMode());
	};
	this.shiftMonths = function(plusOrMinusMonths) {
		// FIXME: this is broken for this.shiftMonths(-1) when this.getMonth()=>'Mar' and this.getDayOfMonth()=>29 (i.e. leap year)
		return new Calendar().setYear(this.getYear()).setMonth(this.getMonth()+plusOrMinusMonths).setDayOfMonth(this.getDayOfMonth()).setMode(this.getMode())
	};
	this.shiftMonthsName = function(plusOrMinusMonths) {
		var month = this.getMonth(plusOrMinusMonths);
		return Constants.months[(month-1).toString()];
	};
	this.shiftDays = function(plusOrMinusDays) {
		return new Calendar().setYear(this.getYear()).setMonth(this.getMonth()).setDayOfMonth(this.getDayOfMonth()+plusOrMinusDays).setMode(this.getMode());
	};
	
	this.setMode = function(mode) {
		this.__mode = mode;
		return this;
	};
	this.getMode = function() {
		if (undefined === this.__mode) {
			this.__mode = Constants.DAYS;
		}
		return this.__mode;
	};
	
	this.toString = function() {
		return new Date(this.getMonthName()+' '+this.getDayOfMonth()+' '+this.getYear()).toString(); // FIXME: We're missing HH:MM:SS but we do have Time Zone(?!) 
	}; // Note: new Date().getHours() // etc.
	
	this.setDate(date);
};

Calendar.bgcolor = '#fdfdfd';
Calendar.color = '#999999';

//TODO: create a function to render <b> and </b> around the text passed in, if year, month, date, and day match
//and extend it to lastYear/Month, and nextYear/Month, etc. incl. all date/days of the month
Calendar.prototype.render = function(dr) {
	Calendar.today = new Calendar(new Date());
	var today = Calendar.today;
  //var today = new Calendar(new Date("Feb 5 1979"));
	
	var vn = this.variableName();
	var vp = this.picked();
	
	var id = undefined; // Note: was function argument 1
	if (undefined === id || null === id) {
		id = this.htmlElementId();
	}
	var e = window.document.getElementById(id);

	var html = '';
	
	if (true == this.visible()) {
	
	// http://www.1x1px.me/
	// https://www.smashingmagazine.com/2013/07/simple-responsive-images-with-css-background-images/
	html = html + "<table class='calendarTable' style='background-color: "+Calendar.bgcolor+"; background-image: url(img/trans.png);'>\n";

	html = html + "<tr class='ctr'>\n";
	html = html + "<td class='ctd2' colspan=7>\n";
	// https://www.cleanpng.com/png-computer-icons-clip-art-close-button-4045006/download-png.html
	html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".visible(false).render();'><img height=20 width=20 src='img/close.png' alt='X' /></a>";
	html = html + "</td>\n";
	html = html + "</tr>\n";
	html = html + "<tr class='ctr'>\n";

	html = html + "<td class='ctd' colspan=2>";
	if (today.getYear() == this.shiftYears(-1).getYear()) {
		html = html + "<b>";
	}
	html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setYear("+vn+".getYear()-1).render()'>" + this.shiftYears(-1).getYearLabel() + "</a>";
	html = html + "</td>\n";
	html = html + "<td class='ctd' colspan=3>";
	if (today.getYear() == this.getYear()) {
		html = html + "<b>";
	}
	if (this.getMode() != Constants.YEARS) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMode(Constants.YEARS).render()'>";
	}
	html = html + this.getYearLabel();
	if (this.getMode() != Constants.YEARS) {
		html = html + "</a>";
	}
	if (today.getYear() == this.getYear()) {
		html = html + "</b>";
	}
	html = html + "</td>\n";
	html = html + "<td class='ctd' colspan=2>"
	if (today.getYear() == this.shiftYears(+1).getYear()) {
		html = html + "<b>";
	}
	html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setYear("+vn+".getYear()+1).render()'>" + this.shiftYears(+1).getYearLabel() + "</a>";
	if (today.getYear() == this.shiftYears(+1).getYear()) {
		html = html + "</b>";
	}
	html = html + "</td>\n";
	html = html + "</tr>\n";

	html = html + "<tr class='ctr'>\n";
	html = html + "<td class='ctd' colspan=2>";
	if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth(-1)) {
		html = html + "<b>";
	}
	if (Constants.MONTHS == this.getMode()) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMonth("+vn+".getMonth()-1);"+vn+".setMode(Constants.DAYS).render()'>";
	} else {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMonth("+vn+".getMonth()-1);"+vn+".render()'>";
	}
	html = html + this.shiftMonthsName(-1);
	html = html + "</a>";
	if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth(-1)) {
		html = html + "</b>";
	}
	html = html + "</td>\n";
	html = html + "<td class='ctd' colspan=3>";
	if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth()) {
		html = html + "<b>";
	}
	if (Constants.MONTHS != this.getMode()) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMode(Constants.MONTHS).render()'>";
	}
	html = html + this.getMonthName();
	if (Constants.MONTHS != this.getMode()) {
		html = html + "</a>";
	}
	if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth()) {
		html = html + "</b>";
	}
	html = html + "</td>\n";
	html = html + "<td class='ctd' colspan=2>";
	if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth(+1)) {
		html = html + "<b>";
	}
	if (Constants.MONTHS == this.getMode()) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMonth("+vn+".getMonth()+1);"+vn+".setMode(Constants.DAYS).render()'>";
	} else {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMonth("+vn+".getMonth()+1);"+vn+".render()'>";
	}
	html = html + this.shiftMonthsName(+1);
	html = html + "</a>";
	if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth(+1)) {
		html = html + "</b>";
	}
	html = html + "</td>\n";
	html = html + "</tr>\n";

	html = html + "<tr class='ctr'>\n";

	// if (Constants.DAYS == this.getMode()) {

	for (var x = 1; x <= 7; x++) {
		html = html + "<td class='ctd'";
		if (Constants.DAYS == this.getMode()) {
		} else {
			html = html + " style='color: "+Calendar.bgcolor+"'";
		}
		html = html + ">";

		if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth() && today.getDayOfWeek() == x) {
			html = html + "<b>";
		}
		html = html + Constants.daysOfWeek[x - 1].substring(0, 2); // Remember this isn't 0,3 and we've not lengthened "Mon" to "Monday" yet either.. (not insisting we should)
		if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth() && today.getDayOfWeek() == x) {
			html = html + "</b>";
		}

		html = html + "</td>";
	}

// }

	html = html + "</tr>\n";

	var daysThisMonth = this.getDaysInMonth();
	var daysLastMonth = this.getDaysInMonth(this.getMonth(-1));
	var offset = this.getDayOfWeekOffset();
	if (0 == offset) offset = 7;
  //var daysNextMonth = this.getDaysInMonth(this.getMonth(+1));

	var fillRight;
	for (var i = 1, y = 1, z = 0; y <= 6; y++) {
		fillRight = true; // = false; // false if we want not to render the extra empty row

		html = html + "<tr>\n";

		if (Constants.YEARS == this.getMode()) {

			var colspan = 1;
			for (var x = 1; x <= 3; x++) {
				if (z < 18) {
					if (false) {
					} else if (true == (0 == ((x + 2) % 3))) {
						colspan = 2;
					} else if (true == (0 == ((x + 1) % 3))) {
						colspan = 3;
					} else if (true == (0 == ((x + 0) % 3))) {
						colspan = 2;
					} else {
					}
					html = html + "<td class='ctd' colspan='" + colspan + "'>";
				  //if (today.getYear() != this.getYear()+z-7) {
						html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setYear(" + (this.getYear() + z - 7) + ").setMode(Constants.MONTHS).render()'>";
				  //}
					if (today.getYear() == this.getYear() + z - 7) {
						html = html + "<b>";
					}
					html = html + (this.getYear() + z - 7);
					if (today.getYear() == this.getYear() + z - 7) {
						html = html + "</b>";
					}
				  //if (today.getYear() != this.getYear()+z-7) {
						html = html + "</a>";
				  //}
					html = html + "</td>";
				}
				z = z + 1;
			}

		}

		if (Constants.MONTHS == this.getMode()) {

			var colspan = 1;
			for (var x = 1; x <= 3; x++) {
				if (false) {
				} else if (true == (0 == ((x + 2) % 3))) {
					colspan = 2;
				} else if (true == (0 == ((x + 1) % 3))) {
					colspan = 3;
				} else if (true == (0 == ((x + 0) % 3))) {
					colspan = 2;
				} else {
				}
				if (false) {
				} else if (z >= 0 && z < 3) {
					html = html + "<td class='ctd' colspan='" + colspan + "' style='color: "+Calendar.color+"'>";
					html = html + Constants.months[((z - 3 + 12) % 12)];
					html = html + "</td>";
				} else if (z >= 3 && z < 3 + 12) {
					html = html + "<td class='ctd' colspan='" + colspan + "'>";
				  //if (this.getMonth() != ((+1+z-3+12) % 12)) {
						html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".setMonth(" + (1 + ((z - 3 + 12) % 12)) + ").setMode(Constants.DAYS).render()'>";
				  //}
					if (today.getYear() == this.getYear() && today.getMonth() == 1 + ((z - 3 + 12) % 12)) {
						html = html + "<b>";
					}
					html = html + Constants.months[((z - 3 + 12) % 12)];
					if (today.getYear() == this.getYear() && today.getMonth() == 1 + ((z - 3 + 12) % 12)) {
						html = html + "</b>";
					}
				  //if (this.getMonth() != ((+1+z-3+12) % 12)) {
						html = html + "</a>";
				  //}
					html = html + "</td>";
				} else if (z >= 3 + 12 && z < 6 + 12) {
					html = html + "<td class='ctd' colspan='" + colspan + "' style='color: "+Calendar.color+"'>";
					html = html + Constants.months[((z - 3 + 12) % 12)];
					html = html + "</td>";
				} else {
				}
				z = z + 1;
			}

		}

		if (Constants.DAYS == this.getMode()) {
			for (var x = 1; x <= 7; x++) {
				if (z < offset) {
					// html = html + "<td class='ctd'>&nbsp;</td>"; // This line if we want to keep the cell empty
					html = html + "<td class='ctd' style='color: "+Calendar.color+"'>" + (daysLastMonth - offset + x) + "</td>";
				} else if (i <= daysThisMonth) {
					html = html + "<td class='ctd'>";
					// TODO: switch all three compares to one calendar object compare function
					if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth() && today.getDayOfMonth() == i) {
						html = html + "<b>";
					}
					if (false) {
					} else if (vp.getYear() == this.getYear() && vp.getMonth() == this.getMonth() && vp.getDayOfMonth() == i) {
					} else {
						html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".pickDay("+this.getYear()+", "+this.getMonth()+", " + i + ").render()'>";
					}
					html = html + i;
					if (false) {
					} else if (vp.getYear() == this.getYear() && vp.getMonth() == this.getMonth() && vp.getDayOfMonth() == i) {
					} else {
						html = html + "</a>";
					}
					if (today.getYear() == this.getYear() && today.getMonth() == this.getMonth() && today.getDayOfMonth() == i) {
						html = html + "</b>";
					}
					html = html + "</td>";
					fillRight = true;
					i = i + 1;
				} else if (true == fillRight) {
					// html = html + "<td align='center'>&nbsp;</td>"; // This line if we want to keep the cell empty
					html = html
					+ "<td class='ctd' style='color: "+Calendar.color+"'>"
					+ (z - offset + 1 - daysThisMonth) + "</td>";
				}
				z = z + 1;
			}

		}

		html = html + "</tr>\n";

	}

	// BEGINING: YESTERDAY / TODAY / TOMORROW

	html = html + "<tr>\n";

	var yesterday = today.shiftDays(-1);
	html = html + "<td class='ctd' colspan=2>";
	if (/*Constants.DAYS != this.getMode() || */yesterday.getYear() != this.getYear() || yesterday.getMonth() != this.getMonth() || yesterday.getDayOfMonth() != this.getDayOfMonth()) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".pickYesterday().setMode(Constants.DAYS).render()'>";
	}
	html = html + "yesterday";
	if (/*Constants.DAYS != this.getMode() || */yesterday.getYear() != this.getYear() || yesterday.getMonth() != this.getMonth() || yesterday.getDayOfMonth() != this.getDayOfMonth()) {
		html = html + "</a>"
	}
	html = html + "</td>\n";

	html = html + "<td class='ctd' colspan=3>";
	html = html + "<b>";
	if (/*Constants.DAYS != this.getMode() || */today.getYear() != this.getYear() || today.getMonth() != this.getMonth() || today.getDayOfMonth() != this.getDayOfMonth()) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".pickToday().setMode(Constants.DAYS).render()'>"
	}
	html = html + "today";
	if (/*Constants.DAYS != this.getMode() || */today.getYear() != this.getYear() || today.getMonth() != this.getMonth() || today.getDayOfMonth() != this.getDayOfMonth()) {
		html = html + "</a>";
	}
	html = html + "</b>";
	html = html + "</td>\n";

	var tomorrow = today.shiftDays(+1);
	html = html + "<td class='ctd' colspan=2>";
	if (/*Constants.DAYS != this.getMode() || */tomorrow.getYear() != this.getYear() || tomorrow.getMonth() != this.getMonth() || tomorrow.getDayOfMonth() != this.getDayOfMonth()) {
		html = html + "<a class='ca' href='javascript:void(0);' onclick='"+vn+".pickTomorrow().setMode(Constants.DAYS).render()'>";
	}
	html = html + "tomorrow";
	if (/*Constants.DAYS != this.getMode() || */tomorrow.getYear() != this.getYear() || tomorrow.getMonth() != this.getMonth() || tomorrow.getDayOfMonth() != this.getDayOfMonth()) {
		html = html + "</a>";
	}
	html = html + "</td>\n";

	html = html + "</tr>\n";

	html = html + "<tr>\n";
	
	html = html + "<td colspan=7>";
	html = html + "<table class='ct' width=\"100%\">\n";
	html = html + "<tr class='ctr'>\n";
	html = html + "<td class='ctd' width=\"49%\">\n";
	html = html + "<input id='"+vn+"#cancel' type='submit' value=' cancel ' onclick='"+vn+".visible(false).render();' />";
	html = html + "</td>\n";
	html = html + "<td width=\"2%\">\n";
	html = html + "</td>\n";
	html = html + "<td class='ctd' width=\"49%\">\n";
	html = html + "<input id='"+vn+"#choose' type='submit' value=' choose ' onclick='"+vn+".choose().visible(false).render();' />";
	html = html + "</td>\n";
	html = html + "</tr>\n";
	html = html + "</table>\n";
	html = html + "</td>\n";
	
	html = html + "</tr>\n";
	
	// ENDING: YESTERDAY / TODAY / TOMORROW

	html = html + "</table>\n";
	
	} else {
	
		var s = this.cssStyleHide();
		if (false == Constants.isIE) {
			{
				var append = '';
				if (undefined !== s) {
					append = '; '+s;
				}
				e.style = 'z-index: 0'+append;
			}
		} else {
			var sa = s.split(";");
			for (var key in sa) {
				e.style.setAttribute(key, sa[key]);
				alert(key+'='+sa[key]);
			}
			e.style.setAttribute('z-index', '0');
		}
		
	}
	
	if (undefined === e || null === e) {
	} else {
		e.innerHTML = html;
	}
	
	if (dr !== undefined) {
		var e1 = window.document.getElementById(this.variableName()+'#id');
		if (null === e1.onclick) {
			eval('e1.onclick = function(){'+vn+'.show('+dr.variableName()+');}');
		}
		if (true == this.visible()) {
			this.ontopof(dr);
		}
		var e2 = window.document.getElementById(dr.variableName()+'#id');
		if (null === e2.onclick) {
			eval('e2.onclick = function(){'+dr.variableName()+'.show('+vn+');}');
		}
	}
	
	var calendar = this;
	window.setTimeout(function(){/*
		var bodyRect = document.body.getBoundingClientRect();
		var e = document.getElementById(calendar.htmlElementId());
		var elemRect = e.getBoundingClientRect();
		var off = {
			top: bodyRect.top - elemRect.top,
			bottom: elemRect.bottom - bodyRect.bottom,
			left: bodyRect.left - elemRect.left,
			right: elemRect.right - bodyRect.right,
		};
		var size = {
			height: elemRect.top - elemRect.bottom,
			width: elemRect.right - elemRect.left,
		};
		if (off.left > 0) {
			var s = calendar.cssStyleShow();
			var sa = s.split(";");
			var append = '';
			for (var a = 0; a < sa.length; a++) {
				var pair = sa[a];
				var pa = pair.split(":");
				var key = pa[0].trim();
				var value = pa[1].trim();
			*//*if (key === 'left') {
					value = '50%';
				}*//*
			*//*if (key === 'transform') {
					// transform: translate(-50%, 0px) rotate(0deg);
					value = 'translate(-50%, 0px) rotate(0deg)';
				}*//*
				pair = key+": "+value;
				if (false == Constants.isIE) {
					if (append !== '') {
						append = append+'; ';
					}
					append = append+pair;
				} else {
					e.style.setAttribute(key, value);
				}
			}
			if (false == Constants.isIE) {
				e.style = 'z-index: 2'+append;
			} else {
				e.style.setAttribute('z-index', '2');
			}
		}
		if (off.right > 0) {
		  //alert('right!');
		}
		if (off.top > 0) {
		  //alert('top!');
		}
		if (off.bottom > 0) {
		  //alert('bottom!');
		}
  */}, 0);
	return this;
};
Calendar.prototype.ontopof = function(dr) {
	var e1 = window.document.getElementById(this.variableName()+'#id');
	var s1 = undefined;
	if (true == this.visible()) {
		s1 = this.cssStyleShow();
	} else {
		s1 = this.cssStyleHide();
	}
	var e2 = window.document.getElementById(dr.variableName()+'#id');
	var s2 = undefined;
	if (true == dr.visible()) {
		s2 = dr.cssStyleShow();
	} else {
		s2 = dr.cssStyleHide();
	}
	if (false == Constants.isIE) {
		{
			var append = '';
			if (undefined !== s1) {
				append = '; '+s1;
			}
			e1.style = 'z-index: 2'+append;
		}
		{
			var append = '';
			if (undefined !== s2) {
				append = '; '+s2;
			}
			e2.style = 'z-index: 1'+append;
		}
	} else {
		var sa1 = s1.split(";");
		for (var key in sa1) {
			e1.style.setAttribute(key, sa1[key]);
		}
		e1.style.setAttribute('z-index', '2');
		var sa2 = s2.split(";");
		for (var key in sa2) {
			e2.style.setAttribute(key, sa2[key]);
		}
		e2.style.setAttribute('z-index', '1');
	}
	return this;
}