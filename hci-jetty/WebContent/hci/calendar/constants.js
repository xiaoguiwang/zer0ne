var Constants = function() {};

//https://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser/9851769
Constants.isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Opera 8.0+
Constants.isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
Constants.isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification)); // Safari 3.0+ "[object HTMLElementConstructor]" 
Constants.isIE = /*@cc_on!@*/false || !!document.documentMode; // Internet Explorer 6-11
Constants.isEdge = !Constants.isIE && !!window.StyleMedia; //Edge 20+
Constants.isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime); // Chrome 1-71
Constants.isBlink = (Constants.isChrome || Constants.isOpera) && !!window.CSS; // Blink engine detection

// TODO: National Language Support
Constants.daysOfWeek = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
// TODO: Consider what needs to happen if/when someone wants to start the week on a Monday..
Constants.daysOfWeekOffset = {
	"Sun": 0,
	"Mon": 1,
	"Tue": 2,
	"Wed": 3,
	"Thu": 4,
	"Fri": 5,
	"Sat": 6,
};
Constants.months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
Constants.daysInMonth = {
	"Jan": 31,
	"Feb": undefined,
	"Mar": 31,
	"Apr": 30,
	"May": 31,
	"Jun": 30,
	"Jul": 31,
	"Aug": 31,
	"Sep": 30,
	"Oct": 31,
	"Nov": 30, 
	"Dec": 31
};

Constants.YEARS  = 'YEARS' ;
Constants.MONTHS = 'MONTHS';
Constants.DAYS   = 'DAYS'  ;
Constants.DAY    = 'DAY'   ;