function Data(meta) {
  //var __meta = undefined;
	
	this.__array = [];
	this.__map = {};
	
	if (undefined === meta) {
	  //window.alert('new Data() // constructor is not supported');
	} else {
		this.setMeta(meta);
	}
	da[di] = this;
	di++;
};
Data.prototype.setMeta = function(meta) {
	if (undefined === meta) {
	} else {
		this.__meta = meta;
		var fields = this.__meta.getFields();
		for (var f = 0; false == (undefined === fields || null === fields) && f < fields.length; f++) {
			this.addField(fields[f],null);
		}
	}
	return this;
};
Data.prototype.getMeta = function() {
	return this.__meta;
};
Data.prototype.id = function(id) {
	if (undefined === id || null === id) {
		return this.getValue("$id");
	} else {
		this.setValue("$id",id);
		this.__meta.addData(this,id);
		return this;
	}
};
Data.prototype.addField = function(field,value) {
  /*if (undefined === field || null === field) {
	}*/
  /*if (undefined === value || null === value) {
	}*/
	var name = field.getName();
	this.__map[name] = new DataField(field,value);
	this.__array.push(this.__map[name]);
	return this;
};
Data.prototype.getFields = function() {
	return this.__array;
};
Data.prototype.setValue = function(name,value) {
  /*if (undefined === name || null === name) {
	}*/
  /*if (undefined === value || null === value) {
	}*/
	this.__map[name].setValue(value);
	return this;
};
Data.prototype.getValue = function(name) {
  /*if (undefined === name || null === name) {
	}*/
	return this.__map[name].getValue();
};
Data.prototype.toString = function() {
	var s = this.getMeta().getName();
	for (var f = 0; f < this.__array.length; f++) {
		s = s + ":"+this.__array[f].toString();
	}
	return s;
};

Data.prototype.render = function(id) {
	window.alert('Data.prototype.render=function(id){..} // is not implemented');
};

function DataField(field,value) {
	Field.call(this,field.getName());
	
	this.__field = field;
	this.__value = undefined;
	
	this.setValue(value);
};
DataField.prototype = Object.create(Field.prototype);
DataField.prototype.constructor = DataField;
DataField.prototype.setValue = function(value) {
	if (undefined === value) {
	} else {
		this.__value = value;
	}
	return this;
};
DataField.prototype.getValue = function() {
	return this.__value;
};
DataField.prototype.toString = function() {
	var value = this.getValue();
	if (undefined === value) {
		value = '<undefined>';
	} else if (null === value) {
		value = '<null>';
	} else {
		value = "'"+value+"'";
	}
	return this.__field.getName()+"="+value;
};