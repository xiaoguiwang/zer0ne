// https://github.com/mdn/learning-area/blob/master/javascript/oojs/advanced/oojs-class-inheritance-finished.html

// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance

function MetaData(name) {
  //var __name = undefined;
  //var __id = undefined;
	
	this.__array = [];
	this.__map = {};
	this.__data = {};
	this.__draft = true;
	
	if (undefined === name) {
	  //window.alert('new MetaData() // constructor is not supported');
	} else {
		this.setName(name);
	}
	this.addField(new MetaDataField("$id",Constants.prototype.TEXT));
	this.id(MetaData.index++);
};
MetaData.index = 0;
MetaData.array = [];
MetaData.map = {};
MetaData.prototype.setName = function(name) {
	if (undefined === name) {
	} else {
		this.__name = name;
	}
	return this;
};
MetaData.prototype.getName = function() {
	return this.__name;
};
function getLabel(name) {
	var name = this.__name;
	if (undefined === name || null === name) {
		name = "";
	}
	return name;
};
MetaData.prototype.id = function(id) {
	if (undefined === id || null === id) {
		return this.__id;
	} else {
		this.__id = id;
		return this;
	}
};
MetaData.prototype.draft = function(isDraft) {
	if (undefined === isDraft || null === isDraft) {
		if (undefined == this.__draft || null == this.__draft) {
			this.__draft = true;
		}
		return this.__draft;
	} else {
		this.__draft = isDraft;
		return this;
	}
};
MetaData.prototype.addField = function(nameOrField,type) {
  /*if (undefined === nameOrField || null === nameOrField) {
	}*/
	var name = null;
	var field = null;
	if (undefined === type || null === type) {
		field = nameOrField;
		name = field.getName();
	} else {
		name = nameOrField;
		field = new MetaDataField(name,type);
	}
	this.__map[name] = field;
	this.__array.push(this.__map[name]);
	this.__fieldIndex = this.__array.length;
	return this;
};
MetaData.prototype.getFields = function() {
	return this.__array;
};
MetaData.prototype.getFieldById = function(f) {
	return this.__array[f];
}
MetaData.prototype.removeField = function(fieldIndex) {
	var mdf = this.__array[fieldIndex];
	var name = mdf.getName();
	delete this.__map[name];
	this.__array.splice(fieldIndex, 1);
	return this;
}
MetaData.prototype.setType = function(name,type) {
  /*if (undefined === name || null === name) {
	}*/
  /*if (undefined === type || null === type) {
	}*/
	this.__map[name].setType(type);
	return this;
};
MetaData.prototype.getType = function(name) {
  /*if (undefined === name || null === name) {
	}*/
	return this.__map[name].getType();
}
MetaData.prototype.addData = function(data,id) {
  /*if (undefined === data || null === data) {
	}*/
  /*if (undefined === id || null === id) {
	}*/
	if (undefined === this.__data) {
		this.__data = {};
	}
	this.__data[''+id] = data;
	return this;
};
MetaData.prototype.getData = function(id) {
	if (undefined === id || null === id) {
		return this.__data;
	} else {
		return this.__data[id];
	}
};
MetaData.prototype.toString = function() {
	var s = this.getName();
	for (var f = 0; f < this.__array.length; f++) {
		s = s + ":"+this.__array[f].toString();
	}
	return s;
};

MetaData.prototype.render = function(id) {
	window.alert('MetaData.prototype.render=function(id){..} // is not implemented');
};

function MetaDataField(name,type) {
	Field.call(this,name);
	
  //var __type = undefined;
	
	this.__map = {};
	
	this.setType(type);
};
MetaDataField.prototype = Object.create(Field.prototype);
MetaDataField.prototype.constructor = MetaDataField;
MetaDataField.prototype.setType = function(type) {
	if (undefined === type) {
	} else {
		this.__type = type;
	}
	return this;
};
MetaDataField.prototype.getType = function() {
	return this.__type;
};
MetaDataField.prototype.setAttr = function(name,value) {
  /*if (undefined === name || null == name) {
	}*/
  /*if (undefined === attr || null == attr) {
	}*/
	this.__map[name] = value;
	return this;
};
MetaDataField.prototype.getAttr = function(name) {
	return this.__map[name];
};
MetaDataField.prototype.toString = function() {
	return this.getName()+":"+this.getType();
};