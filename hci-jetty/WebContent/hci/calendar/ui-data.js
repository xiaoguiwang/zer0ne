function UiData() {
	if (undefined === UiData.array || null == UiData.array) {
		UiData.array = [];
		UiData.map = {};
	}
	var id = this.id();
	UiData.map[id] = this;
	UiData.array.push(UiData.map[id]);
	this.id(id+1);
};
UiData.prototype.id = function(uid) {
	if (undefined === uid || null === uid) {
		if (undefined === this.__id || null === this.__id) {
			this.__id = 0;
		}
		return this.__id;
	} else {
		if (undefined === this.__id || null === this.__id) {
		} else {
			UiData.map[uid] = UiData.map[this.__id];
			delete UiData.map[this.__id]; // Need to test!
		}
		this.__id = uid;
		return this;
	}
};
UiData.prototype.render = function(id,mode) {
	var e;
	
	if (undefined === id || null === id) {
	} else {
		if (null == id) {
		} else {
			e = document.getElementById(id);
		}
	}
	
	var html = "<table border=1 cellpadding=10 cellspacing=0>\n";
	
	if (undefined === mode || null === mode) {
	  //mode = Constants.prototype.CREATE;
		mode = Constants.prototype.READ  ;
	  //mode = Constants.prototype.UPDATE;
	  //mode = Constants.prototype.DELETE;
	}
	
	d = this;
	var md = d.getMeta();
	var mdfa = md.getFields();
	if (mode == Constants.prototype.CREATE) {
		var name = md.getName();
		html = html + "<tr><td colspan='3'>"+name+"</td></tr>";
	} else if (mode == Constants.prototype.READ) {
		html = html + "<tr>";
		for (var f = 0; f < mdfa.length; f++) {
			html = html + "<td>"+mdfa[f].getName()+"</td>";
		}
		html = html + "</tr>\n";
		var da = md.getData();
		for (var i = 0; i < di; i++) {
			d = da[i];
			html = html + "<tr>\n";
			for (var f = 0; f < mdfa.length; f++) {
				var name = mdfa[f].getName();
				var type = mdfa[f].getType();
				if ('$id' == mdfa[f].getName()) {
					html = html + "<td>$id</td>";
				  //continue;
				} else {
					var value = d.getValue(mdfa[f].getName());
					html = html + "<td>";
					if (Constants.prototype.TEXT == type) {
						var value = d.getValue(name);
						if (undefined === value) {
							html = html + '{undefined}';
						} else if (null === value) {
							html = html + '{null}';
						} else if (0 == value.length) {
							html = html + '{blank}';
						} else {
							html = html + value;
						}
					} else if (Constants.prototype.NUMBER == type) {
type = 'text'; // TODO: HTML Form input element/field type as number
						var value = d.getValue(name);
						html = html + value;
					}
					html = html + "</td>";
				}
			}
			html = html + "</tr>\n";
		}
	} else if (mode == Constants.prototype.UPDATE) {
		html = html + "<tr>";
		var name = md.getName();
		var __id = this.id(); // getValue("$id");
		html = html + "<td>"+name+"#"+__id+"</td><td>";
		html = html + "<input id='"+name+"#"+__id+"' name='"+name+"' type='hidden' value='"+__id+"'/>";
		html = html + "</td>";
		html = html + "</tr>\n";
	} else if (mode == Constants.prototype.DELETE) {
	}
	
	if (mode == Constants.prototype.CREATE || mode == Constants.prototype.UPDATE || mode == Constants.prototype.DELETE) {
		
		for (var f = 0; f < fields.length; f++) {
			html = html + "<tr>";
			var name = fields[f].getName();
			if ('$id' == name) {
				continue;
			}
			var type = fields[f].getType();
			html = html + "<td>"+name+":"+type+"</td><td>";
			if (Constants.prototype.TEXT == type) {
				html = html + "<input";
				if (mode == Constants.prototype.DELETE) {
					html = html + " disabled";
				}
				html = html + " id='"+name+"#id'";
				var maxlength = fields[f].getAttr('maxlength');
				if (null === maxlength) {
				} else {
					html = html + " maxlength="+maxlength;
				}
				var value = d.getValue(name);
				html = html + " name='"+name+"' type='"+type+"' value='"+value+"'/>";
			} else if (Constants.prototype.NUMBER == type) {
type = 'text'; // TODO: HTML Form input element/field type as number
				html = html + "<input";
				if (mode == Constants.prototype.DELETE) {
					html = html + " disabled";
				}
				html = html + " id='"+name+"#id'";
				var maxvalue = fields[f].getAttr('maxvalue');
				if (null === maxvalue) {
				} else {
					var maxlength = maxvalue.toString().length;
					html = html + " maxlength="+maxlength;
				}
				var value = d.getValue(name);
				html = html + " name='"+name+"' type='"+type+"' value='"+value+"'/>";
			}
			html = html + "</td>";
			html = html + "</tr>\n";
		}
		
		if (mode == Constants.prototype.CREATE) {
			html = html + "<tr><td align='center' colspan='2'><input id='' name='' type='submit' onclick='' value='create'/>&nbsp;<a href='javascript:void(0);' onclick=''>cancel</a></td></tr>\n";
		} else if (mode == Constants.prototype.READ) {
		} else if (mode == Constants.prototype.UPDATE) {
			html = html + "<tr><td align='center' colspan='2'><input id='' name='' type='submit' onclick='' value='update'/>&nbsp;<a href='javascript:void(0);' onclick=''>cancel</a></td></tr>\n";
		} else if (mode == Constants.prototype.DELETE) {
			html = html + "<tr><td align='center' colspan='2'><input id='' name='' type='submit' onclick='' value='delete'/>&nbsp;<a href='javascript:void(0);' onclick=''>cancel</a></td></tr>\n";
		}
	
	} else if (mode == Constants.prototype.READ) {
		html = html + "<tr><td align='center' colspan='4'>data:\n";
		html = html + "<a href='javascript:void(0);' onclick='d.render(\""+id+"\",Constants.prototype.CREATE)'>create</a>\n";
		html = html + "<input id='' name='' type='submit' onclick='d.render(\""+id+"\",Constants.prototype.READ)' value='read'/>\n"
		html = html + "<input id='' name='' type='submit' onclick='d.render(\""+id+"\",Constants.prototype.UPDATE)' value='update'/>\n"
		html = html + "<input id='' name='' type='submit' onclick='d.render(\""+id+"\",Constants.prototype.DELETE)' value='delete'/>\n"
		html = html + "</td></tr>\n";
		html = html + "<tr><td align='center' colspan='4'>meta-data:\n";
		html = html + "<a href='javascript:void(0);' onclick='md.render(\""+id+"\",Constants.prototype.READ)'>cancel</a>\n";
		html = html + "</td></tr>\n";
	}
	
	html = html + "</table>\n";
	
	if (undefined === e || null === e) {
		return html; // Freaky but funky
	} else {
		e.innerHTML = html;
	}
};