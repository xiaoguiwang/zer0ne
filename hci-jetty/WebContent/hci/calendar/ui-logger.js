function UiLogger(id) {
	this.__id = id;
	this.__html = '';
};
UiLogger.prototype.debug = function(message) {
	if (0 < this.__html.length) {
		this.__html = this.__html + "<br/>";
	}
	this.__html = this.__html + message;
	var e = document.getElementById(this.__id);
	if (undefined === e || null === e) {
		return this.__html; // Freaky but funky
	} else {
		e.innerHTML = this.__html;
		return this;
	}
}
UiLogger.prototype.clear = function() {
	this.__html = '';
	var e = document.getElementById(this.__id);
	if (undefined === e || null === e) {
		return this.__html; // Freaky but funky
	} else {
		e.innerHTML = this.__html;
		return this;
	}
}