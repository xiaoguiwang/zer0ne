UiMetaData.init = function() {
	UiMetaData.map = {};
	UiMetaData.array = [];
}
function UiMetaData() {
	if (undefined === UiMetaData.map || null == UiMetaData.map) {
		UiMetaData.init();
	}
	var id = this.id();
	UiMetaData.map[id] = this;
	UiMetaData.array.push(UiMetaData.map[id]);
	this.id(id+1);
// https://www.w3schools.com/html/html5_webstorage.asp
if (typeof(Storage) === "undefined") {
} else {
  //var jsonAsString = sessionStorage.getItem("meta-data");
	var jsonAsString = localStorage.getItem("meta-data");
	if (undefined === jsonAsString || null === jsonAsString) {
	} else {
alert(jsonAsString);
		this.setMetaDataByJsonString(jsonAsString);
	}
}
};
UiMetaData.prototype.id = function(uid) {
	if (undefined === uid || null === uid) {
		if (undefined === this.__id || null === this.__id) {
			this.__id = 0;
		}
		return this.__id;
	} else {
		if (undefined === this.__id || null === this.__id) {
		} else {
			UiMetaData.map[uid] = UiMetaData.map[this.__id];
			delete UiMetaData.map[this.__id]; // Need to test!
		}
		this.__id = uid;
		return this;
	}
};
UiMetaData.getById = function(uid) {
	var uimd = null;
	if (undefined === uid || null === uid) {
	} else {
		uimd = UiMetaData.map[uid];
	}
	return uimd;
};
UiMetaData.prototype.addMetaData = function(md) {
	if (undefined === md || null === md) {
	} else {
		if (undefined == this.__map || null === this.__map) {
			this.__map = {};
			this.__array = [];
		}
		var mdi = md.id();
		this.__map[mdi] = md;
		this.__array.push(this.__map[mdi]);
	}
	return this;
};
UiMetaData.prototype.getMetaData = function(mdi) {
	if (undefined == this.__array || null === this.__array) {
		this.__array = [];
		this.__map = {};
	}
	if (undefined === mdi || null === mdi) {
		return this.__array;
	} else {
		return this.__map[mdi];
	}
};
UiMetaData.prototype.setMetaDataByJsonString = function(jsonAsString) {
//	{ "meta-data": [
//		{ "name": "service", "draft": true, "fields": [
//			{ "name": "$id", "type": "TEXT" },
//			{ "name": "number2", "type": "TEXT" },
//			{ "name": "boolean3", "type": "BOOLEAN" }
//		] }
//	] }'
	var obj = JSON.parse(jsonAsString);
	var mda = obj["meta-data"];
	for (var i = 0; i < mda.length; i++) {
		var name = mda[i]["name"];
		var md = new MetaData(name, null); // FIXME: constructor is always null second parameter - it's never taken notice of
		md.draft(mda[i]["draft"]);
		var mdfa = mda[i]["fields"];
		for (var f = 0; f < mdfa.length; f++) {
			var name = mdfa[f]["name"];
			if ('$id' == name) {
				continue;
			}
			var type = mdfa[f]["type"];
			md.addField(new MetaDataField(name,type)); // md.addField(name, type);
		}
		this.addMetaData(md);
	}
}
UiMetaData.prototype.getMetaDataAsJsonString = function() {
	var obj = {}; obj['meta-data'] = [];
	var mda = this.getMetaData();
	for (var i = 0; i < mda.length; i++) {
		obj['meta-data'][i] = {}
		obj['meta-data'][i]["name"] = mda[i].getName();
		obj['meta-data'][i]["draft"] = mda[i].draft();
		obj['meta-data'][i]["fields"] = [];
		var mdfa = mda[i].getFields();
		for (var f = 0; f < mdfa.length; f++) {
			obj['meta-data'][i]["fields"][f] = {};
			obj['meta-data'][i]["fields"][f]["name"] = mdfa[f].getName();
			obj['meta-data'][i]["fields"][f]["type"] = mdfa[f].getType();
		}
	}
	return JSON.stringify(obj);
}

UiMetaData.changeMetaDataName = function(uid,mdi) {
	var uimd = UiMetaData.getById(uid);
	var metaDataNameId = metaDataName+"#id:"+uid+'-'+mdi;
	var md = uimd.getMetaData(mdi);
	var e = document.getElementById(metaDataNameId);
	md.setName(e.value);
	if (false) {
		;
	} else if (true) {
		localStorage.setItem("meta-data", uimd.getMetaDataAsJsonString());
	} else {
		;
	}
};
UiMetaData.changeMetaDataFieldName = function(uid,mdi,f) {
	var uimd = UiMetaData.getById(uid);
	var metaDataFieldNameId = metaDataFieldName+"#id:"+uid+'-'+mdi+'-'+f;
	var md = uimd.getMetaData(mdi);
	var mdf = md.getFieldById(f);
	var e = document.getElementById(metaDataFieldNameId);
	mdf.setName(e.value);
};
UiMetaData.changeMetaDataFieldType = function(uid,mdi,f) {
	var uimd = UiMetaData.getById(uid);
	var metaDataFieldTypeId = metaDataFieldType+"#id:"+uid+'-'+mdi+'-'+f;
	var md = uimd.getMetaData(mdi);
	var mdf = md.getFieldById(f);
	var e = document.getElementById(metaDataFieldTypeId);
	mdf.setType(e.value);
};
UiMetaData.clickMetaDataFieldAdd = function(uid,mdi) {
	var uimd = UiMetaData.getById(uid);
	var md = uimd.getMetaData(mdi);
	md.addField(new MetaDataField('',null));
};
UiMetaData.clickMetaDataFieldsRemove = function(uid,mdi) {
	var uimd = UiMetaData.getById(uid)
	var ea = document.getElementsByName(metaDataFieldCheckbox);
	for (var a = 0; a < ea.length; a++) {
		if (true == ea[a].checked) {
		  //var metaDataFieldNameId = metaDataFieldName+"#id:"+uid+"-"+mdi+"-"+f;
			var split = ea[a].id.split(':')[1].split('-');
			var uid = split[0], mdi = split[1], f = split[2];
			uimd.getMetaData(mdi).removeField(f);
		}
	}
	return this;
};

//TODO - this below (as of 2018/Aug/15th 10:20PM) is an initial implementation i.e. probably not final
UiMetaData.createMetaData = function(uid) {
	var uimd = UiMetaData.getById(uid);
	var md = new MetaData('',null).draft(true);
	uimd.addMetaData(md);
}
UiMetaData.readMetaData = function() { // FIXME to be used if it makes sense..
}
//TODO - this below (as of 2018/Aug/15th 10:20PM) is a draft implementation i.e. not final
UiMetaData.updateMetaData = function(uid,mdi) {
	var uimd = UiMetaData.getById(uid);
	var md = uimd.getMetaData(mdi);
	md.draft(false);
}
UiMetaData.deleteMetaData = function(uid,mdi) { // FIXME to be used
}
////
UiMetaData.types = [Constants.TEXT,Constants.NUMBER,Constants.BOOLEAN];
////
UiMetaData.prototype.render = function(md,mode) {
//log.debug('UiMetaData#uid='+this.id()+'.render(md='+md+',mode='+mode+')');
	var html = '';
	
	var name = md.getName();
	var label = getLabel(name);
	var mdfa = md.getFields();
	
	var uid = this.id();
	var mdi = md.id();
	for (var f = 0; null != mdfa && f < mdfa.length; f++) {
		html = html + "<tr __valign='top'>";
		
		if (0 == f) {
			var metaDataId = metaData+"#id";
			html = html + "<td rowspan='"+mdfa.length+"'>";
			if (mode == Constants.CREATE || mode == Constants.READ || mode == Constants.UPDATE) {
				var etc = ' disabled';
				html = html + "<input id='"+metaDataId+"' name='"+metaData+"' type='checkbox'"+etc+"/>";
			}
			html = html + "</td>";
			
			html = html + "<td align='center' rowspan='"+mdfa.length+"'>";
		  //html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+","+id+",Constants.READ)'>";
			var etc = ' disabled';
			if (mode == Constants.CREATE || mode == Constants.UPDATE) {
				if ('$id' != mdfa[f].getName()) {
					etc = ' enabled';
				}
			}
			html = html + mdi;
			html = html + "</td>"
			
			html = html + "<td rowspan='"+mdfa.length+"'>";
			var metaDataNameId = metaDataName+"#id:"+uid+"-"+mdi;
			if (mode == Constants.CREATE || mode == Constants.UPDATE) {
				html = html + "<input id='"+metaDataNameId+"' name='"+metaDataName+"' onchange='UiMetaData.changeMetaDataName("+uid+","+mdi+")' type='text' value='"+label+"'/>";
			} else if (mode == Constants.READ) {
				html = html + label;
			}
			html = html + "</td>"
		} else {
			etc = ' enabled';
		}
		
		html = html + "<td>";
		
		var metaDataFieldNameId = metaDataFieldName+"#id:"+uid+"-"+mdi+"-"+f;
		if (mode == Constants.CREATE || mode == Constants.UPDATE) {
			html = html + "<input id='"+metaDataFieldNameId+"' name='"+metaDataFieldName+"' onchange='UiMetaData.changeMetaDataFieldName("+uid+","+mdi+","+f+")' type='text' value='"+getLabel(mdfa[f].getName())+"'"+etc+"/>";
		} else if (mode == Constants.READ) {
			html = html + getLabel(mdfa[f].getName());
		}
		
		html = html + "</td><td>";
		
		var metaDataFieldTypeId = metaDataFieldType+"#id:"+uid+"-"+mdi+"-"+f;
		if (mode == Constants.CREATE || mode == Constants.UPDATE) {
			html = html + "<select id='"+metaDataFieldTypeId+"' name='"+metaDataFieldType+"' onchange='UiMetaData.changeMetaDataFieldType("+uid+","+mdi+","+f+")' type='text' value='"+mdfa[f].getType()+"'"+etc+">\n";
		}
		if (mode == Constants.CREATE || mode == Constants.UPDATE) {
			var __t__ = 0;
			if (mode == Constants.CREATE) {
				mdfa[f].setType(UiMetaData.types[__t__]);
			}
			for (var t = 0; t < UiMetaData.types.length; t++) {
				html = html + "<option";
				if (UiMetaData.types[t] == mdfa[f].getType()) {
					__t__ = t;
					html = html + " selected";
				}
				html = html + ">"+UiMetaData.types[t]+"</option>\n";
			}
		}
		if (mode == Constants.CREATE || mode == Constants.UPDATE) {
			html = html + "</select>\n";
		} else if (mode == Constants.READ) {
			html = html + mdfa[f].getType();
		}
		
		html = html + "</td><td>";
		
		var metaDataFieldCheckboxId = metaDataFieldCheckbox+"#id:"+uid+"-"+mdi+"-"+f;
		etc = ' disabled';
		if (mode == Constants.CREATE || mode == Constants.UPDATE) {
			if ('$id' != mdfa[f].getName()) {
				etc = ' enabled';
			}
		}
		html = html + "<input id='"+metaDataFieldCheckboxId+"' name='"+metaDataFieldCheckbox+"' type='checkbox'"+etc+"/>";
		
		html = html + "</td>";
		
		html = html + "</tr>\n";
	}
	
	return html;
};
////
UiMetaData.render = function(uid,id,mode) {
//log.debug('UiMetaData.render(uid='+uid+',id="'+id+'",mode='+mode+')');
	var e;

	if (undefined === id || null === id) {
	} else {
		if (null == id) {
		} else {
			e = document.getElementById(id);
		}
	}
	
	var html = "<table border=1 cellpadding=10 cellspacing=0>\n";
	
	if (undefined === mode || null === mode) {
	  //mode = Constants.CREATE;
		mode = Constants.READ  ;
	  //mode = Constants.UPDATE;
	  //mode = Constants.DELETE;
	}
//log.debug('UiMetaData.render(uid='+uid+',id="'+id+'",mode\'='+mode+')');
	
	var uimd = UiMetaData.getById(uid);
//log.debug('UiMetaData#uid='+uid+' '+uimd.getMetaDataAsJsonString());
	
	if (mode == Constants.CREATE) {
		if (0 == uimd.getMetaData().length) { // FIXME: check for non-zero and drafts
			uimd.addMetaData(new MetaData('',null));
		}
	}
	
	if (mode == Constants.CREATE || mode == Constants.READ || mode == Constants.UPDATE) {
		
		html = html + "<tr>";
		
		var mda = uimd.getMetaData();
		var drafts = 0;
		for (var i = 0; i < mda.length; i++) {
			if (true == mda[i].draft()) {
				drafts = drafts + 1;
			}
		}
		html = html + "<td colspan='6'>drafts#"+drafts+"/"+mda.length+"</td>";
		
		html = html + "</tr>";
		
		html = html + "<tr>";
		
		html = html + "<td></td>";
		html = html + "<td>$id</td>";
		html = html + "<td>meta$data$name</td>";
		html = html + "<td>meta$data$field$name</td>";
		html = html + "<td>meta$data$field$type</td>";
		var metaDataFieldAllCheckboxEnabledLabel = ' disabled'; // = ' enabled';
		if (mode == Constants.CREATE) {
			var mda = uimd.getMetaData();
			if (1 <= mda.length) {
				for (var i = 0; i < mda.length; i++) {
					if (2 <= mda[i].getFields().length) {
						metaDataFieldAllCheckboxEnabledLabel = ' enabled';
						break;
					}
				}
			}
		}
		var metaDataFieldId = metaDataField+"#id:"+uid;
		var moreThanOneMetaDataField = false;
		html = html + "<td><input id='"+metaDataFieldId+"' name='meta$data$field' type='checkbox'"+metaDataFieldAllCheckboxEnabledLabel+"/>";
		
		html = html + "</tr>";
		
		if (mode == Constants.CREATE) {
			var mda = uimd.getMetaData();
//log.debug('mda.length='+mda.length);
			for (var i = 0; i < mda.length; i++) {
//log.debug('i='+i);
				var md = mda[i];
				var mdi = md.id();
//log.debug('md.draft()='+md.draft());
				if (true == md.draft()) {
					html = html + uimd.render(md,mode);
				}
				if (1 < md.getFields().length) {
					moreThanOneMetaDataField = true;
				}
				html = html + "<tr><td align='center' colspan='6'>meta-data-field:\n";
				html = html + "<input id='meta$data$field#id:add' name='meta$data$field$add' type='submit' onclick='UiMetaData.clickMetaDataFieldAdd("+uid+","+mdi+");UiMetaData.render("+uid+",\""+id+"\",Constants.CREATE)' value='add'/>\n";
				if (mode == Constants.CREATE || mode == Constants.READ || mode == Constants.UPDATE) {
					if (true == moreThanOneMetaDataField) {
						html = html + "<input id='meta$data$field#id:remove' name='meta$data$field$remove' type='submit' onclick='UiMetaData.clickMetaDataFieldsRemove("+uid+","+mdi+");UiMetaData.render("+uid+",\""+id+"\",Constants.CREATE)' value='remove'/>\n";
					}
				}
				html = html + "</td></tr>\n";
			}
		} else if (mode == Constants.READ) {
			var mda = uimd.getMetaData();
			for (var i = 0; i < mda.length; i++) {
				var md = mda[i];
				if (false == md.draft()) {
					html = html + uimd.render(md,mode);
				}
				if (1 < md.getFields().length) {
					moreThanOneMetaDataField = true;
				}
			}
		} else if (mode == Constants.UPDATE) {
			var mda = uimd.getMetaData();
			for (var i = 0; i < mda.length; i++) {
				var md = mda[i];
				html = html + uimd.render(md,mode);
				if (1 < md.getFields().length) {
					moreThanOneMetaDataField = true;
				}
			}
		}
//log.debug('uimd.getMetaData().length='+uimd.getMetaData().length);
		
		html = html + "<tr><td align='center' colspan='6'>meta-data:\n"; // Always render this button/hyper-link row, regardless
		if (mode == Constants.CREATE) {
			html = html + "<input id='meta$data#id:create' name='meta$data' type='submit' onclick='UiMetaData.updateMetaData("+uid+","+mdi+");UiMetaData.render("+uid+",\""+id+"\",Constants.READ)' value='create'/>\n"
			html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+",\""+id+"\",Constants.READ)'>cancel</a>\n";
		} else if (mode == Constants.READ) {
			html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+",\""+id+"\",Constants.CREATE)'>create</a>\n";
			if (0 < uimd.getMetaData().length) {
				html = html + "<input id='meta$data#id:update' name='meta$data' type='submit' onclick='UiMetaData.updateMetaData("+uid+");UiMetaData.render("+uid+",\""+id+"\",Constants.READ)' value='update'/>\n";
				html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+",\""+id+"\",Constants.READ)'>cancel</a>\n";
			}
		} else {
			html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+",\""+id+"\",Constants.READ)'>cancel</a>\n";
		}
		html = html + "</td></tr>\n";
	
	} else if (mode == Constants.READ) {
		html = html + "<tr>";
		html = html + "<td></td>";
		html = html + "<td>$id</td>";
		html = html + "<td>meta-data-name</td>";
		html = html + "<td>meta-data-field</td>";
		html = html + "<td>meta-data-type</td>";
		html = html + "<td></td>";
		html = html + "</tr>";
		for (var i = 0; i < mdi; i++) {
			var mda = UiMetaData.getMetaData();
			var name = mda[i].getName();
			var mdfa = mda[i].getFields();
			for (var f = 0; f < mdfa.length; f++) {
				html = html + "<tr>";
				if (0 == f) {
					html = html + "<td rowspan='"+mdfa.length+"'>";
					html = html + "<input id='meta$data$checkbox#id:"+uid+"-"+mda[i].id()+"' name='meta$data$checkbox' __onclick='UiMetaData.clickMetaDataCheckbox("+uid+","+mda[i].id()+")' type='checkbox'";
				  //html = html + " checked";
					html = html + "/>";
					html = html + "</td>";
					html = html + "<td rowspan='"+mdfa.length+"'>";
				  //html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+","+id+",Constants.UPDATE)'>";
					html = html + mda[i].id();
				  //html = html + "</a>";
					html = html + "</td>";
					html = html + "<td rowspan='"+mdfa.length+"'>";
				  //html = html + "<a href='javascript:void(0);' onclick='UiMetaData.render("+uid+","+id+",Constants.UPDATE)'>";
					html = html + mda[i].getName();
				  //html = html + "</a>";
					html = html + "</td>"
				}
				html = html + "<td>";
				html = html + mdfa[f].getName();
				html = html + "</td><td>";
				html = html + mdfa[f].getType();
				html = html + "</td><td>";
				html = html + "</td>";
				html = html + "</tr>\n";
			}
		}
		
		html = html + "<tr><td align='center' colspan='6'>meta-data:\n";
		html = html + "<a href='javascript:void(0);' onclick='MetaData.prototype.render(\""+id+"\",Constants.CREATE)'>create</a>\n";
	  //html = html + "<input id='' name='' type='submit' onclick='mda[__mdi__].render(\""+id+"\",Constants.READ)' value='read'/>\n"
		html = html + "<input id='' name='' type='submit' onclick='mda[__mdi__].render(\""+id+"\",Constants.UPDATE)' value='update'/>\n"
		html = html + "<input id='' name='' type='submit' onclick='mda[__mdi__].render(\""+id+"\",Constants.READ)' value='delete'/>\n"
		html = html + "</td></tr>\n";
		
		html = html + "<tr><td align='center' colspan='6'>data:\n";
		html = html + "<input id='' name='' type='submit' onclick='da[0].render(\""+id+"\",Constants.CREATE)' value='create'/>\n"
		html = html + "<input id='' name='' type='submit' onclick='da[0].render(\""+id+"\",Constants.READ)' value='read'/>\n"
	  //html = html + "<input id='' name='' type='submit' onclick='da[0].render(\""+id+"\",Constants.UPDATE)' value='update'/>\n"
	  //html = html + "<input id='' name='' type='submit' onclick='da[0].render(\""+id+"\",Constants.READ)' value='delete'/>\n"
		html = html + "</td></tr>\n";
		
	// } else if (mode == Constants.UPDATE) {
		
	} else if (mode == Constants.DELETE) {
	
	}
	
	html = html + "</table>\n";
	
	if (undefined === e || null === e) {
		return html; // Freaky but funky
	} else {
		e.innerHTML = html;
		return md;
	}
};