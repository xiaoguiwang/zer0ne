// hci/user/sign/page.js

function getHtmlForUserSignPage() {
	var html = '\
\
<form action="javascript:void(0);" id="id#form" method="POST" onsubmit="zer0ne();">\
<table border=0 cellpadding=4 cellspacing=0 height="100%">\
<tr>\
<td width="50"></td>\
<td align="center">\
<b style="font-family: Lucida Console, monospace; font-size: 4.0em; text-decoration: underline; font-weight: bold">Zer0ne</b>\
</td>\
<td width="50"></td>\
</tr>\
<tr>\
<td id="id#td-1">\
</td>\
<td>\
<input id="id#user" type="text" maxlength=25 size=25 disabled />\
</td>\
<td></td>\
</tr>\
<tr>\
<td>\
<img id="id#img-1" height=48 ondragstart="return false;" src="user.png" width=48 />\
</td>\
<td>\
<input id="id#zer0ne" type="text" maxlength=25 size=25 onkeydown="return __func_onkeydown(this);" />\
</td>\
<td></td>\
</tr>\
<tr>\
<td id="id#td-2">\
</td>\
<td>\
<input id="id#pass" type="password" maxlength=25 size=25 onkeydown="return __func_onkeydown(this);" disabled />\
</td>\
<td></td>\
</tr>\
<!--tr>\
<td></td>\
<td align="center">\
<input id="id#reset" type="submit" value="request password reset"/>\
</td>\
<td></td>\
</tr-->\
</table>\
</form>\
'	;
	return html;
}

function funcUserSignedIn() {
	var un = new Element('id#user');
	un.state(VISIBLE);
	var ue = new Element('id#email').value(user);
	ue.state(VISIBLE);
	var um = new Element('id#mobile');
	um.state(VISIBLE);
	var pe = new Element('id#pass');
	pe.state(HIGHLIGHT);
	var se = new Element('id#same');
	se.state(LOWLIGHT);
	return;
}
function __formPostUserKeyValue(id, jso) {
	var e = window.document.getElementById(id);
	jso[id] = e.value;
}
function formSubmitUserSetUp() {
	var returns = true;
	var jso = new Object();
	__formPostUserKeyValue('id#user'  , jso);
	__formPostUserKeyValue('id#email' , jso);
	__formPostUserKeyValue('id#mobile', jso);
	__formPostUserKeyValue('id#pass'   , jso);
	var x = AjaxPutRequest(user, JSON.stringify(jso));
	if (200 == x.status) {
		alert('Set-up!');
		returns = true;
	} else {
		alert('NOT Set-up!');
	  //returns = false;
	}
	return returns;
}
function getHtmlForUserSignedInPage() {
	var html = '\
\
<form action="javascript:void(0);" id="id#form" method="POST" onsubmit="formSubmitUserSetUp();">\
<table border=0 cellpadding=4 cellspacing=0 height="100%">\
<tr>\
<td width="50"></td>\
<td align="left">\
<b style="font-family: Lucida Console, monospace; font-size: 4.0em; text-decoration: underline; font-weight: bold">Signed-In</b>\
<a href="javascript:void(0);" onclick="formSubmitUserSetUp();">submit</a>\
</td>\
<td width="50"></td>\
</tr>\
<tr>\
<td id="id#td-1" style="font-family: Lucida Console, monospace; font-size: 3.0em">\
<img height=48 ondragstart="return false;" src="user.png" width=48 />\
</td>\
<td style="font-family: Lucida Console, monospace; font-size: 2.0em">\
User is one (1) or more of:<br/>\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td style="font-family: Lucida Console, monospace; font-size: 1.0em">\
&bull;&nbsp;You need to fill in at least one field below:<br/>\
<br/>\
&nbsp;A. user-name e.g. initial + last name<br/>\
<br/>\
<input id="id#user" type="text" maxlength=25 size=25/><br/>\
<br/>\
&nbsp;B. e-mail address e.g. user.name@domain.tld<br/>\
<br/>\
<input id="id#email" type="text" maxlength=25 size=25 value="'+user+'"/><br/>\
<br/>\
&nbsp;C. (cell/mobile) phone number<br/>\
<br/>\
<b>|</b> +1 <b>|</b> <input id="id#mobile" type="text" maxlength=25 size=22/>\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td style="font-family: Lucida Console, monospace; font-size: 1.0em">\
&nbsp;Note: we currently (only) support Canada and USA for text messaging.<br/>\
<br/>\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td style="font-family: Lucida Console, monospace; font-size: 2.0em">\
Pass-word/phrase must be eight (8) or more characters:\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td style="font-family: Lucida Console, monospace; font-size: 1.2em">\
A-Z/a-z &nbsp; 0-9 &nbsp; symbols ~@#$%^&*\\\'" &nbsp; punctuation .:;,?! &nbsp; brackets ({< &nbsp; math -=+/\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td style="font-family: Lucida Console, monospace; font-size: 1.0em">\
&bull;&nbsp;We <b>only</b> disallow dictionary words and the same character three (3) times in a row.<br/>\
&bull;&nbsp;We <u>don\'t</u> force you to change your password on a regular basis.<br/>\
&bull;&nbsp;We <u>won\'t</u> prevent you from using a password you\'ve used before.<br/>\
<br/>\
&nbsp;i. Enter your password below\
</td>\
<td></td>\
</tr>\
<tr>\
<td id="id#td-1">\
<img height=48 ondragstart="return false;" src="pass.png" width=48 />\
</td>\
<td>\
<input id="id#pass" type="password" maxlength=25 size=25 onkeydown="return __func_onkeydown(this);" />\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td style="font-family: Lucida Console, monospace; font-size: 1.0em">\
&nbsp;Note: You can click-and-hold the button beside the password to reveal it.<br/>\
<br/>\
&nbsp;ii. Enter your password again (if you like) to check you type the same password twice\
</td>\
<td></td>\
</tr>\
<tr>\
<td></td>\
<td>\
<input id="id#same" type="password" maxlength=25 size=25 onkeydown="return __func_onkeydown(this);" />\
</td>\
<td></td>\
</tr>\
</table>\
</form>\
'	;
	window.setTimeout(funcUserSignedIn, 0);
	return html;
}

function funcUserSigningUp() {
	var ue = new Element('id#user').value(user);
	ue.state(VISIBLE);
	var pe = new Element('id#pass');
	pe.state(HIGHLIGHT);
	var se = new Element('id#same');
	se.state(LOWLIGHT);
}
function getHtmlForUserSigningUpPage() {
	var html = '\
\
<form action="javascript:void(0);" id="id#form" method="POST" onsubmit="zer0ne();">\
<table border=0 cellpadding=4 cellspacing=0 height="100%">\
<tr>\
<td width="50"></td>\
<td align="left">\
<b style="font-family: Lucida Console, monospace; font-size: 4.0em; text-decoration: underline; font-weight: bold">Signing-Up</b>\
</td>\
<td width="50"></td>\
</tr>\
<tr>\
<td>\
<img height=48 ondragstart="return false;" src="user.png" width=48 />\
</td>\
<td>\
<input id="id#user" type="text" maxlength=25 size=25 value="'+user+'" disabled />\
</td>\
<td></td>\
</tr>\
<tr>\
<td id="id#td-1">\
<img height=48 ondragstart="return false;" src="pass.png" width=48 />\
</td>\
<td>\
<input id="id#pass" type="password" maxlength=25 size=25 onkeydown="return __func_onkeydown(this);" />\
</td>\
<td></td>\
</tr>\
<tr>\
<td id="id#td-2">\
<img height=48 ondragstart="return false;" src="pass.png" width=48 />\
</td>\
<td>\
<input id="id#same" type="password" maxlength=25 size=25 onkeydown="return __func_onkeydown(this);" />\
</td>\
<td></td>\
</tr>\
</table>\
</form>\
'	;
	window.setTimeout(funcUserSigningUp, 0);
	return html;
}