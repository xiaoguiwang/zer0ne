// This file: index.js

function getJSessionId() {
	var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
	if (null != jsId) {
		if (jsId instanceof Array) {
			jsId = jsId[0].substring(11);
		} else {
			jsId = jsId.substring(11);
		}
	}
	return jsId;
}

// https://dev.to/karataev/set-css-styles-with-javascript-3nl5

Element = function(id) {
	this.id = id;
	return this;
}

Element.prototype.state = function(state) {
	var result;
	if (state === undefined) {
		result = this.__state;
	} else {
		var e = window.document.getElementById(this.id);
		if (false == isIE) {
		} else {
			e.style.setAttribute('border-radius', '10px');
			e.style.setAttribute('border-style', 'solid');
			e.style.setAttribute('text-align', 'center');
		}
		if (INVISIBLE == state) {
			if (false == isIE) {
				e.style = "color: "+bgcolor+"; background-color: "+bgcolor+"; border-radius: 10px; border: 2px solid "+bgcolor+"; padding: 10px 10px 10px 10px; text-align: center";
			} else {
				e.style.setAttribute('color', bgcolor);
				e.style.setAttribute('background-color', bgcolor);
				e.style.setAttribute('border-width', '2px');
				e.style.setAttribute('border-color', bgcolor);
				e.style.setAttribute('padding', '10px 10px 10px 10px');
			}
			e.disabled = true;
			this.__state = state;
		} else if (VISIBLE == state) {
			if (false == isIE) {
				e.style = "color: "+fgcolor+"; background-color: "+darker+"; border-radius: 10px; border: 2px solid "+bgcolor+"; padding: 10px 10px 10px 10px; text-align: center";
			} else {
				e.style.setAttribute('color', fgcolor);
				e.style.setAttribute('background-color', lowlight);
				e.style.setAttribute('border-width', '2px');
				e.style.setAttribute('border-color', bgcolor);
				e.style.setAttribute('padding', '10px 10px 10px 10px');
			}
			e.disabled = true;
			this.__state = state;
		} else if (LOWLIGHT == state) {
			if (false == isIE) {
				e.style = "color: "+fgcolor+"; background-color: "+darker+"; border-radius: 10px; border: 2px solid "+nooutline+"; padding: 10px 10px 10px 10px; text-align: center";
			} else {
				e.style.setAttribute('color', fgcolor);
				e.style.setAttribute('background-color', darker);
				e.style.setAttribute('border-width', '2px');
				e.style.setAttribute('border-color', nooutline);
				e.style.setAttribute('padding', '10px 10px 10px 10px');
			}
			e.disabled = false;
			this.__state = state;
		} else if (HIGHLIGHT == state) {
			if (false == isIE) {
				e.style = "color: "+fgcolor+"; background-color: "+lighter+"; border-radius: 10px; border: 4px solid "+outline+"; padding: 8px 8px 8px 8px; text-align: center";
			} else {
				e.style.setAttribute('color', fgcolor);
				e.style.setAttribute('background-color', lighter);
				e.style.setAttribute('border-width', '4px');
				e.style.setAttribute('border-color', outline);
				e.style.setAttribute('padding', '8px 8px 8px 8px');
			}
			window.id = this.id;
			e.disabled = false;
			this.__state = state;
		} else {
		  //window.alert('Element: state='+state+' not supported');
		}
		result = this;
	}
	return result;
}
Element.prototype.value = function(value) {
	var result = '';
	if (value !== undefined) {
		this.__value = value;
		this.element().value = value;
		result = this;
	} else {
		if (this.element() === undefined) {
			this.__value = '';
		} else if (this.element().value !== undefined) {
			this.__value = this.element().value;
		} else {
			this.__value = '';
		}
		result = this.__value;
	}
	return result;
}
Element.prototype.element = function() {
	return window.document.getElementById(this.id);
}
Element.prototype.focus = function() {
	return this.element().focus();
}

var INVISIBLE = 'invisible';
var VISIBLE   = 'visible';
var LOWLIGHT  = 'lowlight';
var HIGHLIGHT = 'highlight';

var __user__   = new Element('id#user');
var __zer0ne__ = new Element('id#zer0ne');
var __pass__   = new Element('id#pass');

function funcState(state) {
	Logger.getLogger('zer0ne').log(Logger.FINER, "entering funcState("+state+")");
	var state_change = true;
	if (0 == state || state === undefined) {
		if (0 == window.state || window.state === undefined) {
			__user__.state(INVISIBLE);
			__zer0ne__.state(HIGHLIGHT).focus();
			__pass__.state(INVISIBLE);
		} else if (1 == window.state) {
			window.document.getElementById('id#td-1').innerHTML = ''; // FIXME
			__user__.state(INVISIBLE);
			__user__.element().value = '';
			window.document.getElementById('id#img-1').src = 'user.png'; // FIXME
			__zer0ne__.element().value = __user__.value();
			__zer0ne__.element().type = 'text';
			__zer0ne__.state(HIGHLIGHT).focus();
			funcSign(undefined, [0, 0, 1]);
		} else if (2 == window.state) {
			window.document.getElementById('id#td-1').innerHTML = ''; // FIXME
			__user__.state(INVISIBLE);
			__user__.element().value = '';
			window.document.getElementById('id#img-1').src = 'user.png'; // FIXME
			__zer0ne__.element().value = __user__.value();
			__zer0ne__.element().type = 'text';
			__zer0ne__.state(HIGHLIGHT).focus();
			window.document.getElementById('id#td-2').innerHTML = '';
			__pass__.state(INVISIBLE);
			funcSign(undefined, [0, 0, 1]);
		} else if (-1 == window.state) {
			// Sign-out: HEAD https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/sign?out={session:id}
			var x = AjaxHeadRequest(user);
			if (200 == x.status) {
			  //alert('Signed-out!');
				teardown();
				window.state = 0; // Mandatory before throwup();
				throwup(); // Yay!
				funcColor();
				funcSign(undefined, [0, 0, 1]); // in, up, out // TODO: roll this in to state transition?
				__user__.state(INVISIBLE);
				__zer0ne__.state(HIGHLIGHT).focus();
				__zer0ne__.value(user);
				__pass__.state(INVISIBLE);
			} else {
				alert('Error: NOT signed-out!');
			}
		} else if (-2 == window.state) {
			// Sign-out: HEAD https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/sign?out={session:id}
			var x = AjaxHeadRequest(user);
			if (200 == x.status) {
			  //alert('Signed-out!');
				teardown();
				window.state = 0; // Mandatory before throwup();
				throwup(); // Yay!
				funcColor();
				funcSign(undefined, [0, 0, 1]); // in, up, out // TODO: roll this in to state transition?
				__user__.state(INVISIBLE);
				__zer0ne__.state(HIGHLIGHT).focus();
				__zer0ne__.value(user);
				__pass__.state(INVISIBLE);
			} else {
				alert('Error: NOT signed-out!');
			}
		}
		if (window.state === undefined) {
			state = 0;
		}
	} else if (1 == state) {
		if (window.state === undefined) {
			__user__.state(VISIBLE);
			__zer0ne__.state(HIGHLIGHT).focus();
			__pass__.state(INVISIBLE);
		} else if (0 == window.state) {
			window.document.getElementById('id#td-1').innerHTML = '<img height=48 src="user.png" width=48 />';
			__user__.value(__zer0ne__.element().value);
			user = __user__.value(); // TODO
			__user__.element().value = __user__.value();
			__user__.state(VISIBLE);
			window.document.getElementById('id#img-1').src = 'pass.png';
			window.document.getElementById('id#img-1').onmousedown = function() {
				funcShow('id#zer0ne', 1);
			}
			__zer0ne__.element().value = '';
			__zer0ne__.state(HIGHLIGHT).focus();
			__zer0ne__.element().type = 'password';
			__pass__.state(INVISIBLE);
		} else if (1 == window.state) {
		} else if (2 == window.state) {
			__zer0ne__.state(HIGHLIGHT).focus();
			funcSign(undefined, [0, 0, 1]);
		}
	} else if (2 == state) {
		if (window.state === undefined) {
			__user__.state(VISIBLE);
			__zer0ne__.state(HIGHLIGHT).focus();
			__pass__.state(LOWLIGHT);
		} else if (0 == window.state) {
			window.document.getElementById('id#td-1').innerHTML = '<img height=48 src="user.png" width=48 />';
			__user__.state(VISIBLE);
			__user__.element().value = __zer0ne__.element().value;
			window.document.getElementById('id#img-1').src = 'pass.png';
			window.document.getElementById('id#img-1').onmousedown = function() {
				funcShow('id#zer0ne', 1);
			}
			__zer0ne__.element().value = '';
			__zer0ne__.element().type = 'password';
			__zer0ne__.state(HIGHLIGHT).focus();
			window.document.getElementById('id#td-2').innerHTML = '<img id="id#img-2" height=48 ondragstart="return false;" onmousedown="funcShow(\'id#pass\', 1)" src="pass.png" width=48 />';
			__pass__.state(LOWLIGHT);
			funcSign(undefined, [1, 0, 1]);
		} else if (1 == window.state) {
			window.document.getElementById('id#td-2').innerHTML = '<img id="id#img-2" height=48 ondragstart="return false;" onmousedown="funcShow(\'id#pass\', 1)" src="pass.png" width=48 />';
			if (__zer0ne__.element().value == '') {
				__zer0ne__.state(HIGHLIGHT).focus();
				__pass__.state(LOWLIGHT);
			} else {
				__zer0ne__.state(LOWLIGHT);
				__pass__.state(HIGHLIGHT).focus();
			}
			funcSign(undefined, [1, 0, 1]);
		} else if (2 == window.state) {
		}
	} else if (-2 == state) {
		if (2 == window.state) {
			user = __user__.value();
			var pass = __zer0ne__.value();
			var same = __pass__.value();
			// Sign-up: POST https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/sign?up={user}&pass={pass}&same={same} // Note: {pass} and {same} are url-encoded
			var x = AjaxPostRequest(user, pass, same);
			if (200 == x.status) {
			  //alert('Signed-up!');
				teardown();
				window.state = -2;
				throwup();
				funcColor();
				funcSign(undefined, [1, 1, 0]); // in, up, out
			} else {
				alert('Error: NOT signed-up!');
				state_change = false;
			}
		} else if (-2 == window.state) {
		}
	} else if (-1 == state) {
		if (1 == window.state) {
			user = __user__.value();
			var pass = __zer0ne__.value();
			// Sign-in: GET https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/sign?in={user}&pass={pass} // Note: {pass} are url-encoded
			var x = AjaxGetRequest(user, pass);
			if (200 == x.status) {
			  //alert('Signed-in!');
				teardown();
				window.state = -1;
				throwup(); // Yay!
				funcColor();
				funcSign(undefined, [1, 1, 0]); // in, up, out
			} else {
				alert('Error: NOT signed-in!');
				state_change = false;
			}
		} else if (-1 == window.state) {
		}
	}
	if (true == state_change) {
		if (window.state == state) {
			if (state >= 0) {
				__user__.state(__user__.state());
				__zer0ne__.state(__zer0ne__.state());
				__pass__.state(__pass__.state());
			}
		} else {
			window.state = state;
		}
	}
	Logger.getLogger('zer0ne').log(Logger.FINER, "exiting funcState("+state+")");
}

function __funcSign_style(id, color, textDecoration) {
	var e = window.document.getElementById(id);
	if (false == isIE) {
		if (textDecoration === undefined) {
			e.style = 'color: '+color;
		} else {
			e.style = 'color: '+color+"; text-decoration: "+textDecoration;
		}
	} else {
		e.style.setAttribute('color', color);
		if (textDecoration === undefined) {
			e.style.removeAttribute('text-decoration');
		} else {
			e.style.setAttribute('text-decoration', textDecoration);
		}
	}
	return e;
}
function __funcSign_render(id, f, ti, fa) {
	if (0 == f) {
		var e = __funcSign_style(id, outline, undefined);
		e.tabIndex = ti++;
		e.onclick = function() { funcSign(e, fa); };
	} else if (1 == f) {
		var e = __funcSign_style(id, nooutline, 'none');
		e.tabIndex = -1;
		e.onclick = undefined;
	}
	return ti;
}
function funcSign(e, fa) {
	Logger.getLogger('zer0ne').log(Logger.FINER, "entering funcSign()");
  //if (e === undefined) {
	if (fa !== undefined) {
		window.fa = fa;
	} else {
		fa = window.fa;
	}
		var ti = 4;
		ti = __funcSign_render('id#sign-in' , fa[0], ti, [1, 0, 0]);
		ti = __funcSign_render('id#sign-up' , fa[1], ti, [0, 1, 0]);
		ti = __funcSign_render('id#sign-out', fa[2], ti, [0, 0, 1]);
  //}
	if (e !== undefined) {
		if (0 == window.state || window.state === undefined) {
			if (e.id == 'id#sign-in') {
				var __e = __zer0ne__.element();
				if (__e !== undefined && __e.value != '') {
					funcState(1);
				} else {
					funcState(0);
				}
			} else if (e.id == 'id#sign-up') {
				var __e = __zer0ne__.element();
				if (__e !== undefined && __e.value != '') {
					funcState(2);
				} else {
					funcState(0);
				}
			}
		} else if (1 == window.state) {
			if (e.id == 'id#sign-in' || e.id == 'id#zer0ne') {
				var __e = e;
				if (__e.id == 'id#sign-in') {
					__e = __zer0ne__.element();
				}
				if (__e !== undefined && __e.value != '') {
					funcState(-1);
				} else {
					funcState(1);
				}
			} else if (e.id == 'id#sign-up') {
				funcState(2);
			}
		} else if (2 == window.state) {
			if (e.id == 'id#sign-up' || e.id == 'id#zer0ne' || e.id == 'id#pass') {
				var __e = e;
				if (__e.id == 'id#sign-up' || __e.id == 'id#pass') {
					__e = __zer0ne__.element();
				}
				var __f = __pass__.element();
				if (__e !== undefined && __e.value != '' && __f !== undefined && __f.value != '' && __e.value == __f.value) {
					funcState(-2);
				}
			}
		} else if (-2 == window.state && e.id == 'id#sign-out') {
			funcState(0); // Yay!
		} else if (-1 == window.state && e.id == 'id#sign-out') {
			funcState(0); // Yay!
		}
	}
	Logger.getLogger('zer0ne').log(Logger.FINER, "exiting funcSign()");
}
function __funcColor_style(id, color, textDecoration) {
	var e = window.document.getElementById(id);
	if (false == isIE) {
		if (textDecoration === undefined) {
			e.style = 'color: '+color;
		} else {
			e.style = 'color: '+color+"; text-decoration: "+textDecoration;
		}
	} else {
		e.style.setAttribute('color', color);
		if (textDecoration === undefined) {
			e.style.removeAttribute('text-decoration');
		} else {
			e.style.setAttribute('text-decoration', textDecoration);
		}
	}
	return e;
}
function __funcColor_render(id, c, ti) {
	var e = __funcColor_style(id, fgcolor, undefined);
	e.tabIndex = ti++;
	if (c == 'red') {
		e.onclick = function() { funcColor('red'); };
	} else if (c == 'yellow') {
		e.onclick = function() { funcColor('yellow'); };
	} else if (c == 'green') {
		e.onclick = function() { funcColor('green'); };
	} else if (c == 'cyan') {
		e.onclick = function() { funcColor('cyan'); };
	} else if (c == 'blue') {
		e.onclick = function() { funcColor('blue'); };
	} else if (c == 'magenta') {
		e.onclick = function() { funcColor('magenta'); };
	} else if (c == 'grey') {
		e.onclick = function() { funcColor('grey'); };
	}
	return ti;
}
function __funcBorder_render(id, borderColor, borderStyle, backgroundColor) {
	var e = window.document.getElementById(id);
	if (false == isIE) {
		if (borderStyle === undefined) {
			if (backgroundColor == undefined) {
				e.style = "border-color: "+borderColor;
			} else {
				e.style = "border-color: "+borderColor+"; background-color: "+backgroundColor;
			}
		} else {
			e.style = "border-color: "+borderColor+"; border-style: solid; background-color: "+backgroundColor;
		}
	} else {
		e.style.setAttribute('border-color', borderColor);
		if (borderStyle !== undefined) {
			e.style.setAttribute('border-style', 'solid');
		}
		if (backgroundColor !== undefined) {
			e.style.setAttribute('background-color', backgroundColor);
		}
	}
}
function __funcColor_set(c) {
	if (c == 'red') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#cc2222';
			lighter = '#ee2222'; darker = '#c51111';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#fafafa';
			lighter = '#ffffff'; darker = '#dddddd';
			outline = '#c51111'; nooutline = '#000000';
		}
	} else if (c == 'yellow') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#cccc22';
			lighter = '#eeee22'; darker = '#c5c511';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#fafafa';
			lighter = '#ffffff'; darker = '#dddddd';
			outline = '#c5c511'; nooutline = '#000000';
		}
	} else if (c == 'green') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#22cc22';
			lighter = '#22ee22'; darker = '#11c511';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#fafafa';
			lighter = '#ffffff'; darker = '#dddddd';
			outline = '#11c511'; nooutline = '#000000';
		}
	} else if (c == 'cyan') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#22cccc';
			lighter = '#22eeee'; darker = '#11c5c5';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#fafafa';
			lighter = '#ffffff'; darker = '#dddddd';
			outline = '#11c5c5'; nooutline = '#000000';
		}
	} else if (c == 'blue') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#2222cc';
			lighter = '#2222ee'; darker = '#1111c5';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#fafafa';
			lighter = '#ffffff'; darker = '#dddddd';
			outline = '#1111c5'; nooutline = '#000000';
		}
	} else if (c == 'magenta') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#cc22cc';
			lighter = '#ee22ee'; darker = '#c511c5';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#fafafa';
			lighter = '#ffffff'; darker = '#dddddd';
			outline = '#c511c5'; nooutline = '#000000';
		}
	} else if (c == 'grey') {
		if (false == invert) {
			fgcolor = '#ffffff'; bgcolor = '#444444';
			lighter = '#555555'; darker = '#383838';
			outline = '#ffffff'; nooutline = '#000000';
		} else {
			fgcolor = '#000000'; bgcolor = '#eeeeee';
			lighter = '#f2f2f2'; darker = '#d7d7d7';
			outline = '#000000'; nooutline = '#ffffff';
		}
	}
	head();
}
var invert = undefined;
function funcColor(c) {
	if (invert === undefined) {
		invert = false;
	}
	Logger.getLogger('zer0ne').log(Logger.FINER, "entering funcColor()");
	if (c === undefined) {
		if (window.color === undefined) {
			var x = Math.random();
			var y = 1/6;
			if (x < 0) {
			} else if (x < 1*y) {
				c = 'red';
			} else if (x < 2*y) {
				c = 'green';
			} else if (x < 3*y) {
				c = 'blue';
			} else if (x < 4*y) {
				c = 'yellow';
			} else if (x < 5*y) {
				c = 'cyan';
			} else if (x <= 6*y) {
				c = 'magenta';
			}
			window.color = c;
		} else {
			c = window.color;
		}
	/**/c = 'grey'; // Default to 'grey' for cross-browser eye-catcher testing
		__funcColor_set(c);
		window.color = c;
	} else {
		if (window.color == c) {
			invert = !invert;
		}
		__funcColor_set(c);
		window.color = c;
	}
	__funcBorder_render('id#table-1', lighter, 'solid'   , lighter  );
	__funcBorder_render('id#table-2', outline, 'solid'   , outline  );
	__funcBorder_render('id#table-3', darker ,  undefined, bgcolor  );
	var ti = 1;
	ti = __funcColor_render('id#red'    , 'red'    , ti);
	ti = __funcColor_render('id#green'  , 'green'  , ti);
	ti = __funcColor_render('id#blue'   , 'blue'   , ti);
	ti = __funcColor_render('id#yellow' , 'yellow' , ti);
	ti = __funcColor_render('id#cyan'   , 'cyan'   , ti);
	ti = __funcColor_render('id#magenta', 'magenta', ti);
	ti = __funcColor_render('id#grey'   , 'grey'   , ti);
	funcState(window.state);
	funcSign();
	Logger.getLogger('zer0ne').log(Logger.FINER, "exiting funcColor()");
}

var show = {};
function funcShow(__id, f) {
	Logger.getLogger('zer0ne').log(Logger.FINER, "entering funcShow()");
	Logger.getLogger('zer0ne').log(Logger.FINE, "__id="+__id+",f="+f);
	if (1 == f) {
		window.document.getElementById(__id).type = 'text';
	} else if (0 == f) {
		window.document.getElementById(__id).type = 'password';
	  //window.document.getElementById(window.id);//.focus(); // Return focus to the appropriate most recently clicked element --Q. Do we need to if auto-focus is enabled/working
	}
	show[__id] = f;
	Logger.getLogger('zer0ne').log(Logger.FINER, "exiting funcShow()");
	return false; // To prevent drag (which prevents text returning to password - sometimes)
}
/*
function funcMatch() {
	var a = window.document.getElementById('id#zer0ne');
	var b = window.document.getElementById('id#pass');
	if (b.value == a.value && b.value !== undefined && b.value != '') {
		__zer0ne__.state(HIGHLIGHT);
		__pass__.state(HIGHLIGHT);
	} else {
		__zer0ne__.state(LOWLIGHT);
		__pass__.state(LOWLIGHT);
	}
}
*/
function __func_onkeydown(e) {
	var result = true;
	if (window.id !== undefined) {
		 if (window.event.key === 'Escape') {
			e.value = ''; // DONE
		  //window.setTimeout(funcMatch, 0);
		} else if (window.event.key === 'Enter') {
			if (e.id == 'id#zer0ne') {
				if (0 == window.state) {
					if (e.value !== undefined && e.value != '') {
						funcState(1); // Yay!
					}
				} else if (1 == window.state) {
					if (e.value !== undefined && e.value != '') {
						funcState(-1); // Yay!
					}
				} else if (2 == window.state) { // e.id='id#zer0ne' and 2==window.state can't ever happen?!
					var __e = e;
					if (__e.id == 'id#sign-up' || __e.id == 'id#pass') {
						__e = __zer0ne__.element();
					}
					var __f = __pass__.element();
					if (__e !== undefined && __e.value != '' && __f !== undefined && __f.value != '' && __e.value == __f.value) {
						funcState(-2);
					}
				}
			} else if (e.id == 'id#pass') {
				var __e = e;
				if (__e.id == 'id#sign-up' || __e.id == 'id#pass') {
					__e = __zer0ne__.element();
				}
				var __f = __pass__.element();
				if (__e !== undefined && __e.value != '' && __f !== undefined && __f.value != '' && __e.value == __f.value) {
					funcState(-2);
				}
			}
		} else if (window.event.key === 'Backspace') {
			if (e.value == '') {
				if (1 == window.state) {
					funcState(0);
					result = false;
				} else if (2 == window.state) {
					if (e.id == 'id#zer0ne') {
						funcState(0);
						result = false;
					} else if (e.id == 'id#pass') {
						funcState(0);
						result = false;
					}
				}
			}
			// TODO: both values, or switch-aro between user|pass/pass|confirm fields
			// Note: include giving focus depending on which password input is/was empty
		  //window.setTimeout(funcMatch, 0);
		} else if (window.event.key === 'Tab') {
		  //window.alert('Tab!'); // Works!
		}
	}
	return result;
}
// https://www.w3schools.com/jsref/event_onfocusout.asp
function focusin() {
  //if (window.id == 'id#zer0ne' || window.id == 'id#pass') {
		var e = window.document.getElementById(window.id);
		if (e !== undefined && e !== null) {
			e.focus();
		}
  //}
}
function __func_setFocusHandler(__this) {
	var e = window.document.getElementById(__this.id);
	__this.focusout = function() {
		Logger.getLogger('zer0ne').log(Logger.FINER, "entering [on]focusout()");
		window.setTimeout(focusin, 0);
		Logger.getLogger('zer0ne').log(Logger.FINER, "exiting [on]focusout()");
		return true;
	};
	e.addEventListener('focusout', __this.focusout); // Note: use onfocusout when using HTML but use addEventListener when using JavaScript 
	e.onfocus = function() {
		Logger.getLogger('zer0ne').log(Logger.FINER, "entering [on]focus()");
		window.id = __this.id;
		if (__this.id == 'id#zer0ne') {
			__zer0ne__.state(HIGHLIGHT);//.focus();
		  //if (window.id == 'id#pass') {
				if (2 == window.state) {
					__pass__.state(LOWLIGHT);
				  //__pass__.element().autofocus = false;
				}
		  //}
		} else if (__this.id == 'id#pass') {
			__pass__.state(HIGHLIGHT);//.focus();
			__zer0ne__.state(LOWLIGHT);
		  //__zer0ne__.element().autofocus = false;
		}
	  //window.id = __this.id;
		Logger.getLogger('zer0ne').log(Logger.FINER, "exiting [on]focus()");
		return true;
	}
  //e.autofocus = true;
}
function main() {
	if (window.state === undefined || 0 == window.state) {
		if (window.state === undefined) {
			window.state = +0; // This default will give us sign-in page/screen
			window.state = -1; // This default will make us signed-in
		}
		throwup();
	}
	Logger.id('id#p');
	window.fa = [0, 0, 1];
	funcColor();
	if (window.state >= 0) {
	  //funcSign(undefined, window.fa);
		window.onmouseup = function() {
			if (1 == show['id#zer0ne']) {
				funcShow('id#zer0ne', 0);
			} else if (1 == show['id#pass']) {
				funcShow('id#pass', 0);
			}
		};
		funcState(window.state);
		__func_setFocusHandler(__zer0ne__);
		__func_setFocusHandler(__pass__);
	}
	Logger.getLogger('zer0ne').log(Logger.INFO, "READY");
}