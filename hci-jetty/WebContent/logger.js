// logger.js

var Loggers = new Object();

function Logger(name) {
	this.__name = name;
	Loggers[name] = this;
	this.__stack = new Stack(name, 0);
	return this;
}

// https://docs.oracle.com/javase/7/docs/api/java/util/logging/Level.html
Logger.OFF     = 'OFF';
Logger.SEVERE  = 'SEVERE'; // Highest
Logger.WARNING = 'WARNING';
Logger.INFO    = 'INFO';
Logger.CONFIG  = 'CONFIG';
Logger.FINE    = 'FINE';
Logger.FINER   = 'FINER';
Logger.FINEST  = 'FINEST'; // Lowest
Logger.ALL     = 'ALL';

Logger.id = function(__id) {
	Logger.__id = __id;
}

Logger.getLogger = function(name) {
	var logger = Loggers[name];
	if (null == logger) {
		logger = new Logger(name);
		Loggers[name] = logger;
	}
	return logger;
}

Logger.prototype.log = function(level, message) {
	this.__stack.push(new Date().toISOString()+' ['+level+'] '+message);
	var e = window.document.getElementById(Logger.__id);
	var html = '';
	var array = this.__stack.getAll();
	for (var i = 0; i < array.length; i++) {
		if (i > 0) {
			html = html + '<br/>';
		}
		html = html + array[i];
	}
	e.innerHTML = html;
}