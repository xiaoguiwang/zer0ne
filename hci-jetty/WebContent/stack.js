// stack.js

function Stack(name, size) {
	this.__name = name;
	this.__array = new Array();
	this.__index = 0;
	this.__length = 0;
	this.__size = size;
	return this;
}

Stack.prototype.push = function(object) {
	this.__array.push(object);
	this.__index++;
}

Stack.prototype.pop = function() {
	var result = this.__array.pop();
	this.__index--;
	return result;
}

Stack.prototype.peek = function() {
	return this.__array[this.__index];
}

Stack.prototype.getAll = function() {
	var __array = new Array();
	var s, f; // s=start f=finish
	if (false) {
		s = 1;
		f = s+this.__size-1;
	} else {
		f = this.__array.length;
		if (f > 0 && this.__size > 0) {
			s = f-this.__size+1; // TODO: when there aren't even this.__size in the array (i.e. this.__size < this.__array.length)
		}
	}
	var a, b;
	if (s == 1 && f < this.__array.length) {
		a = 0;
		b = this.__size-1-1;
		__array[this.__size-1] = '...';
	} else if (f == this.__array.length && s > 1) {
		__array[0] = '...';
		a = 1;
		b = this.__size-1;
	} else if (s > 1 && f < this.__array.length) {
		__array[0] = '...';
		a = 1
		b = this.__size-1-1;
		__array[this.__size-1] = '...';
	}
	for (var i = a; i <= b; i++) {
		__array[i] = this.__array[s-1+i];
	}
	return __array;
}