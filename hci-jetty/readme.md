# readme.md #

CSS input xml attribute: spellcheck="false"

https://www.mojohaus.org/keytool/keytool-maven-plugin/index.html
mvn keytool:clean --> NullPointerException
mvn keytool:generateKeyPair --> NullPointerException

http://commons.apache.org/proper/commons-daemon/procrun.html (http://commons.apache.org/proper/commons-daemon/download_daemon.cgi)
Note: use --Jvm (for in-process) so we don't need to provide different start/stop JVMs
Note: provide path expansion value to jvm.dll path e.g. C:\Program Files\Java\jdk1.8.0_201\jre\bin\server\jvm.dll

https://www.baeldung.com/deploy-to-jetty - Follow:
2. Project Setup
6. Deploying with Jetty Maven Plugin, including
6.2. Changing the ContextPath
6.3. Changing the Port, and <host> from -
- https://www.eclipse.org/jetty/documentation/9.4.x/jetty-maven-plugin.html
And then:
9.  Embedding Jetty in Eclipse using (see more notes below on Run-Jetty-Run
9.1. Embedding Jetty in Eclipse

Note: https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/ but
      doesn't have 6.1.26 (which was tested as working with J2ME 1.4[1.8]) on samsung tab 3 (lite)
Note: https://jar-download.com/artifacts/org.mortbay.jetty/jetty/6.1.26/source-code
      does have 6.1.26 but BEWARE/CAUTIOUS on the download link

Run-Jetty-Run 1.3.5 in Eclipse Marketplace (not Install New Software)
Ref. https://github.com/xzer/run-jetty-run/wiki/GettingStarted
Accept licenses etc.
Ref. https://marketplace.eclipse.org/content/run-jetty-run explicitly supports 6.x (whereas Eclipse Jetty supports 7.x+)

jetty.xml:
Ref. https://www.eclipse.org/jetty/documentation/current/reference-section.html#jetty-xml-configure
Ref. https://stackoverflow.com/questions/24644114/change-jetty-default-port

Jetty 6.1 API:
http://api.dpml.net/org/mortbay/jetty/6.1.0/org/mortbay/jetty/Server.html

mvn clean compile -Dmaven.test.skip=true package

Jetty XmlConfiguration..
https://www.eclipse.org/jetty/documentation/current/reference-section.html#jetty-xml-array

https://github.com/dekellum/jetty/blob/master/jetty-xml/src/main/resources/org/eclipse/jetty/xml/configure_6_0.dtd

###### ----T-H-E--E-N-D---- ######