package hci.user.sign;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mortbay.jetty.HttpStatus;

import dro.util.Properties;

// POST https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/sign?up={user}&pass={pass}&same={same}
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 0L;
private static Map<java.lang.String, java.lang.String> tmp = new HashMap<>();
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	private dro.data.Zer0ne zer0ne;// = null;
	
	{
	  //zer0ne = null;
	}
	
	public void init(final ServletConfig servletConfig) throws ServletException {
	  //servletConfig.getInitParameter("my.init.param");
		try {
			Properties.properties(new dro.util.Properties(Servlet.class));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		final java.lang.Class<?> clazz;// = null;
		try {
			clazz = Class.forName("dao.Zer0ne");
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		try {
			this.zer0ne = (dro.data.Zer0ne)clazz.newInstance();
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		}/*
	*//*final *//*dro.meta.Data md = dro.meta.Data.returns("user", dro.meta.Data.Return.Data);
		if (null == md) {
			md = new dro.meta.Data("user");
			{
				final dro.meta.data.Field mdf = new dro.meta.data.Field("ID"); // TODO/FIXME
				mdf.set("$type", "INTEGER"); // TODO/FIXME
				md.add(mdf);
			}
			{
				final dro.meta.data.Field mdf = new dro.meta.data.Field("USER#NAME"); // TODO/FIXME
				mdf.set("$type", "TEXT"); // TODO/FIXME
				md.add(mdf);
			}
			{
				final dro.meta.data.Field mdf = new dro.meta.data.Field("USER#EMAIL"); // TODO/FIXME
				mdf.set("$type", "TEXT"); // TODO/FIXME
				md.add(mdf);
			}
			{
				final dro.meta.data.Field mdf = new dro.meta.data.Field("USER#MOBILE"); // TODO/FIXME
				mdf.set("$type", "TEXT"); // TODO/FIXME
				md.add(mdf);
			}
			{
				this.zer0ne.zer0ne(dro.meta.Data.class, md, "create", (java.lang.Object[])null); // TODO: automagically if md doesn't exist yet
			}
		} else {
			this.zer0ne.zer0ne(dro.meta.Data.class, md, "select", (java.lang.Object[])null);
		}*/
	}
	
	private void doMethod(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  /*request.getSession()
		       .getServletContext()
		       .getInitParameter("my.context.param");*/
		boolean authenticated = false;
		final String remoteUser = request.getRemoteUser();
		if (null != remoteUser) {
			authenticated = true;
		}
	  /*final Principal userPrincipal = request.getUserPrincipal();
		if (null != userPrincipal) {
			final String name = userPrincipal.getName();
			if (null != name) {
			authenticated = true;
		}*/
	  /*final HttpSession httpSession = request.getSession();
		if (null != httpSession) {
		}*/
		if (true == authenticated && false == dro.lang.String.isNullOrTrimmedBlank((dro.lang.String)null)) {
		} else {
			response.setStatus(HttpStatus.ORDINAL_401_Unauthorized);
		}
	}
	// HEAD https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/user/sign/out?user={user}
	protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
		if (true == request.getRequestURI().endsWith("/user/sign/out")) {
			final String queryString = request.getQueryString();
		  /*final */String[] pairs = null;
			if (null != queryString) {
				pairs = queryString.split("&");
			}
			final Map<java.lang.String, java.lang.String> map = new HashMap<>();
			if (null != pairs) {
				for (final String pair: pairs) {
					final String[] split = pair.split("=");
					map.put(split[0], split[1]);
				}
			}
			if (true == map.containsKey("user")) {
				final java.lang.String user = map.get("user");
			  //final dro.meta.Data md = dro.meta.Data.returns("user", dro.Data.Return.Data);
			  //final dro.Data user = new dro.Data(md);
				// And, persist?..
				response.setStatus(HttpStatus.ORDINAL_200_OK);
			} else {
				response.setStatus(HttpStatus.ORDINAL_400_Bad_Request);
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	// POST https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/user/sign/up?user={user} "{ pass: [{pass},{same}] }"
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
		boolean flag = false;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == request.getRequestURI().endsWith("/user/sign/up")) {
			flag = true;
		}
	  /*final */Map<java.lang.String, java.lang.String> map = null;
	  /*final */StringBuffer sb = null;
		if (true == flag) {
			final String queryString = request.getQueryString();
		  /*final */String[] pairs = null;
			if (null != queryString) {
				pairs = queryString.split("&");
			}
			map = new HashMap<>();
			if (null != pairs) {
				for (final String pair: pairs) {
					final String[] split = pair.split("=");
					map.put(split[0], split[1]);
				}
			}
			final BufferedReader br = request.getReader();
			String line = "";
			while (line != null) {
				try {
					line = br.readLine();
					if (null == line) {
						break;
					}
					if (null == sb) {
						sb = new StringBuffer();
					}
					sb.append(line);
				} catch (final IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == request.getRequestURI().endsWith("/user/sign/up") && null != map && true == map.containsKey("user")) {
			final java.lang.String user = map.get("user");
			final String json = null == sb ? null : sb.toString();
		  /*final */dro.Data d = dto.Data.fromJson(json);
			if (true == d.get("pass") instanceof java.lang.Object[]) {
				final java.lang.Object[] oa = (java.lang.Object[])d.get("pass");
				final java.lang.String pass = (java.lang.String)oa[0];
				final java.lang.String same = (java.lang.String)oa[1];
				if (true == pass.equals(same)) {
					if (false == tmp.containsKey(user)) {
						tmp.put(user, pass); // TODO: add entry for partial activation and send out e-mail, including audit record of where request originated from in terms of [geo-]ip(?) and date/time stamp for dos
					  /*new dto.mail.Adapter().send(
							new dto.mail.Email()
								.to("alex@itfromblighty.ca")
								.subject("/zer0ne/hci/0.0.1-SNAPSHOT/user/sign/up?user="+user)
								.body(
									new dro.io.File("template.html").returns(dro.io.File.Return.Reader).read(dro.lang.String.Return.String)
								)
							*//*.attachment(
									new dto.mail.Email.Attachment(
										new dro.io.File("template.html")
									)
								)*//*
						);*/
						response.setStatus(HttpStatus.ORDINAL_200_OK);
					} else {
						response.setStatus(HttpStatus.ORDINAL_409_Conflict);
					}
				} else {
				  //response.setStatus(HttpStatus.ORDINAL_401_Unauthorized); // Don't reveal they didn't match
					response.setStatus(HttpStatus.ORDINAL_409_Conflict);
				}
			} else {
				response.setStatus(HttpStatus.ORDINAL_400_Bad_Request);
			}
		} else {
			response.setStatus(HttpStatus.ORDINAL_400_Bad_Request);
			throw new IllegalArgumentException();
		}
	}
	// PUT https://itfromblighty.ca:4443/zer0ne/hci/0.0.1-SNAPSHOT/user/set/up?user={user} "{ 'id#user': 'USER#NAME', 'id#email': 'USER#EMAIL', 'id#mobile': 'USER#MOBILE', 'id#pass': {pass} }"
	protected void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
		boolean flag = false;
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == request.getRequestURI().endsWith("/user/set/up")) {
			flag = true;
		}
	  /*final */Map<java.lang.String, java.lang.String> map = null;
	  /*final */StringBuffer sb = null;
		if (true == flag) {
			final String queryString = request.getQueryString();
		  /*final */String[] pairs = null;
			if (null != queryString) {
				pairs = queryString.split("&");
			}
			map = new HashMap<>();
			if (null != pairs) {
				for (final String pair: pairs) {
					final String[] split = pair.split("=");
					map.put(split[0], split[1]);
				}
			}
			final BufferedReader br = request.getReader();
			String line = "";
			while (line != null) {
				try {
					line = br.readLine();
					if (null == line) {
						break;
					}
					if (null == sb) {
						sb = new StringBuffer();
					}
					sb.append(line);
				} catch (final IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
	  /*if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == request.getRequestURI().endsWith("/user/set/up") && null != map) {
			final String json = null == sb ? null : sb.toString();
		*//*final *//*dro.Data u = dto.Data.fromJson(json);
			final dro.meta.Data md = dro.meta.Data.returns("user", dro.meta.Data.Return.Data);
			final dro.Data d = new dro.Data(md);
			{
				@SuppressWarnings("unchecked")
			*//*final *//*Map<java.lang.Object, dro.data.Field> dfs = (Map<java.lang.Object, dro.data.Field>)d.get("$dro$data$Field%id");
				for (final java.lang.Object id: dfs.keySet()) {
					final dro.data.Field df = (dro.data.Field)dfs.get(id);
					final dro.meta.data.Field mdf = (dro.meta.data.Field)df.get("$dro$meta$data$Field");
					final java.lang.String name = mdf.get("$id").toString();
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == name.equals("ID")) {
					  //d.set(df, (int)1); // TODO/FIXME - knowing we're auto-generated *in the database* (not in JVM heap)
					} else if (true == name.equals("USER#NAME")) {
						d.set(df, (java.lang.String)u.get("id#user"));
					} else if (true == name.equals("USER#EMAIL")) {
						d.set(df, (java.lang.String)u.get("id#email"));
					} else if (true == name.equals("USER#MOBILE")) {
						d.set(df, (java.lang.String)u.get("id#mobile"));
					} else if (true == name.equals("USER#PASS")) {
				  *//**//*d.set(df, (java.lang.String)u.get("id#pass"));
				*//*} else {*//*
					}
				}
				try {
					this.zer0ne.zer0ne(dro.Data.class, d, "insert", (java.lang.Object[])null);
					response.setStatus(HttpStatus.ORDINAL_200_OK);
				} catch (final RuntimeException e) {
					response.setStatus(HttpStatus.ORDINAL_400_Bad_Request);
					throw e;
				}
			}
		} else {
			response.setStatus(HttpStatus.ORDINAL_400_Bad_Request);
			throw new IllegalArgumentException();
		}*/
	}
	private void __void(){
	  //this.doMethod(request, response);
	  /*final HttpSession hs = request.getSession(true);
		final java.lang.String jsessionId = hs.getId();
		jsessionId.hashCode();*/
	  /*final java.io.PrintWriter pw = response.getWriter();
		pw.append("<h1>Hello World! {WebApp::Servlet}</h1>").flush();
		final Client c = new Client("http://127.0.0.1:4444/zer0ne/api/0.0.1-SNAPSHOT/dto/meta/Data");
		pw.append("<h2>"+c.response+"</h2>").flush();
		response.setStatus(HttpStatus.ORDINAL_200_OK);*/
	}
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
		if (true == request.getRequestURI().endsWith("/user/sign/in")) {
			final String queryString = request.getQueryString();
		  /*final */String[] pairs = null;
			if (null != queryString) {
				pairs = queryString.split("&");
			}
			final Map<java.lang.String, java.lang.String> map = new HashMap<>();
			if (null != pairs) {
				for (final String pair: pairs) {
					final String[] split = pair.split("=");
					map.put(split[0], 1==split.length?null:split[1]);
				}
			}
			if (true == map.containsKey("user")) {
				final java.lang.String user = map.get("user");
				if (true == map.get("pass").equals(tmp.get(user))) {
				  //final dro.meta.Data md = dro.meta.Data.returns("user", dro.Data.Return.Data);
				  //final dro.Data user = new dro.Data(md);
					// TODO: retrieve user based on some kind of filter/rule/search function/feature that DOES NOT YET exist (without writingg our own loop)
					response.setStatus(HttpStatus.ORDINAL_200_OK);
				} else {
					response.setStatus(HttpStatus.ORDINAL_401_Unauthorized);
				}
			} else {
				response.setStatus(HttpStatus.ORDINAL_400_Bad_Request);
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	protected void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
	  //this.doMethod(request, response);
	}
}