# readme.md #


https://serverfault.com/questions/695849/services-remain-in-failed-state-after-stopped-with-systemctl/695863#695863
```
..according to the Posix specification, "The exit status of a command that terminated because it received a signal shall be reported as greater than 128". (http://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_08_02)

Here the Java VM adds 128 + 15 and you get this exit code of 143.

This non zero exit code here make senses, as this allows to see that your java program exited because of an external signal, and you get a chance to find out which signal.
```