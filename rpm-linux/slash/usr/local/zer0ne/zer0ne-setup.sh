#!/bin/bash

# zer0ne-setup.sh

dirnam=$(dirname $0)
basnam=$(basename $0)

cd $dirnam
pwd=$(pwd)

umask 0027

mkdir -p $dirnam/logs

. $dirnam/zer0ne-setup.cf # username, domain, url
## For example:
#username=Alex.Russell@fcl.crs
hostname=$(hostname)
#domain=FCL.ad.crs/Prod
#url=https://vpn.itfromblighty.ca/api/heartbeat/v1

cat - << EOB > zer0ne-hostname-$hostname.properties
## ./zer0ne-hostname-$hostname.properties

dro.main.Context@client#username=$username
dro.main.Context@client#passwords=$pwd/passwords
dro.main.Context@client#hostname=$hostname
dro.main.Context@client#domain=$domain
dro.main.Context@client#url=$url

dro.main.Context#sleepInMilliseconds=30000
EOB
chmod 0640 zer0ne-hostname-$hostname.properties

mv logging.properties logging.properties~
cat logging.properties~|sed -e "s|\(java\.util\.logging\.FileHandler\.pattern\)=.*|\1=$pwd/logs/dro-zer0ne-%g.txt|">logging.properties
rm logging.properties~
chmod 0640 logging.properties

flag=
if [ ! -d "passwords/https/$hostname/" ]; then
  chmod u+w passwords/https
  mkdir passwords/https/$hostname/
  chmod g-rx passwords/https/$hostname/
  chmod u-w passwords/https
  flag=1
fi
touch passwords/https/$hostname/$username
chmod g-r passwords/https/$hostname/$username
echo -n Enter password for passwords/https/$hostname/$username file:
read -s password
echo
echo $password > passwords/https/$hostname/$username
chmod u-w passwords/https/$hostname/$username
if [ ! -z "$flag" ]; then
  chmod u-w passwords/https/$hostname/
fi

chmod u+w passwords/jks/
if [ ! -f 'passwords/jks/zer0ne' ]; then
  touch passwords/jks/zer0ne
  chmod g-r passwords/jks/zer0ne
fi
echo -n Enter password for passwords/jks/zer0ne file:
read -s password
echo
echo $password > passwords/jks/zer0ne
chmod u-w passwords/jks/zer0ne
chmod u-w passwords/jks/

whoami=$(whoami)
if [ "$whoami" = 'root' ]; then
 #cd $dirnam
  chown $id:$gid .
  chmod 0750 .
  find . -type d -exec chown $id:$gid {} \;
 #chmod to be sorted out next..
  find . -type f -exec chown $id:$gid {} \;
 #chmod to be sorted out next..
fi
#chmod 0640 zer0ne-hostname-$hostname.properties
chmod 0640 zer0ne.jar
#rm zer0ne.pid # chmod 0600 zer0ne.pid
chmod 0750 zer0ne.sh
chmod 0500 keystores
chmod 0400 keystores/*.jks # zer0ne.jks
chmod 0750 lib
chmod 640 lib/*.jar
#chmod 0640 log4j.properties
chmod 0640 logging.properties
chmod 0750 logs
find logs -name "zer0ne-*.txt*" -type f -exec chmod o-rwx {} \;
#rm nohup.out # chmod 0600 nohup.out
chmod 0500 passwords
find passwords -type d -exec chmod 0500 {} \;
find passwords -type f -exec chmod 0400 {} \;
#cd -

cd - &>/dev/null

exit $rc