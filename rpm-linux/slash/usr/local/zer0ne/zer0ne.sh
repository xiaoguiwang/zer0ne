#!/bin/bash

## This file: zer0ne.sh

dirnam=$(dirname $0)
basnam=$(basename $0)
zero=$(echo $basnam|sed 's/\(.*\)\..*/\1/')

rc=0

#DRYRUN=

umask=$(umask)
umask 0027

cd $dirnam

##export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/mqm/java/lib64
find lib -name "*.jar" -type f -print > /tmp/$zero.$$
CLASSPATH_JARS=''
while read jar
do
  if [ -z "$CLASSPATH_JARS" ]; then
    if [ -z "$CLASSPATH" ]; then
      CLASSPATH_JARS="$jar"
    else
      CLASSPATH_JARS=":$jar"
    fi
  else
    CLASSPATH_JARS="$CLASSPATH_JARS:$jar"
  fi
done < /tmp/$zero.$$
rm -f /tmp/$zero.$$
#export CLASSPATH_JARS

nohup() {
  echo $(which nohup) $@
  if [ -z "$DRYRUN" ]; then
    $(which nohup) $@
  fi
}

if [ -z "$pidfile" ]; then
  pidfile=$dirnam/$zero.pid
fi
nohup java -cp "$CLASSPATH$CLASSPATH_JARS" dro.main.Context 2>&1 & echo $! > $pidfile
rc=$?

cd - &>/dev/null

umask $umask

exit $rc