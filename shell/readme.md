# readme.md

export PATH=$PATH:.

. z0.fn

. z0.d/ref.fn
. z0-ref.ut

. z0.d/var/fn
. z0-var.ut

. z0.d/array.fn
. z0-array.ut

. z0.d/list.fn
. z0-list.fn

. z0.d/set.fn
. z0-set.fn

. z0.d/map.fn
. z0-map.fn
