# readme.md

dro.meta.data.*, dro.meta.*, and dro.* require -Djboss.vfs.forceCaseSensitive=true which is available in JBOSS 7 and Wildfly 14
ref. https://issues.redhat.com/browse/JBVFS-170 