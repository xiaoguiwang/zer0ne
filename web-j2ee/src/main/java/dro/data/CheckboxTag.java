package dro.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class CheckboxTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.d.id().toString();
		if (false == id.equals("{id}")) {
			jw.write("<input id='id:d-checkbox' name='d:checkbox' type='checkbox'/>\n"); 
		}
	}
}