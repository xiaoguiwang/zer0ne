package dro.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdDataContextTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.d.get(dro.data.Context.$id).toString();
		jw.write("<input name='d:dc:id' type='text' hidden=true value='"+id+"'/>\n");
		jw.write("<input id='id:d:dc' type='text' value='"+id+"' disabled/>\n");
	}
}