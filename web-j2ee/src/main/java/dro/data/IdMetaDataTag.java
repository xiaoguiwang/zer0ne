package dro.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdMetaDataTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.d.get(dro.meta.Data.$id).toString();
		jw.write("<input name='d:md:id' type='text' hidden=true value='"+id+"'/>\n");
		jw.write("<input id='id:d:md' type='text' value='"+id+"' disabled/>");
	}
}