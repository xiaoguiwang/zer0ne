package dro.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdTag extends __abstract_Tag {
	protected boolean href;// = false;
	
	{
	  //href = false;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.d.id().toString();
		jw.write("<input name='d:id' type='text' hidden=true value='"+id+"'/>\n");
		if (false == this.href) {
			jw.write("<input id='id:d' type='text' value='"+id+"' disabled/>\n");
		} else {
			final String[] href = (String[])pageContext.getRequest().getAttribute("d:href"); // TODO: go via d direct
			if (false == id.equals("{id}")) {
				jw.write("<a href='?"+href[0]+"d:id="+id+"&d:view=one"+href[1]+"'>"+id+"</a>\n");
			} else {
				jw.write("<input type='text' value='"+id+"' disabled/>\n");
			}
		}
	}
}