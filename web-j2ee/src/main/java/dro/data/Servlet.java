package dro.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.ServletTool;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;

	private static final java.lang.String POST = "POST";
	private static final java.lang.String GET = "GET";

	// private static final String className = Servlet.class.getName();

	// private static final java.util.logging.Logger logger =
	// dro.util.Logger.getLogger(className);

	protected dro.util.Properties p = null;

	{
		// p = null;
	}

	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}

	public dro.util.Properties properties() {
		return this.p;
	}

	/*
	 * public static final java.lang.Boolean includeHeader(final HttpServletRequest
	 * request, final java.lang.String key, final HttpServletResponse response)
	 * throws IOException, ServletException { return
	 * ServletTool.includeHeader(request, key, "/html_head_title_body.jsp",
	 * response); }
	 */
	public static final void includeBody(final HttpServletRequest request,
			final java.util.Map<java.lang.String, java.lang.String> map, final java.util.List<dro.Data> list,
			final HttpServletResponse response, /* final */boolean recurse) throws IOException, ServletException {
		/* final boolean */recurse = false;
		final String mdc_id = ServletTool.get(request, map, "mdc:id");
		final String md_id = ServletTool.get(request, map, "md:id");
		request.setAttribute("md-id", new String[] { map.get("md:id") });
		final String mdf_id = ServletTool.get(request, map, "mdf:id");
		request.setAttribute("mdf-id", new String[] { map.get("mdf:id") });
		final String dc_id = ServletTool.get(request, map, "dc:id");
		request.setAttribute("dc-id", new String[] { map.get("dc:id") });
		final String d_id = null;// ServletTool.get(request, map, "d:id");
		final String df_id = ServletTool.get(request, map, "df:id"); // FIXME: If/When a df is deleted this has to be
																		// updated!
		{
			final StringBuffer[] sb = new StringBuffer[] { new StringBuffer(), new StringBuffer() };
			ServletTool.appendIdAndView(sb[0], mdc_id, "mdc", false);
			ServletTool.appendIdAndView(sb[0], md_id, "md", true);
			ServletTool.appendIdAndView(sb[0], mdf_id, "mdf", true);
			ServletTool.appendIdAndView(sb[0], dc_id, "dc", true);
			ServletTool.appendIdAndView(null, d_id, "d", true);
			ServletTool.appendIdAndView(sb[1], df_id, "df", true);
			request.setAttribute("d:href", new String[] { sb[0].append("&").toString(), sb[1].toString() });
		}
		{
			final StringBuffer sb = new StringBuffer();
			/*
			 * final Map<String, String[]> map2 = request.getParameterMap(); for (final
			 * String key: map2.keySet()) { if (0 < sb.length()) sb.append("&");
			 * sb.append(key+"="+map2.get(key)[0]); }
			 */
			final java.util.Map<String, String> map3 = ServletTool.buildMap(request.getQueryString());
			for (final String key : map3.keySet()) {
				sb.append("&").append(key + "=" + map3.get(key));
			}
			// if (0 < sb.length()) sb.append("&");
			// sb.append("clear=d"); // Like ..&cancel=dc
			request.setAttribute("d:href:search-clear", sb.toString());
		}
		final String view = map.get("d:view");
		request.setAttribute("d:view", view);
		request.setAttribute("d:list", list);
		final java.util.List<java.lang.String> ids = new ArrayList<>();
		for (final dro.Data d : list) {
			ids.add(d.id().toString());
		}
		request.setAttribute("d:id", ids.toArray(new String[0]));

		// final String referer = request.getHeader("referer");
		{
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/Data.jsp");
			requestDispatcher.include(request, response);
		}
		if (true == recurse) {
			request.setAttribute("df:embed", true);
			request.setAttribute("df:view", map.get("df:view"));
			request.setAttribute("df:id", map.get("df:id"));
			request.setAttribute("df:d:id", map.get("df:d:id"));
			request.setAttribute("df:md:id", map.get("df:md:id"));

			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/data/Field");
			requestDispatcher.include(request, response);

			request.setAttribute("df:embed", false);
		}
	}
	/*
	 * public static final void includeFooter(final HttpServletRequest request,
	 * final HttpServletResponse response, final java.lang.Boolean embed) throws
	 * IOException, ServletException { ServletTool.includeFooter(request,
	 * "/_body_html.jsp", response, embed); }
	 */

	protected static java.util.List<dro.Data> createData(final dro.meta.Data md, final dro.data.Context dc,
			final java.lang.String[] ids, final java.lang.String[] keys, /* final */java.lang.String[][] arrays) {
		arrays = ServletTool.arrays(ids, arrays, true);
		final java.util.List<dro.Data> list = new ArrayList<>();
		for (int i = 0; i < arrays[0].length; i++) {
			final dro.Data d = dc.add(new dro.Data(md), dro.Data.Return.Data);
			for (int j = 0; j < keys.length; j++) {
				d.set(keys[j], arrays[j][i]);
			}
			list.add(d);
		}
		return list;
	}

	protected static boolean searchData(final dro.Data d,
			final java.util.Map<java.lang.Integer, java.lang.String> search) {
		/* final */boolean found = true;
		if (null == search) {
			found = true;
		} else {
			found = true;
			final java.util.Set<dro.data.Field> dfs = d.returns(dro.data.Field.Set.Return.Set);
			final java.util.Set<java.lang.Integer> ids = search.keySet();
			for (final java.lang.Integer id : ids) {
				for (final dro.data.Field df : dfs) {
					final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
					if (id == mdf.id()) {
						final java.lang.Object v = df.get("$value"); // FIXME!
						final java.lang.String value = null == v ? null : v.toString();
						final java.lang.String __search = search.get(id);
						if (null != __search && 0 < __search.trim().length() && null != value
								&& false == value.contains(__search)) {
							found = false;
							break;
						}
					}
				}
			}
		}
		return found;
	}

	protected static java.util.List<dro.Data> readData(final HttpServletRequest request, final dro.meta.Data md,
			final Map<java.lang.String, java.lang.String[]> parameters) {
		/* final */java.lang.String[] ids = parameters.get("d:id");
		final java.util.List<dro.Data> list = new ArrayList<>();
		final java.util.Map<java.lang.Integer, java.lang.String> ordering = new LinkedHashMap<>();
		if (null == md) {
			// TOOD: build d for all dc and all md in all mdc..
		} else {
			/* final */java.util.Map<java.lang.Integer, java.lang.String> search = null;
			{
				/* final */java.util.Set<dro.meta.data.Field> set = md.returns(dro.meta.data.Field.Set.Return.Set);
				for (final dro.meta.data.Field mdf : set) {
					final java.lang.Integer id = (java.lang.Integer) mdf.id();
					final java.lang.String k = "mdf#" + id + ":search";
					final java.lang.String[] values = parameters.get(k);
					request.setAttribute(k, values);
					final java.lang.String o = "mdf#" + id + ":order";
					final java.lang.String[] orders = parameters.get(o); // Note: if it's not clicked, it's not not-null
					request.setAttribute(o, orders);
					if (null != orders && 0 < orders.length) {
						ordering.put(id, orders[0]);
					}
					if (null != values && 0 < values.length) {
						if (null == search) {
							search = new java.util.LinkedHashMap<java.lang.Integer, java.lang.String>();
						}
						search.put(id, values[0]);
					}
				}
			}
			if (null != ids) {
				ids = ServletTool.ids(ids);
			}
			if (null == ids) {
				final java.util.Set<dro.Data> set = md.returns(dro.Data.Return.Set);
				for (final dro.Data d : set) {
					if (null == search || true == Servlet.searchData(d, search)) {
						list.add(d);
					}
				}
			} else {
				for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
					if (true == ids[j].equals("{id}"))
						continue;
					final dro.Data d = md.returns(new java.lang.Integer(ids[j]), dro.Data.Return.Data);
					if (null == d)
						continue;
					if (null == search || true == Servlet.searchData(d, search)) {
						list.add(d);
					}
				}
			}
		}
		// https://stackoverflow.com/questions/20483208/multiple-sort-in-java/20483654
		Collections.sort(list, new dro.util.CompareTool(md, ordering));
		return list;
	}

	protected static java.util.List<dro.Data> updateData(final dro.meta.Data md, /* final */java.lang.String[] ids,
			final java.lang.String[] keys, /* final */java.lang.String[][] arrays, final java.lang.String[] checkbox) {
		final String[] __ids = ServletTool.ids(ids, arrays);
		arrays = ServletTool.arrays(ids, arrays, false);
		ids = __ids;
		final java.util.List<dro.Data> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			final dro.Data d = md.returns(new java.lang.Integer(ids[i]), dro.Data.Return.Data);
			if (null == d)
				continue;
			final boolean update = null == checkbox
					|| (i < checkbox.length && null != checkbox[i] && true == checkbox[i].equals("on"));
			if (true == update) {
				for (int j = 0; j < keys.length; j++) {
					if (j > arrays.length - 1)
						continue;
					if (i > arrays[j].length - 1)
						continue;
					d.set(keys[j], arrays[j][i]);
				}
			}
			list.add(d);
		}
		return list;
	}

	protected static java.util.List<dro.Data> deleteData(final dro.meta.Data md, final dro.data.Context dc,
			/* final */java.lang.String[] ids, final java.lang.String[] checkbox) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.Data> list = new ArrayList<>();
		for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
			if (true == ids[j].equals("{id}"))
				continue; // FIXME
			final dro.Data d = md.returns(new java.lang.Integer(ids[j]), dro.Data.Return.Data);
			final boolean remove = null == checkbox
					|| (j < checkbox.length && null != checkbox[j] && true == checkbox[j].equals("on"));
			if (true == remove) {
				md.remove(d, dro.Data.Return.Data);
			} else {
				list.add(d);
			}
		}
		return list;
	}

	protected static java.util.List<dro.Data> buildDataStringArrays(final HttpServletRequest request,
			final java.lang.String view) {
		/* final */java.util.List<dro.Data> list = null;
		final Map<String, String[]> parameters = request.getParameterMap();
		final java.lang.String method = request.getMethod();

		final String[] mdc_id = parameters.get("mdc:id");
		/* final */dro.meta.data.Context mdc = null;
		if (null != mdc_id && 0 < mdc_id.length && false == mdc_id[0].equals("{id}")) {
			/* final dro.meta.data.Context */mdc = dro.meta.data.Context.returns(new java.lang.Integer(mdc_id[0]),
					dro.meta.data.Context.Return.Context);
		}
		final String[] md_id = parameters.get("md:id");
		/* final */dro.meta.Data md = null;
		if (null != md_id && 0 < md_id.length && false == md_id[0].equals("{id}") && null != mdc) {
			/* final dro.meta.Data */md = mdc.returns(new java.lang.Integer(md_id[0]), dro.meta.Data.Return.Data);
		}
		final String[] dc_id = parameters.get("dc:id");
		/* final */dro.data.Context dc = null;
		if (null != dc_id && 0 < dc_id.length && false == dc_id[0].equals("{id}") && null != mdc) {
			/* final dro.data.Context */dc = mdc.returns(new java.lang.Integer(dc_id[0]),
					dro.data.Context.Return.Context);
		}

		final String[] submit_d = parameters.get("submit:d");
		if (false == Boolean.TRUE.booleanValue()) {

			// >C<RUD
		} else if ((null == request.getAttribute("d:state") && true == POST.equals(method)) && null != submit_d
				&& 0 < submit_d.length && true == submit_d[0].equals("create")) {
			/* final java.util.List<dro.Data> */list = Servlet.createData(md, dc, parameters.get("d:id"),
					new java.lang.String[] { dro.meta.Data.$id, dro.data.Context.$id },
					new java.lang.String[][] { parameters.get("d:md:id"), parameters.get("d:dc:id") });

			// C>R<UD
		} else if (null != request.getAttribute("d:state") || (true == GET.equals(method) && null == submit_d)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (null != view && true == view.equals("one")) {
				/* final java.util.List<dro.Data> */list = Servlet.readData(request, md, parameters);
			} else if (null == view || true == view.equals("many")) {
				/* final java.util.List<dro.Data> */list = Servlet.readData(request, md, parameters);
			} else if (null == view || true == view.equals("data")) {
				/* final java.util.List<dro.Data> */list = Servlet.readData(request, md, parameters);
			} else {
				throw new IllegalStateException();
			}

			// CR>U<D
		} else if ((null == request.getAttribute("d:state") && true == POST.equals(method)) && null != submit_d
				&& 0 < submit_d.length && true == submit_d[0].equals("update")) {
			/* java.util.List<dro.Data> */list = Servlet.updateData(md, parameters.get("d:id"),
					new java.lang.String[] { dro.meta.Data.$id, dro.data.Context.$id },
					new java.lang.String[][] { parameters.get("d:md:id"), parameters.get("d:dc:id") },
					parameters.get("d:checkbox"));

			// CRU>D<
		} else if ((null == request.getAttribute("d:state") && true == POST.equals(method)) && null != submit_d
				&& 0 < submit_d.length && true == submit_d[0].equals("delete")) {
			/* final java.util.List<dro.Data> list = */Servlet.deleteData(md, dc, parameters.get("d:id"),
					parameters.get("d:checkbox"));
			/* final java.util.List<dro.Data> */list = Servlet.readData(request, md, parameters);

		} else {
			/* final java.util.List<dro.Data> */list = Servlet.readData(request, md, parameters);
		}

		if (0 == list.size() || null == view || true == view.equals("many") || true == view.equals("data")) {
			final dro.Data d;// = null;
			if (null == md) {
				d = new dro.Data().set("$id", "{id}").set(dro.meta.Data.$id, "{id#md}").set(dro.data.Context.$id,
						"{id#dc}");
			} else {
				d = new dro.Data(md) // FIXME: hack to get mdf:$type for dro.meta.data.Field.jsp
						.set("$id", "{id}").set(dro.meta.Data.$id, "{id#md}").set(dro.data.Context.$id, "{id#dc}");
			}
			list.add(d);
			if (null == md) {
			} else {
				md.remove(d);
			}
		}

		return list;
	}

	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		final Map<String, String[]> parameters = request.getParameterMap();

		try {
			// dro.lang.Event.entered(Servlet.class, this, "..", new
			// Object[]{request,response});

			// final java.lang.Boolean embed = Servlet.includeHeader(request, "d:embed",
			// response);

			final java.util.Map<java.lang.String, java.lang.String> map = ServletTool
					.buildMap(request.getQueryString());

			final String view = map.get("d:view");
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == POST.equals(method) && null == request.getAttribute("d:state")) {
				final java.util.List<dro.Data> list = Servlet.buildDataStringArrays(request, view);
				request.setAttribute("d:state", 1);
				final String[] submit_d = parameters.get("submit:d");
				if (null != submit_d && 0 < submit_d.length) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == submit_d[0].equals("create")) {
						map.put("d:view", "one");
						Servlet.includeBody(request, map, list, response, /* false */true);
					} else if (true == submit_d[0].equals("search")) {
						Servlet.includeBody(request, map, list, response, /* false */true);
					} else if (true == submit_d[0].equals("update")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && true == view.equals("one")) {
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many") || true == view.equals("data")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else if (true == submit_d[0].equals("delete")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && true == view.equals("one")) {
							map.put("d:view", "many");
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many") || true == view.equals("data")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else {
						throw new IllegalStateException();
					}
				} else {
					Servlet.includeBody(request, map, list, response, true);
				}
			} else if (true == GET.equals(method) || null != request.getAttribute("d:state")) {
				final java.util.List<dro.Data> list = Servlet.buildDataStringArrays(request, view);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (null != view && true == view.equals("one")) {
					// request.setAttribute("df:mdf:id", parameters.get("df:mdf:id"));
					Servlet.includeBody(request, map, list, response, true);
				} else if (null == view || true == view.equals("many") || true == view.equals("data")) {
					Servlet.includeBody(request, map, list, response, true);
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}

			// Servlet.includeFooter(request, response, embed);

		} finally {
			// dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}

	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		this.doMethod(req, resp);
	}

	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		this.doMethod(req, resp);
	}

	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}