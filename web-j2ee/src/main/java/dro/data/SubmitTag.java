package dro.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SubmitTag extends __abstract_Tag {
	protected String value;// = null;
	protected boolean href;// = false;
	
	{
	  //value = null;
	  //href = false;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == this.href) {
			if (true == this.value.equals("create")) {
				jw.write("<input id='id:d:submit-create' name='submit:d' type='submit' value='create'"+(null == super.d || true == super.d.get(dro.meta.Data.$id).toString().equals("{id#md}") || true == super.d.get(dro.data.Context.$id).toString().equals("{id#dc}") ? "disabled" : "")+"/>\n");
			} else if (true == this.value.equals("search")) {
				jw.write("<input id='id:d:submit-search' name='submit:d' type='submit' value='search'"+(null == super.d || true == super.d.get(dro.meta.Data.$id).toString().equals("{id#md}") || true == super.d.get(dro.data.Context.$id).toString().equals("{id#dc}") ? "disabled" : "")+"/>\n");
			} else if (true == this.value.equals("update")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("d:id"); // TODO: go via d direct
				jw.write("<input id='id:d:submit-update' name='submit:d' type='submit'"+(null == super.d || true == super.d.get(dro.meta.Data.$id).toString().equals("{id#md}") || true == super.d.get(dro.data.Context.$id).toString().equals("{id#dc}") || 1 == ids.length ? " disabled" : "")+"' value='update'/>\n");
			} else if (true == this.value.equals("delete")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("d:id"); // TODO: go via d direct
				jw.write("<input id='id:d:submit-delete' name='submit:d' type='submit'"+(null == super.d || true == super.d.get(dro.meta.Data.$id).toString().equals("{id#md}") || true == super.d.get(dro.data.Context.$id).toString().equals("{id#dc}") || 1 == ids.length ? " disabled" : "")+"' value='delete'/>\n");
			}
		} else {
			if (true == this.value.equals("cancel")) {
				final String[] href = (String[])pageContext.getRequest().getAttribute("d:href"); // TODO: go via d direct
				jw.write("<a href='?"+href[0]+"d:view=many"+href[1]+"&cancel=d'>cancel</a>\n");
			}
		}
	}
}