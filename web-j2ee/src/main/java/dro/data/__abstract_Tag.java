package dro.data;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public abstract class __abstract_Tag extends SimpleTagSupport {
	protected static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	protected static String getDateTimeStamp(final Date d) {
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		return sdf.format(d);
	}
	
	protected dro.Data d;// = null;
	
	{
	  //d = null;
	}
	
	public void setData(final dro.Data d) {
		this.d = d;
	}
	
	@Override
	public abstract void doTag() throws JspException, IOException;
}