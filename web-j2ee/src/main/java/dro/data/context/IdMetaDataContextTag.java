package dro.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdMetaDataContextTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.dc.get(dro.meta.data.Context.$id).toString();
		jw.write("<input name='dc:mdc:id' type='text' hidden=true value='"+id+"'/>\n");
		jw.write("<input id='id:dc:mdc' type='text' value='"+id+"' disabled/>\n");
	}
}