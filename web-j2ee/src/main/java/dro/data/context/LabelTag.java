package dro.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class LabelTag extends __abstract_Tag {
	protected String value;// = null;
	
	{
	  //value = null;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == this.value.equals("$id")) {
			jw.write("$id");
		} else if (true == this.value.equals("$id#mdc")) {
			jw.write("$id#mdc");
		} else {
		  //throw new IllegalArgumentException();
			jw.write(this.value);
		}
	}
}