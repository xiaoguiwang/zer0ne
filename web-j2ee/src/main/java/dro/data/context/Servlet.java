package dro.data.context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.ServletTool;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String POST = "POST";
	private static final java.lang.String GET  = "GET" ;
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
  /*ublic static final java.lang.Boolean includeHeader(final HttpServletRequest request, final java.lang.String key, final HttpServletResponse response) throws IOException, ServletException {
		return ServletTool.includeHeader(request, key, "/html_head_title_body.jsp", response);
	}*/
	public static final void includeBody(final HttpServletRequest request, final java.util.Map<java.lang.String, java.lang.String> map, final java.util.List<dro.data.Context> list, final HttpServletResponse response, /*final */boolean recurse) throws IOException, ServletException {
	  /*final boolean */recurse = false;
		final String mdc_id = ServletTool.get(request, map, "mdc:id");
		request.setAttribute("mdc-id", new String[]{map.get("mdc:id")});
		final String md_id = ServletTool.get(request, map, "md:id");
		request.setAttribute("md-id", new String[]{map.get("md:id")});
		final String mdf_id = ServletTool.get(request, map, "mdf:id");
		final String dc_id = null;//ServletTool.get(request, map, "dc:id");
		final String d_id = ServletTool.get(request, map, "d:id");
		final String df_id = ServletTool.get(request, map, "df:id");
		final StringBuffer[] sb = new StringBuffer[]{new StringBuffer(),new StringBuffer()};
		ServletTool.appendIdAndView(sb[0], mdc_id, "mdc", false);
		ServletTool.appendIdAndView(sb[0], md_id, "md", true);
		ServletTool.appendIdAndView(sb[0], mdf_id, "mdf", true);
		ServletTool.appendIdAndView(null, dc_id, "dc", true);
		ServletTool.appendIdAndView(sb[1], d_id, "d", true);
		ServletTool.appendIdAndView(sb[1], df_id, "df", true);
		request.setAttribute("dc:href", new String[]{sb[0].append("&").toString(),sb[1].toString()});
		final String view = map.get("dc:view");
		request.setAttribute("dc:view", view);
		request.setAttribute("dc:list", list);
		final java.util.List<java.lang.String> ids = new ArrayList<>();
		for (final dro.data.Context dc: list) {
			ids.add(dc.id().toString());
		}
		request.setAttribute("dc:id", ids.toArray(new String[0]));
		
	  //final String referer = request.getHeader("referer");
		{
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/data/Context.jsp");
			requestDispatcher.include(request, response);
		}
		if (true == recurse) {
			request.setAttribute("d:embed", true);
			request.setAttribute("d:view", map.get("d:view"));
			request.setAttribute("d:id", map.get("d:id"));
			request.setAttribute("d:md:id", map.get("d:md:id"));
			
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/Data");
			requestDispatcher.include(request, response);
			
			request.setAttribute("dc:embed", false);
		}
	}
  /*public static final void includeFooter(final HttpServletRequest request, final HttpServletResponse response, final java.lang.Boolean embed) throws IOException, ServletException {
		ServletTool.includeFooter(request, "/_body_html.jsp", response, embed);
	}*/
	
	protected static java.util.List<dro.data.Context> createDataContexts(
		final dro.meta.data.Context mdc, final java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays
	) {
		arrays = ServletTool.arrays(ids, arrays, true);
		final java.util.List<dro.data.Context> list = new ArrayList<>();
		for (int i = 0; i < arrays[0].length; i++) {
			final dro.data.Context dc = mdc.add(new dro.data.Context(), dro.data.Context.Return.Context);
			for (int j = 0; j < keys.length; j++) {
				dc.set(keys[j], arrays[j][i]);
			}
			list.add(dc);
		}
		return list;
	}
	protected static java.util.List<dro.data.Context> readDataContexts(
		final dro.meta.data.Context mdc, /*final */java.lang.String[] ids
	) {
		final java.util.List<dro.data.Context> list = new ArrayList<>();
		if (null == mdc) {
			// TOOD: build dc for all mdc but only if/when ids are null as these are dc ids and we don't have mdc ids and dc ids are not unique across mdc
		} else {
			if (null != ids) {
				ids = ServletTool.ids(ids);
			}
			if (null == ids) {
				final java.util.Set<dro.data.Context> set = mdc.returns(dro.data.Context.Set.Return.Set);
				for (final dro.data.Context dc: set) {
					list.add(dc);
				}
			} else {
				for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
					if (true == ids[j].equals("{id}")) continue; // FIXME
					final dro.data.Context dc = mdc.returns(new java.lang.Integer(ids[j]), dro.data.Context.Return.Context);
					if (null == dc) continue;
					list.add(dc);
				}
			}
		}
		return list;
	}
	protected static java.util.List<dro.data.Context> updateDataContexts(
		final dro.meta.data.Context mdc, /*final */java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays, final java.lang.String[] checkbox
	) {
		final String[] __ids = ServletTool.ids(ids, arrays);
		arrays = ServletTool.arrays(ids, arrays, false);
		ids = __ids;
		final java.util.List<dro.data.Context> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			final dro.data.Context dc = mdc.returns(new java.lang.Integer(ids[i]), dro.data.Context.Return.Context);
			if (null == dc) continue;
			final boolean update = null == checkbox || (i < checkbox.length && null != checkbox[i] && true == checkbox[i].equals("on"));
			if (true == update) {
				for (int j = 0; j < keys.length; j++) {
					if (j > arrays.length-1) continue;
					if (i > arrays[j].length-1) continue;
					dc.set(keys[j], arrays[j][i]);
				}
			}
			list.add(dc);
		}
		return list;
	}
	protected static java.util.List<dro.data.Context> deleteDataContexts(
		final dro.meta.data.Context mdc, /*final */java.lang.String[] ids, final java.lang.String[] checkbox
	) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.data.Context> list = new ArrayList<>();
		for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
			if (true == ids[j].equals("{id}")) continue; // FIXME
			final dro.data.Context dc = mdc.returns(new java.lang.Integer(ids[j]), dro.data.Context.Return.Context);
			if (null == dc) continue;
			final boolean remove = null == checkbox || (j < checkbox.length && null != checkbox[j] && true == checkbox[j].equals("on"));
			if (true == remove) {
				mdc.remove(dc, dro.data.Context.Return.Context);
			} else {
				list.add(dc);
			}
		}
		return list;
	}
	
	protected static java.util.List<dro.data.Context> buildDataContextList(final HttpServletRequest request, final java.lang.String view) {
	  /*final */java.util.List<dro.data.Context> list = null;
		final Map<String, String[]> parameters = request.getParameterMap();
		final java.lang.String method = request.getMethod();
		
		final String[] mdc_id = parameters.get("mdc:id");
	  /*final */dro.meta.data.Context mdc = null;
		if (null != mdc_id && 0 < mdc_id.length && false == mdc_id[0].equals("{id}")) {
		  /*final dro.meta.data.Context */mdc = dro.meta.data.Context.returns(
				new java.lang.Integer(mdc_id[0]), dro.meta.data.Context.Return.Context
			);
		}
		
		final String[] submit_dc = parameters.get("submit:dc");
		if (false == Boolean.TRUE.booleanValue()) {
		
		// >C<RUD
		} else if ((null == request.getAttribute("dc:state") && true == POST.equals(method)) && null != submit_dc && 0 < submit_dc.length && true == submit_dc[0].equals("create")) {
		  /*final java.util.List<dro.data.Context> */list = Servlet.createDataContexts(mdc,
				parameters.get("dc:id"),
				new java.lang.String[]{dro.meta.data.Context.$id},
				new java.lang.String[][]{parameters.get("dc:mdc:id")}
			);
			
		// C>R<UD
		} else if (null != request.getAttribute("dc:state") || (true == GET.equals(method) && null == submit_dc)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (null != view && true == view.equals("one")) {
			  /*final java.util.List<dro.data.Context> */list = Servlet.readDataContexts(mdc,
					parameters.get("dc:id")
				);
			} else if (null == view || true == view.equals("many")) {
			  /*final java.util.List<dro.data.Context> */list = Servlet.readDataContexts(mdc,
					parameters.get("dc:id")
				);
			} else {
				throw new IllegalStateException();
			}
			
		// CR>U<D
		} else if ((null == request.getAttribute("dc:state") && true == POST.equals(method)) && null != submit_dc && 0 < submit_dc.length && true == submit_dc[0].equals("update")) {
		  /*java.util.List<dro.data.Context> */list = Servlet.updateDataContexts(mdc,
				parameters.get("dc:id"),
				new java.lang.String[]{dro.meta.data.Context.$id},
				new java.lang.String[][]{parameters.get("dc:mdc:id")},
				parameters.get("dc:checkbox")
			);
		
		// CRU>D<
		} else if ((null == request.getAttribute("dc:state") && true == POST.equals(method)) && null != submit_dc && 0 < submit_dc.length && true == submit_dc[0].equals("delete")) {
		  /*final java.util.List<dro.data.Context> list = */Servlet.deleteDataContexts(mdc,
				parameters.get("dc:id"),
				parameters.get("dc:checkbox")
			);
		  /*final java.util.List<dro.data.Context> */list = Servlet.readDataContexts(mdc,
				parameters.get("dc:id")
			);
		
		} else {
		  /*final java.util.List<dro.Data.Context> */list = Servlet.readDataContexts(mdc,
				parameters.get("dc:id")
			);
		}
		
		if (0 == list.size() || null == view || true == view.equals("many")) {
			list.add(
				new dro.data.Context()
					.set("$id", "{id}")
					.set(dro.meta.data.Context.$id, "{id#mdc}")
			);
		}
		
		return list;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		final Map<String, String[]> parameters = request.getParameterMap();
		
		try {
		  //dro.lang.Event.entered(Servlet.class, this, "..", new Object[]{request,response});
			
		  //final java.lang.Boolean embed = Servlet.includeHeader(request, "dc:embed", response);
			
			final java.util.Map<java.lang.String, java.lang.String> map = ServletTool.buildMap(request.getQueryString());
			
			final String view = map.get("dc:view");
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == POST.equals(method) && null == request.getAttribute("dc:state")) {
				final java.util.List<dro.data.Context> list = Servlet.buildDataContextList(request, view);
				request.setAttribute("dc:state", 1);
				final String[] submit_dc = parameters.get("submit:dc");
				if (null != submit_dc && 0 < submit_dc.length) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == submit_dc[0].equals("create")) {
						map.put("dc:view", "one");
						Servlet.includeBody(request, map, list, response, /*false*/true);
					} else if (true == submit_dc[0].equals("update")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else if (true == submit_dc[0].equals("delete")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							map.put("dc:view", "many");
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else {
						throw new IllegalStateException();
					}
				} else {
					Servlet.includeBody(request, map, list, response, true);
				}
			} else if (true == GET.equals(method) || null != request.getAttribute("dc:state")) {
				final java.util.List<dro.data.Context> list = Servlet.buildDataContextList(request, view);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (null != view && view.equals("one")) {
				  //request.setAttribute("d:md:id", parameters.get("d:md:id"));
					Servlet.includeBody(request, map, list, response, true);
				} else if (null == view || view.equals("many")) {
					Servlet.includeBody(request, map, list, response, true);
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
			
		  //Servlet.includeFooter(request, response, embed);
			
		} finally {
		  //dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}