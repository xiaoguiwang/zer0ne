package dro.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SubmitTag extends __abstract_Tag {
	protected String value;// = null;
	protected boolean href;// = false;
	
	{
	  //value = null;
	  //href = false;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == this.href) {
			if (true == this.value.equals("create")) {
				jw.write("<input id='id:dc:submit-create' name='submit:dc' type='submit' value='create'"+(true == super.dc.get(dro.meta.data.Context.$id).toString().equals("{id#mdc}") ? "disabled" : "")+"/>\n");
			} else if (true == this.value.equals("update")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("dc:id"); // TODO: go via dc direct
				jw.write("<input id='id:dc:submit-update' name='submit:dc' type='submit'"+(true == super.dc.get(dro.meta.data.Context.$id).toString().equals("{id#mdc}") || 1 == ids.length ? " disabled" : "")+"' value='update'/>\n");
			} else if (true == this.value.equals("delete")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("dc:id"); // TODO: go via dc direct
				jw.write("<input id='id:dc:submit-delete' name='submit:dc' type='submit'"+(true == super.dc.get(dro.meta.data.Context.$id).toString().equals("{id#mdc}") || 1 == ids.length ? " disabled" : "")+"' value='delete'/>\n");
			}
		} else {
			if (true == this.value.equals("cancel")) {
				final String[] href = (String[])pageContext.getRequest().getAttribute("dc:href"); // TODO: go via dc direct
				jw.write("<a href='?"+href[0]+"dc:view=many"+href[1]+"&cancel=dc'>cancel</a>\n");
			}
		}
	}
}