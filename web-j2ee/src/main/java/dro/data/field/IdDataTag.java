package dro.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdDataTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.df.get(dro.Data.$id).toString();
		jw.write("<input name='df:d:id' type='text' hidden=true value='"+id+"'/>\n");
		jw.write("<input id='id:df:d' type='text' value='"+id+"' disabled/>\n");
	}
}