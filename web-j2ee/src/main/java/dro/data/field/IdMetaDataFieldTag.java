package dro.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdMetaDataFieldTag extends __abstract_Tag {
	protected String value;// = null;
	
	{
	  //value = null;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.df.get(dro.meta.data.Field.$id).toString();
		if (null == this.value || true == this.value.equals("mdf:id")) {
			jw.write("<input name='df:mdf:id' type='text' hidden=true value='"+id+"'/>\n");
			jw.write("<input id='id:df:mdf' type='text' value='"+id+"' disabled/>\n");
		} else if (true == this.value.equals("mdf:name")) {
			final dro.meta.data.Field mdf = super.df.returns(dro.meta.data.Field.Return.Field);
			final String name = (String)mdf.get("$name");
			jw.write("<input id='id:df:mdf:name' type='text' value='"+name+"' disabled/>\n");
		} else if (true == this.value.equals("mdf:type")) {
			final dro.meta.data.Field mdf = super.df.returns(dro.meta.data.Field.Return.Field);
			final String type = (String)mdf.get("$type");
			jw.write("<input id='id:df:mdf:type' type='text' value='"+type+"' disabled/>\n");
		}
	}
}