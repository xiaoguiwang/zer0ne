package dro.data.field;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.ServletTool;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String POST   = "POST";
	private static final java.lang.String GET    = "GET" ;
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
  /*public static final java.lang.Boolean includeHeader(final HttpServletRequest request, final java.lang.String key, final HttpServletResponse response) throws IOException, ServletException {
		return ServletTool.includeHeader(request, key, "/html_head_title_body.jsp", response);
	}*/
	public static final void includeBody(final HttpServletRequest request, final java.util.Map<java.lang.String, java.lang.String> map, final java.util.List<dro.data.Field> list, final HttpServletResponse response, /*final */boolean recurse) throws IOException, ServletException {
	  /*final boolean */recurse = false;
		final String mdc_id = ServletTool.get(request, map, "mdc:id");
		final String md_id = ServletTool.get(request, map, "md:id");
		final String mdf_id = ServletTool.get(request, map, "mdf:id");
		request.setAttribute("mdf-id", new String[]{map.get("mdf:id")});
		final String dc_id = ServletTool.get(request, map, "dc:id");
		final String d_id = ServletTool.get(request, map, "d:id");
		request.setAttribute("d-id", new String[]{map.get("d:id")});
		final String df_id = null;//ServletTool.get(request, map, "df:id");
		final StringBuffer[] sb = new StringBuffer[]{new StringBuffer(),null};
		ServletTool.appendIdAndView(sb[0], mdc_id, "mdc", false);
		ServletTool.appendIdAndView(sb[0], md_id, "md", true);
		ServletTool.appendIdAndView(sb[0], mdf_id, "mdf", true);
		ServletTool.appendIdAndView(sb[0], dc_id, "dc", true);
		ServletTool.appendIdAndView(sb[0], d_id, "d", true);
		ServletTool.appendIdAndView(null, df_id, "df", true);
		request.setAttribute("df:href", new String[]{sb[0].append("&").toString(),""});
		final String view = map.get("df:view");
		request.setAttribute("df:view", view);
		request.setAttribute("df:list", list);
		final java.util.List<java.lang.String> ids = new ArrayList<>();
		for (final dro.data.Field df: list) {
			ids.add(df.id().toString());
		}
		request.setAttribute("df:id", ids.toArray(new String[0]));
		
	  //final String referer = request.getHeader("referer");
		{
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/data/Field.jsp");
			requestDispatcher.include(request, response);
		}
	}
  /*public static final void includeFooter(final HttpServletRequest request, final HttpServletResponse response, final java.lang.Boolean embed) throws IOException, ServletException {
		ServletTool.includeFooter(request, "/_body_html.jsp", response, embed);
	}*/
	
	protected static java.util.List<dro.data.Field> createDataField(
		final dro.Data d, final java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays
	) {
		arrays = ServletTool.arrays(ids, arrays, true);
		final dro.meta.Data md = d.returns(dro.meta.Data.Return.Data);
		final java.util.List<dro.data.Field> list = new ArrayList<>();
		for (int i = 0; i < arrays[0].length; i++) {
			final dro.meta.data.Field mdf = md.returns(new java.lang.Integer(arrays[1][i]), dro.meta.data.Field.Return.Field);
			final dro.data.Field df = d.add(
				new dro.data.Field(mdf), dro.data.Field.Return.Field
			);
			for (int j = 0; j < keys.length; j++) {
				df.set(keys[j], arrays[j][i]);
			}
			list.add(df);
		}
		return list;
	}
	protected static java.util.List<dro.data.Field> readDataField(
		final dro.Data d, /*final */java.lang.String[] ids
	) {
		final java.util.List<dro.data.Field> list = new ArrayList<>();
		if (null != d) {
			if (null != ids) {
				ids = ServletTool.ids(ids);
			}
			if (null == ids) {
				final java.util.Set<dro.data.Field> set = d.returns(dro.data.Field.Return.Set);
				for (final dro.data.Field df: set) {
					list.add(df);
				}
			} else {
				for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
					if (true == ids[j].equals("{id}")) continue; // FIXME
					final dro.data.Field df = d.returns(new java.lang.Integer(ids[j]), dro.data.Field.Return.Field);
					if (null == df) continue;
					list.add(df);
				}
			}
		}
		return list;
	}
	protected static java.util.List<dro.data.Field> updateDataField(
		final dro.Data d, Map<String, String[]> parameters
	) {
	  /*final */java.lang.String[] ids = parameters.get("df:id");
		final java.lang.String[] keys = new java.lang.String[]{dro.Data.$id,dro.meta.data.Field.$id,"$value"};
	  /*final */java.lang.String[][] arrays = new java.lang.String[][]{parameters.get("df:d:id"),parameters.get("df:mdf:id"),parameters.get("df:value")};
		
		final String[] __ids = ServletTool.ids(ids, arrays);
		arrays = ServletTool.arrays(ids, arrays, false);
		ids = __ids;
		final java.util.List<dro.data.Field> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			final java.lang.String[] checkbox = parameters.get("df#"+ids[i]+":checkbox");
			final dro.data.Field df = d.returns(new java.lang.Integer(ids[i]), dro.data.Field.Return.Field);
			final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
			final String type = (String)mdf.get("$type");
			final String limit = (String)mdf.get("$limit");
			final boolean update = null == checkbox || (1 == checkbox.length && null != checkbox[0] && true == checkbox[0].equals("on"));
			if (true == update) {
				for (int k = 0; k < keys.length; k++) {
					try {
						if (true == keys[k].equals("$value")) {
							if (null == limit || true == limit.equals("") || true == limit.equals("{0:1}")) {
								final String value = null == arrays[k][i] ? "" : arrays[k][i];
								if (null != value && false == value.equals("{value}")) {
									df.set(keys[k], arrays[k][i]);
								}
							} else if (true == limit.equals("{0:n}")) {
							  //final String[] values = parameters.get("df#"+ids[i]+":value");
								final java.util.List<String> __values = new ArrayList<>();
								for (int j = 0; ; j++) {
									final String[] value = parameters.get("df#"+ids[i]+"["+j+"]:value");
									if (null == value) break;
									__values.add(value[0]);
								};
								final String[] values = __values.toArray(new String[__values.size()]);
								if (true == type.equals("boolean")) {
								} else if (true == type.equals("number")) {
								} else if (true == type.equals("text")) {
									df.set(keys[k], values);
								}
							} else if (true == type.equals("bytes")) {
							}
						} else {
							df.set(keys[k], arrays[k][i]);
						}
					} catch (final IllegalArgumentException e) {
						// For now, ignore silently..
					}
				}
				list.add(df);
			}
		}
		return list;
	}
	protected static java.util.List<dro.data.Field> deleteDataField(
		final dro.Data d, /*final */java.lang.String[] ids, final java.lang.String[] checkbox
	) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.data.Field> list = new ArrayList<>();
		for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
			if (true == ids[j].equals("{id}")) continue; // FIXME
			final dro.data.Field df = d.returns(new java.lang.Integer(ids[j]), dro.data.Field.Return.Field);
			final boolean remove = null == checkbox || (j < checkbox.length && null != checkbox[j] && true == checkbox[j].equals("on"));
			if (true == remove) {
				// Note: if we remove it twice with re-submit, next bit would fail without if check
				if (null != df) {
					d.remove(df);//, dro.Data.Return.Data);
				}
			} else {
				list.add(df);
			}
		}
		return list;
	}
	
	protected static java.util.List<dro.data.Field> buildDataFieldList(final HttpServletRequest request, final java.lang.String view) {
	  /*final */java.util.List<dro.data.Field> list = null;
		final Map<String, String[]> parameters = request.getParameterMap();
		final java.lang.String method = request.getMethod();
		
		final String[] mdc_id = parameters.get("mdc:id");
	  /*final */dro.meta.data.Context mdc = null;
		if (null != mdc_id && 0 < mdc_id.length && false == mdc_id[0].equals("{id}")) {
		  /*final dro.meta.data.Context */mdc = dro.meta.data.Context.returns(
				new java.lang.Integer(mdc_id[0]), dro.meta.data.Context.Return.Context
			);
		}
		final String[] md_id = parameters.get("md:id");
	  /*final */dro.meta.Data md = null;
		if (null != md_id && 0 < md_id.length && false == md_id[0].equals("{id}") && null != mdc) {
		  /*final dro.meta.Data */md = mdc.returns(
				new java.lang.Integer(md_id[0]), dro.meta.Data.Return.Data
			);
		}
		final String[] d_id = parameters.get("d:id");
	  /*final */dro.Data d = null;
		if (null != d_id && 0 < d_id.length && false == d_id[0].equals("{id}") && null != md) {
		  /*final dro.Data */d = md.returns(
				new java.lang.Integer(d_id[0]), dro.Data.Return.Data
			);
		}
		final String[] mdf_id = parameters.get("mdf:id");
	  /*final */dro.meta.data.Field mdf = null;
		if (null != mdf_id && 0 < mdf_id.length && false == mdf_id[0].equals("{id}") && null != md) {
		  /*final dro.meta.data.Field */mdf = md.returns(
				new java.lang.Integer(mdf_id[0]), dro.meta.data.Field.Return.Field
			);
		}
		
		final String[] submit_df = parameters.get("submit:df");
		if (false == Boolean.TRUE.booleanValue()) {
		
		// >C<RUD
		} else if ((null == request.getAttribute("df:state") && true == POST.equals(method)) && null != submit_df && true == submit_df[0].equals("create")) {
		  /*final java.util.List<dro.data.Field> */list = Servlet.createDataField(d,
				parameters.get("df:id"),
				new java.lang.String[]{dro.Data.$id,dro.meta.data.Field.$id,"$value"},
				new java.lang.String[][]{parameters.get("df:d:id"),parameters.get("df:mdf:id"),parameters.get("df:value")}
			);
			
		// C>R<UD
		} else if (null != request.getAttribute("df:state") || (true == GET.equals(method) && null == submit_df)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (null != view && true == view.equals("one")) {
			  /*final java.util.List<dro.data.Field> */list = Servlet.readDataField(d,
					parameters.get("df:id")
				);
			} else if (null == view || true == view.equals("many")) {
			  /*final java.util.List<dro.data.Field> */list = Servlet.readDataField(d,
					(String[])null
				);
			} else {
				throw new IllegalStateException();
			}
			
		// CR>U<D
		} else if ((null == request.getAttribute("df:state") && true == POST.equals(method)) && null != submit_df && 0 < submit_df.length && true == submit_df[0].equals("update")) {
		  /*java.util.List<dro.data.Field> */list = Servlet.updateDataField(d, parameters);
		
		// CRU>D<
		} else if ((null == request.getAttribute("df:state") && true == POST.equals(method)) && null != submit_df && 0 < submit_df.length && true == submit_df[0].equals("delete")) {
		  /*final java.util.List<dro.data.Field> list = */Servlet.deleteDataField(d,
				parameters.get("df:id"),
				parameters.get("df:checkbox")
			);
		  /*final java.util.List<dro.data.Field> */list = Servlet.readDataField(d,
				parameters.get("df:id")
			);
		
		} else {
		  /*final java.util.List<dro.data.Field> */list = Servlet.readDataField(d,
				parameters.get("df:id")
			);
		}
		
		if (0 == list.size() || null == view || true == view.equals("many")) {
			list.add(
				new dro.data.Field(
					new dro.meta.data.Field()
						.set("$id", "{id}")
						.set(dro.meta.Data.$id, "{id#md}")
						.set("$name", "{name}")
						.set("$type", "{type}")
					)
					.set("$id", "{id}")
					.set(dro.Data.$id, "{id#d}")
					.set(dro.meta.data.Field.$id, "{id#mdf}")
			);
		}
		
		return list;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		final Map<String, String[]> parameters = request.getParameterMap();
		
		try {
		  //dro.lang.Event.entered(Servlet.class, this, "..", new Object[]{request,response});
			
		  //final java.lang.Boolean embed = Servlet.includeHeader(request, "df:embed", response);
			
			final java.util.Map<java.lang.String, java.lang.String> map = ServletTool.buildMap(request.getQueryString());
			
			final String view = map.get("df:view");
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == POST.equals(method) && null == request.getAttribute("df:state")) {
				final java.util.List<dro.data.Field> list = Servlet.buildDataFieldList(request, view);
				request.setAttribute("df:state", 1);
				final String[] submit_df = parameters.get("submit:df");
				if (null != submit_df && 0 < submit_df.length) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == submit_df[0].equals("create")) {
						map.put("df:view", "one");
						Servlet.includeBody(request, map, list, response, /*false*/true);
					} else if (true == submit_df[0].equals("update")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else if (true == submit_df[0].equals("delete")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							map.put("df:view", "many");
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else {
						throw new IllegalStateException();
					}
				} else {
					Servlet.includeBody(request, map, list, response, true);
				}
			} else if (true == GET.equals(method) || null != request.getAttribute("df:state")) {
				final java.util.List<dro.data.Field> list = Servlet.buildDataFieldList(request, view);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (null != view && view.equals("one")) {
					Servlet.includeBody(request, map, list, response, true);
				} else if (null == view || view.equals("many")) {
					Servlet.includeBody(request, map, list, response, true);
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
			
		  //Servlet.includeFooter(request, response, embed);
			
		} finally {
		  //dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}