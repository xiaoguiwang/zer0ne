package dro.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SubmitTag extends __abstract_Tag {
	protected String value;// = null;
	protected boolean href;// = false;
	
	{
	  //value = null;
	  //href = false;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == this.href) {
			if (true == this.value.equals("create")) {
				jw.write("<input id='id:df:submit-create' name='submit:df' type='submit' value='create'"+(true == super.df.get(dro.Data.$id).toString().equals("{id#d}") || true == super.df.get(dro.meta.data.Field.$id).toString().equals("{id#mdf}") ? "disabled" : "")+"/>\n");
			} else if (true == this.value.equals("update")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("df:id"); // TODO: go via df direct
				jw.write("<input id='id:df:submit-update' name='submit:df' type='submit'"+(true == super.df.get(dro.Data.$id).toString().equals("{id#d}") || true == super.df.get(dro.meta.data.Field.$id).toString().equals("{id#mdf}") || 1 == ids.length ? " disabled" : "")+"' value='update'/>\n");
			} else if (true == this.value.equals("delete")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("df:id"); // TODO: go via df direct
				jw.write("<input id='id:df:submit-delete' name='submit:df' type='submit'"+(true == super.df.get(dro.Data.$id).toString().equals("{id#d}") || true == super.df.get(dro.meta.data.Field.$id).toString().equals("{id#mdf}") || 1 == ids.length ? " disabled" : "")+"' value='delete'/>\n");
			}
		} else {
			if (true == this.value.equals("cancel")) {
				final String[] href = (String[])pageContext.getRequest().getAttribute("df:href"); // TODO: go via df direct
				jw.write("<a href='?"+href[0]+"df:view=many"+href[1]+"&cancel=df'>cancel</a>\n");
			}
		}
	}
}