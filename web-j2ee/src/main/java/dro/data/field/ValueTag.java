package dro.data.field;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class ValueTag extends __abstract_Tag {
	protected String key;// = null;
	
	{
	  //key = false;
	}
	
	public void setKey(final String key) {
		this.key = key;
	}
	
	private void __doTag(final JspWriter jw, final String elemName, final String[] attrKeys, final String[] attrValues, final String type, final java.util.List<String> list, final String value, final boolean disabled) throws JspException, IOException {
		if (true == elemName.equals("input")) {
			jw.write("<input");
			for (int a = 0; a < attrKeys.length; a++) {
				jw.write(" "+attrKeys[a]+"='"+attrValues[a]+"'");
			}
			if (null != value) {
				jw.write(" value='"+value+"'");
			}
			if (true == disabled) {
				jw.write(" disabled");
			}
			jw.write("/><br/>\n");
		} else if (true == elemName.equals("select")) {
			jw.write("<select");
			for (int a = 0; a < attrKeys.length; a++) {
				jw.write(" "+attrKeys[a]+"='"+attrValues[a]+"'");
			}
			if (true == disabled) {
				jw.write(" disabled");
			}
			jw.write(">\n");
			if (null != list) {
				for (final String __value: list) {
					jw.write("<option value='"+__value+"'"+(true == __value.equals(value) ? " selected" : "")+">"+__value+"</option>\n");
				}
			}
			jw.write("</select>\n");
		}
	}
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (null != this.key) {
			final Object o = super.df.get(this.key);
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == o instanceof Date) {
				final String dts = __abstract_Tag.getDateTimeStamp((Date)o);
				jw.write(dts);
			} else if (true == o instanceof String) {
				
			} else if (true == o instanceof Integer) {
				
			} else {
				
			}
		} else {
			final String id = super.df.id().toString();
			final String df_d_id = super.df.get(dro.Data.$id).toString();
			final String df_mdf_id = super.df.get(dro.meta.data.Field.$id).toString();
			final dro.meta.data.Field mdf = super.df.returns(dro.meta.data.Field.Return.Field);
			final String type = (String)mdf.get("$type");
		  /*final */Object o = super.df.get("$value");
			final String value = null == o ? "" : o.toString();
			final boolean disabled = (true == df_d_id.equals("{id#d}") || true == df_mdf_id.equals("{id#mdf}"));
			// TODO: according to mdf:type/limit/order/unique/lookup render different controls (with JavaScript etc. soon) especially if/when mdf:type is d and d has list of values
			final String limit = (String)mdf.get("$limit");
		  //final String order = (String)mdf.get("$order");
		  //final String unique = (String)mdf.get("$unique");
		  //final String lookup = (String)mdf.get("$lookup");
			if (null == value || value.equals("") || true == value.equals("{value}")) {
				if (null == limit || true == limit.equals("") || true == limit.equals("{0:1}")) {
					final String __value = value;
					this.__doTag(jw, "input", new String[]{"id","name","type"}, new String[]{"id:df:value","df:value","text"}, type, null, __value, disabled);
				} else if (true == limit.equals("{0:n}")) {
					@SuppressWarnings("unchecked")
					final java.util.List<String> list = null == o ? null : (java.util.List<String>)o;
					for (final String __value: list) {
						this.__doTag(jw, "input", new String[]{"id","name","type"}, new String[]{"id:df:value","df:value","text"}, type, null, __value, disabled);
					}
				}
			} else if (true == type.equals("boolean")) {
				final java.util.List<String> __values = new ArrayList<String>(){
					private static final long serialVersionUID = 0L;
					{
						add("null");
						add("true");
						add("false");
					}
				};
				final String[] attrKeys = new String[]{"id","name"};
				final String[] attrValues = new String[]{"id:df:value","df:value"};
				if (null == limit || true == limit.equals("") || true == limit.equals("{0:1}")) {
					final String __value = value;
					this.__doTag(jw, "select", attrKeys, attrValues, type, __values, __value, disabled);
				} else if (true == limit.equals("{0:n}")) {
					attrValues[0] = "id:df#"+id+":value";
					attrValues[1] = "df#"+id+":value";
					@SuppressWarnings("unchecked")
					final java.util.List<String> list = null == o ? null : (java.util.List<String>)o;
					for (int i = 0; i < list.size(); i++) {
						final String __value = list.get(i);
						this.__doTag(jw, "select", attrKeys, attrValues, type, __values, __value, disabled);
					}
				}
			} else if (true == type.equals("number")) {
				// Looks the same as above, but we'll be adding in some JavaScript at some point (soon)
				final java.util.List<String> __values = null;
				final String[] attrKeys = new String[]{"id","name","type"};
				final String[] attrValues = new String[]{"id:df:value","df:value","text"};
				if (null == limit || true == limit.equals("") || true == limit.equals("{0:1}")) {
					final String __value = value;
					this.__doTag(jw, "input", attrKeys, attrValues, type, __values, __value, disabled);
				} else if (true == limit.equals("{0:n}")) {
					attrValues[0] = "id:df#"+id+":value";
					attrValues[1] = "df#"+id+":value";
					@SuppressWarnings("unchecked")
					final java.util.List<String> list = null == o ? null : (java.util.List<String>)o;
					for (int i = 0; i < list.size(); i++) {
						final String __value = list.get(i);
						this.__doTag(jw, "input", attrKeys, attrValues, type, __values, __value, disabled);
					}
				}
				// Looks the same as below, but we'll be adding in some JavaScript at some point (soon)
			} else if (true == type.equals("text")) {
				// Looks the same as above, but we'll be adding in some JavaScript at some point (soon)
				final java.util.List<String> __values = null;
				final String[] attrKeys = new String[]{"id","name","type"};
				final String[] attrValues = new String[]{"id:df:value","df:value","text"};
				if (null == limit || true == limit.equals("") || true == limit.equals("{0:1}")) {
					final String __value = value;
					this.__doTag(jw, "input", attrKeys, attrValues, type, __values, __value, disabled);
				} else if (true == limit.equals("{0:n}")) {
					// TODO: This is where we have to introspect and/or try/catch cast for single, array/set/list, or map
					//       And everywhere else we have this same/similar code pattern..
				  /*final */java.util.List<String> list = null;
					if (null == o) {
						list = null;
					} else if (true == o instanceof String[]) {
						list = Arrays.asList((String[])o);
					} else if (true == o instanceof java.util.List) {
						@SuppressWarnings("unchecked")
						java.util.List<String> __list = (java.util.List<String>)o;
						list = __list;
					} else if (true == o instanceof java.util.Set) {
						@SuppressWarnings("unchecked")
						java.util.Set<String> __set = (java.util.Set<String>)o;
						list = new ArrayList<String>();
						list.addAll(__set);
				  //} else if (true == o instanceof java.util.Map) {
					} else {
						throw new IllegalArgumentException();
					}
					jw.write("\n");
					for (int i = 0; i < list.size(); i++) {
						attrValues[0] = "id:df#"+id+"["+i+"]:value";
						attrValues[1] = "df#"+id+"["+i+"]:value";
						final String __value = list.get(i);
						this.__doTag(jw, "input", attrKeys, attrValues, type, __values, __value, disabled);
					}
				}
				// Looks the same as below, but we'll be adding in some JavaScript at some point (soon)
			} else if (true == type.equals("bytes")) {
				// Looks the same as above, but we'll be adding in some JavaScript at some point (soon), and supporting specifically Base16/Base64 encoded
				final java.util.List<String> __values = null;
				final String[] attrKeys = new String[]{"id","name","type"};
				final String[] attrValues = new String[]{"id:df:value","df:value","text"};
				if (null == limit || true == limit.equals("") || true == limit.equals("{0:1}")) {
					final String __value = value;
					this.__doTag(jw, "input", attrKeys, attrValues, type, __values, __value, disabled);
				} else if (true == limit.equals("{0:n}")) {
					attrValues[0] = "id:df#"+id+":value";
					attrValues[1] = "df#"+id+":value";
					@SuppressWarnings("unchecked")
					final java.util.List<String> list = null == o ? null : (java.util.List<String>)o;
					for (int i = 0; i < list.size(); i++) {
						final String __value = list.get(i);
						this.__doTag(jw, "input", attrKeys, attrValues, type, __values, __value, disabled);
					}
				}
			}
		}
	}
}