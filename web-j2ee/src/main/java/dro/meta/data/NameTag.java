package dro.meta.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class NameTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.md.get(dro.meta.data.Context.$id).toString();
		final String name;// = null;
		if (true == id.equals("{id#mdc}")) {
			name = "{name}";
		} else {
			name = (String)super.md.get("$name");
		}
		jw.write("<input id='id:mdc:name' name='mdc:name' type='text' value='"+name+"'"+(true == id.equals("{id#mdc}") ? " disabled" : "")+"/>\n");
	}
}