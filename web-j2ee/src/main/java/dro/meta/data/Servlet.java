package dro.meta.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.ServletTool;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String POST   = "POST";
	private static final java.lang.String GET    = "GET" ;
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
  /*public static final java.lang.Boolean includeHeader(final HttpServletRequest request, final java.lang.String key, final HttpServletResponse response) throws IOException, ServletException {
		return ServletTool.includeHeader(request, key, "/html_head_title_body.jsp", response);
	}*/
	public static final void includeBody(final HttpServletRequest request, final java.util.Map<java.lang.String, java.lang.String> map, final java.util.List<dro.meta.Data> list, final HttpServletResponse response, /*final */boolean recurse) throws IOException, ServletException {
	  /*final boolean */recurse = false;
		final String mdc_id = ServletTool.get(request, map, "mdc:id");
		request.setAttribute("mdc-id", new String[]{map.get("mdc:id")});
		final String md_id = null;//ServletTool.get(request, map, "md:id");
		final String mdf_id = ServletTool.get(request, map, "mdf:id");
		final String dc_id = ServletTool.get(request, map, "dc:id");
		request.setAttribute("dc-id", new String[]{map.get("dc:id")});
		final String d_id = ServletTool.get(request, map, "d:id");
		final String df_id = ServletTool.get(request, map, "df:id");
		final StringBuffer[] sb = new StringBuffer[]{new StringBuffer(),new StringBuffer()};
		ServletTool.appendIdAndView(sb[0], mdc_id, "mdc", false);
		ServletTool.appendIdAndView(null, md_id, "md", true);
		ServletTool.appendIdAndView(sb[1], mdf_id, "mdf", true);
		ServletTool.appendIdAndView(sb[1], dc_id, "dc", true);
		ServletTool.appendIdAndView(sb[1], d_id, "d", true);
		ServletTool.appendIdAndView(sb[1], df_id, "df", true);
		request.setAttribute("md:href", new String[]{sb[0].append("&").toString(),sb[1].toString()});
		final String view = map.get("md:view");
		request.setAttribute("md:view", view);
		request.setAttribute("md:list", list);
		final java.util.List<java.lang.String> ids = new ArrayList<>();
		for (final dro.meta.Data md: list) {
			ids.add(md.id().toString());
		}
		request.setAttribute("md:id", ids.toArray(new String[0]));
		
	  //final String referer = request.getHeader("referer");
		{
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/meta/Data.jsp");
			requestDispatcher.include(request, response);
		}
		if (true == recurse) {
			request.setAttribute("mdf:embed", true);
			request.setAttribute("mdf:view", map.get("mdf:view"));
			request.setAttribute("mdf:id", map.get("mdf:id"));
			request.setAttribute("mdf:md:id", map.get("mdf:md:id"));
			
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/meta/data/Field");
			requestDispatcher.include(request, response);
			
			request.setAttribute("mdf:embed", false);
		}
	  /*if (true == recurse) {
			request.setAttribute("d:embed", true);
			request.setAttribute("d:view", map.get("d:view"));
			request.setAttribute("d:id", map.get("d:id"));
			request.setAttribute("d:md:id", map.get("md:id"));
			
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/Data");
			requestDispatcher.include(request, response);
			
			request.setAttribute("d:embed", false);
		}*/
	}
  /*public static final void includeFooter(final HttpServletRequest request, final HttpServletResponse response, final java.lang.Boolean embed) throws IOException, ServletException {
		ServletTool.includeFooter(request, "/_body_html.jsp", response, embed);
	}*/
	
	protected static java.util.List<dro.meta.Data> createMetaData(
		final dro.meta.data.Context mdc, final java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays
	) {
		arrays = ServletTool.arrays(ids, arrays, true);
		final java.util.List<dro.meta.Data> list = new ArrayList<>();
		for (int i = 0; i < arrays[0].length; i++) {
			final dro.meta.Data md = mdc.add(new dro.meta.Data(), dro.meta.Data.Return.Data);
			for (int j = 0; j < keys.length; j++) {
				md.set(keys[j], arrays[j][i]);
			}
			list.add(md);
		}
		return list;
	}
	protected static java.util.List<dro.meta.Data> readMetaData(
	  /*final */dro.meta.data.Context mdc, /*final */java.lang.String[] ids
	) {
		final java.util.List<dro.meta.Data> list = new ArrayList<>();
		if (null == mdc) {
			// TOOD: build md for all mdc but only if/when ids are null as these are md ids and we don't have mdc ids and md ids are not unique across mdc
		} else {
			if (null != ids) {
				ids = ServletTool.ids(ids);
			}
			if (null == ids) {
				final java.util.Set<dro.meta.Data> set = mdc.returns(dro.meta.Data.Set.Return.Set);
				for (final dro.meta.Data md: set) {
					list.add(md);
				}
			} else {
				for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
					if (true == ids[j].equals("{id}")) continue;
					final dro.meta.Data md = mdc.returns(new java.lang.Integer(ids[j]), dro.meta.Data.Return.Data);
					if (null == md) continue;
					list.add(md);
				}
			}
		}
		return list;
	}
	protected static java.util.List<dro.meta.Data> updateMetaData(
		final dro.meta.data.Context mdc, /*final */java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays, final java.lang.String[] checkbox
	) {
		final String[] __ids = ServletTool.ids(ids, arrays);
		arrays = ServletTool.arrays(ids, arrays, false);
		ids = __ids;
		final java.util.List<dro.meta.Data> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			final dro.meta.Data md = mdc.returns(new java.lang.Integer(ids[i]), dro.meta.Data.Return.Data);
			if (null == md) continue;
			final boolean update = null == checkbox || (i < checkbox.length && null != checkbox[i] && true == checkbox[i].equals("on"));
			if (true == update) {
				for (int j = 0; j < keys.length; j++) {
					if (j > arrays.length-1) continue;
					if (i > arrays[j].length-1) continue;
					md.set(keys[j], arrays[j][i]);
				}
			}
			list.add(md);
		}
		return list;
	}
	protected static java.util.List<dro.meta.Data> deleteMetaData(
		final dro.meta.data.Context mdc, /*final */java.lang.String[] ids, final java.lang.String[] checkbox
	) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.meta.Data> list = new ArrayList<>();
		for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
			if (true == ids[j].equals("{id}")) continue; // FIXME
			final dro.meta.Data md = mdc.returns(new java.lang.Integer(ids[j]), dro.meta.Data.Return.Data);
			if (null == md) continue;
			final boolean remove = null == checkbox || (j < checkbox.length && null != checkbox[j] && true == checkbox[j].equals("on"));
			if (true == remove) {
				mdc.remove(md, dro.meta.Data.Return.Data);
			} else {
				list.add(md);
			}
		}
		return list;
	}
	
	protected static java.util.List<dro.meta.Data> buildMetaDataList(final HttpServletRequest request, final java.lang.String view) {
	  /*final */java.util.List<dro.meta.Data> list = null;
		final Map<String, String[]> parameters = request.getParameterMap();
		final java.lang.String method = request.getMethod();
		
		final String[] mdc_id = parameters.get("mdc:id");
	  /*final */dro.meta.data.Context mdc = null;
		if (null != mdc_id && 0 < mdc_id.length && false == mdc_id[0].equals("{id}")) {
		  /*final dro.meta.data.Context */mdc = dro.meta.data.Context.returns(
				new java.lang.Integer(mdc_id[0]), dro.meta.data.Context.Return.Context
			);
		}
		
		final String[] submit_md = parameters.get("submit:md");
		if (false == Boolean.TRUE.booleanValue()) {
		
		// >C<RUD
		} else if ((null == request.getAttribute("md:state") && true == POST.equals(method)) && null != submit_md && 0 < submit_md.length && true == submit_md[0].equals("create")) {
		  /*final java.util.List<dro.meta.Data> */list = Servlet.createMetaData(mdc,
				parameters.get("md:id"),
				new java.lang.String[]{dro.meta.data.Context.$id,"$name"},
				new java.lang.String[][]{parameters.get("md:mdc:id"),parameters.get("md:name")}
			);
			
		// C>R<UD
		} else if (null != request.getAttribute("md:state") || (true == GET.equals(method) && null == submit_md)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (null != view && true == view.equals("one")) {
			  /*final java.util.List<dro.meta.Data>*/ list = Servlet.readMetaData(mdc,
					parameters.get("md:id")
				);
			} else if (null == view || true == view.equals("many")) {
			  /*final java.util.List<dro.meta.Data> */list = Servlet.readMetaData(mdc,
					parameters.get("md:id")
				);
			} else {
				throw new IllegalStateException();
			}
			
		// CR>U<D
		} else if ((null == request.getAttribute("md:state") && true == POST.equals(method)) && null != submit_md && 0 < submit_md.length && true == submit_md[0].equals("update")) {
		  /*final java.util.List<dro.meta.Data> */list = Servlet.updateMetaData(mdc,
				parameters.get("md:id"),
				new java.lang.String[]{dro.meta.data.Context.$id,"$name"},
				new java.lang.String[][]{parameters.get("md:mdc:id"),parameters.get("md:name")},
				parameters.get("md:checkbox")
			);
		
		// CRU>D<
		} else if ((null == request.getAttribute("md:state") && true == POST.equals(method)) && null != submit_md && 0 < submit_md.length && true == submit_md[0].equals("delete")) {
		  /*final java.util.List<dro.meta.Data> list = */Servlet.deleteMetaData(mdc,
				parameters.get("md:id"),
				parameters.get("md:checkbox")
			);
		  /*final java.util.List<dro.meta.Data> */list = Servlet.readMetaData(mdc,
				parameters.get("md:id")
			);
		
		} else {
		  /*final java.util.List<dro.meta.Data> */list = Servlet.readMetaData(mdc,
				parameters.get("md:id")
			);
		}
		
		if (0 == list.size() || null == view || true == view.equals("many")) {
			list.add(
				new dro.meta.Data()
					.set("$id", "{id}")
					.set("$name", "{name}")
					.set(dro.meta.data.Context.$id, "{id#mdc}")
			);
		}
		
		return list;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		final Map<String, String[]> parameters = request.getParameterMap();
		
		try {
		  //dro.lang.Event.entered(Servlet.class, this, "..", new Object[]{request,response});
			
		  //final java.lang.Boolean embed = Servlet.includeHeader(request, "md:embed", response);
			
			final java.util.Map<java.lang.String, java.lang.String> map = ServletTool.buildMap(request.getQueryString());
			
			final String view = map.get("md:view");
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == POST.equals(method) && null == request.getAttribute("md:state")) {
				final java.util.List<dro.meta.Data> list = Servlet.buildMetaDataList(request, view);
				request.setAttribute("md:state", 1);
				final String[] submit_md = parameters.get("submit:md");
				if (null != submit_md && 0 < submit_md.length) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == submit_md[0].equals("create")) {
						map.put("md:view", "one");
						Servlet.includeBody(request, map, list, response, /*false*/true);
					} else if (true == submit_md[0].equals("update")) { 
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else if (true == submit_md[0].equals("delete")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							map.put("md:view", "many");
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else {
						throw new IllegalStateException();
					}
				} else {
					Servlet.includeBody(request, map, list, response, true);
				}
			} else if (true == GET.equals(method) || null != request.getAttribute("md:state")) {
				final java.util.List<dro.meta.Data> list = Servlet.buildMetaDataList(request, view);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (null != view && view.equals("one")) {
				  //request.setAttribute("md:dc:id", parameters.get("md:dc:id"));
					Servlet.includeBody(request, map, list, response, true);
				} else if (null == view || view.equals("many")) {
					Servlet.includeBody(request, map, list, response, true);
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
			
		  //Servlet.includeFooter(request, response, embed);
			
		} finally {
		  //dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}