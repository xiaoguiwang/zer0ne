package dro.meta.data;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SubmitTag extends __abstract_Tag {
	protected String value;// = null;
	protected boolean href;// = false;
	
	{
	  //value = null;
	  //href = false;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == this.href) {
			if (true == this.value.equals("create")) {
				jw.write("<input id='id:md:submit-create' name='submit:md' type='submit' value='create'"+(true == super.md.get(dro.meta.data.Context.$id).toString().equals("{id#mdc}") ? "disabled" : "")+"/>\n");
			} else if (true == this.value.equals("update")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("md:id"); // TODO: go via md direct
				jw.write("<input id='id:md:submit-update' name='submit:md' type='submit'"+(true == super.md.get(dro.meta.data.Context.$id).toString().equals("{id#mdc}") || 1 == ids.length ? " disabled" : "")+"' value='update'/>\n");
			} else if (true == this.value.equals("delete")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("md:id"); // TODO: go via md direct
				jw.write("<input id='id:md:submit-delete' name='submit:md' type='submit'"+(true == super.md.get(dro.meta.data.Context.$id).toString().equals("{id#mdc}") || 1 == ids.length ? " disabled" : "")+"' value='delete'/>\n");
			}
		} else {
			if (true == this.value.equals("cancel")) {
				final String[] href = (String[])pageContext.getRequest().getAttribute("md:href"); // TODO: go via md direct
				jw.write("<a href='?"+href[0]+"md:view=many"+href[1]+"&cancel=md'>cancel</a>\n");
			}
		}
	}
}