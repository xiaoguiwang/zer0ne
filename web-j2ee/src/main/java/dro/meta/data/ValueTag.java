package dro.meta.data;

import java.io.IOException;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class ValueTag extends __abstract_Tag {
	protected String key;// = null;
	
	{
	  //key = false;
	}
	
	public void setKey(final String key) {
		this.key = key;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final Object o = super.md.get(this.key);
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == o instanceof Date) {
			final String dts = __abstract_Tag.getDateTimeStamp((Date)o);
			jw.write(dts);
		} else if (true == o instanceof String) {
			
		} else if (true == o instanceof Integer) {
			
		} else {
			throw new IllegalArgumentException();
		}
	}
}