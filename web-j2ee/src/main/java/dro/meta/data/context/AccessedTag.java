package dro.meta.data.context;

import java.io.IOException;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class AccessedTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String dts = __abstract_Tag.getDateTimeStamp((Date)super.mdc.get("$accessed-date-time-stamp"));
		jw.write(dts);
	}
}