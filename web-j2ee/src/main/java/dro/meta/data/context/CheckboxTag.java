package dro.meta.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class CheckboxTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdc.id().toString();
		if (false == id.equals("{id}")) {
			jw.write("<input id='id:mdc-checkbox' name='mdc:checkbox' type='checkbox'/>\n"); 
		}
	}
}