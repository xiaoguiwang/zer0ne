package dro.meta.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdTag extends __abstract_Tag {
	protected boolean href;// = false;
	
	{
	  //href = false;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdc.id().toString();
		jw.write("<input name='mdc:id' type='text' hidden=true value='"+id+"'/>\n");
		if (false == this.href) {
			jw.write("<input id='id:mdc' type='text' value='"+id+"' disabled/>\n");
		} else {
			final String[] href = (String[])pageContext.getRequest().getAttribute("mdc:href"); // TODO: go via mdc direct
			if (false == id.equals("{id}")) {
				jw.write("<a href='?"+href[0]+"mdc:id="+id+"&mdc:view=one"+href[1]+"'>"+id+"</a>\n");
			} else {
				jw.write("<input type='text' value='"+id+"' disabled/>\n");
			}
		}
	}
}