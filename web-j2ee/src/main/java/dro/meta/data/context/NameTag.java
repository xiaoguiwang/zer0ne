package dro.meta.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class NameTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String name = (String)super.mdc.get("$name");
		jw.write("<input id='id:mdc:name' name='mdc:name' type='text' value='"+name+"'/>\n");
	}
}