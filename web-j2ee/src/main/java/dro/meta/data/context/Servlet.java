package dro.meta.data.context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.ServletTool;

/*
https://www.cyberciti.biz/faq/understanding-etcpasswd-file-format/
/etc/passwd -- user-name, password, uid, gid, comment, home-directory, shell
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:/sbin/nologin
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	adm:x:3:4:adm:/var/adm:/sbin/nologin
	lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	operator:x:11:0:operator:/root:/sbin/nologin
	games:x:12:100:games:/usr/games:/sbin/nologin
	ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
	nobody:x:99:99:Nobody:/:/sbin/nologin
 */
/*
https://www.cyberciti.biz/faq/understanding-etcshadow-file/
/etc/shadow -- user-name, password, last-password-changed, password-minimum-age, password-maximum-age, password-expiry-warning-days, account-disable-after-password-expiry-days, expire (days since 1970/Jan/1st)
	root:$6$GhaZ43KoahtO4T97$rsiwWz4OslpUo8mVTMq615SY6u7nnssWijZhPMyAynAGBV7zYcmu3Z7mEQopHQZexuVT5tfe6lUiD8UV/sUwP/::0:99999:7:::
	bin:*:17492:0:99999:7:::
	daemon:*:17492:0:99999:7:::
	adm:*:17492:0:99999:7:::
	lp:*:17492:0:99999:7:::
	sync:*:17492:0:99999:7:::
	shutdown:*:17492:0:99999:7:::
	halt:*:17492:0:99999:7:::
	mail:*:17492:0:99999:7:::
	operator:*:17492:0:99999:7:::
	games:*:17492:0:99999:7:::
	ftp:*:17492:0:99999:7:::
	nobody:*:17492:0:99999:7:::
 */
/*
https://www.cyberciti.biz/faq/understanding-etcgroup-file/
/etc/group -- group-name, password, group-id/gid, group list (of comma-separated names)
	root:x:0:
	bin:x:1:
	daemon:x:2:
	sys:x:3:
	adm:x:4:
	tty:x:5:
	disk:x:6:
	lp:x:7:
	mem:x:8:
	kmem:x:9:
	wheel:x:10:fcladmin
	cdrom:x:11:
	mail:x:12:postfix
	man:x:15:
	dialout:x:18:
	floppy:x:19:
	games:x:20:
	tape:x:33:
	video:x:39:
	ftp:x:50:
	lock:x:54:
	audio:x:63:
	nobody:x:99:
 */

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String POST = "POST";
	private static final java.lang.String GET  = "GET" ;
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
		final dro.meta.data.Context mdc = dro.meta.data.Context.add(
			new dro.meta.data.Context("mdc"), dro.meta.data.Context.Return.Context
		);
		final dro.data.Context dc = mdc.add(
			new dro.data.Context(), dro.data.Context.Return.Context
		);
		{
			final dro.meta.Data md = mdc.add(
				new dro.meta.Data("/etc/passwd:line"), dro.meta.Data.Return.Data
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "user-name")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "password")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "uid")
				.set("$type", "number")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "gid")
				.set("$type", "number")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "comment")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "home-directory")
				.set("$type", "text") // TODO: path (new rule-set)
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "shell")
				.set("$type", "text") // TODO: shell (pre-determined drop-down)
				.set("$limit", "{0:1}") // TODO: {0:n} and supply list of valid shells.. (allow admin to add new ones, allow manager to pick, allow user to view)
				.set("$unique", "true")
				.set("$order", "first-in/out")
			);
		  /*md.add(new dro.meta.data.Field()
				.set("$name", "list-of-text")
				.set("$type", "text") // type: any, boolean, number, text, bytes/base16 (ascii-rendered hex) - support base64 encoded later (don't allow any, yet)
				.set("$limit", "{0:n}") // limit: limit-is-n/one or many/fixed (even if it is fixed it's not necessarily an array and even when not fixed isn't necessarily a set/map)
				.set("$unique", "true") // unique: yes or no
				.set("$order", "first-in/out") // order: order-is-un/known or random/hashed or un/pre-determined, order-is-fifo/lifo
			);*/
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("user-name", "root")
				.set("password", "x")
				.set("uid", Integer.valueOf("0"))
				.set("gid", Integer.valueOf("0"))
				.set("comment", "root")
				.set("home-directory", "/root")
				.set("shell", "/bin/bash")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("user-name", "bin")
				.set("password", "x")
				.set("uid", Integer.valueOf("1"))
				.set("gid", Integer.valueOf("1"))
				.set("comment", "bin")
				.set("home-directory", "/bin")
				.set("shell", "/sbin/nologin")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("user-name", "daemon")
				.set("password", "x")
				.set("uid", Integer.valueOf("2"))
				.set("gid", Integer.valueOf("2"))
				.set("comment", "daemon")
				.set("home-directory", "/sbin")
				.set("shell", "/sbin/nologin")
			);
		}
		{
			final dro.meta.Data md = mdc.add(
				new dro.meta.Data("/etc/shadow:line"), dro.meta.Data.Return.Data
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "user-name")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "password")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "last-pass-change")
				.set("$type", "text") // TODO: try Integer with null as df:value renders as blank
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "pass-min-age")
				.set("$type", "number")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "pass-max-age")
				.set("$type", "number")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "pass-expire-warn-days")
				.set("$type", "number")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "account-disable-after-pass-expire-days")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "expire-days-since-epoch")
				.set("$type", "text")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("user-name", "root")
				.set("password", "$6$GhaZ43..")
				.set("last-pass-change", "") // TODO: try null renders as blank
				.set("pass-min-age", Integer.valueOf("0"))
				.set("pass-max-age", Integer.valueOf("99999"))
				.set("pass-expire-warn-days", Integer.valueOf("7"))
				.set("account-disable-after-pass-expire-days", "")
				.set("expire-days-since-epoch", "")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("user-name", "bin")
				.set("password", "*")
				.set("last-pass-change", "17492") // TODO: try null renders as blank
				.set("pass-min-age", Integer.valueOf("0"))
				.set("pass-max-age", Integer.valueOf("99999"))
				.set("pass-expire-warn-days", Integer.valueOf("7"))
				.set("account-disable-after-pass-expire-days", "")
				.set("expire-days-since-epoch", "")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("user-name", "daemon")
				.set("password", "*")
				.set("last-pass-change", "17492") // TODO: try null renders as blank
				.set("pass-min-age", Integer.valueOf("0"))
				.set("pass-max-age", Integer.valueOf("99999"))
				.set("pass-expire-warn-days", Integer.valueOf("7"))
				.set("account-disable-after-pass-expire-days", "")
				.set("expire-days-since-epoch", "")
			);
		}
		{
			final dro.meta.Data md = mdc.add(
				new dro.meta.Data("/etc/group:line"), dro.meta.Data.Return.Data
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "group-name")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "password")
				.set("$type", "text")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "gid")
				.set("$type", "number")
			);
			md.add(new dro.meta.data.Field()
				.set("$name", "user-name-list")
				.set("$type", "text")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("group-name", "root")
				.set("password", "x")
				.set("gid", Integer.valueOf("0"))
				.set("user-name-list", "")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("group-name", "bin")
				.set("password", "x")
				.set("gid", Integer.valueOf("1"))
				.set("user-name-list", "")
			);
			md.add(new dro.Data(md)
				.set(dro.data.Context.$id, dc.id())
				.set("group-name", "daemon")
				.set("password", "x")
				.set("gid", Integer.valueOf("2"))
				.set("user-name-list", "")
			);
		}
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
  /*public static final java.lang.Boolean includeHeader(final HttpServletRequest request, final java.lang.String key, final HttpServletResponse response) throws IOException, ServletException {
		return ServletTool.includeHeader(request, key, "/html_head_title_body.jsp", response);
	}*/
	public static final void includeBody(final HttpServletRequest request, final java.util.Map<java.lang.String, java.lang.String> map, final java.util.List<dro.meta.data.Context> list, final HttpServletResponse response, /*final */boolean recurse) throws IOException, ServletException {
	  /*final boolean */recurse = false;
		final String mdc_id = null;//ServletTool.get(request, map, "mdc:id");
		final String md_id = ServletTool.get(request, map, "md:id");
		final String mdf_id = ServletTool.get(request, map, "mdf:id");
		final String dc_id = ServletTool.get(request, map, "dc:id");
		final String d_id = ServletTool.get(request, map, "d:id");
		final String df_id = ServletTool.get(request, map, "df:id");
		final StringBuffer[] sb = new StringBuffer[]{null,new StringBuffer()};
		ServletTool.appendIdAndView(null, mdc_id, "mdc", false);
		ServletTool.appendIdAndView(sb[1], md_id, "md", true);
		ServletTool.appendIdAndView(sb[1], mdf_id, "mdf", true);
		ServletTool.appendIdAndView(sb[1], dc_id, "dc", true);
		ServletTool.appendIdAndView(sb[1], d_id, "d", true);
		ServletTool.appendIdAndView(sb[1], df_id, "df", true);
		request.setAttribute("mdc:href", new String[]{"",sb[1].toString()});
		final String view = map.get("mdc:view");
		request.setAttribute("mdc:view", view);
		request.setAttribute("mdc:list", list);
		final java.util.List<java.lang.String> ids = new ArrayList<>();
		for (final dro.meta.data.Context mdc: list) {
			ids.add(mdc.id().toString());
		}
		request.setAttribute("mdc:id", ids.toArray(new String[0]));
		
	  //final String referer = request.getHeader("referer");
		{
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/meta/data/Context.jsp");
			requestDispatcher.include(request, response);
		}
		if (true == recurse) {
			request.setAttribute("md:embed", true);
			request.setAttribute("md:view", map.get("md:view"));
			request.setAttribute("md:id", map.get("md:id"));
			
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/meta/Data");
			requestDispatcher.include(request, response);
			
			request.setAttribute("md:embed", false);
		}
		if (true == recurse) {
			request.setAttribute("dc:embed", true);
			request.setAttribute("dc:view", map.get("dc:view"));
			request.setAttribute("dc:id", map.get("dc:id"));
			
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/data/Context");
			requestDispatcher.include(request, response);
			
			request.setAttribute("dc:embed", false);
		}
	}
  /*public static final void includeFooter(final HttpServletRequest request, final HttpServletResponse response, final java.lang.Boolean embed) throws IOException, ServletException {
		ServletTool.includeFooter(request, "/_body_html.jsp", response, embed);
	}*/
	
	protected static java.util.List<dro.meta.data.Context> createMetaDataContexts(
		final java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays
	) {
		arrays = ServletTool.arrays(ids, arrays, true);
		final java.util.List<dro.meta.data.Context> list = new ArrayList<>();
		for (int i = 0; i < arrays[0].length; i++) {
			final dro.meta.data.Context mdc = dro.meta.data.Context.add(new dro.meta.data.Context(), dro.meta.data.Context.Return.Context);
			for (int j = 0; j < keys.length; j++) {
				mdc.set(keys[j], arrays[j][i]);
			}
			list.add(mdc);
		}
		return list;
	}
	protected static java.util.List<dro.meta.data.Context> readMetaDataContexts(
	  /*final */java.lang.String[] ids
	) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.meta.data.Context> list = new ArrayList<>();
		if (null == ids) {
			final java.util.Set<dro.meta.data.Context> set = dro.meta.data.Context.returns(dro.meta.data.Context.Set.Return.Set);
			for (final dro.meta.data.Context mdc: set) {
				list.add(mdc);
			}
		} else {
			for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
				if (true == ids[j].equals("{id}")) continue; // FIXME
				final dro.meta.data.Context mdc = dro.meta.data.Context.returns(new java.lang.Integer(ids[j]), dro.meta.data.Context.Return.Context);
				if (null == mdc) continue;
				list.add(mdc);
			}
		}
		return list;
	}
	protected static java.util.List<dro.meta.data.Context> updateMetaDataContexts(
	  /*final */java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays, final java.lang.String[] checkbox
	) {
		final String[] __ids = ServletTool.ids(ids, arrays);
		arrays = ServletTool.arrays(ids, arrays, false);
		ids = __ids;
		final java.util.List<dro.meta.data.Context> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			final dro.meta.data.Context mdc = dro.meta.data.Context.returns(new java.lang.Integer(ids[i]), dro.meta.data.Context.Return.Context);
			if (null == mdc) continue;
			final boolean update = null == checkbox || (i < checkbox.length && null != checkbox[i] && true == checkbox[i].equals("on"));
			if (true == update) {
				for (int j = 0; j < keys.length; j++) {
					if (j > arrays.length-1) continue;
					if (i > arrays[j].length-1) continue;
					mdc.set(keys[j], arrays[j][i]);
				}
			}
			list.add(mdc);
		}
		return list;
	}
	protected static java.util.List<dro.meta.data.Context> deleteMetaDataContexts(
	  /*final */java.lang.String[] ids, final java.lang.String[] checkbox
	) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.meta.data.Context> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			if (true == ids[i].equals("{id}")) continue; // FIXME: ServletTool.ids is not doing it's job
			final dro.meta.data.Context mdc = dro.meta.data.Context.returns(new java.lang.Integer(ids[i]), dro.meta.data.Context.Return.Context);
			if (null == mdc) continue;
			final boolean remove = null == checkbox || (i < checkbox.length && null != checkbox[i] && true == checkbox[i].equals("on"));
			if (true == remove) {
				dro.meta.data.Context.remove(mdc, dro.meta.data.Context.Return.Context);
			} else {
				list.add(mdc);
			}
		}
		return list;
	}
	
	protected static java.util.List<dro.meta.data.Context> buildMetaDataContextList(final HttpServletRequest request, final java.lang.String view) {
	  /*final */java.util.List<dro.meta.data.Context> list = null;
		final Map<String, String[]> parameters = request.getParameterMap();
		final java.lang.String method = request.getMethod();
		
		final String[] submit_mdc = parameters.get("submit:mdc");
		
		if (false == Boolean.TRUE.booleanValue()) {
		
		// >C<RUD
		} else if ((null == request.getAttribute("mdc:state") && true == POST.equals(method)) && null != submit_mdc && 0 < submit_mdc.length && true == submit_mdc[0].equals("create")) {
		  /*final java.util.List<dro.meta.data.Context> */list = Servlet.createMetaDataContexts(
				parameters.get("mdc:id"),
				new java.lang.String[]{"$name"},
				new java.lang.String[][]{parameters.get("mdc:name")}
			);
			
		// C>R<UD
		} else if (null != request.getAttribute("mdc:state") || (true == GET.equals(method) && null == submit_mdc)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (null != view && true == view.equals("one")) {
			  /*final java.util.List<dro.meta.data.Context> */list = Servlet.readMetaDataContexts(
					parameters.get("mdc:id")
				);
			} else if (null == view || true == view.equals("many")) {
			  /*final java.util.List<dro.meta.data.Context> */list = Servlet.readMetaDataContexts(
					parameters.get("mdc:id")
				);
			} else {
				throw new IllegalStateException();
			}
			
		// CR>U<D
		} else if ((null == request.getAttribute("mdc:state") && true == POST.equals(method)) && null != submit_mdc && 0 < submit_mdc.length && true == submit_mdc[0].equals("update")) {
		  /*final java.util.List<dro.meta.data.Context> */list = Servlet.updateMetaDataContexts(
				parameters.get("mdc:id"),
				new java.lang.String[]{"$name"},
				new java.lang.String[][]{parameters.get("mdc:name")},
				parameters.get("mdc:checkbox")
			);
		
		// CRU>D<
		} else if ((null == request.getAttribute("mdc:state") && true == POST.equals(method)) && null != submit_mdc && 0 < submit_mdc.length && true == submit_mdc[0].equals("delete")) {
		  /*final java.util.List<dro.meta.data.Context> list = */Servlet.deleteMetaDataContexts(
				parameters.get("mdc:id"),
				parameters.get("mdc:checkbox")
			);
		  /*final java.util.List<dro.meta.data.Context> */list = Servlet.readMetaDataContexts(
				parameters.get("mdc:id")
			);
		
		} else {
		  /*final java.util.List<dro.meta.data.Context> */list = Servlet.readMetaDataContexts(
				parameters.get("mdc:id")
			);
		}
		
		if (0 == list.size() || null == view || true == view.equals("many")) {
			list.add(
				new dro.meta.data.Context()
					.set("$id", "{id}")
					.set("$name", "{name}")
			);
		}
		
		return list;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		final Map<String, String[]> parameters = request.getParameterMap();
		
		try {
		  //dro.lang.Event.entered(Servlet.class, this, "..", new Object[]{request,response});
			
		  //final java.lang.Boolean embed = Servlet.includeHeader(request, "mdc:embed", response);
			
			final java.util.Map<java.lang.String, java.lang.String> map = ServletTool.buildMap(request.getQueryString());
			
			final String view = map.get("mdc:view");
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == POST.equals(method) && null == request.getAttribute("mdc:state")) {
				final java.util.List<dro.meta.data.Context> list = Servlet.buildMetaDataContextList(request, view);
				request.setAttribute("mdc:state", 1);
				final String[] submit_mdc = parameters.get("submit:mdc");
				if (null != submit_mdc && 0 < submit_mdc.length) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == submit_mdc[0].equals("create")) {
						map.put("mdc:view", "one");
						Servlet.includeBody(request, map, list, response, /*false*/true);
					} else if (true == submit_mdc[0].equals("update")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else if (true == submit_mdc[0].equals("delete")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							map.put("mdc:view", "many");
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else {
						throw new IllegalStateException();
					}
				} else {
					Servlet.includeBody(request, map, list, response, true);
				}
			} else if (true == GET.equals(method) || null != request.getAttribute("mdc:state")) {
				final java.util.List<dro.meta.data.Context> list = Servlet.buildMetaDataContextList(request, view);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (null != view && view.equals("one")) {
					Servlet.includeBody(request, map, list, response, true);
				} else if (null == view || view.equals("many")) {
					Servlet.includeBody(request, map, list, response, true);
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
			
		  //Servlet.includeFooter(request, response, embed);
			
		} finally {
		  //dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}