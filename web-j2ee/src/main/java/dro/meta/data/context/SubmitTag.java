package dro.meta.data.context;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SubmitTag extends __abstract_Tag {
	protected String value;// = null;
	protected boolean href;// = false;
	
	{
	  //value = null;
	  //href = false;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdc.id().toString();
		if (false == this.href) {
			if (true == this.value.equals("create")) {
				jw.write("<input id='id:mdc:submit-create' name='submit:mdc' type='submit' value='create'"+(true == id.equals("{id}") ? "" : " disabled")+"/>\n");
			} else if (true == this.value.equals("update")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("mdc:id"); // TODO: go via mdc direct
				jw.write("<input id='id:mdc:submit-update' name='submit:mdc' type='submit'"+(false == id.equals("{id}") || 1 == ids.length ? " disabled" : "")+"' value='update'/>\n");
			} else if (true == this.value.equals("delete")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("mdc:id"); // TODO: go via mdc direct
				jw.write("<input id='id:mdc:submit-delete' name='submit:mdc' type='submit'"+(false == id.equals("{id}") || 1 == ids.length ? " disabled" : "")+"' value='delete'/>\n");
			}
		} else {
			if (true == this.value.equals("cancel")) {
				final String[] href = (String[])pageContext.getRequest().getAttribute("mdc:href"); // TODO: go via mdc direct
				jw.write("<a href='?"+href[0]+"mdc:view=many"+href[1]+"&cancel=mdc'>cancel</a>\n");
			}
		}
	}
}