package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class IdMetaDataTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		jw.write("<input name='mdf:md:id' type='text' hidden=true value='"+id+"'/>\n");
		jw.write("<input id='id:mdf:md' type='text' value='"+id+"' disabled/>\n");
	}
}