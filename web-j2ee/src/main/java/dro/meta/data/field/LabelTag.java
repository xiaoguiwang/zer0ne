package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class LabelTag extends __abstract_Tag {
	protected String value;// = null;
	
	{
	  //value = null;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == Boolean.TRUE.booleanValue()) {
		} else if (true == this.value.equals("$id")) {
			jw.write("$id");
		} else if (true == this.value.equals("$id#md")) {
			jw.write("$id#md");
		} else if (true == this.value.equals("$name")) {
			jw.write("$name");
		} else if (true == this.value.equals("$type")) {
			jw.write("$type");
		} else if (true == this.value.equals("$limit")) {
			jw.write("$limit");
		} else if (true == this.value.equals("$order")) {
			jw.write("$order");
		} else if (true == this.value.equals("$unique")) {
			jw.write("$unique");
		} else if (true == this.value.equals("$lookup")) {
			jw.write("$lookup");
		} else {
		  //throw new IllegalArgumentException();
			jw.write(this.value);
		}
	}
}