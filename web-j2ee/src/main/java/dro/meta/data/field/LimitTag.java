package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class LimitTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		final String limit;// = null;
		if (true == id.equals("{id#md}")) {
			limit = "{n/a}";
		} else {
			limit = (String)super.mdf.get("$limit");
		}
		jw.write("<input id='id:mdf:limit' name='mdf:limit' type='text' value='"+(null == limit || true == limit.equals("") ? "{0:1}" : limit)+"'"+(true == id.equals("{id#md}") || null == limit || true == limit.equals("") || true == limit.equals("{n/a}") ? " disabled" : "")+"/>");
	}
}