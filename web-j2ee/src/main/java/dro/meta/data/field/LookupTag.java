package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class LookupTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		final String lookup;// = null;
		if (true == id.equals("{id#md}")) {
			lookup = "{n/a}";
		} else {
			final String limit = (String)super.mdf.get("$limit");
			if (null == limit || true == limit.equals("{0:1}")) {
				lookup = "{n/a}";
			} else {
				lookup = (String)super.mdf.get("$lookup");
			}
		}
		if (null == lookup || true == lookup.equals("{n/a}")) {
			jw.write("<input id='id:mdf:lookup' name='mdf:lookup' value='"+"{n/a}"+"' disabled/>\n");
		} else {
			jw.write("<select id='id:mdf:lookup' name='mdf:lookup'"+(true == id.equals("{id#md}") || null == lookup ||  true == lookup.equals("") || true == lookup.equals("{n/a}") ? " disabled" : "")+">\n");
			jw.write("<option value='true'"+(null != lookup && (true == lookup.equals("") || true == lookup.equals("true")) ? " selected" : "")+">yes</option>\n");
			jw.write("<option value='false'"+(null != lookup && (true == lookup.equals("false")) ? " selected" : "")+">no</option>\n");
			jw.write("</select>\n");
		}
	}
}