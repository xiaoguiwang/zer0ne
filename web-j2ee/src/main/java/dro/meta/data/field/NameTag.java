package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class NameTag extends __abstract_Tag {
	protected boolean immutable;// = false;
	
	{
	  //immutable = false;
	}
	
	public void setImmutable(final boolean immutable) {
		this.immutable = immutable;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		final String name;// = null;
		if (true == id.equals("{id#md}")) {
			name = "{name}";
		} else {
			name = (String)super.mdf.get("$name");
		}
		if (false == this.immutable) {
			jw.write("<input id='id:mdf:name' name='mdf:name' type='text' value='"+name+"'"+(true == id.equals("{id#md}") ? " disabled" : "")+"/>\n");
		} else {
			jw.write(name+"\n");
		}
	}
}