package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class OrderTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		final String order;// = null;
		if (true == id.equals("{id#md}")) {
			order = "{n/a}";
		} else {
			final String limit = (String)super.mdf.get("$limit");
			if (null == limit || true == limit.equals("{0:1}")) {
				order = "{n/a}";
			} else {
				order = (String)super.mdf.get("$order");
			}
		}
		if (null == order || order.equals("{n/a}")) {
			jw.write("<input id='id:mdf:order' name='mdf:order' value='"+"{n/a}"+"' disabled/>\n");
		} else {
			jw.write("<select id='id:mdf:order' name='mdf:order'"+(true == id.equals("{id#md}") || null == order || true == order.equals("") || true == order.equals("{n/a}") ? " disabled" : "")+">\n");
			jw.write("<option value='unordered'"+(null != order && (true == order.equals("unordered")) ? " selected" : "")+">unordered</option>\n");
			jw.write("<option value='ordered'"+(null != order && (true == order.equals("ordered")) ? " selected" : "")+">ordered</option>\n");
			jw.write("<option value='first-in/out'"+(null != order && (true == order.equals("first-in/out")) ? " selected" : "")+">first-in/out</option>\n");
			jw.write("<option value='last-in/out'"+(null != order && (true == order.equals("last-in/out")) ? " selected" : "")+">last-in/out</option>\n");
			jw.write("</select>\n");
		}
	}
}