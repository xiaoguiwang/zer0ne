package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SearchTag extends __abstract_Tag {
	protected String order;// = null;
	protected String value;// = null;
	
	{
	  //order = null;
	  //value = null;
	}
	
	public void setOrder(final String eitherAscOrDesc) {
		this.order = eitherAscOrDesc;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.id().toString();
		final String[] search = (String[])pageContext.getRequest().getAttribute("mdf#"+id+":search");
		if (null == this.order) {
			jw.write("<input id='id:mdf#"+id+":search' name='mdf#"+id+":search' type='text' value='"+(null == search || null == search[0] ? "" : search[0])+"'/>\n");
		} else {
			if (true == this.order.equals("ascending")) {
				jw.write("<input id='id:mdf#"+id+":order' name='mdf#"+id+":order' type='submit' value='^'/>\n");
			} else if (true == this.order.equals("descending")) {
				jw.write("<input id='id:mdf#"+id+":order' name='mdf#"+id+":order' type='submit' value='v''/>\n");
			} else {
				throw new IllegalArgumentException();
			}
		}
	}
}