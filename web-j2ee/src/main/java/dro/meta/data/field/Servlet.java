package dro.meta.data.field;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dro.util.ServletTool;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String POST = "POST";
	private static final java.lang.String GET = "GET";
	
  //private static final String className = Servlet.class.getName();
	
  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	private static final java.util.Map<java.lang.String, java.lang.String> staticMetaDataTypes;// = null;
	
	static {
		staticMetaDataTypes = new java.util.LinkedHashMap<java.lang.String, java.lang.String>(){
			private static final long serialVersionUID = 0L;
			{
				put("{value}", "{undefined}");
				put("boolean", "boolean");
				put("number" , "number" );
				put("text"   , "text"   );
				put("bytes"  , "bytes"  );
				put("data"   , "data"   );
			}
		};
	}
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
  /*public static final java.lang.Boolean includeHeader(final HttpServletRequest request, final java.lang.String key, final HttpServletResponse response) throws IOException, ServletException {
		return ServletTool.includeHeader(request, key, "/html_head_title_body.jsp", response);
	}*/
	public static final void includeBody(final HttpServletRequest request, final java.util.Map<java.lang.String, java.lang.String> map, final java.util.List<dro.meta.data.Field> list, final HttpServletResponse response, /*final */boolean recurse) throws IOException, ServletException {
	  /*final boolean */recurse = false;
		final String mdc_id = ServletTool.get(request, map, "mdc:id");
		final String md_id = ServletTool.get(request, map, "md:id");
		request.setAttribute("md-id", new String[]{map.get("md:id")});
		final String mdf_id = null;//ServletTool.get(request, map, "mdf:id");
		final String dc_id = ServletTool.get(request, map, "dc:id");
		final String d_id = ServletTool.get(request, map, "d:id");
		final String df_id = ServletTool.get(request, map, "df:id");
		final StringBuffer[] sb = new StringBuffer[]{new StringBuffer(),new StringBuffer()};
		ServletTool.appendIdAndView(sb[0], mdc_id, "mdc", false);
		ServletTool.appendIdAndView(sb[0], md_id, "md", true);
		ServletTool.appendIdAndView(null, mdf_id, "mdf", true);
		ServletTool.appendIdAndView(sb[1], dc_id, "dc", true);
		ServletTool.appendIdAndView(sb[1], d_id, "d", true);
		ServletTool.appendIdAndView(sb[1], df_id, "df", true);
		request.setAttribute("mdf:href", new String[]{sb[0].append("&").toString(),sb[1].toString()});
		final String view = map.get("mdf:view");
		request.setAttribute("mdf:view", view);
		request.setAttribute("mdf:list", list);
		final java.util.List<java.lang.String> ids = new ArrayList<>();
		for (final dro.meta.data.Field mdf: list) {
			ids.add(mdf.id().toString());
		}
		request.setAttribute("mdf:id", ids.toArray(new String[0]));
		final java.util.Map<java.lang.String, java.lang.String> dynamicMetaDataTypes = new java.util.LinkedHashMap<>();
	  /*final */dro.meta.data.Context mdc = null;
		if (null != mdc_id) {
		  /*final dro.meta.data.Context */mdc = dro.meta.data.Context.returns(
				new java.lang.Integer(mdc_id), dro.meta.data.Context.Return.Context
			);
		}
	  /*final */java.util.Map<java.lang.Object, dro.meta.Data> mds = null;
		if (null != mdc) {
			mds = mdc.returns(dro.meta.Data.Map.Return.Map);
		}
		if (null != mds) {
			for (final java.lang.Object id: mds.keySet()) {
				final dro.meta.Data md = mds.get(id);
				if (true == md.id().toString().equals(md_id)) continue;
				dynamicMetaDataTypes.put("md#"+md.get("$id"), "md#"+md.get("$id"));
			}
			request.setAttribute("mdf:types", new java.util.LinkedHashMap<java.lang.String, java.lang.String>(){
				private static final long serialVersionUID = 0L;
				{
					putAll(staticMetaDataTypes);
					putAll(dynamicMetaDataTypes);
				}
			});
		}
		
		// final String referer = request.getHeader("referer");
		{
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/meta/data/Field.jsp");
			requestDispatcher.include(request, response);
		}
		// Don't think it's a good idea (but noting it all the same) that we should recurse from mdf to df
	  /*if (true == recurse) {
			request.setAttribute("df:embed", true);
			request.setAttribute("df:view", map.get("df:view"));
			request.setAttribute("df:id", map.get("df:id"));
			request.setAttribute("df:d:id", map.get("df:md:id"));
			
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher("/dro/data/Field");
			requestDispatcher.include(request, response);
			
			request.setAttribute("df:embed", false);
		}*/
	}
  /*public static final void includeFooter(final HttpServletRequest request, final HttpServletResponse response, final java.lang.Boolean embed) throws IOException, ServletException {
		ServletTool.includeFooter(request, "/_body_html.jsp", response, embed);
	}*/
	
	protected static java.util.List<dro.meta.data.Field> createMetaDataField(
		final dro.meta.Data md, final java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays
	) {
		arrays = ServletTool.arrays(ids, arrays, true);
		final java.util.List<dro.meta.data.Field> list = new ArrayList<>();
		for (int i = 0; i < arrays[0].length; i++) {
			final dro.meta.data.Field mdf = md.add(new dro.meta.data.Field(), dro.meta.data.Field.Return.Field);
			for (int j = 0; j < keys.length; j++) {
				mdf.set(keys[j], arrays[j][i]);
			}
			list.add(mdf);
		}
		return list;
	}
	
	protected static java.util.List<dro.meta.data.Field> readMetaDataField(
		final dro.meta.Data md, /*final */java.lang.String[] ids
	) {
		final java.util.List<dro.meta.data.Field> list = new ArrayList<>();
		if (null == md) {
			// TOOD: build mdf for all md in all mdc..
		} else {
			if (null != ids) {
				ids = ServletTool.ids(ids);
			}
			if (null == ids) {
				final java.util.Set<dro.meta.data.Field> set = md.returns(dro.meta.data.Field.Return.Set);
				for (final dro.meta.data.Field mdf: set) {
					list.add(mdf);
				}
			} else {
				for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
					if (true == ids[j].equals("{id}")) continue; // FIXME
					final dro.meta.data.Field mdf = md.returns(new java.lang.Integer(ids[j]), dro.meta.data.Field.Return.Field);
					if (null == mdf) continue;
					list.add(mdf);
				}
			}
		}
		return list;
	}
	
	protected static java.util.List<dro.meta.data.Field> updateMetaDataField(
		final dro.meta.Data md, /*final */java.lang.String[] ids, final java.lang.String[] keys, /*final */java.lang.String[][] arrays, final java.lang.String[] checkbox
	) {
		final String[] __ids = ServletTool.ids(ids, arrays);
		arrays = ServletTool.arrays(ids, arrays, false);
		ids = __ids;
		final java.util.List<dro.meta.data.Field> list = new ArrayList<>();
		for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
			if (i > arrays[0].length-1) break; // FIXME: Hack! 
			final dro.meta.data.Field mdf = md.returns(new java.lang.Integer(ids[i]), dro.meta.data.Field.Return.Field);
			final boolean update = null == checkbox || (i < checkbox.length && null != checkbox[i] && true == checkbox[i].equals("on"));
			if (true == update) {
				for (int j = 0; j < keys.length; j++) {
					mdf.set(keys[j], arrays[j][i]);
				}
			}
			list.add(mdf);
		}
		return list;
	}
	
	protected static java.util.List<dro.meta.data.Field> deleteMetaDataField(
		final dro.meta.Data md, /*final */java.lang.String[] ids, final java.lang.String[] checkbox
	) {
		ids = ServletTool.ids(ids);
		final java.util.List<dro.meta.data.Field> list = new ArrayList<>();
		for (int j = 0; j < (null == ids ? 0 : ids.length); j++) {
			if (true == ids[j].equals("{id}")) continue; // FIXME
			final dro.meta.data.Field mdf = md.returns(new java.lang.Integer(ids[j]), dro.meta.data.Field.Return.Field);
			final boolean remove = null == checkbox || (j < checkbox.length && null != checkbox[j] && true == checkbox[j].equals("on"));
			if (true == remove) {
				md.remove(mdf, dro.meta.data.Field.Return.Field);
			} else {
				list.add(mdf);
			}
		}
		return list;
	}
	
	protected static java.util.List<dro.meta.data.Field> buildMetaDataFieldList(final HttpServletRequest request, final java.lang.String view) {
	  /*final */java.util.List<dro.meta.data.Field> list = null;
		final Map<String, String[]> parameters = request.getParameterMap();
		final java.lang.String method = request.getMethod();
		
		final String[] mdc_id = parameters.get("mdc:id");
	  /*final */dro.meta.data.Context mdc = null;
		if (null != mdc_id && 0 < mdc_id.length && false == mdc_id[0].equals("{id}")) {
		  /*final dro.meta.data.Context */mdc = dro.meta.data.Context.returns(
				new java.lang.Integer(mdc_id[0]), dro.meta.data.Context.Return.Context
			);
		}
		final String[] md_id = parameters.get("md:id");
	  /*final */dro.meta.Data md = null;
		if (null != md_id && 0 < md_id.length && false == md_id[0].equals("{id}") && null != mdc) {
		  /*final dro.meta.Data */md = mdc.returns(
				new java.lang.Integer(md_id[0]), dro.meta.Data.Return.Data
			);
		}
		
		final String[] submit_mdf = parameters.get("submit:mdf");
		if (false == Boolean.TRUE.booleanValue()) {
			
		// >C<RUD
		} else if ((null == request.getAttribute("mdf:state") && true == POST.equals(method)) && null != submit_mdf && 0 < submit_mdf.length && true == submit_mdf[0].equals("create")) {
		  /*final java.util.List<dro.meta.data.Field> */list = Servlet.createMetaDataField(md,
				parameters.get("mdf:id"),
				new java.lang.String[]{dro.meta.Data.$id,"$name","$type","$limit","$unique","$order","$lookup"},
				new java.lang.String[][]{parameters.get("mdf:md:id"),parameters.get("mdf:name"),parameters.get("mdf:type"),parameters.get("mdf:limit"),parameters.get("mdf:unique"),parameters.get("mdf:order"),parameters.get("mdf:lookup")}
			);
		
		// C>R<UD
		} else if (null != request.getAttribute("mdf:state") || (true == GET.equals(method) && null == submit_mdf)) {
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (null != view && true == view.equals("one")) {
			  /*final java.util.List<dro.meta.data.Field> */list = Servlet.readMetaDataField(md,
					parameters.get("mdf:id")
				);
			} else if (null == view || true == view.equals("many")) {
			  /*final java.util.List<dro.meta.data.Field> */list = Servlet.readMetaDataField(md,
					parameters.get("mdf:id")
				);
			} else {
				throw new IllegalStateException();
			}
			
		// CR>U<D
		} else if ((null == request.getAttribute("mdf:state") && true == POST.equals(method)) && null != submit_mdf && 0 < submit_mdf.length && true == submit_mdf[0].equals("update")) {
		  /*java.util.List<dro.meta.data.Field> */list = Servlet.updateMetaDataField(md,
				parameters.get("mdf:id"),
				new java.lang.String[]{dro.meta.Data.$id,"$name","$type","$limit","$unique","$order","$lookup"},
				new java.lang.String[][]{parameters.get("mdf:md:id"),parameters.get("mdf:name"),parameters.get("mdf:type"),parameters.get("mdf:limit"),parameters.get("mdf:unique"),parameters.get("mdf:order"),parameters.get("mdf:lookup")},
				parameters.get("mdf:checkbox")
			);
		
		// CRU>D<
		} else if ((null == request.getAttribute("mdf:state") && true == POST.equals(method)) && null != submit_mdf && 0 < submit_mdf.length && true == submit_mdf[0].equals("delete")) {
		  /*final java.util.List<dro.meta.data.Field> list =*/Servlet.deleteMetaDataField(md,
				parameters.get("mdf:id"),
				parameters.get("mdf:checkbox")
			);
		  /*final java.util.List<dro.meta.data.Field> */list = Servlet.readMetaDataField(md,
				parameters.get("mdf:id")
			);
		
		} else {
		  /*final java.util.List<dro.meta.data.Field> */list = Servlet.readMetaDataField(md,
				parameters.get("mdf:id")
			);
		}
		
		if (0 == list.size() || null == view || true == view.equals("many")) {
			list.add(
				new dro.meta.data.Field()
					.set("$id", "{id}")
					.set(dro.meta.Data.$id, "{id#md}")
					.set("$name", "{name}")
					.set("$type", "{type}")
					.set("$limit", "{limit}")
					.set("$unique", "{unique}")
					.set("$order", "{order}")
					.set("$lookup", "{lookup}")
			);
		}
		
		return list;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		final Map<String, String[]> parameters = request.getParameterMap();
		
		try {
		  //dro.lang.Event.entered(Servlet.class, this, "..", new Object[]{request,response});
			
		  //final java.lang.Boolean embed = Servlet.includeHeader(request, "mdf:embed", response);
			
			final java.util.Map<java.lang.String, java.lang.String> map = ServletTool.buildMap(request.getQueryString());
			
			final String view = map.get("mdf:view");
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == POST.equals(method) && null == request.getAttribute("mdf:state")) {
				final java.util.List<dro.meta.data.Field> list = Servlet.buildMetaDataFieldList(request, view);
				request.setAttribute("mdf:state", 1);
				final String[] submit_mdf = parameters.get("submit:mdf");
				if (null != submit_mdf && 0 < submit_mdf.length) {
					if (false == Boolean.TRUE.booleanValue()) {
					} else if (true == submit_mdf[0].equals("create")) {
						map.put("mdf:view", "one");
						Servlet.includeBody(request, map, list, response, /*false*/true);
					} else if (true == submit_mdf[0].equals("update")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || true == view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else if (true == submit_mdf[0].equals("delete")) {
						if (false == Boolean.TRUE.booleanValue()) {
						} else if (null != view && view.equals("one")) {
							map.put("mdf:view", "many");
							Servlet.includeBody(request, map, list, response, true);
						} else if (null == view || view.equals("many")) {
							Servlet.includeBody(request, map, list, response, true);
						} else {
							throw new IllegalStateException();
						}
					} else {
						throw new IllegalStateException();
					}
				} else {
					Servlet.includeBody(request, map, list, response, true);
				}
			} else if (true == GET.equals(method) || null != request.getAttribute("mdf:state")) {
				final java.util.List<dro.meta.data.Field> list = Servlet.buildMetaDataFieldList(request, view);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (null != view && view.equals("one")) {
					final String mdf_id = ServletTool.get(request, map, "df:mdf:id");
					request.setAttribute("df:mdf:id", mdf_id);
					Servlet.includeBody(request, map, list, response, true);
				} else if (null == view || view.equals("many")) {
					Servlet.includeBody(request, map, list, response, true);
				} else {
					throw new IllegalStateException();
				}
			} else {
				throw new UnsupportedOperationException();
			}
			
		  //Servlet.includeFooter(request, response, embed);
			
		} finally {
		  //dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}