package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class SubmitTag extends __abstract_Tag {
	protected String value;// = null;
	protected boolean href;// = false;
	
	{
	  //value = null;
	  //href = false;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public void setHref(final boolean href) {
		this.href = href;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		if (false == this.href) {
			if (true == this.value.equals("create")) {
				jw.write("<input id='id:mdf:submit-create' name='submit:mdf' type='submit' value='create'"+(true == super.mdf.get(dro.meta.Data.$id).toString().equals("{id#md}") ? "disabled" : "")+"/>\n");
			} else if (true == this.value.equals("update")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("mdf:id"); // TODO: go via mdf direct
				jw.write("<input id='id:mdf:submit-update' name='submit:mdf' type='submit'"+(true == super.mdf.get(dro.meta.Data.$id).toString().equals("{id#md}") || 1 == ids.length ? " disabled" : "")+"' value='update'/>\n");
			} else if (true == this.value.equals("delete")) {
				final String[] ids = (String[])pageContext.getRequest().getAttribute("mdf:id"); // TODO: go via mdf direct
				jw.write("<input id='id:mdf:submit-delete' name='submit:mdf' type='submit'"+(true == super.mdf.get(dro.meta.Data.$id).toString().equals("{id#md}") || 1 == ids.length ? " disabled" : "")+"' value='delete'/>\n");
			}
		} else {
			if (true == this.value.equals("cancel")) {
				final String[] href = (String[])pageContext.getRequest().getAttribute("mdf:href"); // TODO: go via mdf direct
				jw.write("<a href='?"+href[0]+"mdf:view=many"+href[1]+"&cancel=mdf'>cancel</a>\n");
			}
		}
	}
}