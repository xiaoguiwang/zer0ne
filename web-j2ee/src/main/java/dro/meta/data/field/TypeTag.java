package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class TypeTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		final String type;// = null;
		if (true == id.equals("{id#md}")) {
			type = "{type}";
			jw.write("<input id='id:mdf:type' name='mdf:type' value='"+type+"' "+(true == id.equals("{id#md}") ? " disabled" : "")+">\n");
		} else {
			type = (String)super.mdf.get("$type");
			@SuppressWarnings("unchecked")
			final java.util.Map<String, String> mdf_types = (java.util.Map<String, String>)pageContext.getRequest().getAttribute("mdf:types"); // TODO: go via mdf direct
			jw.write("<select id='id:mdf:type' name='mdf:type'"+(true == id.equals("{id#md}") ? " disabled" : "")+">\n");
			for (final String key: mdf_types.keySet()) {
				jw.write("<option value='"+key+"'"+(true == type.equals(mdf_types.get(key)) ? " selected" : "")+">"+mdf_types.get(key)+"</option>\n");
			}
			jw.write("</select>\n");
		}
	}
}