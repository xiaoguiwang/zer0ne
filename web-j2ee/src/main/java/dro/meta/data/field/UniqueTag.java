package dro.meta.data.field;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

public class UniqueTag extends __abstract_Tag {
	@Override
	public void doTag() throws JspException, IOException {
		final PageContext pageContext = (PageContext)super.getJspContext();
		final JspWriter jw = pageContext.getOut();
		final String id = super.mdf.get(dro.meta.Data.$id).toString();
		final String unique;// = null;
		if (true == id.equals("{id#md}")) {
			unique = "{n/a}";
		} else {
			final String limit = (String)super.mdf.get("$limit");
			if (null == limit || true == limit.equals("{0:1}")) {
				unique = "{n/a}";
			} else {
				unique = (String)super.mdf.get("$unique");
			}
		}
		if (null == unique || true == unique.equals("{n/a}")) {
			jw.write("<input id='id:mdf:unique' name='mdf:unique' value='"+"{n/a}"+"' disabled/>\n");
		} else {
			jw.write("<select id='id:mdf:unique' name='mdf:unique'"+(true == id.equals("{id#md}") || null == unique || true == unique.equals("") || true == unique.equals("{n/a}") ? " disabled" : "")+"/>\n");
			jw.write("<option value='true'"+(null != unique && (true == unique.equals("") || true == unique.equals("true")) ? " selected" : "")+">yes</option>\n");
			jw.write("<option value='false'"+(null != unique && (true == unique.equals("false")) ? " selected" : "")+">no</option>\n");
			jw.write("</select>\n");
		}
	}
}