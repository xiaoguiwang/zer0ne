package dro.util;

// https://stackoverflow.com/questions/20483208/multiple-sort-in-java/20483654
public class CompareTool implements java.util.Comparator<dro.Data> {
	protected dro.meta.Data md;// = null;
	protected java.util.Map<java.lang.Integer, java.lang.String> ordering;// = null;
	
	@SuppressWarnings("unused")
	private CompareTool() {
	}
	public CompareTool(final dro.meta.Data md, final java.util.Map<java.lang.Integer, java.lang.String> ordering) {
		this.md = md;
		this.ordering = ordering;
	}
	
	private static java.lang.Object __HACK__dataGetFieldValueForFieldName(final dro.Data d, final java.lang.String name) {
	  /*final */Object o = null;
		final java.util.Set<dro.data.Field> set = d.returns(dro.data.Field.Set.Return.Set);
		for (final dro.Data df: set) {
			if (true == ((String)df.get("$name")).equals(name)) {
				o = df.get("$value");
			}
		}
		return o;
	}
	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final dro.Data d1, final dro.Data d2) {
	  /*final */java.lang.Integer compare = null;
		if (d1 == d2) {
			compare = new java.lang.Integer(0);
		} else if (null == d1) { // && null != d2
			compare = new java.lang.Integer(-1);
		} else if (null == d2) { // && null != d1
			compare = new java.lang.Integer(+1);
		} else {
			for (final java.lang.Integer id: this.ordering.keySet()) {
				int sign = +1;
				final dro.meta.data.Field mdf = this.md.returns(id, dro.meta.data.Field.Return.Field);
				if (null == mdf) break;
				final java.lang.String order = this.ordering.get(id);
				if (null == order) {
				} else if (true == order.equals("^")) {
					sign = +1;
				} else if (true == order.equals("v")) {
					sign = -1;
				} else {
					throw new IllegalArgumentException();
				}
				final java.lang.String name = (String)mdf.get("$name");
				final java.lang.String type = (String)mdf.get("$type");
				final java.lang.Object o1 = CompareTool.__HACK__dataGetFieldValueForFieldName(d1, name);
				final java.lang.Object o2 = CompareTool.__HACK__dataGetFieldValueForFieldName(d2, name);
				if (false == Boolean.TRUE.booleanValue()) {
				} else if (true == type.equals("number")) {
					if (o1 == o2) {
						continue;
					} else if (null == o1) {
						compare = new java.lang.Integer(-sign);
					} else if (null == o2) {
						compare = new java.lang.Integer(+sign);
					} else if (true == o1 instanceof java.lang.Integer && true == o2 instanceof java.lang.Integer) {
						final java.lang.Integer v1 = (java.lang.Integer)o1;
						final java.lang.Integer v2 = (java.lang.Integer)o2;
						final int compareTo = v1.compareTo(v2);
						if (0 == compareTo) {
							continue;
						} else {
							compare = new java.lang.Integer(sign*compareTo);
							break;
						}
					} else {
						throw new IllegalArgumentException();
					}
				} else if (true == type.equals("text")) {
					if (o1 == o2) {
						continue;
					} else if (null == o1) {
						compare = new java.lang.Integer(-sign);
					} else if (null == o2) {
						compare = new java.lang.Integer(+sign);
					} else if (true == o1 instanceof java.lang.String && true == o2 instanceof java.lang.String) {
						final java.lang.String v1 = (java.lang.String)o1;
						final java.lang.String v2 = (java.lang.String)o2;
						final int compareTo = v1.compareTo(v2);
						if (0 == compareTo) {
							continue;
						} else {
							compare = new java.lang.Integer(sign*compareTo);
							break;
						}
					} else {
						throw new IllegalArgumentException();
					}
				} else {
					throw new IllegalStateException();
				}
			}
			if (null == compare) {
				compare = new java.lang.Integer(0);
			}
		}
		return null == compare ? 0 : compare.intValue();
	}
}