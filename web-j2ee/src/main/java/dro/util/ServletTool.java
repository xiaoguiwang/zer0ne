package dro.util;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ServletTool {
	public static final java.util.Map<java.lang.String, java.lang.String> buildMap(final String queryString) {
	  /*final */java.lang.String[] pairs = null;
		if (null != queryString) {
			pairs = queryString.split("&");
		}
		final java.util.Map<java.lang.String, java.lang.String> map = new LinkedHashMap<>();
		if (null != pairs) {
			for (final java.lang.String pair: pairs) {
				final java.lang.String[] split = pair.split("=", 2);
				if (1 == split.length) { 
					map.put(split[0], "");
				} else {
					map.put(split[0], split[1]);
				}
			}
		}
		return map;
	}
	
  /*public static final java.lang.Boolean includeHeader(final HttpServletRequest request, final java.lang.String key, final java.lang.String path, final HttpServletResponse response) throws IOException, ServletException {
		java.lang.Boolean embed = null;
		if (null != request.getAttribute(key)) {
			embed = (java.lang.Boolean)request.getAttribute(key);
		}
		if (null == embed || false == embed) {
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher(path);
			requestDispatcher.include(request, response);
		}
		return embed;
	}*/
  /*public static final void includeFooter(final HttpServletRequest request, final java.lang.String path, final HttpServletResponse response, final java.lang.Boolean embed) throws IOException, ServletException {
		if (null == embed || false == embed) {
			final RequestDispatcher requestDispatcher = request.getRequestDispatcher(path);
			requestDispatcher.include(request, response);
		  //response.setStatus(200);
		}
	}*/
	
	public static String[] ids(final String[] ids, final String[][] arrays) {
		java.util.List<String> list = null;
		boolean[] isId = null;
		if (null != arrays) {
			isId = new boolean[ids.length];
			for (int i = 0; i < ids.length; i++) {
				isId[i] = ids[i].startsWith("{id");
			}
		}
		for (int j = 0; j < (null == arrays ? 0 : arrays.length); j++) {
			for (int i = 0; i < (null == arrays[j] ? 0 : arrays[j].length); i++) {
				if (null != arrays[j][i] && true == arrays[j][i].startsWith("{id") && i < isId.length && false == isId[i]) {
					isId[i] = true;
				}
			}
		}
		for (int i = 0; i < (null == isId ? 0 : isId.length); i++) {
			if (false == isId[i]) {
				if (null == list) {
					list = new java.util.ArrayList<String>();
				}
				final String value = ids[i];
				list.add(value);
			}
		}
		return null == list ? ids : list.toArray(new String[0]);
	}
	public static String[] ids(final String[] ids) {
		return ServletTool.ids(ids, (String[][])null);
	}
	public static String[][] arrays(final String[] ids, final String[][] arrays, final boolean create) {
		@SuppressWarnings("unchecked")
		java.util.List<String>[] lists = new java.util.List[arrays.length];
		boolean[] isId = new boolean[ids.length];
		for (int i = 0; i < ids.length; i++) {
			isId[i] = null != ids[i] && true == ids[i].startsWith("{id");
		}
		for (int j = 0; j < arrays.length; j++) {
			lists[j] = new java.util.ArrayList<String>();
			for (int i = 0; i < arrays[j].length; i++) {
				if (null != arrays[j][i] && true == arrays[j][i].startsWith("{id") && i < isId.length && false == isId[i]) {
					isId[i] = true;
				}
			}
		}
		for (int j = 0; j < arrays.length; j++) {
			for (int i = 0; i < arrays[j].length; i++) {
				if (i < isId.length && create == isId[i]) {
					final String value = arrays[j][i];
					lists[j].add(value);
				}
			}
			arrays[j] = lists[j].toArray(new String[0]);
		}
		return arrays;
	}
	public static String[][] arrays(final String[][] arrays) {
		return ServletTool.arrays((String[])null, arrays, false);
	}
	
	public static String get(final HttpServletRequest request, final java.util.Map<java.lang.String, java.lang.String> map, final String key) {
	  /*final */String returns = null;
	  /*final */String value = map.get(key);
		if (null == value) {
			final String[] values = (String[])request.getAttribute(key);
			if (null != values && 0 < values.length) { 
			  /*final String */returns = values[0];
			}
		}
		if (null == value) {
			final Map<String, String[]> parameters = request.getParameterMap();
			final String[] values = parameters.get(key);
			if (null != values && 0 < values.length && false == values[0].startsWith("{id")) { 
			  /*final String */returns = values[0];
			}
		} else {
			returns = value;
		}
		return returns;
	}
	
	public static StringBuffer appendIdAndView(final StringBuffer sb, final String id, final String label, final boolean ampersand) {
		if (null != sb) {
			if (true == ampersand) {
				sb.append("&");
			}
			if (null != id) {
				sb.append(label).append(":id=").append(id).append("&");
			}
			if (null == id) {
				sb.append(label).append(":view=many");
			} else {
				sb.append(label).append(":view=one");
			}
		}
		return sb;
	}
}