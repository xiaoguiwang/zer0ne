package dto.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String HEAD   = "HEAD"  ;
	private static final java.lang.String POST   = "POST"  ;
	private static final java.lang.String GET    = "GET"   ;
	private static final java.lang.String PUT    = "PUT"   ;
	private static final java.lang.String DELETE = "DELETE";
	
  //private static final String className = Servlet.class.getName();

  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpServletRequest request = req;
		final HttpServletResponse response = resp;
		final String method = request.getMethod();
		
		try {
		  //dro.lang.Event.entered(Servlet.class, this, "..", new Object[]{request,response});
			
			final String queryString = request.getQueryString();
		  /*final */String[] pairs = null;
			if (null != queryString) {
				pairs = queryString.split("&");
			}
			final Map<java.lang.String, java.lang.String> map = new HashMap<>();
			if (null != pairs) {
				for (final String pair: pairs) {
					final String[] split = pair.split("=", 2);
					if (1 == split.length) { 
						map.put(split[0], "");
					} else {
						map.put(split[0], split[1]);
					}
				}
			}
			
			if (false == Boolean.TRUE.booleanValue()) {
			} else if (true == HEAD.equals(method)) { // summary
			} else if (true == POST.equals(method)) { // create
				final BufferedReader br = request.getReader();
				StringBuffer sb = null;
				String line = "";
				while (line != null) {
					try {
						line = br.readLine();
						if (null == line) {
							break;
						}
						if (null == sb) {
							sb = new StringBuffer();
						}
						sb.append(line);
					} catch (final IOException e) {
						throw new RuntimeException(e);
					}
				}
				final String content = null == sb ? null : sb.toString();
				final String contentType = request.getContentType();
				if (true == contentType.equals("application/json") || true == contentType.startsWith("application/json;")) {
					final dro.Data d = dto.Data.fromJson(content); // FIXME, perform via a string lookup for json transformer
				} else {
				}
				response.setStatus(200);
			} else if (true == GET.equals(method)) { // read
				if (false == java.lang.Boolean.TRUE.booleanValue()) {
				} else if (null != map.get("$dro$meta$Data")) {
				  //final java.util.Map<java.lang.Object, dro.Data> __map = this.dc.returns(dro.Data.Return.Map);
				} else if (null != map.get("$dro$Data")) {
				  //final dro.Data __d = this.dc.returns(map.get("id"), dro.Data.Return.Data);
					response.setContentType("application/json");
				  //final String json = dto.Data.toJson(__d); // FIXME, perform via a string lookup for json transformer
				  //response.getOutputStream().println(json);
				} else {
				}
				response.setStatus(200);
			} else if (true == PUT.equals(method)) { // update
			} else if (true == DELETE.equals(method)) { // delete
			} else {
				throw new UnsupportedOperationException();
			}
		} catch (final Exception e) {
			throw e;
		} finally {
		  //dro.lang.Event.exiting(Servlet.class, this, "..", this);
		}
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}