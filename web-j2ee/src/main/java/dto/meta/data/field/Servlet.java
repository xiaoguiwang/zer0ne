package dto.meta.data.field;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet extends javax.servlet.http.HttpServlet {
	private static final long serialVersionUID = 0L;
	
	private static final java.lang.String HEAD   = "HEAD"  ;
	private static final java.lang.String POST   = "POST"  ;
	private static final java.lang.String GET    = "GET"   ;
	private static final java.lang.String PUT    = "PUT"   ;
	private static final java.lang.String DELETE = "DELETE";
	
  //private static final String className = Servlet.class.getName();

  //private static final java.util.logging.Logger logger = dro.util.Logger.getLogger(className);
	
	protected dro.util.Properties p = null;
	
	{
	  //p = null;
	}
	
	public Servlet properties(final dro.util.Properties p) {
		this.p = p;
		return this;
	}
	public dro.util.Properties properties() {
		return this.p;
	}
	
	protected Servlet doMethod(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		return this;
	}
	
	@Override
	public void doOptions(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void doHead(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		this.doMethod(req, resp);
	}
	@Override
	public void doTrace(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		throw new UnsupportedOperationException();
	}
}