<%@ page language="java" import="dro.*,dro.meta.*,dro.data.*" trimDirectiveWhitespaces="true" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/d-tags" prefix="d" %>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/df-tags" prefix="df" %>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/mdf-tags" prefix="mdf" %>
<%
final String[] href = (String[])request.getAttribute("d:href");
final String view = (String)request.getAttribute("d:view");
@SuppressWarnings("unchecked")
final java.util.List<dro.Data> list = (java.util.List<dro.Data>)request.getAttribute("d:list");
final String[] ids = (String[])request.getAttribute("d:id");
%>
<br/>
dro.Data
<br/>

<%
/*final */dro.Data d = null;
/*final */String id = null;
/*final */String md_id = null;
/*final */String dc_id = null;
if (null != view && true == view.equals("one")) {
/*final dro.Data */d = list.get(0);
  pageContext.setAttribute("d", d);
/*final String */id = d.id().toString();
%>

<table border=1>
<tr>
<td align="right"><d:label value="$id" data="${d}"/></td>
<td align="center"><d:id data="${d}"/></td>
</tr>
<tr>
<td align="right"><d:label value="$id#md" data="${d}"/></td>
<td align="center"><d:md data="${d}"/></td>
</tr>
<tr>
<td align="right"><d:label value="$id#dc" data="${d}"/></td>
<td align="center"><d:dc data="${d}"/></td>
</tr>
<tr>
<td align="right"><d:label value="$created-date-time-stamp" data="${d}"/></td>
<td align="center"><d:value key="$created-date-time-stamp" data="${d}"/></td>
</tr>
<tr>
<td align="right"><d:label value="$accessed-date-time-stamp" data="${d}"/></td>
<td align="center"><d:value key="$accessed-date-time-stamp" data="${d}"/></td>
</tr>
<tr>
<td align="right"><d:label value="$modified-date-time-stamp" data="${d}"/></td>
<td align="center"><d:value key="$modified-date-time-stamp" data="${d}"/></td>
</tr>
<!--
<tr>
<td align="right">df:</td>
<td align="center">
<input id="id:df:mdf" name="df:mdf:id" type="hidden" value="--><!--%=null == mdf_id || 0 == mdf_id.length || null == mdf_id[0] ? "" : mdf_id[0]%>"/--><!--
<input id="id:df:d" name="df:d:id" type="hidden" value="--><!--%=__d_id[0]%>"/--><!--
<input id="id:df:submit-create" name="submit:df" type="submit"
--><!--%=true == __d_id[0].equals("{id}") || null == mdf_id || 0 == mdf_id.length || null == mdf_id[0] ? "disabled" : ""%--><!--
value="create"/>
</td>
</tr>
-->
<tr>
<td align="right"></td>
<td align="center">
<d:submit value="cancel" data="${d}" href="true"/>
<d:submit value="create" data="${d}"/>
<d:submit value="update" data="${d}"/>
</td>
</tr>
</table>

<%
} else if (null == view || true == view.equals("many")) {
%>

<table border=1>
<tr>
<td></td>
<td align="center"><d:label value="$id" data="${d}"/></td>
<td align="center"><d:label value="$id#md" data="${d}"/></td>
<td align="center"><d:label value="$id#dc" data="${d}"/></td>
</tr>

<%
if (null != ids) {
	for (int i = 0; i < ids.length; i++) {
	  /*final dro.Data */d = list.get(i);
		pageContext.setAttribute("d", d);
	  /*final String */id = d.id().toString();
		final String id_md = d.get(dro.meta.Data.$id).toString();
		final String id_dc = d.get(dro.data.Context.$id).toString();
%>

<tr>
<td align="center"><d:checkbox data="${d}"/></td>
<td align="center"><d:id data="${d}" href="true"/></td>
<td align="center"><d:md data="${d}"/></td>
<td align="center"><d:dc data="${d}"/></td>
</tr>

<%
	}
}
%>

</table>
<br/>

<d:submit value="create" data="${d}"/>
<d:submit value="update" data="${d}"/>
<d:submit value="delete" data="${d}"/>
<br/>

<%
} else if (null == view || true == view.equals("data")) {
%>

<%
/*final dro.Data */d = list.get(0);
pageContext.setAttribute("d", d);
/*final */dro.meta.Data md = d.returns(dro.meta.Data.Return.Data);
/*final */java.util.Set<dro.data.Field> set = d.returns(dro.data.Field.Set.Return.Set);
%>

<table border=1>

<tr>
<td></td>
<%
for (final dro.data.Field df: set) {
	final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
	pageContext.setAttribute("mdf", mdf); // TODO: input id hidden=true
%>
<td align="center"><mdf:search field="${mdf}" order='ascending' value='^'/></td>
<%
}
%>
<td></td>
</tr>

<tr>
<td></td>
<%
for (final dro.data.Field df: set) {
	final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
	pageContext.setAttribute("mdf", mdf); // TODO: input id hidden=true
%>
<td align="center"><mdf:name field="${mdf}" immutable='true'/></td>
<%
}
%>
<td></td>
</tr>

<tr>
<td align="right"><d:submit value='search' data="${d}"/></td>
<%
for (final dro.data.Field df: set) {
  //pageContext.setAttribute("df", df); // TODO: input id hidden=true
	final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
	pageContext.setAttribute("mdf", mdf); // TODO: input id hidden=true
%>
<td align="center"><mdf:search field="${mdf}"/></td>
<%
}
%>
<td align="left"><d:label value='search:clear' data="${d}"/></td>
</tr>

<%
for (int i = 0; i < (null == ids ? 0 : ids.length); i++) {
  /*final dro.Data */d = list.get(i);
	pageContext.setAttribute("d", d); // TODO: input id hidden=true 
	if (true == ids[i].equals("{id}")) continue;
%>
<tr>
<td></td><!-- TODO: checkbox for update -->
<%
  /*final dro.meta.Data */md = d.returns(dro.meta.Data.Return.Data);
  /*final java.util.Set<dro.data.Field> */set = d.returns(dro.data.Field.Set.Return.Set);
  /*final String */id = d.id().toString();
	for (final dro.data.Field df: set) {
		pageContext.setAttribute("df", df); // TODO: input id hidden=true
%>
<td align="center"><df:value field="${df}"/></td>
<%
	}
%>
<td></td>
</tr>
<%
}
%>

<tr>
<td></td>
<%
for (final dro.data.Field df: set) {
	final dro.meta.data.Field mdf = df.returns(dro.meta.data.Field.Return.Field);
	pageContext.setAttribute("mdf", mdf); // TODO: input id hidden=true
%>
<td align="center"><mdf:search field="${mdf}" order='descending' value='v'/></td>
<%
}
%>
<td></td>
</tr>

</table>
<br/>

<d:submit value="create" data="${d}"/>
<d:submit value="update" data="${d}"/>
<d:submit value="delete" data="${d}"/>
<br/>

<%
}
%>