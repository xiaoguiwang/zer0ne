<%@ page language="java" import="dro.data.*,dro.meta.data.*" trimDirectiveWhitespaces="true" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/dc-tags" prefix="dc" %>
<%
final java.lang.String[] href = (String[])request.getAttribute("dc:href");
final java.lang.String view = (String)request.getAttribute("dc:view");
@SuppressWarnings("unchecked")
final java.util.List<dro.data.Context> list = (java.util.List<dro.data.Context>)request.getAttribute("dc:list");
final String[] ids = (String[])request.getAttribute("dc:id");
%>
<br/>
dro.data.Context
<br/>

<%
/*final */dro.data.Context dc = null;
/*final */String id = null;
/*final */String mdc_id = null;
if (null != view && true == view.equals("one")) {
  /*dro.data.Context */dc = list.get(0);
  pageContext.setAttribute("dc", dc);
/*final String */id = dc.id().toString();
%>

<table border=1>
<tr>
<td align="right"><dc:label value="$id" context="${dc}"/></td>
<td align="center"><dc:id context="${dc}"/></td>
</tr>
<tr>
<td align="right"><dc:label value="$id#mdc" context="${dc}"/></td>
<td align="center"><dc:mdc context="${dc}"/></td>
</tr>
<tr>
<td align="right"><dc:label value="$created-date-time-stamp" context="${dc}"/></td>
<td align="center"><dc:value key="$created-date-time-stamp" context="${dc}"/></td>
</tr>
<tr>
<td align="right"><dc:label value="$accessed-date-time-stamp" context="${dc}"/></td>
<td align="center"><dc:value key="$accessed-date-time-stamp" context="${dc}"/></td>
</tr>
<tr>
<td align="right"><dc:label value="$modified-date-time-stamp" context="${dc}"/></td>
<td align="center"><dc:value key="$modified-date-time-stamp" context="${dc}"/></td>
</tr>
<!--
<tr>
<td align="right">d:</td>
<td align="center">
<input id="id:d:md" name="d:md:id" type="hidden" value="--><!--%=null == md_id || 0 == md_id.length || null == md_id[0] ? "" : md_id[0]%>"/--><!--
<input id="id:d:dc" name="d:dc:id" type="hidden" value="--><!--%=__dc_id[0]%>"/--><!--
<input id="id:d:submit-create" name="submit:d" type="submit"
--><!--%=true == __dc_id[0].equals("{id}") || null == md_id || 0 == md_id.length || null == md_id[0] ? "disabled" : ""%--><!--
value="create"/>
</td>
</tr>
-->
<tr>
<td align="right"></td>
<td align="center">
<dc:submit value="cancel" context="${dc}" href="true"/>
<dc:submit value="create" context="${dc}"/>
<dc:submit value="update" context="${dc}"/>
</td>
</tr>
</table>

<%
} else if (null == view || true == view.equals("many")) {
%>

<table border=1>
<tr>
<td></td>
<td align="center"><dc:label value="$id" context="${dc}"/></td>
<td align="center"><dc:label value="$id#mdc" context="${dc}"/></td>
</tr>

<%
if (null != ids) {
	for (int i = 0; i < ids.length; i++) {
	  /*final dro.data.Context */dc = list.get(i);
		pageContext.setAttribute("dc", dc);
	  /*final String */id = dc.id().toString();
		final String id_mdc = dc.get(dro.meta.data.Context.$id).toString();
%>

<tr>
<td align="center"><dc:checkbox context="${dc}"/></td>
<td align="center"><dc:id context="${dc}" href="true"/></td>
<td align="center"><dc:mdc context="${dc}"/></td>
</tr>

<%
	}
}
%>

</table>
<br/>

<dc:submit value="create" context="${dc}"/>
<dc:submit value="update" context="${dc}"/>
<dc:submit value="delete" context="${dc}"/>
<br/>

<%
}
%>