<%@ page language="java" import="dro.data.*,dro.meta.data.*" trimDirectiveWhitespaces="true" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/df-tags" prefix="df" %>
<%
final String[] href = (String[])request.getAttribute("df:href");
final String view = (String)request.getAttribute("df:view");
@SuppressWarnings("unchecked")
final java.util.List<dro.data.Field> list = (java.util.List<dro.data.Field>)request.getAttribute("df:list");
final String[] ids = (String[])request.getAttribute("df:id");
%>
<br/>
dro.data.Field
<br/>

<%
/*final */dro.data.Field df = null;
/*final */String id = null;
if (null != view && true == view.equals("one")) {
/*final dro.data.Field */df = list.get(0);
  pageContext.setAttribute("df", df);
/*final String */id = df.id().toString();
%>

<table border=1>
<tr>
<td align="right"><df:label value="$id" field="${df}"/></td>
<td align="center"><df:id field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$id#d" field="${df}"/></td>
<td align="center"><df:d field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$id#mdf" field="${df}"/></td>
<td align="center"><df:mdf field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$name#mdf" field="${df}"/></td>
<td align="center"><df:mdf value="mdf:name" field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$type#mdf" field="${df}"/></td>
<td align="center"><df:mdf value="mdf:type" field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$created-date-time-stamp" field="${df}"/></td>
<td align="center"><df:value key="$created-date-time-stamp" field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$accessed-date-time-stamp" field="${df}"/></td>
<td align="center"><df:value key="$accessed-date-time-stamp" field="${df}"/></td>
</tr>
<tr>
<td align="right"><df:label value="$modified-date-time-stamp" field="${df}"/></td>
<td align="center"><df:value key="$modified-date-time-stamp" field="${df}"/></td>
</tr>
<!--
<tr>
<td align="right">
--><!--% if (true == mdf_limit[0].equals("") || true == mdf_limit[0].equals("{0:1}")) { %--><!--
$value
--><!--% } else if (true == mdf_lookup[0].equals("")) { %--><!--
@value
--><!--% } else { %--><!--
%value
--><!--% } %--><!--
</td>
<td align="center">
<input id="id:df:value" name="df:value" type="text" value="--><!--%=__df_value[0]%>"/--><!--
</td>
</tr>
-->
<tr>
<td align="right"></td>
<td align="center">
<df:submit value="cancel" field="${df}" href="true"/>
<df:submit value="create" field="${df}"/>
<df:submit value="update" field="${df}"/>
</td>
</tr>
</table>

<%
} else if (null == view || true == view.equals("many")) {
%>

<table border=1>
<tr>
<td></td>
<td align="center"><df:label value="$id" field="${df}"/></td>
<td align="center"><df:label value="$id#d" field="${df}"/></td>
<td align="center"><df:label value="$id#mdf" field="${df}"/></td>
<td align="center"><df:label value="$name#mdf" field="${df}"/></td>
<td align="center"><df:label value="$type#mdf" field="${df}"/></td>
<td align="center"><df:label value="$value" field="${df}"/></td>
</tr>

<%
if (null != ids) {
	for (int i = 0; i < ids.length; i++) {
	  /*final dro.data.Field */df = list.get(i);
		pageContext.setAttribute("df", df);
	  /*final String */id = df.id().toString();
%>

<tr>
<td align="center"><df:checkbox field="${df}"/></td>
<td align="center"><df:id field="${df}" href="true"/></td>
<td align="center"><df:d field="${df}"/></td>
<td align="center"><df:mdf field="${df}"/></td>
<td align="center"><df:mdf value="mdf:name" field="${df}"/></td>
<td align="center"><df:mdf value="mdf:type" field="${df}"/></td>
<td align="center"><df:value field="${df}"/></td>
</tr>

<%
	}
}
%>

</table>
<br/>

<df:submit value="create" field="${df}"/>
<df:submit value="update" field="${df}"/>
<df:submit value="delete" field="${df}"/>
<br/>

<%
}
%>