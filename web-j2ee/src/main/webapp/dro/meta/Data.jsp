<%@ page language="java" import="dro.meta.*,dro.meta.data.*" trimDirectiveWhitespaces="true" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/md-tags" prefix="md" %>
<%
final String[] href = (String[])request.getAttribute("md:href");
final String view = (String)request.getAttribute("md:view");
@SuppressWarnings("unchecked")
final java.util.List<dro.meta.Data> list = (java.util.List<dro.meta.Data>)request.getAttribute("md:list");
final String[] ids = (String[])request.getAttribute("md:id");
%>
<br/>
dro.meta.Data
<br/>

<%
/*final */dro.meta.Data md = null;
/*final */String id = null;
/*final */String mdc_id = null;
if (null != view && true == view.equals("one")) {
/*final dro.meta.Data */md = list.get(0);
  pageContext.setAttribute("md", md);
/*final String */id = md.id().toString();
/*final String */mdc_id = md.get(dro.meta.data.Context.$id).toString();
%>

<table border=1>
<tr>
<td align="right"><md:label value="$id" data="${md}"/></td>
<td align="center"><md:id data="${md}"/></td>
</tr>
<tr>
<td align="right"><md:label value="$id#mdc" data="${md}"/></td>
<td align="center"><md:mdc data="${md}"/></td>
</tr>
<tr>
<td align="right"><md:label value="$name" data="${md}"/></td>
<td align="center"><md:name data="${md}"/></td>
</tr>
<tr>
<td align="right"><md:label value="$created-date-time-stamp" data="${md}"/></td>
<td align="center"><md:value key="$created-date-time-stamp" data="${md}"/></td>
</tr>
<tr>
<td align="right"><md:label value="$accessed-date-time-stamp" data="${md}"/></td>
<td align="center"><md:value key="$accessed-date-time-stamp" data="${md}"/></td>
</tr>
<tr>
<td align="right"><md:label value="$modified-date-time-stamp" data="${md}"/></td>
<td align="center"><md:value key="$modified-date-time-stamp" data="${md}"/></td>
</tr>
<!--
<tr>
<td align="right">mdf:</td>
<td align="center">
<input id="id:mdf:md" name="mdf:md:id" type="hidden" value="--><!--%=__md_id[0]%>"/--><!--
<input id="id:mdf:submit-create" name="submit:mdf" type="submit"
--><!--%=true == __md_id[0].equals("{id}") ? "disabled" : ""%--><!--
value="create"/>
</td>
</tr>
-->
<!--
<tr>
<td align="right">d:</td>
<td align="center">
<input id="id:d:md" name="d:md:id" type="hidden" value="--><!--%=__md_id[0]%>"/--><!--
<input id="id:d:dc" name="d:dc:id" type="hidden" value="--><!--%=null == dc_id || 0 == dc_id.length || null == dc_id[0] ? "" : dc_id[0]%>"/--><!--
<input id="id:d:submit-create" name="submit:d" type="submit"
--><!--%=true == __md_id[0].equals("{id}") || null == dc_id || 0 == dc_id.length || null == dc_id[0] ? "disabled" : ""%--><!--
value="create"/>
</td>
</tr>
-->
<tr>
<td align="right"></td>
<td align="center">
<md:submit value="cancel" data="${md}" href="true"/>
<md:submit value="create" data="${md}"/>
<md:submit value="update" data="${md}"/>
</td>
</tr>
</table>

<%
} else if (null == view || true == view.equals("many")) {
%>

<table border=1>
<tr>
<td></td>
<td align="center"><md:label value="$id" data="${md}"/></td>
<td align="center"><md:label value="$id#mdc" data="${md}"/></td>
<td align="center"><md:label value="$name" data="${md}"/></td>
</tr>

<%
if (null != ids) {
	for (int i = 0; i < ids.length; i++) {
	  /*final dro.meta.Data */md = list.get(i);
		pageContext.setAttribute("md", md);
	  /*final String */id = md.id().toString();
%>

<tr>
<td align="center"><md:checkbox data="${md}"/></td>
<td align="center"><md:id data="${md}" href="true"/></td>
<td align="center"><md:mdc data="${md}"/></td>
<td align="center"><md:name data="${md}"/></td>
</tr>

<%
	}
}
%>

</table>
<br/>

<md:submit value="create" data="${md}"/>
<md:submit value="update" data="${md}"/>
<md:submit value="delete" data="${md}"/>
<br/>

<%
}
%>