<%@ page language="java" import="dro.meta.data.*" trimDirectiveWhitespaces="true" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/mdc-tags" prefix="mdc" %>
<%
final String[] href = (String[])request.getAttribute("mdc:href");
final String view = (String)request.getAttribute("mdc:view");
@SuppressWarnings("unchecked")
final java.util.List<dro.meta.data.Context> list = (java.util.List<dro.meta.data.Context>)request.getAttribute("mdc:list");
final String[] ids = (String[])request.getAttribute("mdc:id");
%>
<br/>
dro.meta.data.Context
<br/>

<%
/*final */dro.meta.data.Context mdc = null;
/*final */String id = null;
if (null != view && true == view.equals("one")) {
/*final dro.meta.data.Context */mdc = list.get(0);
  pageContext.setAttribute("mdc", mdc);
/*final String */id = mdc.id().toString();
%>

<table border=1>
<tr>
<td align="right"><mdc:label value="$id" context="${mdc}"/></td>
<td align="center"><mdc:id context="${mdc}"/></td>
</tr>
<tr>
<td align="right"><mdc:label value="$name" context="${mdc}"/></td>
<td align="center"><mdc:name context="${mdc}"/></td>
</tr>
<tr>
<td align="right"><mdc:label value="$created-date-time-stamp" context="${mdc}"/></td>
<td align="center"><mdc:value key="$created-date-time-stamp" context="${mdc}"/></td>
</tr>
<tr>
<td align="right"><mdc:label value="$accessed-date-time-stamp" context="${mdc}"/></td>
<td align="center"><mdc:value key="$accessed-date-time-stamp" context="${mdc}"/></td>
</tr>
<tr>
<td align="right"><mdc:label value="$modified-date-time-stamp" context="${mdc}"/></td>
<td align="center"><mdc:value key="$modified-date-time-stamp" context="${mdc}"/></td>
</tr>
<!--
<tr>/
<td align="right">md:</td>
<td align="center">
<input id="id:md:mdc" name="md:mdc:id" type="text" hidden=true value="--><!--%=__mdc_id[0]%>"/--><!--
<input id="id:md:submit-create" name="submit:md" type="submit"
--><!--%=true == __mdc_id[0].equals("{id}") ? "disabled" : ""%--><!--
value="create"/>
</td>
</tr>
-->
<!--
<tr>
<td align="right">dc:</td>
<td align="center">
<input id="id:dc:mdc" name="dc:mdc:id" type="text" hidden=true value="--><!--%=__mdc_id[0]%>"/--><!--
<input id="id:dc:submit-create" name="submit:dc" type="submit"
--><!--%=true == __mdc_id[0].equals("{id}") ? "disabled" : ""%--><!--
value="create"/>
</td>
</tr>
-->
<tr>
<td align="right"></td>
<td align="center">
<mdc:submit value="cancel" context="${mdc}" href="true"/>
<mdc:submit value="create" context="${mdc}"/>
<mdc:submit value="update" context="${mdc}"/>
</td>
</tr>
</table>

<%
} else if (null == view || true == view.equals("many")) {
%>

<table border=1>
<tr>
<td></td>
<td align="center"><mdc:label value="$id" context="${mdc}"/></td>
<td align="center"><mdc:label value="$name" context="${mdc}"/></td>
</tr>

<%
if (null != ids) {
	for (int i = 0; i < ids.length; i++) {
	  /*final dro.meta.data.Context */mdc = list.get(i);
		pageContext.setAttribute("mdc", mdc);
	  /*final String */id = mdc.id().toString();
%>

<tr>
<td align="center"><mdc:checkbox context="${mdc}"/></td>
<td align="center"><mdc:id context="${mdc}" href="true"/></td>
<td align="center"><mdc:name context="${mdc}"/></td>
</tr>

<%
	}
}
%>

</table>
<br/>

<mdc:submit value="create" context="${mdc}"/>
<mdc:submit value="update" context="${mdc}"/>
<mdc:submit value="delete" context="${mdc}"/>
<br/>

<%
}
%>