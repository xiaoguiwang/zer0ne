<%@ page language="java" import="dro.meta.*,dro.meta.data.*" trimDirectiveWhitespaces="true" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="https://zer0ne.net/jsp/tlds/mdf-tags" prefix="mdf" %>
<%
final String[] href = (String[])request.getAttribute("mdf:href");
final String view = (String)request.getAttribute("mdf:view");
@SuppressWarnings("unchecked")
final java.util.List<dro.meta.data.Field> list = (java.util.List<dro.meta.data.Field>)request.getAttribute("mdf:list");
final String[] ids = (String[])request.getAttribute("mdf:id");
%>
<br/>
dro.meta.data.Field
<br/>

<%
/*final */dro.meta.data.Field mdf = null;
/*final */String id = null;
if (null != view && true == view.equals("one")) {
/*final dro.meta.data.Field */mdf = list.get(0);
  pageContext.setAttribute("mdf", mdf);
/*final String */id = mdf.id().toString();
%>

<table border=1>
<tr>
<td align="right"><mdf:label value="$id" field="${mdf}"/></td>
<td align="center"><mdf:id field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$id#md" field="${mdf}"/></td>
<td align="center"><mdf:md field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$name" field="${mdf}"/></td>
<td align="center"><mdf:name field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$type" field="${mdf}"/></td>
<td align="center"><mdf:type field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$limit" field="${mdf}"/></td>
<td align="center"><mdf:limit field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$unique" field="${mdf}"/></td>
<td align="center"><mdf:unique field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$order" field="${mdf}"/></td>
<td align="center"><mdf:order field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$lookup" field="${mdf}"/></td>
<td align="center"><mdf:lookup field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$created-date-time-stamp" field="${mdf}"/></td>
<td align="center"><mdf:value key="$created-date-time-stamp" field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$accessed-date-time-stamp" field="${mdf}"/></td>
<td align="center"><mdf:value key="$accessed-date-time-stamp" field="${mdf}"/></td>
</tr>
<tr>
<td align="right"><mdf:label value="$modified-date-time-stamp" field="${mdf}"/></td>
<td align="center"><mdf:value key="$modified-date-time-stamp" field="${mdf}"/></td>
</tr>
<tr>
<td align="right"></td>
<td align="center">
<mdf:submit value="cancel" field="${mdf}" href="true"/>
<mdf:submit value="create" field="${mdf}"/>
<mdf:submit value="update" field="${mdf}"/>
</td>
</tr>
</table>

<%
} else if (null == view || true == view.equals("many")) {
%>

<table border=1>
<tr>
<td></td>
<td align="center"><mdf:label value="$id" field="${mdf}"/></td>
<td align="center"><mdf:label value="$id#md" field="${mdf}"/></td>
<td align="center"><mdf:label value="$name" field="${mdf}"/></td>
<td align="center"><mdf:label value="$type" field="${mdf}"/></td>
<td align="center"><mdf:label value="$limit" field="${mdf}"/></td>
<td align="center"><mdf:label value="$unique" field="${mdf}"/></td>
<td align="center"><mdf:label value="$order" field="${mdf}"/></td>
<td align="center"><mdf:label value="$lookup" field="${mdf}"/></td>
</tr>

<%
if (null != ids) {
	for (int i = 0; i < ids.length; i++) {
	  /*final dro.meta.data.Field */mdf = list.get(i);
		pageContext.setAttribute("mdf", mdf);
	  /*final String */id = mdf.id().toString();
%>

<tr>
<td align="center"><mdf:checkbox field="${mdf}"/></td>
<td align="center"><mdf:id field="${mdf}" href="true"/></td>
<td align="center"><mdf:md field="${mdf}"/></td>
<td align="center"><mdf:name field="${mdf}"/></td>
<td align="center"><mdf:type field="${mdf}"/></td>
<td align="center"><mdf:limit field="${mdf}"/></td>
<td align="center"><mdf:unique field="${mdf}"/></td>
<td align="center"><mdf:order field="${mdf}"/></td>
<td align="center"><mdf:lookup field="${mdf}"/></td>
</tr>

<%
	}
}
%>

</table>
<br/>

<mdf:submit value="create" field="${mdf}"/>
<mdf:submit value="update" field="${mdf}"/>
<mdf:submit value="delete" field="${mdf}"/>
<br/>

<%
}
%>