// ajax.js

function AjaxPostRequest(qs, json, cb) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState) {
			AjaxPostResponse(this, cb);
		}
	}
	x.open('POST', '/dro/meta/Data?'+qs, true);
	x.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	try {
		x.send(json);
	} catch (error) {
	}
	return x;
}
function AjaxPostResponse(x, cb) {
	if (200 == this.status) {
		cb(x);
	} else {+
		window.alert(x.responseXML);
	}
}

function AjaxHeadRequest(qs, cb) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState) {
			AjaxHeadResponse(this, cb);
		}
	}
	x.open('HEAD', '/dro/meta/Data?'+qs, true);
	try {
		x.send();
	} catch (error) {
	}
	return x;
}
function AjaxHeadResponse(x, cb) {
	if (200 == x.status) {
		cb(x);
	} else {
		window.alert(x.responseXML);
	}
}

function AjaxGetRequest(qs, cb) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState) {
			AjaxGetResponse(this, cb);
		}
	}
	x.open('GET', '/dro/meta/Data?'+qs, true);
	try {
		x.send();
	} catch (error) {
	}
	return x;
}
function AjaxGetResponse(x, cb) {
	if (200 == x.status) {
		cb(x);
	} else {
		window.alert(x.responseXML);
	}
}

function AjaxPutRequest(qs, json, cb) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState) {
			AjaxPutResponse(this, cb);
		}
	}
	x.open('PUT', '/dro/meta/Data?'+qs, true);
	x.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	try {
		x.send(json);
	} catch (error) {
	}
	return x;
}
function AjaxPutResponse(x, cb) {
	if (200 == x.status) {
		cb(x);
	} else {
		window.alert(x.responseXML);
	}
}

function AjaxDeleteRequest(qs, cb) {
	var x = new XMLHttpRequest();
	x.onreadystatechange = function() {
		if (4 == this.readyState) {
			AjaxDeleteResponse(this, cb);
		}
	}
	x.open('DELETE', '/dro/meta/Data?'+qs, true);
	try {
		x.send();
	} catch (error) {
	}
	return x;
}
function AjaxDeleteResponse(x, cb) {
	if (200 == x.status) {
		cb(x);
	} else {
		window.alert(x.responseXML);
	}
}