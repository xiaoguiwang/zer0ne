# readme.md #

http://commons.apache.org/proper/commons-daemon/procrun.html

C:\zer0ne-0.0.1-SNAPSHOT-bin-windows\bin>prunsrv.exe //?
Usage: prunsrv command [ServiceName] [--options]
  Commands:
  help                   This page
  install [ServiceName]  Install Service
  update  [ServiceName]  Update Service parameters
  delete  [ServiceName]  Delete Service
  start   [ServiceName]  Start Service
  stop    [ServiceName]  Stop Service
  run     [ServiceName]  Run Service as console application
  pause   [Num Seconds]  Sleep for n Seconds (defaults to 60)
  version                Display version
  Options:
  --Description
  --DisplayName
  --Install
  --ServiceUser
  --ServicePassword
  --Startup
  --Type
  --DependsOn
  --Environment
  --User
  --Password
  --LibraryPath
  --JavaHome
  --Jvm
  --JvmOptions
  --JvmOptions9
  --Classpath
  --JvmMs
  --JvmMx
  --JvmSs
  --StopImage
  --StopPath
  --StopClass
  --StopParams
  --StopMethod
  --StopMode
  --StopTimeout
  --StartImage
  --StartPath
  --StartClass
  --StartParams
  --StartMethod
  --StartMode
  --LogPath
  --LogPrefix
  --LogLevel
  --StdError
  --StdOutput
  --LogJniMessages
  --PidFile
  --Rotate

C:\zer0ne-0.0.1-SNAPSHOT-bin-windows\bin>