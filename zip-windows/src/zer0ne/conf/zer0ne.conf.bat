@REM zer0ne.conf.bat

SET "exitCode=0"

@REM INTERNAL ENVIRONMENT VARIABLES - DO NOT CHANGE
SET "NameMixedCase=Zer0ne"
SET "NameLowerCase=zer0ne"
SET "Version=0.0.1-SNAPSHOT"
SET "ClassName=dro.main.Context"
SET "Mode=jvm"
SET "ServiceUser=LocalSystem"

@REM Turn this on and break SET after this point
@REM SETLOCAL EnableDelayedExpansion

IF NOT "x%JAVA_OPTS%" == "x" GOTO END_OF_JAVA_OPTS

@REM SET "JAVA_OPTS=-Xms64M -Xmx512M -XX:MaxMetaspaceSize=256m"
@REM SET "JAVA_OPTS=-Xms64M -Xmx512M"
@REM Logging
IF "x" == "x%JAVA_OPTS%" (
  SET "JAVA_OPTS=-Djava.util.logging.config.file=logging.properties"
) ELSE (
  SET "JAVA_OPTS=%JAVA_OPTS% -Djava.util.logging.config.file=logging.properties"
)
@REM Prefer IPv4
IF "x" == "x%JAVA_OPTS%" (
  SET "JAVA_OPTS=-Djava.net.preferIPv4Stack=true"
) ELSE (
  SET "JAVA_OPTS=%JAVA_OPTS% -Djava.net.preferIPv4Stack=true"
)

:END_OF_JAVA_OPTS

IF "x%JAVA_OPTS%" == "x" (
  SET "JAVA=java"
) ELSE (
  SET "JAVA=java %JAVA_OPTS%"
)

@REM http://commons.apache.org/proper/commons-daemon/procrun.html
@REM EXTERNAL ENVIRONMENT VARIABLES - CAN BE CHANGED
@REM SET "Jvm=C:\PROGRA~1\Java\jdk1.7.0_80\jre\bin\server\jvm.dll"
@REM SET "Jvm=C:\Program Files\Java\jdk1.8.0_92\jre\bin\server\jvm.dll"
SET "Jvm=%JAVA_HOME%\jre\bin\server\jvm.dll"
@REM SET "Jvm=auto"
@REM LogLevel: Error, Info, Warn, Debug
SET "LogLevel=Debug"

EXIT /B !exitCode!